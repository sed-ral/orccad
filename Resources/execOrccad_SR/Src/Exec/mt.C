//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Src/Exec/mt.C
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 beta
// Creation       : 18 May 1994
//
//***************************** Description ***********************************
//
//           Implementation for Orccad Exec Runtine of
//           a Module Task.
//
// Class Definition:
//    ModuleTask:  controls a real-time algorithm
//    ListModuleTask: manages a  ModuleTask list
//
//
//**************************** Modifications **********************************
//
// 6 November 1996  Integration in Orccad Exec
// MODIF 1: RPG MODIF 15 June 99 (relax the condition of overrun)
//
// 3 July 2000 : Soraya Arias
//               Modification to enable time display when errors
//               (RT->GetGlobalTime())
//
// 25 August 2006 RPG Add methods for param handling within MT context
//                No more param handling within algorithic module context
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#include <string.h>

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/clockgen.h"

ModuleTask::ModuleTask(FUNCPTR f_ptr, RobotTask *rt,
                       char *name, int ItFilter, int indexclk)
    : Global(rt->GetPrr())
{
    Status = 1;
    State = MT_NOT_CREATED;
    Next = Previous = (ModuleTask *)NULL;
    for (int i = 0; i < USED_SEM; i++) {
        if ((SemTable[i] =  orcSemCreate(ORCEMPTY)) == NULL) {
            Status = 0;
            break;
        }
    }

    if (indexclk < 0) {
        Status = 0;
    }
    else {
        IndexClk = indexclk;
        Sample_time = Clock->GetHarmonicPeriod(indexclk);
    }

    CondEnd       = FALSE;
    CondKill      = FALSE;
    RequestParam  = FALSE;
    OnlineParam   = TRUE;

    Name = new char[strlen(name) + 1];
    strcpy(Name, name);
    func = f_ptr;
    RT = rt;
    Filter_Iteration = ItFilter;
    // RPG#param 25/08/06 vvvvv
    pTabParam = NULL;
    // RPG#param 25/08/06 ^^^^^
}

ModuleTask::~ModuleTask()
{
    CondEnd = CondKill = TRUE;
    for (int i = 0; i < USED_SEM; i++) {
        orcSemDelete(SemTable[i]);
    }
    delete Name;
}

void ModuleTask::DeleteTask()
{
    CondEnd = CondKill = TRUE;
    if (State != MT_NOT_CREATED) {        // MtId a qqch valable
        if (orcTaskIdVerify(MtId) == OK) {
            if (orcTaskDelete(MtId) == ERROR) {
                //fprintf(stderr, "ModuleTask::DeleteTask %s not deleted\n ", Name);
            }
            else {
                //fprintf(stderr, "ModuleTask::DeleteTask %s  deleted\n ", Name);
            }
        }
    }
}

STATUS ModuleTask::create()
{
    if (orcSpawn(&MtId, Name, PRI_MT, BIG_STACK, (FUNCPTR) func, (void *)this) == ERROR) {
        EventStorage->displayError(MT_ERROR3, RT->GetGlobalTime());
        State = MT_NOT_CREATED;
        return ERROR;
    }
#ifdef _DEBUG
    fprintf(stderr, "ModuleTask::spawn  task ID = %x \n", (unsigned int)MtId);
#endif
    State = MT_FREE;

    return OK;
}

void ModuleTask::BeginCompute()
{
    if (State == MT_OCCUPY) {
        int state = RT->GetState();
        State = MT_OVERRUN;
        //RPG MODIF 15 June 99
        if ((state == ACTIVE) || (state == TRANSITE)) {
            fprintf(stderr, "ModuleTask %s : Overrun detected\n", Name);
            EventStorage->displayError(MT_ERROR6, RT->GetGlobalTime());
            EventInterface->SendEvent(SOFT_FAIL, Name,
                                      RT->GetGlobalTime());
        }
    }
    else {
        State = MT_OCCUPY;
    }
}

void ModuleTask::EndCompute()
{
    State = MT_FREE;
}

/*---------------------------------------------------------------------------
 *
 *  void ModuleTask::SemaphoreGive(int index)
 *
 * Action :
 *
 * Inclure :
 *
 * Parametres de sortie :
 *
 * Parametres d'entree :
 *
 * Parametres d'entree/sortie :
 *
 * Retour :
 *
 * Description :
 *
 *---------------------------------------------------------------------------
 */
STATUS ModuleTask::SemaphoreGive(int index)
{
    if (index > USED_SEM || index < 0 ) {
        EventStorage->displayError(MT_ERROR1, RT->GetGlobalTime());
        EventInterface->SendEvent(SOFT_FAIL, Name,
                                  RT->GetGlobalTime());
        return ERROR;
    }

    if (orcSemGive(SemTable[index]) == ERROR) {
        Status = 0;
        EventStorage->displayError(MT_ERROR4, RT->GetGlobalTime());
        EventInterface->SendEvent(SOFT_FAIL, Name,
                                  RT->GetGlobalTime());
        return ERROR;
    }
    EventStorage->displaySem(index, SEM_GIVE, RT->GetGlobalTime());
    //    EventStorage->displaySem(index,SEM_GIVE,0.0);
    return OK;
}

/*---------------------------------------------------------------------------
 *
 * void ModuleTask::SemaphoreTake(int index, int timeout)
 *
 * Action :
 *
 * Inclure :
 *
 * Parametres de sortie :
 *
 * Parametres d'entree :
 *
 * Parametres d'entree/sortie :
 *
 * Retour :
 *
 * Description :
 *
 * Erreurs :
 *   301: Semaphore index > USED_SEM
 *---------------------------------------------------------------------------
 */
STATUS ModuleTask::SemaphoreTake(int index, int timeout)
{
    if (index > USED_SEM || index < 0) {
        EventStorage->displayError(MT_ERROR1, RT->GetGlobalTime());
        EventInterface->SendEvent(SOFT_FAIL, Name,
                                  RT->GetGlobalTime());
        return ERROR;
    }
#ifdef _DEBUG
    fprintf(stderr, "ModuleTask::SemaphoreTake orcSemTake SemTable[%d] BEGIN \n", index);
#endif
    if (orcSemTake(SemTable[index], timeout) == ERROR) {
        Status = 0;
        EventStorage->displayError(MT_ERROR5, RT->GetGlobalTime());
        EventInterface->SendEvent(SOFT_FAIL, Name,
                                  RT->GetGlobalTime());
        return ERROR;
    }
#ifdef _DEBUG
    fprintf(stderr, "ModuleTask::SemaphoreTake orcSemTake END OK SemTable[%d]\n", index);
#endif
    //    EventStorage->displaySem(index,SEM_TAKE,0.0);
    EventStorage->displaySem(index, SEM_TAKE, RT->GetGlobalTime());

    return OK;
}

/*---------------------------------------------------------------------------
 *
 * void ModuleTask::PlugParam()
 *
 * Action :
 *
 * Inclure :
 *
 * Parametres de sortie :
 *
 * Parametres d'entree :
 *
 * Parametres d'entree/sortie :
 *
 * Retour :
 *
 * Description :
 *
 *---------------------------------------------------------------------------
 */
void ModuleTask::PlugParam()
{
    pTabParam = RT->GetPtrParameters();
}

int ListModuleTask::insert(ModuleTask *MT)
{
    if (!MT->GetStatus()) {
        return 0;
    }
    if (List == NULL) {
        List = MT;
        Number++;
    }
    else {
        ModuleTask *mt = List;
        while (mt->Next != NULL) {
            mt = mt->Next;
        }
        MT->Previous = mt;
        mt->Next = MT;
        Number++;
    }
    return 1;
}

void ListModuleTask::DeleteTasks()
{
    ModuleTask *mt_next;
    ModuleTask *mt = List;
    while (mt) {
        mt_next = mt->Next;
        mt->DeleteTask();
        mt = mt_next;
    }
}
/*-----------------------------------------------------------------------------
 *   File end
 *-----------------------------------------------------------------------------
 */


