// *******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/myExecSystOrcc.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
// ***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime
//           abstract type, define, inline functions
//
// **************************** Modifications **********************************
//
// Author         :
// Date           :
//
// This file provides an interface between all Orccad system call
// and <exec system>
//
// Other files to be customized according to the exec system :
// * ${ORCCAD_SRC}/Inc/Exec/utils.h :
//   to add the above H file
// * ${ORCCAD_SRC}/Src/Exec/utils.C :
//   to add files in C if necessary to implement the system routines
// * ${ORCCAD_SYS}/Lib/data/Makefiles/makefile.<execSyst>
//   -> see makefile.execSyst as a Makefile to custom
// * ${ORCCAD_SRC}/Src/Exec/Makefile
//   -> see Makefile.type as a file to be customed
//
// *****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
// *****************************************************************************
#ifndef __mysystOrcc_h
#define __mysystOrcc_h

/////////////////////////
/// mySyst include
/////////////////////////

/**
 * Here list all the include files needed according to your system
 */
#include <errno.h>
#include <stdio.h>
#include <string.h>


/////////////////////////
/// Define & Typedef List
/////////////////////////
/**
 *  Here list the define and typedef needed
 */
#define OK 0
#define ERROR -1
#define TRUE 1
#define FALSE 0
#define STATUS int

// Value set when no delay is associated to operations
// on semaphores, message queues ...etc
#define WAIT_FOREVER 0
/*-------Message Queues----------*/
// Message Queue Type
#define ORCMSGQ int /* to be customized */
// Size of the Message Queue
#define SIZE_QUEUE 10 /* to be customized */
// Message Queue Priority
#define ORCNORMALPRI 0
#define ORCURGENTPRI 1

/*-------Semaphores----------*/
// Semaphore Type
#define ORCSEM_ID int /* to be customized */
// Value to set a binary semaphore empty
#define ORCEMPTY 0 /* to be customized */
// Value to set a binary semaphore full
#define ORCFULL  1    /* to be customized */
// Value to set the semaphore waiting delay
#define ORCFOREVER -1 /* to be customized */

/*-------Tasks----------*/
// Task Object Type
#define ORCTHR_ID int /* to be customized */
// Size Type of the stack assigned to a task
#define STACKSIZE int /* to be customized */

// Small Stack Size assigned to a task
#define SMALL_STACK  5000 /* to be customized */
// Usual Stack Size assigned to a task
#define NORMAL_STACK 10000 /* to be customized */
// Huge Stack Size assigned to a task
#define BIG_STACK    100000 /* to be customized */

// Task Priority Level
// Priority of the clock manage task (higher priority)
#define PRI_CLK 0 /* to be customized */
// Priority of the canal manage task (interface with Esterel automaton)
// PRI_CNL lower than PRI_CLK
#define PRI_CNL 0
// Priority of the robot task manage task ()
// PRI_RT lower than PRI_CNL
#define PRI_RT  0
// Priority of the module task manage task ()
#define PRI_MT  PRI_RT
// Priority of the main task (lower priority)
#define PRI_PRR PRI_RT - 1

/*-------Timers----------*/
// Timer Object Type
#define ORCTIMER_ID int /* to be customized */

/*-------Misc------------*/
// PathFile Name Max length
#define MAXFILENAME 80 /* to be customized */

// Type definition needed to point to the thread handler
typedef int (*FUNCPTR) (...); /* to be customized */
// Handler of end-timer routine type definition
typedef FUNCPTR TIMERHANDLER;


/*-------Message Queues----------*/
// Function to create message queues
inline ORCMSGQ orcMsgQCreate()
{
    return (ORCMSGQ)ERROR;
}


// Function to get the number of messages in queue
inline long orcMsgQNumMsgs(ORCMSGQ queue)
{
    return ERROR;
}

// Function to send messages on a message queue
inline int orcMsgQSend(ORCMSGQ queue, int msg, int prio)
{
    return ERROR;
}

// Function to receive messages on a message queue
inline int orcMsgQReceive(ORCMSGQ queue, int *msg)
{
    return ERROR;
}

// Function to close a message queue
inline int orcMsgQClose(ORCMSGQ queue)
{
    return ERROR;
}

/*-------Semaphores----------*/
// Function to create semaphores
inline ORCSEM_ID orcSemCreate(int initState)
{
    return (ORCSEM_ID) ERROR;
}


// Function to delete semaphores
inline int orcSemDelete(ORCSEM_ID sem)
{
    return ERROR;
}

// Function to give a semaphore
inline int orcSemGive(ORCSEM_ID sem)
{
    return ERROR;
}

// Function to take a semaphore
// with a timeout in case ...
inline int orcSemTake(ORCSEM_ID sem, int timeout)
{
    return ERROR;
}

/*-------Tasks----------*/
// Function for suspending current process
inline int orcTaskDelay(int delay)
{
    return ERROR;
}

// Function to check if a task is still valid or not
inline int orcTaskIdVerify(ORCTHR_ID Id)
{
    return ERROR;
}

// Function to kill a task (thread)
inline int orcTaskDelete(ORCTHR_ID Id)
{
    return ERROR;
}

// Function to spawn
inline int orcSpawn(ORCTHR_ID *tid, const char *name, int prio, STACKSIZE stackSize, FUNCPTR funcptr, void *arg)
{
    return ERROR;
}

//function to lock the task
inline int orcTaskLock()
{
    return ERROR;
}

//function to unlock the task
inline int orcTaskUnlock()
{
    return ERROR;
}

/*-------Timers----------*/
// Function to mask timer signal interrruption
inline int orcTimerSigMask()
{
    return ERROR;
}

// Function to create a Timer
inline int orcTimerInit(double period)
{
    return ERROR;
}

// Function to arm the Timer period and the associated end-timer routine
inline int orcTimerArm(TIMERHANDLER func, void *arg)
{
    return ERROR;
}

// Function to start effectively the Timer
inline int orcTimerLaunch()
{
    return ERROR;
}

// Function to delete the timer
inline int orcTimerDelete()
{
    return ERROR;
}

/* -------- MISC -------- */
// Function for getting the current path
// TODO : MAXFILENAME is set to 80 in util.h, it should be PATH_MAX+1
inline int orcGetPath(char *path)
{
    return ERROR;
}

// Function to set the current directory
inline int orcSetPath(const char *path)
{
    return ERROR;
}

// Spy for Events: ???
inline int orcSpyEvent(int eventid, char *buffer, int bufsize)
{
    return ERROR;
};

#endif
