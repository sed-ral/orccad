//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/linuxOrcc.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
// ***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime for Linux
//            abstract type, define
//
// **************************** Modifications **********************************
//
// Author         : Soraya Arias
// Date           : 12 April 2000
//
// This file provides an interface between all Orccad system call and
// Red Hat Linux kernel 2.4
//
// BEWARE: timer resolution grain is 10ms in this version !
//
//*****************************************************************************
// (c) Copyright 1996-2011, INRIA, all rights reserved
//*****************************************************************************
#ifndef __linuxorcc_h
#define __linuxorcc_h

/////////////////////////
/// Linux include
/////////////////////////
#include <values.h>
#include <unistd.h>
#include <limits.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <semaphore.h>
#include <pthread.h>
#include <sched.h>

/////////////////////////
/// Define
/////////////////////////
typedef void (*FUNCPTR)  (...);
typedef void *(*SOSO)  (void *);  /**< Special type to be used for casting in pthread_create() */

typedef int STATUS;

const int OK    =  0;
const int ERROR = -1;
const int FALSE =  0;
const int  TRUE =  1;

/**
 * Max number of characters for file name
 * This number is based on _POSIX_PATH_MAX value (see posix1_lim.h and see limits.h the
 * ifdef __USE_POSIX statement)
 *
 * @see <posix1_lim.h>#_POSIX_PATH_MAX
 * @see <limits.h>
 */
const int MAXFILENAME = _POSIX_PATH_MAX + 1;

/*-------Message Queues----------*/
/**
 * Message queue Id structure
 */
typedef struct msgQId {
    int msgId; /**< Message queue id */
    key_t key; /**< Key */
    //  char msgName[256];
} orcmsgQ_t;

/**
 * Message buffer structure used within message queue
 */
typedef struct msgBuf {
    long mtype;     /**< Message type, must be > 0 */
    int orcmsgInfo; /**< Message data */
} orcmsgQBuf_t;

typedef orcmsgQ_t *ORCMSGQ; /**< Message Queue Type */
const int SIZE_QUEUE   =   10;      /**< Size of the Message Queue */
const int SIZE_MES_MAX = 4056;   /**< Maximum size of a message in Message Queue */
const int SIZE_MES_MIN =  128;    /**< Minimum size of a message in Message Queue */
const int       MQPERM = IPC_CREAT | IPC_EXCL | 0666; /**< Message Queue Permission */
const int  MQPRIO_MAX  = (255);        /**< Max Message Queue Priority Level */
const int ORCNORMALPRI = 1;          /**< Message Queue Normal Priority Level */
const int ORCURGENTPRI = MQPRIO_MAX; /**< Message Queue Urgent Priority Level */

/*-------Semaphores----------*/
typedef sem_t *ORCSEM_ID; /**< Semaphore Type */
const int    ORCFULL   =  1;     /**< State of a Full Binary Semaphore */
const int   ORCEMPTY   =  0;     /**< State of a Empty Binary Semaphore */
const int ORCFOREVER   = -1;  /**< State of a ??? Binary Semaphore */

const int WAIT_FOREVER =  0; /**< Define set for interoperability with previous OS */

/*-------Tasks----------*/
typedef pthread_t *ORCTHR_ID;    /**< Posix Thread Type */
typedef size_t STACKSIZE;        /**< Stack size Type */
const int SCHEDPOLICY = SCHED_OTHER;  /**< Scheduling policy (see schedbits.h, sched.h) */

const int PRI_CLK = sched_get_priority_max(SCHEDPOLICY); /**< Clock task priority level (see sched_get_priority_max() in sched.h) */
const int PRI_CNL = PRI_CLK - 1; /**< Canal task priority level */
const int PRI_RT  = PRI_CLK - 2; /**< Robot controller task priority level */
const int PRI_MT  = PRI_RT;      /**< Module controller task priority level */
const int PRI_PRR = PRI_RT;      /**< Procedure controller task priority level */

const int SMALL_STACK  = 5000;           /**< Small task stack size PTHREAD_STACK_MIN not defined */
const int NORMAL_STACK = 5 * SMALL_STACK; /**< Normal task stack size */
const int BIG_STACK    = 15 * SMALL_STACK; /**< Big task stack size */

/*-------Timers----------*/
const int TIME_UNIT = 1000000; /**< Time unit = microsecond */
const int TIME_RES  = 10000;  /**< Linux Timer Resolution = 10 ms */

/**
 * Timer structure definition
 */
typedef struct timer {
    struct timeval interval;
    double period; /**< Periodic delay */
} orcTimer_t;

typedef orcTimer_t *ORCTIMER_ID;  /**< Timer Type */
typedef FUNCPTR TIMERHANDLER;  /**< Timer function handler type */
typedef struct two_args {
    TIMERHANDLER arg1;
    void *arg2;
}  TIMERSTRUCT; /**< Timer function handler argument structure */
const int ORCSIGTIMER  = SIGALRM;      /**< Signal the timer is linked with */

static sigset_t orcSigSet;
static ORCTHR_ID clkAlrmId;

static TIMERSTRUCT orcTimerStruct; /**< global Timer function handler argument */
static ORCTIMER_ID orcTimer;       /**< global Timer object */

/*-------Message Queues----------*/
/**
 * Function to create a message queue
 *
 * @param ORCMSGQ pointer if message queue created, NULL otherwise and errno is set
 * @return OK if successfull, ERROR otherwise
 * @see ORCMSGQ
 * @see <sys/msg.h>#msgget
 * @see <sys/msg.h>#msgctl
 *
 */
inline int orcMsgQCreate(ORCMSGQ *orcMsgQ)
{
    //  ORCMSGQ orcMsgQ;
    key_t key = IPC_PRIVATE;
    int err;

    err = msgget(key, MQPERM);
    if ((err == ERROR) && (errno == EEXIST)) {
#ifdef _DEBUG
        fprintf(stderr, "orcMsgQCreate: msgQ already exists \n");
#endif
        err = msgget(key, 0);
        msgctl(err, IPC_RMID, 0);
        err = msgget(key, IPC_CREAT | IPC_EXCL | MQPERM);
    }
    else if ((err == ERROR) && (errno != EEXIST)) {
#ifdef _DEBUG
        fprintf(stderr, "orcMsgQCreate: Error = %d \n", errno);
#endif
        return ERROR;
    }

    *orcMsgQ = (ORCMSGQ) malloc(sizeof(orcmsgQ_t));
    (*orcMsgQ)->msgId = err;
    (*orcMsgQ)->key = key;

    return err;
}

/**
 * Function to get the number of messages pending in a message queue
 *
 * @param queue pointer to the message queue
 * @return long number of messages if no error occurs otherwise ERROR and errno is set
 * @see <sys/msg.h>#msgctl
 */
inline long orcMsgQNumMsgs(ORCMSGQ queue)
{
    struct msqid_ds tmpBuf;

    if (msgctl(queue->msgId, IPC_STAT, &tmpBuf) == ERROR) {
#ifdef _DEBUG
        fprintf(stderr, "orcMsgQNumMsgs: Error = %d \n", errno);
#endif
        return ERROR;
    }
#ifdef _DEBUG
    fprintf(stderr, "orcMsgQNumMsgs: nb of msgs = %d \n", (unsigned int)tmpBuf.msg_qnum);
#endif
    return (long)tmpBuf.msg_qnum;
}

/**
 * Function to send a message on a message queue
 * Sending the message is done according to a priority level such as :
 *
 * @param queue pointer to the message queue
 * @param msg message to be sent
 * @param prio message priority level
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <sys/msg.h>#msgsnd
 */
inline int orcMsgQSend(ORCMSGQ queue, int msg, int prio)
{
    orcmsgQBuf_t tmpBuf;
    int nMsgSize;

    nMsgSize = sizeof(orcmsgQBuf_t) - sizeof(long);
#ifdef _DEBUG
    fprintf(stderr, "orcMsgQSend: msgId = %d nMsgSize = %d \n", queue->msgId, nMsgSize);
#endif
    tmpBuf.orcmsgInfo = msg;
    tmpBuf.mtype = MQPRIO_MAX - prio;

    /* 0 : If message queue is full -> message is not written to the queue */
    if (msgsnd(queue->msgId, &tmpBuf, nMsgSize, 0) == ERROR) {
#ifdef _DEBUG
        fprintf(stderr, "orcMsgQSend: Error = %d \n", errno);
#endif
        return ERROR;
    }
    return OK;
}

/**
 * Function to receive messages on a message queue
 *
 * @param queue pointer to the message queue
 * @param msg pointer to the received message
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <sys/msg.h>#msgrcv
 */
inline int orcMsgQReceive(ORCMSGQ queue, int *msg)
{
    orcmsgQBuf_t tmpBuf;
    int nMsgSize;

    nMsgSize = sizeof(orcmsgQBuf_t) - sizeof(long);

    /* 0 : the calling process blocks until a message arrives in queue */
    /* mtype = -MQPRIO_MAX: type of message to retrieve from the queue
               if mtype < 0 : first message on
                the queue with the lowest type less than  or  equal
                to the absolute value of msgtyp will be read
    */
    if (msgrcv(queue->msgId, &tmpBuf, nMsgSize, -MQPRIO_MAX, 0) == ERROR) {
#ifdef _DEBUG
        fprintf(stderr, "orcMsgQReceive: Error = %d \n", errno);
#endif
        return ERROR;
    }
#ifdef _DEBUG
    fprintf(stderr, "orcMsgQReceive: msg received = %d \n", tmpBuf.orcmsgInfo);
#endif
    *msg = tmpBuf.orcmsgInfo;

    return OK;
}

/**
 * Function to close and erase a message queue
 *
 * @param queue pointer to the message queue
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <sys/msg.h>#msgctl
 */
inline int orcMsgQClose(ORCMSGQ queue)
{

    if (queue != NULL) {
        if (msgctl(queue->msgId, IPC_RMID, 0) == ERROR) {
#ifdef _DEBUG
            fprintf(stderr, "orcMsgQClose: Error = %d \n", errno);
#endif
            return ERROR;
        }
        free(queue);
        queue = NULL;
    }
    return OK;
}

/*-------Semaphores----------*/
/**
 * Function to create a semaphore
 *
 * @param initState semaphore initial creation state, eg ORCFULL, ORCEMPTY
 * @return ORCSEM_ID semaphore pointer created if successfull, NULL otherwise
 * @see ORCFULL
 * @see ORCEMPTY
 * @see <semaphore.h>#sem_init
 */
inline ORCSEM_ID orcSemCreate(int initState)
{
    ORCSEM_ID newSem;
    newSem = new sem_t;
    int status;

    if (initState == ORCEMPTY) {
        status = sem_init(newSem, 0, 0);    // 1: shared between processes, 0 init value
    }
    else {
        status = sem_init(newSem, 0, 1);
    }

    if (status == OK) {
        return newSem;
    }
    else {
        return NULL;
    }
}

/**
 * Function to delete a given semaphore
 *
 * @param sem semaphore id to be deleted
 * @return sem_destroy() returned value
 * @see <semaphore.h>#sem_destroy
 */
inline int orcSemDelete(ORCSEM_ID sem)
{
    return (sem_destroy(sem));
}

/**
 * Function to give a semaphore
 *
 * @param sem semaphore id
 * @return sem_post) returned value
 * @see <semaphore.h>#sem_post
 */
inline int orcSemGive(ORCSEM_ID sem)
{
    return (sem_post(sem));
}

/**
 * Function to take a semaphore
 * TODO : no timeout in Posix semaphores
 *
 * @param sem semaphore id
 * @param timeout value
 * @return sem_wait() returned value
 * @see <semaphore.h>#sem_wait
 */
inline int orcSemTake(ORCSEM_ID sem, int timeout)
{

    /* RPG 24 Aout 06
    int dummy = timeout; // set to avoid compilation warnings
    */

    return (sem_wait(sem));
}

/*-------Tasks----------*/
/**
 * Function for suspending current process
 * TODO : no taskDelay in Posix (sleep uses seconds, delay ?, nanosleep ?)
 *
 * @param delay
 * @return OK
 */
inline int orcTaskDelay(int delay)
{
    /* RPG 24 Aout 06
    int dummy = delay; // set to avoid compilation warnings
    */

    return OK;
}

/**
 * Function to check if a task is still valid or not
 *
 * @param Id task identifier
 * @return OK if task is valid, ERROR otherwise
 * @see ORCTHR_ID
 * @see pthread_kill
 */
inline int orcTaskIdVerify(ORCTHR_ID Id)
{
    if (Id != NULL) {
        if (pthread_kill(*Id, 0) != OK) {
            return ERROR;
        }
        else {
            return OK;
        }
    }
    return ERROR;
}

/**
 * Function to kill a task (pthread)
 *
 * @param Id task identifier
 * @return ERROR or pthread_cancel returned value
 * @see ORCTHR_ID
 * @see <pthread.h>#pthread_kill
 * @see <pthread.h>#pthread_cancel
 */
inline int orcTaskDelete(ORCTHR_ID Id)
{
    // Check if thread is ok
    if ( Id != NULL) {
        if (pthread_kill(*Id, 0) == OK) {
            // Send SIGTERM signal <- BAD!!
            // return (pthread_kill(*Id,SIGTERM)) ;
            return(pthread_cancel(*Id));
        }
    }
    return ERROR;
}

/**
 * Function to spawn a task
 *
 * @param tid task identifier
 * @param name name descripting the task
 * @param prio task priority level
 * @param stackSize stack size to be allocated for the task
 * @param funcptr routine to be spawn by the task
 * @param arg argument needed for the routine to be spawn
 * @return pthread_create returned value
 * @see ORCTHR_ID
 * @see STACKSIZE
 * @see FUNCPTR
 * @see <pthread.h>#pthread_create
 */
inline int orcSpawn(ORCTHR_ID *tid, const char *name, int prio, STACKSIZE stackSize, FUNCPTR funcptr, void *arg)
{

    int status;

    *tid = new pthread_t;

    pthread_attr_t attributes;
    sched_param schedattributes;

    // To avoid compilation warnings
    char *dummy = (char *)malloc((strlen(name) * sizeof(char)) + 1);
    strcpy(dummy, name);

    // Allocation and initialization of the attribute structure
    pthread_attr_init(&attributes);
    // Set stack size : BEWARE in Solaris is better to set stacksize to default value
    // i.e. NULL instead of a user defined stacksize value -> see man pthread_create
#ifdef _DEBUG
    fprintf(stderr, "StackSize = %d \n", stackSize);
#endif
    //  pthread_attr_setstacksize(&attributes, stackSize);
    // Set thread priority
    schedattributes.sched_priority = prio;
    pthread_attr_setschedparam(&attributes, &schedattributes);

    // Thread creation
    status =  pthread_create(*tid, &attributes, (SOSO)funcptr, arg);
#ifdef _DEBUG
    fprintf(stderr, "pthread_create %s error = %d \n", name, status);
#endif

    return status;
}

/**
 * Function to lock a task
 * This function does nothing : defined for interoperability between OS needs
 *
 * @param
 * @return OK
 */
inline int orcTaskLock()
{
    return OK;
}

/**
 * Function to unlock a task
 * This function does nothing : defined for interoperability between OS needs
 *
 * @param
 * @return OK
 */
inline int orcTaskUnlock()
{
    return OK;
}

/*-------Timers----------*/
/**
 * Timer handler function
 *
 * @param int
 * @return
 */
void orcTimer_handler(TIMERSTRUCT *arg);

/**
 * Function to mask timer signal interruption
 *
 * @param
 * @return OK if successful, ERROR otherwise and errno is set
 * @see ORCSIGTIMER
 * @see <pthread.h>#pthread_sigmask
 * @see <signal.h>#sigaddset
 */
inline int orcTimerSigMask()
{
    sigemptyset(&orcSigSet);
    sigaddset(&orcSigSet, ORCSIGTIMER);
    if (pthread_sigmask(SIG_BLOCK, &orcSigSet, NULL) != OK) {
#ifdef _DEBUG
        fprintf(stderr, "orcTimerSigMask: pthread_sigmask Failed %d \n", errno);
#endif
        return ERROR;
    }
    return OK;
}

/**
 * Function to set the timer settings
 * This function sets in particular the period for the timer
 *
 * @param period value to be set for timer
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see TIME_UNIT
 * @see TIME_RES
 * @see <sys/timer.h>#setitimer
 */
inline int orcTimerSetTime(double period)
{
    struct itimerval new_setting, old_setting;
    struct timeval time_delay ;
    double sec, usec;

    //Convert the period in microsecond
    double delay =  (double)(TIME_UNIT * period);
    // Set the timespec struct -> delay in second, delay in microsecond
    // Get the part in second
    sec  = floor(delay / (double)TIME_UNIT);
    // Get the remainder part in microsecond
    usec = rint(fmod(delay, (double)TIME_UNIT));

    time_delay.tv_sec = (int)(sec);
    // Handle period < TIME_RES
    if (delay < (double)TIME_RES) {
        time_delay.tv_usec = TIME_RES ;
    }
    else {
        time_delay.tv_usec = (int)(floor(usec / (double)TIME_RES)) * TIME_RES;
    }

    // Setting the delay for the alarm
    new_setting.it_value.tv_sec     = time_delay.tv_sec;
    new_setting.it_value.tv_usec    = time_delay.tv_usec;
    new_setting.it_interval.tv_sec  = time_delay.tv_sec;
    new_setting.it_interval.tv_usec = time_delay.tv_usec;

    orcTimer->interval = new_setting.it_value;

#ifdef _DEBUG
    fprintf(stderr, "orcTimerSetTime: new_setting.it_value.tv_sec = %ld, new_setting.it_value.tv_usec = %ld \n", (long int)new_setting.it_value.tv_sec, (long int)new_setting.it_value.tv_usec);
#endif
    // Arm the timer using the new_setting delay
    // in Linux: timer resolution = 10ms !
    // ITIMER_REAL decrements  in  real  time,  and   delivers
    //             SIGALRM upon expiration
    if (setitimer(ITIMER_REAL, &new_setting, &old_setting) == ERROR) {
#ifdef _DEBUG
        fprintf(stderr, "orcTimerSetTime: Error setitimer = %d \n", errno);
#endif
        return ERROR;
    }
    return OK;
}

/**
 * Function to create a periodic timer
 * This function sets a periodic timer,
 * the corresponding signal  and the timer handler
 * and the orcTimer structure is affected
 *
 * @param period value to be set for periodic timer
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see ORCTIMER_ID
 * @see orcTimerSetTime
 * @see orcTimer
 */
inline int orcTimerInit(double period)
{
    orcTimer = (ORCTIMER_ID) malloc(sizeof(orcTimer_t));
    orcTimer->period = period;

    return (orcTimerSetTime(period));
}

/**
 * Function to delete/erase the timer
 * This function disabled the periodic timer, the associated signal
 * unmask this signal within the task and erase the orcTimer structure
 *
 * @param
 * @return OK if successfull, ERROR otherwise
 * @see orcTimer
 * @see <signal.h>#sigemptyset
 * @see <pthread.h>#pthread_sigmask
 */
inline int orcTimerDelete()
{
    int err1 = OK;
    int err2 = OK;

    if (orcTimer != NULL) {
        // Cancel the timer
        err1 = setitimer(ITIMER_REAL, NULL, NULL);
        // Set a new IT mask
        sigemptyset(&orcSigSet);
        pthread_sigmask(SIG_SETMASK, &orcSigSet, NULL);

        free(orcTimer);
        orcTimer = NULL;
    }
    return ((err1) ? err1 : err2);
}

/**
 * Function to set the periodic timer handler
 *
 * @param func timer handler function pointer
 * @param arg argument for the timer handler function
 * @return OK if successfull ERROR otherwise
 */
inline int orcTimerArm(TIMERHANDLER func, void *arg)
{
    orcTimerStruct.arg1 = func;
    orcTimerStruct.arg2 = (int *)arg;

    clkAlrmId = (ORCTHR_ID) malloc(sizeof(pthread_t));
    if (orcSpawn(&clkAlrmId, (char *)"orcWaitAlarm", PRI_CLK, NORMAL_STACK, (FUNCPTR)orcTimer_handler, (void *)&orcTimerStruct) == ERROR) {
        return ERROR;
    }
    return OK;
}

/**
 * Function to launch the periodic timer
 * Function does nothing but defined for interoperability need between OS
 *
 * @param
 * @return OK
 */
inline int orcTimerLaunch()
{
    return OK;
}

/* -------- MISC -------- */
/**
 * Function to check a given path
 * TODO : MAXFILENAME is set to 80 in util.h, it should be PATH_MAX+1
 *
 * @param path pathname to retrieve
 * @return OK if successfull, ERROR otherwise
 * @see MAXFILENAME
 */
inline int orcGetPath(char *path)
{
    if (getcwd(path, MAXFILENAME) != OK) {
        return ERROR;
    }
    return OK;
}

/**
 * Function to set a directory path
 *
 * @param path name pf the directory path
 * @return chdir() returned value
 * @see chdir
 */
inline int orcSetPath(const char *path)
{
    return chdir(path);
}

/**
 * Function to spy events
 * Function does nothing but defined for interoperability need between OS
 *
 * @param eventid event identifier to spy
 * @param buffer
 * @param bufsize
 * @return OK
 */
inline int orcSpyEvent(int eventid, char *buffer, int bufsize)
{
    return OK;
};
#endif
