//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/datadbg.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime
//            debug which can display data
//
//
// Class Definition:
//  DataElement: data element passing through port
//  DataTab:     table of data elements
//  DataDebug: display datas
//
//**************************** Modifications **********************************
//
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************

#ifndef __datadbg_h
#define __datadbg_h

#include "utils.h"
#include <math.h>

class DataElement
{
    //  friend ostream& operator<<(ostream&, DataElement&);

protected:
    int    **Ielement;
    double **Delement;
    float  **Felement; // #NT FLOAT

    int    *VIelement;
    double *VDelement;
    float  *VFelement;

    TYPE_DATA Type;
    double Time;

public:
    int    Row;
    int    Col;
    void DataPrint(FILE *, DataElement &);
    // Constructors
    DataElement(TYPE_DATA, int, int);
    //Destructor
    ~DataElement();

    int  getRow() {
        return Row;
    };
    int  getCol() {
        return Col;
    };
    TYPE_DATA  getType() {
        return Type;
    };

    void Put(int *, double);
    void Put(int, double);
    void Put(float *, double);
    void Put(float, double);
    void Put(double *, double);
    void Put(double, double);

};

class DataDebug;

class DataTab
{
    friend class DataDebug;

protected:
    DataElement **Tab;
    //  DataElement *Tab;
    char VarName[MAXVARNAME];
    int IndexTab;
    int Buffer;
    int IndexMax;
    int IndexStep;
    int CurrentStep;
    BUFFER_MODE BufferMode;
    int DisplayMode;

    DataTab *Next;
    DataTab *Prev;

public:
    // Constructors
    DataTab(char *name, TYPE_DATA type, int row, int col,
            int imax, int step,
            int dmode, BUFFER_MODE bmode);
    //Destructor
    ~DataTab();

    void Put(int *, double);
    void Put(double *, double);
    void Put(float *, double);
    void Put(float, double);
    void Put(int, double);
    void Put(double, double);

    void Open();
    void Close();

};

class DataDebug
{
protected:
    DataTab *List;
    int Number;

public:
    DataDebug() {
        List = (DataTab *) NULL;
        Number = 0;
    }
    ~DataDebug();

    int Insert(char *name, TYPE_DATA type, int row, int col,
               int imax, int step,
               int dmode, BUFFER_MODE bmode);

    DataTab *GetDataTab(int);
    void Put(int, int *, double);
    void Put(int, double *, double);
    void Put(int, float *, double);
    void Put(int, float, double);
    void Put(int, int, double);
    void Put(int, double, double);
    void Open(int);
    void Close(int);
    void Close();
};
#endif
