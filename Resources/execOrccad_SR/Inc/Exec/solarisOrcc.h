// *******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/solarisOrcc.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
// ***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime for Solaris
//            abstract type, define
//
// **************************** Modifications **********************************
//
// Author         : Nicolas Turro and Soraya Arias
// Date           : 3 October 1998
//
// This file provides an interface between all Orccad system call and Posix Solaris2.6
//
// *****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
// *****************************************************************************
#ifndef __solarisorcc_h
#define __solarisorcc_h

/**
 * Define necessary unless compilation troubles
 * with Solaris 2.6
 * For Solaris 2.5
 * #define _POSIX_C_SOURCE 199309
 */
#define _POSIX_C_SOURCE 199506

/////////////////////////
/// Solaris include
/////////////////////////
#include <sys/types.h>
#include <unistd.h>
#include <limits.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <signal.h>
#include <mqueue.h>
#include <semaphore.h>
#include <pthread.h>
#include <sched.h>

/////////////////////////
/// Define
/////////////////////////

typedef void (*FUNCPTR)  (...);
extern "C" typedef void *(*SOSO) (void *);  /**< Special type to be used for casting in pthread_create() */


#define OK 0
#define ERROR -1
#define STATUS int

#define FALSE 0
#define TRUE 1

/**
 * Max number of characters for file name
 * This number is based on _POSIX_PATH_MAX value (see posix1_lim.h and see limits.h the
 * ifdef __USE_POSIX statement)
 *
 * @see <posix1_lim.h>#_POSIX_PATH_MAX
 * @see <limits.h>
 */
#define MAXFILENAME _POSIX_PATH_MAX +1

/*-------Message Queues----------*/
#define ORCMSGQ mqd_t  /**< Posix Message Queue Type */
#define SIZE_QUEUE  10 /**< Size of the Message Queue */
#define SIZE_MES       /**< Size of a message in Message Queue */
//#define MQNAME "/orcMsgQ" /**< Message Queue Name */
#define MQPRIO_MIN 1   /**< Min Message Queue Priority Level */
#define MQPRIO_MAX _POSIX_MQ_PRIO_MAX /**< Max Message Queue Priority Level */
#define MQACCESS_FIFO 0 /**< Message Queue Receive Policy */

#define ORCNORMALPRI 0  /**< Message Queue Normal Priority Level */
#define ORCURGENTPRI 1  /**< Message Queue Urgent Priority Level */

/*-------Semaphores----------*/
#define ORCSEM_ID sem_t * /**< Semaphore Type */
#define ORCFULL  1        /**< State of a Full Binary Semaphore */
#define ORCEMPTY 0        /**< State of a Empty Binary Semaphore */
#define ORCFOREVER -1     /**< State of a ??? Binary Semaphore */

#define WAIT_FOREVER 0     /**< Define set for interoperability needs */

/*-------Tasks----------*/
#define ORCTHR_ID pthread_t *   /**< Posix Thread Type */
#define STACKSIZE size_t        /**< Stack size Type */

#define SCHEDPOLICY SCHED_OTHER /**< Scheduling policy (see schedbits.h, sched.h )*/
#define PRI_CLK   sched_get_priority_max(SCHEDPOLICY) /**< Clock task priority level (see sched_get_priority_max() in sched.h) */
#define PRI_CNL   PRI_CLK - 1 /**< Canal task priority level */
#define PRI_RT    PRI_CLK - 2 /**< Robot controller task priority level */
#define PRI_MT    PRI_RT      /**< Module controller task priority level */
#define PRI_PRR   PRI_RT      /**< Procedure controller task priority level */

#define SMALL_STACK  PTHREAD_STACK_MIN /**< Small task stack size */
#define NORMAL_STACK 5*SMALL_STACK     /**< Normal task stack size */
#define BIG_STACK    15*SMALL_STACK    /**< Big task stack size */

/*-------Timers----------*/
#define TIME_UNIT 1000000000  /**< Time unit = nanosecond */

#define ORCTIMER_ID timer_t   /**< Timer Type */
#define ORCSIGTIMER SIGRTMIN  /**< Signal the timer is linked with */
typedef FUNCPTR TIMERHANDLER; /**< Timer function handler type */
typedef struct two_args {
    TIMERHANDLER arg1;
    void *arg2;
}  TIMERSTRUCT; /**< Timer function handler argument structure */

extern "C" double rint(double);

extern TIMERSTRUCT staticTS;       /**< Global Timer function handler argument */
extern ORCTIMER_ID created_timer;  /**< Global Timer object */
extern ORCTHR_ID tArmId;           /**< Global Thread that handles timer */

/*-------Message Queues----------*/
extern char orcMsgQName[256];       /**< Global name for the message queue */
extern char orcMsgQRec[SIZE_QUEUE]; /**< Global name for the received message message queue */

/**
 * Function to create a message queue
 *
 * @param ORCMSGQ pointer if posix message queue created, NULL otherwise and errno is set
 * @return OK if successfull, ERROR otherwise
 * @see ORCMSGQ
 * @see <mqueue.h>#mq_open
 * @see <mqueue.h>#mq_unlink
 */
inline int orcMsgQCreate(ORCMSGQ *orcMsgQ)
{
    struct mq_attr attribute;
    ORCMSGQ err ;

    char s[256];
    int size_mes;
    sprintf(s, "%d", INT_MAX);
    size_mes = strlen(s);

    attribute.mq_maxmsg = SIZE_QUEUE;
    attribute.mq_msgsize = size_mes;
    attribute.mq_flags = 0; // 0 = blocking open on the queue

    // Create the MQNAME identifier MQNAME =  getpid()
    sprintf(orcMsgQName, "/%ld", getpid());

    err = mq_open(orcMsgQName, O_RDWR | O_CREAT | O_EXCL, 0x1B0, &attribute) ;
    if (err == (ORCMSGQ)ERROR) {
        if (errno == EEXIST) {
#ifdef _DEBUG
            fprintf(stderr, "orcMsgQCreate: msq already exists \n");
#endif
            mq_unlink(orcMsgQName);
            err = mq_open(orcMsgQName, O_RDWR | O_CREAT, 0x1B0, &attribute);
            if (err == (ORCMSGQ)ERROR) {
                *orcMsgQ = (mqd_t) - 1;
                return ERROR;
            }
        }
        else {
            *orcMsgQ = (mqd_t) - 1;
            return ERROR;
        }
    }
    *orcMsgQ = err;
    return OK;
}

/**
 * Function to get the number of messages pending in a posix message queue
 *
 * @param posix message queue
 * @return long number of messages
 * @see <mqueue.h>#mq_getattr
 * @see <mqueue.h>#mq_attr
 */
inline long orcMsgQNumMsgs(ORCMSGQ queue)
{
    struct mq_attr mqstat;
    mq_getattr(queue, &mqstat);
    return mqstat.mq_curmsgs;
}

/**
 * Function to send a message on a posix message queue
 * Sending the message is done according to a priority level such as :
 *
 * @param posix message queue
 * @param msg message to be sent
 * @param prio message priority level
 * @return mq_send returned value
 * @see <mqueue.h>#mq_send
 */
inline int orcMsgQSend(ORCMSGQ queue, int msg, int prio)
{

#ifdef _DEBUG
    fprintf(stderr, "orcMsgQSend: msg = %d \n", msg);
#endif
    sprintf(orcMsgQRec, "%d", msg);

    if (prio == ORCNORMALPRI) {
        return mq_send(queue, orcMsgQRec, strlen(orcMsgQRec) + 1, MQPRIO_MIN);
    }
    else {
        return mq_send(queue, orcMsgQRec, strlen(orcMsgQRec) + 1, MQPRIO_MAX);
    }
}

/**
 * Function to receive messages on a posix message queue
 *
 * @param posix message queue
 * @param msg pointer to the received message
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <mqueue.h>#mq_receive
 */
inline int orcMsgQReceive(ORCMSGQ queue, int *msg)
{

    if (mq_receive(queue, orcMsgQRec, SIZE_QUEUE, MQACCESS_FIFO) == ERROR) {
#ifdef _DEBUG
        fprintf(stderr, "orcMsgQReceive: mq_receive Error = %d \n", errno);
#endif
        return ERROR;
    }
    *msg = atoi(orcMsgQRec);

#ifdef _DEBUG
    fprintf(stderr, "orcMsgQReceive: msg = %d \n", *msg);
#endif

    return OK;
}

/**
 * Function to close and erase a posix message queue
 *
 * @param queue pointer to the message queue
 * @return mq_close returned status or mq_unlink returned status
 * @see  <mqueue.h>#mq_close
 * @see  <mqueue.h>#mq_unlink
 */
inline int orcMsgQClose(ORCMSGQ queue)
{
    int err1, err2;

    err1 = mq_close(queue);
    err2 = mq_unlink(orcMsgQName);

    return(err1 ? err1 : err2);
}

/*-------Semaphores----------*/
/**
 * Function to create a semaphore
 *
 * @param initState semaphore initial creation state, eg ORCFULL, ORCEMPTY
 * @return ORCSEM_ID semaphore pointer created if successfull, NULL otherwise
 * @see ORCFULL
 * @see ORCEMPTY
 * @see <semaphore.h>#sem_init
 */
inline ORCSEM_ID orcSemCreate(int initState)
{
    ORCSEM_ID newSem;
    newSem = new sem_t;
    int status;

    if (initState == ORCEMPTY) {
        status = sem_init(newSem, 0, 0);    // 1: shared between processes, 0 init value
    }
    else {
        status = sem_init(newSem, 0, 1);
    }

    if (status == OK) {
        return newSem;
    }
    else {
        return NULL;
    }
}

/**
 * Function to delete a given semaphore
 *
 * @param sem semaphore to be deleted
 * @return sem_destroy() returned value
 * @see <semaphore.h>#sem_destroy
 */
inline int orcSemDelete(ORCSEM_ID sem)
{
    return (sem_destroy(sem));
}

/**
 * Function to give a semaphore
 *
 * @param sem semaphore id
 * @return sem_post) returned value
 * @see <semaphore.h>#sem_post
 */
inline int orcSemGive(ORCSEM_ID sem)
{
    return (sem_post(sem));
}

/**
 * Function to take a semaphore
 * TODO : no timeout in Posix semaphores
 *
 * @param sem semaphore
 * @param timeout value
 * @return sem_wait() returned value
 * @see <semaphore.h>#sem_wait
 */
inline int orcSemTake(ORCSEM_ID sem, int timeout)
{
    int dummy = timeout; // set to avoid compilation warnings

    return (sem_wait(sem));
}

/*-------Tasks----------*/
/**
 * Function for suspending current process
 * TODO : no taskDelay in Posix (sleep uses seconds, delay ?, nanosleep ?)
 *
 * @param delay
 * @return OK
 */
inline int orcTaskDelay(int delay)
{
    int dummy = delay; // set to avoid compilation warnings

    return OK;
}

/**
 * Function to check if a task is still valid or not
 *
 * @param Id task identifier
 * @return OK if task is valid, ERROR otherwise
 * @see ORCTHR_ID
 * @see pthread_kill
 */
inline int orcTaskIdVerify(ORCTHR_ID Id)
{
    if (Id != NULL) {
        if (pthread_kill(*Id, 0) != OK) {
            return ERROR;
        }
        else {
            return OK;
        }
    }
    return ERROR;
}

/**
 * Function to kill a task (pthread)
 *
 * @param Id task identifier
 * @return ERROR or pthread_cancel returned value
 * @see ORCTHR_ID
 * @see <pthread.h>#pthread_kill
 * @see <pthread.h>#pthread_cancel
 */
inline int orcTaskDelete(ORCTHR_ID Id)
{
    // SOSO#tskdel
#ifdef _DEBUG
    fprintf(stderr, "orcTaskDelete: called tsk id = %x \n", Id);
#endif
    // SOSO#tskdel
    // Check if thread is ok
    if ( Id != NULL) {
        if (pthread_kill(*Id, 0) == OK) {
            // Send SIGTERM signal <- BAD !!
            //return (pthread_kill(*Id,SIGTERM)) ;
            // SOSO#tskdel 30-06-00 vvvv
            return(pthread_cancel(*Id));
            //      return(pthread_join(*Id, NULL));
            // SOSO#tskdel 30-06-00 ^^^^
        }
    }
    return ERROR;
}

/**
 * Function to spawn a task
 *
 * @param tid task identifier
 * @param name name descripting the task
 * @param prio task priority level
 * @param stackSize stack size to be allocated for the task
 * @param funcptr routine to be spawn by the task
 * @param arg argument needed for the routine to be spawn
 * @return pthread_create returned value
 * @see ORCTHR_ID
 * @see STACKSIZE
 * @see FUNCPTR
 * @see <pthread.h>#pthread_create
 */
inline int orcSpawn(ORCTHR_ID *tid, const char *name, int prio, STACKSIZE stackSize, FUNCPTR funcptr, void *arg)
{

    int status;

    *tid = new pthread_t;

    pthread_attr_t attributes;
    sched_param schedattributes;

    // To avoid compilation warnings
    char *dummy = (char *)malloc((strlen(name) * sizeof(char)) + 1);
    strcpy(dummy, name);

    // Allocation and initialization of the attribute structure
    pthread_attr_init(&attributes);
    // Set stack size : BEWARE in SOlaris is better to set stacksize to default value
    // i.e. NULL instead of a user defined stacksize value -> see man pthread_create
#ifdef _DEBUG
    fprintf(stderr, "PThread StackSize = %d \n", stackSize);
#endif
    //  pthread_attr_setstacksize(&attributes, stackSize);
    // Set thread priority
    schedattributes.sched_priority = prio;
    pthread_attr_setschedparam(&attributes, &schedattributes);

    // Thread creation
    status =  pthread_create(*tid, &attributes, (SOSO)funcptr, arg);
#ifdef _DEBUG
    fprintf(stderr, "pthread_create %s error = %d \n", name, status);
#endif

    return status;
}

/**
 * Function to lock a task
 * This function does nothing : defined for interoperability between OS needs
 *
 * @param
 * @return OK
 */
inline int orcTaskLock()
{
    return OK;
}

/**
 * Function to unlock a task
 * This function does nothing : defined for interoperability between OS needs
 *
 * @param
 * @return OK
 */
inline int orcTaskUnlock()
{
    return OK;
}

/*-------Timers----------*/
/**
 * Function to mask timer delivery signal
 *
 * @param
 * @return OK if successful, ERROR otherwise and errno is set
 * @see ORCSIGTIMER
 * @see <pthread.h>#pthread_sigmask
 * @see <signal.h>#sigaddset
 */
inline int orcTimerSigMask()
{
    sigset_t allsig;

    sigemptyset(&allsig);
    sigaddset(&allsig, ORCSIGTIMER);
    if (pthread_sigmask(SIG_BLOCK, &allsig, NULL) != OK) {
#ifdef _DEBUG
        fprintf(stderr, "orcTimerSigMask: pthread_sigmask Failed %d \n", errno);
#endif
        return ERROR;
    }

    return OK;
}

/**
 * Function to set the timer settings
 * This function sets in particular the period for the timer
 *
 * @param period value to be set for periodic timer
 * @return timer_settime returned value
 * @see TIME_UNIT
 * @see TIME_RES
 * @see <time.h>#timer_settime
 */
inline int orcTimerSetTime(double period)
{
    struct itimerspec new_setting;
    struct timespec time_delay ;

    //Convert the period in nanosecond *
    double delay =  (double) TIME_UNIT * period ;
    // Set the timespec struct -> delay in second, delay in nanosecond
    // get the part in second
    time_delay.tv_sec =  (int)(floor(delay / (double)TIME_UNIT));
    // get the remainder part in nanosecond
    time_delay.tv_nsec = (int)(rint(fmod(delay, (double)TIME_UNIT)));

    // Setting the delay for the alarm
    new_setting.it_value.tv_sec = time_delay.tv_sec ;
    new_setting.it_value.tv_nsec = time_delay.tv_nsec ;
    new_setting.it_interval.tv_sec =  time_delay.tv_sec ;
    new_setting.it_interval.tv_nsec = time_delay.tv_nsec ;

    // Arm the timer using the new_setting delay
    return ((timer_settime(created_timer, 0, &new_setting, NULL)) ? ERROR : OK);
}

/**
 * Function to create a periodic timer
 * This function creates a periodic timer, sets the periodic delay
 * and sets the timer delivery signal
 * The global object created_timer is affected
 *
 * @param period value to be set for periodic timer
 * @return orcTimerSetTime returned value
 * @see created_timer
 * @see ORCSIGTIMER
 * @see orcTimerSigMask
 * @see orcTimerSetTime
 * @see <time.h>#timer_create
 */
inline int orcTimerInit(double period)
{
    struct sigevent timer_event ;

    if (orcTimerSigMask() == ERROR) {
        return ERROR;
    }

    // Setting the sigevent structure
    timer_event.sigev_notify = SIGEV_SIGNAL ;
    timer_event.sigev_signo = ORCSIGTIMER ;
    timer_event.sigev_value.sival_int = 0;

    // Creation of a Timer based upon the system clock
    // &timer_event -> SIGRTMIN is the signal delivery for the timer
    // &timer_event = NULL -> SIGALRM is the signal delivery for the timer
    if (timer_create(CLOCK_REALTIME, &timer_event, &created_timer)) {
        return ERROR;
    }

    return (orcTimerSetTime(period));
}

/**
 * Function to delete/erase the timer
 * This function delete the periodic timer
 * unmask the timer delivery signal within the task and
 * cancel the timer thread
 *
 * @param
 * @return OK if successfull, ERROR otherwise
 * @see created_timer
 * @see tArmId
 * @see <time.h>#timer_delete
 * @see <pthread.h>#pthread_cancel
 * @see <pthread.h>#pthread_sigmask
 */
//
inline int orcTimerDelete()
{
    int err1 = OK;
    int err2 = OK;
    sigset_t allsig;

    err1 = timer_delete(created_timer);

    if (tArmId != NULL) {
        if ((err2 = pthread_cancel(*tArmId)) != OK) {
#ifdef _DEBUG
            fprintf(stderr, "orcTimerDelete: Error TaskDelete armId errno = %d \n", err2);
#endif
        }
    }
    // Set a new IT mask
    sigemptyset(&allsig);
    pthread_sigmask(SIG_SETMASK, &allsig, NULL);

    return ((err1) ? err1 : err2);
}

/**
 * Timer Handler function called periodically when timer period ends
 *
 * @param ap void argument
 * @return
 * @see orcTimerArm
 */
void timerArm(void *ap);

/**
 * Function to spawn the thread that handles the periodic timer
 *
 * @param func timer handler function pointer
 * @param arg argument for the timer handler function
 * @return orcSpawn returned value
 * @see orcSpawn
 * @see tArmId
 */
inline int orcTimerArm(TIMERHANDLER func, void *arg)
{
    staticTS.arg1 = func;
    staticTS.arg2 = arg;

    return(orcSpawn(&tArmId, "TIMERTHREAD", PRI_CLK, NORMAL_STACK, (FUNCPTR) timerArm, (void *)0));
}

/**
 * Function to launch the periodic timer
 * This function does nothing : defined for interoperability between OS needs
 *
 * @param
 * @return OK
 */
inline int orcTimerLaunch()
{
    return OK;
}

/* -------- MISC -------- */
/**
 * Function to check a given path
 * TODO : MAXFILENAME is set to 80 in util.h, it should be PATH_MAX+1
 *
 * @param path pathname to retrieve
 * @return OK if successfull, ERROR otherwise
 * @see MAXFILENAME
 */
inline int orcGetPath(char *path)
{
    if (getcwd(path, MAXFILENAME) != OK) {
        return ERROR;
    }
    return OK;
}

/**
 * Function to set a directory path
 *
 * @param path name pf the directory path
 * @return chdir() returned value
 * @see chdir
 */
inline int orcSetPath(const char *path)
{
    return chdir(path);
}

/**
 * Function to spy events
 * Function does nothing but defined for interoperability need between OS
 *
 * @param eventid event identifier to spy
 * @param buffer
 * @param bufsize
 * @return OK
 */
inline int orcSpyEvent(int eventid, char *buffer, int bufsize)
{
    return OK;
};

#endif
