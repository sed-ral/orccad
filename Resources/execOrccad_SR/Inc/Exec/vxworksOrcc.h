/*******************************  Orccad *************************************

 File           : $ORCCAD_SRC/Inc/Exec/vxworksOrcc.h
 Author         : Soraya Arias and Nicolas Turro
 Version        : 3.0 alpha
 Creation       : 5 November 1998
 Modification   : 14 December 1998 (Message Queue mis-used)
                  03 July 2000 (Timer setting modification)

 ***************************** Description ***********************************

           Header for Orccad Exec Runtime for VxWorks
            abstract type, define

 **************************** Modifications **********************************

 Author         :
 Date           :

 This file provides an interface between all Orccad system call and VxWorks

 *****************************************************************************
 (c) Copyright 1998, INRIA, all rights reserved
 *****************************************************************************/
#ifndef __vxworksorcc_h
#define __vxworksorcc_h

/////////////////////////
/// VxWorks include
/////////////////////////
#include        <vxWorks.h>
#include        <msgQLib.h>
#include        <semLib.h>
#include        <taskLib.h>
#include        <stdioLib.h>
#include        <sysLib.h>
#include        <string.h>
#include        <ioLib.h>
#include        <wvLib.h>

/////////////////////////
/// Define
/////////////////////////

#define MAXFILENAME  80 /**< Max numbers of characters for file name */

/*-------Message Queues----------*/
#define ORCMSGQ MSG_Q_ID /**< Message Queue Type */
#define SIZE_QUEUE  10   /**< Size of the Message Queue */
#define SIZE_MES    sizeof(int) /**< Size of a message in Message Queue */

#define ORCNORMALPRI 0 /**< Message Queue Normal Priority Level */
#define ORCURGENTPRI 1 /**< Message Queue Urgent Priority Level */

/*-------Semaphores----------*/
#define ORCSEM_ID SEM_ID /**< Semaphore Type */

#define ORCFULL  1    /**< State of a Full Binary Semaphore */
#define ORCEMPTY 0    /**< State of a Empty Binary Semaphore */
#define ORCFOREVER -1 /**< Flag */

/*-------Tasks----------*/
#define ORCTHR_ID    int    /**< Thread Type */

#define PRI_CLK   2 /**< Static clock task priority level */
#define PRI_CNL   3 /**< Static canal task priority level */
#define PRI_RT    5 /**< Static robot controller task priority level */
#define PRI_MT    5 /**< Static module controller task priority level */
#define PRI_PRR   3 /**< Static procedure controller task priority level */

#define SMALL_STACK  5000   /**< Small task stack size */
#define NORMAL_STACK 10000  /**< Normal task stack size */
#define BIG_STACK    100000 /**< Big task stack size */

/*-------Message Queues----------*/
/**
 * Function to create a message queue
 *
 * @param ORCMSGQ pointer to the message queue identifier
 * @return OK if successfull, ERROR otherwise
 * @see ORCMSGQ
 * @see SIZE_QUEUE
 * @see SIZE_MES
 * @see <msgQLib.h>#msgQCreate
 */
inline int orcMsgQCreate(ORCMSGQ *orcMsgQ)
{
    ORCMSGQ nStatus = msgQCreate(SIZE_QUEUE, SIZE_MES, MSG_Q_PRIORITY);

    if (nStatus == NULL) {
        *orcMsgQ = NULL;
        return ERROR;
    }
    *orcMsgQ = nStatus;
    return OK;
}

/**
 * Function to get the number of messages pending in a message queue
 *
 * @param queue the message queue identifier
 * @return msgQNumMsgs() returned value
 * @see <msgQLib.h>#msgQNumMsgs
 */
inline int orcMsgQNumMsgs(ORCMSGQ queue)
{
#ifdef _DEBUG
    printf("orcMsgQNumMsgs nb msg = %d \n \n", msgQNumMsgs(queue));
#endif
    return msgQNumMsgs(queue);
}

/**
 * Function to send a message on a message queue
 * Sending the message is done according to a priority level such as :
 *
 * @param queue message queue identifier
 * @param msg message to be sent
 * @param prio message priority level
 * @return msgQsend() returned value
 * @see <msgQLib.h>#msgQSend
 */
inline int orcMsgQSend(ORCMSGQ queue, int msg, int prio)
{

    if (prio == ORCNORMALPRI) {
        return msgQSend(queue, (char *)&msg, SIZE_MES, WAIT_FOREVER, MSG_PRI_NORMAL);
    }
    else {
        return msgQSend(queue, (char *)&msg, SIZE_MES, WAIT_FOREVER, MSG_PRI_URGENT);
    }
}

/**
 * Function to receive messages on a message queue
 *
 * @param queue message queue identifier
 * @param msg pointer to the received message
 * @return OK if successfull, ERROR otherwise
 * @see SIZE_MES
 * @see WAIT_FOREVER
 * @see <msgQLib.h>#msgQReceive
 */
inline int orcMsgQReceive(ORCMSGQ queue, int *msg)
{

    if (msgQReceive(queue, (char *)msg, SIZE_MES, WAIT_FOREVER) == ERROR) {
        return ERROR;
    }

    //printf("orcMsgQReceive msg received = %d \n", (int) (*msg));
    return OK;
}

/**
 * Function to close and erase a message queue
 *
 * @param queue message queue identifier
 * @return msgQDelete returned value
 * @see  <msgQLib.h>#msgQDelete
 */
inline int orcMsgQClose(ORCMSGQ queue)
{
    return msgQDelete(queue);
}

/*-------Semaphores----------*/
/**
 * Function to create a semaphore
 *
 * @param initState semaphore initial creation state, eg ORCFULL, ORCEMPTY
 * @return ORCSEM_ID semBcreate() returned value
 * @see ORCFULL
 * @see ORCEMPTY
 * @see <semLib.h>#semBCreate
 */
inline ORCSEM_ID orcSemCreate(int initState)
{
    if (initState == ORCEMPTY) {
        return semBCreate(SEM_Q_PRIORITY, SEM_EMPTY);
    }
    else {
        return semBCreate(SEM_Q_PRIORITY, SEM_FULL);
    }
}

/**
 * Function to delete a given semaphore
 *
 * @param sem semaphore identifer to be deleted
 * @return semDelete() returned value
 * @see <semLib.h>#semDelete
 */
inline int orcSemDelete(ORCSEM_ID sem)
{
    return semDelete(sem);
}

/**
 * Function to give a semaphore
 *
 * @param sem semaphore identifier
 * @return semGive() returned value
 * @see <semLib.h>#semGive
 */
inline int orcSemGive(ORCSEM_ID sem)
{
    return semGive(sem);
}

/**
 * Function to take a semaphore
 *
 * @param sem semaphore identifier
 * @return semTake() returned value
 * @see <semLib.h>#semTake
 */
inline int orcSemTake(ORCSEM_ID sem, int timeout)
{
    if (timeout == ORCFOREVER) {
        return semTake(sem, WAIT_FOREVER);
    }
    else {
        return semTake(sem, timeout);
    }
}

/*-------Tasks----------*/
/**
 * Function for suspending current process
 *
 * @param delay in ??
 * @return taskDelay() returned value
 * @see <taskLib.h>#taskDelay
 */
inline int orcTaskDelay(int delay)
{
    return taskDelay(delay);
}

/**
 * Function to check if a task is still valid or not
 *
 * @param Id task identifier
 * @return taskIdVerify() returned value
 * @see <taskLib.h>#taskIdVerify
 */
inline int orcTaskIdVerify(ORCTHR_ID Id)
{
    return taskIdVerify(Id);
}

/**
 * Function to kill a task (pthread)
 *
 * @param Id task identifier
 * @return taskDelete() returned value
 * @see <taskLib.h>#taskDelete
 */
inline int orcTaskDelete(ORCTHR_ID Id)
{
    return taskDelete(Id);
}

/**
 * Function to spawn a task
 *
 * @param tid task identifier
 * @param name name descripting the task
 * @param prio task priority level
 * @param stackSize stack size to be allocated for the task
 * @param funcptr routine to be spawn by the task
 * @param arg argument needed for the routine to be spawn
 * @return OK if successfull, ERROR otherwise
 * @see <taskLib.h>#taskSpawn
 */
inline int orcSpawn(ORCTHR_ID *tid, const char *name, int prio, int stackSize,
                    FUNCPTR funcptr, void *arg)
{
    if ((*tid = taskSpawn((char *)name, prio, VX_FP_TASK | VX_STDIO, stackSize,
                          (FUNCPTR) funcptr, (int) arg, 0, 0, 0, 0, 0, 0, 0, 0, 0)) == ERROR) {
        return ERROR;
    }
    else {
        return OK;
    }
}

/**
 * Function to lock a task
 *
 * @param
 * @return taskLock() returned value
 * @see <taskLib.h>#taskLock
 */
inline int orcTaskLock()
{
    return taskLock();
}

/**
 * Function to unlock a task
 *
 * @param
 * @return taskUnLock() returned value
 * @see <taskLib.h>#taskUnLock
 */
inline int orcTaskUnlock()
{
    return taskUnlock();
}

/*-------Timers----------*/
/**
 * Function to set timer signal mask
 * Function does nothing but defined for interoperability needs between OS
 *
 * @param
 * @return OK
 */
inline int orcTimerSigMask()
{
    return OK;
}

/**
 * Function to create a periodic timer
 * Frequency must be higher than 2 Hz
 *
 * @param period value to be set for periodic timer
 * @return sysAuxClkRateSet() returned value
 * @see <vxworks.h>#sysAuxClkRateSet
 */
inline int orcTimerInit(double period)
{
    int frequency;
    double freq;

    // PB when frequency < 2 ??
    freq = 1 / period;
    if (freq < 3.0) {
        printf("Frequency forced to  be > 2Hz -> unless vxworks timer failed\n");
        frequency = 3;
    }
    else {
        frequency = (int) freq;
    }
    return sysAuxClkRateSet(frequency);
}

/**
 * Function to set the periodic timer handler
 *
 * @param func timer handler function pointer
 * @param arg argument for the timer handler function
 * @return sysAuxClkConnect() returned value
 * @see <vxworks.h>#sysAuxClkConnect
 */
inline int orcTimerArm(FUNCPTR func, void *arg)
{
    return sysAuxClkConnect(func, (int)arg);
}

/**
 * Function to launch the periodic timer
 *
 * @param
 * @return OK
 * @see  <vxworks.h>#sysAuxClkEnable()
 */
inline int orcTimerLaunch()
{
    sysAuxClkEnable();
    return OK;
}

/**
 * Function to delete/erase the timer
 *
 * @param
 * @return OK
 * @see <vxworks.h>#sysAuxClkDisable
 */
inline int orcTimerDelete()
{
    sysAuxClkDisable();
    return OK;
}

/*------ Misc ----------*/
/**
 * Function to check a given path
 *
 * @param path pathname to retrieve
 * @return OK
 * @see <ioLib.h>#ioDefPathGet
 */
inline int orcGetPath(char *path)
{
    ioDefPathGet(path);
    return OK;
}

/**
 * Function to set a directory path
 *
 * @param path name pf the directory path
 * @return ioDefPathSet() returned value
 * @see <ioLib.h>#ioDefPathSet
 */
inline int orcSetPath(const char *path)
{
    return ioDefPathSet((char *)path);
}

/**
 * Function to spy events
 *
 * @param eventid event identifier to spy
 * @param buffer
 * @param bufsize
 * @return wvEvent() returned value
 * @see <wvLib.h>#wvEvent
 */
inline int orcSpyEvent(int eventid, char *buffer, int bufsize)
{
    return wvEvent(eventid, buffer, bufsize);
};

#endif
