//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/users.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime
//            abstract type, define
//
//**************************** Modifications **********************************
//
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#ifndef __utils_h
#define __utils_h

enum TYPE_EVENT {
    NO_EVENT,   // No occurence of event
    SET_EVENT,  // Set an user event occurence
    TAKE_EVENT  // event take by the system
};
#endif
