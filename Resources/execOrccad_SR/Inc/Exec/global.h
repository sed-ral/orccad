//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/global.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 beta
// Creation       : 6 November 1996
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec storage
//           of globals object (time, debug, datas...)
//
// Class Definition:
//   Global: Storage of globals object (time, debug, datas...)
//
//**************************** Modifications **********************************
//
// 6 November 1996 integration on Orccad Exec
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#ifndef __global_h
#define __global_h

class ProcedureRobot;
class Canal;
class Parameters;
class ClockGen;
class DataDebug;
class EventDebug;

class Global
{
protected:
    ProcedureRobot *PrR;       // application
    //we prefer to copy pointers for a fast acces
    Canal *EventInterface;     // synchronous/asynchronous interface
    Parameters *GlobalState;   // Datas for parameters
    ClockGen  *Clock;          // Clock
    DataDebug *DataStorage;    // Store Datas coming from ports
    EventDebug *EventStorage;  // Store Events and Errors

public:
    Global(ProcedureRobot *prr);
    ProcedureRobot *GetPrr() {
        return PrR;
    };

};
#endif
