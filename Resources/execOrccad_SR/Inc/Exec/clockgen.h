//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/clock.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime
//           a clock generator
//
// Class Definition:
//   ClockGen  = object which generate clocks
//
//
//**************************** Modifications **********************************
//
// 6 November 1996
//
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#ifndef __clockgen_h
#define __clockgen_h

#include "utils.h"

class RobotTask;
class ProcedureRobot;

class ClockGen
{
    // real-time horloge interrupt function
    // which control MTs (active) periodicity
    // friend void clockit(ClockGen* );
    friend void timerArm(void *arg);
    friend void clockit(void *clock);
    friend void clocks(ProcedureRobot *prr);
    friend class RobotTask;
protected:
    int clockStatus;
    ORCTHR_ID clockitId;            // id of the base clock connected to IT
    ORCTHR_ID clocksId;             // id of the Harmonic Clocks real-time task
    ORCSEM_ID BaseClock;            // base clock semaphore
    double BasePeriod;              // period of the base clock
    int HarmonicClock[MAXHARMONIC]; // harmonic clock (time of base clock)
    int ActiveClock[MAXHARMONIC];   // Active a clock (= 1)
    int NbHClocks;                  // Number of harmonic clocks
    double Time;                    // Global time of the prr

public:
    // Constructor
    ClockGen(double, int *, int);
    // Destructor
    ~ClockGen();
    // Time functions
    double GetBasePeriod() {
        return BasePeriod;
    }
    void AddBasePeriod() {
        Time += GetBasePeriod();
    }
    int GetNumberOfClks() {
        return NbHClocks;
    }
    void ResetClks();
    double GetHarmonicPeriod(int); // Get a harmonic Clock period
    double GetTime() {
        return(Time);
    }
    void Stop();
    void Init(ProcedureRobot *);
    int IsClkActive(int);

};
#endif
