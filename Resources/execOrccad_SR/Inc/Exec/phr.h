//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/phr.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 beta
// Creation       : 18 May 1994
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtine
//           of a Physical Ressource
//
// Class Definition:
//    PhR: represents an interface with the physical world using soft driver
//         for the robot...
//
// Errors: 500-600
//
//**************************** Modifications **********************************
//
// 6 November 1996  Integration in Orccad Exec
// Modif: RPG1 = integration of sensors init RPG 26 Jul 99.
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#ifndef __phr_h
#define __phr_h

#include "utils.h"
#include "global.h"
#include "eventdbg.h"

class ProcedureRobot;
class RobotTask;

class PhRSensor: public Global
{
    friend class PhR;
    friend class ListPhRSensor;
protected:
    int Status;
    int State;
    char *Name;
    PhRSensor *Next;        // double pointer list for rt
    PhRSensor *Previous;
public:
    PhRSensor(ProcedureRobot *prr, char *);
    virtual ~PhRSensor();
    char *GetName() {
        return Name;
    }
    int GetStatus() {
        return Status;
    };
    int GetState() {
        return State;
    };
    void SetState(int st) {
        State = st ;
    };
    virtual int Init() {
        printf("virtual PhRSensor::Init()\n");
        return ERROR;
    };
    virtual int Close() {
        printf("virtual PhRSensor::Close()\n");
        return ERROR;
    };
};

//
// Class to manage PhRSensor list
//
class ListPhRSensor
{
    friend class RobotTask;
    friend void fct_control(RobotTask *rt);
    friend class PhR;

protected:
    PhRSensor *List;  // PhRSensor list
    int Number;       // Number of PhRSensor
public:
    // Destructor,Constructor
    ListPhRSensor() {
        List = (PhRSensor *)NULL;
        Number = 0;
    }
    ~ListPhRSensor();
    int insert(PhRSensor *);
    int GetNumber() {
        return(Number);
    };
    // Init & Close driver on Phrs
    int Init();
    int Close();
};

class PhR: public Global
{
    friend class RobotTask;
    friend void fct_control(RobotTask *rt);

protected:
    int Status;
    int State;
    char *Name;
    int NbOutputs;           // number of automaton output to wait
    ORCSEM_ID TabSem[MAXTABOUTEVENT]; // Tab of semaphores where index is
    // a outputnumber: O_ACTIVATE,
    // O_NORMAL,O_TRANSITE,O_END
    int TabEvent[MAX_SEMPHR];
    ListPhRSensor *Sensors;   // List of sensors

public:
    PhR(ProcedureRobot *prr, char *, int, int, int, int);
    virtual ~PhR();
    char *GetName() {
        return Name;
    }
    int GetStatus() {
        return Status;
    };
    int GetState() {
        return State;
    };
    void SetState(int st) {
        State = st ;
    };
    int insert(PhRSensor * );
    PhRSensor *getSensor(char *);
    virtual int Init() {
        printf("virtual PhR::Init()\n");
        return ERROR;
    };
    virtual int Close() {
        printf("virtual PhR::Close()\n");
        return ERROR;
    };
    // RPG1 add two methods for init and close
    // Call the Init of the Phr robot and sensors
    int AllInit();
    // Call the Close of the Phr robot and sensors
    int AllClose();
};
#endif
