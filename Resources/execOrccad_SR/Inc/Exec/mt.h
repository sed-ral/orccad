//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/mt.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 beta
// Creation       : 18 May 1994
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtine of
//           a Module Task.
//
// Class Definition:
//    ModuleTask:  controls a real-time algorithm
//    ListModuleTask: manages a  ModuleTask list
//
// Errors: 300-400
//
//**************************** Modifications **********************************
//
// 6 November 1996  Integration in Orccad Exec
//
// 25 August 2006 RPG Add methods for param handling within MT context
//                No more param handling within algorithic module context
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
// *****************************************************************************
#ifndef __mt_h
#define __mt_h

#include "utils.h"
#include "global.h"

#define ORC_SEND_EVENT(EVNAMEFROM,EVNAMETO,MODNAME,TRNAME) \
    if (mt->MODNAME.EVNAMEFROM == SET_EVENT){\
        (mt->EventInterface)->SendEvent(EVNAMETO,TRNAME,mt->RT->GetGlobalTime());\
        mt->MODNAME.EVNAMEFROM = TAKE_EVENT; }


#define ORC_REINIT_PHR(PHR_NAME) \
    if ( (access_drv == PHR_ACCESS) &&\
            ( (status_rt==ACTIVE)||(status_rt==TRANSITE)) &&\
            ( do_reinit == FALSE) ) {\
        mt->PHR_NAME.reinit();\
        do_reinit = TRUE;}

class RobotTask;
class ModuleTask: public Global
{
    friend class RobotTask;
    friend class ListModuleTask;
    friend void fct_control(RobotTask *rt);

protected:
    char *Name;
    ModuleTask *Next;         // double pointer for list
    ModuleTask *Previous;
    RobotTask  *RT;
    int Status;
    MT_STATE State;            // mt state (MT_NOT_CREATED,MT_FREE,MT_OCCUPY)
    ORCTHR_ID MtId;            // id of the real-time task
    // correspponding at the MT
    int IndexClk;              // Clock index activated by ClockGen
    double Sample_time;        // algorithm current time
    ORCSEM_ID SemTable[USED_SEM]; // table of semaphores for the
    // management
    // of MTs from the controler
    int    Filter_Iteration;   // MT filter size iteration
    int    CondEnd;            // TRUE/FALSE authorises MT to be
    // actived
    int    CondKill;           // TRUE/FALSE authorises Tms to be
    // killed
    int    RequestParam;       // TRUE/FALSE request mts
    // reparametrization
    int    OnlineParam;        // TRUE/FALSE request re-parametrization
    // RPG#param 25/08/06 vvvvv
    Parameters *pTabParam;     // Parameters rt
    // RPG#param 25/08/06 ^^^^^
    FUNCPTR func;              // pointer real-time mt function

public:
    // Constructor, Destructor
    ModuleTask(FUNCPTR f_ptr, RobotTask *rt,
               char *name, int itFilter, int indexclk);
    ~ModuleTask();
    // Delete real-time task
    void DeleteTask();
    // Reparam
    int IsReparam() {
        return OnlineParam;
    };
    int SetReparam() {
        OnlineParam = TRUE;
        return OK;
    };
    int EndReparam() {
        OnlineParam = FALSE;
        return OK;
    };
    // RPG#param 25/08/06 vvvvv
    void PlugParam();
    // RPG#param 25/08/06 ^^^^^

    // launch
    STATUS create();
    STATUS SemaphoreGive(int);      // gives a semaphore
    // from SemTable
    STATUS SemaphoreTake(int, int); // takes a semaphore from
    // SemTable with timeout option
    double GetSampleTime() {
        return Sample_time;
    };
    int GetStatus() {
        return Status;
    };
    MT_STATE GetState() {
        return State;
    };
    RobotTask  *GetRobotTaskPtr() {
        return(RT);
    }; // KK
    void BeginCompute();
    void EndCompute();
    // RPG#param 25/08/06 vvvvv
    // Methods redefined by inheritance
    void Param() {
        printf("Module-task::param()\n");
    }
    void ReParam() {
        printf("Module-task::reparam()\n");
    }
    // RPG#param 25/08/06 ^^^^^

};

//
// Class to manage mts list
//
class ListModuleTask
{
    friend class RobotTask;
    // real-time rt controler
    friend void fct_control(RobotTask *);

protected:
    ModuleTask *List; // Module-task list
    int Number;       // Number of mt

    ListModuleTask() {
        List = (ModuleTask *)NULL;
        Number = 0;
    }
    ~ListModuleTask() {};
    // Delete real-time tasks
    void DeleteTasks();
    int insert(ModuleTask *);
    // int insert(FUNCPTR,RobotTask *,char*,int);
    int GetNumber() {
        return(Number);
    };
};
#endif
