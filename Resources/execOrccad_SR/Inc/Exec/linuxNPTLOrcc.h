// *******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/linuxNPTLOrcc.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
// ***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime for Linux 2.6
//            abstract type, define
//
// **************************** Modifications **********************************
//
// Author         : Soraya Arias
// Date           : 24 June 2004
//
// This file provides an interface between all Orccad system call and
// Linux 2.6 Kernel (integrating the native posix thread library and
// message queues)
//
// BEWARE: timer resolution grain is 1ms in this version !
//
// *****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
// *****************************************************************************
#ifndef __linuxnptlorcc_h
#define __linuxnptlorcc_h

/////////////////////////
/// Linux include
/////////////////////////
#include <values.h>
#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <signal.h>
#include <time.h>
#include <semaphore.h>
#include <pthread.h>
#include <sched.h>

/////////////////////////
/// Define
/////////////////////////
typedef void (*FUNCPTR)  (...);
typedef void *(*SOSO)  (void *);  /**< Special type to be used for casting in pthread_create() */


#define OK 0
#define ERROR -1
#define STATUS int

#define FALSE 0
#define TRUE 1

/**
 * Max number of characters for file name
 * This number is based on _POSIX_PATH_MAX value (see posix1_lim.h and see limits.h the
 * ifdef __USE_POSIX statement)
 *
 * @see <posix1_lim.h>#_POSIX_PATH_MAX
 * @see <limits.h>
 */
#define MAXFILENAME _POSIX_PATH_MAX +1


/*-------Message Queues----------*/
#ifdef _ORC_MQ_POSIX
#include <mqueue.h>
#define ORCMSGQ mqd_t     /**< Posix Message Queue Type */
#define SIZE_QUEUE  16    /**< Size of the Message Queue */
#define MSG_SIZE  sizeof(MAXINT)
#define MQPERM IPC_CREAT | IPC_EXCL | 0666 /**< Message Queue Permission */
#define MQPRIO_MIN 1      /**< Min Message Queue Priority Level */
#define MQPRIO_MAX _POSIX_MQ_PRIO_MAX /**< Max Message Queue Priority Level */
#define MQACCESS_FIFO 0   /**< Message Queue Receive Policy */

#define ORCNORMALPRI 0    /**< Message Queue Normal Priority Level */
#define ORCURGENTPRI 1    /**< Message Queue Urgent Priority Level */

extern char orcMsgQName[256];       /**< Global name for the message queue */
extern char orcMsgQRec[SIZE_QUEUE]; /**< Global name for the received message message queue */
#else
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
/**
 * Message queue Id structure
 */
typedef struct msgQId {
    int msgId; /**< Message queue id */
    key_t key; /**< Key */
    //  char msgName[256];
} orcmsgQ_t;

/**
 * Message buffer structure used within message queue
 */
typedef struct msgBuf {
    long mtype;     /**< Message type, must be > 0 */
    int orcmsgInfo; /**< Message data */
} orcmsgQBuf_t;

#define ORCMSGQ orcmsgQ_t * /**< Message Queue Type */
#define SIZE_QUEUE  10      /**< Size of the Message Queue */
#define SIZE_MES_MAX 4056   /**< Maximum size of message in Message Queue */
#define SIZE_MES_MIN 128    /**< Minimum size of message in Message Queue */
#define MQPERM IPC_CREAT | IPC_EXCL | 0666 /**< Message Queue Permission */
#define MQPRIO_MAX (255)        /**< Message Queue Priorities */
#define ORCNORMALPRI 1          /**< Message Queue Normal Priority Level */
#define ORCURGENTPRI MQPRIO_MAX /**< Message Queue Urgent Priority Level */
#endif
/*-------Semaphores----------*/
#define ORCSEM_ID sem_t * /**< Semaphore Type */
#define ORCFULL  1        /**< State of a Full Binary Semaphore */
#define ORCEMPTY 0        /**< State of a Empty Binary Semaphore */
#define ORCFOREVER -1     /**< State of a ??? Binary Semaphore */

#define WAIT_FOREVER 0 /**< Define set to be compatible with previous OS */

/*-------Tasks----------*/
#define ORCTHR_ID pthread_t *    /**< Posix Thread Type */
#define STACKSIZE size_t         /**< Stack size Type */
#define SCHEDPOLICY SCHED_OTHER  /**< Scheduling policy (see schedbits.h, sched.h )*/

#define PRI_CLK   sched_get_priority_max(SCHEDPOLICY) /**< Clock task priority level (see sched_get_priority_max() in sched.h) */
#define PRI_CNL   PRI_CLK - 1 /**< Canal task priority level */
#define PRI_RT    PRI_CLK - 2 /**< Robot controller task priority level */
#define PRI_MT    PRI_RT      /**< Module controller task priority level */
#define PRI_PRR   PRI_RT      /**< Procedure controller task priority level */

#define SMALL_STACK  5000           /**< Small task stack size PTHREAD_STACK_MIN not defined */
#define NORMAL_STACK 5*SMALL_STACK  /**< Normal task stack size */
#define BIG_STACK    15*SMALL_STACK /**< Big task stack size */

/*-------Timers----------*/
#define TIME_UNIT 1000000000 /**< Time unit = nanosecond */
#define TIME_RES  1000000    /**< Linux Timer Resolution : 1 ms = 1000000 ns*/

/**
 * Timer structure definition
 */
typedef struct _orc_timer_t {
    timer_t timerId;
    ORCTHR_ID pthrTimerHandlerId;
    struct itimerspec timerInterval;
    double period; /**< Periodic delay */
} orcTimer_t;

#define ORCTIMER_ID orcTimer_t * /**< Timer Type */
#define ORCSIGTIMER SIGRTMIN      /**< Signal the timer is linked with */
#define TIMERHANDLER FUNCPTR     /**< Timer function handler type */
typedef struct _timer_struct_t {
    TIMERHANDLER arg1;
    void *arg2;
    sigset_t *arg3;
}  TIMERSTRUCT; /**< Timer function handler argument structure */

//extern "C" double rint(double);

static sigset_t allsigset, oldsigset;

static TIMERSTRUCT orcTimerStruct; /**< Timer function handler argument */
static ORCTIMER_ID orcTimer;       /**< Timer function handler */
static int nMsgSize;

/*-------Message Queues----------*/
#ifdef _ORC_MQ_POSIX
/**
 * Function to create a message queue
 *
 * @param
 * @return ORCMSGQ pointer if posix message queue created, NULL otherwise and errno is set
 * @see ORCMSGQ
 * @see <mqueue.h>#mq_open
 * @see <mqueue.h>#mq_unlink
 */
inline int orcMsgQCreate(ORCMSGQ *orcMsgQ)
{
    ORCMSGQ err;
    struct mq_attr mqAttr;
    char sTmp[256];

    sprintf(sTmp, "%d", MAXINT);
    nMsgSize = sizeof(sTmp) - 1;

#ifdef _DEBUG
    fprintf(stderr, "orcMsgQCreate:: nMsgSize = %d\n", nMsgSize);
    fprintf(stderr, "orcMsgQCreate:: sizeof(MAXINT = %d \n", sizeof(MAXINT));
#endif
    mqAttr.mq_maxmsg = SIZE_QUEUE;
    mqAttr.mq_msgsize = nMsgSize;
    mqAttr.mq_flags = 0; // 0 = blocking open on the queue

    // Create the MQNAME identifier MQNAME =  getpid()
    sprintf(orcMsgQName, "/%ld", getpid());

    err = mq_open(orcMsgQName, O_RDWR | O_CREAT | O_EXCL, 0x1B0, &mqAttr);
    if (err == (ORCMSGQ)ERROR) {
        if (errno == EEXIST) {
            printf("msq already exists \n");
            mq_unlink(orcMsgQName);
            err = mq_open(orcMsgQName, O_RDWR | O_CREAT, 0x1B0, &mqAttr);
            if (err == (ORCMSGQ)ERROR) {
                *orcMsgQ = -1;
                return ERROR;
            }
        }
        else {
            *orcMsgQ = -1;
            return ERROR;
        }
    }
    *orcMsgQ = err;
    return OK;
}

/**
 * Function to get the number of messages pending in a posix message queue
 *
 * @param posix message queue
 * @return long number of messages
 * @see <mqueue.h>#mq_getattr
 * @see <mqueue.h>#mq_attr
 */
inline long orcMsgQNumMsgs(ORCMSGQ queue)
{
    struct mq_attr mqstat;
    mq_getattr(queue, &mqstat);
    return mqstat.mq_curmsgs;
}

/**
 * Function to send a message on a posix message queue
 * Sending the message is done according to a priority level such as :
 *
 * @param posix message queue
 * @param msg message to be sent
 * @param prio message priority level
 * @return mq_send returned value
 * @see <mqueue.h>#mq_send
 */
inline int orcMsgQSend(ORCMSGQ queue, int msg, int prio)
{

#ifdef _DEBUG
    fprintf(stderr, "orcMsgQSend msg = %d \n", msg);
#endif
    sprintf(orcMsgQRec, "%d", msg);

    if (prio == ORCNORMALPRI) {
        return mq_send(queue, orcMsgQRec, sizeof(orcMsgQRec), MQPRIO_MIN);
    }
    else {
        return mq_send(queue, orcMsgQRec, sizeof(orcMsgQRec), MQPRIO_MAX);
    }
}

/**
 * Function to receive messages on a posix message queue
 *
 * @param posix message queue
 * @param msg pointer to the received message
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <mqueue.h>#mq_receive
 */
inline int orcMsgQReceive(ORCMSGQ queue, int *nMsgVal)
{
#ifdef _DEBUG
    fprintf(stderr, "orcMsgQReceive:: SIZE_QUEUE = %d \n", SIZE_QUEUE);
#endif

    if (mq_receive(queue, orcMsgQRec, nMsgSize, MQACCESS_FIFO) == ERROR) {
        fprintf(stderr, "mq_receive Error = %d \n", errno);
        return ERROR;
    }
    *nMsgVal = atoi(orcMsgQRec);

#ifdef _DEBUG
    fprintf(stderr, "orcMsgQReceive:: msg = %d \n", *nMsgVal);
#endif

    return OK;
}

/**
 * Function to close and erase a posix message queue
 *
 * @param queue pointer to the message queue
 * @return mq_close returned status or mq_unlink returned status
 * @see  <mqueue.h>#mq_close
 * @see  <mqueue.h>#mq_unlink
 */
inline int orcMsgQClose(ORCMSGQ queue)
{
    int err1, err2;

    err1 = mq_close(queue);
    err2 = mq_unlink(orcMsgQName);

    return(err1 ? err1 : err2);
}
#else
/**
 * Function to create a message queue
 *
 * @param ORCMSGQ pointer if message queue created, NULL otherwise and errno is set
 * @return OK if successfull, ERROR otherwise
 * @see ORCMSGQ
 * @see <sys/msg.h>#msgget
 * @see <sys/msg.h>#msgctl
 *
 */
inline int orcMsgQCreate(ORCMSGQ *orcMsgQ)
{
    //  ORCMSGQ orcMsgQ;
    key_t key = IPC_PRIVATE;
    int err;

    err = msgget(key, MQPERM);
    if ((err == ERROR) && (errno == EEXIST)) {
        printf("orcMsgQCreate: msgQ already exists \n");
        err = msgget(key, 0);
        msgctl(err, IPC_RMID, 0);
        err = msgget(key, IPC_CREAT | IPC_EXCL | MQPERM);
    }
    else if ((err == ERROR) && (errno != EEXIST)) {
        printf("orcMsgQCreate: Error = %d \n", errno);
        return ERROR;
    }

    *orcMsgQ = (ORCMSGQ) malloc(sizeof(orcmsgQ_t));
    (*orcMsgQ)->msgId = err;
    (*orcMsgQ)->key = key;

    return err;
}

/**
 * Function to get the number of messages pending in a message queue
 *
 * @param queue pointer to the message queue
 * @return long number of messages if no error occurs otherwise ERROR and errno is set
 * @see <sys/msg.h>#msgctl
 */
inline long orcMsgQNumMsgs(ORCMSGQ queue)
{
    struct msqid_ds tmpBuf;

    if (msgctl(queue->msgId, IPC_STAT, &tmpBuf) == ERROR) {
        printf("orcMsgQNumMsgs: Error = %d \n", errno);
        return ERROR;
    }
#ifdef _DEBUG
    printf("orcMsgQNumMsgs:: nb of msgs = %d \n", tmpBuf.msg_qnum );
#endif
    return (long)tmpBuf.msg_qnum;
}

/**
 * Function to send a message on a message queue
 * Sending the message is done according to a priority level such as :
 *
 * @param queue pointer to the message queue
 * @param msg message to be sent
 * @param prio priority level
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <sys/msg.h>#msgsnd
 */
inline int orcMsgQSend(ORCMSGQ queue, int msg, int prio)
{
    orcmsgQBuf_t tmpBuf;
    int nMsgSize;

    nMsgSize = sizeof(orcmsgQBuf_t) - sizeof(long);
#ifdef _DEBUG
    printf("orcMsgQSend: msgId = %d \n", queue->msgId);
    printf("orcMsgQSend: nMsgSize = %d \n", nMsgSize);
#endif
    tmpBuf.orcmsgInfo = msg;
    tmpBuf.mtype = MQPRIO_MAX - prio;

    /* 0 : If message queue is full -> message is not written to the queue */
    if (msgsnd(queue->msgId, &tmpBuf, nMsgSize, 0) == ERROR) {
        printf("orcMsgQSend: Error = %d \n", errno);
        return ERROR;
    }
    return OK;
}

/**
 * Function to receive messages on a message queue
 *
 * @param queue pointer to the message queue
 * @param msg pointer to the message received
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <sys/msg.h>#msgrcv
 */
inline int orcMsgQReceive(ORCMSGQ queue, int *msg)
{
    orcmsgQBuf_t tmpBuf;
    int nMsgSize;

    nMsgSize = sizeof(orcmsgQBuf_t) - sizeof(long);

    /* 0 : the calling process blocks until a message arrives in queue */
    /* mtype = -MQPRIO_MAX: type of message to retrieve from the queue
               if mtype < 0 : first message on
                the queue with the lowest type less than  or  equal
                to the absolute value of msgtyp will be read
    */
    if (msgrcv(queue->msgId, &tmpBuf, nMsgSize, -MQPRIO_MAX, 0) == ERROR) {
        printf("orcMsgQReceive: Error = %d \n", errno);
        return ERROR;
    }
#ifdef _DEBUG
    printf("orcMsgQReceive: msg received = %d \n", tmpBuf.orcmsgInfo);
#endif
    *msg = tmpBuf.orcmsgInfo;

    return OK;
}

/**
 * Function to close and erase a message queue
 *
 * @param queue pointer to the message queue
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <sys/msg.h>#msgctl
 */
inline int orcMsgQClose(ORCMSGQ queue)
{

    if (msgctl(queue->msgId, IPC_RMID, 0) == ERROR) {
        printf("orcMsgQClose: Error = %d \n", errno);
        return ERROR;
    }
    free(queue);

    return OK;
}
#endif

/*-------Semaphores----------*/
/**
 * Function to create a semaphore
 *
 * @param initState semaphore initial creation state, eg ORCFULL, ORCEMPTY
 * @return ORCSEM_ID semaphore pointer created if successfull, NULL otherwise
 * @see ORCFULL
 * @see ORCEMPTY
 * @see <semaphore.h>#sem_init
 */
inline ORCSEM_ID orcSemCreate(int initState)
{
    ORCSEM_ID newSem;
    newSem = new sem_t;
    int status;

    if (initState == ORCEMPTY) {
        status = sem_init(newSem, 0, 0);    // 1: shared between processes, 0 init value
    }
    else {
        status = sem_init(newSem, 0, 1);
    }

    if (status == OK) {
        return newSem;
    }
    else {
        return NULL;
    }
}

/**
 * Function to delete a given semaphore
 *
 * @param sem semaphore to be deleted
 * @return sem_destroy() returned value
 * @see <semaphore.h>#sem_destroy
 */
inline int orcSemDelete(ORCSEM_ID sem)
{
    return (sem_destroy(sem));
}

/**
 * Function to give a semaphore
 *
 * @param sem semaphore
 * @return sem_post) returned value
 * @see <semaphore.h>#sem_post
 */
inline int orcSemGive(ORCSEM_ID sem)
{
    return (sem_post(sem));
}

/**
 * Function to take a semaphore
 * TODO : no timeout in Posix semaphores
 *
 * @param sem semaphore
 * @param timeout value
 * @return sem_wait() returned value
 * @see <semaphore.h>#sem_wait
 */
inline int orcSemTake(ORCSEM_ID sem, int timeout)
{

    int dummy = timeout; // set to avoid compilation warnings

    return (sem_wait(sem));
}

/*-------Tasks----------*/
/**
 * Function for suspending current process
 * TODO : no taskDelay in Posix (sleep uses seconds, delay ?, nanosleep ?)
 *
 * @param delay
 * @return OK
 */
inline int orcTaskDelay(int delay)
{
    int dummy = delay; // set to avoid compilation warnings

    return OK;
}

/**
 * Function to check if a task is still valid or not
 *
 * @param Id task identifier
 * @return OK if task is valid, ERROR otherwise
 * @see ORCTHR_ID
 * @see pthread_kill
 */
inline int orcTaskIdVerify(ORCTHR_ID Id)
{
    if (Id != NULL) {
        if (pthread_kill(*Id, 0) != OK) {
            return ERROR;
        }
        else {
            return OK;
        }
    }
    return ERROR;
}

/**
 * Function to kill a task (pthread)
 *
 * @param Id task identifier
 * @return ERROR or pthread_cancel returned value
 * @see ORCTHR_ID
 * @see <pthread.h>#pthread_kill
 * @see <pthread.h>#pthread_cancel
 */
inline int orcTaskDelete(ORCTHR_ID Id)
{
    // Check if thread is ok
    if ( Id != NULL) {
        if (pthread_kill(*Id, 0) == OK) {
            // Send SIGTERM signal <- BAD!!
            // return (pthread_kill(*Id,SIGTERM));
            return(pthread_cancel(*Id));
        }
    }
    return ERROR;
}

/**
 * Function to spawn a task
 *
 * @param tid task identifier
 * @param name name descripting the task
 * @param prio task priority level
 * @param stackSize stack size to be allocated for the task
 * @param funcptr routine to be spawn by the task
 * @param arg argument needed for the routine to be spawn
 * @return pthread_create returned value
 * @see ORCTHR_ID
 * @see STACKSIZE
 * @see FUNCPTR
 * @see <pthread.h>#pthread_create
 */
inline int orcSpawn(ORCTHR_ID *tid, const char *name, int prio, STACKSIZE stackSize, FUNCPTR funcptr, void *arg)
{

    int status;

    *tid = new pthread_t;

    pthread_attr_t attributes;
    sched_param schedattributes;

    // To avoid compilation warnings
    char *dummy = (char *)malloc((strlen(name) * sizeof(char)) + 1);
    strcpy(dummy, name);

    // Allocation and initialization of the attribute structure
    pthread_attr_init(&attributes);
    // Set stack size : BEWARE in SOlaris is better to set stacksize to default value
    // i.e. NULL instead of a user defined stacksize value -> see man pthread_create
    /*
      #ifdef _DEBUG
      fprintf(stderr, "StackSize = %d \n", stackSize);
      #endif
    */
    //  pthread_attr_setstacksize(&attributes, stackSize);
    // Set thread priority
    /*
      schedattributes.sched_priority = prio;
      pthread_attr_setschedparam(&attributes, &schedattributes);
    */
    // Thread creation
    status =  pthread_create(*tid, &attributes, (SOSO)funcptr, arg);
#ifdef _DEBUG
    fprintf(stderr, "pthread_Create %s error = %d\n%s\n", name, status, strerror(errno));
#endif

    return status;
}

/**
 * Function to lock a task
 * This function does nothing : defined for interoperability between OS needs
 *
 * @param
 * @return OK
 */
inline int orcTaskLock()
{
    return OK;
}

/**
 * Function to unlock a task
 * This function does nothing : defined for interoperability between OS needs
 *
 * @param
 * @return OK
 */
inline int orcTaskUnlock()
{
    return OK;
}

/*-------Timers----------*/
/**
 * Timer handler function
 *
 * @param int
 * @return
 */
void *orcTimer_handler(void *);

/**
 * Function to mask timer signal interruption
 *
 * @param
 * @return OK if successful, ERROR otherwise and errno is set
 * @see ORCSIGTIMER
 * @see <signal.h>#sigprocmask
 * @see <signal.h>#sigaddset
 */
inline int orcTimerSigMask()
{
    //sigprocmask(SIG_SETMASK, NULL, &oldsigset);
    sigemptyset(&allsigset);
    sigaddset(&allsigset, ORCSIGTIMER);
    if (sigprocmask(SIG_BLOCK, &allsigset, NULL) != OK) {
        fprintf(stderr, "pthread_sigmask Failed %d \n", errno);
        return ERROR;
    }
    return OK;
}

/**
 * Function to set the timer settings
 * This function sets in particular the period for the timer
 *
 * @param period value to be set for timer
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see TIME_UNIT
 * @see TIME_RES
 */
inline int orcTimerSetTime(double period)
{
    struct itimerspec new_setting;
    struct timespec time_delay;
    double sec, nsec;

    //Convert the period in nanosecond
    double delay =  (double) TIME_UNIT * period;
    // Set the timespec struct -> delay in second, delay in microsecond
    // get the part in second
    sec = floor(delay / (double)TIME_UNIT);
    // get the remainder part in nanosecond
    nsec = rint(fmod(delay, (double)TIME_UNIT));

    time_delay.tv_sec =  (int)(sec);
    // Handle period < TIME_RES
    if (delay < (double) TIME_RES) {
        time_delay.tv_nsec = TIME_RES;
    }
    else {
        time_delay.tv_nsec = (int)(floor(nsec / (double)TIME_RES)) * TIME_RES;
    }

    // Setting the delay for the timer interval
    new_setting.it_value.tv_sec = 0; ///time_delay.tv_sec; /** initial expiration */
    new_setting.it_value.tv_nsec = 2 * TIME_RES; //time_delay.tv_nsec;
    new_setting.it_interval.tv_sec =  time_delay.tv_sec; /** timer interval */
    new_setting.it_interval.tv_nsec = time_delay.tv_nsec;

    orcTimer->timerInterval = new_setting;

#ifdef _DEBUG
    fprintf(stderr, "orcTimerSetTime:: new_setting.it_value.tv_sec = %d, new_setting.it_value.tv_nsec = %d \n", new_setting.it_value.tv_sec, new_setting.it_value.tv_nsec);
#endif

    return OK;
}

/**
 * Function to create a periodic timer
 * This function sets a periodic timer,
 * the corresponding signal  and the timer handler
 * and the orcTimer structure is affected
 *
 * @param period value to be set for timer
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see ORCTIMER_ID
 * @see orcTimerSetTime
 * @see orcTimer
 * @see <timer.h>#timer_create
 */
inline int orcTimerInit(double period)
{
    struct sigevent timer_event;

    orcTimer = (ORCTIMER_ID) malloc(sizeof(orcTimer_t));

    orcTimer->period = period;
    orcTimer->pthrTimerHandlerId = new pthread_t;

    // Block the ORCSIGTIMER IT
    if (orcTimerSigMask() == ERROR) {
        return ERROR;
    }

    // Set the asynchronous notification to be posted upon timer's expiration
    // - use signals (sigevent structure)
    // - send ORCSIGTIMER
    // - send extra data consisting of a pointer back to the timer ID
    //  cannot pass the timer ID itself because we haven't created it yet
    timer_event.sigev_notify = SIGEV_SIGNAL;
    timer_event.sigev_signo = ORCSIGTIMER;
    timer_event.sigev_value.sival_ptr = (void *) & (orcTimer->timerId);

    if (timer_create(CLOCK_REALTIME, &timer_event, &(orcTimer->timerId)) < 0) {
        return ERROR;
    }

    return (orcTimerSetTime(period));
}

/**
 * Function to delete/erase the timer
 * This function disabled the periodic timer, the associated signal
 * unmask this signal within the task and erase the orcTimer structure
 *
 * @param
 * @return OK if successfull, ERROR otherwise
 * @see orcTimer
 * @see <timer.h>#timer_delete
 * @see <signal.h>#sigemptyset
 * @see <signal.h>#sigprocmask
 */
inline int orcTimerDelete()
{
    int err = OK;

    // Cancel the timer
    if (timer_delete(orcTimer->timerId) < 0) {
        err = ERROR;
    }
    // Cancel the timer handler thread ???
    orcTaskDelete(orcTimer->pthrTimerHandlerId);
    free(orcTimer->pthrTimerHandlerId);
    // Set a new IT mask
    sigprocmask(SIG_UNBLOCK, &allsigset, NULL);


    free(orcTimer);

    return (err);
}

/**
 * Function to set the periodic timer handler
 *
 * @param func timer handler function pointer
 * @param arg argument for the timer handler function
 * @return OK
 */
inline int orcTimerArm(TIMERHANDLER func, void *arg)
{
    int err;
    sigset_t *sigset = &oldsigset;

    orcTimerStruct.arg1 = func;
    orcTimerStruct.arg2 = arg;
    orcTimerStruct.arg3 = sigset;

    if ((err = orcSpawn(&(orcTimer->pthrTimerHandlerId), "Timer Handler Task", PRI_CLK, SMALL_STACK, (FUNCPTR)orcTimer_handler, (void *)(&orcTimerStruct))) != 0) {
        return err;
    }

    return OK;
}

/**
 * Function to launch the periodic timer
 *
 * @param
 * @return OK if successfull, ERROR otherwise
 * @see ORCSIGTIMER
 * @see <signal.h>#sigismember
 * @see <pthread.h>#pthread_sigmask
 */
inline int orcTimerLaunch()
{
    /*
    // Unblock the ORCSIGTIMER signal
    if (sigismember(&allsig, ORCSIGTIMER)) {
      // Unblock the ORSIGTIMER ie allow timer interruption
      if ((err = pthread_sigmask(SIG_UNBLOCK, &allsig, NULL)) != OK) {
        printf("orcTimerHandler error pthread_sigmask : %d \n", err);
        return ERROR;
      }
    }
    */
    if (timer_settime(orcTimer->timerId, 0, &(orcTimer->timerInterval), NULL) < 0) {
#ifdef _DEBUG
        fprintf(stderr, "orcTimerLaunch:: failed errno = %d \n", errno);
#endif
        orcTaskDelete(orcTimer->pthrTimerHandlerId);
        return ERROR;
    }
#ifdef _DEBUG
    fprintf(stderr, "orcTimerLaunch:: succeeded\n");
#endif

    return OK;
}

/* -------- MISC -------- */
/**
 * Function to check a given path
 * TODO : MAXFILENAME is set to 80 in util.h, it should be PATH_MAX+1
 *
 * @param path pathname to retrieve
 * @return OK if successfull, ERROR otherwise
 * @see MAXFILENAME
 */
inline int orcGetPath(char *path)
{
    if (getcwd(path, MAXFILENAME) != OK) {
        return ERROR;
    }
    return OK;
}

/**
 * Function to set a directory path
 *
 * @param path name pf the directory path
 * @return chdir() returned value
 * @see chdir
 */
inline int orcSetPath(const char *path)
{
    return chdir(path);
}

/**
 * Function to spy events
 * Function does nothing but defined for interoperability need between OS
 *
 * @param eventid event identifier to spy
 * @param buffer
 * @param bufsize
 * @return OK
 */
inline int orcSpyEvent(int eventid, char *buffer, int bufsize)
{
    return OK;
};

#endif
