// Orccad Version: 3.2
// Module : Algo
// Computation File 
// 
// Module Ports :
// 	output EVENT Stop
// 	output INTEGER OutO
// 	param Inre
// 
// 
// Date of creation : Tue Aug 29 23:13:08 2006


OutO=OutO+Incre[0];

if (OutO>Incre[1])
  Stop=SET_EVENT;
