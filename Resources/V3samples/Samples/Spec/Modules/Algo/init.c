// Orccad Version: 3.2
// Module : Algo
// Initialisation File
// 
// Module Ports :
// 	output EVENT Stop
// 	output INTEGER OutO
// 	param EVENT 
// 
// Time Methods usable in module files:
// 	 GetLocalTime(): return relative period time as double value
// 	 GetCurrentTime(): return global period time as double value 
// 
// Date of creation : Tue Aug 29 23:13:08 2006

OutO=0;
