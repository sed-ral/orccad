// Ilv Version: 3.1
// File generated: Tue Aug 29 23:15:35 2006
// Creator class: IlvGraphOutput
Palettes 7
2 "white" 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 61166 61166 59110 "blue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 61166 61166 59110 0 0 0 "default" "fixed" 0 solid solid 0 0 0
3 61166 61166 59110 "red" "default" "fixed" 0 solid solid 0 0 0
5 61166 61166 59110 "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
1 "red" "red" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 6 61166 61166 59110 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 110 60 180 220 
6 IlvMessageLabel 110 60 181 221 F8 0 1 16 4 "r> Algo Saved"   [Box
0 1 1 Algo 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 180 270 20 20 2 
2 IlvMessageLabel 178 256 50 28 F8 0 25 16 4 "Stop"   0
[Port
0 1 1 Stop 190 280 178 256 50 28 8  8 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 280 100 20 20 
2 IlvMessageLabel 260 96 40 28 F8 0 25 16 4 "Out"   0
[Port
0 1 2 Out 290 110 260 96 40 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortPrm 2
4 IlvPolyline 11
142 52 158 68 150 60 158 52 142 68 150 60 158 60 142 60 150 60 150 52
150 68 
2 IlvMessageLabel 136 65 56 28 F8 0 25 16 4 "Incre"   0
[Port
0 1 3 Incre 150 60 136 65 56 28 2  2 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
EOF
