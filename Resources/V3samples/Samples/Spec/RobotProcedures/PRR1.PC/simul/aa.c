/* sscc : C CODE OF SORTED EQUATIONS aa - SIMULATION MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define CSIMUL_H_LOADED
typedef struct {char text[STRLEN];} symbolic;
extern void _boolean(boolean*, boolean);
extern boolean _eq_boolean(boolean, boolean);
extern boolean _ne_boolean(boolean, boolean);
extern boolean _cond_boolean(boolean ,boolean ,boolean);
extern char* _boolean_to_text(boolean);
extern int _check_boolean(char*);
extern void _text_to_boolean(boolean*, char*);
extern void _integer(integer*, integer);
extern boolean _eq_integer(integer, integer);
extern boolean _ne_integer(integer, integer);
extern integer _cond_integer(boolean ,integer ,integer);
extern char* _integer_to_text(integer);
extern int _check_integer(char*);
extern void _text_to_integer(integer*, char*);
extern void _string(string, string);
extern boolean _eq_string(string, string);
extern boolean _ne_string(string, string);
extern string _cond_string(boolean ,string ,string);
extern char* _string_to_text(string);
extern int _check_string(char*);
extern void _text_to_string(string, char*);
extern void _float(float*, float);
extern boolean _eq_float(float, float);
extern boolean _ne_float(float, float);
extern float _cond_float(boolean ,float ,float);
extern char* _float_to_text(float);
extern int _check_float(char*);
extern void _text_to_float(float*, char*);
extern void _double(double*, double);
extern boolean _eq_double(double, double);
extern boolean _ne_double(double, double);
extern double _cond_double(boolean ,double ,double);
extern char* _double_to_text(double);
extern int _check_double(char*);
extern void _text_to_double(double*, char*);
extern void _symbolic(symbolic*, symbolic);
extern boolean _eq_symbolic(symbolic, symbolic);
extern boolean _ne_symbolic(symbolic, symbolic);
extern symbolic _cond_symbolic(boolean ,symbolic ,symbolic);
extern char* _symbolic_to_text(symbolic);
extern int _check_symbolic(char*);
extern void _text_to_symbolic(symbolic*, char*);
extern char* __PredefinedTypeToText(int, char*);
#define _true 1
#define _false 0
#define __aa_GENERIC_TEST(TEST) return TEST;
typedef void (*__aa_APF)();
static __aa_APF *__aa_PActionArray;
static int **__aa_PCheckArray;
struct __SourcePoint {
int linkback;
int line;
int column;
int instance_index;
};
struct __InstanceEntry {
char *module_name;
int father_index;
char *dir_name;
char *file_name;
struct __SourcePoint source_point;
struct __SourcePoint end_point;
struct __SourcePoint instance_point;
};
struct __TaskEntry {
char *name;
int   nb_args_ref;
int   nb_args_val;
int  *type_codes_array;
struct __SourcePoint source_point;
};
struct __SignalEntry {
char *name;
int code;
int variable_index;
int present;
struct __SourcePoint source_point;
int number_of_emit_source_points;
struct __SourcePoint* emit_source_point_array;
int number_of_present_source_points;
struct __SourcePoint* present_source_point_array;
int number_of_access_source_points;
struct __SourcePoint* access_source_point_array;
};
struct __InputEntry {
char *name;
int hash;
char *type_name;
int is_a_sensor;
int type_code;
int multiple;
int signal_index;
int (*p_check_input)(char*);
void (*p_input_function)();
int present;
struct __SourcePoint source_point;
};
struct __ReturnEntry {
char *name;
int hash;
char *type_name;
int type_code;
int signal_index;
int exec_index;
int (*p_check_input)(char*);
void (*p_input_function)();
int present;
struct __SourcePoint source_point;
};
struct __ImplicationEntry {
int master;
int slave;
struct __SourcePoint source_point;
};
struct __ExclusionEntry {
int *exclusion_list;
struct __SourcePoint source_point;
};
struct __VariableEntry {
char *full_name;
char *short_name;
char *type_name;
int type_code;
int comment_kind;
int is_initialized;
char *p_variable;
char *source_name;
int written;
unsigned char written_in_transition;
unsigned char read_in_transition;
struct __SourcePoint source_point;
};
struct __ExecEntry {
int task_index;
int *var_indexes_array;
char **p_values_array;
struct __SourcePoint source_point;
};
struct __HaltEntry {
struct __SourcePoint source_point;
};
struct __NetEntry {
int known;
int value;
int number_of_source_points;
struct __SourcePoint* source_point_array;
};
struct __ModuleEntry {
char *version_id;
char *name;
int number_of_instances;
int number_of_tasks;
int number_of_signals;
int number_of_inputs;
int number_of_returns;
int number_of_sensors;
int number_of_outputs;
int number_of_locals;
int number_of_exceptions;
int number_of_implications;
int number_of_exclusions;
int number_of_variables;
int number_of_execs;
int number_of_halts;
int number_of_nets;
int number_of_states;
int state;
unsigned short *halt_list;
unsigned short *awaited_list;
unsigned short *emitted_list;
unsigned short *started_list;
unsigned short *killed_list;
unsigned short *suspended_list;
unsigned short *active_list;
int run_time_error_code;
int error_info;
void (*init)();
int (*run)();
int (*reset)();
char *(*show_variable)(int);
void (*set_variable)(int, char*, char*);
int (*check_value)(int, char*);
int (*execute_action)();
struct __InstanceEntry* instance_table;
struct __TaskEntry* task_table;
struct __SignalEntry* signal_table;
struct __InputEntry* input_table;
struct __ReturnEntry* return_table;
struct __ImplicationEntry* implication_table;
struct __ExclusionEntry* exclusion_table;
struct __VariableEntry* variable_table;
struct __ExecEntry* exec_table;
struct __HaltEntry* halt_table;
struct __NetEntry* net_table;
};

#include "aa.h"

/* EXTERN DECLARATIONS */

#ifndef _orcStrlPtr_to_text
extern char* _orcStrlPtr_to_text(orcStrlPtr);
#endif
#ifndef _check_orcStrlPtr
extern int _check_orcStrlPtr(char*);
#endif
#ifndef _text_to_orcStrlPtr
extern void _text_to_orcStrlPtr(orcStrlPtr*, char*);
#endif
extern int __CheckVariables(int*);
extern void __ResetInput();
extern void __ResetExecs();
extern void __ResetVariables();
extern void __ResetVariableStatus();
extern void __AppendToList(unsigned short*, unsigned short);
extern void __ListCopy(unsigned short*, unsigned short**);
extern void __WriteVariable(int);
extern void __ResetVariable(int);
extern void __ResetModuleEntry();
extern void __ResetModuleEntryBeforeReaction();
extern void __ResetModuleEntryAfterReaction();
#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _TR1_controler_DEFINED
#ifndef TR1_controler
extern void TR1_controler(orcStrlPtr);
#endif
#endif
#ifndef _TR1_fileparameter_DEFINED
#ifndef TR1_fileparameter
extern void TR1_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _TR1_parameter_DEFINED
#ifndef TR1_parameter
extern void TR1_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static orcStrlPtr __aa_V0;
static boolean __aa_V1;
static boolean __aa_V2;
static boolean __aa_V3;
static boolean __aa_V4;
static boolean __aa_V5;
static boolean __aa_V6;
static orcStrlPtr __aa_V7;
static boolean __aa_V8;
static boolean __aa_V9;
static boolean __aa_V10;
static boolean __aa_V11;
static boolean __aa_V12;
static boolean __aa_V13;
static boolean __aa_V14;
static boolean __aa_V15;
static orcStrlPtr __aa_V16;
static orcStrlPtr __aa_V17;
static orcStrlPtr __aa_V18;
static orcStrlPtr __aa_V19;
static orcStrlPtr __aa_V20;
static orcStrlPtr __aa_V21;
static orcStrlPtr __aa_V22;
static orcStrlPtr __aa_V23;
static boolean __aa_V24;

static unsigned short __aa_HaltList[18];
static unsigned short __aa_AwaitedList[56];
static unsigned short __aa_EmittedList[56];
static unsigned short __aa_StartedList[1];
static unsigned short __aa_KilledList[1];
static unsigned short __aa_SuspendedList[1];
static unsigned short __aa_ActiveList[1];
static unsigned short __aa_AllAwaitedList[56]={14,0,1,2,3,4,5,6,7,8,9,10,11,12,13};


/* INPUT FUNCTIONS */

void aa_I_Prr_Start (orcStrlPtr __V) {
__WriteVariable(0);
_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
void aa_IS_Prr_Start (string __V) {
__WriteVariable(0);
_text_to_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
void aa_I_USERSTART_TR1 () {
__aa_V2 = _true;
}
void aa_IS_USERSTART_TR1 () {
__aa_V2 = _true;
}
void aa_I_Stop () {
__aa_V3 = _true;
}
void aa_IS_Stop () {
__aa_V3 = _true;
}
void aa_I_ReadyToStop_RobPrint () {
__aa_V4 = _true;
}
void aa_IS_ReadyToStop_RobPrint () {
__aa_V4 = _true;
}
void aa_I_KillOK_RobPrint () {
__aa_V5 = _true;
}
void aa_IS_KillOK_RobPrint () {
__aa_V5 = _true;
}
void aa_I_InitOK_RobPrint () {
__aa_V6 = _true;
}
void aa_IS_InitOK_RobPrint () {
__aa_V6 = _true;
}
void aa_I_TR1_Start (orcStrlPtr __V) {
__WriteVariable(7);
_orcStrlPtr(&__aa_V7,__V);
__aa_V8 = _true;
}
void aa_IS_TR1_Start (string __V) {
__WriteVariable(7);
_text_to_orcStrlPtr(&__aa_V7,__V);
__aa_V8 = _true;
}
void aa_I_ActivateOK_RobPrint () {
__aa_V9 = _true;
}
void aa_IS_ActivateOK_RobPrint () {
__aa_V9 = _true;
}
void aa_I_CmdStopOK_RobPrint () {
__aa_V10 = _true;
}
void aa_IS_CmdStopOK_RobPrint () {
__aa_V10 = _true;
}
void aa_I_T2_TR1 () {
__aa_V11 = _true;
}
void aa_IS_T2_TR1 () {
__aa_V11 = _true;
}
void aa_I_ROBOT_FAIL () {
__aa_V12 = _true;
}
void aa_IS_ROBOT_FAIL () {
__aa_V12 = _true;
}
void aa_I_SOFT_FAIL () {
__aa_V13 = _true;
}
void aa_IS_SOFT_FAIL () {
__aa_V13 = _true;
}
void aa_I_SENSOR_FAIL () {
__aa_V14 = _true;
}
void aa_IS_SENSOR_FAIL () {
__aa_V14 = _true;
}
void aa_I_CPU_OVERLOAD () {
__aa_V15 = _true;
}
void aa_IS_CPU_OVERLOAD () {
__aa_V15 = _true;
}

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int aa_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

static int __aa_A1 () {
__aa_GENERIC_TEST(__aa_V1);
}
static int __aa_Check1 [] = {1,0,0};
static int __aa_A2 () {
__aa_GENERIC_TEST(__aa_V2);
}
static int __aa_Check2 [] = {1,0,0};
static int __aa_A3 () {
__aa_GENERIC_TEST(__aa_V3);
}
static int __aa_Check3 [] = {1,0,0};
static int __aa_A4 () {
__aa_GENERIC_TEST(__aa_V4);
}
static int __aa_Check4 [] = {1,0,0};
static int __aa_A5 () {
__aa_GENERIC_TEST(__aa_V5);
}
static int __aa_Check5 [] = {1,0,0};
static int __aa_A6 () {
__aa_GENERIC_TEST(__aa_V6);
}
static int __aa_Check6 [] = {1,0,0};
static int __aa_A7 () {
__aa_GENERIC_TEST(__aa_V8);
}
static int __aa_Check7 [] = {1,0,0};
static int __aa_A8 () {
__aa_GENERIC_TEST(__aa_V9);
}
static int __aa_Check8 [] = {1,0,0};
static int __aa_A9 () {
__aa_GENERIC_TEST(__aa_V10);
}
static int __aa_Check9 [] = {1,0,0};
static int __aa_A10 () {
__aa_GENERIC_TEST(__aa_V11);
}
static int __aa_Check10 [] = {1,0,0};
static int __aa_A11 () {
__aa_GENERIC_TEST(__aa_V12);
}
static int __aa_Check11 [] = {1,0,0};
static int __aa_A12 () {
__aa_GENERIC_TEST(__aa_V13);
}
static int __aa_Check12 [] = {1,0,0};
static int __aa_A13 () {
__aa_GENERIC_TEST(__aa_V14);
}
static int __aa_Check13 [] = {1,0,0};
static int __aa_A14 () {
__aa_GENERIC_TEST(__aa_V15);
}
static int __aa_Check14 [] = {1,0,0};

/* OUTPUT ACTIONS */

static void __aa_A15 () {
#ifdef __OUTPUT
aa_O_Activate(__aa_V16);
#endif
__AppendToList(__aa_EmittedList,14);
}
static int __aa_Check15 [] = {1,0,0};
static void __aa_A16 () {
#ifdef __OUTPUT
aa_O_STARTED_PRR1();
#endif
__AppendToList(__aa_EmittedList,15);
}
static int __aa_Check16 [] = {1,0,0};
static void __aa_A17 () {
#ifdef __OUTPUT
aa_O_ActivateTR1_RobPrint(__aa_V17);
#endif
__AppendToList(__aa_EmittedList,16);
}
static int __aa_Check17 [] = {1,0,0};
static void __aa_A18 () {
#ifdef __OUTPUT
aa_O_TR1Transite(__aa_V18);
#endif
__AppendToList(__aa_EmittedList,17);
}
static int __aa_Check18 [] = {1,0,0};
static void __aa_A19 () {
#ifdef __OUTPUT
aa_O_Abort_TR1(__aa_V19);
#endif
__AppendToList(__aa_EmittedList,18);
}
static int __aa_Check19 [] = {1,0,0};
static void __aa_A20 () {
#ifdef __OUTPUT
aa_O_STARTED_TR1();
#endif
__AppendToList(__aa_EmittedList,19);
}
static int __aa_Check20 [] = {1,0,0};
static void __aa_A21 () {
#ifdef __OUTPUT
aa_O_GoodEnd_TR1();
#endif
__AppendToList(__aa_EmittedList,20);
}
static int __aa_Check21 [] = {1,0,0};
static void __aa_A22 () {
#ifdef __OUTPUT
aa_O_GoodEnd_PRR1();
#endif
__AppendToList(__aa_EmittedList,21);
}
static int __aa_Check22 [] = {1,0,0};
static void __aa_A23 () {
#ifdef __OUTPUT
aa_O_Abort_PRR1();
#endif
__AppendToList(__aa_EmittedList,22);
}
static int __aa_Check23 [] = {1,0,0};
static void __aa_A24 () {
#ifdef __OUTPUT
aa_O_EndKill(__aa_V20);
#endif
__AppendToList(__aa_EmittedList,23);
}
static int __aa_Check24 [] = {1,0,0};
static void __aa_A25 () {
#ifdef __OUTPUT
aa_O_FinBF(__aa_V21);
#endif
__AppendToList(__aa_EmittedList,24);
}
static int __aa_Check25 [] = {1,0,0};
static void __aa_A26 () {
#ifdef __OUTPUT
aa_O_FinT3(__aa_V22);
#endif
__AppendToList(__aa_EmittedList,25);
}
static int __aa_Check26 [] = {1,0,0};
static void __aa_A27 () {
#ifdef __OUTPUT
aa_O_GoodEndRPr();
#endif
__AppendToList(__aa_EmittedList,26);
}
static int __aa_Check27 [] = {1,0,0};
static void __aa_A28 () {
#ifdef __OUTPUT
aa_O_T3RPr();
#endif
__AppendToList(__aa_EmittedList,27);
}
static int __aa_Check28 [] = {1,0,0};

/* ASSIGNMENTS */

static void __aa_A29 () {
__aa_V1 = _false;
}
static int __aa_Check29 [] = {1,0,1,1};
static void __aa_A30 () {
__aa_V2 = _false;
}
static int __aa_Check30 [] = {1,0,1,2};
static void __aa_A31 () {
__aa_V3 = _false;
}
static int __aa_Check31 [] = {1,0,1,3};
static void __aa_A32 () {
__aa_V4 = _false;
}
static int __aa_Check32 [] = {1,0,1,4};
static void __aa_A33 () {
__aa_V5 = _false;
}
static int __aa_Check33 [] = {1,0,1,5};
static void __aa_A34 () {
__aa_V6 = _false;
}
static int __aa_Check34 [] = {1,0,1,6};
static void __aa_A35 () {
__aa_V8 = _false;
}
static int __aa_Check35 [] = {1,0,1,8};
static void __aa_A36 () {
__aa_V9 = _false;
}
static int __aa_Check36 [] = {1,0,1,9};
static void __aa_A37 () {
__aa_V10 = _false;
}
static int __aa_Check37 [] = {1,0,1,10};
static void __aa_A38 () {
__aa_V11 = _false;
}
static int __aa_Check38 [] = {1,0,1,11};
static void __aa_A39 () {
__aa_V12 = _false;
}
static int __aa_Check39 [] = {1,0,1,12};
static void __aa_A40 () {
__aa_V13 = _false;
}
static int __aa_Check40 [] = {1,0,1,13};
static void __aa_A41 () {
__aa_V14 = _false;
}
static int __aa_Check41 [] = {1,0,1,14};
static void __aa_A42 () {
__aa_V15 = _false;
}
static int __aa_Check42 [] = {1,0,1,15};
static void __aa_A43 () {
_orcStrlPtr(&__aa_V20,__aa_V23);
}
static int __aa_Check43 [] = {1,1,23,1,20};
static void __aa_A44 () {
_orcStrlPtr(&__aa_V21,__aa_V0);
}
static int __aa_Check44 [] = {1,1,0,1,21};
static void __aa_A45 () {
_orcStrlPtr(&__aa_V22,__aa_V0);
}
static int __aa_Check45 [] = {1,1,0,1,22};
static void __aa_A46 () {
_orcStrlPtr(&__aa_V19,__aa_V7);
}
static int __aa_Check46 [] = {1,1,7,1,19};
static void __aa_A47 () {
_orcStrlPtr(&__aa_V17,__aa_V7);
}
static int __aa_Check47 [] = {1,1,7,1,17};
static void __aa_A48 () {
_orcStrlPtr(&__aa_V23,__aa_V7);
}
static int __aa_Check48 [] = {1,1,7,1,23};
static void __aa_A49 () {
_orcStrlPtr(&__aa_V18,__aa_V7);
}
static int __aa_Check49 [] = {1,1,7,1,18};
static void __aa_A50 () {
_orcStrlPtr(&__aa_V18,__aa_V7);
}
static int __aa_Check50 [] = {1,1,7,1,18};
static void __aa_A51 () {
_orcStrlPtr(&__aa_V18,__aa_V7);
}
static int __aa_Check51 [] = {1,1,7,1,18};
static void __aa_A52 () {
_orcStrlPtr(&__aa_V18,__aa_V7);
}
static int __aa_Check52 [] = {1,1,7,1,18};
static void __aa_A53 () {
_orcStrlPtr(&__aa_V20,__aa_V23);
}
static int __aa_Check53 [] = {1,1,23,1,20};

/* PROCEDURE CALLS */

static void __aa_A54 () {
TR1_parameter(__aa_V7);
}
static int __aa_Check54 [] = {1,1,7,0};
static void __aa_A55 () {
TR1_controler(__aa_V7);
}
static int __aa_Check55 [] = {1,1,7,0};

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

static void __aa_A56 () {
;
__ResetVariable(0);
}
static int __aa_Check56 [] = {1,0,0};
static void __aa_A57 () {
;
__ResetVariable(7);
}
static int __aa_Check57 [] = {1,0,0};
static void __aa_A58 () {
;
__ResetVariable(16);
}
static int __aa_Check58 [] = {1,0,0};
static void __aa_A59 () {
;
__ResetVariable(17);
}
static int __aa_Check59 [] = {1,0,0};
static void __aa_A60 () {
;
__ResetVariable(18);
}
static int __aa_Check60 [] = {1,0,0};
static void __aa_A61 () {
;
__ResetVariable(19);
}
static int __aa_Check61 [] = {1,0,0};
static void __aa_A62 () {
;
__ResetVariable(20);
}
static int __aa_Check62 [] = {1,0,0};
static void __aa_A63 () {
;
__ResetVariable(21);
}
static int __aa_Check63 [] = {1,0,0};
static void __aa_A64 () {
;
__ResetVariable(22);
}
static int __aa_Check64 [] = {1,0,0};
static void __aa_A65 () {
;
__ResetVariable(23);
}
static int __aa_Check65 [] = {1,0,0};
static void __aa_A66 () {
;
__ResetVariable(24);
}
static int __aa_Check66 [] = {1,0,0};

/* ACTION SEQUENCES */


static int *__aa_CheckArray[] = {
0,
__aa_Check1,
__aa_Check2,
__aa_Check3,
__aa_Check4,
__aa_Check5,
__aa_Check6,
__aa_Check7,
__aa_Check8,
__aa_Check9,
__aa_Check10,
__aa_Check11,
__aa_Check12,
__aa_Check13,
__aa_Check14,
__aa_Check15,
__aa_Check16,
__aa_Check17,
__aa_Check18,
__aa_Check19,
__aa_Check20,
__aa_Check21,
__aa_Check22,
__aa_Check23,
__aa_Check24,
__aa_Check25,
__aa_Check26,
__aa_Check27,
__aa_Check28,
__aa_Check29,
__aa_Check30,
__aa_Check31,
__aa_Check32,
__aa_Check33,
__aa_Check34,
__aa_Check35,
__aa_Check36,
__aa_Check37,
__aa_Check38,
__aa_Check39,
__aa_Check40,
__aa_Check41,
__aa_Check42,
__aa_Check43,
__aa_Check44,
__aa_Check45,
__aa_Check46,
__aa_Check47,
__aa_Check48,
__aa_Check49,
__aa_Check50,
__aa_Check51,
__aa_Check52,
__aa_Check53,
__aa_Check54,
__aa_Check55,
__aa_Check56,
__aa_Check57,
__aa_Check58,
__aa_Check59,
__aa_Check60,
__aa_Check61,
__aa_Check62,
__aa_Check63,
__aa_Check64,
__aa_Check65,
__aa_Check66
};
static int **__aa_PCheckArray =  __aa_CheckArray;

/* INIT FUNCTION */

#ifndef NO_INIT
void aa_initialize () {
}
#endif

/* SHOW VARIABLE FUNCTION */

char* __aa_show_variable (int __V) {
extern struct __VariableEntry __aa_VariableTable[];
struct __VariableEntry* p_var = &__aa_VariableTable[__V];
if (p_var->type_code < 0) {return __PredefinedTypeToText(p_var->type_code, p_var->p_variable);
} else {
switch (p_var->type_code) {
case 0: return _orcStrlPtr_to_text(*(orcStrlPtr*)p_var->p_variable);
default: return 0;
}
}
}

/* SET VARIABLE FUNCTION */

static void __aa_set_variable(int __Type, char* __pVar, char* __Text) {
}

/* CHECK VALUE FUNCTION */

static int __aa_check_value (int __Type, char* __Text) {
return 0;
}

/* SIMULATION TABLES */

struct __InstanceEntry __aa_InstanceTable [] = {
{"aa",0,"/local_home/pissard/Workspace/orccad/orccadV3/Samples/Spec/RobotProcedures/PRR1.PC/common/","aa.strl",{1,1,1,0},{1,98,1,0},{0,0,0,0}},
{"RTs_Init",0,"/local_home/pissard/Workspace/orccad/orccadV3/Samples/Spec/RobotProcedures/PRR1.PC/common/","aa.strl",{1,122,1,1},{1,136,1,1},{1,69,1,0}},
{"TR1",0,"/local_home/pissard/Workspace/orccad/orccadV3/Samples/Spec/RobotProcedures/PRR1.PC/common/","TR1.strl",{1,1,1,2},{1,94,1,2},{1,71,1,0}},
{"PRR1",0,"/local_home/pissard/Workspace/orccad/orccadV3/Samples/Spec/RobotProcedures/PRR1.PC/common/","PRR1.strl",{1,1,1,3},{1,77,1,3},{1,73,1,0}},
{"TransiteRobot",0,"/local_home/pissard/Workspace/orccad/orccadV3/Samples/Spec/RobotProcedures/PRR1.PC/common/","aa.strl",{1,101,1,4},{1,119,1,4},{1,75,1,0}},
};

struct __SignalEntry __aa_SignalTable [] = {
{"Prr_Start",1,0,0,{1,4,7,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"USERSTART_TR1",33,0,0,{1,5,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Stop",33,0,0,{1,6,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ReadyToStop_RobPrint",33,0,0,{1,7,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"KillOK_RobPrint",33,0,0,{1,8,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"InitOK_RobPrint",33,0,0,{1,9,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"TR1_Start",1,7,0,{1,10,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ActivateOK_RobPrint",33,0,0,{1,11,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"CmdStopOK_RobPrint",33,0,0,{1,12,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2_TR1",33,0,0,{1,13,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ROBOT_FAIL",33,0,0,{1,14,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"SOFT_FAIL",33,0,0,{1,15,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"SENSOR_FAIL",33,0,0,{1,16,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"CPU_OVERLOAD",33,0,0,{1,17,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Activate",2,16,0,{1,19,8,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"STARTED_PRR1",34,0,0,{1,19,33,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ActivateTR1_RobPrint",2,17,0,{1,20,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"TR1Transite",2,18,0,{1,21,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_TR1",2,19,0,{1,22,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"STARTED_TR1",34,0,0,{1,23,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEnd_TR1",34,0,0,{1,24,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEnd_PRR1",34,0,0,{1,25,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_PRR1",34,0,0,{1,26,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"EndKill",2,20,0,{1,27,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FinBF",2,21,0,{1,28,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FinT3",2,22,0,{1,29,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEndRPr",34,0,0,{1,30,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3RPr",34,0,0,{1,31,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Prev_rt_RobPrint",8,23,0,{1,48,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"StartTransite_RobPrint",40,0,0,{1,49,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FinTransite_RobPrint",40,0,0,{1,50,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FirstRT_RobPrint",40,0,0,{1,51,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Reparam_RobPrint",8,24,0,{1,52,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_Local_TR1",40,0,0,{1,53,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_Local_PRR1",40,0,0,{1,54,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"START_TR1",40,0,0,{1,55,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"START_PRR1",40,0,0,{1,56,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ReadyToStart_RobPrint",40,0,0,{1,57,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_TR1",40,0,0,{1,58,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_TR1",40,0,0,{1,59,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_PRR1",40,0,0,{1,60,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_PRR1",40,0,0,{1,61,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2",40,0,0,{1,62,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_controle_prr",40,0,0,{1,63,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_controle_prr",40,0,0,{1,64,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_prr",48,0,0,{1,66,6,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_prr",48,0,0,{1,67,6,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3",48,0,0,{1,32,6,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort",48,0,0,{1,36,8,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2",48,0,0,{1,57,8,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF",48,0,0,{1,58,9,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"dummylocalsignal",40,0,0,{1,26,9,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort",48,0,0,{1,32,6,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_controle_PRR1",48,0,0,{1,33,7,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_controle_PRR1",48,0,0,{1,34,8,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL}};

struct __InputEntry __aa_InputTable [] = {
{"Prr_Start",20,"orcStrlPtr",0,0,0,0,_check_orcStrlPtr,aa_IS_Prr_Start,0,{1,4,7,0}},
{"USERSTART_TR1",17,0,0,-1,0,1,0,aa_IS_USERSTART_TR1,0,{1,5,4,0}},
{"Stop",18,0,0,-1,0,2,0,aa_IS_Stop,0,{1,6,4,0}},
{"ReadyToStop_RobPrint",9,0,0,-1,0,3,0,aa_IS_ReadyToStop_RobPrint,0,{1,7,4,0}},
{"KillOK_RobPrint",47,0,0,-1,0,4,0,aa_IS_KillOK_RobPrint,0,{1,8,4,0}},
{"InitOK_RobPrint",55,0,0,-1,0,5,0,aa_IS_InitOK_RobPrint,0,{1,9,4,0}},
{"TR1_Start",28,"orcStrlPtr",0,0,0,6,_check_orcStrlPtr,aa_IS_TR1_Start,0,{1,10,4,0}},
{"ActivateOK_RobPrint",64,0,0,-1,0,7,0,aa_IS_ActivateOK_RobPrint,0,{1,11,4,0}},
{"CmdStopOK_RobPrint",46,0,0,-1,0,8,0,aa_IS_CmdStopOK_RobPrint,0,{1,12,4,0}},
{"T2_TR1",40,0,0,-1,0,9,0,aa_IS_T2_TR1,0,{1,13,4,0}},
{"ROBOT_FAIL",62,0,0,-1,0,10,0,aa_IS_ROBOT_FAIL,0,{1,14,4,0}},
{"SOFT_FAIL",89,0,0,-1,0,11,0,aa_IS_SOFT_FAIL,0,{1,15,4,0}},
{"SENSOR_FAIL",45,0,0,-1,0,12,0,aa_IS_SENSOR_FAIL,0,{1,16,4,0}},
{"CPU_OVERLOAD",22,0,0,-1,0,13,0,aa_IS_CPU_OVERLOAD,0,{1,17,4,0}}};

static int __aa_ExclusionEntry0[] = {12,2,3,4,6,5,7,8,9,10,11,12,13};

struct __ExclusionEntry __aa_ExclusionTable [] = {
{__aa_ExclusionEntry0,{1,34,4,0}}
};

struct __VariableEntry __aa_VariableTable [] = {
{"__aa_V0","V0","orcStrlPtr",0,1,0,(char*)&__aa_V0,"Prr_Start",0,0,0,{1,4,7,0}},
{"__aa_V1","V1","boolean",-2,2,0,(char*)&__aa_V1,"Prr_Start",0,0,0,{1,4,7,0}},
{"__aa_V2","V2","boolean",-2,2,0,(char*)&__aa_V2,"USERSTART_TR1",0,0,0,{1,5,4,0}},
{"__aa_V3","V3","boolean",-2,2,0,(char*)&__aa_V3,"Stop",0,0,0,{1,6,4,0}},
{"__aa_V4","V4","boolean",-2,2,0,(char*)&__aa_V4,"ReadyToStop_RobPrint",0,0,0,{1,7,4,0}},
{"__aa_V5","V5","boolean",-2,2,0,(char*)&__aa_V5,"KillOK_RobPrint",0,0,0,{1,8,4,0}},
{"__aa_V6","V6","boolean",-2,2,0,(char*)&__aa_V6,"InitOK_RobPrint",0,0,0,{1,9,4,0}},
{"__aa_V7","V7","orcStrlPtr",0,1,0,(char*)&__aa_V7,"TR1_Start",0,0,0,{1,10,4,0}},
{"__aa_V8","V8","boolean",-2,2,0,(char*)&__aa_V8,"TR1_Start",0,0,0,{1,10,4,0}},
{"__aa_V9","V9","boolean",-2,2,0,(char*)&__aa_V9,"ActivateOK_RobPrint",0,0,0,{1,11,4,0}},
{"__aa_V10","V10","boolean",-2,2,0,(char*)&__aa_V10,"CmdStopOK_RobPrint",0,0,0,{1,12,4,0}},
{"__aa_V11","V11","boolean",-2,2,0,(char*)&__aa_V11,"T2_TR1",0,0,0,{1,13,4,0}},
{"__aa_V12","V12","boolean",-2,2,0,(char*)&__aa_V12,"ROBOT_FAIL",0,0,0,{1,14,4,0}},
{"__aa_V13","V13","boolean",-2,2,0,(char*)&__aa_V13,"SOFT_FAIL",0,0,0,{1,15,4,0}},
{"__aa_V14","V14","boolean",-2,2,0,(char*)&__aa_V14,"SENSOR_FAIL",0,0,0,{1,16,4,0}},
{"__aa_V15","V15","boolean",-2,2,0,(char*)&__aa_V15,"CPU_OVERLOAD",0,0,0,{1,17,4,0}},
{"__aa_V16","V16","orcStrlPtr",0,1,0,(char*)&__aa_V16,"Activate",0,0,0,{1,19,8,0}},
{"__aa_V17","V17","orcStrlPtr",0,1,0,(char*)&__aa_V17,"ActivateTR1_RobPrint",0,0,0,{1,20,4,0}},
{"__aa_V18","V18","orcStrlPtr",0,1,0,(char*)&__aa_V18,"TR1Transite",0,0,0,{1,21,4,0}},
{"__aa_V19","V19","orcStrlPtr",0,1,0,(char*)&__aa_V19,"Abort_TR1",0,0,0,{1,22,4,0}},
{"__aa_V20","V20","orcStrlPtr",0,1,0,(char*)&__aa_V20,"EndKill",0,0,0,{1,27,4,0}},
{"__aa_V21","V21","orcStrlPtr",0,1,0,(char*)&__aa_V21,"FinBF",0,0,0,{1,28,4,0}},
{"__aa_V22","V22","orcStrlPtr",0,1,0,(char*)&__aa_V22,"FinT3",0,0,0,{1,29,4,0}},
{"__aa_V23","V23","orcStrlPtr",0,1,0,(char*)&__aa_V23,"Prev_rt_RobPrint",0,0,0,{1,48,4,0}},
{"__aa_V24","V24","boolean",-2,1,0,(char*)&__aa_V24,"Reparam_RobPrint",0,0,0,{1,52,4,0}}
};

struct __HaltEntry __aa_HaltTable [] = {
{{1,98,1,0}},
{{1,82,1,0}},
{{1,129,2,1}},
{{1,131,2,1}},
{{1,33,9,2}},
{{1,39,4,2}},
{{1,48,4,2}},
{{1,61,5,2}},
{{1,67,4,2}},
{{1,71,2,2}},
{{1,29,1,3}},
{{1,47,5,3}},
{{1,61,6,3}},
{{1,108,3,4}},
{{1,110,6,4}},
{{1,112,8,4}},
{{1,115,6,4}}
};


static void __aa__reset_input () {
__aa_V1 = _false;
__aa_V2 = _false;
__aa_V3 = _false;
__aa_V4 = _false;
__aa_V5 = _false;
__aa_V6 = _false;
__aa_V8 = _false;
__aa_V9 = _false;
__aa_V10 = _false;
__aa_V11 = _false;
__aa_V12 = _false;
__aa_V13 = _false;
__aa_V14 = _false;
__aa_V15 = _false;
}


/* MODULE DATA FOR SIMULATION */

int aa();
int aa_reset();

static struct __ModuleEntry __aa_ModuleData = {
"Simulation interface release 5","aa",
5,0,55,14,0,0,14,18,9,0,1,25,0,17,0,0,0,
__aa_HaltList,
__aa_AwaitedList,
__aa_EmittedList,
__aa_StartedList,
__aa_KilledList,
__aa_SuspendedList,
__aa_ActiveList,
0,0,
aa_initialize,aa,aa_reset,
__aa_show_variable,__aa_set_variable,__aa_check_value,0,
__aa_InstanceTable,
0,
__aa_SignalTable,__aa_InputTable,0,
0,__aa_ExclusionTable,
__aa_VariableTable,
0,
__aa_HaltTable,
0};

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __aa_R[17] = {_true,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int aa () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[40];

__aa_ModuleData.awaited_list = __aa_AwaitedList;
__ResetModuleEntryBeforeReaction();
if (!(_true)) {
__CheckVariables(__aa_CheckArray[15]);__aa_A15();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A15\n");
#endif
}
E[0] = __aa_R[0]&&!((__CheckVariables(__aa_CheckArray[1]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1()));
if (E[0]) {
__CheckVariables(__aa_CheckArray[56]);__aa_A56();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A56\n");
#endif
}
E[1] = __aa_R[0]&&!((__CheckVariables(__aa_CheckArray[7]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7()));
if (E[1]) {
__CheckVariables(__aa_CheckArray[57]);__aa_A57();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A57\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[58]);__aa_A58();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A58\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[59]);__aa_A59();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A59\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[60]);__aa_A60();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A60\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[61]);__aa_A61();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A61\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[62]);__aa_A62();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A62\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[63]);__aa_A63();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A63\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[64]);__aa_A64();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A64\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[65]);__aa_A65();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A65\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[66]);__aa_A66();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A66\n");
#endif
}
E[2] = __aa_R[2]&&!(__aa_R[0]);
E[3] = E[2]&&(__CheckVariables(__aa_CheckArray[7]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7());
E[4] = __aa_R[3]&&!(__aa_R[0]);
E[5] = E[4]&&(__CheckVariables(__aa_CheckArray[1]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1());
E[6] = __aa_R[2]||__aa_R[3];
E[7] = (E[6]&&!(__aa_R[2]))||E[3];
E[8] = (E[6]&&!(__aa_R[3]))||E[5];
E[5] = (E[3]||E[5])&&E[7]&&E[8];
if (E[5]) {
__AppendToList(__aa_EmittedList,31);
__AppendToList(__aa_EmittedList,36);
}
E[3] = __aa_R[10]&&!(__aa_R[0]);
E[9] = (__aa_R[0]&&E[5])||(E[3]&&E[5]);
if (E[9]) {
__AppendToList(__aa_EmittedList,15);
__AppendToList(__aa_EmittedList,35);
}
if (E[9]) {
__CheckVariables(__aa_CheckArray[16]);__aa_A16();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A16\n");
#endif
}
E[10] = __aa_R[5]&&__aa_R[5]&&!(__aa_R[0]);
E[11] = E[10]&&(__CheckVariables(__aa_CheckArray[6]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__aa_A6());
if (E[11]) {
__AppendToList(__aa_EmittedList,37);
}
E[12] = __aa_R[13]&&!(__aa_R[0]);
E[13] = E[12]&&E[11];
E[14] = __aa_R[16]&&!(__aa_R[0]);
E[15] = E[14]&&(__CheckVariables(__aa_CheckArray[9]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9());
E[16] = E[13]||E[15];
if (E[16]) {
__AppendToList(__aa_EmittedList,30);
}
E[17] = __aa_R[6]&&__aa_R[6]&&!(__aa_R[0]);
E[18] = (E[11]&&E[16])||(E[17]&&E[16]);
if (E[18]) {
__AppendToList(__aa_EmittedList,16);
__AppendToList(__aa_EmittedList,28);
}
if (E[18]) {
__CheckVariables(__aa_CheckArray[47]);__aa_A47();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A47\n");
#endif
}
if (E[18]) {
__CheckVariables(__aa_CheckArray[17]);__aa_A17();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A17\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[50]);__aa_A50();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A50\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[49]);__aa_A49();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A49\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[18]);__aa_A18();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A18\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[46]);__aa_A46();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A46\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[19]);__aa_A19();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A19\n");
#endif
}
E[19] = __aa_R[7]||__aa_R[8]||__aa_R[9];
if (E[18]) {
__CheckVariables(__aa_CheckArray[48]);__aa_A48();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A48\n");
#endif
}
E[20] = E[18]||(__aa_R[7]&&__aa_R[7]&&!(__aa_R[0]));
E[21] = (E[19]&&!(__aa_R[7]))||E[20];
E[22] = __aa_R[8]&&!(__aa_R[0]);
E[23] = E[22]&&!((__CheckVariables(__aa_CheckArray[3]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3()));
E[23] = E[18]||(__aa_R[8]&&E[23]);
E[24] = (E[19]&&!(__aa_R[8]))||E[23];
E[25] = E[18]||(__aa_R[9]&&!(__aa_R[0]));
E[26] = (E[19]&&!(__aa_R[9]))||E[25];
if (E[9]) {
__CheckVariables(__aa_CheckArray[54]);__aa_A54();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A54\n");
#endif
}
if (E[9]) {
__CheckVariables(__aa_CheckArray[55]);__aa_A55();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A55\n");
#endif
}
E[27] = __aa_R[4]&&!(__aa_R[0]);
E[28] = (__aa_R[0]&&E[9])||(E[27]&&E[9]);
if (E[28]) {
__AppendToList(__aa_EmittedList,19);
}
if (E[28]) {
__CheckVariables(__aa_CheckArray[20]);__aa_A20();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A20\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[21]);__aa_A21();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A21\n");
#endif
}
E[29] = __aa_R[11]&&!(__aa_R[0]);
E[30] = __aa_R[11]||__aa_R[12];
E[22] = E[22]&&(__CheckVariables(__aa_CheckArray[3]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3());
if (E[22]) {
__AppendToList(__aa_EmittedList,47);
}
E[22] = E[22]&&E[21]&&(E[24]||E[22])&&E[26];
if (E[22]) {
__AppendToList(__aa_EmittedList,39);
}
E[31] = E[9]||(__aa_R[11]&&E[29]&&!(E[22]));
E[32] = (E[30]&&!(__aa_R[11]))||E[31];
E[33] = E[9]||(__aa_R[12]&&__aa_R[12]&&!(__aa_R[0]));
E[34] = (E[30]&&!(__aa_R[12]))||E[33];
if (!(_true)) {
__CheckVariables(__aa_CheckArray[22]);__aa_A22();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A22\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[23]);__aa_A23();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A23\n");
#endif
}
E[35] = __aa_R[15]&&!(__aa_R[0]);
E[36] = E[35]&&E[11];
if (E[36]) {
__AppendToList(__aa_EmittedList,23);
}
if (E[36]) {
__CheckVariables(__aa_CheckArray[53]);__aa_A53();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A53\n");
#endif
}
E[37] = __aa_R[1]&&!(__aa_R[0]);
E[19] = __aa_R[5]||E[19]||__aa_R[6]||__aa_R[4];
E[30] = E[30]||__aa_R[10];
E[38] = __aa_R[13]||__aa_R[14]||__aa_R[15]||__aa_R[16];
E[39] = E[6]||E[19]||E[30]||E[38]||__aa_R[1];
E[2] = E[2]&&!((__CheckVariables(__aa_CheckArray[7]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7()));
E[2] = __aa_R[0]||(__aa_R[2]&&E[2]);
E[4] = E[4]&&!((__CheckVariables(__aa_CheckArray[1]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1()));
E[4] = __aa_R[0]||(__aa_R[3]&&E[4]);
E[8] = (E[2]||E[4])&&(E[7]||E[2])&&(E[8]||E[4]);
E[6] = (E[39]&&!(E[6]))||E[5]||E[8];
E[10] = E[10]&&!((__CheckVariables(__aa_CheckArray[6]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__aa_A6()));
E[10] = __aa_R[5]&&E[10];
E[16] = (E[11]&&!(E[16]))||(__aa_R[6]&&E[17]&&!(E[16]));
E[27] = (__aa_R[0]&&!(E[9]))||(__aa_R[4]&&E[27]&&!(E[9]));
E[26] = E[28]||E[10]||((E[20]||E[23]||E[25])&&E[21]&&E[24]&&E[26])||E[16]||E[27];
E[19] = (E[39]&&!(E[19]))||E[22]||E[26];
E[29] = E[29]&&E[22];
if (E[29]) {
__AppendToList(__aa_EmittedList,53);
}
E[29] = E[29]&&(E[32]||E[29])&&E[34];
if (E[29]) {
__AppendToList(__aa_EmittedList,41);
}
E[5] = (__aa_R[0]&&!(E[5]))||(__aa_R[10]&&E[3]&&!(E[5]));
E[34] = ((E[31]||E[33])&&E[32]&&E[34])||E[5];
E[30] = (E[39]&&!(E[30]))||E[29]||E[34];
E[12] = __aa_R[0]||(__aa_R[13]&&E[12]&&!(E[11]));
E[15] = E[13]||E[15]||(__aa_R[14]&&__aa_R[14]&&!(__aa_R[0]));
E[11] = __aa_R[15]&&E[35]&&!(E[11]);
E[14] = E[14]&&!((__CheckVariables(__aa_CheckArray[9]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9()));
E[14] = E[36]||(__aa_R[16]&&E[14]);
E[38] = (E[39]&&!(E[38]))||E[12]||E[15]||E[11]||E[14];
E[35] = E[37]&&!(E[29]);
E[13] = E[35]&&!((__CheckVariables(__aa_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11()));
E[32] = E[13]&&!((__CheckVariables(__aa_CheckArray[12]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12()));
E[3] = E[32]&&!((__CheckVariables(__aa_CheckArray[13]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13()));
E[24] = E[3]&&!((__CheckVariables(__aa_CheckArray[14]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14()));
E[24] = __aa_R[0]||(__aa_R[1]&&E[24]);
E[39] = (E[39]&&!(__aa_R[1]))||E[24];
if (!(_true)) {
__CheckVariables(__aa_CheckArray[43]);__aa_A43();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A43\n");
#endif
}
if (E[36]) {
__CheckVariables(__aa_CheckArray[24]);__aa_A24();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A24\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[44]);__aa_A44();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A44\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[25]);__aa_A25();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A25\n");
#endif
}
E[37] = E[37]&&E[29];
E[35] = E[35]&&(__CheckVariables(__aa_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11());
E[13] = E[13]&&(__CheckVariables(__aa_CheckArray[12]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12());
E[32] = E[32]&&(__CheckVariables(__aa_CheckArray[13]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13());
E[3] = E[3]&&(__CheckVariables(__aa_CheckArray[14]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14());
E[21] = (E[37]||E[35]||E[13]||E[32]||E[3])&&E[6]&&E[19]&&E[30]&&E[38]&&(E[39]||E[37]||E[35]||E[13]||E[32]||E[3]);
if (E[21]) {
__AppendToList(__aa_EmittedList,25);
__AppendToList(__aa_EmittedList,27);
}
if (E[21]) {
__CheckVariables(__aa_CheckArray[45]);__aa_A45();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A45\n");
#endif
}
if (E[21]) {
__CheckVariables(__aa_CheckArray[26]);__aa_A26();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A26\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[27]);__aa_A27();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A27\n");
#endif
}
if (E[21]) {
__CheckVariables(__aa_CheckArray[28]);__aa_A28();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A28\n");
#endif
}
E[3] = E[37]||E[35]||E[13]||E[32]||E[3];
if (E[3]) {
__AppendToList(__aa_EmittedList,45);
}
E[32] = E[21];
E[39] = (E[8]||E[26]||E[34]||E[12]||E[15]||E[11]||E[14]||E[24])&&E[6]&&E[19]&&E[30]&&E[38]&&E[39];
__aa_R[1] = E[24]&&!(E[21]);
__aa_R[2] = E[2]&&!(E[21]);
__aa_R[3] = E[4]&&!(E[21]);
__aa_R[4] = E[27]&&!(E[21]);
E[27] = E[22]||E[21];
__aa_R[5] = (E[28]&&!(E[21]))||(E[10]&&!(E[27]));
__aa_R[6] = E[16]&&!(E[27]);
E[22] = E[27]||E[22]||E[22]||E[22];
__aa_R[7] = E[20]&&!(E[22]);
__aa_R[8] = E[23]&&!(E[22]);
__aa_R[9] = E[25]&&!(E[22]);
__aa_R[10] = E[5]&&!(E[21]);
E[29] = E[21]||E[29]||E[29];
__aa_R[11] = E[31]&&!(E[29]);
__aa_R[12] = E[33]&&!(E[29]);
__aa_R[13] = E[12]&&!(E[21]);
__aa_R[14] = E[15]&&!(E[21]);
__aa_R[15] = E[11]&&!(E[21]);
__aa_R[16] = E[14]&&!(E[21]);
__aa_R[0] = !(_true);
if (__aa_R[1]) { __AppendToList(__aa_HaltList,1); }
if (__aa_R[2]) { __AppendToList(__aa_HaltList,2); }
if (__aa_R[3]) { __AppendToList(__aa_HaltList,3); }
if (__aa_R[4]) { __AppendToList(__aa_HaltList,4); }
if (__aa_R[5]) { __AppendToList(__aa_HaltList,5); }
if (__aa_R[6]) { __AppendToList(__aa_HaltList,6); }
if (__aa_R[7]) { __AppendToList(__aa_HaltList,7); }
if (__aa_R[8]) { __AppendToList(__aa_HaltList,8); }
if (__aa_R[9]) { __AppendToList(__aa_HaltList,9); }
if (__aa_R[10]) { __AppendToList(__aa_HaltList,10); }
if (__aa_R[11]) { __AppendToList(__aa_HaltList,11); }
if (__aa_R[12]) { __AppendToList(__aa_HaltList,12); }
if (__aa_R[13]) { __AppendToList(__aa_HaltList,13); }
if (__aa_R[14]) { __AppendToList(__aa_HaltList,14); }
if (__aa_R[15]) { __AppendToList(__aa_HaltList,15); }
if (__aa_R[16]) { __AppendToList(__aa_HaltList,16); }
if (!E[39]) { __AppendToList(__aa_HaltList,0); }
__ResetModuleEntryAfterReaction();
__aa_ModuleData.awaited_list = __aa_AllAwaitedList;
__aa__reset_input();
return E[39];
}

/* AUTOMATON RESET */

int aa_reset () {
__aa_ModuleData.awaited_list = __aa_AwaitedList;
__ResetModuleEntry();
__aa_ModuleData.awaited_list = __aa_AllAwaitedList;
__aa_ModuleData.state = 0;
__aa_R[0] = _true;
__aa_R[1] = _false;
__aa_R[2] = _false;
__aa_R[3] = _false;
__aa_R[4] = _false;
__aa_R[5] = _false;
__aa_R[6] = _false;
__aa_R[7] = _false;
__aa_R[8] = _false;
__aa_R[9] = _false;
__aa_R[10] = _false;
__aa_R[11] = _false;
__aa_R[12] = _false;
__aa_R[13] = _false;
__aa_R[14] = _false;
__aa_R[15] = _false;
__aa_R[16] = _false;
__aa__reset_input();
return 0;
}
char* CompilationType = "Compiled Sorted Equations";

int __NumberOfModules = 1;
struct __ModuleEntry* __ModuleTable[] = {
&__aa_ModuleData
};
