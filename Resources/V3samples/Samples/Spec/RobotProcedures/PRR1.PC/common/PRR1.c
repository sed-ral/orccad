/* sscc : C CODE OF SORTED EQUATIONS PRR1 - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __PRR1_GENERIC_TEST(TEST) return TEST;
typedef void (*__PRR1_APF)();
static __PRR1_APF *__PRR1_PActionArray;

#include "PRR1.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _TR1_controler_DEFINED
#ifndef TR1_controler
extern void TR1_controler(orcStrlPtr);
#endif
#endif
#ifndef _TR1_fileparameter_DEFINED
#ifndef TR1_fileparameter
extern void TR1_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _TR1_parameter_DEFINED
#ifndef TR1_parameter
extern void TR1_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __PRR1_V0;
static boolean __PRR1_V1;
static orcStrlPtr __PRR1_V2;
static boolean __PRR1_V3;
static boolean __PRR1_V4;
static boolean __PRR1_V5;
static boolean __PRR1_V6;


/* INPUT FUNCTIONS */

void PRR1_I_START_PRR1 () {
__PRR1_V0 = _true;
}
void PRR1_I_Abort_Local_PRR1 () {
__PRR1_V1 = _true;
}
void PRR1_I_TR1_Start (orcStrlPtr __V) {
_orcStrlPtr(&__PRR1_V2,__V);
__PRR1_V3 = _true;
}
void PRR1_I_USERSTART_TR1 () {
__PRR1_V4 = _true;
}
void PRR1_I_BF_TR1 () {
__PRR1_V5 = _true;
}
void PRR1_I_T3_TR1 () {
__PRR1_V6 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __PRR1_A1 \
__PRR1_V0
#define __PRR1_A2 \
__PRR1_V1
#define __PRR1_A3 \
__PRR1_V3
#define __PRR1_A4 \
__PRR1_V4
#define __PRR1_A5 \
__PRR1_V5
#define __PRR1_A6 \
__PRR1_V6

/* OUTPUT ACTIONS */

#define __PRR1_A7 \
PRR1_O_BF_PRR1()
#define __PRR1_A8 \
PRR1_O_STARTED_PRR1()
#define __PRR1_A9 \
PRR1_O_GoodEnd_PRR1()
#define __PRR1_A10 \
PRR1_O_Abort_PRR1()
#define __PRR1_A11 \
PRR1_O_T3_PRR1()
#define __PRR1_A12 \
PRR1_O_START_TR1()
#define __PRR1_A13 \
PRR1_O_Abort_Local_TR1()

/* ASSIGNMENTS */

#define __PRR1_A14 \
__PRR1_V0 = _false
#define __PRR1_A15 \
__PRR1_V1 = _false
#define __PRR1_A16 \
__PRR1_V3 = _false
#define __PRR1_A17 \
__PRR1_V4 = _false
#define __PRR1_A18 \
__PRR1_V5 = _false
#define __PRR1_A19 \
__PRR1_V6 = _false

/* PROCEDURE CALLS */

#define __PRR1_A20 \
TR1_parameter(__PRR1_V2)
#define __PRR1_A21 \
TR1_controler(__PRR1_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __PRR1_A22 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int PRR1_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __PRR1__reset_input () {
__PRR1_V0 = _false;
__PRR1_V1 = _false;
__PRR1_V3 = _false;
__PRR1_V4 = _false;
__PRR1_V5 = _false;
__PRR1_V6 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __PRR1_R[4] = {_true,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int PRR1 () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = __PRR1_R[2]&&!(__PRR1_R[0]);
E[1] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__PRR1_A5);
E[2] = __PRR1_R[2]||__PRR1_R[3];
E[3] = __PRR1_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__PRR1_A3));
if (E[3]) {
__PRR1_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRR1_A22\n");
#endif
}
E[4] = __PRR1_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRR1_A1);
E[5] = __PRR1_R[1]&&!(__PRR1_R[0]);
E[6] = E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRR1_A1);
E[6] = E[4]||E[6];
if (E[6]) {
__PRR1_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRR1_A20\n");
#endif
}
if (E[6]) {
__PRR1_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRR1_A21\n");
#endif
}
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__PRR1_A5));
E[4] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__PRR1_A6));
E[4] = E[6]||(__PRR1_R[2]&&E[4]);
E[7] = (E[2]&&!(__PRR1_R[2]))||E[4];
E[8] = E[7]||E[1];
E[9] = __PRR1_R[3]&&!(__PRR1_R[0]);
E[10] = E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__PRR1_A2));
E[10] = E[6]||(__PRR1_R[3]&&E[10]);
E[11] = (E[2]&&!(__PRR1_R[3]))||E[10];
E[1] = E[1]&&E[8]&&E[11];
if (E[1]) {
__PRR1_A7;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRR1_A7\n");
#endif
}
if (E[6]) {
__PRR1_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRR1_A8\n");
#endif
}
if (E[1]) {
__PRR1_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRR1_A9\n");
#endif
}
E[9] = E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__PRR1_A2);
if (E[9]) {
__PRR1_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRR1_A10\n");
#endif
}
E[0] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__PRR1_A6);
E[8] = E[8]||E[0];
E[0] = E[0]&&E[8]&&E[11];
if (E[0]) {
__PRR1_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRR1_A11\n");
#endif
}
if (E[6]) {
__PRR1_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRR1_A12\n");
#endif
}
if (E[9]) {
__PRR1_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRR1_A13\n");
#endif
}
E[8] = E[9]&&E[8]&&(E[11]||E[9]);
E[12] = E[1]||E[0]||E[8];
E[13] = __PRR1_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRR1_A1));
E[5] = E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRR1_A1));
E[5] = E[13]||(__PRR1_R[1]&&E[5]);
E[11] = ((E[4]||E[10])&&E[7]&&E[11])||E[5];
E[2] = E[2]||__PRR1_R[1];
E[8] = E[8]||E[0]||E[1]||E[0]||E[8];
__PRR1_R[2] = E[4]&&!(E[8]);
__PRR1_R[3] = E[10]&&!(E[8]);
__PRR1_R[0] = !(_true);
__PRR1_R[1] = E[5];
__PRR1__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int PRR1_reset () {
__PRR1_R[0] = _true;
__PRR1_R[1] = _false;
__PRR1_R[2] = _false;
__PRR1_R[3] = _false;
__PRR1__reset_input();
return 0;
}
