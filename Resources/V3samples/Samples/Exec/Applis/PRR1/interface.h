#ifndef __interface_h
#define __interface_h

#include "Exec/canal.h"

enum INPUT_SIGNALS_USER {
  USERSTART_TR1 = COMPUTETIMEOUT + 1,  
  Stop,  
  KillOK_RobPrint,  
  TR1_Start,  
  InitOK_RobPrint,  
  ActivateOK_RobPrint,  
  CmdStopOK_RobPrint, 
  NB_SIGNALS
};

extern "C" { int aa();}
extern "C" { int aa_reset ();}
extern "C" { int prr_reset (ProcedureRobot*);}
extern "C" { 
  extern FUNCPTR TABSIG[NB_SIGNALS];

  FUNCPTR aa_I_USERSTART_TR1();
  FUNCPTR aa_I_Stop();
  FUNCPTR aa_I_KillOK_RobPrint();
  FUNCPTR aa_I_TR1_Start();
  FUNCPTR aa_I_InitOK_RobPrint();
  FUNCPTR aa_I_ActivateOK_RobPrint();
  FUNCPTR aa_I_CmdStopOK_RobPrint();
  FUNCPTR aa_I_Prr_Start(void*);
  FUNCPTR aa_I_ROBOT_FAIL();
  FUNCPTR aa_I_SOFT_FAIL();
  FUNCPTR aa_I_SENSOR_FAIL();
  FUNCPTR aa_I_CPU_OVERLOAD();
  int aa_O_Abort_TR1(void*);
  int aa_O_ActivateTR1_RobPrint(void*);
  int aa_O_TR1Transite(void*);
  int aa_O_Activate(void*);
  int aa_O_Transite(void*);
  int aa_O_EndParam(void*);
  int aa_O_EndKill(void*);
  int aa_O_EndObs(void*);
  int aa_O_FinBF(void*);
  int aa_O_FinT3(void*);
  int aa_O_GoodEndRPr();
  int aa_O_T3RPr();
  int f_default();
} // end extern 

extern char TABEVENT[NB_SIGNALS][MAXCHARNAME];

#endif
