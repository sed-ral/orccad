//----------------------------------------------------------
// Module Algo
//----------------------------------------------------------
#ifndef __orc_Mod_Algo_h
#define __orc_Mod_Algo_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"

#include "module_Algo_Inc.h"

class orc_Mod_Algo: public ModuleAlgo { 
protected: 
	// Internal Variables 
// Orccad Version: 3.2
// Module : Algo
// Variables declaration File
// Date of creation : Tue Aug 29 23:13:08 2006


public: 
	orc_Mod_Algo(ModuleTask *mt,int indexclk):
	  ModuleAlgo("orc_Mod_Algo",mt,indexclk) {};
	~orc_Mod_Algo() {};
	 // Output Ports declaration
	 int OutO;
	 // Output Event Ports declaration
	 int Stop;
	 // Output param Ports declaration
	 int Incre[2];

	 // Methods of computation 
	 void init(); 
	 void param(); 
	 void reparam(); 
	 void compute( ); 
	 void end(); 
 }; 
#endif 
// End class  orc_Mod_Algo

