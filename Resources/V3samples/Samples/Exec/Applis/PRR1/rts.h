#ifndef __rts_h
#define __rts_h

#include "Exec/utils.h"
#include "Exec/rt.h"
#include "Exec/param.h"


#include "mt_TR1.h"
class orc_RT_TR1: public RobotTask 
{
    protected:
	 char cfgFile[MAXFILENAME]; 
	 //Robot task Output parameters
	 //Robot task Input parameters
	 // Module Tasks
	 orc_Mt_TR1 mt_TR1;

    public: 
	 //Type 1 Exceptions
	 orc_RT_TR1(char* ,ProcedureRobot *, PhR *,char *);
	 ~orc_RT_TR1();
	 int SetParam(); 
	 int TestTimeout(); 
	 int SetT1Exceptions(); 
};
#endif
