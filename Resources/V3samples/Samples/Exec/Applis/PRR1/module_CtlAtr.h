//----------------------------------------------------------
// Module CtlAtr
//----------------------------------------------------------
#ifndef __orc_Mod_CtlAtr_h
#define __orc_Mod_CtlAtr_h

#include "Exec/module.h"

class orc_Mod_CtlAtr: public ModuleBeh { 
protected: 
	// No Internal Variables 

public: 
	orc_Mod_CtlAtr():
	  ModuleBeh("orc_Mod_CtlAtr") {
};
	~orc_Mod_CtlAtr() {};
	 // Output Ports declaration

 }; 
#endif 
// End class  orc_Mod_CtlAtr

