//----------------------------------------------------------
// Physical Resource RobPrint
//----------------------------------------------------------

#ifndef __phr_RobPrint_h
#define __phr_RobPrint_h

#include "Exec/phr.h"

#include "module_RobPrint_Inc.h"

class orc_PhR_RobPrint : public PhR {
private:
// Orccad Version: 3.2
// Module : RobPrint
// Variables declaration File
// Date of creation : Tue Aug 29 23:45:01 2006



public: 
   orc_PhR_RobPrint(ProcedureRobot *prr, char *ch ,int a, int b,int c ,int d) : PhR (prr,ch,a,b,c,d) {}; 
	 // Output Ports declaration
	 // Event Ports declaration
	 // Methods to call functions driver
	 void PUT_PortPrintI(const int &PortPrintI);
	 // Methods to control the driver mode
	 void reinit(); 
	 void param(); 
	 int Init(); 
	 int Close(); 
};

#endif 
// End of Physical Resource RobPrint

