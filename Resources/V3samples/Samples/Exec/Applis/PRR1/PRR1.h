#ifndef __orc_PRR1_h
#define __orc_PRR1_h

#include "Exec/utils.h"
#include "Exec/prr.h"

#include "interface.h"
#include "rts.h"

#include "PhR_RobPrint.h"
#define NBCLKS 1
#define BASECLK 1

class orc_PRR1:public ProcedureRobot {
protected: 
   // Index Tab for clocks
   int *TabClks;
   // Physical Ressources
   orc_PhR_RobPrint *orcRobPrint;
public:
   // Robot-Tasks
   orc_RT_TR1 *TR1;
   orc_RT_TR1 *GetRTTR1Ptr() { return TR1;}
   orc_PRR1();
   ~orc_PRR1();
   void Launch();
}; 

#endif 
// End class orc_PRR1
