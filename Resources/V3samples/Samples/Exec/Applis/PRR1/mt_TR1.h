#ifndef __mt_TR1_h
#define __mt_TR1_h

#include "Exec/mt.h"

#include "module_Algo.h"
#include "PhR_RobPrint.h"
#include "module_CtlAtr.h"

class RobotTask; 
class orc_Mt_TR1;
extern void fct_TR1(orc_Mt_TR1*);

class orc_Mt_TR1: public ModuleTask
{
	 friend void fct_TR1(orc_Mt_TR1*);
protected:
	 orc_Mod_Algo Algo_2;
	 orc_PhR_RobPrint* RobPrint;
public: 
	 orc_Mod_CtlAtr CtlAtr_4;
	 void InitTrace();
public:
#ifndef SIMPARC_RUN
	 orc_Mt_TR1(RobotTask *rt, char *name, int itFilter)
	 :ModuleTask((FUNCPTR) fct_TR1,rt,name,itFilter,0), 
#else
	 orc_Mt_TR1(RobotTask *rt, char *name, int itFilter)
	 :ModuleTask((VOIDFUNCPTRVOID) fct_TR1,rt,name,itFilter,0), 
#endif /* SIMPARC_RUN */
	 Algo_2(this,0)
	 ,CtlAtr_4()
	 {
	    InitTrace();
#ifdef SIMPARC_RUN
	Algo_2.vxwSet(vxwGet());
	CtlAtr_4.vxwSet(vxwGet());
#endif /* SIMPARC_RUN */
	    RobPrint = (orc_PhR_RobPrint*) RT->GetPhR();
	 }
	 ~orc_Mt_TR1(){};
};
#endif
