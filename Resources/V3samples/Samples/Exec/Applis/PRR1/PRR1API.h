//
// This file defines the API functions 
// to interfere with the generated Orccad program 
//
// FOR SOLARIS USERS:
// You can use it including it in the main program
// launching the Orccad run time tasks:
// e.g: 
// 	 #include "PRR1API.h"
// 	 main() { 
// 	 orcPRR1Init();
// 	 orcPRR1Launch();
// 	 // Launch task calling the others API Functions 
// 	 // ...add code here...
// 	 }
//
#ifndef __orc_PRR1Api_h
#define __orc_PRR1Api_h

// C function Interfaces
extern "C" { 
extern int orcPRR1Init();
extern int orcPRR1Launch();
extern int orcPRR1TaskLaunch();
extern int orcPRR1Go();
extern int orcPRR1Stop();
} // extern "C"

#endif 
