// Ilv Version: 3.1
// File generated: Tue May 30 16:43:47 2000
// Creator class: IlvGraphOutput
Palettes 5
1 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 4 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "gray" "turquoise" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAtr 2
3 IlvFilledRectangle 100 100 250 190 
4 IlvMessageLabel 100 100 251 191 F8 0 1 16 4 "Exc_Atr"   [Box
0 1 1 Exc_Atr 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 220 90 20 20 2 
2 IlvMessageLabel 207 105 92 28 F8 0 25 16 4 "End_traj"   0
[Port
0 1 1 End_traj 230 100 207 105 92 28 2  2 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 90 190 20 20 2 
2 IlvMessageLabel 112 186 50 28 F8 0 25 16 4 "Sing"   0
[Port
0 1 2 Sing 100 200 112 186 50 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
EOF
