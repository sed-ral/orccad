// Ilv Version: 3.1
// File generated: Tue Aug  8 13:03:54 2006
// Creator class: IlvGraphOutput
Palettes 6
4 "gray" "wheat" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
3 "red" "red" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"IlvStText" 5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModPr 2
4 IlvFilledRectangle 90 80 200 160 
5 IlvMessageLabel 90 80 201 161 F8 0 1 16 4 "Capteur"   [Box
0 1 1 Capteur 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 80 110 20 20 2 
2 IlvMessageLabel 100 106 64 28 F8 0 25 16 4 "Sortie"   0
[Port
0 1 1 Sortie 90 120 100 106 64 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 280 150 20 20 2 
2 IlvMessageLabel 252 146 56 28 F8 0 25 16 4 "Seuil"   0
[Port
0 1 2 Seuil 290 160 252 146 56 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 80 190 20 20 2 
2 IlvMessageLabel 100 186 42 28 F8 0 25 16 4 "Top"   0
[Port
0 1 3 Top 90 200 100 186 42 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
EOF
