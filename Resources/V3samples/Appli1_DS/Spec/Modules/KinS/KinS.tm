// Ilv Version: 3.1
// File generated: Tue Aug  8 12:47:28 2006
// Creator class: IlvGraphOutput
Palettes 6
0 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 1 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 2 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
4 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
0 IlvFilledRectangle 90 70 250 200 
1 IlvMessageLabel 90 70 251 201 F8 0 1 16 4 "KinS"   [Box
0 1 1 KinS 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
3 IlvFilledEllipse 80 160 20 20 
4 IlvMessageLabel 102 156 18 28 F8 0 25 16 4 "X"   0
[Port
0 1 1 X 90 170 102 156 18 28 1  1 
Port]

 } 20
1 1 { 2 2 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
5 IlvFilledEllipse 330 160 20 20 
4 IlvMessageLabel 321 156 14 28 F8 0 25 16 4 "q"   0
[Port
0 1 2 q 340 170 321 156 14 28 4  4 
Port]

 } 20
1 1 { 4 2 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
5 IlvFilledEllipse 210 260 20 20 
4 IlvMessageLabel 210 246 42 28 F8 0 25 16 4 "Top"   0
[Port
0 1 3 Top 220 270 210 246 42 28 8  8 
Port]

 } 20
1 1 { 6 2 OrcIlvVoidLink 1 0 5 } 26
EOF
