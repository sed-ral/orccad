// Orccad Version: 3.0 alpha
// Module : Excavator
// Initialisation Driver File
// 
// Driver Ports : 
// 	output DOUBLE Y[8]
// 	input DOUBLE U[4]
//
// Date of creation : Tue May 30 15:55:09 2000


/************************************************************************/
/* do not modify  --  init parameters for lsoda!!!!!  */
	itol = 1;
	itask = 4;
	istate = 1;
	iopt = 0;
	liw = LIW;
	lrw = LRW;
	jt = 2;
        neq = NEQ;
/* end do not modify */

        for(i=0;i<NEQ;i++) x[i] = 0.0;

        readinitstate(x,tol);
        abstol = tol[0]; rtol = tol[1];
	robotconfig();

    tint = 0.0;

  for(i=0;i<NDOF;i++){
    if(x[2*i] > ymax[i]){ x[2*i] = ymax[i];printf("initial position out of joint limit on ymax[%d] reset to %f \n", i, ymax[i]);}
    if(x[2*i] < ymin[i]){ x[2*i] = ymin[i];printf("initial position out of joint limit on ymin[%d] reset to %f \n", i, ymin[i]);}
  }

    for (i = 0;i < NEQ;i++) Y[i] = x[i];

printf("fin init PhR \n");

    if ((Reading_PhR = orcSemCreate(ORCFULL)) == NULL)
    {
        printf("ERROR orcSemCreate(Reading_PhR) \n");
    }
