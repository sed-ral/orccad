// Ilv Version: 3.1
// File generated: Wed Jul  5 16:47:18 2000
// Creator class: IlvGraphOutput
Palettes 6
4 "gray" "wheat" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModPr 2
4 IlvFilledRectangle 90 80 250 200 
5 IlvMessageLabel 90 80 251 201 F8 0 1 16 4 "Excavator"   [Box
0 1 1 Excavator 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 80 170 20 20 2 
2 IlvMessageLabel 102 166 18 28 F8 0 25 16 4 "Y"   0
[Port
0 1 1 Y 90 180 102 166 18 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 330 170 20 20 2 
2 IlvMessageLabel 287 166 82 28 F8 0 25 16 4 "Gamma"   0
[Port
0 1 2 Gamma 340 180 287 166 82 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 250 70 20 20 2 
2 IlvMessageLabel 257 85 14 28 F8 0 25 16 4 "T"   0
[Port
0 1 4 T 260 80 257 85 14 28 2  2 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
EOF
