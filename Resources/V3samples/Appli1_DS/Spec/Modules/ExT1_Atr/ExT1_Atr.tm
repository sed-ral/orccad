// Ilv Version: 3.1
// File generated: Tue Aug  8 17:11:02 2006
// Creator class: IlvGraphOutput
Palettes 6
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
1 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"IlvStText" 5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "turquoise" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAtr 2
4 IlvFilledRectangle 110 90 190 140 
5 IlvMessageLabel 110 90 191 141 F8 0 1 16 4 "ExT1_Atr"   [Box
0 1 1 ExT1_Atr 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 100 150 20 20 2 
2 IlvMessageLabel 120 146 108 28 F8 0 25 16 4 "End_Man"   0
[Port
0 1 1 End_Man 110 160 120 146 108 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 200 80 20 20 2 
2 IlvMessageLabel 185 95 100 28 F8 0 25 16 4 "Sing_OK"   0
[Port
0 1 2 Sing_OK 210 90 185 95 100 28 2  2 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
3 IlvFilledEllipse 290 150 20 20 
2 IlvMessageLabel 241 146 98 28 F8 0 25 16 4 "SingT1O"   0
[Port
0 1 3 SingT1O 300 160 241 146 98 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
EOF
