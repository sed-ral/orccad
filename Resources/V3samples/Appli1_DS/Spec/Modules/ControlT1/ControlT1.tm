// Ilv Version: 3.1
// File generated: Fri Aug  4 17:32:20 2006
// Creator class: IlvGraphOutput
Palettes 7
"IlvStText" 6 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 80 80 250 180 
6 IlvMessageLabel 80 80 251 181 F8 0 1 16 4 "ControlT1"   [Box
0 1 1 ControlT1 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 320 160 20 20 
2 IlvMessageLabel 297 156 42 28 F8 0 25 16 4 "q_d"   0
[Port
0 1 1 q_d 330 170 297 156 42 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 70 160 20 20 
2 IlvMessageLabel 92 156 82 28 F8 0 25 16 4 "Gamma"   0
[Port
0 1 2 Gamma 80 170 92 156 82 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
1 IlvFilledEllipse 160 250 20 20 
2 IlvMessageLabel 167 231 14 28 F8 0 25 16 4 "q"   0
[Port
0 1 3 q 170 260 167 231 14 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 250 250 20 20 2 
2 IlvMessageLabel 236 236 96 28 F8 0 25 16 4 "PutCond"   0
[Port
0 1 4 PutCond 260 260 236 236 96 28 8  8 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
EOF
