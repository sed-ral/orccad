// Orccad Version: 3.0 alpha
// Module : Control
// Initialisation File
//
// Module Ports :
// 	input DOUBLE q_d[5]
// 	output DOUBLE Gamma[5]
//
// Date of creation : Thu Jun  8 12:26:08 2000
        neq = NEQ;

	gravite(q,G);
      Gamma[0]=  -G[0];
      Gamma[1]=  -G[1] +10000.0*(q_d[2]-q[2]) +1250.0*(q_d[3]-q[3]);
      Gamma[2]=  -G[2] +10000.0*(q_d[4]-q[4]) +250.0*(q_d[5]-q[5]);
      Gamma[3]=  -G[3] +200.0*(q_d[6]-q[6]) +25.0*(q_d[7]-q[7]);

if(ABS(Gamma[1]) > 50000.0) Gamma[1] = 50000.0 * SIGN(Gamma[1]);
if(ABS(Gamma[2]) > 10000.0) Gamma[2] = 10000.0 * SIGN(Gamma[2]);
if(ABS(Gamma[3]) > 1000.0) Gamma[3] = 1000.0 * SIGN(Gamma[3]);


