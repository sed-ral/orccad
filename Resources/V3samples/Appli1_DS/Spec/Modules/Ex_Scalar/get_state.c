// Orccad Version: 3.0 alpha
// Module :  get_state.c
// Call Driver File for variable: 
// 	 output DOUBLE Y
// Date of creation : Tue May 30 15:55:09 2000



    int i;
    double Time;
    //Lock the access to the physical Resource
    orcSemTake(Reading_PhR, ORCFOREVER);
    /* #ifdef RTAI */
    /*     Time = ((double) orcGetCpuTime() - TZERO)/1.e9; */
    /* #else */
    Time = GetTime(module->phr->Clock);
    /* #endif */
    /*      printf("GET_Y at %f \n", Time);  */

    if (Time > tint) /*integrate from tint to current Time*/
    {
        tout = Time;
        rwork[0] = tout;
        istate = 1;
        //Start the computation
	printf("lsoda tint tout %lf %lf\n", tint, tout);
        lsoda((void(*)( void))modelrobot, &neq, x, &tint, &tout, &itol, &rtol, &abstol, &itask, &istate, &iopt, rwork, &lrw, iwork, &liw, (void(*)( void))jdum, &jt, Gamma2);

    }

    for (i = 0;i < NEQ;i++) Y[i] = x[i]; /*measure at time Time*/

    //Unlock the physical Resource
    orcSemGive(Reading_PhR);

