// Orccad Version: 3.1 
// Module : Control
// Initialisation File
// 
// Module Ports :
// 	input DOUBLE q[8]
// 	output DOUBLE Gamma[4]
// 	input DOUBLE q_d[8]
// 
// Time Methods usable in module files:
// 	 GetLocalTime(): return relative period time as double value
// 	 GetCurrentTime(): return global period time as double value 
// 
// Date of creation : Fri Aug  4 17:34:50 2006

