// Ilv Version: 3.1
// File generated: Wed Jul  5 16:32:58 2000
// Creator class: IlvGraphOutput
Palettes 7
"IlvStText" 6 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 19
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 100 80 220 170 
6 IlvMessageLabel 100 80 221 171 F8 0 1 16 4 "Gene_Traj_SE3"   [Box
0 1 1 Gene_Traj_SE3 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 160 20 20 
2 IlvMessageLabel 112 156 42 28 F8 0 25 16 4 "q_d"   0
[Port
0 1 1 q_d 100 170 112 156 42 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortPrm 2
3 IlvPolyline 11
312 192 328 208 320 200 328 192 312 208 320 200 328 200 312 200 320 200 320 192
320 208 
2 IlvMessageLabel 289 186 38 28 F8 0 25 16 4 "X_f"   0
[Port
0 1 2 X_f 320 200 289 186 38 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortPrm 2
3 IlvPolyline 11
312 152 328 168 320 160 328 152 312 168 320 160 328 160 312 160 320 160 320 152
320 168 
2 IlvMessageLabel 283 146 50 28 F8 0 25 16 4 "Kind"   0
[Port
0 1 4 Kind 320 160 283 146 50 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortPrm 2
3 IlvPolyline 11
312 92 328 108 320 100 328 92 312 108 320 100 328 100 312 100 320 100 320 92
320 108 
2 IlvMessageLabel 262 86 92 28 F8 0 25 16 4 "SimTime"   0
[Port
0 1 5 SimTime 320 100 262 86 92 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 140 240 20 20 2 
2 IlvMessageLabel 128 221 90 28 F8 0 25 16 4 "Fin_Sim"   0
[Port
0 1 6 Fin_Sim 150 250 128 221 90 28 8  8 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 200 240 20 20 2 
2 IlvMessageLabel 198 221 50 28 F8 0 25 16 4 "Sing"   0
[Port
0 1 8 Sing 210 250 198 221 50 28 8  8 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortVar 2
3 IlvFilledEllipse 270 240 20 20 
2 IlvMessageLabel 276 221 18 28 F8 0 25 16 4 "X"   0
[Port
0 1 9 X 280 250 276 221 18 28 8  8 
Port]

 } 20
1 1 { 14 0 OrcIlvVoidLink 1 0 13 } 30
2 0 { 15 0 OrcIlvPortVar 2
1 IlvFilledEllipse 130 70 20 20 
2 IlvMessageLabel 137 85 14 28 F8 0 25 16 4 "T"   0
[Port
0 1 10 T 140 80 137 85 14 28 2  2 
Port]

 } 20
1 1 { 16 0 OrcIlvVoidLink 1 0 15 } 30
2 0 { 17 0 OrcIlvPortVar 2
3 IlvFilledEllipse 90 200 20 20 
2 IlvMessageLabel 112 196 14 28 F8 0 25 16 4 "q"   0
[Port
0 1 11 q 100 210 112 196 14 28 1  1 
Port]

 } 20
1 1 { 18 0 OrcIlvVoidLink 1 0 17 } 30
EOF
