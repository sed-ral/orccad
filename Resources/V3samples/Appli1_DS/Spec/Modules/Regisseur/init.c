// Orccad Version: 3.2
// Module : Regisseur
// Initialisation File
// 
// Module Ports :
// 
// Time Methods usable in module files:
// 	 GetLocalTime(): return relative period time as double value
// 	 GetCurrentTime(): return global period time as double value 
// 
// Date of creation : Tue Sep  6 16:34:37 2005

    int i;
    //for file output
#ifdef RTAI
    //if (rtf_create(2, 200*5*sizeof(double)) < 0) printf("ERROR rtf_create2\n");
    if (rtf_create(2, 4000) < 0) printf("ERROR rtf_create2\n");
#endif
    sc->u[0] = sc->ud[0] = 0;
    sc->u[1] = sc->ud[1] = 0;
    sc->u[2] = sc->ud[2] = 0;
    sc->u[3] = sc->ud[3] = 0;
    sc->u[4] = sc->ud[4] = 0;
    sc->u[5] = sc->ud[5] = 0;
    sc->Ut = sc->Uvt = sc->Ucl = 0;
    //Mise a zero des compteurs???
    MTgetExecTime(sc->SC->Mt_ptr);
    MTgetExecTime(mt_TldReg_1->mt);
    MTgetExecTime(mt_TldReg_2->mt);
    MTgetExecTime(mt_TldReg_3->mt);
/*     MTgetExecTime(Mt_Ctc4->mt); */
/*     MTgetExecTime(Mt_Ctc5->mt); */

    mt_TldReg_1->mt->count = 0;
    mt_TldReg_2->mt->count = 0;
    mt_TldReg_3->mt->count = 0;
/*     Mt_Ctc4->mt->count = 0; */
/*     Mt_Ctc5->mt->count = 0; */

    mt_TldReg_1->mt->tot_time = 0;
    mt_TldReg_2->mt->tot_time = 0;
    mt_TldReg_3->mt->tot_time = 0;
/*     Mt_Ctc4->mt->tot_time = 0; */
/*     Mt_Ctc5->mt->tot_time = 0; */


    sc->lambda = .3; //pour qu'au premier run, on n'ait pas d'effets de bord. on l'initialise vraiment dans le compute

    sc->Uref = Uref1;
    for (i = 0; i < 4;i++) sc->X[i] = 0.0;
    for (i = 0; i < 5;i++) sc->Xk[i] = 0.0;
    

#ifdef OUTPUT_SC
    sched.t = sc->SC->Clock->Time;
    sched.Uref = sc->Uref;
    sched.u1 = sc->u[1];
    sched.u2 = sc->u[2];
    sched.u3 = sc->u[3];
    sched.u4 = sc->u[4];
    sched.u5 = sc->u[5];
    sched.ut = sc->Ut;
    sched.per1 = GetSampleTime(mt_TldReg_1->mt);
    sched.per2 = GetSampleTime(mt_TldReg_2->mt);
    sched.per3 = GetSampleTime(mt_TldReg_3->mt);
/*     sched.per4 = GetSampleTime(Mt_Ctc4->mt); */
/*     sched.per5 = GetSampleTime(Mt_Ctc5->mt); */
    sched.uvt = sc->Uvt;
    sched.u0 = sc->u[0];
    sched.ucl = sc->Ucl;
    sched.jsave = 0;
#ifdef RTAI
    rtf_put(2, &sched, sizeof(sched));
#endif
#endif
