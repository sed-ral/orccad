// Orccad Version: 3.2
// Module : Regisseur
// Computation File 
// 
// Module Ports :
// 
// 
// Date of creation : Tue Sep  6 16:34:37 2005

    if (GetTime(sc->SC->Clock) > T_echelon) sc->Uref = Uref2;

    //Calcul de l'Erreur
    orc_Mod_Regisseur_ErrorSignal(sc, mt_TldReg_1, mt_TldReg_2, mt_TldReg_3/* , mt_TldReg_4, mt_TldReg_5 */);

    //Application de la commande
    orc_Mod_Regisseur_Control(sc, mt_TldReg_1, mt_TldReg_2, mt_TldReg_3/* , mt_TldReg_4, mt_TldReg_5 */);

    //Reset

    //printf("regisseur duree = %ld \n", sc->tt[0]);
/*     printf("dans mt_TldReg_1 count %d tot_time %ld tt %ld \n", mt_TldReg_1->mt->count, (long)mt_TldReg_1->mt->tot_time, sc->tt[1]); */
/*     printf("dans mt_TldReg_2 count %d tot_time %ld tt %ld \n", mt_TldReg_2->mt->count, (long)mt_TldReg_2->mt->tot_time, sc->tt[2]); */
/*     printf("dans mt_TldReg_3 count %d tot_time %ld tt  %ld \n", mt_TldReg_3->mt->count, (long)mt_TldReg_3->mt->tot_time, sc->tt[3]); */
/*     printf("dans mt_TldReg_4 count %d tot_time %ld tt  %ld \n", mt_TldReg_4->mt->count, (long)mt_TldReg_4->mt->tot_time, sc->tt[4]); */
/*     printf("dans mt_TldReg_5 count %d tot_time %ld tt  %ld \n", mt_TldReg_5->mt->count, (long)mt_TldReg_5->mt->tot_time, sc->tt[5]); */

    mt_TldReg_1->mt->count = 0;
    mt_TldReg_2->mt->count = 0;
    mt_TldReg_3->mt->count = 0;
    /* mt_TldReg_4->mt->count = 0; */
/*     mt_TldReg_5->mt->count = 0; */

    mt_TldReg_1->mt->tot_time = 0;
    mt_TldReg_2->mt->tot_time = 0;
    mt_TldReg_3->mt->tot_time = 0;
    /* mt_TldReg_4->mt->tot_time = 0; */
/*     mt_TldReg_5->mt->tot_time = 0; */

}

void orc_Mod_Regisseur_ErrorSignal(orc_Mod_Regisseur* sc, orc_mt_TldReg_1*mt_TldReg_1, orc_mt_TldReg_2*mt_TldReg_2, orc_mt_TldReg_3*mt_TldReg_3/* , orc_mt_TldReg_4*mt_TldReg_4, orc_mt_TldReg_5*mt_TldReg_5 */)
{
    double T[6];
    int n[6], i;
    double t[6], tm[6];

    double A[16] =

        {1.0005e+000, -5.5897e-006, -1.7099e-001, 4.5268e-002,
         -1.9206e-002, -1.7773e-006, -1.7582e+000, 4.7938e+000,
         5.7819e-003, -3.2830e-008, -1.4799e-003, 1.6002e-003,
         6.4127e-003, -3.5546e-008, -8.3157e-004, -4.2957e-004};

    double B[4] =

        { -6.1594e-001,
          -3.4295e-004,
          -3.5631e-003,
          -3.9462e-003};

    double C[12] =

        { -3.7831e-001, 2.1624e-006, 4.5231e-001, 2.0338e-001,
          -1.6557e-001, 3.0305e-001, 1.6604e+000, -5.0878e+000,
          -2.0436e-001, -3.0305e-001, -1.8917e+000, 4.5971e+000};

    double D[3] =

        {3.2956e-001,
         1.6514e-001,
         1.6444e-001};

double Ak[25] =

  {1.0345,    0.0001,    0.0001,   -0.1771,    0.0826,
   0.0032,   -0.0000,   -0.0000,   -0.0003,   -0.1826,
   -0.0008,    0.0000,    0.0000,    0.0001,    0.0526,
   0.2112,    0.0000,    0.0000,   -0.0362,    0.0248,
   0.0179,    0.0000,    0.0000,   -0.0031,    0.0014};

 double Bk[5] =

       {-0.6231,
	0.0000,
	-0.0001,
	-0.1273,
	-0.0108};

double Ck[20] =

  {-0.5866,    0.0645,   0.0777,    0.4628,   -0.0774,
   0.0452,    0.1936,    0.2331,   -0.2252,    0.0467,
   -0.1078,   -0.3622,    0.0382,   -0.0536,   -0.2756,
   -0.1137,    0.1041,   -0.3490,   -0.0531,    0.0761};

double Dk[4] =

  {0.4394,
   0.0000,
   0.1099,
   0.1098};


    //Temps d'execution
 printf("exectimes in nanos %ld %ld %ld %ld %ld %ld  secs\n", (long)(sc->SC->Mt_ptr->raw_time), (long)(mt_TldReg_1->mt->raw_time), (long)(mt_TldReg_2->mt->raw_time), (long)(mt_TldReg_3->mt->raw_time)/* , (long)(mt_TldReg_4->mt->raw_time), (long)(mt_TldReg_5->mt->raw_time) */);
    t[0] = ((double)(sc->SC->Mt_ptr->raw_time)) / 1.e9;
    t[1] = ((double)(mt_TldReg_1->mt->raw_time)) / 1.e9;
    t[2] = ((double)(mt_TldReg_2->mt->raw_time)) / 1.e9;
    t[3] = ((double)(mt_TldReg_3->mt->raw_time)) / 1.e9;
    /* t[4] = ((double)(mt_TldReg_4->mt->raw_time)) / 1.e9; */
/*     t[5] = ((double)(mt_TldReg_5->mt->raw_time)) / 1.e9; */
    /*     printf("exectimes en secs dble %lf %lf %lf %lf %lf\n", t[1], t[2], t[3], t[4], t[5]); */    //Nombre d'instance
    n[0] = 1;
    n[1] = mt_TldReg_1->mt->count;
    n[2] = mt_TldReg_2->mt->count;
    n[3] = mt_TldReg_3->mt->count;
    /* n[4] = mt_TldReg_4->mt->count; */
/*     n[5] = mt_TldReg_5->mt->count; */
    for (i = 0;i < 6;i++)
    {
        if (n[i] == 0) /* printf("N = 0 for task Ctc%d\n", i); */ n[i] = 1;
    }
    printf("counts %d %d %d %d %d\n", n[1], n[2], n[3], n[4], n[5]);
    //dur�e d'exec moyenne
#ifdef RTAI
    sc->tt[0] = MTgetExecTime(sc->SC->Mt_ptr);
    sc->tt[1] = MTgetExecTime(mt_TldReg_1->mt);
    sc->tt[2] = MTgetExecTime(mt_TldReg_2->mt);
    sc->tt[3] = MTgetExecTime(mt_TldReg_3->mt);
    /* sc->tt[4] = MTgetExecTime(mt_TldReg_4->mt); */
/*     sc->tt[5] = MTgetExecTime(mt_TldReg_5->mt); */
    for (i = 0;i < 6;i++) tm[i] = (double)(sc->tt[i]) / (1.e9 * n[i]);
    //sc->Ucl = MTgetExecTime(clock->clocksId);
#else
    tm[1] = ((double)(mt_TldReg_1->mt->tot_time)) / (1.e9 * n[1]);
    tm[2] = ((double)(mt_TldReg_2->mt->tot_time)) / (1.e9 * n[2]);
    tm[3] = ((double)(mt_TldReg_3->mt->tot_time)) / (1.e9 * n[3]);
    /* tm[4] = ((double)(mt_TldReg_4->mt->tot_time)) / (1.e9 * n[4]); */
/*     tm[5] = ((double)(mt_TldReg_5->mt->tot_time)) / (1.e9 * n[5]); */
#endif
#ifdef RTAI
/*     for (i = 1;i < 6;i++) printf("tt[%d] %ld ", i, (sc->tt[i])); */
/*     printf("\n"); */
/*     for (i = 1;i < 6;i++) printf("tm[%d] %ld ", i, (sc->tt[i])/n[i]); */
/*     printf("\n"); */
#else
    for (i = 1;i < 6;i++) printf("tm[%d] %lf ", i, tm[i]);
    printf("\n");
#endif

    //Periode
    T[1] = GetSampleTime(mt_TldReg_1->mt);
    T[2] = GetSampleTime(mt_TldReg_2->mt);
    T[3] = GetSampleTime(mt_TldReg_3->mt);
    /* T[4] = GetSampleTime(mt_TldReg_4->mt); */
/*     T[5] = GetSampleTime(mt_TldReg_5->mt); */
    T[0] = GetSampleTime(sc->SC->Mt_ptr);
#ifdef RTAI 
        printf("periode scheduler %ld\n", (long)(T[0]*1000000));
        printf("GetSampleTime %ld %ld %ld %ld %ld microsecs\n", (long)(T[1]*1000000), (long)(T[2]*1000000), (long)(T[3]*1000000)/* , (long)(T[4]*1000000), (long)(T[5]*1000000) */);
#else
    printf("periode scheduler %lf secs \n", T[0]);
    printf("GetSampleTime %lf %lf %lf %lf %lf secs\n", T[1], T[2], T[3]/* , T[4], T[5] */);
#endif
    //Calcul de la charge
    /* #ifdef RTAI */
    /*     for (i=0;i<6;i++) sc->u[i] = (double)(sc->tt[i])/(1.e9*T[0]); */
    /* #else */
    sc->u[0] = n[0] * tm[0] / T[0];
    sc->u[1] = n[1] * tm[1] / T[0];
    sc->u[2] = n[2] * tm[2] / T[0];
    sc->u[3] = n[3] * tm[3] / T[0];
    /* sc->u[4] = n[4] * tm[4] / T[0]; */
/*     sc->u[5] = n[5] * tm[5] / T[0]; */
    /* #endif */
#ifdef RTAI 
    /*     printf("instant load %ld %ld %ld %ld %ld total %ld\n",  (long)(sc->u[1]*100),  (long)(sc->u[2]*100),  (long)(sc->u[3]*100),  (long)(sc->u[4]*100),  (long)(sc->u[5]*100),  (long)((sc->u[5] + sc->u[1] + sc->u[2] + sc->u[3] + sc->u[4])*100)); */
#else
    printf("instant load %lf %lf %lf %lf %lf total %lf\n", (sc->u[1]*100), (sc->u[2]*100), (sc->u[3]*100), (sc->u[4]*100), (sc->u[5]*100), ((sc->u[5] + sc->u[1] + sc->u[2] + sc->u[3] + sc->u[4])*100));
#endif
    //Filtrage
    sc->ud[0] = sc->lambda * sc->ud[0] + (1 - sc->lambda) * sc->u[0];
    sc->ud[1] = sc->lambda * sc->ud[1] + (1 - sc->lambda) * sc->u[1];
    sc->ud[2] = sc->lambda * sc->ud[2] + (1 - sc->lambda) * sc->u[2];
    sc->ud[3] = sc->lambda * sc->ud[3] + (1 - sc->lambda) * sc->u[3];
    /* sc->ud[4] = sc->lambda * sc->ud[4] + (1 - sc->lambda) * sc->u[4]; */
/*     sc->ud[5] = sc->lambda * sc->ud[5] + (1 - sc->lambda) * sc->u[5]; */
#ifdef RTAI 
    /*     printf("filtered load %ld %ld %ld %ld %ld total %ld\n",  (long)(sc->ud[1]*100),  (long)(sc->ud[2]*100),  (long)(sc->ud[3]*100),  (long)(sc->ud[4]*100),  (long)(sc->ud[5]*100),  (long)((sc->ud[5] + sc->ud[1] + sc->ud[2] + sc->ud[3] + sc->ud[4])*100)); */
#else
/*     printf("filtered load %lf %lf %lf %lf %lf total %lf\n", (sc->ud[1]*100), (sc->ud[2]*100), (sc->ud[3]*100), (sc->ud[4]*100), (sc->ud[5]*100), ((sc->ud[5] + sc->ud[1] + sc->ud[2] + sc->ud[3] + sc->ud[4])*100)); */
#endif
    sc->Ut = sc->ud[1] + sc->ud[3] /* + sc->ud[4] + sc->ud[5] */ ;
    sc->Uvt = sc->ud[0] + sc->ud[1] + sc->ud[2] + sc->ud[3] /* + sc->ud[4] + sc->ud[5] */;
    sc->Ue = sc->Uref - sc->Ut;
#ifdef RTAI 
    /*     printf("Ut*1.e3 %ld Uref*1.e3 %ld Uvt*1.e3 %ld\n", (long)(sc->Ut*1000), (long)(sc->Uref*1000), (long)(sc->Uvt*1000)); */
#else
    printf("Ut*1.e3 %lf Uref*1.e3 %lf\n", (sc->Ut*1000), (sc->Uref*1000));
    printf("Ue*1.e6 %lf\n", (sc->Ue*1000000));
#endif
    //Commande:
    //sc->lambda = .3;

    sc->Y[0] = C[0] * sc->X[0] + C[1] * sc->X[1] + C[2] * sc->X[2] + C[3] * sc->X[3] + D[0] * sc->Ue;
    sc->Y[1] = C[4] * sc->X[0] + C[5] * sc->X[1] + C[6] * sc->X[2] + C[7] * sc->X[3] + D[1] * sc->Ue;
    sc->Y[2] = C[8] * sc->X[0] + C[9] * sc->X[1] + C[10] * sc->X[2] + C[11] * sc->X[3] + D[2] * sc->Ue;

    sc->X[0] = A[0] * sc->X[0] + A[1] * sc->X[1] + A[2] * sc->X[2] + A[3] * sc->X[3] + B[0] * sc->Ue;
    sc->X[1] = A[4] * sc->X[0] + A[5] * sc->X[1] + A[6] * sc->X[2] + A[7] * sc->X[3] + B[1] * sc->Ue;
    sc->X[2] = A[8] * sc->X[0] + A[9] * sc->X[1] + A[10] * sc->X[2] + A[11] * sc->X[3] + B[2] * sc->Ue;
    sc->X[3] = A[12] * sc->X[0] + A[13] * sc->X[1] + A[14] * sc->X[2] + A[15] * sc->X[3] + B[3] * sc->Ue;

    sc->Yk[0] = Ck[0] * sc->Xk[0] + Ck[1] * sc->Xk[1] + Ck[2] * sc->Xk[2] + Ck[3] * sc->Xk[3] + Ck[4] * sc->Xk[4] + Dk[0] * sc->Ue;
    sc->Yk[1] = Ck[5] * sc->Xk[0] + Ck[6] * sc->Xk[1] + Ck[7] * sc->Xk[2] + Ck[8] * sc->Xk[3] + Ck[9] * sc->Xk[4] + Dk[1] * sc->Ue;
    sc->Yk[2] = Ck[10] * sc->Xk[0] + Ck[11] * sc->Xk[1] + Ck[12] * sc->Xk[2] + Ck[13] * sc->Xk[3] + Ck[14] * sc->Xk[4] + Dk[2] * sc->Ue;
    sc->Yk[3] = Ck[15] * sc->Xk[0] + Ck[16] * sc->Xk[1] + Ck[17] * sc->Xk[2] + Ck[18] * sc->Xk[3] + Ck[19] * sc->Xk[4] + Dk[3] * sc->Ue;

    sc->Xk[0] = Ak[0] * sc->Xk[0] + Ak[1] * sc->Xk[1] + Ak[2] * sc->Xk[2] + Ak[3] * sc->Xk[3] + Ak[4] * sc->Xk[4] + Bk[0] * sc->Ue;
    sc->Xk[1] = Ak[5] * sc->Xk[0] + Ak[6] * sc->Xk[1] + Ak[7] * sc->Xk[2] + Ak[8] * sc->Xk[3] + Ak[9] * sc->Xk[4] + Bk[1] * sc->Ue;
    sc->Xk[2] = Ak[10] * sc->Xk[0] + Ak[11] * sc->Xk[1] + Ak[12] * sc->Xk[2] + Ak[13] * sc->Xk[3] + Ak[14] * sc->Xk[4] + Bk[2] * sc->Ue;
    sc->Xk[3] = Ak[15] * sc->Xk[0] + Ak[16] * sc->Xk[1] + Ak[17] * sc->Xk[2] + Ak[18] * sc->Xk[3] + Ak[19] * sc->Xk[4] + Bk[3] * sc->Ue;
    sc->Xk[4] = Ak[20] * sc->Xk[0] + Ak[21] * sc->Xk[1] + Ak[22] * sc->Xk[2] + Ak[23] * sc->Xk[3] + Ak[24] * sc->Xk[4] + Bk[4] * sc->Ue;

    /* #ifdef RTAI */
    /*     sc->f[3] = sc->Y[2] * n[3]/ sc->tt[3]; */
    /*     sc->f[4] = sc->Y[0] * n[4]/ sc->tt[4]; */
    /*     sc->f[5] = sc->Y[1] * n[5]/ sc->tt[5]; */
    /* #else */
/*     sc->f[3] = sc->Y[2] / tm[3]; */
/*     sc->f[4] = sc->Y[0] / tm[4]; */
/*     sc->f[5] = sc->Y[1] / tm[5]; */
/*     sc->f[1] = 1.0/T[1]; */
    sc->f[1] = sc->Yk[0] / tm[1];
    sc->f[3] = sc->Yk[3] / tm[3];
    /* sc->f[4] = sc->Yk[1] / tm[4]; */
/*     sc->f[5] = sc->Yk[2] / tm[5]; */
    /* #endif */
    if(sc->f[1] < 100.0) sc->f[1] = 100.0;
    //if(sc->f[1] > 2000.0) sc->f[1] = 2000.0;   
    for (i = 3;i < 6;i++)
    {
        if (sc->f[i] > 1000.0) sc->f[i] = 1000.0;
        if (sc->f[i] < 1.0 / T[0]) sc->f[i] = 1.0 / T[0];
    }
}
void orc_Mod_Regisseur_Control(orc_Mod_Regisseur* sc, orc_mt_TldReg_1*mt_TldReg_1, orc_mt_TldReg_2*mt_TldReg_2, orc_mt_TldReg_3*mt_TldReg_3/* , orc_mt_TldReg_4*mt_TldReg_4, orc_mt_TldReg_5*mt_TldReg_5 */)
{
    /*   int i; */
    /*   double Tsc; */
  double per1, per3, per4, per5;
    /*   if ( sc->f[0] < 1.02/GetSampleTime(sc->SC->Mt_ptr)) sc->f[0]=1.02/GetSampleTime(sc->SC->Mt_ptr);  */

    /*   if ( sc->f[1] < 1.02/GetSampleTime(sc->SC->Mt_ptr)) sc->f[1]=1.02/GetSampleTime(sc->SC->Mt_ptr); */
    /*   Tsc = GetSampleTime(sc->SC->Mt_ptr); */
    /*   for(i=3;i<6;i++) */
    /*     {if(sc->f[i] < 1/Tsc) sc->f[i] = 1/Tsc;} */
    per1 = 1.0 / sc->f[1];    
    per3 = 1.0 / sc->f[3];
    /* per4 = 1.0 / sc->f[4]; */
/*     per5 = 1.0 / sc->f[5]; */

    SetSampleTime(mt_TldReg_1->mt, per1);
    SetSampleTime(mt_TldReg_3->mt, per3);
    /* SetSampleTime(mt_TldReg_4->mt, per4); */
/*     SetSampleTime(mt_TldReg_5->mt, per5); */
#ifdef RTAI
    printf("periodes microsecs t1 %ld t3 %ld t4 %ld t5 %ld \n", (long)(per1*1.e6), (long)(per3*1.e6), (long)(per4*1.e6), (long)(per5*1.e6));

#else
    printf("periodes microsecs t1 %lf t3 %lf t4 %lf t5 %lf \n", (1 / sc->f[1]*1000000), (1 / sc->f[3]*1000000)/* , (1 / sc->f[4]*1000000), (1 / sc->f[5]*1000000)) */;
    printf("fr�quences f1 %lf f3 %lf f4 %lf f5 %lf\n", sc->f[1], sc->f[3]/* , sc->f[4], sc->f[5] */ );
#endif
#ifdef OUTPUT_SC
    sched.t = sc->SC->Clock->Time;
    sched.Uref = sc->Uref;
    sched.u1 = sc->u[1];
    sched.u2 = sc->u[2];
    sched.u3 = sc->u[3];
    sched.u4 = sc->u[4];
    sched.u5 = sc->u[5];
    sched.ut = sc->Ut;
    sched.per1 = GetSampleTime(mt_TldReg_1->mt);
    sched.per2 = GetSampleTime(mt_TldReg_2->mt);
    sched.per3 = GetSampleTime(mt_TldReg_3->mt);
    /* sched.per4 = GetSampleTime(mt_TldReg_4->mt); */
/*     sched.per5 = GetSampleTime(mt_TldReg_5->mt); */
    sched.uvt = sc->Uvt;
    sched.u0 = sc->u[0];
    sched.ucl = sc->Ucl;
    sched.jsave = 0;
#ifdef RTAI
    rtf_put(2, &sched, sizeof(sched));
#endif
#endif
    printf("END Regisseur\n");
