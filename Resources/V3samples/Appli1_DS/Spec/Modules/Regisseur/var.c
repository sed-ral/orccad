// Orccad Version: 3.2
// Module : Regisseur
// Variables declaration File
// Date of creation : Tue Sep  6 16:34:37 2005

#define Uref1 .8
#define Uref2 .6 
/* #define Uref1 .01 */
/* #define Uref2 .005 */
#define T_echelon 1.5

#define OUTPUT_SC
struct
{
    double t; double Uref, u1, u2, u3, u4, u5, ut, per1, per2, per3, per4, per5, uvt, u0, ucl;int jsave;
}
sched;

