// Ilv Version: 3.1
// File generated: Tue Aug  8 12:28:40 2006
// Creator class: IlvGraphOutput
Palettes 5
0 "gray" "turquoise" "default" "fixed" 0 solid solid 0 0 0
3 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 1 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 2 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAtr 2
0 IlvFilledRectangle 100 100 250 190 
1 IlvMessageLabel 100 100 251 191 F8 0 1 16 4 "Sensor_Atr"   [Box
0 1 1 Sensor_Atr 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 220 90 20 20 2 
4 IlvMessageLabel 207 105 92 28 F8 0 25 16 4 "End_traj"   0
[Port
0 1 1 End_traj 230 100 207 105 92 28 2  2 
Port]

 } 20
1 1 { 2 2 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 90 190 20 20 2 
4 IlvMessageLabel 112 186 50 28 F8 0 25 16 4 "Sing"   0
[Port
0 1 2 Sing 100 200 112 186 50 28 1  1 
Port]

 } 20
1 1 { 4 2 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 340 190 20 20 2 
4 IlvMessageLabel 294 186 92 28 F8 0 25 16 4 "SensEvt"   0
[Port
0 1 3 SensEvt 350 200 294 186 92 28 4  4 
Port]

 } 20
1 1 { 6 2 OrcIlvVoidLink 1 0 5 } 26
EOF
