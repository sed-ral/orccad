// Ilv Version: 3.1
// File generated: Wed Jul  5 16:21:28 2000
// Creator class: IlvGraphOutput
Palettes 6
"IlvStText" 5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 90 70 250 200 
5 IlvMessageLabel 90 70 251 201 F8 0 1 16 4 "Kin"   [Box
0 1 1 Kin 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 80 160 20 20 
2 IlvMessageLabel 102 156 18 28 F8 0 25 16 4 "X"   0
[Port
0 1 1 X 90 170 102 156 18 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 330 160 20 20 
2 IlvMessageLabel 321 156 14 28 F8 0 25 16 4 "q"   0
[Port
0 1 2 q 340 170 321 156 14 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
EOF
