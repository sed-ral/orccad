// Orccad Version: 3.0 alpha   
// Module : Gene_Traj_SE3
// Computation File 
//
// Module Ports :
// 	output DOUBLE q_d[8]
// 	param DOUBLE X_f[2]
// 	param DOUBLE t_traj
// 	param INTEGER Kind
// 	param DOUBLE Tsim
//
// Date of creation : Tue May 30 13:45:04 2000

/* T[0] = t; */
/* T[1] = Dt; */

// printf("t = %lf \n", t);
  if (rint(t*1000000) <= rint(t_traj*1000000)) {
	  switch (Kind)
	    { 
	    case 1 :
	      r = t/t_traj; 
	    break;
	    case 2 : 
	      r1 = t/t_traj;
	      r2 = r1*r1;
	      r = 3.0*r2 - 2.0*r1*r2;
	    break;
	    case 3 : 
	      r1 = t/t_traj;
	      r2 = r1*r1;
	      r3 = r2*r1;
	      r4 = r3*r1;
	      r = 10.0*r3 - 15.0*r4 + 6.0*r1*r4;
	      break;
	    default : printf("trajectory not available \n");
	      Sing = SET_EVENT;
	}
        for (i = 0;i < NDOF;i++)
        {
            q_d[2*i] = q_i[2 * i] + r * (q_f[2 * i] - q_i[2 * i]);
            q_d[2*i + 1] = (q_d[2 * i] - q_dp[2 * i]) / Dt;
            q_dp[2*i] = q_d[2 * i];
        }


      }
    else
    {
        for (i = 0;i < NEQ;i++) q_d[i] = q_f[i];
    }


  output.t=t; 
  output.f1 = q[0];
  output.f2 = q[2];
  output.f3 = q[4];
  output.f4 = q[6];
  output.f5 = q[6];
  output.f6 = /*genet->*/q_d[0];
  output.f7 = /*genet->*/q_d[2];
  output.f8 = /*genet->*/q_d[4];
  output.f9 = /*genet->*/q_d[6];
  output.f10 = /*genet->*/q_d[6];
/*   output.f11 = X[NDOF*NSE-2]; */
/*   output.f12 = X[NDOF*NSE-1]; */
/*   output.f13 = Xd; */
/*   output.f14 = Yd; */
  output.jsave = jsave;
  jsave++;
  
#ifdef ORCRTAI
  rtf_put(1,&(output),sizeof(output));
#else
  printf("Traj q = %f %f %f %f %f\n",t,q[0],q[2],q[4],q[6]);
  printf("Traj qd =%f %f %f %f %f\n",t,q_d[0],q_d[2],q_d[4],q_d[6]);
#endif

  if (rint(t*1000000)>=rint(Tsim*1000000))
{
printf("Tsim atteint MAN \n");
		printf("pseudo inverse finale MAN: \n");

     output.jsave = 99999;
#ifdef ORCRTAI
     rtf_put(1,&(output),sizeof(output));
#endif
        printf("position articulaire finale: \n");
        printf("%f %f %f %f \n", q[0], q[2], q[4], q[6]);
 Fin_Sim = SET_EVENT;
}
t = t + Dt;
