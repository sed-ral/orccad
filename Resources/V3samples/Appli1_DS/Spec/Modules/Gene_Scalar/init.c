// Orccad Version: 3.0 alpha
// Module : Gene_Traj_SE3
// Initialisation File
//
// Module Ports :
// 	output DOUBLE X_d[2]
// 	param DOUBLE X_f[2]
// 	param DOUBLE t_traj
// 	param INTEGER Kind
//
// Date of creation : Tue May 30 13:45:04 2000

	 //for file output 
#ifdef ORCRTAI
	 if(rtf_create(1, 700*16*sizeof(double))<0) printf("ERROR rtf_create2\n");
#endif

t = 0.0;
//Dt = Mt_Tld->Kin_7.getSampleTime();
//Dt = 0.05;
Dt = getSampleTime(module->malgo);
//T[0] = t; T[1] = Dt;
TimeSource = t;
affich = 0;

    for (i = 0;i < NEQ;i++)
    {
        q_d[i] = q_i[i] = q[i];
    }

//       robotconfig();

//readtldparam(SimTime,Kind,X_f,alpha);

    printf("param avant \n");
    SimTime[0] = 1.0;
    SimTime[1] = 2.0;
    Kind = 1;
/*     X_f[0] = 0.0; */
/*     X_f[1] = 0.0; */
/*     X_f[2] = 1.3; */
/*     X_f[3] = 0.0; */
/*     X_f[4] = 0.0; */
/*     X_f[5] = 0.0; */
    q_f[0] = -1.57;
    q_f[2] = -1.57;
    q_f[4] = -1.57;
    q_f[6] = -1.57;

    for (i = 0;i < NDOF;i++) q_f[2*i + 1] = 0.0;

    printf("param apres \n");

    t_traj = SimTime[0];
    Tsim = SimTime[1];
  
	neq = NEQ;
	jsave = 0;



    printf("position articulaire  initiale: \n");
    printf("%f %f %f %f  \n", q_i[0], q_i[2], q_i[4], q_i[6]);
    printf("position articulaire d�sir�e: \n");
    printf("%f %f %f %f \n", q_f[0], q_f[2], q_f[4], q_f[6]);

    for (j = 0;j < NEQ;j++)
    {
        q_dp[j] = 0.0;
    }

  output.t=t; 
  output.f1 = q[0];
  output.f2 = q[2];
  output.f3 = q[4];
  output.f4 = q[6];
  output.f5 = q[6];
  output.f6 = /*genet->*/q_d[0];
  output.f7 = /*genet->*/q_d[2];
  output.f8 = /*genet->*/q_d[4];
  output.f9 = /*genet->*/q_d[6];
  output.f10 = /*genet->*/q_d[6];
/*   output.f11 = X[NDOF*NSE-2]; */
/*   output.f12 = X[NDOF*NSE-1]; */
/*   output.f13 = Xd; */
/*   output.f14 = Yd; */
  output.jsave = jsave;
  jsave++;
#ifdef ORCRTAI
  rtf_put(1,&(output),sizeof(output));
#else
printf("%f %f %f %f\n",t,q[2],q[4],q[6]);
#endif

