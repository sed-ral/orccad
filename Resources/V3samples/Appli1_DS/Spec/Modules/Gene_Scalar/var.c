// Orccad Version: 3.0 alpha
// Module : Gene_Traj_SE3
// Variables declaration File
// Date of creation : Tue May 30 13:45:04 2000

 double t, Dt, q_dp[NEQ];
 double t_traj, Tsim;
 double TZERO;
 double r, r1, r2, r3, r4 ;
 int i, j, err, irank, affich, jsave, neq;


  struct {double t, f1, f2, f3, f4, f5, f6, f7, f8, f9, f10, f11, f12, f13, f14; int jsave;} output;


 double *q_d, q_i[NEQ], q_f[NEQ];
 double *X_f;
 int Kind;
 double *SimTime;
 long long debut, fin;
double TimeSource;
