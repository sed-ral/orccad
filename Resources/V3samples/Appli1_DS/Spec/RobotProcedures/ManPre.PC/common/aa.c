/* sscc : C CODE OF SORTED EQUATIONS aa - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __aa_GENERIC_TEST(TEST) return TEST;
typedef void (*__aa_APF)();
static __aa_APF *__aa_PActionArray;

#include "aa.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _Man_controler_DEFINED
#ifndef Man_controler
extern void Man_controler(orcStrlPtr);
#endif
#endif
#ifndef _Man_fileparameter_DEFINED
#ifndef Man_fileparameter
extern void Man_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _Man_parameter_DEFINED
#ifndef Man_parameter
extern void Man_parameter(orcStrlPtr ,string ,string ,string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static orcStrlPtr __aa_V0;
static boolean __aa_V1;
static boolean __aa_V2;
static boolean __aa_V3;
static boolean __aa_V4;
static boolean __aa_V5;
static boolean __aa_V6;
static boolean __aa_V7;
static boolean __aa_V8;
static orcStrlPtr __aa_V9;
static boolean __aa_V10;
static boolean __aa_V11;
static boolean __aa_V12;
static boolean __aa_V13;
static boolean __aa_V14;
static boolean __aa_V15;
static boolean __aa_V16;
static boolean __aa_V17;
static boolean __aa_V18;
static orcStrlPtr __aa_V19;
static orcStrlPtr __aa_V20;
static orcStrlPtr __aa_V21;
static orcStrlPtr __aa_V22;
static orcStrlPtr __aa_V23;
static orcStrlPtr __aa_V24;
static orcStrlPtr __aa_V25;
static orcStrlPtr __aa_V26;
static orcStrlPtr __aa_V27;
static boolean __aa_V28;


/* INPUT FUNCTIONS */

void aa_I_Prr_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
void aa_I_CmdStopOK_Excavator () {
__aa_V2 = _true;
}
void aa_I_End_Man () {
__aa_V3 = _true;
}
void aa_I_ActivateOK_Excavator () {
__aa_V4 = _true;
}
void aa_I_USERSTART_Man () {
__aa_V5 = _true;
}
void aa_I_KillOK_Excavator () {
__aa_V6 = _true;
}
void aa_I_InitOK_Excavator () {
__aa_V7 = _true;
}
void aa_I_ManTimeoutPreCond () {
__aa_V8 = _true;
}
void aa_I_Man_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V9,__V);
__aa_V10 = _true;
}
void aa_I_Sing_OK () {
__aa_V11 = _true;
}
void aa_I_T2_Man () {
__aa_V12 = _true;
}
void aa_I_PreCond () {
__aa_V13 = _true;
}
void aa_I_ReadyToStop_Excavator () {
__aa_V14 = _true;
}
void aa_I_ROBOT_FAIL () {
__aa_V15 = _true;
}
void aa_I_SOFT_FAIL () {
__aa_V16 = _true;
}
void aa_I_SENSOR_FAIL () {
__aa_V17 = _true;
}
void aa_I_CPU_OVERLOAD () {
__aa_V18 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __aa_A1 \
__aa_V1
#define __aa_A2 \
__aa_V2
#define __aa_A3 \
__aa_V3
#define __aa_A4 \
__aa_V4
#define __aa_A5 \
__aa_V5
#define __aa_A6 \
__aa_V6
#define __aa_A7 \
__aa_V7
#define __aa_A8 \
__aa_V8
#define __aa_A9 \
__aa_V10
#define __aa_A10 \
__aa_V11
#define __aa_A11 \
__aa_V12
#define __aa_A12 \
__aa_V13
#define __aa_A13 \
__aa_V14
#define __aa_A14 \
__aa_V15
#define __aa_A15 \
__aa_V16
#define __aa_A16 \
__aa_V17
#define __aa_A17 \
__aa_V18

/* OUTPUT ACTIONS */

#define __aa_A18 \
aa_O_Activate(__aa_V19)
#define __aa_A19 \
aa_O_ActivateMan_Excavator(__aa_V20)
#define __aa_A20 \
aa_O_ManTransite(__aa_V21)
#define __aa_A21 \
aa_O_Abort_Man(__aa_V22)
#define __aa_A22 \
aa_O_STARTED_Man()
#define __aa_A23 \
aa_O_GoodEnd_Man()
#define __aa_A24 \
aa_O_GoodEnd_ManPre()
#define __aa_A25 \
aa_O_STARTED_ManPre()
#define __aa_A26 \
aa_O_Abort_ManPre()
#define __aa_A27 \
aa_O_TreatSing_OK(__aa_V23)
#define __aa_A28 \
aa_O_EndKill(__aa_V24)
#define __aa_A29 \
aa_O_FinBF(__aa_V25)
#define __aa_A30 \
aa_O_FinT3(__aa_V26)
#define __aa_A31 \
aa_O_GoodEndRPr()
#define __aa_A32 \
aa_O_T3RPr()

/* ASSIGNMENTS */

#define __aa_A33 \
__aa_V1 = _false
#define __aa_A34 \
__aa_V2 = _false
#define __aa_A35 \
__aa_V3 = _false
#define __aa_A36 \
__aa_V4 = _false
#define __aa_A37 \
__aa_V5 = _false
#define __aa_A38 \
__aa_V6 = _false
#define __aa_A39 \
__aa_V7 = _false
#define __aa_A40 \
__aa_V8 = _false
#define __aa_A41 \
__aa_V10 = _false
#define __aa_A42 \
__aa_V11 = _false
#define __aa_A43 \
__aa_V12 = _false
#define __aa_A44 \
__aa_V13 = _false
#define __aa_A45 \
__aa_V14 = _false
#define __aa_A46 \
__aa_V15 = _false
#define __aa_A47 \
__aa_V16 = _false
#define __aa_A48 \
__aa_V17 = _false
#define __aa_A49 \
__aa_V18 = _false
#define __aa_A50 \
_orcStrlPtr(&__aa_V24,__aa_V27)
#define __aa_A51 \
_orcStrlPtr(&__aa_V25,__aa_V0)
#define __aa_A52 \
_orcStrlPtr(&__aa_V26,__aa_V0)
#define __aa_A53 \
_orcStrlPtr(&__aa_V22,__aa_V9)
#define __aa_A54 \
_orcStrlPtr(&__aa_V20,__aa_V9)
#define __aa_A55 \
_orcStrlPtr(&__aa_V27,__aa_V9)
#define __aa_A56 \
_orcStrlPtr(&__aa_V21,__aa_V9)
#define __aa_A57 \
_orcStrlPtr(&__aa_V21,__aa_V9)
#define __aa_A58 \
_orcStrlPtr(&__aa_V23,__aa_V9)
#define __aa_A59 \
_orcStrlPtr(&__aa_V21,__aa_V9)
#define __aa_A60 \
_orcStrlPtr(&__aa_V21,__aa_V9)
#define __aa_A61 \
_orcStrlPtr(&__aa_V24,__aa_V27)

/* PROCEDURE CALLS */

#define __aa_A62 \
Man_parameter(__aa_V9,"SimTime","Kind","X_f")
#define __aa_A63 \
Man_controler(__aa_V9)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __aa_A64 \

#define __aa_A65 \

#define __aa_A66 \

#define __aa_A67 \

#define __aa_A68 \

#define __aa_A69 \

#define __aa_A70 \

#define __aa_A71 \

#define __aa_A72 \

#define __aa_A73 \

#define __aa_A74 \

#define __aa_A75 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int aa_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __aa__reset_input () {
__aa_V1 = _false;
__aa_V2 = _false;
__aa_V3 = _false;
__aa_V4 = _false;
__aa_V5 = _false;
__aa_V6 = _false;
__aa_V7 = _false;
__aa_V8 = _false;
__aa_V10 = _false;
__aa_V11 = _false;
__aa_V12 = _false;
__aa_V13 = _false;
__aa_V14 = _false;
__aa_V15 = _false;
__aa_V16 = _false;
__aa_V17 = _false;
__aa_V18 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __aa_R[18] = {_true,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int aa () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[99];
E[0] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3);
E[1] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__aa_A17);
E[2] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 16\n"),
#endif
__aa_A16);
E[3] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15);
E[4] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14);
E[5] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2);
E[6] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13);
E[7] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12);
E[8] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11);
E[9] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10);
E[10] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9);
E[11] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8);
E[12] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7);
E[13] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__aa_A6);
E[14] = __aa_R[9]&&!(__aa_R[0]);
E[15] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10)&&E[14];
E[16] = E[15];
E[17] = __aa_R[10]&&!(__aa_R[0]);
E[18] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3)&&E[17];
E[19] = E[18];
E[20] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5);
E[21] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4);
E[22] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9))&&__aa_R[0];
if (E[22]) {
__aa_A65;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A65\n");
#endif
}
E[23] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1))&&__aa_R[0];
if (E[23]) {
__aa_A64;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A64\n");
#endif
}
if (__aa_R[0]) {
__aa_A73;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A73\n");
#endif
}
if (__aa_R[0]) {
__aa_A72;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A72\n");
#endif
}
if (__aa_R[0]) {
__aa_A71;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A71\n");
#endif
}
if (__aa_R[0]) {
__aa_A70;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A70\n");
#endif
}
if (__aa_R[0]) {
__aa_A69;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A69\n");
#endif
}
if (__aa_R[0]) {
__aa_A68;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A68\n");
#endif
}
if (__aa_R[0]) {
__aa_A67;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A67\n");
#endif
}
if (__aa_R[0]) {
__aa_A66;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A66\n");
#endif
}
if (__aa_R[0]) {
__aa_A75;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A75\n");
#endif
}
if (__aa_R[0]) {
__aa_A74;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A74\n");
#endif
}
E[24] = __aa_R[2]&&!(__aa_R[0]);
E[25] = E[24]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9);
E[26] = __aa_R[3]&&!(__aa_R[0]);
E[27] = E[26]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1);
E[28] = __aa_R[3]||__aa_R[2];
E[29] = (E[28]&&!(__aa_R[2]))||E[25];
E[30] = (E[28]&&!(__aa_R[3]))||E[27];
E[25] = (E[27]||E[25])&&E[30]&&E[29];
E[27] = __aa_R[11]&&!(__aa_R[0]);
E[31] = (E[27]&&E[25])||(E[25]&&__aa_R[0]);
E[32] = E[31];
if (E[31]) {
__aa_A25;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A25\n");
#endif
}
E[33] = E[25];
E[34] = E[25];
E[35] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1);
if (E[31]) {
__aa_A62;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A62\n");
#endif
}
if (E[31]) {
__aa_A63;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A63\n");
#endif
}
E[36] = E[31];
if (E[15]) {
__aa_A58;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A58\n");
#endif
}
if (E[15]) {
__aa_A27;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A27\n");
#endif
}
E[37] = _false;
E[38] = _false;
E[39] = _false;
E[40] = _false;
E[41] = _false;
E[42] = _false;
E[43] = __aa_R[6]||__aa_R[5];
E[44] = E[43]&&!(__aa_R[0]);
E[45] = E[44]&&__aa_R[5];
E[46] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8))&&E[45];
E[46] = E[46]&&__aa_R[5];
E[47] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12)&&E[46];
E[44] = E[44]&&__aa_R[6];
E[48] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7)&&E[44];
E[49] = (E[43]&&!(__aa_R[5]))||E[47];
E[50] = (E[43]&&!(__aa_R[6]))||E[48];
E[47] = E[50]&&(E[48]||E[47])&&E[49];
E[48] = __aa_R[14]&&!(__aa_R[0]);
E[51] = E[48]&&E[47];
E[52] = __aa_R[17]&&!(__aa_R[0]);
E[53] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2)&&E[52];
E[54] = E[53]||E[51];
E[55] = __aa_R[7]&&!(__aa_R[0])&&__aa_R[7];
E[56] = (E[54]&&E[47])||(E[55]&&E[54]);
if (E[56]) {
__aa_A54;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A54\n");
#endif
}
if (E[56]) {
__aa_A55;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A55\n");
#endif
}
E[57] = __aa_R[16]&&!(__aa_R[0]);
E[58] = E[57]&&E[47];
if (E[58]) {
__aa_A61;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A61\n");
#endif
}
E[59] = __aa_R[9]||__aa_R[8]||__aa_R[10];
E[60] = (__aa_R[8]&&!(__aa_R[0])&&__aa_R[8])||E[56];
E[61] = (E[59]&&!(__aa_R[8]))||E[60];
E[14] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10))&&E[14];
E[14] = (E[14]&&__aa_R[9])||E[15]||E[56];
E[62] = (E[59]&&!(__aa_R[9]))||E[14];
E[17] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3))&&E[17];
E[17] = (E[17]&&__aa_R[10])||E[56];
E[63] = (E[59]&&!(__aa_R[10]))||E[17];
E[18] = E[62]&&E[61]&&(E[18]||E[63])&&E[18];
if (E[18]) {
__aa_A59;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A59\n");
#endif
}
if (_false) {
__aa_A56;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A56\n");
#endif
}
if (_false) {
__aa_A57;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A57\n");
#endif
}
if (_false) {
__aa_A53;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A53\n");
#endif
}
E[48] = (E[48]&&!(E[47])&&__aa_R[14])||__aa_R[0];
E[64] = __aa_R[12]&&!(__aa_R[0]);
E[65] = E[64]&&!(E[18]);
E[45] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8)&&E[45];
E[44] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7))&&E[44];
E[44] = E[44]&&__aa_R[6];
E[50] = E[44]||E[50];
E[46] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12))&&E[46];
E[46] = E[46]&&__aa_R[5];
E[49] = E[46]||E[49];
E[66] = E[50]&&(E[45]||E[49])&&E[45];
E[67] = E[66]&&E[65];
E[68] = __aa_R[12]||__aa_R[13];
E[69] = (__aa_R[13]&&!(__aa_R[0])&&__aa_R[13])||E[31];
E[70] = (E[68]&&!(__aa_R[13]))||E[69];
E[64] = E[64]&&E[18];
E[65] = E[31]||(!(E[66])&&E[65]&&__aa_R[12]);
E[71] = (E[68]&&!(__aa_R[12]))||E[65];
E[72] = E[64]||E[71];
E[73] = E[70]&&(E[67]||E[72])&&E[67];
E[72] = E[70]&&E[72]&&E[64];
E[74] = __aa_R[1]&&!(__aa_R[0]);
E[75] = E[74]&&!(E[72]);
E[76] = E[75]&&E[73];
E[75] = E[75]&&!(E[73]);
E[77] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14)&&E[75];
E[75] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14))&&E[75];
E[78] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15)&&E[75];
E[75] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15))&&E[75];
E[79] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 16\n"),
#endif
__aa_A16)&&E[75];
E[75] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 16\n"),
#endif
__aa_A16))&&E[75];
E[80] = (
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__aa_A17)&&E[75];
E[81] = E[80]||E[79]||E[78]||E[77]||E[76];
E[74] = E[74]&&E[72];
E[43] = __aa_R[4]||E[59]||__aa_R[7]||E[43];
E[68] = E[68]||__aa_R[11];
E[59] = __aa_R[16]||__aa_R[15]||__aa_R[17]||__aa_R[14];
E[82] = E[59]||E[68]||E[43]||__aa_R[1]||E[28];
E[75] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__aa_A17))&&E[75];
E[75] = (E[75]&&__aa_R[1])||__aa_R[0];
E[83] = (E[82]&&!(__aa_R[1]))||E[75];
E[84] = E[74]||E[83];
E[27] = (E[27]&&!(E[25])&&__aa_R[11])||(!(E[25])&&__aa_R[0]);
E[71] = E[27]||((E[69]||E[65])&&E[70]&&E[71]);
E[68] = E[71]||(E[82]&&!(E[68]))||E[72]||E[73];
E[70] = __aa_R[4]&&!(__aa_R[0]);
E[85] = E[18]||__aa_R[0];
E[86] = (E[70]&&E[31])||(E[85]&&E[31]);
E[85] = (E[70]&&!(E[31])&&__aa_R[4])||(E[85]&&!(E[31]));
E[55] = (!(E[54])&&E[47])||(E[55]&&!(E[54])&&__aa_R[7]);
E[63] = ((E[44]||E[46])&&E[50]&&E[49])||E[55]||((E[14]||E[60]||E[17])&&E[62]&&E[61]&&E[63])||E[85]||E[86];
E[43] = (E[82]&&!(E[43]))||E[66]||E[63];
E[61] = __aa_R[15]&&!(__aa_R[0]);
E[51] = (E[61]&&!(E[18])&&__aa_R[15])||E[53]||E[51];
E[61] = (E[57]&&!(E[47])&&__aa_R[16])||(E[61]&&E[18]);
E[52] = !((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2))&&E[52];
E[52] = (E[52]&&__aa_R[17])||E[58];
E[59] = (E[82]&&!(E[59]))||E[48]||E[52]||E[61]||E[51];
E[24] = E[24]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9));
E[24] = (E[24]&&__aa_R[2])||__aa_R[0];
E[26] = E[26]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1));
E[26] = (E[26]&&__aa_R[3])||__aa_R[0];
E[29] = (E[26]||E[30])&&(E[24]||E[29])&&(E[26]||E[24]);
E[25] = (E[82]&&!(E[28]))||E[25]||E[29];
E[76] = E[25]&&E[59]&&E[43]&&E[68]&&(E[84]||E[80]||E[79]||E[78]||E[77]||E[76])&&E[81];
E[84] = E[25]&&E[59]&&E[43]&&E[68]&&E[84]&&E[74];
E[77] = E[84]||E[76];
E[64] = E[64];
E[78] = E[18];
E[79] = E[18];
E[80] = E[18];
if (E[18]) {
__aa_A23;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A23\n");
#endif
}
E[28] = _true;
if (E[18]) {
__aa_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A20\n");
#endif
}
E[82] = E[18];
E[30] = E[56];
if (E[56]) {
__aa_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A19\n");
#endif
}
E[57] = E[56];
E[54] = E[54];
E[47] = E[47];
E[74] = E[74];
E[53] = E[72];
E[62] = E[72];
if (E[72]) {
__aa_A24;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A24\n");
#endif
}
E[81] = E[81];
E[49] = E[73];
E[67] = E[67];
E[50] = E[66];
E[45] = E[45];
if (E[76]) {
__aa_A52;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A52\n");
#endif
}
E[70] = E[76];
if (E[76]) {
__aa_A32;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A32\n");
#endif
}
if (E[76]) {
__aa_A30;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A30\n");
#endif
}
E[87] = E[76];
if (E[84]) {
__aa_A50;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A50\n");
#endif
}
if (E[84]) {
__aa_A51;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A51\n");
#endif
}
E[88] = E[77];
E[89] = E[84];
if (E[84]) {
__aa_A31;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A31\n");
#endif
}
if (E[84]) {
__aa_A29;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A29\n");
#endif
}
E[90] = E[84];
E[73] = E[77]||E[73]||E[72]||E[73];
E[91] = E[77]||E[66];
E[92] = E[91]||E[18];
E[66] = E[91]||E[66];
E[93] = !(E[77])&&E[86];
E[94] = !(E[84])||!(E[58]);
E[95] = E[84]||E[58];
if (E[95]) {
__aa_A28;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A28\n");
#endif
}
E[96] = E[95];
E[83] = E[25]&&(E[29]||E[48]||E[52]||E[61]||E[51]||E[63]||E[71]||E[75])&&E[59]&&E[43]&&E[68]&&E[83];
E[68] = E[86];
if (E[86]) {
__aa_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A22\n");
#endif
}
E[43] = _false;
if (_false) {
__aa_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A21\n");
#endif
}
E[59] = _false;
E[71] = _false;
E[63] = _false;
if (_false) {
__aa_A26;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A26\n");
#endif
}
E[29] = _false;
E[25] = _false;
if (_false) {
__aa_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A18\n");
#endif
}
E[97] = _false;
E[98] = _false;
__aa_R[1] = !(E[77])&&E[75];
__aa_R[2] = !(E[77])&&E[24];
__aa_R[3] = !(E[77])&&E[26];
__aa_R[4] = !(E[77])&&E[85];
__aa_R[5] = E[93]||(!(E[66])&&E[46]);
__aa_R[6] = E[93]||(!(E[66])&&E[44]);
__aa_R[7] = E[55]&&!(E[91]);
__aa_R[8] = !(E[92])&&E[60];
__aa_R[9] = !(E[92])&&E[14];
__aa_R[10] = !(E[92])&&E[17];
__aa_R[11] = !(E[77])&&E[27];
__aa_R[12] = !(E[73])&&E[65];
__aa_R[13] = !(E[73])&&E[69];
__aa_R[14] = !(E[77])&&E[48];
__aa_R[15] = !(E[77])&&E[51];
__aa_R[16] = !(E[77])&&E[61];
__aa_R[17] = !(E[77])&&E[52];
__aa_R[0] = E[98];
__aa__reset_input();
return E[83];
}

/* AUTOMATON RESET */

int aa_reset () {
__aa_R[0] = _true;
__aa_R[1] = _false;
__aa_R[2] = _false;
__aa_R[3] = _false;
__aa_R[4] = _false;
__aa_R[5] = _false;
__aa_R[6] = _false;
__aa_R[7] = _false;
__aa_R[8] = _false;
__aa_R[9] = _false;
__aa_R[10] = _false;
__aa_R[11] = _false;
__aa_R[12] = _false;
__aa_R[13] = _false;
__aa_R[14] = _false;
__aa_R[15] = _false;
__aa_R[16] = _false;
__aa_R[17] = _false;
__aa__reset_input();
return 0;
}
