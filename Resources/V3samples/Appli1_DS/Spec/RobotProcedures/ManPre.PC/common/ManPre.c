/* sscc : C CODE OF SORTED EQUATIONS ManPre - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __ManPre_GENERIC_TEST(TEST) return TEST;
typedef void (*__ManPre_APF)();
static __ManPre_APF *__ManPre_PActionArray;

#include "ManPre.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _Man_controler_DEFINED
#ifndef Man_controler
extern void Man_controler(orcStrlPtr);
#endif
#endif
#ifndef _Man_fileparameter_DEFINED
#ifndef Man_fileparameter
extern void Man_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _Man_parameter_DEFINED
#ifndef Man_parameter
extern void Man_parameter(orcStrlPtr ,string ,string ,string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __ManPre_V0;
static boolean __ManPre_V1;
static orcStrlPtr __ManPre_V2;
static boolean __ManPre_V3;
static boolean __ManPre_V4;
static boolean __ManPre_V5;
static boolean __ManPre_V6;


/* INPUT FUNCTIONS */

void ManPre_I_START_ManPre () {
__ManPre_V0 = _true;
}
void ManPre_I_Abort_Local_ManPre () {
__ManPre_V1 = _true;
}
void ManPre_I_Man_Start (orcStrlPtr __V) {
_orcStrlPtr(&__ManPre_V2,__V);
__ManPre_V3 = _true;
}
void ManPre_I_USERSTART_Man () {
__ManPre_V4 = _true;
}
void ManPre_I_BF_Man () {
__ManPre_V5 = _true;
}
void ManPre_I_T3_Man () {
__ManPre_V6 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __ManPre_A1 \
__ManPre_V0
#define __ManPre_A2 \
__ManPre_V1
#define __ManPre_A3 \
__ManPre_V3
#define __ManPre_A4 \
__ManPre_V4
#define __ManPre_A5 \
__ManPre_V5
#define __ManPre_A6 \
__ManPre_V6

/* OUTPUT ACTIONS */

#define __ManPre_A7 \
ManPre_O_BF_ManPre()
#define __ManPre_A8 \
ManPre_O_STARTED_ManPre()
#define __ManPre_A9 \
ManPre_O_GoodEnd_ManPre()
#define __ManPre_A10 \
ManPre_O_Abort_ManPre()
#define __ManPre_A11 \
ManPre_O_T3_ManPre()
#define __ManPre_A12 \
ManPre_O_START_Man()
#define __ManPre_A13 \
ManPre_O_Abort_Local_Man()

/* ASSIGNMENTS */

#define __ManPre_A14 \
__ManPre_V0 = _false
#define __ManPre_A15 \
__ManPre_V1 = _false
#define __ManPre_A16 \
__ManPre_V3 = _false
#define __ManPre_A17 \
__ManPre_V4 = _false
#define __ManPre_A18 \
__ManPre_V5 = _false
#define __ManPre_A19 \
__ManPre_V6 = _false

/* PROCEDURE CALLS */

#define __ManPre_A20 \
Man_parameter(__ManPre_V2,"SimTime","Kind","X_f")
#define __ManPre_A21 \
Man_controler(__ManPre_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __ManPre_A22 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int ManPre_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __ManPre__reset_input () {
__ManPre_V0 = _false;
__ManPre_V1 = _false;
__ManPre_V3 = _false;
__ManPre_V4 = _false;
__ManPre_V5 = _false;
__ManPre_V6 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __ManPre_R[4] = {_true,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int ManPre () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = __ManPre_R[2]&&!(__ManPre_R[0]);
E[1] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__ManPre_A5);
E[2] = __ManPre_R[2]||__ManPre_R[3];
E[3] = __ManPre_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__ManPre_A3));
if (E[3]) {
__ManPre_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__ManPre_A22\n");
#endif
}
E[4] = __ManPre_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ManPre_A1);
E[5] = __ManPre_R[1]&&!(__ManPre_R[0]);
E[6] = E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ManPre_A1);
E[6] = E[4]||E[6];
if (E[6]) {
__ManPre_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__ManPre_A20\n");
#endif
}
if (E[6]) {
__ManPre_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__ManPre_A21\n");
#endif
}
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__ManPre_A5));
E[4] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__ManPre_A6));
E[4] = E[6]||(__ManPre_R[2]&&E[4]);
E[7] = (E[2]&&!(__ManPre_R[2]))||E[4];
E[8] = E[7]||E[1];
E[9] = __ManPre_R[3]&&!(__ManPre_R[0]);
E[10] = E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__ManPre_A2));
E[10] = E[6]||(__ManPre_R[3]&&E[10]);
E[11] = (E[2]&&!(__ManPre_R[3]))||E[10];
E[1] = E[1]&&E[8]&&E[11];
if (E[1]) {
__ManPre_A7;
#ifdef TRACE_ACTION
fprintf(stderr, "__ManPre_A7\n");
#endif
}
if (E[6]) {
__ManPre_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__ManPre_A8\n");
#endif
}
if (E[1]) {
__ManPre_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__ManPre_A9\n");
#endif
}
E[9] = E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__ManPre_A2);
if (E[9]) {
__ManPre_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__ManPre_A10\n");
#endif
}
E[0] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__ManPre_A6);
E[8] = E[8]||E[0];
E[0] = E[0]&&E[8]&&E[11];
if (E[0]) {
__ManPre_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__ManPre_A11\n");
#endif
}
if (E[6]) {
__ManPre_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__ManPre_A12\n");
#endif
}
if (E[9]) {
__ManPre_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__ManPre_A13\n");
#endif
}
E[8] = E[9]&&E[8]&&(E[11]||E[9]);
E[12] = E[1]||E[0]||E[8];
E[13] = __ManPre_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ManPre_A1));
E[5] = E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ManPre_A1));
E[5] = E[13]||(__ManPre_R[1]&&E[5]);
E[11] = ((E[4]||E[10])&&E[7]&&E[11])||E[5];
E[2] = E[2]||__ManPre_R[1];
E[8] = E[8]||E[0]||E[1]||E[0]||E[8];
__ManPre_R[2] = E[4]&&!(E[8]);
__ManPre_R[3] = E[10]&&!(E[8]);
__ManPre_R[0] = !(_true);
__ManPre_R[1] = E[5];
__ManPre__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int ManPre_reset () {
__ManPre_R[0] = _true;
__ManPre_R[1] = _false;
__ManPre_R[2] = _false;
__ManPre_R[3] = _false;
__ManPre__reset_input();
return 0;
}
