/* sscc : C CODE OF SORTED EQUATIONS Tld2Sim - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __Tld2Sim_GENERIC_TEST(TEST) return TEST;
typedef void (*__Tld2Sim_APF)();
static __Tld2Sim_APF *__Tld2Sim_PActionArray;

#include "Tld2Sim.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _Tld2_controler_DEFINED
#ifndef Tld2_controler
extern void Tld2_controler(orcStrlPtr);
#endif
#endif
#ifndef _Tld2_fileparameter_DEFINED
#ifndef Tld2_fileparameter
extern void Tld2_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _Tld2_parameter_DEFINED
#ifndef Tld2_parameter
extern void Tld2_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __Tld2Sim_V0;
static boolean __Tld2Sim_V1;
static orcStrlPtr __Tld2Sim_V2;
static boolean __Tld2Sim_V3;
static boolean __Tld2Sim_V4;
static boolean __Tld2Sim_V5;
static boolean __Tld2Sim_V6;
static boolean __Tld2Sim_V7;


/* INPUT FUNCTIONS */

void Tld2Sim_I_START_Tld2Sim () {
__Tld2Sim_V0 = _true;
}
void Tld2Sim_I_Abort_Local_Tld2Sim () {
__Tld2Sim_V1 = _true;
}
void Tld2Sim_I_Tld2_Start (orcStrlPtr __V) {
_orcStrlPtr(&__Tld2Sim_V2,__V);
__Tld2Sim_V3 = _true;
}
void Tld2Sim_I_USERSTART_Tld2 () {
__Tld2Sim_V4 = _true;
}
void Tld2Sim_I_BF_Tld2 () {
__Tld2Sim_V5 = _true;
}
void Tld2Sim_I_T3_Tld2 () {
__Tld2Sim_V6 = _true;
}
void Tld2Sim_I_T2_Sing () {
__Tld2Sim_V7 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __Tld2Sim_A1 \
__Tld2Sim_V0
#define __Tld2Sim_A2 \
__Tld2Sim_V1
#define __Tld2Sim_A3 \
__Tld2Sim_V3
#define __Tld2Sim_A4 \
__Tld2Sim_V4
#define __Tld2Sim_A5 \
__Tld2Sim_V5
#define __Tld2Sim_A6 \
__Tld2Sim_V6
#define __Tld2Sim_A7 \
__Tld2Sim_V7

/* OUTPUT ACTIONS */

#define __Tld2Sim_A8 \
Tld2Sim_O_BF_Tld2Sim()
#define __Tld2Sim_A9 \
Tld2Sim_O_STARTED_Tld2Sim()
#define __Tld2Sim_A10 \
Tld2Sim_O_GoodEnd_Tld2Sim()
#define __Tld2Sim_A11 \
Tld2Sim_O_Abort_Tld2Sim()
#define __Tld2Sim_A12 \
Tld2Sim_O_T3_Tld2Sim()
#define __Tld2Sim_A13 \
Tld2Sim_O_START_Tld2()
#define __Tld2Sim_A14 \
Tld2Sim_O_Abort_Local_Tld2()

/* ASSIGNMENTS */

#define __Tld2Sim_A15 \
__Tld2Sim_V0 = _false
#define __Tld2Sim_A16 \
__Tld2Sim_V1 = _false
#define __Tld2Sim_A17 \
__Tld2Sim_V3 = _false
#define __Tld2Sim_A18 \
__Tld2Sim_V4 = _false
#define __Tld2Sim_A19 \
__Tld2Sim_V5 = _false
#define __Tld2Sim_A20 \
__Tld2Sim_V6 = _false
#define __Tld2Sim_A21 \
__Tld2Sim_V7 = _false

/* PROCEDURE CALLS */

#define __Tld2Sim_A22 \
Tld2_parameter(__Tld2Sim_V2)
#define __Tld2Sim_A23 \
Tld2_controler(__Tld2Sim_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __Tld2Sim_A24 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int Tld2Sim_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __Tld2Sim__reset_input () {
__Tld2Sim_V0 = _false;
__Tld2Sim_V1 = _false;
__Tld2Sim_V3 = _false;
__Tld2Sim_V4 = _false;
__Tld2Sim_V5 = _false;
__Tld2Sim_V6 = _false;
__Tld2Sim_V7 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __Tld2Sim_R[4] = {_true,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int Tld2Sim () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = __Tld2Sim_R[2]&&!(__Tld2Sim_R[0]);
E[1] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Tld2Sim_A5);
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Tld2Sim_A5));
E[2] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__Tld2Sim_A7);
E[2] = E[1]||E[2];
E[1] = __Tld2Sim_R[2]||__Tld2Sim_R[3];
E[3] = __Tld2Sim_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__Tld2Sim_A3));
if (E[3]) {
__Tld2Sim_A24;
#ifdef TRACE_ACTION
fprintf(stderr, "__Tld2Sim_A24\n");
#endif
}
E[4] = __Tld2Sim_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Tld2Sim_A1);
E[5] = __Tld2Sim_R[1]&&!(__Tld2Sim_R[0]);
E[6] = E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Tld2Sim_A1);
E[6] = E[4]||E[6];
if (E[6]) {
__Tld2Sim_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__Tld2Sim_A22\n");
#endif
}
if (E[6]) {
__Tld2Sim_A23;
#ifdef TRACE_ACTION
fprintf(stderr, "__Tld2Sim_A23\n");
#endif
}
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__Tld2Sim_A7));
E[4] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Tld2Sim_A6));
E[4] = E[6]||(__Tld2Sim_R[2]&&E[4]);
E[7] = (E[1]&&!(__Tld2Sim_R[2]))||E[4];
E[8] = E[7]||E[2];
E[9] = __Tld2Sim_R[3]&&!(__Tld2Sim_R[0]);
E[10] = E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Tld2Sim_A2));
E[10] = E[6]||(__Tld2Sim_R[3]&&E[10]);
E[11] = (E[1]&&!(__Tld2Sim_R[3]))||E[10];
E[2] = E[2]&&E[8]&&E[11];
if (E[2]) {
__Tld2Sim_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__Tld2Sim_A8\n");
#endif
}
if (E[6]) {
__Tld2Sim_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__Tld2Sim_A9\n");
#endif
}
if (E[2]) {
__Tld2Sim_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__Tld2Sim_A10\n");
#endif
}
E[9] = E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Tld2Sim_A2);
if (E[9]) {
__Tld2Sim_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__Tld2Sim_A11\n");
#endif
}
E[0] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Tld2Sim_A6);
E[8] = E[8]||E[0];
E[0] = E[0]&&E[8]&&E[11];
if (E[0]) {
__Tld2Sim_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__Tld2Sim_A12\n");
#endif
}
if (E[6]) {
__Tld2Sim_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__Tld2Sim_A13\n");
#endif
}
if (E[9]) {
__Tld2Sim_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__Tld2Sim_A14\n");
#endif
}
E[8] = E[9]&&E[8]&&(E[11]||E[9]);
E[12] = E[2]||E[0]||E[8];
E[13] = __Tld2Sim_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Tld2Sim_A1));
E[5] = E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Tld2Sim_A1));
E[5] = E[13]||(__Tld2Sim_R[1]&&E[5]);
E[11] = ((E[4]||E[10])&&E[7]&&E[11])||E[5];
E[1] = E[1]||__Tld2Sim_R[1];
E[8] = E[8]||E[0]||E[2]||E[0]||E[8];
__Tld2Sim_R[2] = E[4]&&!(E[8]);
__Tld2Sim_R[3] = E[10]&&!(E[8]);
__Tld2Sim_R[0] = !(_true);
__Tld2Sim_R[1] = E[5];
__Tld2Sim__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int Tld2Sim_reset () {
__Tld2Sim_R[0] = _true;
__Tld2Sim_R[1] = _false;
__Tld2Sim_R[2] = _false;
__Tld2Sim_R[3] = _false;
__Tld2Sim__reset_input();
return 0;
}
