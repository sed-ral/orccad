/* sscc : C CODE OF SORTED EQUATIONS Scalar - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __Scalar_GENERIC_TEST(TEST) return TEST;
typedef void (*__Scalar_APF)();
static __Scalar_APF *__Scalar_PActionArray;

#include "Scalar.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _Tld_Scalar_controler_DEFINED
#ifndef Tld_Scalar_controler
extern void Tld_Scalar_controler(orcStrlPtr);
#endif
#endif
#ifndef _Tld_Scalar_fileparameter_DEFINED
#ifndef Tld_Scalar_fileparameter
extern void Tld_Scalar_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _Tld_Scalar_parameter_DEFINED
#ifndef Tld_Scalar_parameter
extern void Tld_Scalar_parameter(orcStrlPtr ,string ,string ,string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __Scalar_V0;
static boolean __Scalar_V1;
static orcStrlPtr __Scalar_V2;
static boolean __Scalar_V3;
static boolean __Scalar_V4;
static boolean __Scalar_V5;
static boolean __Scalar_V6;
static boolean __Scalar_V7;


/* INPUT FUNCTIONS */

void Scalar_I_START_Scalar () {
__Scalar_V0 = _true;
}
void Scalar_I_Abort_Local_Scalar () {
__Scalar_V1 = _true;
}
void Scalar_I_Tld_Scalar_Start (orcStrlPtr __V) {
_orcStrlPtr(&__Scalar_V2,__V);
__Scalar_V3 = _true;
}
void Scalar_I_USERSTART_Tld_Scalar () {
__Scalar_V4 = _true;
}
void Scalar_I_BF_Tld_Scalar () {
__Scalar_V5 = _true;
}
void Scalar_I_T3_Tld_Scalar () {
__Scalar_V6 = _true;
}
void Scalar_I_T2_Sing () {
__Scalar_V7 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __Scalar_A1 \
__Scalar_V0
#define __Scalar_A2 \
__Scalar_V1
#define __Scalar_A3 \
__Scalar_V3
#define __Scalar_A4 \
__Scalar_V4
#define __Scalar_A5 \
__Scalar_V5
#define __Scalar_A6 \
__Scalar_V6
#define __Scalar_A7 \
__Scalar_V7

/* OUTPUT ACTIONS */

#define __Scalar_A8 \
Scalar_O_BF_Scalar()
#define __Scalar_A9 \
Scalar_O_STARTED_Scalar()
#define __Scalar_A10 \
Scalar_O_GoodEnd_Scalar()
#define __Scalar_A11 \
Scalar_O_Abort_Scalar()
#define __Scalar_A12 \
Scalar_O_T3_Scalar()
#define __Scalar_A13 \
Scalar_O_START_Tld_Scalar()
#define __Scalar_A14 \
Scalar_O_Abort_Local_Tld_Scalar()

/* ASSIGNMENTS */

#define __Scalar_A15 \
__Scalar_V0 = _false
#define __Scalar_A16 \
__Scalar_V1 = _false
#define __Scalar_A17 \
__Scalar_V3 = _false
#define __Scalar_A18 \
__Scalar_V4 = _false
#define __Scalar_A19 \
__Scalar_V5 = _false
#define __Scalar_A20 \
__Scalar_V6 = _false
#define __Scalar_A21 \
__Scalar_V7 = _false

/* PROCEDURE CALLS */

#define __Scalar_A22 \
Tld_Scalar_parameter(__Scalar_V2,"SimTime","Kind","X_f")
#define __Scalar_A23 \
Tld_Scalar_controler(__Scalar_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __Scalar_A24 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int Scalar_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __Scalar__reset_input () {
__Scalar_V0 = _false;
__Scalar_V1 = _false;
__Scalar_V3 = _false;
__Scalar_V4 = _false;
__Scalar_V5 = _false;
__Scalar_V6 = _false;
__Scalar_V7 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __Scalar_R[4] = {_true,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int Scalar () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = __Scalar_R[2]&&!(__Scalar_R[0]);
E[1] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Scalar_A5);
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Scalar_A5));
E[2] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__Scalar_A7);
E[2] = E[1]||E[2];
E[1] = __Scalar_R[2]||__Scalar_R[3];
E[3] = __Scalar_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__Scalar_A3));
if (E[3]) {
__Scalar_A24;
#ifdef TRACE_ACTION
fprintf(stderr, "__Scalar_A24\n");
#endif
}
E[4] = __Scalar_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Scalar_A1);
E[5] = __Scalar_R[1]&&!(__Scalar_R[0]);
E[6] = E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Scalar_A1);
E[6] = E[4]||E[6];
if (E[6]) {
__Scalar_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__Scalar_A22\n");
#endif
}
if (E[6]) {
__Scalar_A23;
#ifdef TRACE_ACTION
fprintf(stderr, "__Scalar_A23\n");
#endif
}
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__Scalar_A7));
E[4] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Scalar_A6));
E[4] = E[6]||(__Scalar_R[2]&&E[4]);
E[7] = (E[1]&&!(__Scalar_R[2]))||E[4];
E[8] = E[7]||E[2];
E[9] = __Scalar_R[3]&&!(__Scalar_R[0]);
E[10] = E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Scalar_A2));
E[10] = E[6]||(__Scalar_R[3]&&E[10]);
E[11] = (E[1]&&!(__Scalar_R[3]))||E[10];
E[2] = E[2]&&E[8]&&E[11];
if (E[2]) {
__Scalar_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__Scalar_A8\n");
#endif
}
if (E[6]) {
__Scalar_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__Scalar_A9\n");
#endif
}
if (E[2]) {
__Scalar_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__Scalar_A10\n");
#endif
}
E[9] = E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Scalar_A2);
if (E[9]) {
__Scalar_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__Scalar_A11\n");
#endif
}
E[0] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Scalar_A6);
E[8] = E[8]||E[0];
E[0] = E[0]&&E[8]&&E[11];
if (E[0]) {
__Scalar_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__Scalar_A12\n");
#endif
}
if (E[6]) {
__Scalar_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__Scalar_A13\n");
#endif
}
if (E[9]) {
__Scalar_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__Scalar_A14\n");
#endif
}
E[8] = E[9]&&E[8]&&(E[11]||E[9]);
E[12] = E[2]||E[0]||E[8];
E[13] = __Scalar_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Scalar_A1));
E[5] = E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Scalar_A1));
E[5] = E[13]||(__Scalar_R[1]&&E[5]);
E[11] = ((E[4]||E[10])&&E[7]&&E[11])||E[5];
E[1] = E[1]||__Scalar_R[1];
E[8] = E[8]||E[0]||E[2]||E[0]||E[8];
__Scalar_R[2] = E[4]&&!(E[8]);
__Scalar_R[3] = E[10]&&!(E[8]);
__Scalar_R[0] = !(_true);
__Scalar_R[1] = E[5];
__Scalar__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int Scalar_reset () {
__Scalar_R[0] = _true;
__Scalar_R[1] = _false;
__Scalar_R[2] = _false;
__Scalar_R[3] = _false;
__Scalar__reset_input();
return 0;
}
