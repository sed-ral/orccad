/* sscc : C CODE OF SORTED EQUATIONS TldCPP - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __TldCPP_GENERIC_TEST(TEST) return TEST;
typedef void (*__TldCPP_APF)();
static __TldCPP_APF *__TldCPP_PActionArray;

#include "TldCPP.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _Tldcpp_controler_DEFINED
#ifndef Tldcpp_controler
extern void Tldcpp_controler(orcStrlPtr);
#endif
#endif
#ifndef _Tldcpp_fileparameter_DEFINED
#ifndef Tldcpp_fileparameter
extern void Tldcpp_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _Tldcpp_parameter_DEFINED
#ifndef Tldcpp_parameter
extern void Tldcpp_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __TldCPP_V0;
static boolean __TldCPP_V1;
static orcStrlPtr __TldCPP_V2;
static boolean __TldCPP_V3;
static boolean __TldCPP_V4;
static boolean __TldCPP_V5;
static boolean __TldCPP_V6;
static boolean __TldCPP_V7;


/* INPUT FUNCTIONS */

void TldCPP_I_START_TldCPP () {
__TldCPP_V0 = _true;
}
void TldCPP_I_Abort_Local_TldCPP () {
__TldCPP_V1 = _true;
}
void TldCPP_I_Tldcpp_Start (orcStrlPtr __V) {
_orcStrlPtr(&__TldCPP_V2,__V);
__TldCPP_V3 = _true;
}
void TldCPP_I_USERSTART_Tldcpp () {
__TldCPP_V4 = _true;
}
void TldCPP_I_BF_Tldcpp () {
__TldCPP_V5 = _true;
}
void TldCPP_I_T3_Tldcpp () {
__TldCPP_V6 = _true;
}
void TldCPP_I_T2_Sing () {
__TldCPP_V7 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __TldCPP_A1 \
__TldCPP_V0
#define __TldCPP_A2 \
__TldCPP_V1
#define __TldCPP_A3 \
__TldCPP_V3
#define __TldCPP_A4 \
__TldCPP_V4
#define __TldCPP_A5 \
__TldCPP_V5
#define __TldCPP_A6 \
__TldCPP_V6
#define __TldCPP_A7 \
__TldCPP_V7

/* OUTPUT ACTIONS */

#define __TldCPP_A8 \
TldCPP_O_BF_TldCPP()
#define __TldCPP_A9 \
TldCPP_O_STARTED_TldCPP()
#define __TldCPP_A10 \
TldCPP_O_GoodEnd_TldCPP()
#define __TldCPP_A11 \
TldCPP_O_Abort_TldCPP()
#define __TldCPP_A12 \
TldCPP_O_T3_TldCPP()
#define __TldCPP_A13 \
TldCPP_O_START_Tldcpp()
#define __TldCPP_A14 \
TldCPP_O_Abort_Local_Tldcpp()

/* ASSIGNMENTS */

#define __TldCPP_A15 \
__TldCPP_V0 = _false
#define __TldCPP_A16 \
__TldCPP_V1 = _false
#define __TldCPP_A17 \
__TldCPP_V3 = _false
#define __TldCPP_A18 \
__TldCPP_V4 = _false
#define __TldCPP_A19 \
__TldCPP_V5 = _false
#define __TldCPP_A20 \
__TldCPP_V6 = _false
#define __TldCPP_A21 \
__TldCPP_V7 = _false

/* PROCEDURE CALLS */

#define __TldCPP_A22 \
Tldcpp_parameter(__TldCPP_V2)
#define __TldCPP_A23 \
Tldcpp_controler(__TldCPP_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __TldCPP_A24 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int TldCPP_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __TldCPP__reset_input () {
__TldCPP_V0 = _false;
__TldCPP_V1 = _false;
__TldCPP_V3 = _false;
__TldCPP_V4 = _false;
__TldCPP_V5 = _false;
__TldCPP_V6 = _false;
__TldCPP_V7 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __TldCPP_R[4] = {_true,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int TldCPP () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = __TldCPP_R[2]&&!(__TldCPP_R[0]);
E[1] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__TldCPP_A5);
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__TldCPP_A5));
E[2] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__TldCPP_A7);
E[2] = E[1]||E[2];
E[1] = __TldCPP_R[2]||__TldCPP_R[3];
E[3] = __TldCPP_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__TldCPP_A3));
if (E[3]) {
__TldCPP_A24;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldCPP_A24\n");
#endif
}
E[4] = __TldCPP_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TldCPP_A1);
E[5] = __TldCPP_R[1]&&!(__TldCPP_R[0]);
E[6] = E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TldCPP_A1);
E[6] = E[4]||E[6];
if (E[6]) {
__TldCPP_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldCPP_A22\n");
#endif
}
if (E[6]) {
__TldCPP_A23;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldCPP_A23\n");
#endif
}
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__TldCPP_A7));
E[4] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__TldCPP_A6));
E[4] = E[6]||(__TldCPP_R[2]&&E[4]);
E[7] = (E[1]&&!(__TldCPP_R[2]))||E[4];
E[8] = E[7]||E[2];
E[9] = __TldCPP_R[3]&&!(__TldCPP_R[0]);
E[10] = E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__TldCPP_A2));
E[10] = E[6]||(__TldCPP_R[3]&&E[10]);
E[11] = (E[1]&&!(__TldCPP_R[3]))||E[10];
E[2] = E[2]&&E[8]&&E[11];
if (E[2]) {
__TldCPP_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldCPP_A8\n");
#endif
}
if (E[6]) {
__TldCPP_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldCPP_A9\n");
#endif
}
if (E[2]) {
__TldCPP_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldCPP_A10\n");
#endif
}
E[9] = E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__TldCPP_A2);
if (E[9]) {
__TldCPP_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldCPP_A11\n");
#endif
}
E[0] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__TldCPP_A6);
E[8] = E[8]||E[0];
E[0] = E[0]&&E[8]&&E[11];
if (E[0]) {
__TldCPP_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldCPP_A12\n");
#endif
}
if (E[6]) {
__TldCPP_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldCPP_A13\n");
#endif
}
if (E[9]) {
__TldCPP_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldCPP_A14\n");
#endif
}
E[8] = E[9]&&E[8]&&(E[11]||E[9]);
E[12] = E[2]||E[0]||E[8];
E[13] = __TldCPP_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TldCPP_A1));
E[5] = E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TldCPP_A1));
E[5] = E[13]||(__TldCPP_R[1]&&E[5]);
E[11] = ((E[4]||E[10])&&E[7]&&E[11])||E[5];
E[1] = E[1]||__TldCPP_R[1];
E[8] = E[8]||E[0]||E[2]||E[0]||E[8];
__TldCPP_R[2] = E[4]&&!(E[8]);
__TldCPP_R[3] = E[10]&&!(E[8]);
__TldCPP_R[0] = !(_true);
__TldCPP_R[1] = E[5];
__TldCPP__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int TldCPP_reset () {
__TldCPP_R[0] = _true;
__TldCPP_R[1] = _false;
__TldCPP_R[2] = _false;
__TldCPP_R[3] = _false;
__TldCPP__reset_input();
return 0;
}
