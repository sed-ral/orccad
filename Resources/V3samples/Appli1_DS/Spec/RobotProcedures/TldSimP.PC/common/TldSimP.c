/* sscc : C CODE OF SORTED EQUATIONS TldSimP - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __TldSimP_GENERIC_TEST(TEST) return TEST;
typedef void (*__TldSimP_APF)();
static __TldSimP_APF *__TldSimP_PActionArray;

#include "TldSimP.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _Tld_controler_DEFINED
#ifndef Tld_controler
extern void Tld_controler(orcStrlPtr);
#endif
#endif
#ifndef _Tld_fileparameter_DEFINED
#ifndef Tld_fileparameter
extern void Tld_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _Tld_parameter_DEFINED
#ifndef Tld_parameter
extern void Tld_parameter(orcStrlPtr ,string ,string ,string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __TldSimP_V0;
static boolean __TldSimP_V1;
static orcStrlPtr __TldSimP_V2;
static boolean __TldSimP_V3;
static boolean __TldSimP_V4;
static boolean __TldSimP_V5;
static boolean __TldSimP_V6;
static boolean __TldSimP_V7;


/* INPUT FUNCTIONS */

void TldSimP_I_START_TldSimP () {
__TldSimP_V0 = _true;
}
void TldSimP_I_Abort_Local_TldSimP () {
__TldSimP_V1 = _true;
}
void TldSimP_I_Tld_Start (orcStrlPtr __V) {
_orcStrlPtr(&__TldSimP_V2,__V);
__TldSimP_V3 = _true;
}
void TldSimP_I_USERSTART_Tld () {
__TldSimP_V4 = _true;
}
void TldSimP_I_BF_Tld () {
__TldSimP_V5 = _true;
}
void TldSimP_I_T3_Tld () {
__TldSimP_V6 = _true;
}
void TldSimP_I_T2_Sing () {
__TldSimP_V7 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __TldSimP_A1 \
__TldSimP_V0
#define __TldSimP_A2 \
__TldSimP_V1
#define __TldSimP_A3 \
__TldSimP_V3
#define __TldSimP_A4 \
__TldSimP_V4
#define __TldSimP_A5 \
__TldSimP_V5
#define __TldSimP_A6 \
__TldSimP_V6
#define __TldSimP_A7 \
__TldSimP_V7

/* OUTPUT ACTIONS */

#define __TldSimP_A8 \
TldSimP_O_BF_TldSimP()
#define __TldSimP_A9 \
TldSimP_O_STARTED_TldSimP()
#define __TldSimP_A10 \
TldSimP_O_GoodEnd_TldSimP()
#define __TldSimP_A11 \
TldSimP_O_Abort_TldSimP()
#define __TldSimP_A12 \
TldSimP_O_T3_TldSimP()
#define __TldSimP_A13 \
TldSimP_O_START_Tld()
#define __TldSimP_A14 \
TldSimP_O_Abort_Local_Tld()

/* ASSIGNMENTS */

#define __TldSimP_A15 \
__TldSimP_V0 = _false
#define __TldSimP_A16 \
__TldSimP_V1 = _false
#define __TldSimP_A17 \
__TldSimP_V3 = _false
#define __TldSimP_A18 \
__TldSimP_V4 = _false
#define __TldSimP_A19 \
__TldSimP_V5 = _false
#define __TldSimP_A20 \
__TldSimP_V6 = _false
#define __TldSimP_A21 \
__TldSimP_V7 = _false

/* PROCEDURE CALLS */

#define __TldSimP_A22 \
Tld_parameter(__TldSimP_V2,"SimTime","Kind","X_f")
#define __TldSimP_A23 \
Tld_controler(__TldSimP_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __TldSimP_A24 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int TldSimP_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __TldSimP__reset_input () {
__TldSimP_V0 = _false;
__TldSimP_V1 = _false;
__TldSimP_V3 = _false;
__TldSimP_V4 = _false;
__TldSimP_V5 = _false;
__TldSimP_V6 = _false;
__TldSimP_V7 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __TldSimP_R[4] = {_true,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int TldSimP () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = __TldSimP_R[2]&&!(__TldSimP_R[0]);
E[1] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__TldSimP_A5);
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__TldSimP_A5));
E[2] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__TldSimP_A7);
E[2] = E[1]||E[2];
E[1] = __TldSimP_R[2]||__TldSimP_R[3];
E[3] = __TldSimP_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__TldSimP_A3));
if (E[3]) {
__TldSimP_A24;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldSimP_A24\n");
#endif
}
E[4] = __TldSimP_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TldSimP_A1);
E[5] = __TldSimP_R[1]&&!(__TldSimP_R[0]);
E[6] = E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TldSimP_A1);
E[6] = E[4]||E[6];
if (E[6]) {
__TldSimP_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldSimP_A22\n");
#endif
}
if (E[6]) {
__TldSimP_A23;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldSimP_A23\n");
#endif
}
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__TldSimP_A7));
E[4] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__TldSimP_A6));
E[4] = E[6]||(__TldSimP_R[2]&&E[4]);
E[7] = (E[1]&&!(__TldSimP_R[2]))||E[4];
E[8] = E[7]||E[2];
E[9] = __TldSimP_R[3]&&!(__TldSimP_R[0]);
E[10] = E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__TldSimP_A2));
E[10] = E[6]||(__TldSimP_R[3]&&E[10]);
E[11] = (E[1]&&!(__TldSimP_R[3]))||E[10];
E[2] = E[2]&&E[8]&&E[11];
if (E[2]) {
__TldSimP_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldSimP_A8\n");
#endif
}
if (E[6]) {
__TldSimP_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldSimP_A9\n");
#endif
}
if (E[2]) {
__TldSimP_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldSimP_A10\n");
#endif
}
E[9] = E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__TldSimP_A2);
if (E[9]) {
__TldSimP_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldSimP_A11\n");
#endif
}
E[0] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__TldSimP_A6);
E[8] = E[8]||E[0];
E[0] = E[0]&&E[8]&&E[11];
if (E[0]) {
__TldSimP_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldSimP_A12\n");
#endif
}
if (E[6]) {
__TldSimP_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldSimP_A13\n");
#endif
}
if (E[9]) {
__TldSimP_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__TldSimP_A14\n");
#endif
}
E[8] = E[9]&&E[8]&&(E[11]||E[9]);
E[12] = E[2]||E[0]||E[8];
E[13] = __TldSimP_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TldSimP_A1));
E[5] = E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TldSimP_A1));
E[5] = E[13]||(__TldSimP_R[1]&&E[5]);
E[11] = ((E[4]||E[10])&&E[7]&&E[11])||E[5];
E[1] = E[1]||__TldSimP_R[1];
E[8] = E[8]||E[0]||E[2]||E[0]||E[8];
__TldSimP_R[2] = E[4]&&!(E[8]);
__TldSimP_R[3] = E[10]&&!(E[8]);
__TldSimP_R[0] = !(_true);
__TldSimP_R[1] = E[5];
__TldSimP__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int TldSimP_reset () {
__TldSimP_R[0] = _true;
__TldSimP_R[1] = _false;
__TldSimP_R[2] = _false;
__TldSimP_R[3] = _false;
__TldSimP__reset_input();
return 0;
}
