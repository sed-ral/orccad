/* sscc : C CODE OF SORTED EQUATIONS TestT1CPP - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __TestT1CPP_GENERIC_TEST(TEST) return TEST;
typedef void (*__TestT1CPP_APF)();
static __TestT1CPP_APF *__TestT1CPP_PActionArray;

#include "TestT1CPP.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _TestT1_controler_DEFINED
#ifndef TestT1_controler
extern void TestT1_controler(orcStrlPtr);
#endif
#endif
#ifndef _TestT1_fileparameter_DEFINED
#ifndef TestT1_fileparameter
extern void TestT1_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _TestT1_parameter_DEFINED
#ifndef TestT1_parameter
extern void TestT1_parameter(orcStrlPtr ,string ,string ,string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __TestT1CPP_V0;
static boolean __TestT1CPP_V1;
static orcStrlPtr __TestT1CPP_V2;
static boolean __TestT1CPP_V3;
static boolean __TestT1CPP_V4;
static boolean __TestT1CPP_V5;
static boolean __TestT1CPP_V6;


/* INPUT FUNCTIONS */

void TestT1CPP_I_START_TestT1CPP () {
__TestT1CPP_V0 = _true;
}
void TestT1CPP_I_Abort_Local_TestT1CPP () {
__TestT1CPP_V1 = _true;
}
void TestT1CPP_I_TestT1_Start (orcStrlPtr __V) {
_orcStrlPtr(&__TestT1CPP_V2,__V);
__TestT1CPP_V3 = _true;
}
void TestT1CPP_I_USERSTART_TestT1 () {
__TestT1CPP_V4 = _true;
}
void TestT1CPP_I_BF_TestT1 () {
__TestT1CPP_V5 = _true;
}
void TestT1CPP_I_T3_TestT1 () {
__TestT1CPP_V6 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __TestT1CPP_A1 \
__TestT1CPP_V0
#define __TestT1CPP_A2 \
__TestT1CPP_V1
#define __TestT1CPP_A3 \
__TestT1CPP_V3
#define __TestT1CPP_A4 \
__TestT1CPP_V4
#define __TestT1CPP_A5 \
__TestT1CPP_V5
#define __TestT1CPP_A6 \
__TestT1CPP_V6

/* OUTPUT ACTIONS */

#define __TestT1CPP_A7 \
TestT1CPP_O_BF_TestT1CPP()
#define __TestT1CPP_A8 \
TestT1CPP_O_STARTED_TestT1CPP()
#define __TestT1CPP_A9 \
TestT1CPP_O_GoodEnd_TestT1CPP()
#define __TestT1CPP_A10 \
TestT1CPP_O_Abort_TestT1CPP()
#define __TestT1CPP_A11 \
TestT1CPP_O_T3_TestT1CPP()
#define __TestT1CPP_A12 \
TestT1CPP_O_START_TestT1()
#define __TestT1CPP_A13 \
TestT1CPP_O_Abort_Local_TestT1()

/* ASSIGNMENTS */

#define __TestT1CPP_A14 \
__TestT1CPP_V0 = _false
#define __TestT1CPP_A15 \
__TestT1CPP_V1 = _false
#define __TestT1CPP_A16 \
__TestT1CPP_V3 = _false
#define __TestT1CPP_A17 \
__TestT1CPP_V4 = _false
#define __TestT1CPP_A18 \
__TestT1CPP_V5 = _false
#define __TestT1CPP_A19 \
__TestT1CPP_V6 = _false

/* PROCEDURE CALLS */

#define __TestT1CPP_A20 \
TestT1_parameter(__TestT1CPP_V2,"SimTime","X_f","Kind")
#define __TestT1CPP_A21 \
TestT1_controler(__TestT1CPP_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __TestT1CPP_A22 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int TestT1CPP_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __TestT1CPP__reset_input () {
__TestT1CPP_V0 = _false;
__TestT1CPP_V1 = _false;
__TestT1CPP_V3 = _false;
__TestT1CPP_V4 = _false;
__TestT1CPP_V5 = _false;
__TestT1CPP_V6 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __TestT1CPP_R[4] = {_true,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int TestT1CPP () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = __TestT1CPP_R[2]&&!(__TestT1CPP_R[0]);
E[1] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__TestT1CPP_A5);
E[2] = __TestT1CPP_R[2]||__TestT1CPP_R[3];
E[3] = __TestT1CPP_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__TestT1CPP_A3));
if (E[3]) {
__TestT1CPP_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__TestT1CPP_A22\n");
#endif
}
E[4] = __TestT1CPP_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TestT1CPP_A1);
E[5] = __TestT1CPP_R[1]&&!(__TestT1CPP_R[0]);
E[6] = E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TestT1CPP_A1);
E[6] = E[4]||E[6];
if (E[6]) {
__TestT1CPP_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__TestT1CPP_A20\n");
#endif
}
if (E[6]) {
__TestT1CPP_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__TestT1CPP_A21\n");
#endif
}
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__TestT1CPP_A5));
E[4] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__TestT1CPP_A6));
E[4] = E[6]||(__TestT1CPP_R[2]&&E[4]);
E[7] = (E[2]&&!(__TestT1CPP_R[2]))||E[4];
E[8] = E[7]||E[1];
E[9] = __TestT1CPP_R[3]&&!(__TestT1CPP_R[0]);
E[10] = E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__TestT1CPP_A2));
E[10] = E[6]||(__TestT1CPP_R[3]&&E[10]);
E[11] = (E[2]&&!(__TestT1CPP_R[3]))||E[10];
E[1] = E[1]&&E[8]&&E[11];
if (E[1]) {
__TestT1CPP_A7;
#ifdef TRACE_ACTION
fprintf(stderr, "__TestT1CPP_A7\n");
#endif
}
if (E[6]) {
__TestT1CPP_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__TestT1CPP_A8\n");
#endif
}
if (E[1]) {
__TestT1CPP_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__TestT1CPP_A9\n");
#endif
}
E[9] = E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__TestT1CPP_A2);
if (E[9]) {
__TestT1CPP_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__TestT1CPP_A10\n");
#endif
}
E[0] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__TestT1CPP_A6);
E[8] = E[8]||E[0];
E[0] = E[0]&&E[8]&&E[11];
if (E[0]) {
__TestT1CPP_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__TestT1CPP_A11\n");
#endif
}
if (E[6]) {
__TestT1CPP_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__TestT1CPP_A12\n");
#endif
}
if (E[9]) {
__TestT1CPP_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__TestT1CPP_A13\n");
#endif
}
E[8] = E[9]&&E[8]&&(E[11]||E[9]);
E[12] = E[1]||E[0]||E[8];
E[13] = __TestT1CPP_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TestT1CPP_A1));
E[5] = E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__TestT1CPP_A1));
E[5] = E[13]||(__TestT1CPP_R[1]&&E[5]);
E[11] = ((E[4]||E[10])&&E[7]&&E[11])||E[5];
E[2] = E[2]||__TestT1CPP_R[1];
E[8] = E[8]||E[0]||E[1]||E[0]||E[8];
__TestT1CPP_R[2] = E[4]&&!(E[8]);
__TestT1CPP_R[3] = E[10]&&!(E[8]);
__TestT1CPP_R[0] = !(_true);
__TestT1CPP_R[1] = E[5];
__TestT1CPP__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int TestT1CPP_reset () {
__TestT1CPP_R[0] = _true;
__TestT1CPP_R[1] = _false;
__TestT1CPP_R[2] = _false;
__TestT1CPP_R[3] = _false;
__TestT1CPP__reset_input();
return 0;
}
