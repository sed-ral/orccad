/* sscc : C CODE OF SORTED EQUATIONS aa - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __aa_GENERIC_TEST(TEST) return TEST;
typedef void (*__aa_APF)();
static __aa_APF *__aa_PActionArray;

#include "aa.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _TestT1_controler_DEFINED
#ifndef TestT1_controler
extern void TestT1_controler(orcStrlPtr);
#endif
#endif
#ifndef _TestT1_fileparameter_DEFINED
#ifndef TestT1_fileparameter
extern void TestT1_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _TestT1_parameter_DEFINED
#ifndef TestT1_parameter
extern void TestT1_parameter(orcStrlPtr ,string ,string ,string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static orcStrlPtr __aa_V0;
static boolean __aa_V1;
static boolean __aa_V2;
static boolean __aa_V3;
static boolean __aa_V4;
static boolean __aa_V5;
static boolean __aa_V6;
static boolean __aa_V7;
static boolean __aa_V8;
static boolean __aa_V9;
static orcStrlPtr __aa_V10;
static boolean __aa_V11;
static boolean __aa_V12;
static boolean __aa_V13;
static boolean __aa_V14;
static boolean __aa_V15;
static boolean __aa_V16;
static orcStrlPtr __aa_V17;
static orcStrlPtr __aa_V18;
static orcStrlPtr __aa_V19;
static orcStrlPtr __aa_V20;
static orcStrlPtr __aa_V21;
static orcStrlPtr __aa_V22;
static orcStrlPtr __aa_V23;
static orcStrlPtr __aa_V24;
static orcStrlPtr __aa_V25;
static boolean __aa_V26;


/* INPUT FUNCTIONS */

void aa_I_Prr_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
void aa_I_CmdStopOK_Ex_Scalar () {
__aa_V2 = _true;
}
void aa_I_ActivateOK_Ex_Scalar () {
__aa_V3 = _true;
}
void aa_I_End_Man () {
__aa_V4 = _true;
}
void aa_I_T2_TestT1 () {
__aa_V5 = _true;
}
void aa_I_InitOK_Ex_Scalar () {
__aa_V6 = _true;
}
void aa_I_KillOK_Ex_Scalar () {
__aa_V7 = _true;
}
void aa_I_ReadyToStop_Ex_Scalar () {
__aa_V8 = _true;
}
void aa_I_USERSTART_TestT1 () {
__aa_V9 = _true;
}
void aa_I_TestT1_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V10,__V);
__aa_V11 = _true;
}
void aa_I_Sing_OK () {
__aa_V12 = _true;
}
void aa_I_ROBOT_FAIL () {
__aa_V13 = _true;
}
void aa_I_SOFT_FAIL () {
__aa_V14 = _true;
}
void aa_I_SENSOR_FAIL () {
__aa_V15 = _true;
}
void aa_I_CPU_OVERLOAD () {
__aa_V16 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __aa_A1 \
__aa_V1
#define __aa_A2 \
__aa_V2
#define __aa_A3 \
__aa_V3
#define __aa_A4 \
__aa_V4
#define __aa_A5 \
__aa_V5
#define __aa_A6 \
__aa_V6
#define __aa_A7 \
__aa_V7
#define __aa_A8 \
__aa_V8
#define __aa_A9 \
__aa_V9
#define __aa_A10 \
__aa_V11
#define __aa_A11 \
__aa_V12
#define __aa_A12 \
__aa_V13
#define __aa_A13 \
__aa_V14
#define __aa_A14 \
__aa_V15
#define __aa_A15 \
__aa_V16

/* OUTPUT ACTIONS */

#define __aa_A16 \
aa_O_Activate(__aa_V17)
#define __aa_A17 \
aa_O_GoodEnd_TestT1CPP()
#define __aa_A18 \
aa_O_Abort_TestT1CPP()
#define __aa_A19 \
aa_O_ActivateTestT1_Ex_Scalar(__aa_V18)
#define __aa_A20 \
aa_O_TestT1Transite(__aa_V19)
#define __aa_A21 \
aa_O_Abort_TestT1(__aa_V20)
#define __aa_A22 \
aa_O_STARTED_TestT1()
#define __aa_A23 \
aa_O_GoodEnd_TestT1()
#define __aa_A24 \
aa_O_TreatSing_OK(__aa_V21)
#define __aa_A25 \
aa_O_STARTED_TestT1CPP()
#define __aa_A26 \
aa_O_EndKill(__aa_V22)
#define __aa_A27 \
aa_O_FinBF(__aa_V23)
#define __aa_A28 \
aa_O_FinT3(__aa_V24)
#define __aa_A29 \
aa_O_GoodEndRPr()
#define __aa_A30 \
aa_O_T3RPr()

/* ASSIGNMENTS */

#define __aa_A31 \
__aa_V1 = _false
#define __aa_A32 \
__aa_V2 = _false
#define __aa_A33 \
__aa_V3 = _false
#define __aa_A34 \
__aa_V4 = _false
#define __aa_A35 \
__aa_V5 = _false
#define __aa_A36 \
__aa_V6 = _false
#define __aa_A37 \
__aa_V7 = _false
#define __aa_A38 \
__aa_V8 = _false
#define __aa_A39 \
__aa_V9 = _false
#define __aa_A40 \
__aa_V11 = _false
#define __aa_A41 \
__aa_V12 = _false
#define __aa_A42 \
__aa_V13 = _false
#define __aa_A43 \
__aa_V14 = _false
#define __aa_A44 \
__aa_V15 = _false
#define __aa_A45 \
__aa_V16 = _false
#define __aa_A46 \
_orcStrlPtr(&__aa_V22,__aa_V25)
#define __aa_A47 \
_orcStrlPtr(&__aa_V23,__aa_V0)
#define __aa_A48 \
_orcStrlPtr(&__aa_V24,__aa_V0)
#define __aa_A49 \
_orcStrlPtr(&__aa_V20,__aa_V10)
#define __aa_A50 \
_orcStrlPtr(&__aa_V18,__aa_V10)
#define __aa_A51 \
_orcStrlPtr(&__aa_V25,__aa_V10)
#define __aa_A52 \
_orcStrlPtr(&__aa_V19,__aa_V10)
#define __aa_A53 \
_orcStrlPtr(&__aa_V19,__aa_V10)
#define __aa_A54 \
_orcStrlPtr(&__aa_V21,__aa_V10)
#define __aa_A55 \
_orcStrlPtr(&__aa_V19,__aa_V10)
#define __aa_A56 \
_orcStrlPtr(&__aa_V19,__aa_V10)
#define __aa_A57 \
_orcStrlPtr(&__aa_V22,__aa_V25)

/* PROCEDURE CALLS */

#define __aa_A58 \
TestT1_parameter(__aa_V10,"SimTime","X_f","Kind")
#define __aa_A59 \
TestT1_controler(__aa_V10)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __aa_A60 \

#define __aa_A61 \

#define __aa_A62 \

#define __aa_A63 \

#define __aa_A64 \

#define __aa_A65 \

#define __aa_A66 \

#define __aa_A67 \

#define __aa_A68 \

#define __aa_A69 \

#define __aa_A70 \

#define __aa_A71 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int aa_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __aa__reset_input () {
__aa_V1 = _false;
__aa_V2 = _false;
__aa_V3 = _false;
__aa_V4 = _false;
__aa_V5 = _false;
__aa_V6 = _false;
__aa_V7 = _false;
__aa_V8 = _false;
__aa_V9 = _false;
__aa_V11 = _false;
__aa_V12 = _false;
__aa_V13 = _false;
__aa_V14 = _false;
__aa_V15 = _false;
__aa_V16 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __aa_R[17] = {_true,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int aa () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[43];
if (!(_true)) {
__aa_A16;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A16\n");
#endif
}
E[0] = __aa_R[11]&&!(__aa_R[0]);
E[1] = __aa_R[9]&&!(__aa_R[0]);
E[2] = E[1]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4);
E[3] = __aa_R[7]||__aa_R[8]||__aa_R[9];
E[4] = __aa_R[5]&&__aa_R[5]&&!(__aa_R[0]);
E[5] = E[4]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__aa_A6);
E[6] = __aa_R[13]&&!(__aa_R[0]);
E[7] = E[6]&&E[5];
E[8] = __aa_R[16]&&!(__aa_R[0]);
E[9] = E[8]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2);
E[10] = E[7]||E[9];
E[11] = __aa_R[6]&&__aa_R[6]&&!(__aa_R[0]);
E[12] = (E[5]&&E[10])||(E[11]&&E[10]);
if (E[12]) {
__aa_A50;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A50\n");
#endif
}
if (E[12]) {
__aa_A51;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A51\n");
#endif
}
E[13] = E[12]||(__aa_R[7]&&__aa_R[7]&&!(__aa_R[0]));
E[14] = (E[3]&&!(__aa_R[7]))||E[13];
E[15] = __aa_R[8]&&!(__aa_R[0]);
E[16] = E[15]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11);
if (E[16]) {
__aa_A54;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A54\n");
#endif
}
E[15] = E[15]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11));
E[15] = E[12]||E[16]||(__aa_R[8]&&E[15]);
E[17] = (E[3]&&!(__aa_R[8]))||E[15];
E[1] = E[1]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4));
E[1] = E[12]||(__aa_R[9]&&E[1]);
E[18] = (E[3]&&!(__aa_R[9]))||E[1];
E[2] = E[2]&&E[14]&&E[17]&&(E[18]||E[2]);
if (E[2]) {
__aa_A55;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A55\n");
#endif
}
E[19] = E[0]&&E[2];
E[20] = __aa_R[11]||__aa_R[12];
E[21] = __aa_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1));
if (E[21]) {
__aa_A60;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A60\n");
#endif
}
E[22] = __aa_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10));
if (E[22]) {
__aa_A61;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A61\n");
#endif
}
if (__aa_R[0]) {
__aa_A62;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A62\n");
#endif
}
if (__aa_R[0]) {
__aa_A63;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A63\n");
#endif
}
if (__aa_R[0]) {
__aa_A64;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A64\n");
#endif
}
if (__aa_R[0]) {
__aa_A65;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A65\n");
#endif
}
if (__aa_R[0]) {
__aa_A66;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A66\n");
#endif
}
if (__aa_R[0]) {
__aa_A67;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A67\n");
#endif
}
if (__aa_R[0]) {
__aa_A68;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A68\n");
#endif
}
if (__aa_R[0]) {
__aa_A69;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A69\n");
#endif
}
if (__aa_R[0]) {
__aa_A70;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A70\n");
#endif
}
if (__aa_R[0]) {
__aa_A71;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A71\n");
#endif
}
E[23] = __aa_R[2]&&!(__aa_R[0]);
E[24] = E[23]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10);
E[25] = __aa_R[3]&&!(__aa_R[0]);
E[26] = E[25]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1);
E[27] = __aa_R[2]||__aa_R[3];
E[28] = (E[27]&&!(__aa_R[2]))||E[24];
E[29] = (E[27]&&!(__aa_R[3]))||E[26];
E[26] = (E[24]||E[26])&&E[28]&&E[29];
E[24] = __aa_R[10]&&!(__aa_R[0]);
E[30] = (__aa_R[0]&&E[26])||(E[24]&&E[26]);
if (E[30]) {
__aa_A58;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A58\n");
#endif
}
if (E[30]) {
__aa_A59;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A59\n");
#endif
}
E[0] = E[30]||(__aa_R[11]&&E[0]&&!(E[2]));
E[31] = (E[20]&&!(__aa_R[11]))||E[0];
E[32] = E[30]||(__aa_R[12]&&__aa_R[12]&&!(__aa_R[0]));
E[33] = (E[20]&&!(__aa_R[12]))||E[32];
E[19] = E[19]&&(E[31]||E[19])&&E[33];
if (E[19]) {
__aa_A17;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A17\n");
#endif
}
if (!(_true)) {
__aa_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A18\n");
#endif
}
if (E[12]) {
__aa_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A19\n");
#endif
}
if (!(_true)) {
__aa_A53;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A53\n");
#endif
}
if (!(_true)) {
__aa_A52;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A52\n");
#endif
}
if (E[2]) {
__aa_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A20\n");
#endif
}
if (!(_true)) {
__aa_A49;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A49\n");
#endif
}
if (!(_true)) {
__aa_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A21\n");
#endif
}
E[34] = E[2]||__aa_R[0];
E[35] = __aa_R[4]&&!(__aa_R[0]);
E[36] = (E[34]&&E[30])||(E[35]&&E[30]);
if (E[36]) {
__aa_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A22\n");
#endif
}
if (E[2]) {
__aa_A23;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A23\n");
#endif
}
if (E[16]) {
__aa_A24;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A24\n");
#endif
}
if (E[30]) {
__aa_A25;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A25\n");
#endif
}
E[37] = __aa_R[15]&&!(__aa_R[0]);
E[38] = E[37]&&E[5];
if (E[38]) {
__aa_A57;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A57\n");
#endif
}
E[39] = __aa_R[1]&&!(__aa_R[0]);
E[40] = E[39]&&E[19];
E[3] = __aa_R[5]||E[3]||__aa_R[6]||__aa_R[4];
E[20] = E[20]||__aa_R[10];
E[41] = __aa_R[13]||__aa_R[14]||__aa_R[15]||__aa_R[16];
E[42] = E[27]||E[3]||E[20]||E[41]||__aa_R[1];
E[23] = E[23]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10));
E[23] = __aa_R[0]||(__aa_R[2]&&E[23]);
E[25] = E[25]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1));
E[25] = __aa_R[0]||(__aa_R[3]&&E[25]);
E[29] = (E[23]||E[25])&&(E[28]||E[23])&&(E[29]||E[25]);
E[27] = (E[42]&&!(E[27]))||E[26]||E[29];
E[4] = E[4]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__aa_A6));
E[4] = __aa_R[5]&&E[4];
E[10] = (E[5]&&!(E[10]))||(__aa_R[6]&&E[11]&&!(E[10]));
E[35] = (E[34]&&!(E[30]))||(__aa_R[4]&&E[35]&&!(E[30]));
E[18] = E[36]||E[4]||((E[13]||E[15]||E[1])&&E[14]&&E[17]&&E[18])||E[10]||E[35];
E[3] = (E[42]&&!(E[3]))||E[18];
E[26] = (__aa_R[0]&&!(E[26]))||(__aa_R[10]&&E[24]&&!(E[26]));
E[33] = ((E[0]||E[32])&&E[31]&&E[33])||E[26];
E[20] = (E[42]&&!(E[20]))||E[19]||E[33];
E[6] = __aa_R[0]||(__aa_R[13]&&E[6]&&!(E[5]));
E[31] = __aa_R[14]&&!(__aa_R[0]);
E[9] = E[7]||E[9]||(__aa_R[14]&&E[31]&&!(E[2]));
E[5] = (E[31]&&E[2])||(__aa_R[15]&&E[37]&&!(E[5]));
E[8] = E[8]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2));
E[8] = E[38]||(__aa_R[16]&&E[8]);
E[41] = (E[42]&&!(E[41]))||E[6]||E[9]||E[5]||E[8];
E[39] = E[39]&&!(E[19]);
E[37] = E[39]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12));
E[31] = E[37]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13));
E[7] = E[31]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14));
E[24] = E[7]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15));
E[24] = __aa_R[0]||(__aa_R[1]&&E[24]);
E[42] = (E[42]&&!(__aa_R[1]))||E[24];
E[17] = E[42]||E[40];
E[40] = E[40]&&E[27]&&E[3]&&E[20]&&E[41]&&E[17];
if (E[40]) {
__aa_A46;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A46\n");
#endif
}
if (E[38]||E[40]) {
__aa_A26;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A26\n");
#endif
}
if (E[40]) {
__aa_A47;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A47\n");
#endif
}
if (E[40]) {
__aa_A27;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A27\n");
#endif
}
E[39] = E[39]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12);
E[37] = E[37]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13);
E[31] = E[31]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14);
E[7] = E[7]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15);
E[17] = (E[39]||E[37]||E[31]||E[7])&&E[27]&&E[3]&&E[20]&&E[41]&&(E[17]||E[39]||E[37]||E[31]||E[7]);
if (E[17]) {
__aa_A48;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A48\n");
#endif
}
if (E[17]) {
__aa_A28;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A28\n");
#endif
}
if (E[40]) {
__aa_A29;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A29\n");
#endif
}
if (E[17]) {
__aa_A30;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A30\n");
#endif
}
E[7] = E[39]||E[37]||E[31]||E[7];
E[31] = E[40]||E[17];
E[42] = (E[29]||E[18]||E[33]||E[6]||E[9]||E[5]||E[8]||E[24])&&E[27]&&E[3]&&E[20]&&E[41]&&E[42];
E[41] = E[17]||E[40];
__aa_R[1] = E[24]&&!(E[41]);
__aa_R[2] = E[23]&&!(E[41]);
__aa_R[3] = E[25]&&!(E[41]);
__aa_R[4] = E[35]&&!(E[41]);
__aa_R[5] = (E[36]&&!(E[41]))||(E[4]&&!(E[41]));
__aa_R[6] = E[10]&&!(E[41]);
E[10] = E[41]||E[2];
__aa_R[7] = E[13]&&!(E[10]);
__aa_R[8] = E[15]&&!(E[10]);
__aa_R[9] = E[1]&&!(E[10]);
__aa_R[10] = E[26]&&!(E[41]);
E[26] = E[41]||E[19];
__aa_R[11] = E[0]&&!(E[26]);
__aa_R[12] = E[32]&&!(E[26]);
__aa_R[13] = E[6]&&!(E[41]);
__aa_R[14] = E[9]&&!(E[41]);
__aa_R[15] = E[5]&&!(E[41]);
__aa_R[16] = E[8]&&!(E[41]);
__aa_R[0] = !(_true);
__aa__reset_input();
return E[42];
}

/* AUTOMATON RESET */

int aa_reset () {
__aa_R[0] = _true;
__aa_R[1] = _false;
__aa_R[2] = _false;
__aa_R[3] = _false;
__aa_R[4] = _false;
__aa_R[5] = _false;
__aa_R[6] = _false;
__aa_R[7] = _false;
__aa_R[8] = _false;
__aa_R[9] = _false;
__aa_R[10] = _false;
__aa_R[11] = _false;
__aa_R[12] = _false;
__aa_R[13] = _false;
__aa_R[14] = _false;
__aa_R[15] = _false;
__aa_R[16] = _false;
__aa__reset_input();
return 0;
}
