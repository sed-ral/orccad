#ifndef __orc_manageRx90_h
#define __orc_manageRx90_h

// Definition Used for int end_P
typedef enum {END_NO, END_TR, END_PRR} END_TYPE;

// C function Interfaces

extern int orcmanageRx90Init();
extern int orcmanageRx90Launch();
extern int orcmanageRx90Go();
extern int orcmanageRx90Stop();
extern int orcmovejRx_Rx90_joint(double joint[6]);
extern int orcveljRx_Rx90_joint(double joint[6]);
extern int orcmovejRxPrm(double Qpos[7]);
extern int orcveljRxPrm(double veljD_P[6], int *end_P);

#endif
