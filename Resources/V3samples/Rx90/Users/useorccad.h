#include <stdio.h>
#include <vxWorks.h>
#include "drvRx90.h"
#include "manageRx90API.h"

#define PI              3.14159265358979323846
#define RADTODEG(rad)   ((rad)*(180./PI))
#define DEGTORAD(deg)   ((deg)*(PI/180.))
#define NB_AXIS_RX90    6

#define VMAX            0.5
#define VMIN            0.02
#define VDEF            0.2

/* function defined in sample.c for Rx90 driver */
extern int keywords(char *key,double param[NB_AXIS_RX90]);

typedef enum {RT_NONE,RT_MOVEJRX,RT_VELJRX} RT_STATE;

// Definition Used for int end_P
typedef enum {END_NO, END_TR, END_PRR} END_TYPE;


typedef enum{
    NONDEF_TRAJ,
    POLY_CPOS_TRAJ,
    POLY_CVEL_TRAJ,
    POLY_CACC_TRAJ,
    POLY_CVEL_TRAJ_NRZ,
    POLY_CACC_TRAJ_NRZ} TrajectoryType;



