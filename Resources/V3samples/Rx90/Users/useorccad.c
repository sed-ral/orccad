/* Example of use MovejRx */
#include "useorccad.h"

/*****************************************************************************
** void rxOrccadHelp() 
**      
** Description: Help for Main Menu 
**
** No Parameters.
**
*****************************************************************************/
void rxOrccadHelp()
{
    printf(" (H)elp                            \n");
    printf(" (Q)uit                            \n");
    printf(" (I)nit move position joint        \n");
    printf(" (S)witch robot task (position/vel.)\n");
    printf(" (E)nd  robot control              \n");
    printf(" (P)ut joint position              \n");
    printf(" (Z)ero  position                  \n");
    printf(" (V)elocity trajectory             \n");
    printf(" (G)et joint position              \n");
    printf(" (M)ode of robot control           \n");
    printf(" (F)ile Execution                  \n");
}

void rxOrccad()
{
    int fin,state,i,nbargs,stop,bank; 
    char key;
    double param[NB_AXIS_RX90];
    double jpos[2*NB_AXIS_RX90] = {0.0,0.0,0.0,0.0,0.0,0.0,0.2,0.0,0.0,0.0,0.0,0.0};
    double jvel[NB_AXIS_RX90]   = {0.0,0.0,0.0,0.0,0.0,0.0};
    double trajvel=VDEF;
    double pp[NB_AXIS_RX90] = {0.0,0.0,0.0,0.0,0.0,0.0};
    int typtraj=POLY_CACC_TRAJ;

    fin=0;
    stop = END_NO;
    state=RT_NONE;
    printf("-------------------------------------------------- \n");
    printf("---            RX90 - INTERFACE                --- \n");  
    printf("---       for testing Orccad Interface         --- \n");
    printf("---      version 3.0            Avril. 03      --- \n");
    printf("-------------------------------------------------- \n");
    printf("\n \n \n");

    while(fin==0){
	printf("ORCCAD> ");
	nbargs=keywords(&key,param);
	switch(key){
	case 'Q':
	case 'q':fin=1;break;
	case 'H':
	case 'h': 
	    rxOrccadHelp();
	    break;
	case 'I':
	case 'i': 
	    if (state==RT_NONE){
		printf("Initialisation of move joint...\n");
		orcmanageRx90Go();
		state = RT_MOVEJRX;
	    }
	    else
		printf("ROBOT ALREADY INITIALISED !!!\n");
	    break;
	case 'P': 
	case 'p': 
	    if (state==RT_MOVEJRX){
		if (nbargs==NB_AXIS_RX90){
		    for (i=0;i <NB_AXIS_RX90;i++)
			jpos[i] = DEGTORAD(param[i]);
		    jpos[6] = trajvel;
		    printf("Joints : %g %g %g %g %g %g , Vel: %g\n",
			   param[0],param[1],param[2],
			   param[3],param[4],param[5],trajvel);
		    orcmovejRxPrm(jpos,&typtraj);
		    taskDelay(10);
		    orcmovejRx_jtraj_Bank(&bank);
		    printf("Bank=%d\n",bank);
		}
	    }
	    else
		printf("ROBOT MUST BE IN MOVEJRX !!!\n");
		break;
	case 'v':
	case 'V':
	    switch(state){
	    default:
	    case RT_NONE:
		printf("ROBOT NOT INITIALISED !!!\n");
		break;
	    case RT_MOVEJRX:
		if (nbargs==1){
		    printf("Vel : %g \n",param[0]);
		    trajvel = param[0];
		}
		break;
	    case RT_VELJRX:
		if (nbargs==NB_AXIS_RX90){
		    for (i=0;i <NB_AXIS_RX90;i++)
			jvel[i] = DEGTORAD(param[i]);
		    printf("Joints Vel : %g %g %g %g %g %g \n",
			   param[0],param[1],param[2],
			   param[3],param[4],param[5]);
		    stop = END_NO;
		    orcveljRxPrm(jvel,&stop);
		}
		break;
	     }
	    break;
	case 'z':
	case 'Z':
	    switch(state){
	    default:
	    case RT_NONE:
		printf("Mode = not initialised\n");
		break;
	    case RT_MOVEJRX:
		for (i=0;i <NB_AXIS_RX90;i++)
		    jpos[i] = 0.0;
		jpos[6] = trajvel;
		printf("Joints: 0.0 0.0 0.0 0.0 0.0 0.0 , Vel: %g\n",trajvel);
		orcmovejRxPrm(jpos,&typtraj);
		break;
	    case RT_VELJRX:
		for (i=0;i <NB_AXIS_RX90;i++)
		    jvel[i] = 0.0;
		stop = END_NO;
		orcveljRxPrm(jvel,&stop);
		break;
	     }
	   	    break;
	case 'g':
	case 'G':
	    switch(state){
	    default:
	    case RT_NONE:	
		printf("ROBOT NOT INITIALISED !!!\n");
		break;
	    case RT_MOVEJRX:
		if (orcmovejRx_Rx90_joint(pp)!= ERROR)
		    printf("POS = %g %g %g %g %g %g \n", 
			   RADTODEG(pp[0]),RADTODEG(pp[1]),
			   RADTODEG(pp[2]),RADTODEG(pp[3]),
			   RADTODEG(pp[4]),RADTODEG(pp[5]));
	    else
		printf("ERR = read joint\n");
		break;
	    case RT_VELJRX:
		if (orcveljRx_Rx90_joint(pp)!= ERROR)
		    printf("POS = %g %g %g %g %g %g \n", 
			   RADTODEG(pp[0]),RADTODEG(pp[1]),
			   RADTODEG(pp[2]),RADTODEG(pp[3]),
			   RADTODEG(pp[4]),RADTODEG(pp[5]));
	    else
		printf("ERR = read joint\n");
		break;

	    }
	    break;
	case 's':
	case 'S': 
	    switch(state){
	    default:
	    case RT_NONE:
		printf("ROBOT NOT INITIALISED !!!\n");
		break;
	    case RT_MOVEJRX:
		jpos[0] = 1000.0;
		orcmovejRxPrm(jpos,&typtraj);
		state = RT_VELJRX;
		break;
	    case RT_VELJRX:
		for (i=0;i <NB_AXIS_RX90;i++)
		    jvel[i] = 0.0;
		stop = END_TR;
		orcveljRxPrm(jvel,&stop);
		state = RT_MOVEJRX;
		break;
	    }
	break;
	case 'e':
	case 'E':
	    if (state==RT_VELJRX){
		for (i=0;i <NB_AXIS_RX90;i++)
		    jvel[i] = 0.0;
		stop = END_PRR;
		orcveljRxPrm(jvel,&stop);
		state = RT_NONE;
	    }
	    else
		printf("ROBOT MUST BE IN VELJRX !!!\n");
	    break;
        case 'm':
	case 'M':
	    switch(state){
	    default:
	    case RT_NONE:
		printf("Mode = not initialised\n");
		break;
	    case RT_MOVEJRX:
	      bank=-1;
	      orcmovejRx_jtraj_Bank(&bank);
		printf("Mode = joint position control, Bank=%d\n",bank);
		break;
	    case RT_VELJRX:
		printf("MODE = joint velocity control\n");
		break;
	    }
	    break;	
	case 'f':
	case 'F':
	  if (state==RT_MOVEJRX){
	    rxOrccadFile();
	    }
	    else
		printf("ROBOT MUST BE IN MOVEJRX !!!\n");
	  break;
	default : rxOrccadHelp();
     }/* switch end */
    }/* while end */

    if (state!=RT_NONE){
	printf("KILL manageRx90 \n");
	orcmanageRx90Stop();
    }
}
