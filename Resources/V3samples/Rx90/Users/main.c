#include <stdio.h>
#include <string.h>

#define OK 0
#define ERROR 1
#define MAXCHAR 100
#define TRUE 1
#define FALSE 0
#define HEADER "RX90"
#define VERSION 0
#define _DEBUG 0

rx90move(double trajd[12],int typtraj)
{
  printf("traj : %lf %lf\n",trajd[0],trajd[11]);
  /*
    orcmovejRxPrm(jpos,&typtraj);

  */
}

int main()
{
  char filename[MAXCHAR]="traj.cfg";
  FILE *file;
  int file_end=FALSE,file_err=FALSE,file_header=FALSE;
  int file_version=-1;
  char fligne[MAXCHAR],line_key[MAXCHAR],file_typtraj[MAXCHAR];
  int icar=0,cr,iligne;
  double trajd[12];
  
  file = fopen(filename,"r");

  if (file==NULL)
    {
    printf("Can not open file %s\n",filename);
    return ERROR;
    }

  iligne=0;
  while (file_end == FALSE)
    { /* Read a file line */
      iligne++;
      if (fgets(fligne,MAXCHAR,file)==NULL)
	file_end = TRUE;
      else
	{
	  /* Analyse the line */
	  icar=0;
	 /* Eat space begining the line */
	  while(fligne[icar]==' ')
	    icar++;
	  /* Skip commentary */
	  if ((fligne[icar]!='#')&&(fligne[icar]!='\n'))
	    {
	      /* The line can be analyzed */  
	      if (file_header==FALSE)
		{
                  /* Read the header */
		  cr=sscanf(&fligne[icar],"%s %d %s",line_key,&file_version,&file_typtraj);
		  if (_DEBUG)
		    printf("%s:%d:%d\n",line_key,cr,strcmp(line_key,HEADER));
		  file_header=TRUE;
		  if ( (cr!=3)
		       ||(strcmp(HEADER,line_key)!=0) 
		       || (file_version!=VERSION) )
		  {
		    file_err=file_end = TRUE;
		    printf("ligne %d : Error in header file\n",iligne);
		    return (ERROR);
		  }
		  if ( (strcmp("POLY_CVEL_TRAJ_NRZ",file_typtraj)!=0) 
		       && 
		       (strcmp("POLY_CACC_TRAJ_NRZ",file_typtraj)!=0)  )
		  {
		    file_err=file_end = TRUE;
		    printf("ligne %d : Error in header file\n",iligne);
		    return (ERROR);
		  }
		}
	      else
		{
		  /* Read the traj POS*/
		  cr=sscanf(&fligne[icar],"%lf %lf %lf %lf %lf %lf %s %lf %lf %lf %lf %lf %lf ",&trajd[0],&trajd[1],&trajd[2],&trajd[3],&trajd[4],&trajd[5],
line_key,&trajd[6],&trajd[7],&trajd[8],&trajd[9],&trajd[10],&trajd[11]);
		   if ( (cr!=13)
			||(strcmp("*",line_key)!=0) )
		  {
		     printf("ligne %d : Error in trajectory\n",iligne);
		    return (ERROR);
		  }
		   else
		     {
		       rx90move(trajd,0);
		     }
		}
	    }
	}
    }
  fclose(file);
}
