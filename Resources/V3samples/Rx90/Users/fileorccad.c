#include <stdio.h>
#include "useorccad.h"

#define MY_OK 0
#define MY_ERROR 1
#define MAXCHAR 100
#define TRUE 1
#define FALSE 0
#define HEADER "RX90"
#define VERSION 0
#define _DEBUG 0

int rx90move(double trajd[13],int typtraj)
{
  int bank;

  // Wait the last point
  do
  {
    orcmovejRx_jtraj_Bank(&bank);
    taskDelay(1);
    orcmovejRx_jtraj_Bank(&bank);
    taskDelay(100);
    printf("Bank=%d\n",bank);
  } while (bank==1); 

  printf("traj -> %g %g \n",trajd[0],trajd[12]);
  orcmovejRxPrm(trajd,&typtraj);

  return MY_OK;
}

/*****************************************************************************
** void rxOrccadFile() 
**      
** Description: File interpretation and execution 
**
** No Parameters.
**
*****************************************************************************/

int rxOrccadFile()
{
  char filename[MAXCHAR]="traj.cfg";
  FILE *file;
  int file_end=FALSE,file_err=FALSE,file_header=FALSE,typtraj,i;
  int file_version=-1;
  char fligne[MAXCHAR],line_key[MAXCHAR],line_key1[MAXCHAR],file_typtraj[MAXCHAR];
  int icar=0,cr,iligne;
  double trajd[13];
  
  file = fopen(filename,"r");

  if (file==NULL)
    {
    printf("Can not open file %s\n",filename);
    return MY_ERROR;
    }

  iligne=0;
  while (file_end == FALSE)
    { /* Read a file line */
      iligne++;
      if (fgets(fligne,MAXCHAR,file)==NULL)
	file_end = TRUE;
      else
	{
	  /* Analyse the line */
	  icar=0;
	 /* Eat space begining the line */
	  while(fligne[icar]==' ')
	    icar++;
	  /* Skip commentary */
	  if ((fligne[icar]!='#')&&(fligne[icar]!='\n'))
	    {
	      /* The line can be analyzed */  
	      if (file_header==FALSE)
		{
                  /* Read the header */
		  cr=sscanf(&fligne[icar],"%s %d %s",line_key,&file_version,&file_typtraj);
		  if (_DEBUG)
		    printf("%s:%d:%d\n",line_key,cr,strcmp(line_key,HEADER));
		  file_header=TRUE;
		  if ( (cr!=3)
		       ||(strcmp(HEADER,line_key)!=0) 
		       || (file_version!=VERSION) )
		  {
		    file_err=file_end = TRUE;
		    printf("ligne %d : Error in header file\n",iligne);
		    return (MY_ERROR);
		  }
		  if (strcmp("POLY_CVEL_TRAJ_NRZ",file_typtraj)==0) 
		      { 
			typtraj=POLY_CVEL_TRAJ_NRZ;
		      }
		  else
		  if (strcmp("POLY_CACC_TRAJ_NRZ",file_typtraj)==0) 
		       {
			 typtraj=POLY_CACC_TRAJ_NRZ;
		       }
		   else
		       {
			 file_err=file_end = TRUE;
			 printf("ligne %d : Error in header file\n",iligne);
			 return (MY_ERROR);
		       }
		}
	      else
		{
		  /* Read the traj POS*/
		  cr=sscanf(&fligne[icar],"%lf %s %lf %lf %lf %lf %lf %lf %s %lf %lf %lf %lf %lf %lf ",&trajd[12],line_key,&trajd[0],&trajd[1],&trajd[2],&trajd[3],&trajd[4],&trajd[5],
line_key1,&trajd[6],&trajd[7],&trajd[8],&trajd[9],&trajd[10],&trajd[11]);
		   if ( (cr!=15)
			||(strcmp("*",line_key)!=0)
			||(strcmp("*",line_key1)!=0))
		  {
		     printf("ligne %d : Error in trajectory\n",iligne);
		     printf("%d:%s:%s:\n",cr,line_key,line_key1);
		    return (MY_ERROR);
		  }
		   else
		     {
		       for (i=0;i <2*NB_AXIS_RX90;i++)
			 trajd[i] = DEGTORAD(trajd[i]);
		       rx90move(trajd,typtraj);
		     }
		}
	    }
	}
    }
  fclose(file);
  return MY_OK;
}
