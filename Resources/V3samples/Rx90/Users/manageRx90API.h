//
// This file defines the API functions 
// to interfere with the generated Orccad program 
//
// FOR SOLARIS USERS:
// You can use it including it in the main program
// launching the Orccad run time tasks:
// e.g: 
// 	 #include "manageRx90API.h"
// 	 main() { 
// 	 orcmanageRx90Init();
// 	 orcmanageRx90Launch();
// 	 // Launch task calling the others API Functions 
// 	 // ...add code here...
// 	 }
//
#ifndef __orc_manageRx90Api_h
#define __orc_manageRx90Api_h

// C function Interfaces
//extern "C" { 
extern int orcmanageRx90Init();
extern int orcmanageRx90Launch();
extern int orcmanageRx90TaskLaunch();
extern int orcmanageRx90Go();
extern int orcmanageRx90Stop();
int orcmovejRx_Rx90_joint(double joint[6]);
int orcmovejRx_jtraj_Bank(int *Bank);
int orcveljRx_Rx90_joint(double joint[6]);
extern int orcmovejRxPrm(double Qpos[13], int * TypTraj);
extern int orcveljRxPrm(double veljD_P[6], int * end_P);
//} // extern "C"

#endif 
