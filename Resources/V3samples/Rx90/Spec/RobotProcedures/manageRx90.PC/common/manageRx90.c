/* sscc : C CODE OF SORTED EQUATIONS manageRx90 - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __manageRx90_GENERIC_TEST(TEST) return TEST;
typedef void (*__manageRx90_APF)();
static __manageRx90_APF *__manageRx90_PActionArray;

#include "manageRx90.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _movejRx_controler_DEFINED
#ifndef movejRx_controler
extern void movejRx_controler(orcStrlPtr);
#endif
#endif
#ifndef _movejRx_fileparameter_DEFINED
#ifndef movejRx_fileparameter
extern void movejRx_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _movejRx_parameter_DEFINED
#ifndef movejRx_parameter
extern void movejRx_parameter(orcStrlPtr ,string ,string);
#endif
#endif
#ifndef _veljRx_controler_DEFINED
#ifndef veljRx_controler
extern void veljRx_controler(orcStrlPtr);
#endif
#endif
#ifndef _veljRx_fileparameter_DEFINED
#ifndef veljRx_fileparameter
extern void veljRx_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _veljRx_parameter_DEFINED
#ifndef veljRx_parameter
extern void veljRx_parameter(orcStrlPtr ,string ,string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __manageRx90_V0;
static boolean __manageRx90_V1;
static orcStrlPtr __manageRx90_V2;
static boolean __manageRx90_V3;
static boolean __manageRx90_V4;
static boolean __manageRx90_V5;
static boolean __manageRx90_V6;
static orcStrlPtr __manageRx90_V7;
static boolean __manageRx90_V8;
static boolean __manageRx90_V9;
static boolean __manageRx90_V10;
static boolean __manageRx90_V11;


/* INPUT FUNCTIONS */

void manageRx90_I_START_manageRx90 () {
__manageRx90_V0 = _true;
}
void manageRx90_I_Abort_Local_manageRx90 () {
__manageRx90_V1 = _true;
}
void manageRx90_I_movejRx_Start (orcStrlPtr __V) {
_orcStrlPtr(&__manageRx90_V2,__V);
__manageRx90_V3 = _true;
}
void manageRx90_I_USERSTART_movejRx () {
__manageRx90_V4 = _true;
}
void manageRx90_I_BF_movejRx () {
__manageRx90_V5 = _true;
}
void manageRx90_I_T3_movejRx () {
__manageRx90_V6 = _true;
}
void manageRx90_I_veljRx_Start (orcStrlPtr __V) {
_orcStrlPtr(&__manageRx90_V7,__V);
__manageRx90_V8 = _true;
}
void manageRx90_I_USERSTART_veljRx () {
__manageRx90_V9 = _true;
}
void manageRx90_I_BF_veljRx () {
__manageRx90_V10 = _true;
}
void manageRx90_I_T3_veljRx () {
__manageRx90_V11 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __manageRx90_A1 \
__manageRx90_V0
#define __manageRx90_A2 \
__manageRx90_V1
#define __manageRx90_A3 \
__manageRx90_V3
#define __manageRx90_A4 \
__manageRx90_V4
#define __manageRx90_A5 \
__manageRx90_V5
#define __manageRx90_A6 \
__manageRx90_V6
#define __manageRx90_A7 \
__manageRx90_V8
#define __manageRx90_A8 \
__manageRx90_V9
#define __manageRx90_A9 \
__manageRx90_V10
#define __manageRx90_A10 \
__manageRx90_V11

/* OUTPUT ACTIONS */

#define __manageRx90_A11 \
manageRx90_O_BF_manageRx90()
#define __manageRx90_A12 \
manageRx90_O_STARTED_manageRx90()
#define __manageRx90_A13 \
manageRx90_O_GoodEnd_manageRx90()
#define __manageRx90_A14 \
manageRx90_O_Abort_manageRx90()
#define __manageRx90_A15 \
manageRx90_O_T3_manageRx90()
#define __manageRx90_A16 \
manageRx90_O_START_movejRx()
#define __manageRx90_A17 \
manageRx90_O_Abort_Local_movejRx()
#define __manageRx90_A18 \
manageRx90_O_START_veljRx()
#define __manageRx90_A19 \
manageRx90_O_Abort_Local_veljRx()

/* ASSIGNMENTS */

#define __manageRx90_A20 \
__manageRx90_V0 = _false
#define __manageRx90_A21 \
__manageRx90_V1 = _false
#define __manageRx90_A22 \
__manageRx90_V3 = _false
#define __manageRx90_A23 \
__manageRx90_V4 = _false
#define __manageRx90_A24 \
__manageRx90_V5 = _false
#define __manageRx90_A25 \
__manageRx90_V6 = _false
#define __manageRx90_A26 \
__manageRx90_V8 = _false
#define __manageRx90_A27 \
__manageRx90_V9 = _false
#define __manageRx90_A28 \
__manageRx90_V10 = _false
#define __manageRx90_A29 \
__manageRx90_V11 = _false

/* PROCEDURE CALLS */

#define __manageRx90_A30 \
movejRx_parameter(__manageRx90_V2,"0.0 0.0 0.0 0.0 0.0 0.0 0.2 0.0 0.0 0.0 0.0 0.0","3")
#define __manageRx90_A31 \
movejRx_controler(__manageRx90_V2)
#define __manageRx90_A32 \
veljRx_parameter(__manageRx90_V7,"0.0 0.0 .0 0.0 0.0 0.0","0")
#define __manageRx90_A33 \
veljRx_controler(__manageRx90_V7)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __manageRx90_A34 \

#define __manageRx90_A35 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int manageRx90_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __manageRx90__reset_input () {
__manageRx90_V0 = _false;
__manageRx90_V1 = _false;
__manageRx90_V3 = _false;
__manageRx90_V4 = _false;
__manageRx90_V5 = _false;
__manageRx90_V6 = _false;
__manageRx90_V8 = _false;
__manageRx90_V9 = _false;
__manageRx90_V10 = _false;
__manageRx90_V11 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __manageRx90_R[5] = {_true,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int manageRx90 () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[18];
if (!(_true)) {
__manageRx90_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A11\n");
#endif
}
E[0] = (__manageRx90_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__manageRx90_A3)));
if (E[0]) {
__manageRx90_A34;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A34\n");
#endif
}
E[1] = (__manageRx90_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__manageRx90_A7)));
if (E[1]) {
__manageRx90_A35;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A35\n");
#endif
}
E[2] = (__manageRx90_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__manageRx90_A1));
E[3] = (__manageRx90_R[1]&&!(__manageRx90_R[0]));
E[4] = (E[3]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__manageRx90_A1));
E[4] = (E[2]||E[4]);
if (E[4]) {
__manageRx90_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A12\n");
#endif
}
if (!(_true)) {
__manageRx90_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A13\n");
#endif
}
E[2] = (__manageRx90_R[4]&&!(__manageRx90_R[0]));
E[5] = (E[2]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__manageRx90_A2));
if (E[5]) {
__manageRx90_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A14\n");
#endif
}
E[6] = (__manageRx90_R[3]&&!(__manageRx90_R[0]));
E[7] = (E[6]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__manageRx90_A9)));
E[8] = (E[7]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__manageRx90_A10));
E[9] = (__manageRx90_R[2]&&!(__manageRx90_R[0]));
E[10] = (E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__manageRx90_A5)));
E[11] = (E[10]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__manageRx90_A6));
E[12] = (__manageRx90_R[2]||__manageRx90_R[3]);
E[13] = (E[12]||__manageRx90_R[4]);
E[6] = (E[6]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__manageRx90_A9));
E[6] = (E[6]||E[4]);
if (E[6]) {
__manageRx90_A30;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A30\n");
#endif
}
if (E[6]) {
__manageRx90_A31;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A31\n");
#endif
}
E[10] = (E[10]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__manageRx90_A6)));
E[10] = (E[6]||((__manageRx90_R[2]&&E[10])));
E[9] = (E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__manageRx90_A5));
if (E[9]) {
__manageRx90_A32;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A32\n");
#endif
}
if (E[9]) {
__manageRx90_A33;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A33\n");
#endif
}
E[7] = (E[7]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__manageRx90_A10)));
E[7] = (E[9]||((__manageRx90_R[3]&&E[7])));
E[12] = ((((E[13]&&!(E[12])))||E[10])||E[7]);
E[14] = ((E[12]||E[8])||E[11]);
E[2] = (E[2]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__manageRx90_A2)));
E[2] = (E[4]||((__manageRx90_R[4]&&E[2])));
E[15] = (((E[13]&&!(__manageRx90_R[4])))||E[2]);
E[16] = ((((E[8]||E[11]))&&E[14])&&E[15]);
if (E[16]) {
__manageRx90_A15;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A15\n");
#endif
}
if (E[6]) {
__manageRx90_A16;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A16\n");
#endif
}
if (E[5]) {
__manageRx90_A17;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A17\n");
#endif
}
if (E[9]) {
__manageRx90_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A18\n");
#endif
}
if (E[5]) {
__manageRx90_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__manageRx90_A19\n");
#endif
}
E[11] = (E[8]||E[11]);
E[14] = ((E[5]&&E[14])&&((E[15]||E[5])));
E[8] = ((E[16]||E[14]));
E[17] = (__manageRx90_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__manageRx90_A1)));
E[3] = (E[3]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__manageRx90_A1)));
E[3] = (E[17]||((__manageRx90_R[1]&&E[3])));
E[15] = ((((((((E[10]||E[7])||E[2]))&&E[12])&&E[15]))||E[3]));
E[13] = (E[13]||__manageRx90_R[1]);
E[14] = ((((E[14]||E[16]))||E[16])||E[14]);
__manageRx90_R[2] = (E[10]&&!(E[14]));
__manageRx90_R[3] = (E[7]&&!(E[14]));
__manageRx90_R[4] = (E[2]&&!(E[14]));
__manageRx90_R[0] = !(_true);
__manageRx90_R[1] = E[3];
__manageRx90__reset_input();
return E[15];
}

/* AUTOMATON RESET */

int manageRx90_reset () {
__manageRx90_R[0] = _true;
__manageRx90_R[1] = _false;
__manageRx90_R[2] = _false;
__manageRx90_R[3] = _false;
__manageRx90_R[4] = _false;
__manageRx90__reset_input();
return 0;
}
