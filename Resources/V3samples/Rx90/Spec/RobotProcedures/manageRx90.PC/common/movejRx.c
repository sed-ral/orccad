/* sscc : C CODE OF SORTED EQUATIONS movejRx - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __movejRx_GENERIC_TEST(TEST) return TEST;
typedef void (*__movejRx_APF)();
static __movejRx_APF *__movejRx_PActionArray;

#include "movejRx.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __movejRx_V0;
static boolean __movejRx_V1;
static boolean __movejRx_V2;
static boolean __movejRx_V3;
static boolean __movejRx_V4;
static boolean __movejRx_V5;
static boolean __movejRx_V6;
static orcStrlPtr __movejRx_V7;
static boolean __movejRx_V8;
static boolean __movejRx_V9;
static boolean __movejRx_V10;
static boolean __movejRx_V11;
static orcStrlPtr __movejRx_V12;
static orcStrlPtr __movejRx_V13;
static orcStrlPtr __movejRx_V14;
static orcStrlPtr __movejRx_V15;


/* INPUT FUNCTIONS */

void movejRx_I_jointlimit () {
__movejRx_V0 = _true;
}
void movejRx_I_endtraj () {
__movejRx_V1 = _true;
}
void movejRx_I_endtime () {
__movejRx_V2 = _true;
}
void movejRx_I_movejRxTimeoutgoodtraj () {
__movejRx_V3 = _true;
}
void movejRx_I_goodtraj () {
__movejRx_V4 = _true;
}
void movejRx_I_trackerr () {
__movejRx_V5 = _true;
}
void movejRx_I_InitOK_Rx90 () {
__movejRx_V6 = _true;
}
void movejRx_I_movejRx_Start (orcStrlPtr __V) {
_orcStrlPtr(&__movejRx_V7,__V);
__movejRx_V8 = _true;
}
void movejRx_I_START_movejRx () {
__movejRx_V9 = _true;
}
void movejRx_I_FinTransite_Rx90 () {
__movejRx_V10 = _true;
}
void movejRx_I_Abort_Local_movejRx () {
__movejRx_V11 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __movejRx_A1 \
__movejRx_V0
#define __movejRx_A2 \
__movejRx_V1
#define __movejRx_A3 \
__movejRx_V2
#define __movejRx_A4 \
__movejRx_V3
#define __movejRx_A5 \
__movejRx_V4
#define __movejRx_A6 \
__movejRx_V5
#define __movejRx_A7 \
__movejRx_V6
#define __movejRx_A8 \
__movejRx_V8
#define __movejRx_A9 \
__movejRx_V9
#define __movejRx_A10 \
__movejRx_V10
#define __movejRx_A11 \
__movejRx_V11

/* OUTPUT ACTIONS */

#define __movejRx_A12 \
movejRx_O_STARTED_movejRx()
#define __movejRx_A13 \
movejRx_O_GoodEnd_movejRx()
#define __movejRx_A14 \
movejRx_O_ActivatemovejRx_Rx90(__movejRx_V12)
#define __movejRx_A15 \
movejRx_O_T2_movejRx()
#define __movejRx_A16 \
movejRx_O_movejRxTransite(__movejRx_V13)
#define __movejRx_A17 \
movejRx_O_Abort_movejRx(__movejRx_V14)
#define __movejRx_A18 \
movejRx_O_BF_movejRx()
#define __movejRx_A19 \
movejRx_O_T3_movejRx()
#define __movejRx_A20 \
movejRx_O_StartTransite_Rx90()
#define __movejRx_A21 \
movejRx_O_Prev_rt_Rx90(__movejRx_V15)
#define __movejRx_A22 \
movejRx_O_ReadyToStart_Rx90()

/* ASSIGNMENTS */

#define __movejRx_A23 \
__movejRx_V0 = _false
#define __movejRx_A24 \
__movejRx_V1 = _false
#define __movejRx_A25 \
__movejRx_V2 = _false
#define __movejRx_A26 \
__movejRx_V3 = _false
#define __movejRx_A27 \
__movejRx_V4 = _false
#define __movejRx_A28 \
__movejRx_V5 = _false
#define __movejRx_A29 \
__movejRx_V6 = _false
#define __movejRx_A30 \
__movejRx_V8 = _false
#define __movejRx_A31 \
__movejRx_V9 = _false
#define __movejRx_A32 \
__movejRx_V10 = _false
#define __movejRx_A33 \
__movejRx_V11 = _false
#define __movejRx_A34 \
_orcStrlPtr(&__movejRx_V14,__movejRx_V7)
#define __movejRx_A35 \
_orcStrlPtr(&__movejRx_V12,__movejRx_V7)
#define __movejRx_A36 \
_orcStrlPtr(&__movejRx_V15,__movejRx_V7)
#define __movejRx_A37 \
_orcStrlPtr(&__movejRx_V13,__movejRx_V7)
#define __movejRx_A38 \
_orcStrlPtr(&__movejRx_V13,__movejRx_V7)
#define __movejRx_A39 \
_orcStrlPtr(&__movejRx_V13,__movejRx_V7)
#define __movejRx_A40 \
_orcStrlPtr(&__movejRx_V13,__movejRx_V7)

/* PROCEDURE CALLS */

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __movejRx_A41 \

#define __movejRx_A42 \

#define __movejRx_A43 \

#define __movejRx_A44 \

#define __movejRx_A45 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int movejRx_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __movejRx__reset_input () {
__movejRx_V0 = _false;
__movejRx_V1 = _false;
__movejRx_V2 = _false;
__movejRx_V3 = _false;
__movejRx_V4 = _false;
__movejRx_V5 = _false;
__movejRx_V6 = _false;
__movejRx_V8 = _false;
__movejRx_V9 = _false;
__movejRx_V10 = _false;
__movejRx_V11 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __movejRx_R[10] = {_true,_false,_false,_false,_false,_false,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int movejRx () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[35];
E[0] = (__movejRx_R[9]&&!(__movejRx_R[0]));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__movejRx_A2));
E[2] = ((((__movejRx_R[5]||__movejRx_R[6])||__movejRx_R[7])||__movejRx_R[8])||__movejRx_R[9]);
E[3] = (__movejRx_R[2]||__movejRx_R[3]);
E[4] = (E[3]&&!(__movejRx_R[0]));
E[5] = (E[4]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11)));
E[6] = (__movejRx_R[2]&&E[5]);
E[7] = (E[6]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__movejRx_A4)));
E[7] = (__movejRx_R[2]&&E[7]);
E[8] = (E[7]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__movejRx_A5));
E[5] = (__movejRx_R[3]&&E[5]);
E[9] = (E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__movejRx_A7));
E[10] = (((E[3]&&!(__movejRx_R[2])))||E[8]);
E[11] = (((E[3]&&!(__movejRx_R[3])))||E[9]);
E[9] = ((((E[8]||E[9]))&&E[10])&&E[11]);
E[8] = (E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__movejRx_A10));
E[12] = (__movejRx_R[4]&&!(__movejRx_R[0]));
E[13] = (E[12]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11)));
E[13] = (__movejRx_R[4]&&E[13]);
E[14] = (E[13]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__movejRx_A10));
E[14] = (E[8]||E[14]);
if (E[14]) {
__movejRx_A35;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A35\n");
#endif
}
if (E[14]) {
__movejRx_A36;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A36\n");
#endif
}
E[8] = (__movejRx_R[5]&&!(__movejRx_R[0]));
E[15] = (E[8]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11)));
E[15] = (E[14]||((__movejRx_R[5]&&E[15])));
E[16] = (((E[2]&&!(__movejRx_R[5])))||E[15]);
E[17] = (__movejRx_R[6]&&!(__movejRx_R[0]));
E[18] = (E[17]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__movejRx_A3)));
E[18] = (E[14]||((__movejRx_R[6]&&E[18])));
E[19] = (((E[2]&&!(__movejRx_R[6])))||E[18]);
E[20] = (__movejRx_R[7]&&!(__movejRx_R[0]));
E[21] = (E[20]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__movejRx_A6)));
E[21] = (E[14]||((__movejRx_R[7]&&E[21])));
E[22] = (((E[2]&&!(__movejRx_R[7])))||E[21]);
E[23] = (__movejRx_R[8]&&!(__movejRx_R[0]));
E[24] = (E[23]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__movejRx_A1)));
E[24] = (E[14]||((__movejRx_R[8]&&E[24])));
E[25] = (((E[2]&&!(__movejRx_R[8])))||E[24]);
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__movejRx_A2)));
E[0] = (E[14]||((__movejRx_R[9]&&E[0])));
E[26] = (((E[2]&&!(__movejRx_R[9])))||E[0]);
E[27] = (E[26]||E[1]);
E[1] = (((((E[1]&&E[16])&&E[19])&&E[22])&&E[25])&&E[27]);
if (E[1]) {
__movejRx_A39;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A39\n");
#endif
}
E[8] = (E[8]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11));
if (E[8]) {
__movejRx_A38;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A38\n");
#endif
}
E[28] = (E[16]||E[8]);
E[29] = (((((E[8]&&E[28])&&E[19])&&E[22])&&E[25])&&E[27]);
E[12] = (E[12]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11));
if (E[12]) {
__movejRx_A37;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A37\n");
#endif
}
E[4] = (E[4]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11));
if (E[4]) {
__movejRx_A34;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A34\n");
#endif
}
E[30] = (__movejRx_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__movejRx_A8)));
if (E[30]) {
__movejRx_A41;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A41\n");
#endif
}
if (__movejRx_R[0]) {
__movejRx_A42;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A42\n");
#endif
}
if (__movejRx_R[0]) {
__movejRx_A43;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A43\n");
#endif
}
if (__movejRx_R[0]) {
__movejRx_A44;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A44\n");
#endif
}
if (__movejRx_R[0]) {
__movejRx_A45;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A45\n");
#endif
}
E[31] = (((((E[1]||E[29])||E[12])||E[4]))||__movejRx_R[0]);
E[32] = (E[31]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__movejRx_A9));
E[33] = (__movejRx_R[1]&&!(__movejRx_R[0]));
E[34] = (E[33]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__movejRx_A9));
E[34] = (E[32]||E[34]);
if (E[34]) {
__movejRx_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A12\n");
#endif
}
if (E[1]) {
__movejRx_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A13\n");
#endif
}
if (E[14]) {
__movejRx_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A14\n");
#endif
}
if (!(_true)) {
__movejRx_A15;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A15\n");
#endif
}
if ((((E[8]||E[1])||E[12]))) {
__movejRx_A16;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A16\n");
#endif
}
if (E[4]) {
__movejRx_A17;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A17\n");
#endif
}
if (E[1]) {
__movejRx_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A18\n");
#endif
}
E[17] = (E[17]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__movejRx_A3));
E[20] = (E[20]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__movejRx_A6));
E[23] = (E[23]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__movejRx_A1));
E[27] = ((((((((E[17]||E[20])||E[23]))&&E[28])&&((E[19]||E[17])))&&((E[22]||E[20])))&&((E[25]||E[23])))&&E[27]);
E[6] = (E[6]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__movejRx_A4));
E[7] = (E[7]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__movejRx_A5)));
E[7] = (__movejRx_R[2]&&E[7]);
E[10] = (E[10]||E[7]);
E[5] = (E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__movejRx_A7)));
E[5] = (__movejRx_R[3]&&E[5]);
E[11] = (E[11]||E[5]);
E[28] = ((E[6]&&((E[10]||E[6])))&&E[11]);
E[32] = (E[27]||E[28]);
if (E[32]) {
__movejRx_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A19\n");
#endif
}
if ((((E[8]||E[1])||E[12]))) {
__movejRx_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A20\n");
#endif
}
if (E[14]) {
__movejRx_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A21\n");
#endif
}
if (E[9]) {
__movejRx_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A22\n");
#endif
}
E[23] = (((E[6]||E[17])||E[20])||E[23]);
E[20] = ((E[8]||E[12])||E[4]);
E[17] = E[32];
E[6] = (E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__movejRx_A10)));
E[13] = (E[13]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__movejRx_A10)));
E[13] = (E[6]||((__movejRx_R[4]&&E[13])));
E[31] = (E[31]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__movejRx_A9)));
E[33] = (E[33]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__movejRx_A9)));
E[33] = (E[31]||((__movejRx_R[1]&&E[33])));
E[11] = (((E[34]||((((((((((((((E[15]||E[18])||E[21])||E[24])||E[0]))&&E[16])&&E[19])&&E[22])&&E[25])&&E[26]))||E[13])||(((((E[7]||E[5]))&&E[10])&&E[11])))))||E[33]));
E[3] = ((((E[2]||__movejRx_R[4])||E[3]))||__movejRx_R[1]);
E[28] = (E[28]||E[32]);
__movejRx_R[2] = (E[34]||((E[7]&&!(E[28]))));
__movejRx_R[3] = (E[34]||((E[5]&&!(E[28]))));
__movejRx_R[4] = (E[13]&&!(E[32]));
E[27] = (((((((((E[32]||E[29])||E[27]))||E[29])||E[27]))||E[1])||E[29])||E[27]);
__movejRx_R[5] = (E[15]&&!(E[27]));
__movejRx_R[6] = (E[18]&&!(E[27]));
__movejRx_R[7] = (E[21]&&!(E[27]));
__movejRx_R[8] = (E[24]&&!(E[27]));
__movejRx_R[9] = (E[0]&&!(E[27]));
__movejRx_R[0] = !(_true);
__movejRx_R[1] = E[33];
__movejRx__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int movejRx_reset () {
__movejRx_R[0] = _true;
__movejRx_R[1] = _false;
__movejRx_R[2] = _false;
__movejRx_R[3] = _false;
__movejRx_R[4] = _false;
__movejRx_R[5] = _false;
__movejRx_R[6] = _false;
__movejRx_R[7] = _false;
__movejRx_R[8] = _false;
__movejRx_R[9] = _false;
__movejRx__reset_input();
return 0;
}
