/* sscc : C CODE OF SORTED EQUATIONS aa - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __aa_GENERIC_TEST(TEST) return TEST;
typedef void (*__aa_APF)();
static __aa_APF *__aa_PActionArray;

#include "aa.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _movejRx_controler_DEFINED
#ifndef movejRx_controler
extern void movejRx_controler(orcStrlPtr);
#endif
#endif
#ifndef _movejRx_fileparameter_DEFINED
#ifndef movejRx_fileparameter
extern void movejRx_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _movejRx_parameter_DEFINED
#ifndef movejRx_parameter
extern void movejRx_parameter(orcStrlPtr ,string ,string);
#endif
#endif
#ifndef _veljRx_controler_DEFINED
#ifndef veljRx_controler
extern void veljRx_controler(orcStrlPtr);
#endif
#endif
#ifndef _veljRx_fileparameter_DEFINED
#ifndef veljRx_fileparameter
extern void veljRx_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _veljRx_parameter_DEFINED
#ifndef veljRx_parameter
extern void veljRx_parameter(orcStrlPtr ,string ,string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static orcStrlPtr __aa_V0;
static boolean __aa_V1;
static boolean __aa_V2;
static boolean __aa_V3;
static boolean __aa_V4;
static boolean __aa_V5;
static boolean __aa_V6;
static boolean __aa_V7;
static boolean __aa_V8;
static orcStrlPtr __aa_V9;
static boolean __aa_V10;
static boolean __aa_V11;
static boolean __aa_V12;
static boolean __aa_V13;
static boolean __aa_V14;
static orcStrlPtr __aa_V15;
static boolean __aa_V16;
static boolean __aa_V17;
static boolean __aa_V18;
static boolean __aa_V19;
static boolean __aa_V20;
static boolean __aa_V21;
static boolean __aa_V22;
static boolean __aa_V23;
static boolean __aa_V24;
static boolean __aa_V25;
static boolean __aa_V26;
static orcStrlPtr __aa_V27;
static orcStrlPtr __aa_V28;
static orcStrlPtr __aa_V29;
static orcStrlPtr __aa_V30;
static orcStrlPtr __aa_V31;
static orcStrlPtr __aa_V32;
static orcStrlPtr __aa_V33;
static orcStrlPtr __aa_V34;
static orcStrlPtr __aa_V35;
static orcStrlPtr __aa_V36;
static orcStrlPtr __aa_V37;
static boolean __aa_V38;


/* INPUT FUNCTIONS */

void aa_I_Prr_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
void aa_I_jointlimit () {
__aa_V2 = _true;
}
void aa_I_endtraj () {
__aa_V3 = _true;
}
void aa_I_endtime () {
__aa_V4 = _true;
}
void aa_I_InitOK_Rx90 () {
__aa_V5 = _true;
}
void aa_I_USERSTART_veljRx () {
__aa_V6 = _true;
}
void aa_I_T2_veljRx () {
__aa_V7 = _true;
}
void aa_I_CmdStopOK_Rx90 () {
__aa_V8 = _true;
}
void aa_I_veljRx_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V9,__V);
__aa_V10 = _true;
}
void aa_I_movejRxTimeoutgoodtraj () {
__aa_V11 = _true;
}
void aa_I_goodtraj () {
__aa_V12 = _true;
}
void aa_I_T2_movejRx () {
__aa_V13 = _true;
}
void aa_I_ReadyToStop_Rx90 () {
__aa_V14 = _true;
}
void aa_I_movejRx_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V15,__V);
__aa_V16 = _true;
}
void aa_I_trackerr () {
__aa_V17 = _true;
}
void aa_I_KillOK_Rx90 () {
__aa_V18 = _true;
}
void aa_I_ActivateOK_Rx90 () {
__aa_V19 = _true;
}
void aa_I_endPrR () {
__aa_V20 = _true;
}
void aa_I_endTR () {
__aa_V21 = _true;
}
void aa_I_USERSTART_movejRx () {
__aa_V22 = _true;
}
void aa_I_ROBOT_FAIL () {
__aa_V23 = _true;
}
void aa_I_SOFT_FAIL () {
__aa_V24 = _true;
}
void aa_I_SENSOR_FAIL () {
__aa_V25 = _true;
}
void aa_I_CPU_OVERLOAD () {
__aa_V26 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __aa_A1 \
__aa_V1
#define __aa_A2 \
__aa_V2
#define __aa_A3 \
__aa_V3
#define __aa_A4 \
__aa_V4
#define __aa_A5 \
__aa_V5
#define __aa_A6 \
__aa_V6
#define __aa_A7 \
__aa_V7
#define __aa_A8 \
__aa_V8
#define __aa_A9 \
__aa_V10
#define __aa_A10 \
__aa_V11
#define __aa_A11 \
__aa_V12
#define __aa_A12 \
__aa_V13
#define __aa_A13 \
__aa_V14
#define __aa_A14 \
__aa_V16
#define __aa_A15 \
__aa_V17
#define __aa_A16 \
__aa_V18
#define __aa_A17 \
__aa_V19
#define __aa_A18 \
__aa_V20
#define __aa_A19 \
__aa_V21
#define __aa_A20 \
__aa_V22
#define __aa_A21 \
__aa_V23
#define __aa_A22 \
__aa_V24
#define __aa_A23 \
__aa_V25
#define __aa_A24 \
__aa_V26

/* OUTPUT ACTIONS */

#define __aa_A25 \
aa_O_Activate(__aa_V27)
#define __aa_A26 \
aa_O_ActivateveljRx_Rx90(__aa_V28)
#define __aa_A27 \
aa_O_veljRxTransite(__aa_V29)
#define __aa_A28 \
aa_O_Abort_veljRx(__aa_V30)
#define __aa_A29 \
aa_O_STARTED_veljRx()
#define __aa_A30 \
aa_O_GoodEnd_veljRx()
#define __aa_A31 \
aa_O_GoodEnd_manageRx90()
#define __aa_A32 \
aa_O_Abort_manageRx90()
#define __aa_A33 \
aa_O_STARTED_manageRx90()
#define __aa_A34 \
aa_O_ActivatemovejRx_Rx90(__aa_V31)
#define __aa_A35 \
aa_O_movejRxTransite(__aa_V32)
#define __aa_A36 \
aa_O_Abort_movejRx(__aa_V33)
#define __aa_A37 \
aa_O_STARTED_movejRx()
#define __aa_A38 \
aa_O_GoodEnd_movejRx()
#define __aa_A39 \
aa_O_EndKill(__aa_V34)
#define __aa_A40 \
aa_O_FinBF(__aa_V35)
#define __aa_A41 \
aa_O_FinT3(__aa_V36)
#define __aa_A42 \
aa_O_GoodEndRPr()
#define __aa_A43 \
aa_O_T3RPr()

/* ASSIGNMENTS */

#define __aa_A44 \
__aa_V1 = _false
#define __aa_A45 \
__aa_V2 = _false
#define __aa_A46 \
__aa_V3 = _false
#define __aa_A47 \
__aa_V4 = _false
#define __aa_A48 \
__aa_V5 = _false
#define __aa_A49 \
__aa_V6 = _false
#define __aa_A50 \
__aa_V7 = _false
#define __aa_A51 \
__aa_V8 = _false
#define __aa_A52 \
__aa_V10 = _false
#define __aa_A53 \
__aa_V11 = _false
#define __aa_A54 \
__aa_V12 = _false
#define __aa_A55 \
__aa_V13 = _false
#define __aa_A56 \
__aa_V14 = _false
#define __aa_A57 \
__aa_V16 = _false
#define __aa_A58 \
__aa_V17 = _false
#define __aa_A59 \
__aa_V18 = _false
#define __aa_A60 \
__aa_V19 = _false
#define __aa_A61 \
__aa_V20 = _false
#define __aa_A62 \
__aa_V21 = _false
#define __aa_A63 \
__aa_V22 = _false
#define __aa_A64 \
__aa_V23 = _false
#define __aa_A65 \
__aa_V24 = _false
#define __aa_A66 \
__aa_V25 = _false
#define __aa_A67 \
__aa_V26 = _false
#define __aa_A68 \
_orcStrlPtr(&__aa_V34,__aa_V37)
#define __aa_A69 \
_orcStrlPtr(&__aa_V35,__aa_V0)
#define __aa_A70 \
_orcStrlPtr(&__aa_V36,__aa_V0)
#define __aa_A71 \
_orcStrlPtr(&__aa_V33,__aa_V15)
#define __aa_A72 \
_orcStrlPtr(&__aa_V31,__aa_V15)
#define __aa_A73 \
_orcStrlPtr(&__aa_V37,__aa_V15)
#define __aa_A74 \
_orcStrlPtr(&__aa_V32,__aa_V15)
#define __aa_A75 \
_orcStrlPtr(&__aa_V32,__aa_V15)
#define __aa_A76 \
_orcStrlPtr(&__aa_V32,__aa_V15)
#define __aa_A77 \
_orcStrlPtr(&__aa_V32,__aa_V15)
#define __aa_A78 \
_orcStrlPtr(&__aa_V30,__aa_V9)
#define __aa_A79 \
_orcStrlPtr(&__aa_V28,__aa_V9)
#define __aa_A80 \
_orcStrlPtr(&__aa_V37,__aa_V9)
#define __aa_A81 \
_orcStrlPtr(&__aa_V29,__aa_V9)
#define __aa_A82 \
_orcStrlPtr(&__aa_V29,__aa_V9)
#define __aa_A83 \
_orcStrlPtr(&__aa_V29,__aa_V9)
#define __aa_A84 \
_orcStrlPtr(&__aa_V29,__aa_V9)
#define __aa_A85 \
_orcStrlPtr(&__aa_V34,__aa_V37)

/* PROCEDURE CALLS */

#define __aa_A86 \
movejRx_parameter(__aa_V15,"0.0 0.0 0.0 0.0 0.0 0.0 0.2 0.0 0.0 0.0 0.0 0.0","3")
#define __aa_A87 \
movejRx_controler(__aa_V15)
#define __aa_A88 \
veljRx_parameter(__aa_V9,"0.0 0.0 .0 0.0 0.0 0.0","0")
#define __aa_A89 \
veljRx_controler(__aa_V9)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __aa_A90 \

#define __aa_A91 \

#define __aa_A92 \

#define __aa_A93 \

#define __aa_A94 \

#define __aa_A95 \

#define __aa_A96 \

#define __aa_A97 \

#define __aa_A98 \

#define __aa_A99 \

#define __aa_A100 \

#define __aa_A101 \

#define __aa_A102 \

#define __aa_A103 \

#define __aa_A104 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int aa_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __aa__reset_input () {
__aa_V1 = _false;
__aa_V2 = _false;
__aa_V3 = _false;
__aa_V4 = _false;
__aa_V5 = _false;
__aa_V6 = _false;
__aa_V7 = _false;
__aa_V8 = _false;
__aa_V10 = _false;
__aa_V11 = _false;
__aa_V12 = _false;
__aa_V13 = _false;
__aa_V14 = _false;
__aa_V16 = _false;
__aa_V17 = _false;
__aa_V18 = _false;
__aa_V19 = _false;
__aa_V20 = _false;
__aa_V21 = _false;
__aa_V22 = _false;
__aa_V23 = _false;
__aa_V24 = _false;
__aa_V25 = _false;
__aa_V26 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __aa_R[30] = {_true,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int aa () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[81];
if (!(_true)) {
__aa_A25;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A25\n");
#endif
}
E[0] = (__aa_R[15]&&((__aa_R[15]&&!(__aa_R[0]))));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5));
E[2] = (__aa_R[26]&&!(__aa_R[0]));
E[3] = (__aa_R[6]||__aa_R[7]);
E[4] = (E[3]&&!(__aa_R[0]));
E[5] = (__aa_R[6]&&E[4]);
E[6] = (E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10)));
E[6] = (__aa_R[6]&&E[6]);
E[7] = (E[6]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11));
E[4] = (__aa_R[7]&&E[4]);
E[8] = (E[4]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5));
E[9] = (((E[3]&&!(__aa_R[6])))||E[7]);
E[10] = (((E[3]&&!(__aa_R[7])))||E[8]);
E[8] = ((((E[7]||E[8]))&&E[9])&&E[10]);
E[7] = (E[8]||E[1]);
E[11] = (E[2]&&E[7]);
E[12] = (__aa_R[29]&&!(__aa_R[0]));
E[13] = (E[12]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8));
E[14] = (E[11]||E[13]);
E[15] = (__aa_R[16]&&((__aa_R[16]&&!(__aa_R[0]))));
E[16] = (((E[1]&&E[14]))||((E[15]&&E[14])));
if (E[16]) {
__aa_A79;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A79\n");
#endif
}
if (E[16]) {
__aa_A26;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A26\n");
#endif
}
if (!(_true)) {
__aa_A82;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A82\n");
#endif
}
E[17] = (__aa_R[21]&&!(__aa_R[0]));
E[18] = (E[17]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__aa_A19));
E[19] = ((((__aa_R[17]||__aa_R[18])||__aa_R[19])||__aa_R[20])||__aa_R[21]);
if (E[16]) {
__aa_A80;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A80\n");
#endif
}
E[20] = (E[16]||((__aa_R[17]&&((__aa_R[17]&&!(__aa_R[0]))))));
E[21] = (((E[19]&&!(__aa_R[17])))||E[20]);
E[22] = (__aa_R[18]&&!(__aa_R[0]));
E[23] = (E[22]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15)));
E[23] = (E[16]||((__aa_R[18]&&E[23])));
E[24] = (((E[19]&&!(__aa_R[18])))||E[23]);
E[25] = (__aa_R[19]&&!(__aa_R[0]));
E[26] = (E[25]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2)));
E[26] = (E[16]||((__aa_R[19]&&E[26])));
E[27] = (((E[19]&&!(__aa_R[19])))||E[26]);
E[28] = (__aa_R[20]&&!(__aa_R[0]));
E[29] = (E[28]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__aa_A18)));
E[29] = (E[16]||((__aa_R[20]&&E[29])));
E[30] = (((E[19]&&!(__aa_R[20])))||E[29]);
E[17] = (E[17]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__aa_A19)));
E[17] = (E[16]||((__aa_R[21]&&E[17])));
E[31] = (((E[19]&&!(__aa_R[21])))||E[17]);
E[32] = (E[31]||E[18]);
E[18] = (((((E[18]&&E[21])&&E[24])&&E[27])&&E[30])&&E[32]);
if (E[18]) {
__aa_A83;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A83\n");
#endif
}
if (!(_true)) {
__aa_A81;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A81\n");
#endif
}
if (E[18]) {
__aa_A27;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A27\n");
#endif
}
if (!(_true)) {
__aa_A78;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A78\n");
#endif
}
if (!(_true)) {
__aa_A28;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A28\n");
#endif
}
E[33] = (__aa_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1)));
if (E[33]) {
__aa_A90;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A90\n");
#endif
}
E[34] = (__aa_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9)));
if (E[34]) {
__aa_A91;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A91\n");
#endif
}
E[35] = (__aa_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14)));
if (E[35]) {
__aa_A92;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A92\n");
#endif
}
if (__aa_R[0]) {
__aa_A93;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A93\n");
#endif
}
if (__aa_R[0]) {
__aa_A94;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A94\n");
#endif
}
if (__aa_R[0]) {
__aa_A95;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A95\n");
#endif
}
if (__aa_R[0]) {
__aa_A96;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A96\n");
#endif
}
if (__aa_R[0]) {
__aa_A97;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A97\n");
#endif
}
if (__aa_R[0]) {
__aa_A98;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A98\n");
#endif
}
if (__aa_R[0]) {
__aa_A99;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A99\n");
#endif
}
if (__aa_R[0]) {
__aa_A100;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A100\n");
#endif
}
if (__aa_R[0]) {
__aa_A101;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A101\n");
#endif
}
if (__aa_R[0]) {
__aa_A102;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A102\n");
#endif
}
if (__aa_R[0]) {
__aa_A103;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A103\n");
#endif
}
if (__aa_R[0]) {
__aa_A104;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A104\n");
#endif
}
E[36] = (E[18]||__aa_R[0]);
E[37] = (__aa_R[23]&&!(__aa_R[0]));
E[38] = (__aa_R[13]&&!(__aa_R[0]));
E[39] = (E[38]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3));
E[40] = ((((__aa_R[9]||__aa_R[10])||__aa_R[11])||__aa_R[12])||__aa_R[13]);
E[41] = (__aa_R[8]&&((__aa_R[8]&&!(__aa_R[0]))));
E[42] = (((E[8]&&E[14]))||((E[41]&&E[14])));
if (E[42]) {
__aa_A72;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A72\n");
#endif
}
if (E[42]) {
__aa_A73;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A73\n");
#endif
}
E[43] = (E[42]||((__aa_R[9]&&((__aa_R[9]&&!(__aa_R[0]))))));
E[44] = (((E[40]&&!(__aa_R[9])))||E[43]);
E[45] = (__aa_R[10]&&!(__aa_R[0]));
E[46] = (E[45]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4)));
E[46] = (E[42]||((__aa_R[10]&&E[46])));
E[47] = (((E[40]&&!(__aa_R[10])))||E[46]);
E[48] = (__aa_R[11]&&!(__aa_R[0]));
E[49] = (E[48]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15)));
E[49] = (E[42]||((__aa_R[11]&&E[49])));
E[50] = (((E[40]&&!(__aa_R[11])))||E[49]);
E[51] = (__aa_R[12]&&!(__aa_R[0]));
E[52] = (E[51]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2)));
E[52] = (E[42]||((__aa_R[12]&&E[52])));
E[53] = (((E[40]&&!(__aa_R[12])))||E[52]);
E[38] = (E[38]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3)));
E[38] = (E[42]||((__aa_R[13]&&E[38])));
E[54] = (((E[40]&&!(__aa_R[13])))||E[38]);
E[55] = (E[54]||E[39]);
E[39] = (((((E[39]&&E[44])&&E[47])&&E[50])&&E[53])&&E[55]);
if (E[39]) {
__aa_A76;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A76\n");
#endif
}
E[56] = (E[37]&&E[39]);
if (E[56]) {
__aa_A88;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A88\n");
#endif
}
if (E[56]) {
__aa_A89;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A89\n");
#endif
}
E[57] = (__aa_R[14]&&!(__aa_R[0]));
E[58] = (((E[36]&&E[56]))||((E[57]&&E[56])));
if (E[58]) {
__aa_A29;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A29\n");
#endif
}
if (E[18]) {
__aa_A30;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A30\n");
#endif
}
if (!(_true)) {
__aa_A31;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A31\n");
#endif
}
if (!(_true)) {
__aa_A32;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A32\n");
#endif
}
E[59] = (__aa_R[2]&&!(__aa_R[0]));
E[60] = (E[59]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14));
E[61] = (__aa_R[3]&&!(__aa_R[0]));
E[62] = (E[61]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9));
E[63] = (__aa_R[4]&&!(__aa_R[0]));
E[64] = (E[63]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1));
E[65] = ((__aa_R[2]||__aa_R[3])||__aa_R[4]);
E[66] = (((E[65]&&!(__aa_R[2])))||E[60]);
E[67] = (((E[65]&&!(__aa_R[3])))||E[62]);
E[68] = (((E[65]&&!(__aa_R[4])))||E[64]);
E[64] = ((((((E[60]||E[62])||E[64]))&&E[66])&&E[67])&&E[68]);
E[62] = (__aa_R[22]&&!(__aa_R[0]));
E[60] = (((__aa_R[0]&&E[64]))||((E[62]&&E[64])));
if (E[60]) {
__aa_A33;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A33\n");
#endif
}
if (E[42]) {
__aa_A34;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A34\n");
#endif
}
if (!(_true)) {
__aa_A75;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A75\n");
#endif
}
if (!(_true)) {
__aa_A74;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A74\n");
#endif
}
if (E[39]) {
__aa_A35;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A35\n");
#endif
}
if (!(_true)) {
__aa_A71;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A71\n");
#endif
}
if (!(_true)) {
__aa_A36;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A36\n");
#endif
}
E[69] = (E[39]||__aa_R[0]);
E[70] = (__aa_R[24]&&!(__aa_R[0]));
E[71] = (((E[70]&&E[18]))||E[60]);
if (E[71]) {
__aa_A86;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A86\n");
#endif
}
if (E[71]) {
__aa_A87;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A87\n");
#endif
}
E[72] = (__aa_R[5]&&!(__aa_R[0]));
E[73] = (((E[69]&&E[71]))||((E[72]&&E[71])));
if (E[73]) {
__aa_A37;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A37\n");
#endif
}
if (E[39]) {
__aa_A38;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A38\n");
#endif
}
E[74] = (__aa_R[28]&&!(__aa_R[0]));
E[75] = (E[74]&&E[7]);
if (E[75]) {
__aa_A85;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A85\n");
#endif
}
E[76] = (__aa_R[1]&&!(__aa_R[0]));
E[3] = ((((E[40]||__aa_R[8])||E[3]))||__aa_R[5]);
E[19] = ((((__aa_R[15]||E[19])||__aa_R[16]))||__aa_R[14]);
E[40] = (__aa_R[23]||__aa_R[24]);
E[77] = (E[40]||__aa_R[25]);
E[78] = (E[77]||__aa_R[22]);
E[79] = (((__aa_R[26]||__aa_R[27])||__aa_R[28])||__aa_R[29]);
E[80] = (((((E[65]||E[3])||E[19])||E[78])||E[79])||__aa_R[1]);
E[59] = (E[59]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14)));
E[59] = (__aa_R[0]||((__aa_R[2]&&E[59])));
E[61] = (E[61]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9)));
E[61] = (__aa_R[0]||((__aa_R[3]&&E[61])));
E[63] = (E[63]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1)));
E[63] = (__aa_R[0]||((__aa_R[4]&&E[63])));
E[68] = ((((((E[59]||E[61])||E[63]))&&((E[66]||E[59])))&&((E[67]||E[61])))&&((E[68]||E[63])));
E[65] = (((((E[80]&&!(E[65])))||E[64]))||E[68]);
E[45] = (E[45]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4));
E[48] = (E[48]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15));
E[51] = (E[51]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2));
E[55] = ((((((((E[45]||E[48])||E[51]))&&E[44])&&((E[47]||E[45])))&&((E[50]||E[48])))&&((E[53]||E[51])))&&E[55]);
E[5] = (E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10));
E[6] = (E[6]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11)));
E[6] = (__aa_R[6]&&E[6]);
E[9] = (E[9]||E[6]);
E[4] = (E[4]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5)));
E[4] = (__aa_R[7]&&E[4]);
E[10] = (E[10]||E[4]);
E[67] = ((E[5]&&((E[9]||E[5])))&&E[10]);
E[66] = (E[55]||E[67]);
E[41] = (((E[8]&&!(E[14])))||((__aa_R[8]&&((E[41]&&!(E[14]))))));
E[72] = (((E[69]&&!(E[71])))||((__aa_R[5]&&((E[72]&&!(E[71]))))));
E[10] = ((E[73]||((((((((((((((E[43]||E[46])||E[49])||E[52])||E[38]))&&E[44])&&E[47])&&E[50])&&E[53])&&E[54]))||E[41])||(((((E[6]||E[4]))&&E[9])&&E[10])))))||E[72]);
E[3] = (((((E[80]&&!(E[3])))||E[66]))||E[10]);
E[22] = (E[22]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15));
E[25] = (E[25]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2));
E[28] = (E[28]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__aa_A18));
E[32] = ((((((((E[22]||E[25])||E[28]))&&E[21])&&((E[24]||E[22])))&&((E[27]||E[25])))&&((E[30]||E[28])))&&E[32]);
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5)));
E[0] = (__aa_R[15]&&E[0]);
E[14] = (((E[1]&&!(E[14])))||((__aa_R[16]&&((E[15]&&!(E[14]))))));
E[57] = (((E[36]&&!(E[56])))||((__aa_R[14]&&((E[57]&&!(E[56]))))));
E[31] = ((E[58]||(((E[0]||(((((((((((E[20]||E[23])||E[26])||E[29])||E[17]))&&E[21])&&E[24])&&E[27])&&E[30])&&E[31])))||E[14])))||E[57]);
E[19] = (((((E[80]&&!(E[19])))||E[32]))||E[31]);
E[70] = (E[70]&&!(E[18]));
E[30] = (E[70]&&E[32]);
E[37] = (E[37]&&!(E[39]));
E[27] = (E[37]&&E[66]);
E[37] = (E[71]||((__aa_R[23]&&((E[37]&&!(E[66]))))));
E[70] = (E[56]||((__aa_R[24]&&((E[70]&&!(E[32]))))));
E[40] = ((((E[77]&&!(E[40])))||E[37])||E[70]);
E[24] = (E[60]||((__aa_R[25]&&((__aa_R[25]&&!(__aa_R[0]))))));
E[77] = (((E[77]&&!(__aa_R[25])))||E[24]);
E[21] = ((((E[30]||E[27]))&&(((E[40]||E[30])||E[27])))&&E[77]);
E[64] = (((__aa_R[0]&&!(E[64])))||((__aa_R[22]&&((E[62]&&!(E[64]))))));
E[77] = (((((((E[37]||E[70])||E[24]))&&E[40])&&E[77]))||E[64]);
E[78] = (((((E[80]&&!(E[78])))||E[21]))||E[77]);
E[2] = (__aa_R[0]||((__aa_R[26]&&((E[2]&&!(E[7]))))));
E[40] = (__aa_R[27]&&!(__aa_R[0]));
E[62] = (E[39]||E[18]);
E[13] = ((E[11]||E[13])||((__aa_R[27]&&((E[40]&&!(E[62]))))));
E[7] = (((E[40]&&E[62]))||((__aa_R[28]&&((E[74]&&!(E[7]))))));
E[12] = (E[12]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8)));
E[12] = (E[75]||((__aa_R[29]&&E[12])));
E[79] = ((((((E[80]&&!(E[79])))||E[2])||E[13])||E[7])||E[12]);
E[74] = (E[76]&&!(E[21]));
E[62] = (E[74]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__aa_A21)));
E[40] = (E[62]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22)));
E[11] = (E[40]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 23\n"),
#endif
__aa_A23)));
E[36] = (E[11]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 24\n"),
#endif
__aa_A24)));
E[36] = (__aa_R[0]||((__aa_R[1]&&E[36])));
E[80] = (((E[80]&&!(__aa_R[1])))||E[36]);
if (!(_true)) {
__aa_A68;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A68\n");
#endif
}
if (E[75]) {
__aa_A39;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A39\n");
#endif
}
if (!(_true)) {
__aa_A69;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A69\n");
#endif
}
if (!(_true)) {
__aa_A40;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A40\n");
#endif
}
E[76] = (E[76]&&E[21]);
E[74] = (E[74]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__aa_A21));
E[62] = (E[62]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22));
E[40] = (E[40]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 23\n"),
#endif
__aa_A23));
E[11] = (E[11]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 24\n"),
#endif
__aa_A24));
E[15] = (((((((((((E[76]||E[74])||E[62])||E[40])||E[11]))&&E[65])&&E[3])&&E[19])&&E[78])&&E[79])&&((((((E[80]||E[76])||E[74])||E[62])||E[40])||E[11])));
if (E[15]) {
__aa_A70;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A70\n");
#endif
}
if (E[15]) {
__aa_A41;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A41\n");
#endif
}
if (!(_true)) {
__aa_A42;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A42\n");
#endif
}
if (E[15]) {
__aa_A43;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A43\n");
#endif
}
E[1] = (E[42]||E[16]);
E[11] = ((((E[76]||E[74])||E[62])||E[40])||E[11]);
E[51] = (((E[5]||E[45])||E[48])||E[51]);
E[28] = ((E[22]||E[25])||E[28]);
E[27] = (E[30]||E[27]);
E[30] = E[15];
E[80] = ((((((((((((((((E[68]||E[10])||E[31])||E[77])||E[2])||E[13])||E[7])||E[12])||E[36]))&&E[65])&&E[3])&&E[19])&&E[78])&&E[79])&&E[80]));
__aa_R[1] = (E[36]&&!(E[15]));
__aa_R[2] = (E[59]&&!(E[15]));
__aa_R[3] = (E[61]&&!(E[15]));
__aa_R[4] = (E[63]&&!(E[15]));
__aa_R[5] = (E[72]&&!(E[15]));
E[66] = (E[66]||E[15]);
E[67] = (E[67]||E[66]);
__aa_R[6] = (((E[73]&&!(E[15])))||((E[6]&&!(E[67]))));
__aa_R[7] = (((E[73]&&!(E[15])))||((E[4]&&!(E[67]))));
__aa_R[8] = (E[41]&&!(E[66]));
E[55] = ((((((E[66]||E[55]))||E[55]))||E[39])||E[55]);
__aa_R[9] = (E[43]&&!(E[55]));
__aa_R[10] = (E[46]&&!(E[55]));
__aa_R[11] = (E[49]&&!(E[55]));
__aa_R[12] = (E[52]&&!(E[55]));
__aa_R[13] = (E[38]&&!(E[55]));
__aa_R[14] = (E[57]&&!(E[15]));
E[57] = (E[32]||E[15]);
__aa_R[15] = (((E[58]&&!(E[15])))||((E[0]&&!(E[57]))));
__aa_R[16] = (E[14]&&!(E[57]));
E[32] = ((((((E[57]||E[32]))||E[32]))||E[18])||E[32]);
__aa_R[17] = (E[20]&&!(E[32]));
__aa_R[18] = (E[23]&&!(E[32]));
__aa_R[19] = (E[26]&&!(E[32]));
__aa_R[20] = (E[29]&&!(E[32]));
__aa_R[21] = (E[17]&&!(E[32]));
__aa_R[22] = (E[64]&&!(E[15]));
E[21] = (((E[15]||E[21]))||E[21]);
__aa_R[23] = (E[37]&&!(E[21]));
__aa_R[24] = (E[70]&&!(E[21]));
__aa_R[25] = (E[24]&&!(E[21]));
__aa_R[26] = (E[2]&&!(E[15]));
__aa_R[27] = (E[13]&&!(E[15]));
__aa_R[28] = (E[7]&&!(E[15]));
__aa_R[29] = (E[12]&&!(E[15]));
__aa_R[0] = !(_true);
__aa__reset_input();
return E[80];
}

/* AUTOMATON RESET */

int aa_reset () {
__aa_R[0] = _true;
__aa_R[1] = _false;
__aa_R[2] = _false;
__aa_R[3] = _false;
__aa_R[4] = _false;
__aa_R[5] = _false;
__aa_R[6] = _false;
__aa_R[7] = _false;
__aa_R[8] = _false;
__aa_R[9] = _false;
__aa_R[10] = _false;
__aa_R[11] = _false;
__aa_R[12] = _false;
__aa_R[13] = _false;
__aa_R[14] = _false;
__aa_R[15] = _false;
__aa_R[16] = _false;
__aa_R[17] = _false;
__aa_R[18] = _false;
__aa_R[19] = _false;
__aa_R[20] = _false;
__aa_R[21] = _false;
__aa_R[22] = _false;
__aa_R[23] = _false;
__aa_R[24] = _false;
__aa_R[25] = _false;
__aa_R[26] = _false;
__aa_R[27] = _false;
__aa_R[28] = _false;
__aa_R[29] = _false;
__aa__reset_input();
return 0;
}
