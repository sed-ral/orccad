/* sscc : C CODE OF SORTED EQUATIONS veljRx - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __veljRx_GENERIC_TEST(TEST) return TEST;
typedef void (*__veljRx_APF)();
static __veljRx_APF *__veljRx_PActionArray;

                    
/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __veljRx_V0;
static boolean __veljRx_V1;
static boolean __veljRx_V2;
static boolean __veljRx_V3;
static boolean __veljRx_V4;
static integer __veljRx_V5;
static boolean __veljRx_V6;
static boolean __veljRx_V7;
static boolean __veljRx_V8;
static boolean __veljRx_V9;
static integer __veljRx_V10;
static integer __veljRx_V11;
static integer __veljRx_V12;
static integer __veljRx_V13;


/* INPUT FUNCTIONS */

void veljRx_I_trackerr () {
__veljRx_V0 = _true;
}
void veljRx_I_endPrR () {
__veljRx_V1 = _true;
}
void veljRx_I_endTR () {
__veljRx_V2 = _true;
}
void veljRx_I_jointlimit () {
__veljRx_V3 = _true;
}
void veljRx_I_InitOK_Rx90 () {
__veljRx_V4 = _true;
}
void veljRx_I_veljRx_Start (integer __V) {
__veljRx_V5 = __V;
__veljRx_V6 = _true;
}
void veljRx_I_START_veljRx () {
__veljRx_V7 = _true;
}
void veljRx_I_FinTransite_Rx90 () {
__veljRx_V8 = _true;
}
void veljRx_I_Abort_Local_veljRx () {
__veljRx_V9 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __veljRx_A1 \
__veljRx_V0
#define __veljRx_A2 \
__veljRx_V1
#define __veljRx_A3 \
__veljRx_V2
#define __veljRx_A4 \
__veljRx_V3
#define __veljRx_A5 \
__veljRx_V4
#define __veljRx_A6 \
__veljRx_V6
#define __veljRx_A7 \
__veljRx_V7
#define __veljRx_A8 \
__veljRx_V8
#define __veljRx_A9 \
__veljRx_V9

/* OUTPUT ACTIONS */

#define __veljRx_A10 \
veljRx_O_STARTED_veljRx()
#define __veljRx_A11 \
veljRx_O_GoodEnd_veljRx()
#define __veljRx_A12 \
veljRx_O_ActivateveljRx_Rx90(__veljRx_V10)
#define __veljRx_A13 \
veljRx_O_T2_veljRx()
#define __veljRx_A14 \
veljRx_O_veljRxTransite(__veljRx_V11)
#define __veljRx_A15 \
veljRx_O_Abort_veljRx(__veljRx_V12)
#define __veljRx_A16 \
veljRx_O_BF_veljRx()
#define __veljRx_A17 \
veljRx_O_T3_veljRx()
#define __veljRx_A18 \
veljRx_O_StartTransite_Rx90()
#define __veljRx_A19 \
veljRx_O_Prev_rt_Rx90(__veljRx_V13)
#define __veljRx_A20 \
veljRx_O_ReadyToStart_Rx90()

/* ASSIGNMENTS */

#define __veljRx_A21 \
__veljRx_V0 = _false
#define __veljRx_A22 \
__veljRx_V1 = _false
#define __veljRx_A23 \
__veljRx_V2 = _false
#define __veljRx_A24 \
__veljRx_V3 = _false
#define __veljRx_A25 \
__veljRx_V4 = _false
#define __veljRx_A26 \
__veljRx_V6 = _false
#define __veljRx_A27 \
__veljRx_V7 = _false
#define __veljRx_A28 \
__veljRx_V8 = _false
#define __veljRx_A29 \
__veljRx_V9 = _false
#define __veljRx_A30 \
__veljRx_V12 = __veljRx_V5
#define __veljRx_A31 \
__veljRx_V10 = __veljRx_V5
#define __veljRx_A32 \
__veljRx_V13 = __veljRx_V5
#define __veljRx_A33 \
__veljRx_V11 = __veljRx_V5
#define __veljRx_A34 \
__veljRx_V11 = __veljRx_V5
#define __veljRx_A35 \
__veljRx_V11 = __veljRx_V5
#define __veljRx_A36 \
__veljRx_V11 = __veljRx_V5

/* PROCEDURE CALLS */

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __veljRx_A37 \

#define __veljRx_A38 \

#define __veljRx_A39 \

#define __veljRx_A40 \

#define __veljRx_A41 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int veljRx_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __veljRx__reset_input () {
__veljRx_V0 = _false;
__veljRx_V1 = _false;
__veljRx_V2 = _false;
__veljRx_V3 = _false;
__veljRx_V4 = _false;
__veljRx_V6 = _false;
__veljRx_V7 = _false;
__veljRx_V8 = _false;
__veljRx_V9 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __veljRx_R[9] = {_true,_false,_false,_false,_false,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int veljRx () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[30];
E[0] = (__veljRx_R[2]&&!(__veljRx_R[0]));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__veljRx_A9));
if (E[1]) {
__veljRx_A30;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A30\n");
#endif
}
E[2] = (__veljRx_R[8]&&!(__veljRx_R[0]));
E[3] = (E[2]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__veljRx_A3));
E[4] = ((((__veljRx_R[4]||__veljRx_R[5])||__veljRx_R[6])||__veljRx_R[7])||__veljRx_R[8]);
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__veljRx_A9)));
E[0] = (__veljRx_R[2]&&E[0]);
E[5] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__veljRx_A5));
E[6] = (E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__veljRx_A8));
E[7] = (__veljRx_R[3]&&!(__veljRx_R[0]));
E[8] = (E[7]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__veljRx_A9)));
E[8] = (__veljRx_R[3]&&E[8]);
E[9] = (E[8]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__veljRx_A8));
E[9] = (E[6]||E[9]);
if (E[9]) {
__veljRx_A31;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A31\n");
#endif
}
if (E[9]) {
__veljRx_A32;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A32\n");
#endif
}
E[6] = (__veljRx_R[4]&&!(__veljRx_R[0]));
E[10] = (E[6]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__veljRx_A9)));
E[10] = (E[9]||((__veljRx_R[4]&&E[10])));
E[11] = (((E[4]&&!(__veljRx_R[4])))||E[10]);
E[12] = (__veljRx_R[5]&&!(__veljRx_R[0]));
E[13] = (E[12]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__veljRx_A1)));
E[13] = (E[9]||((__veljRx_R[5]&&E[13])));
E[14] = (((E[4]&&!(__veljRx_R[5])))||E[13]);
E[15] = (__veljRx_R[6]&&!(__veljRx_R[0]));
E[16] = (E[15]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__veljRx_A4)));
E[16] = (E[9]||((__veljRx_R[6]&&E[16])));
E[17] = (((E[4]&&!(__veljRx_R[6])))||E[16]);
E[18] = (__veljRx_R[7]&&!(__veljRx_R[0]));
E[19] = (E[18]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__veljRx_A2)));
E[19] = (E[9]||((__veljRx_R[7]&&E[19])));
E[20] = (((E[4]&&!(__veljRx_R[7])))||E[19]);
E[2] = (E[2]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__veljRx_A3)));
E[2] = (E[9]||((__veljRx_R[8]&&E[2])));
E[21] = (((E[4]&&!(__veljRx_R[8])))||E[2]);
E[22] = (E[21]||E[3]);
E[3] = (((((E[3]&&E[11])&&E[14])&&E[17])&&E[20])&&E[22]);
if (E[3]) {
__veljRx_A35;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A35\n");
#endif
}
E[6] = (E[6]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__veljRx_A9));
if (E[6]) {
__veljRx_A34;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A34\n");
#endif
}
E[23] = (E[11]||E[6]);
E[24] = (((((E[6]&&E[23])&&E[14])&&E[17])&&E[20])&&E[22]);
E[7] = (E[7]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__veljRx_A9));
if (E[7]) {
__veljRx_A33;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A33\n");
#endif
}
E[25] = (__veljRx_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__veljRx_A6)));
if (E[25]) {
__veljRx_A37;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A37\n");
#endif
}
if (__veljRx_R[0]) {
__veljRx_A38;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A38\n");
#endif
}
if (__veljRx_R[0]) {
__veljRx_A39;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A39\n");
#endif
}
if (__veljRx_R[0]) {
__veljRx_A40;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A40\n");
#endif
}
if (__veljRx_R[0]) {
__veljRx_A41;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A41\n");
#endif
}
E[26] = (((((E[1]||E[3])||E[24])||E[7]))||__veljRx_R[0]);
E[27] = (E[26]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__veljRx_A7));
E[28] = (__veljRx_R[1]&&!(__veljRx_R[0]));
E[29] = (E[28]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__veljRx_A7));
E[29] = (E[27]||E[29]);
if (E[29]) {
__veljRx_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A10\n");
#endif
}
if (E[3]) {
__veljRx_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A11\n");
#endif
}
if (E[9]) {
__veljRx_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A12\n");
#endif
}
if (!(_true)) {
__veljRx_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A13\n");
#endif
}
if ((((E[6]||E[3])||E[7]))) {
__veljRx_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A14\n");
#endif
}
if (E[1]) {
__veljRx_A15;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A15\n");
#endif
}
if (E[3]) {
__veljRx_A16;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A16\n");
#endif
}
E[12] = (E[12]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__veljRx_A1));
E[15] = (E[15]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__veljRx_A4));
E[18] = (E[18]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__veljRx_A2));
E[22] = ((((((((E[12]||E[15])||E[18]))&&E[23])&&((E[14]||E[12])))&&((E[17]||E[15])))&&((E[20]||E[18])))&&E[22]);
if (E[22]) {
__veljRx_A17;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A17\n");
#endif
}
if ((((E[6]||E[3])||E[7]))) {
__veljRx_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A18\n");
#endif
}
if (E[9]) {
__veljRx_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A19\n");
#endif
}
if (E[5]) {
__veljRx_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__veljRx_A20\n");
#endif
}
E[18] = ((E[12]||E[15])||E[18]);
E[15] = ((E[1]||E[6])||E[7]);
E[12] = E[22];
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__veljRx_A5)));
E[0] = (__veljRx_R[2]&&E[0]);
E[23] = (E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__veljRx_A8)));
E[8] = (E[8]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__veljRx_A8)));
E[8] = (E[23]||((__veljRx_R[3]&&E[8])));
E[26] = (E[26]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__veljRx_A7)));
E[28] = (E[28]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__veljRx_A7)));
E[28] = (E[26]||((__veljRx_R[1]&&E[28])));
E[21] = (((E[29]||(((E[0]||(((((((((((E[10]||E[13])||E[16])||E[19])||E[2]))&&E[11])&&E[14])&&E[17])&&E[20])&&E[21])))||E[8])))||E[28]));
E[4] = ((((__veljRx_R[2]||E[4])||__veljRx_R[3]))||__veljRx_R[1]);
__veljRx_R[2] = (E[29]||((E[0]&&!(E[22]))));
__veljRx_R[3] = (E[8]&&!(E[22]));
E[24] = ((((((((E[22]||E[24]))||E[24])||E[22]))||E[3])||E[24])||E[22]);
__veljRx_R[4] = (E[10]&&!(E[24]));
__veljRx_R[5] = (E[13]&&!(E[24]));
__veljRx_R[6] = (E[16]&&!(E[24]));
__veljRx_R[7] = (E[19]&&!(E[24]));
__veljRx_R[8] = (E[2]&&!(E[24]));
__veljRx_R[0] = !(_true);
__veljRx_R[1] = E[28];
__veljRx__reset_input();
return E[21];
}

/* AUTOMATON RESET */

int veljRx_reset () {
__veljRx_R[0] = _true;
__veljRx_R[1] = _false;
__veljRx_R[2] = _false;
__veljRx_R[3] = _false;
__veljRx_R[4] = _false;
__veljRx_R[5] = _false;
__veljRx_R[6] = _false;
__veljRx_R[7] = _false;
__veljRx_R[8] = _false;
__veljRx__reset_input();
return 0;
}
