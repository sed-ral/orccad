/* sscc : C CODE OF SORTED EQUATIONS aa - SIMULATION MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define CSIMUL_H_LOADED
typedef struct {char text[STRLEN];} symbolic;
extern void _boolean(boolean*, boolean);
extern boolean _eq_boolean(boolean, boolean);
extern boolean _ne_boolean(boolean, boolean);
extern boolean _cond_boolean(boolean ,boolean ,boolean);
extern char* _boolean_to_text(boolean);
extern int _check_boolean(char*);
extern void _text_to_boolean(boolean*, char*);
extern void _integer(integer*, integer);
extern boolean _eq_integer(integer, integer);
extern boolean _ne_integer(integer, integer);
extern integer _cond_integer(boolean ,integer ,integer);
extern char* _integer_to_text(integer);
extern int _check_integer(char*);
extern void _text_to_integer(integer*, char*);
extern void _string(string, string);
extern boolean _eq_string(string, string);
extern boolean _ne_string(string, string);
extern string _cond_string(boolean ,string ,string);
extern char* _string_to_text(string);
extern int _check_string(char*);
extern void _text_to_string(string, char*);
extern void _float(float*, float);
extern boolean _eq_float(float, float);
extern boolean _ne_float(float, float);
extern float _cond_float(boolean ,float ,float);
extern char* _float_to_text(float);
extern int _check_float(char*);
extern void _text_to_float(float*, char*);
extern void _double(double*, double);
extern boolean _eq_double(double, double);
extern boolean _ne_double(double, double);
extern double _cond_double(boolean ,double ,double);
extern char* _double_to_text(double);
extern int _check_double(char*);
extern void _text_to_double(double*, char*);
extern void _symbolic(symbolic*, symbolic);
extern boolean _eq_symbolic(symbolic, symbolic);
extern boolean _ne_symbolic(symbolic, symbolic);
extern symbolic _cond_symbolic(boolean ,symbolic ,symbolic);
extern char* _symbolic_to_text(symbolic);
extern int _check_symbolic(char*);
extern void _text_to_symbolic(symbolic*, char*);
extern char* __PredefinedTypeToText(int, char*);
#define _true 1
#define _false 0
#define __aa_GENERIC_TEST(TEST) return TEST;
typedef void (*__aa_APF)();
static __aa_APF *__aa_PActionArray;
static int **__aa_PCheckArray;
struct __SourcePoint {
int linkback;
int line;
int column;
int instance_index;
};
struct __InstanceEntry {
char *module_name;
int father_index;
char *dir_name;
char *file_name;
struct __SourcePoint source_point;
struct __SourcePoint end_point;
struct __SourcePoint instance_point;
};
struct __TaskEntry {
char *name;
int   nb_args_ref;
int   nb_args_val;
int  *type_codes_array;
struct __SourcePoint source_point;
};
struct __SignalEntry {
char *name;
int code;
int variable_index;
int present;
struct __SourcePoint source_point;
int number_of_emit_source_points;
struct __SourcePoint* emit_source_point_array;
int number_of_present_source_points;
struct __SourcePoint* present_source_point_array;
int number_of_access_source_points;
struct __SourcePoint* access_source_point_array;
};
struct __InputEntry {
char *name;
int hash;
char *type_name;
int is_a_sensor;
int type_code;
int multiple;
int signal_index;
int (*p_check_input)(char*);
void (*p_input_function)();
int present;
struct __SourcePoint source_point;
};
struct __ReturnEntry {
char *name;
int hash;
char *type_name;
int type_code;
int signal_index;
int exec_index;
int (*p_check_input)(char*);
void (*p_input_function)();
int present;
struct __SourcePoint source_point;
};
struct __ImplicationEntry {
int master;
int slave;
struct __SourcePoint source_point;
};
struct __ExclusionEntry {
int *exclusion_list;
struct __SourcePoint source_point;
};
struct __VariableEntry {
char *full_name;
char *short_name;
char *type_name;
int type_code;
int comment_kind;
int is_initialized;
char *p_variable;
char *source_name;
int written;
unsigned char written_in_transition;
unsigned char read_in_transition;
struct __SourcePoint source_point;
};
struct __ExecEntry {
int task_index;
int *var_indexes_array;
char **p_values_array;
struct __SourcePoint source_point;
};
struct __HaltEntry {
struct __SourcePoint source_point;
};
struct __NetEntry {
int known;
int value;
int number_of_source_points;
struct __SourcePoint* source_point_array;
};
struct __ModuleEntry {
char *version_id;
char *name;
int number_of_instances;
int number_of_tasks;
int number_of_signals;
int number_of_inputs;
int number_of_returns;
int number_of_sensors;
int number_of_outputs;
int number_of_locals;
int number_of_exceptions;
int number_of_implications;
int number_of_exclusions;
int number_of_variables;
int number_of_execs;
int number_of_halts;
int number_of_nets;
int number_of_states;
int state;
unsigned short *halt_list;
unsigned short *awaited_list;
unsigned short *emitted_list;
unsigned short *started_list;
unsigned short *killed_list;
unsigned short *suspended_list;
unsigned short *active_list;
int run_time_error_code;
int error_info;
void (*init)();
int (*run)();
int (*reset)();
char *(*show_variable)(int);
void (*set_variable)(int, char*, char*);
int (*check_value)(int, char*);
int (*execute_action)();
struct __InstanceEntry* instance_table;
struct __TaskEntry* task_table;
struct __SignalEntry* signal_table;
struct __InputEntry* input_table;
struct __ReturnEntry* return_table;
struct __ImplicationEntry* implication_table;
struct __ExclusionEntry* exclusion_table;
struct __VariableEntry* variable_table;
struct __ExecEntry* exec_table;
struct __HaltEntry* halt_table;
struct __NetEntry* net_table;
};

#include "aa.h"

/* EXTERN DECLARATIONS */

#ifndef _orcStrlPtr_to_text
extern char* _orcStrlPtr_to_text(orcStrlPtr);
#endif
#ifndef _check_orcStrlPtr
extern int _check_orcStrlPtr(char*);
#endif
#ifndef _text_to_orcStrlPtr
extern void _text_to_orcStrlPtr(orcStrlPtr*, char*);
#endif
extern int __CheckVariables(int*);
extern void __ResetInput();
extern void __ResetExecs();
extern void __ResetVariables();
extern void __ResetVariableStatus();
extern void __AppendToList(unsigned short*, unsigned short);
extern void __ListCopy(unsigned short*, unsigned short**);
extern void __WriteVariable(int);
extern void __ResetVariable(int);
extern void __ResetModuleEntry();
extern void __ResetModuleEntryBeforeReaction();
extern void __ResetModuleEntryAfterReaction();
#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _movejRx_controler_DEFINED
#ifndef movejRx_controler
extern void movejRx_controler(orcStrlPtr);
#endif
#endif
#ifndef _movejRx_fileparameter_DEFINED
#ifndef movejRx_fileparameter
extern void movejRx_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _movejRx_parameter_DEFINED
#ifndef movejRx_parameter
extern void movejRx_parameter(orcStrlPtr ,string ,string);
#endif
#endif
#ifndef _veljRx_controler_DEFINED
#ifndef veljRx_controler
extern void veljRx_controler(orcStrlPtr);
#endif
#endif
#ifndef _veljRx_fileparameter_DEFINED
#ifndef veljRx_fileparameter
extern void veljRx_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _veljRx_parameter_DEFINED
#ifndef veljRx_parameter
extern void veljRx_parameter(orcStrlPtr ,string ,string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static orcStrlPtr __aa_V0;
static boolean __aa_V1;
static boolean __aa_V2;
static boolean __aa_V3;
static boolean __aa_V4;
static boolean __aa_V5;
static boolean __aa_V6;
static boolean __aa_V7;
static boolean __aa_V8;
static orcStrlPtr __aa_V9;
static boolean __aa_V10;
static boolean __aa_V11;
static boolean __aa_V12;
static boolean __aa_V13;
static boolean __aa_V14;
static orcStrlPtr __aa_V15;
static boolean __aa_V16;
static boolean __aa_V17;
static boolean __aa_V18;
static boolean __aa_V19;
static boolean __aa_V20;
static boolean __aa_V21;
static boolean __aa_V22;
static boolean __aa_V23;
static boolean __aa_V24;
static boolean __aa_V25;
static boolean __aa_V26;
static orcStrlPtr __aa_V27;
static orcStrlPtr __aa_V28;
static orcStrlPtr __aa_V29;
static orcStrlPtr __aa_V30;
static orcStrlPtr __aa_V31;
static orcStrlPtr __aa_V32;
static orcStrlPtr __aa_V33;
static orcStrlPtr __aa_V34;
static orcStrlPtr __aa_V35;
static orcStrlPtr __aa_V36;
static orcStrlPtr __aa_V37;
static boolean __aa_V38;

static unsigned short __aa_HaltList[31];
static unsigned short __aa_AwaitedList[79];
static unsigned short __aa_EmittedList[79];
static unsigned short __aa_StartedList[1];
static unsigned short __aa_KilledList[1];
static unsigned short __aa_SuspendedList[1];
static unsigned short __aa_ActiveList[1];
static unsigned short __aa_AllAwaitedList[79]={24,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};


/* INPUT FUNCTIONS */

void aa_I_Prr_Start (orcStrlPtr __V) {
__WriteVariable(0);
_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
void aa_IS_Prr_Start (string __V) {
__WriteVariable(0);
_text_to_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
void aa_I_jointlimit () {
__aa_V2 = _true;
}
void aa_IS_jointlimit () {
__aa_V2 = _true;
}
void aa_I_endtraj () {
__aa_V3 = _true;
}
void aa_IS_endtraj () {
__aa_V3 = _true;
}
void aa_I_endtime () {
__aa_V4 = _true;
}
void aa_IS_endtime () {
__aa_V4 = _true;
}
void aa_I_InitOK_Rx90 () {
__aa_V5 = _true;
}
void aa_IS_InitOK_Rx90 () {
__aa_V5 = _true;
}
void aa_I_USERSTART_veljRx () {
__aa_V6 = _true;
}
void aa_IS_USERSTART_veljRx () {
__aa_V6 = _true;
}
void aa_I_T2_veljRx () {
__aa_V7 = _true;
}
void aa_IS_T2_veljRx () {
__aa_V7 = _true;
}
void aa_I_CmdStopOK_Rx90 () {
__aa_V8 = _true;
}
void aa_IS_CmdStopOK_Rx90 () {
__aa_V8 = _true;
}
void aa_I_veljRx_Start (orcStrlPtr __V) {
__WriteVariable(9);
_orcStrlPtr(&__aa_V9,__V);
__aa_V10 = _true;
}
void aa_IS_veljRx_Start (string __V) {
__WriteVariable(9);
_text_to_orcStrlPtr(&__aa_V9,__V);
__aa_V10 = _true;
}
void aa_I_movejRxTimeoutgoodtraj () {
__aa_V11 = _true;
}
void aa_IS_movejRxTimeoutgoodtraj () {
__aa_V11 = _true;
}
void aa_I_goodtraj () {
__aa_V12 = _true;
}
void aa_IS_goodtraj () {
__aa_V12 = _true;
}
void aa_I_T2_movejRx () {
__aa_V13 = _true;
}
void aa_IS_T2_movejRx () {
__aa_V13 = _true;
}
void aa_I_ReadyToStop_Rx90 () {
__aa_V14 = _true;
}
void aa_IS_ReadyToStop_Rx90 () {
__aa_V14 = _true;
}
void aa_I_movejRx_Start (orcStrlPtr __V) {
__WriteVariable(15);
_orcStrlPtr(&__aa_V15,__V);
__aa_V16 = _true;
}
void aa_IS_movejRx_Start (string __V) {
__WriteVariable(15);
_text_to_orcStrlPtr(&__aa_V15,__V);
__aa_V16 = _true;
}
void aa_I_trackerr () {
__aa_V17 = _true;
}
void aa_IS_trackerr () {
__aa_V17 = _true;
}
void aa_I_KillOK_Rx90 () {
__aa_V18 = _true;
}
void aa_IS_KillOK_Rx90 () {
__aa_V18 = _true;
}
void aa_I_ActivateOK_Rx90 () {
__aa_V19 = _true;
}
void aa_IS_ActivateOK_Rx90 () {
__aa_V19 = _true;
}
void aa_I_endPrR () {
__aa_V20 = _true;
}
void aa_IS_endPrR () {
__aa_V20 = _true;
}
void aa_I_endTR () {
__aa_V21 = _true;
}
void aa_IS_endTR () {
__aa_V21 = _true;
}
void aa_I_USERSTART_movejRx () {
__aa_V22 = _true;
}
void aa_IS_USERSTART_movejRx () {
__aa_V22 = _true;
}
void aa_I_ROBOT_FAIL () {
__aa_V23 = _true;
}
void aa_IS_ROBOT_FAIL () {
__aa_V23 = _true;
}
void aa_I_SOFT_FAIL () {
__aa_V24 = _true;
}
void aa_IS_SOFT_FAIL () {
__aa_V24 = _true;
}
void aa_I_SENSOR_FAIL () {
__aa_V25 = _true;
}
void aa_IS_SENSOR_FAIL () {
__aa_V25 = _true;
}
void aa_I_CPU_OVERLOAD () {
__aa_V26 = _true;
}
void aa_IS_CPU_OVERLOAD () {
__aa_V26 = _true;
}

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int aa_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

static int __aa_A1 () {
__aa_GENERIC_TEST(__aa_V1);
}
static int __aa_Check1 [] = {1,0,0};
static int __aa_A2 () {
__aa_GENERIC_TEST(__aa_V2);
}
static int __aa_Check2 [] = {1,0,0};
static int __aa_A3 () {
__aa_GENERIC_TEST(__aa_V3);
}
static int __aa_Check3 [] = {1,0,0};
static int __aa_A4 () {
__aa_GENERIC_TEST(__aa_V4);
}
static int __aa_Check4 [] = {1,0,0};
static int __aa_A5 () {
__aa_GENERIC_TEST(__aa_V5);
}
static int __aa_Check5 [] = {1,0,0};
static int __aa_A6 () {
__aa_GENERIC_TEST(__aa_V6);
}
static int __aa_Check6 [] = {1,0,0};
static int __aa_A7 () {
__aa_GENERIC_TEST(__aa_V7);
}
static int __aa_Check7 [] = {1,0,0};
static int __aa_A8 () {
__aa_GENERIC_TEST(__aa_V8);
}
static int __aa_Check8 [] = {1,0,0};
static int __aa_A9 () {
__aa_GENERIC_TEST(__aa_V10);
}
static int __aa_Check9 [] = {1,0,0};
static int __aa_A10 () {
__aa_GENERIC_TEST(__aa_V11);
}
static int __aa_Check10 [] = {1,0,0};
static int __aa_A11 () {
__aa_GENERIC_TEST(__aa_V12);
}
static int __aa_Check11 [] = {1,0,0};
static int __aa_A12 () {
__aa_GENERIC_TEST(__aa_V13);
}
static int __aa_Check12 [] = {1,0,0};
static int __aa_A13 () {
__aa_GENERIC_TEST(__aa_V14);
}
static int __aa_Check13 [] = {1,0,0};
static int __aa_A14 () {
__aa_GENERIC_TEST(__aa_V16);
}
static int __aa_Check14 [] = {1,0,0};
static int __aa_A15 () {
__aa_GENERIC_TEST(__aa_V17);
}
static int __aa_Check15 [] = {1,0,0};
static int __aa_A16 () {
__aa_GENERIC_TEST(__aa_V18);
}
static int __aa_Check16 [] = {1,0,0};
static int __aa_A17 () {
__aa_GENERIC_TEST(__aa_V19);
}
static int __aa_Check17 [] = {1,0,0};
static int __aa_A18 () {
__aa_GENERIC_TEST(__aa_V20);
}
static int __aa_Check18 [] = {1,0,0};
static int __aa_A19 () {
__aa_GENERIC_TEST(__aa_V21);
}
static int __aa_Check19 [] = {1,0,0};
static int __aa_A20 () {
__aa_GENERIC_TEST(__aa_V22);
}
static int __aa_Check20 [] = {1,0,0};
static int __aa_A21 () {
__aa_GENERIC_TEST(__aa_V23);
}
static int __aa_Check21 [] = {1,0,0};
static int __aa_A22 () {
__aa_GENERIC_TEST(__aa_V24);
}
static int __aa_Check22 [] = {1,0,0};
static int __aa_A23 () {
__aa_GENERIC_TEST(__aa_V25);
}
static int __aa_Check23 [] = {1,0,0};
static int __aa_A24 () {
__aa_GENERIC_TEST(__aa_V26);
}
static int __aa_Check24 [] = {1,0,0};

/* OUTPUT ACTIONS */

static void __aa_A25 () {
#ifdef __OUTPUT
aa_O_Activate(__aa_V27);
#endif
__AppendToList(__aa_EmittedList,24);
}
static int __aa_Check25 [] = {1,0,0};
static void __aa_A26 () {
#ifdef __OUTPUT
aa_O_ActivateveljRx_Rx90(__aa_V28);
#endif
__AppendToList(__aa_EmittedList,25);
}
static int __aa_Check26 [] = {1,0,0};
static void __aa_A27 () {
#ifdef __OUTPUT
aa_O_veljRxTransite(__aa_V29);
#endif
__AppendToList(__aa_EmittedList,26);
}
static int __aa_Check27 [] = {1,0,0};
static void __aa_A28 () {
#ifdef __OUTPUT
aa_O_Abort_veljRx(__aa_V30);
#endif
__AppendToList(__aa_EmittedList,27);
}
static int __aa_Check28 [] = {1,0,0};
static void __aa_A29 () {
#ifdef __OUTPUT
aa_O_STARTED_veljRx();
#endif
__AppendToList(__aa_EmittedList,28);
}
static int __aa_Check29 [] = {1,0,0};
static void __aa_A30 () {
#ifdef __OUTPUT
aa_O_GoodEnd_veljRx();
#endif
__AppendToList(__aa_EmittedList,29);
}
static int __aa_Check30 [] = {1,0,0};
static void __aa_A31 () {
#ifdef __OUTPUT
aa_O_GoodEnd_manageRx90();
#endif
__AppendToList(__aa_EmittedList,30);
}
static int __aa_Check31 [] = {1,0,0};
static void __aa_A32 () {
#ifdef __OUTPUT
aa_O_Abort_manageRx90();
#endif
__AppendToList(__aa_EmittedList,31);
}
static int __aa_Check32 [] = {1,0,0};
static void __aa_A33 () {
#ifdef __OUTPUT
aa_O_STARTED_manageRx90();
#endif
__AppendToList(__aa_EmittedList,32);
}
static int __aa_Check33 [] = {1,0,0};
static void __aa_A34 () {
#ifdef __OUTPUT
aa_O_ActivatemovejRx_Rx90(__aa_V31);
#endif
__AppendToList(__aa_EmittedList,33);
}
static int __aa_Check34 [] = {1,0,0};
static void __aa_A35 () {
#ifdef __OUTPUT
aa_O_movejRxTransite(__aa_V32);
#endif
__AppendToList(__aa_EmittedList,34);
}
static int __aa_Check35 [] = {1,0,0};
static void __aa_A36 () {
#ifdef __OUTPUT
aa_O_Abort_movejRx(__aa_V33);
#endif
__AppendToList(__aa_EmittedList,35);
}
static int __aa_Check36 [] = {1,0,0};
static void __aa_A37 () {
#ifdef __OUTPUT
aa_O_STARTED_movejRx();
#endif
__AppendToList(__aa_EmittedList,36);
}
static int __aa_Check37 [] = {1,0,0};
static void __aa_A38 () {
#ifdef __OUTPUT
aa_O_GoodEnd_movejRx();
#endif
__AppendToList(__aa_EmittedList,37);
}
static int __aa_Check38 [] = {1,0,0};
static void __aa_A39 () {
#ifdef __OUTPUT
aa_O_EndKill(__aa_V34);
#endif
__AppendToList(__aa_EmittedList,38);
}
static int __aa_Check39 [] = {1,0,0};
static void __aa_A40 () {
#ifdef __OUTPUT
aa_O_FinBF(__aa_V35);
#endif
__AppendToList(__aa_EmittedList,39);
}
static int __aa_Check40 [] = {1,0,0};
static void __aa_A41 () {
#ifdef __OUTPUT
aa_O_FinT3(__aa_V36);
#endif
__AppendToList(__aa_EmittedList,40);
}
static int __aa_Check41 [] = {1,0,0};
static void __aa_A42 () {
#ifdef __OUTPUT
aa_O_GoodEndRPr();
#endif
__AppendToList(__aa_EmittedList,41);
}
static int __aa_Check42 [] = {1,0,0};
static void __aa_A43 () {
#ifdef __OUTPUT
aa_O_T3RPr();
#endif
__AppendToList(__aa_EmittedList,42);
}
static int __aa_Check43 [] = {1,0,0};

/* ASSIGNMENTS */

static void __aa_A44 () {
__aa_V1 = _false;
}
static int __aa_Check44 [] = {1,0,1,1};
static void __aa_A45 () {
__aa_V2 = _false;
}
static int __aa_Check45 [] = {1,0,1,2};
static void __aa_A46 () {
__aa_V3 = _false;
}
static int __aa_Check46 [] = {1,0,1,3};
static void __aa_A47 () {
__aa_V4 = _false;
}
static int __aa_Check47 [] = {1,0,1,4};
static void __aa_A48 () {
__aa_V5 = _false;
}
static int __aa_Check48 [] = {1,0,1,5};
static void __aa_A49 () {
__aa_V6 = _false;
}
static int __aa_Check49 [] = {1,0,1,6};
static void __aa_A50 () {
__aa_V7 = _false;
}
static int __aa_Check50 [] = {1,0,1,7};
static void __aa_A51 () {
__aa_V8 = _false;
}
static int __aa_Check51 [] = {1,0,1,8};
static void __aa_A52 () {
__aa_V10 = _false;
}
static int __aa_Check52 [] = {1,0,1,10};
static void __aa_A53 () {
__aa_V11 = _false;
}
static int __aa_Check53 [] = {1,0,1,11};
static void __aa_A54 () {
__aa_V12 = _false;
}
static int __aa_Check54 [] = {1,0,1,12};
static void __aa_A55 () {
__aa_V13 = _false;
}
static int __aa_Check55 [] = {1,0,1,13};
static void __aa_A56 () {
__aa_V14 = _false;
}
static int __aa_Check56 [] = {1,0,1,14};
static void __aa_A57 () {
__aa_V16 = _false;
}
static int __aa_Check57 [] = {1,0,1,16};
static void __aa_A58 () {
__aa_V17 = _false;
}
static int __aa_Check58 [] = {1,0,1,17};
static void __aa_A59 () {
__aa_V18 = _false;
}
static int __aa_Check59 [] = {1,0,1,18};
static void __aa_A60 () {
__aa_V19 = _false;
}
static int __aa_Check60 [] = {1,0,1,19};
static void __aa_A61 () {
__aa_V20 = _false;
}
static int __aa_Check61 [] = {1,0,1,20};
static void __aa_A62 () {
__aa_V21 = _false;
}
static int __aa_Check62 [] = {1,0,1,21};
static void __aa_A63 () {
__aa_V22 = _false;
}
static int __aa_Check63 [] = {1,0,1,22};
static void __aa_A64 () {
__aa_V23 = _false;
}
static int __aa_Check64 [] = {1,0,1,23};
static void __aa_A65 () {
__aa_V24 = _false;
}
static int __aa_Check65 [] = {1,0,1,24};
static void __aa_A66 () {
__aa_V25 = _false;
}
static int __aa_Check66 [] = {1,0,1,25};
static void __aa_A67 () {
__aa_V26 = _false;
}
static int __aa_Check67 [] = {1,0,1,26};
static void __aa_A68 () {
_orcStrlPtr(&__aa_V34,__aa_V37);
}
static int __aa_Check68 [] = {1,1,37,1,34};
static void __aa_A69 () {
_orcStrlPtr(&__aa_V35,__aa_V0);
}
static int __aa_Check69 [] = {1,1,0,1,35};
static void __aa_A70 () {
_orcStrlPtr(&__aa_V36,__aa_V0);
}
static int __aa_Check70 [] = {1,1,0,1,36};
static void __aa_A71 () {
_orcStrlPtr(&__aa_V33,__aa_V15);
}
static int __aa_Check71 [] = {1,1,15,1,33};
static void __aa_A72 () {
_orcStrlPtr(&__aa_V31,__aa_V15);
}
static int __aa_Check72 [] = {1,1,15,1,31};
static void __aa_A73 () {
_orcStrlPtr(&__aa_V37,__aa_V15);
}
static int __aa_Check73 [] = {1,1,15,1,37};
static void __aa_A74 () {
_orcStrlPtr(&__aa_V32,__aa_V15);
}
static int __aa_Check74 [] = {1,1,15,1,32};
static void __aa_A75 () {
_orcStrlPtr(&__aa_V32,__aa_V15);
}
static int __aa_Check75 [] = {1,1,15,1,32};
static void __aa_A76 () {
_orcStrlPtr(&__aa_V32,__aa_V15);
}
static int __aa_Check76 [] = {1,1,15,1,32};
static void __aa_A77 () {
_orcStrlPtr(&__aa_V32,__aa_V15);
}
static int __aa_Check77 [] = {1,1,15,1,32};
static void __aa_A78 () {
_orcStrlPtr(&__aa_V30,__aa_V9);
}
static int __aa_Check78 [] = {1,1,9,1,30};
static void __aa_A79 () {
_orcStrlPtr(&__aa_V28,__aa_V9);
}
static int __aa_Check79 [] = {1,1,9,1,28};
static void __aa_A80 () {
_orcStrlPtr(&__aa_V37,__aa_V9);
}
static int __aa_Check80 [] = {1,1,9,1,37};
static void __aa_A81 () {
_orcStrlPtr(&__aa_V29,__aa_V9);
}
static int __aa_Check81 [] = {1,1,9,1,29};
static void __aa_A82 () {
_orcStrlPtr(&__aa_V29,__aa_V9);
}
static int __aa_Check82 [] = {1,1,9,1,29};
static void __aa_A83 () {
_orcStrlPtr(&__aa_V29,__aa_V9);
}
static int __aa_Check83 [] = {1,1,9,1,29};
static void __aa_A84 () {
_orcStrlPtr(&__aa_V29,__aa_V9);
}
static int __aa_Check84 [] = {1,1,9,1,29};
static void __aa_A85 () {
_orcStrlPtr(&__aa_V34,__aa_V37);
}
static int __aa_Check85 [] = {1,1,37,1,34};

/* PROCEDURE CALLS */

static void __aa_A86 () {
movejRx_parameter(__aa_V15,"0.0 0.0 0.0 0.0 0.0 0.0 0.2 0.0 0.0 0.0 0.0 0.0","3");
}
static int __aa_Check86 [] = {1,1,15,0};
static void __aa_A87 () {
movejRx_controler(__aa_V15);
}
static int __aa_Check87 [] = {1,1,15,0};
static void __aa_A88 () {
veljRx_parameter(__aa_V9,"0.0 0.0 .0 0.0 0.0 0.0","0");
}
static int __aa_Check88 [] = {1,1,9,0};
static void __aa_A89 () {
veljRx_controler(__aa_V9);
}
static int __aa_Check89 [] = {1,1,9,0};

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

static void __aa_A90 () {
;
__ResetVariable(0);
}
static int __aa_Check90 [] = {1,0,0};
static void __aa_A91 () {
;
__ResetVariable(9);
}
static int __aa_Check91 [] = {1,0,0};
static void __aa_A92 () {
;
__ResetVariable(15);
}
static int __aa_Check92 [] = {1,0,0};
static void __aa_A93 () {
;
__ResetVariable(27);
}
static int __aa_Check93 [] = {1,0,0};
static void __aa_A94 () {
;
__ResetVariable(28);
}
static int __aa_Check94 [] = {1,0,0};
static void __aa_A95 () {
;
__ResetVariable(29);
}
static int __aa_Check95 [] = {1,0,0};
static void __aa_A96 () {
;
__ResetVariable(30);
}
static int __aa_Check96 [] = {1,0,0};
static void __aa_A97 () {
;
__ResetVariable(31);
}
static int __aa_Check97 [] = {1,0,0};
static void __aa_A98 () {
;
__ResetVariable(32);
}
static int __aa_Check98 [] = {1,0,0};
static void __aa_A99 () {
;
__ResetVariable(33);
}
static int __aa_Check99 [] = {1,0,0};
static void __aa_A100 () {
;
__ResetVariable(34);
}
static int __aa_Check100 [] = {1,0,0};
static void __aa_A101 () {
;
__ResetVariable(35);
}
static int __aa_Check101 [] = {1,0,0};
static void __aa_A102 () {
;
__ResetVariable(36);
}
static int __aa_Check102 [] = {1,0,0};
static void __aa_A103 () {
;
__ResetVariable(37);
}
static int __aa_Check103 [] = {1,0,0};
static void __aa_A104 () {
;
__ResetVariable(38);
}
static int __aa_Check104 [] = {1,0,0};

/* ACTION SEQUENCES */


static int *__aa_CheckArray[] = {
0,
__aa_Check1,
__aa_Check2,
__aa_Check3,
__aa_Check4,
__aa_Check5,
__aa_Check6,
__aa_Check7,
__aa_Check8,
__aa_Check9,
__aa_Check10,
__aa_Check11,
__aa_Check12,
__aa_Check13,
__aa_Check14,
__aa_Check15,
__aa_Check16,
__aa_Check17,
__aa_Check18,
__aa_Check19,
__aa_Check20,
__aa_Check21,
__aa_Check22,
__aa_Check23,
__aa_Check24,
__aa_Check25,
__aa_Check26,
__aa_Check27,
__aa_Check28,
__aa_Check29,
__aa_Check30,
__aa_Check31,
__aa_Check32,
__aa_Check33,
__aa_Check34,
__aa_Check35,
__aa_Check36,
__aa_Check37,
__aa_Check38,
__aa_Check39,
__aa_Check40,
__aa_Check41,
__aa_Check42,
__aa_Check43,
__aa_Check44,
__aa_Check45,
__aa_Check46,
__aa_Check47,
__aa_Check48,
__aa_Check49,
__aa_Check50,
__aa_Check51,
__aa_Check52,
__aa_Check53,
__aa_Check54,
__aa_Check55,
__aa_Check56,
__aa_Check57,
__aa_Check58,
__aa_Check59,
__aa_Check60,
__aa_Check61,
__aa_Check62,
__aa_Check63,
__aa_Check64,
__aa_Check65,
__aa_Check66,
__aa_Check67,
__aa_Check68,
__aa_Check69,
__aa_Check70,
__aa_Check71,
__aa_Check72,
__aa_Check73,
__aa_Check74,
__aa_Check75,
__aa_Check76,
__aa_Check77,
__aa_Check78,
__aa_Check79,
__aa_Check80,
__aa_Check81,
__aa_Check82,
__aa_Check83,
__aa_Check84,
__aa_Check85,
__aa_Check86,
__aa_Check87,
__aa_Check88,
__aa_Check89,
__aa_Check90,
__aa_Check91,
__aa_Check92,
__aa_Check93,
__aa_Check94,
__aa_Check95,
__aa_Check96,
__aa_Check97,
__aa_Check98,
__aa_Check99,
__aa_Check100,
__aa_Check101,
__aa_Check102,
__aa_Check103,
__aa_Check104
};
static int **__aa_PCheckArray =  __aa_CheckArray;

/* INIT FUNCTION */

#ifndef NO_INIT
void aa_initialize () {
}
#endif

/* SHOW VARIABLE FUNCTION */

char* __aa_show_variable (int __V) {
extern struct __VariableEntry __aa_VariableTable[];
struct __VariableEntry* p_var = &__aa_VariableTable[__V];
if (p_var->type_code < 0) {return __PredefinedTypeToText(p_var->type_code, p_var->p_variable);
} else {
switch (p_var->type_code) {
case 0: return _orcStrlPtr_to_text(*(orcStrlPtr*)p_var->p_variable);
default: return 0;
}
}
}

/* SET VARIABLE FUNCTION */

static void __aa_set_variable(int __Type, char* __pVar, char* __Text) {
}

/* CHECK VALUE FUNCTION */

static int __aa_check_value (int __Type, char* __Text) {
return 0;
}

/* SIMULATION TABLES */

struct __InstanceEntry __aa_InstanceTable [] = {
{"aa",0,"/local/projets/robotique/Rx90/orccad/Spec/RobotProcedures/manageRx90.PC/common/","aa.strl",{1,1,1,0},{1,128,1,0},{0,0,0,0}},
{"RTs_Init",0,"/local/projets/robotique/Rx90/orccad/Spec/RobotProcedures/manageRx90.PC/common/","aa.strl",{1,152,1,1},{1,169,1,1},{1,97,1,0}},
{"movejRx",0,"/local/projets/robotique/Rx90/orccad/Spec/RobotProcedures/manageRx90.PC/common/","movejRx.strl",{1,1,1,2},{1,114,1,2},{1,99,1,0}},
{"veljRx",0,"/local/projets/robotique/Rx90/orccad/Spec/RobotProcedures/manageRx90.PC/common/","veljRx.strl",{1,1,1,3},{1,104,1,3},{1,101,1,0}},
{"manageRx90",0,"/local/projets/robotique/Rx90/orccad/Spec/RobotProcedures/manageRx90.PC/common/","manageRx90.strl",{1,1,1,4},{1,116,1,4},{1,103,1,0}},
{"TransiteRobot",0,"/local/projets/robotique/Rx90/orccad/Spec/RobotProcedures/manageRx90.PC/common/","aa.strl",{1,131,1,5},{1,149,1,5},{1,105,1,0}},
};

struct __SignalEntry __aa_SignalTable [] = {
{"Prr_Start",1,0,0,{1,4,7,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"jointlimit",33,0,0,{1,5,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"endtraj",33,0,0,{1,6,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"endtime",33,0,0,{1,7,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"InitOK_Rx90",33,0,0,{1,8,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"USERSTART_veljRx",33,0,0,{1,9,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2_veljRx",33,0,0,{1,10,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"CmdStopOK_Rx90",33,0,0,{1,11,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"veljRx_Start",1,9,0,{1,12,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"movejRxTimeoutgoodtraj",33,0,0,{1,13,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"goodtraj",33,0,0,{1,14,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2_movejRx",33,0,0,{1,15,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ReadyToStop_Rx90",33,0,0,{1,16,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"movejRx_Start",1,15,0,{1,17,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"trackerr",33,0,0,{1,18,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"KillOK_Rx90",33,0,0,{1,19,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ActivateOK_Rx90",33,0,0,{1,20,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"endPrR",33,0,0,{1,21,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"endTR",33,0,0,{1,22,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"USERSTART_movejRx",33,0,0,{1,23,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ROBOT_FAIL",33,0,0,{1,24,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"SOFT_FAIL",33,0,0,{1,25,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"SENSOR_FAIL",33,0,0,{1,26,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"CPU_OVERLOAD",33,0,0,{1,27,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Activate",2,27,0,{1,29,8,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ActivateveljRx_Rx90",2,28,0,{1,29,33,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"veljRxTransite",2,29,0,{1,30,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_veljRx",2,30,0,{1,31,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"STARTED_veljRx",34,0,0,{1,32,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEnd_veljRx",34,0,0,{1,33,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEnd_manageRx90",34,0,0,{1,34,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_manageRx90",34,0,0,{1,35,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"STARTED_manageRx90",34,0,0,{1,36,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ActivatemovejRx_Rx90",2,31,0,{1,37,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"movejRxTransite",2,32,0,{1,38,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_movejRx",2,33,0,{1,39,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"STARTED_movejRx",34,0,0,{1,40,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEnd_movejRx",34,0,0,{1,41,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"EndKill",2,34,0,{1,42,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FinBF",2,35,0,{1,43,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FinT3",2,36,0,{1,44,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEndRPr",34,0,0,{1,45,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3RPr",34,0,0,{1,46,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_veljRx",40,0,0,{1,72,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_veljRx",40,0,0,{1,73,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_Local_veljRx",40,0,0,{1,74,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_movejRx",40,0,0,{1,75,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_movejRx",40,0,0,{1,76,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_manageRx90",40,0,0,{1,77,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_manageRx90",40,0,0,{1,78,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ReadyToStart_Rx90",40,0,0,{1,79,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_Local_manageRx90",40,0,0,{1,80,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"START_veljRx",40,0,0,{1,81,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"START_movejRx",40,0,0,{1,82,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Prev_rt_Rx90",8,37,0,{1,83,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"StartTransite_Rx90",40,0,0,{1,84,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FinTransite_Rx90",40,0,0,{1,85,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FirstRT_Rx90",40,0,0,{1,86,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Reparam_Rx90",8,38,0,{1,87,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_Local_movejRx",40,0,0,{1,88,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"START_manageRx90",40,0,0,{1,89,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2",40,0,0,{1,90,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_controle_prr",40,0,0,{1,91,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_controle_prr",40,0,0,{1,92,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_prr",48,0,0,{1,94,6,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_prr",48,0,0,{1,95,6,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3",48,0,0,{1,42,6,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort",48,0,0,{1,46,8,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2",48,0,0,{1,73,8,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF",48,0,0,{1,74,9,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3",48,0,0,{1,38,6,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort",48,0,0,{1,42,8,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2",48,0,0,{1,63,8,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF",48,0,0,{1,64,9,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"dummylocalsignal",40,0,0,{1,36,9,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort",48,0,0,{1,42,6,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_controle_manageRx90",48,0,0,{1,43,7,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_controle_manageRx90",48,0,0,{1,44,8,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL}};

struct __InputEntry __aa_InputTable [] = {
{"Prr_Start",20,"orcStrlPtr",0,0,0,0,_check_orcStrlPtr,aa_IS_Prr_Start,0,{1,4,7,0}},
{"jointlimit",81,0,0,-1,0,1,0,aa_IS_jointlimit,0,{1,5,4,0}},
{"endtraj",37,0,0,-1,0,2,0,aa_IS_endtraj,0,{1,6,4,0}},
{"endtime",35,0,0,-1,0,3,0,aa_IS_endtime,0,{1,7,4,0}},
{"InitOK_Rx90",51,0,0,-1,0,4,0,aa_IS_InitOK_Rx90,0,{1,8,4,0}},
{"USERSTART_veljRx",33,0,0,-1,0,5,0,aa_IS_USERSTART_veljRx,0,{1,9,4,0}},
{"T2_veljRx",56,0,0,-1,0,6,0,aa_IS_T2_veljRx,0,{1,10,4,0}},
{"CmdStopOK_Rx90",42,0,0,-1,0,7,0,aa_IS_CmdStopOK_Rx90,0,{1,11,4,0}},
{"veljRx_Start",44,"orcStrlPtr",0,0,0,8,_check_orcStrlPtr,aa_IS_veljRx_Start,0,{1,12,4,0}},
{"movejRxTimeoutgoodtraj",25,0,0,-1,0,9,0,aa_IS_movejRxTimeoutgoodtraj,0,{1,13,4,0}},
{"goodtraj",50,0,0,-1,0,10,0,aa_IS_goodtraj,0,{1,14,4,0}},
{"T2_movejRx",67,0,0,-1,0,11,0,aa_IS_T2_movejRx,0,{1,15,4,0}},
{"ReadyToStop_Rx90",5,0,0,-1,0,12,0,aa_IS_ReadyToStop_Rx90,0,{1,16,4,0}},
{"movejRx_Start",55,"orcStrlPtr",0,0,0,13,_check_orcStrlPtr,aa_IS_movejRx_Start,0,{1,17,4,0}},
{"trackerr",54,0,0,-1,0,14,0,aa_IS_trackerr,0,{1,18,4,0}},
{"KillOK_Rx90",43,0,0,-1,0,15,0,aa_IS_KillOK_Rx90,0,{1,19,4,0}},
{"ActivateOK_Rx90",60,0,0,-1,0,16,0,aa_IS_ActivateOK_Rx90,0,{1,20,4,0}},
{"endPrR",82,0,0,-1,0,17,0,aa_IS_endPrR,0,{1,21,4,0}},
{"endTR",73,0,0,-1,0,18,0,aa_IS_endTR,0,{1,22,4,0}},
{"USERSTART_movejRx",44,0,0,-1,0,19,0,aa_IS_USERSTART_movejRx,0,{1,23,4,0}},
{"ROBOT_FAIL",62,0,0,-1,0,20,0,aa_IS_ROBOT_FAIL,0,{1,24,4,0}},
{"SOFT_FAIL",89,0,0,-1,0,21,0,aa_IS_SOFT_FAIL,0,{1,25,4,0}},
{"SENSOR_FAIL",45,0,0,-1,0,22,0,aa_IS_SENSOR_FAIL,0,{1,26,4,0}},
{"CPU_OVERLOAD",22,0,0,-1,0,23,0,aa_IS_CPU_OVERLOAD,0,{1,27,4,0}}};

static int __aa_ExclusionEntry0[] = {21,1,2,3,4,6,7,9,8,10,13,11,12,14,15,16,17,18,20,21,22,23};

struct __ExclusionEntry __aa_ExclusionTable [] = {
{__aa_ExclusionEntry0,{1,49,4,0}}
};

struct __VariableEntry __aa_VariableTable [] = {
{"__aa_V0","V0","orcStrlPtr",0,1,0,(char*)&__aa_V0,"Prr_Start",0,0,0,{1,4,7,0}},
{"__aa_V1","V1","boolean",-2,2,0,(char*)&__aa_V1,"Prr_Start",0,0,0,{1,4,7,0}},
{"__aa_V2","V2","boolean",-2,2,0,(char*)&__aa_V2,"jointlimit",0,0,0,{1,5,4,0}},
{"__aa_V3","V3","boolean",-2,2,0,(char*)&__aa_V3,"endtraj",0,0,0,{1,6,4,0}},
{"__aa_V4","V4","boolean",-2,2,0,(char*)&__aa_V4,"endtime",0,0,0,{1,7,4,0}},
{"__aa_V5","V5","boolean",-2,2,0,(char*)&__aa_V5,"InitOK_Rx90",0,0,0,{1,8,4,0}},
{"__aa_V6","V6","boolean",-2,2,0,(char*)&__aa_V6,"USERSTART_veljRx",0,0,0,{1,9,4,0}},
{"__aa_V7","V7","boolean",-2,2,0,(char*)&__aa_V7,"T2_veljRx",0,0,0,{1,10,4,0}},
{"__aa_V8","V8","boolean",-2,2,0,(char*)&__aa_V8,"CmdStopOK_Rx90",0,0,0,{1,11,4,0}},
{"__aa_V9","V9","orcStrlPtr",0,1,0,(char*)&__aa_V9,"veljRx_Start",0,0,0,{1,12,4,0}},
{"__aa_V10","V10","boolean",-2,2,0,(char*)&__aa_V10,"veljRx_Start",0,0,0,{1,12,4,0}},
{"__aa_V11","V11","boolean",-2,2,0,(char*)&__aa_V11,"movejRxTimeoutgoodtraj",0,0,0,{1,13,4,0}},
{"__aa_V12","V12","boolean",-2,2,0,(char*)&__aa_V12,"goodtraj",0,0,0,{1,14,4,0}},
{"__aa_V13","V13","boolean",-2,2,0,(char*)&__aa_V13,"T2_movejRx",0,0,0,{1,15,4,0}},
{"__aa_V14","V14","boolean",-2,2,0,(char*)&__aa_V14,"ReadyToStop_Rx90",0,0,0,{1,16,4,0}},
{"__aa_V15","V15","orcStrlPtr",0,1,0,(char*)&__aa_V15,"movejRx_Start",0,0,0,{1,17,4,0}},
{"__aa_V16","V16","boolean",-2,2,0,(char*)&__aa_V16,"movejRx_Start",0,0,0,{1,17,4,0}},
{"__aa_V17","V17","boolean",-2,2,0,(char*)&__aa_V17,"trackerr",0,0,0,{1,18,4,0}},
{"__aa_V18","V18","boolean",-2,2,0,(char*)&__aa_V18,"KillOK_Rx90",0,0,0,{1,19,4,0}},
{"__aa_V19","V19","boolean",-2,2,0,(char*)&__aa_V19,"ActivateOK_Rx90",0,0,0,{1,20,4,0}},
{"__aa_V20","V20","boolean",-2,2,0,(char*)&__aa_V20,"endPrR",0,0,0,{1,21,4,0}},
{"__aa_V21","V21","boolean",-2,2,0,(char*)&__aa_V21,"endTR",0,0,0,{1,22,4,0}},
{"__aa_V22","V22","boolean",-2,2,0,(char*)&__aa_V22,"USERSTART_movejRx",0,0,0,{1,23,4,0}},
{"__aa_V23","V23","boolean",-2,2,0,(char*)&__aa_V23,"ROBOT_FAIL",0,0,0,{1,24,4,0}},
{"__aa_V24","V24","boolean",-2,2,0,(char*)&__aa_V24,"SOFT_FAIL",0,0,0,{1,25,4,0}},
{"__aa_V25","V25","boolean",-2,2,0,(char*)&__aa_V25,"SENSOR_FAIL",0,0,0,{1,26,4,0}},
{"__aa_V26","V26","boolean",-2,2,0,(char*)&__aa_V26,"CPU_OVERLOAD",0,0,0,{1,27,4,0}},
{"__aa_V27","V27","orcStrlPtr",0,1,0,(char*)&__aa_V27,"Activate",0,0,0,{1,29,8,0}},
{"__aa_V28","V28","orcStrlPtr",0,1,0,(char*)&__aa_V28,"ActivateveljRx_Rx90",0,0,0,{1,29,33,0}},
{"__aa_V29","V29","orcStrlPtr",0,1,0,(char*)&__aa_V29,"veljRxTransite",0,0,0,{1,30,4,0}},
{"__aa_V30","V30","orcStrlPtr",0,1,0,(char*)&__aa_V30,"Abort_veljRx",0,0,0,{1,31,4,0}},
{"__aa_V31","V31","orcStrlPtr",0,1,0,(char*)&__aa_V31,"ActivatemovejRx_Rx90",0,0,0,{1,37,4,0}},
{"__aa_V32","V32","orcStrlPtr",0,1,0,(char*)&__aa_V32,"movejRxTransite",0,0,0,{1,38,4,0}},
{"__aa_V33","V33","orcStrlPtr",0,1,0,(char*)&__aa_V33,"Abort_movejRx",0,0,0,{1,39,4,0}},
{"__aa_V34","V34","orcStrlPtr",0,1,0,(char*)&__aa_V34,"EndKill",0,0,0,{1,42,4,0}},
{"__aa_V35","V35","orcStrlPtr",0,1,0,(char*)&__aa_V35,"FinBF",0,0,0,{1,43,4,0}},
{"__aa_V36","V36","orcStrlPtr",0,1,0,(char*)&__aa_V36,"FinT3",0,0,0,{1,44,4,0}},
{"__aa_V37","V37","orcStrlPtr",0,1,0,(char*)&__aa_V37,"Prev_rt_Rx90",0,0,0,{1,83,4,0}},
{"__aa_V38","V38","boolean",-2,1,0,(char*)&__aa_V38,"Reparam_Rx90",0,0,0,{1,87,4,0}}
};

struct __HaltEntry __aa_HaltTable [] = {
{{1,128,1,0}},
{{1,112,1,0}},
{{1,160,2,1}},
{{1,162,2,1}},
{{1,164,2,1}},
{{1,43,9,2}},
{{1,50,5,2}},
{{1,55,4,2}},
{{1,64,4,2}},
{{1,77,5,2}},
{{1,83,4,2}},
{{1,85,4,2}},
{{1,87,4,2}},
{{1,91,4,2}},
{{1,39,9,3}},
{{1,45,4,3}},
{{1,54,4,3}},
{{1,67,5,3}},
{{1,73,4,3}},
{{1,75,4,3}},
{{1,77,4,3}},
{{1,81,4,3}},
{{1,39,1,4}},
{{1,59,5,4}},
{{1,73,5,4}},
{{1,99,6,4}},
{{1,138,3,5}},
{{1,140,6,5}},
{{1,142,8,5}},
{{1,145,6,5}}
};


static void __aa__reset_input () {
__aa_V1 = _false;
__aa_V2 = _false;
__aa_V3 = _false;
__aa_V4 = _false;
__aa_V5 = _false;
__aa_V6 = _false;
__aa_V7 = _false;
__aa_V8 = _false;
__aa_V10 = _false;
__aa_V11 = _false;
__aa_V12 = _false;
__aa_V13 = _false;
__aa_V14 = _false;
__aa_V16 = _false;
__aa_V17 = _false;
__aa_V18 = _false;
__aa_V19 = _false;
__aa_V20 = _false;
__aa_V21 = _false;
__aa_V22 = _false;
__aa_V23 = _false;
__aa_V24 = _false;
__aa_V25 = _false;
__aa_V26 = _false;
}


/* MODULE DATA FOR SIMULATION */

int aa();
int aa_reset();

static struct __ModuleEntry __aa_ModuleData = {
"Simulation interface release 5","aa",
6,0,78,24,0,0,19,22,13,0,1,39,0,30,0,0,0,
__aa_HaltList,
__aa_AwaitedList,
__aa_EmittedList,
__aa_StartedList,
__aa_KilledList,
__aa_SuspendedList,
__aa_ActiveList,
0,0,
aa_initialize,aa,aa_reset,
__aa_show_variable,__aa_set_variable,__aa_check_value,0,
__aa_InstanceTable,
0,
__aa_SignalTable,__aa_InputTable,0,
0,__aa_ExclusionTable,
__aa_VariableTable,
0,
__aa_HaltTable,
0};

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __aa_R[30] = {_true,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int aa () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[81];

__aa_ModuleData.awaited_list = __aa_AwaitedList;
__ResetModuleEntryBeforeReaction();
if (!(_true)) {
__CheckVariables(__aa_CheckArray[25]);__aa_A25();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A25\n");
#endif
}
E[0] = (__aa_R[15]&&((__aa_R[15]&&!(__aa_R[0]))));
E[1] = (E[0]&&(__CheckVariables(__aa_CheckArray[5]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5()));
E[2] = (__aa_R[26]&&!(__aa_R[0]));
E[3] = (__aa_R[6]||__aa_R[7]);
E[4] = (E[3]&&!(__aa_R[0]));
E[5] = (__aa_R[6]&&E[4]);
E[6] = (E[5]&&!((__CheckVariables(__aa_CheckArray[10]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10())));
E[6] = (__aa_R[6]&&E[6]);
E[7] = (E[6]&&(__CheckVariables(__aa_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11()));
E[4] = (__aa_R[7]&&E[4]);
E[8] = (E[4]&&(__CheckVariables(__aa_CheckArray[5]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5()));
E[9] = (((E[3]&&!(__aa_R[6])))||E[7]);
E[10] = (((E[3]&&!(__aa_R[7])))||E[8]);
E[8] = ((((E[7]||E[8]))&&E[9])&&E[10]);
E[7] = (E[8]||E[1]);
if (E[7]) {
__AppendToList(__aa_EmittedList,50);
}
E[11] = (E[2]&&E[7]);
E[12] = (__aa_R[29]&&!(__aa_R[0]));
E[13] = (E[12]&&(__CheckVariables(__aa_CheckArray[8]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8()));
E[14] = (E[11]||E[13]);
if (E[14]) {
__AppendToList(__aa_EmittedList,56);
}
E[15] = (__aa_R[16]&&((__aa_R[16]&&!(__aa_R[0]))));
E[16] = (((E[1]&&E[14]))||((E[15]&&E[14])));
if (E[16]) {
__AppendToList(__aa_EmittedList,25);
}
if (E[16]) {
__CheckVariables(__aa_CheckArray[79]);__aa_A79();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A79\n");
#endif
}
if (E[16]) {
__CheckVariables(__aa_CheckArray[26]);__aa_A26();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A26\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[82]);__aa_A82();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A82\n");
#endif
}
E[17] = (__aa_R[21]&&!(__aa_R[0]));
E[18] = (E[17]&&(__CheckVariables(__aa_CheckArray[19]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__aa_A19()));
if (E[18]) {
__AppendToList(__aa_EmittedList,73);
}
E[19] = ((((__aa_R[17]||__aa_R[18])||__aa_R[19])||__aa_R[20])||__aa_R[21]);
if (E[16]) {
__CheckVariables(__aa_CheckArray[80]);__aa_A80();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A80\n");
#endif
}
E[20] = (E[16]||((__aa_R[17]&&((__aa_R[17]&&!(__aa_R[0]))))));
E[21] = (((E[19]&&!(__aa_R[17])))||E[20]);
E[22] = (__aa_R[18]&&!(__aa_R[0]));
E[23] = (E[22]&&!((__CheckVariables(__aa_CheckArray[15]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15())));
E[23] = (E[16]||((__aa_R[18]&&E[23])));
E[24] = (((E[19]&&!(__aa_R[18])))||E[23]);
E[25] = (__aa_R[19]&&!(__aa_R[0]));
E[26] = (E[25]&&!((__CheckVariables(__aa_CheckArray[2]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2())));
E[26] = (E[16]||((__aa_R[19]&&E[26])));
E[27] = (((E[19]&&!(__aa_R[19])))||E[26]);
E[28] = (__aa_R[20]&&!(__aa_R[0]));
E[29] = (E[28]&&!((__CheckVariables(__aa_CheckArray[18]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__aa_A18())));
E[29] = (E[16]||((__aa_R[20]&&E[29])));
E[30] = (((E[19]&&!(__aa_R[20])))||E[29]);
E[17] = (E[17]&&!((__CheckVariables(__aa_CheckArray[19]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__aa_A19())));
E[17] = (E[16]||((__aa_R[21]&&E[17])));
E[31] = (((E[19]&&!(__aa_R[21])))||E[17]);
E[32] = (E[31]||E[18]);
E[18] = (((((E[18]&&E[21])&&E[24])&&E[27])&&E[30])&&E[32]);
if (E[18]) {
__AppendToList(__aa_EmittedList,26);
__AppendToList(__aa_EmittedList,29);
__AppendToList(__aa_EmittedList,43);
}
if (E[18]) {
__CheckVariables(__aa_CheckArray[83]);__aa_A83();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A83\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[81]);__aa_A81();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A81\n");
#endif
}
if (E[18]) {
__CheckVariables(__aa_CheckArray[27]);__aa_A27();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A27\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[78]);__aa_A78();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A78\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[28]);__aa_A28();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A28\n");
#endif
}
E[33] = (__aa_R[0]&&!((__CheckVariables(__aa_CheckArray[1]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1())));
if (E[33]) {
__CheckVariables(__aa_CheckArray[90]);__aa_A90();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A90\n");
#endif
}
E[34] = (__aa_R[0]&&!((__CheckVariables(__aa_CheckArray[9]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9())));
if (E[34]) {
__CheckVariables(__aa_CheckArray[91]);__aa_A91();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A91\n");
#endif
}
E[35] = (__aa_R[0]&&!((__CheckVariables(__aa_CheckArray[14]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14())));
if (E[35]) {
__CheckVariables(__aa_CheckArray[92]);__aa_A92();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A92\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[93]);__aa_A93();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A93\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[94]);__aa_A94();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A94\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[95]);__aa_A95();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A95\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[96]);__aa_A96();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A96\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[97]);__aa_A97();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A97\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[98]);__aa_A98();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A98\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[99]);__aa_A99();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A99\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[100]);__aa_A100();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A100\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[101]);__aa_A101();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A101\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[102]);__aa_A102();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A102\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[103]);__aa_A103();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A103\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[104]);__aa_A104();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A104\n");
#endif
}
E[36] = (E[18]||__aa_R[0]);
E[37] = (__aa_R[23]&&!(__aa_R[0]));
E[38] = (__aa_R[13]&&!(__aa_R[0]));
E[39] = (E[38]&&(__CheckVariables(__aa_CheckArray[3]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3()));
if (E[39]) {
__AppendToList(__aa_EmittedList,69);
}
E[40] = ((((__aa_R[9]||__aa_R[10])||__aa_R[11])||__aa_R[12])||__aa_R[13]);
E[41] = (__aa_R[8]&&((__aa_R[8]&&!(__aa_R[0]))));
E[42] = (((E[8]&&E[14]))||((E[41]&&E[14])));
if (E[42]) {
__AppendToList(__aa_EmittedList,33);
}
if (E[42]) {
__CheckVariables(__aa_CheckArray[72]);__aa_A72();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A72\n");
#endif
}
if (E[42]) {
__CheckVariables(__aa_CheckArray[73]);__aa_A73();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A73\n");
#endif
}
E[43] = (E[42]||((__aa_R[9]&&((__aa_R[9]&&!(__aa_R[0]))))));
E[44] = (((E[40]&&!(__aa_R[9])))||E[43]);
E[45] = (__aa_R[10]&&!(__aa_R[0]));
E[46] = (E[45]&&!((__CheckVariables(__aa_CheckArray[4]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4())));
E[46] = (E[42]||((__aa_R[10]&&E[46])));
E[47] = (((E[40]&&!(__aa_R[10])))||E[46]);
E[48] = (__aa_R[11]&&!(__aa_R[0]));
E[49] = (E[48]&&!((__CheckVariables(__aa_CheckArray[15]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15())));
E[49] = (E[42]||((__aa_R[11]&&E[49])));
E[50] = (((E[40]&&!(__aa_R[11])))||E[49]);
E[51] = (__aa_R[12]&&!(__aa_R[0]));
E[52] = (E[51]&&!((__CheckVariables(__aa_CheckArray[2]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2())));
E[52] = (E[42]||((__aa_R[12]&&E[52])));
E[53] = (((E[40]&&!(__aa_R[12])))||E[52]);
E[38] = (E[38]&&!((__CheckVariables(__aa_CheckArray[3]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3())));
E[38] = (E[42]||((__aa_R[13]&&E[38])));
E[54] = (((E[40]&&!(__aa_R[13])))||E[38]);
E[55] = (E[54]||E[39]);
E[39] = (((((E[39]&&E[44])&&E[47])&&E[50])&&E[53])&&E[55]);
if (E[39]) {
__AppendToList(__aa_EmittedList,34);
__AppendToList(__aa_EmittedList,37);
__AppendToList(__aa_EmittedList,46);
}
if (E[39]) {
__CheckVariables(__aa_CheckArray[76]);__aa_A76();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A76\n");
#endif
}
E[56] = (E[37]&&E[39]);
if (E[56]) {
__AppendToList(__aa_EmittedList,52);
}
if (E[56]) {
__CheckVariables(__aa_CheckArray[88]);__aa_A88();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A88\n");
#endif
}
if (E[56]) {
__CheckVariables(__aa_CheckArray[89]);__aa_A89();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A89\n");
#endif
}
E[57] = (__aa_R[14]&&!(__aa_R[0]));
E[58] = (((E[36]&&E[56]))||((E[57]&&E[56])));
if (E[58]) {
__AppendToList(__aa_EmittedList,28);
}
if (E[58]) {
__CheckVariables(__aa_CheckArray[29]);__aa_A29();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A29\n");
#endif
}
if (E[18]) {
__CheckVariables(__aa_CheckArray[30]);__aa_A30();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A30\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[31]);__aa_A31();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A31\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[32]);__aa_A32();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A32\n");
#endif
}
E[59] = (__aa_R[2]&&!(__aa_R[0]));
E[60] = (E[59]&&(__CheckVariables(__aa_CheckArray[14]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14()));
E[61] = (__aa_R[3]&&!(__aa_R[0]));
E[62] = (E[61]&&(__CheckVariables(__aa_CheckArray[9]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9()));
E[63] = (__aa_R[4]&&!(__aa_R[0]));
E[64] = (E[63]&&(__CheckVariables(__aa_CheckArray[1]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1()));
E[65] = ((__aa_R[2]||__aa_R[3])||__aa_R[4]);
E[66] = (((E[65]&&!(__aa_R[2])))||E[60]);
E[67] = (((E[65]&&!(__aa_R[3])))||E[62]);
E[68] = (((E[65]&&!(__aa_R[4])))||E[64]);
E[64] = ((((((E[60]||E[62])||E[64]))&&E[66])&&E[67])&&E[68]);
if (E[64]) {
__AppendToList(__aa_EmittedList,57);
__AppendToList(__aa_EmittedList,60);
}
E[62] = (__aa_R[22]&&!(__aa_R[0]));
E[60] = (((__aa_R[0]&&E[64]))||((E[62]&&E[64])));
if (E[60]) {
__AppendToList(__aa_EmittedList,32);
}
if (E[60]) {
__CheckVariables(__aa_CheckArray[33]);__aa_A33();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A33\n");
#endif
}
if (E[42]) {
__CheckVariables(__aa_CheckArray[34]);__aa_A34();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A34\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[75]);__aa_A75();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A75\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[74]);__aa_A74();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A74\n");
#endif
}
if (E[39]) {
__CheckVariables(__aa_CheckArray[35]);__aa_A35();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A35\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[71]);__aa_A71();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A71\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[36]);__aa_A36();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A36\n");
#endif
}
E[69] = (E[39]||__aa_R[0]);
E[70] = (__aa_R[24]&&!(__aa_R[0]));
E[71] = (((E[70]&&E[18]))||E[60]);
if (E[71]) {
__AppendToList(__aa_EmittedList,53);
}
if (E[71]) {
__CheckVariables(__aa_CheckArray[86]);__aa_A86();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A86\n");
#endif
}
if (E[71]) {
__CheckVariables(__aa_CheckArray[87]);__aa_A87();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A87\n");
#endif
}
E[72] = (__aa_R[5]&&!(__aa_R[0]));
E[73] = (((E[69]&&E[71]))||((E[72]&&E[71])));
if (E[73]) {
__AppendToList(__aa_EmittedList,36);
}
if (E[73]) {
__CheckVariables(__aa_CheckArray[37]);__aa_A37();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A37\n");
#endif
}
if (E[39]) {
__CheckVariables(__aa_CheckArray[38]);__aa_A38();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A38\n");
#endif
}
E[74] = (__aa_R[28]&&!(__aa_R[0]));
E[75] = (E[74]&&E[7]);
if (E[75]) {
__AppendToList(__aa_EmittedList,38);
}
if (E[75]) {
__CheckVariables(__aa_CheckArray[85]);__aa_A85();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A85\n");
#endif
}
E[76] = (__aa_R[1]&&!(__aa_R[0]));
E[3] = ((((E[40]||__aa_R[8])||E[3]))||__aa_R[5]);
E[19] = ((((__aa_R[15]||E[19])||__aa_R[16]))||__aa_R[14]);
E[40] = (__aa_R[23]||__aa_R[24]);
E[77] = (E[40]||__aa_R[25]);
E[78] = (E[77]||__aa_R[22]);
E[79] = (((__aa_R[26]||__aa_R[27])||__aa_R[28])||__aa_R[29]);
E[80] = (((((E[65]||E[3])||E[19])||E[78])||E[79])||__aa_R[1]);
E[59] = (E[59]&&!((__CheckVariables(__aa_CheckArray[14]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14())));
E[59] = (__aa_R[0]||((__aa_R[2]&&E[59])));
E[61] = (E[61]&&!((__CheckVariables(__aa_CheckArray[9]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9())));
E[61] = (__aa_R[0]||((__aa_R[3]&&E[61])));
E[63] = (E[63]&&!((__CheckVariables(__aa_CheckArray[1]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1())));
E[63] = (__aa_R[0]||((__aa_R[4]&&E[63])));
E[68] = ((((((E[59]||E[61])||E[63]))&&((E[66]||E[59])))&&((E[67]||E[61])))&&((E[68]||E[63])));
E[65] = (((((E[80]&&!(E[65])))||E[64]))||E[68]);
E[45] = (E[45]&&(__CheckVariables(__aa_CheckArray[4]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4()));
E[48] = (E[48]&&(__CheckVariables(__aa_CheckArray[15]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15()));
E[51] = (E[51]&&(__CheckVariables(__aa_CheckArray[2]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2()));
E[55] = ((((((((E[45]||E[48])||E[51]))&&E[44])&&((E[47]||E[45])))&&((E[50]||E[48])))&&((E[53]||E[51])))&&E[55]);
E[5] = (E[5]&&(__CheckVariables(__aa_CheckArray[10]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10()));
E[6] = (E[6]&&!((__CheckVariables(__aa_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11())));
E[6] = (__aa_R[6]&&E[6]);
E[9] = (E[9]||E[6]);
E[4] = (E[4]&&!((__CheckVariables(__aa_CheckArray[5]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5())));
E[4] = (__aa_R[7]&&E[4]);
E[10] = (E[10]||E[4]);
E[67] = ((E[5]&&((E[9]||E[5])))&&E[10]);
E[66] = (E[55]||E[67]);
if (E[66]) {
__AppendToList(__aa_EmittedList,47);
}
E[41] = (((E[8]&&!(E[14])))||((__aa_R[8]&&((E[41]&&!(E[14]))))));
E[72] = (((E[69]&&!(E[71])))||((__aa_R[5]&&((E[72]&&!(E[71]))))));
E[10] = ((E[73]||((((((((((((((E[43]||E[46])||E[49])||E[52])||E[38]))&&E[44])&&E[47])&&E[50])&&E[53])&&E[54]))||E[41])||(((((E[6]||E[4]))&&E[9])&&E[10])))))||E[72]);
E[3] = (((((E[80]&&!(E[3])))||E[66]))||E[10]);
E[22] = (E[22]&&(__CheckVariables(__aa_CheckArray[15]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15()));
E[25] = (E[25]&&(__CheckVariables(__aa_CheckArray[2]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2()));
E[28] = (E[28]&&(__CheckVariables(__aa_CheckArray[18]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__aa_A18()));
E[32] = ((((((((E[22]||E[25])||E[28]))&&E[21])&&((E[24]||E[22])))&&((E[27]||E[25])))&&((E[30]||E[28])))&&E[32]);
if (E[32]) {
__AppendToList(__aa_EmittedList,44);
}
E[0] = (E[0]&&!((__CheckVariables(__aa_CheckArray[5]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5())));
E[0] = (__aa_R[15]&&E[0]);
E[14] = (((E[1]&&!(E[14])))||((__aa_R[16]&&((E[15]&&!(E[14]))))));
E[57] = (((E[36]&&!(E[56])))||((__aa_R[14]&&((E[57]&&!(E[56]))))));
E[31] = ((E[58]||(((E[0]||(((((((((((E[20]||E[23])||E[26])||E[29])||E[17]))&&E[21])&&E[24])&&E[27])&&E[30])&&E[31])))||E[14])))||E[57]);
E[19] = (((((E[80]&&!(E[19])))||E[32]))||E[31]);
E[70] = (E[70]&&!(E[18]));
E[30] = (E[70]&&E[32]);
E[37] = (E[37]&&!(E[39]));
E[27] = (E[37]&&E[66]);
E[37] = (E[71]||((__aa_R[23]&&((E[37]&&!(E[66]))))));
E[70] = (E[56]||((__aa_R[24]&&((E[70]&&!(E[32]))))));
E[40] = ((((E[77]&&!(E[40])))||E[37])||E[70]);
E[24] = (E[60]||((__aa_R[25]&&((__aa_R[25]&&!(__aa_R[0]))))));
E[77] = (((E[77]&&!(__aa_R[25])))||E[24]);
E[21] = ((((E[30]||E[27]))&&(((E[40]||E[30])||E[27])))&&E[77]);
if (E[21]) {
__AppendToList(__aa_EmittedList,49);
}
E[64] = (((__aa_R[0]&&!(E[64])))||((__aa_R[22]&&((E[62]&&!(E[64]))))));
E[77] = (((((((E[37]||E[70])||E[24]))&&E[40])&&E[77]))||E[64]);
E[78] = (((((E[80]&&!(E[78])))||E[21]))||E[77]);
E[2] = (__aa_R[0]||((__aa_R[26]&&((E[2]&&!(E[7]))))));
E[40] = (__aa_R[27]&&!(__aa_R[0]));
E[62] = (E[39]||E[18]);
if (E[62]) {
__AppendToList(__aa_EmittedList,55);
}
E[13] = ((E[11]||E[13])||((__aa_R[27]&&((E[40]&&!(E[62]))))));
E[7] = (((E[40]&&E[62]))||((__aa_R[28]&&((E[74]&&!(E[7]))))));
E[12] = (E[12]&&!((__CheckVariables(__aa_CheckArray[8]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8())));
E[12] = (E[75]||((__aa_R[29]&&E[12])));
E[79] = ((((((E[80]&&!(E[79])))||E[2])||E[13])||E[7])||E[12]);
E[74] = (E[76]&&!(E[21]));
E[62] = (E[74]&&!((__CheckVariables(__aa_CheckArray[21]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__aa_A21())));
E[40] = (E[62]&&!((__CheckVariables(__aa_CheckArray[22]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22())));
E[11] = (E[40]&&!((__CheckVariables(__aa_CheckArray[23]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 23\n"),
#endif
__aa_A23())));
E[36] = (E[11]&&!((__CheckVariables(__aa_CheckArray[24]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 24\n"),
#endif
__aa_A24())));
E[36] = (__aa_R[0]||((__aa_R[1]&&E[36])));
E[80] = (((E[80]&&!(__aa_R[1])))||E[36]);
if (!(_true)) {
__CheckVariables(__aa_CheckArray[68]);__aa_A68();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A68\n");
#endif
}
if (E[75]) {
__CheckVariables(__aa_CheckArray[39]);__aa_A39();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A39\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[69]);__aa_A69();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A69\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[40]);__aa_A40();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A40\n");
#endif
}
E[76] = (E[76]&&E[21]);
E[74] = (E[74]&&(__CheckVariables(__aa_CheckArray[21]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__aa_A21()));
E[62] = (E[62]&&(__CheckVariables(__aa_CheckArray[22]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22()));
E[40] = (E[40]&&(__CheckVariables(__aa_CheckArray[23]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 23\n"),
#endif
__aa_A23()));
E[11] = (E[11]&&(__CheckVariables(__aa_CheckArray[24]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 24\n"),
#endif
__aa_A24()));
E[15] = (((((((((((E[76]||E[74])||E[62])||E[40])||E[11]))&&E[65])&&E[3])&&E[19])&&E[78])&&E[79])&&((((((E[80]||E[76])||E[74])||E[62])||E[40])||E[11])));
if (E[15]) {
__AppendToList(__aa_EmittedList,40);
__AppendToList(__aa_EmittedList,42);
}
if (E[15]) {
__CheckVariables(__aa_CheckArray[70]);__aa_A70();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A70\n");
#endif
}
if (E[15]) {
__CheckVariables(__aa_CheckArray[41]);__aa_A41();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A41\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[42]);__aa_A42();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A42\n");
#endif
}
if (E[15]) {
__CheckVariables(__aa_CheckArray[43]);__aa_A43();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A43\n");
#endif
}
E[1] = (E[42]||E[16]);
if (E[1]) {
__AppendToList(__aa_EmittedList,54);
}
E[11] = ((((E[76]||E[74])||E[62])||E[40])||E[11]);
if (E[11]) {
__AppendToList(__aa_EmittedList,64);
}
E[51] = (((E[5]||E[45])||E[48])||E[51]);
if (E[51]) {
__AppendToList(__aa_EmittedList,66);
}
E[28] = ((E[22]||E[25])||E[28]);
if (E[28]) {
__AppendToList(__aa_EmittedList,70);
}
E[27] = (E[30]||E[27]);
if (E[27]) {
__AppendToList(__aa_EmittedList,76);
}
E[30] = E[15];
E[80] = ((((((((((((((((E[68]||E[10])||E[31])||E[77])||E[2])||E[13])||E[7])||E[12])||E[36]))&&E[65])&&E[3])&&E[19])&&E[78])&&E[79])&&E[80]));
__aa_R[1] = (E[36]&&!(E[15]));
__aa_R[2] = (E[59]&&!(E[15]));
__aa_R[3] = (E[61]&&!(E[15]));
__aa_R[4] = (E[63]&&!(E[15]));
__aa_R[5] = (E[72]&&!(E[15]));
E[66] = (E[66]||E[15]);
E[67] = (E[67]||E[66]);
__aa_R[6] = (((E[73]&&!(E[15])))||((E[6]&&!(E[67]))));
__aa_R[7] = (((E[73]&&!(E[15])))||((E[4]&&!(E[67]))));
__aa_R[8] = (E[41]&&!(E[66]));
E[55] = ((((((E[66]||E[55]))||E[55]))||E[39])||E[55]);
__aa_R[9] = (E[43]&&!(E[55]));
__aa_R[10] = (E[46]&&!(E[55]));
__aa_R[11] = (E[49]&&!(E[55]));
__aa_R[12] = (E[52]&&!(E[55]));
__aa_R[13] = (E[38]&&!(E[55]));
__aa_R[14] = (E[57]&&!(E[15]));
E[57] = (E[32]||E[15]);
__aa_R[15] = (((E[58]&&!(E[15])))||((E[0]&&!(E[57]))));
__aa_R[16] = (E[14]&&!(E[57]));
E[32] = ((((((E[57]||E[32]))||E[32]))||E[18])||E[32]);
__aa_R[17] = (E[20]&&!(E[32]));
__aa_R[18] = (E[23]&&!(E[32]));
__aa_R[19] = (E[26]&&!(E[32]));
__aa_R[20] = (E[29]&&!(E[32]));
__aa_R[21] = (E[17]&&!(E[32]));
__aa_R[22] = (E[64]&&!(E[15]));
E[21] = (((E[15]||E[21]))||E[21]);
__aa_R[23] = (E[37]&&!(E[21]));
__aa_R[24] = (E[70]&&!(E[21]));
__aa_R[25] = (E[24]&&!(E[21]));
__aa_R[26] = (E[2]&&!(E[15]));
__aa_R[27] = (E[13]&&!(E[15]));
__aa_R[28] = (E[7]&&!(E[15]));
__aa_R[29] = (E[12]&&!(E[15]));
__aa_R[0] = !(_true);
if (__aa_R[1]) { __AppendToList(__aa_HaltList,1); }
if (__aa_R[2]) { __AppendToList(__aa_HaltList,2); }
if (__aa_R[3]) { __AppendToList(__aa_HaltList,3); }
if (__aa_R[4]) { __AppendToList(__aa_HaltList,4); }
if (__aa_R[5]) { __AppendToList(__aa_HaltList,5); }
if (__aa_R[6]) { __AppendToList(__aa_HaltList,6); }
if (__aa_R[7]) { __AppendToList(__aa_HaltList,7); }
if (__aa_R[8]) { __AppendToList(__aa_HaltList,8); }
if (__aa_R[9]) { __AppendToList(__aa_HaltList,9); }
if (__aa_R[10]) { __AppendToList(__aa_HaltList,10); }
if (__aa_R[11]) { __AppendToList(__aa_HaltList,11); }
if (__aa_R[12]) { __AppendToList(__aa_HaltList,12); }
if (__aa_R[13]) { __AppendToList(__aa_HaltList,13); }
if (__aa_R[14]) { __AppendToList(__aa_HaltList,14); }
if (__aa_R[15]) { __AppendToList(__aa_HaltList,15); }
if (__aa_R[16]) { __AppendToList(__aa_HaltList,16); }
if (__aa_R[17]) { __AppendToList(__aa_HaltList,17); }
if (__aa_R[18]) { __AppendToList(__aa_HaltList,18); }
if (__aa_R[19]) { __AppendToList(__aa_HaltList,19); }
if (__aa_R[20]) { __AppendToList(__aa_HaltList,20); }
if (__aa_R[21]) { __AppendToList(__aa_HaltList,21); }
if (__aa_R[22]) { __AppendToList(__aa_HaltList,22); }
if (__aa_R[23]) { __AppendToList(__aa_HaltList,23); }
if (__aa_R[24]) { __AppendToList(__aa_HaltList,24); }
if (__aa_R[25]) { __AppendToList(__aa_HaltList,25); }
if (__aa_R[26]) { __AppendToList(__aa_HaltList,26); }
if (__aa_R[27]) { __AppendToList(__aa_HaltList,27); }
if (__aa_R[28]) { __AppendToList(__aa_HaltList,28); }
if (__aa_R[29]) { __AppendToList(__aa_HaltList,29); }
if (!E[80]) { __AppendToList(__aa_HaltList,0); }
__ResetModuleEntryAfterReaction();
__aa_ModuleData.awaited_list = __aa_AllAwaitedList;
__aa__reset_input();
return E[80];
}

/* AUTOMATON RESET */

int aa_reset () {
__aa_ModuleData.awaited_list = __aa_AwaitedList;
__ResetModuleEntry();
__aa_ModuleData.awaited_list = __aa_AllAwaitedList;
__aa_ModuleData.state = 0;
__aa_R[0] = _true;
__aa_R[1] = _false;
__aa_R[2] = _false;
__aa_R[3] = _false;
__aa_R[4] = _false;
__aa_R[5] = _false;
__aa_R[6] = _false;
__aa_R[7] = _false;
__aa_R[8] = _false;
__aa_R[9] = _false;
__aa_R[10] = _false;
__aa_R[11] = _false;
__aa_R[12] = _false;
__aa_R[13] = _false;
__aa_R[14] = _false;
__aa_R[15] = _false;
__aa_R[16] = _false;
__aa_R[17] = _false;
__aa_R[18] = _false;
__aa_R[19] = _false;
__aa_R[20] = _false;
__aa_R[21] = _false;
__aa_R[22] = _false;
__aa_R[23] = _false;
__aa_R[24] = _false;
__aa_R[25] = _false;
__aa_R[26] = _false;
__aa_R[27] = _false;
__aa_R[28] = _false;
__aa_R[29] = _false;
__aa__reset_input();
return 0;
}
char* CompilationType = "Compiled Sorted Equations";

int __NumberOfModules = 1;
struct __ModuleEntry* __ModuleTable[] = {
&__aa_ModuleData
};
