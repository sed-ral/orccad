/* sscc : C CODE OF SORTED EQUATIONS movejRx - SIMULATION MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define CSIMUL_H_LOADED
typedef struct {char text[STRLEN];} symbolic;
extern void _boolean(boolean*, boolean);
extern boolean _eq_boolean(boolean, boolean);
extern boolean _ne_boolean(boolean, boolean);
extern boolean _cond_boolean(boolean ,boolean ,boolean);
extern char* _boolean_to_text(boolean);
extern int _check_boolean(char*);
extern void _text_to_boolean(boolean*, char*);
extern void _integer(integer*, integer);
extern boolean _eq_integer(integer, integer);
extern boolean _ne_integer(integer, integer);
extern integer _cond_integer(boolean ,integer ,integer);
extern char* _integer_to_text(integer);
extern int _check_integer(char*);
extern void _text_to_integer(integer*, char*);
extern void _string(string, string);
extern boolean _eq_string(string, string);
extern boolean _ne_string(string, string);
extern string _cond_string(boolean ,string ,string);
extern char* _string_to_text(string);
extern int _check_string(char*);
extern void _text_to_string(string, char*);
extern void _float(float*, float);
extern boolean _eq_float(float, float);
extern boolean _ne_float(float, float);
extern float _cond_float(boolean ,float ,float);
extern char* _float_to_text(float);
extern int _check_float(char*);
extern void _text_to_float(float*, char*);
extern void _double(double*, double);
extern boolean _eq_double(double, double);
extern boolean _ne_double(double, double);
extern double _cond_double(boolean ,double ,double);
extern char* _double_to_text(double);
extern int _check_double(char*);
extern void _text_to_double(double*, char*);
extern void _symbolic(symbolic*, symbolic);
extern boolean _eq_symbolic(symbolic, symbolic);
extern boolean _ne_symbolic(symbolic, symbolic);
extern symbolic _cond_symbolic(boolean ,symbolic ,symbolic);
extern char* _symbolic_to_text(symbolic);
extern int _check_symbolic(char*);
extern void _text_to_symbolic(symbolic*, char*);
extern char* __PredefinedTypeToText(int, char*);
#define _true 1
#define _false 0
#define __movejRx_GENERIC_TEST(TEST) return TEST;
typedef void (*__movejRx_APF)();
static __movejRx_APF *__movejRx_PActionArray;
static int **__movejRx_PCheckArray;
struct __SourcePoint {
int linkback;
int line;
int column;
int instance_index;
};
struct __InstanceEntry {
char *module_name;
int father_index;
char *dir_name;
char *file_name;
struct __SourcePoint source_point;
struct __SourcePoint end_point;
struct __SourcePoint instance_point;
};
struct __TaskEntry {
char *name;
int   nb_args_ref;
int   nb_args_val;
int  *type_codes_array;
struct __SourcePoint source_point;
};
struct __SignalEntry {
char *name;
int code;
int variable_index;
int present;
struct __SourcePoint source_point;
int number_of_emit_source_points;
struct __SourcePoint* emit_source_point_array;
int number_of_present_source_points;
struct __SourcePoint* present_source_point_array;
int number_of_access_source_points;
struct __SourcePoint* access_source_point_array;
};
struct __InputEntry {
char *name;
int hash;
char *type_name;
int is_a_sensor;
int type_code;
int multiple;
int signal_index;
int (*p_check_input)(char*);
void (*p_input_function)();
int present;
struct __SourcePoint source_point;
};
struct __ReturnEntry {
char *name;
int hash;
char *type_name;
int type_code;
int signal_index;
int exec_index;
int (*p_check_input)(char*);
void (*p_input_function)();
int present;
struct __SourcePoint source_point;
};
struct __ImplicationEntry {
int master;
int slave;
struct __SourcePoint source_point;
};
struct __ExclusionEntry {
int *exclusion_list;
struct __SourcePoint source_point;
};
struct __VariableEntry {
char *full_name;
char *short_name;
char *type_name;
int type_code;
int comment_kind;
int is_initialized;
char *p_variable;
char *source_name;
int written;
unsigned char written_in_transition;
unsigned char read_in_transition;
struct __SourcePoint source_point;
};
struct __ExecEntry {
int task_index;
int *var_indexes_array;
char **p_values_array;
struct __SourcePoint source_point;
};
struct __HaltEntry {
struct __SourcePoint source_point;
};
struct __NetEntry {
int known;
int value;
int number_of_source_points;
struct __SourcePoint* source_point_array;
};
struct __ModuleEntry {
char *version_id;
char *name;
int number_of_instances;
int number_of_tasks;
int number_of_signals;
int number_of_inputs;
int number_of_returns;
int number_of_sensors;
int number_of_outputs;
int number_of_locals;
int number_of_exceptions;
int number_of_implications;
int number_of_exclusions;
int number_of_variables;
int number_of_execs;
int number_of_halts;
int number_of_nets;
int number_of_states;
int state;
unsigned short *halt_list;
unsigned short *awaited_list;
unsigned short *emitted_list;
unsigned short *started_list;
unsigned short *killed_list;
unsigned short *suspended_list;
unsigned short *active_list;
int run_time_error_code;
int error_info;
void (*init)();
int (*run)();
int (*reset)();
char *(*show_variable)(int);
void (*set_variable)(int, char*, char*);
int (*check_value)(int, char*);
int (*execute_action)();
struct __InstanceEntry* instance_table;
struct __TaskEntry* task_table;
struct __SignalEntry* signal_table;
struct __InputEntry* input_table;
struct __ReturnEntry* return_table;
struct __ImplicationEntry* implication_table;
struct __ExclusionEntry* exclusion_table;
struct __VariableEntry* variable_table;
struct __ExecEntry* exec_table;
struct __HaltEntry* halt_table;
struct __NetEntry* net_table;
};

#include "movejRx.h"

/* EXTERN DECLARATIONS */

#ifndef _orcStrlPtr_to_text
extern char* _orcStrlPtr_to_text(orcStrlPtr);
#endif
#ifndef _check_orcStrlPtr
extern int _check_orcStrlPtr(char*);
#endif
#ifndef _text_to_orcStrlPtr
extern void _text_to_orcStrlPtr(orcStrlPtr*, char*);
#endif
extern int __CheckVariables(int*);
extern void __ResetInput();
extern void __ResetExecs();
extern void __ResetVariables();
extern void __ResetVariableStatus();
extern void __AppendToList(unsigned short*, unsigned short);
extern void __ListCopy(unsigned short*, unsigned short**);
extern void __WriteVariable(int);
extern void __ResetVariable(int);
extern void __ResetModuleEntry();
extern void __ResetModuleEntryBeforeReaction();
extern void __ResetModuleEntryAfterReaction();
#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __movejRx_V0;
static boolean __movejRx_V1;
static boolean __movejRx_V2;
static boolean __movejRx_V3;
static boolean __movejRx_V4;
static boolean __movejRx_V5;
static boolean __movejRx_V6;
static orcStrlPtr __movejRx_V7;
static boolean __movejRx_V8;
static boolean __movejRx_V9;
static boolean __movejRx_V10;
static boolean __movejRx_V11;
static orcStrlPtr __movejRx_V12;
static orcStrlPtr __movejRx_V13;
static orcStrlPtr __movejRx_V14;
static orcStrlPtr __movejRx_V15;

static unsigned short __movejRx_HaltList[11];
static unsigned short __movejRx_AwaitedList[27];
static unsigned short __movejRx_EmittedList[27];
static unsigned short __movejRx_StartedList[1];
static unsigned short __movejRx_KilledList[1];
static unsigned short __movejRx_SuspendedList[1];
static unsigned short __movejRx_ActiveList[1];
static unsigned short __movejRx_AllAwaitedList[27]={11,0,1,2,3,4,5,6,7,8,9,10};


/* INPUT FUNCTIONS */

void movejRx_I_jointlimit () {
__movejRx_V0 = _true;
}
void movejRx_IS_jointlimit () {
__movejRx_V0 = _true;
}
void movejRx_I_endtraj () {
__movejRx_V1 = _true;
}
void movejRx_IS_endtraj () {
__movejRx_V1 = _true;
}
void movejRx_I_endtime () {
__movejRx_V2 = _true;
}
void movejRx_IS_endtime () {
__movejRx_V2 = _true;
}
void movejRx_I_movejRxTimeoutgoodtraj () {
__movejRx_V3 = _true;
}
void movejRx_IS_movejRxTimeoutgoodtraj () {
__movejRx_V3 = _true;
}
void movejRx_I_goodtraj () {
__movejRx_V4 = _true;
}
void movejRx_IS_goodtraj () {
__movejRx_V4 = _true;
}
void movejRx_I_trackerr () {
__movejRx_V5 = _true;
}
void movejRx_IS_trackerr () {
__movejRx_V5 = _true;
}
void movejRx_I_InitOK_Rx90 () {
__movejRx_V6 = _true;
}
void movejRx_IS_InitOK_Rx90 () {
__movejRx_V6 = _true;
}
void movejRx_I_movejRx_Start (orcStrlPtr __V) {
__WriteVariable(7);
_orcStrlPtr(&__movejRx_V7,__V);
__movejRx_V8 = _true;
}
void movejRx_IS_movejRx_Start (string __V) {
__WriteVariable(7);
_text_to_orcStrlPtr(&__movejRx_V7,__V);
__movejRx_V8 = _true;
}
void movejRx_I_START_movejRx () {
__movejRx_V9 = _true;
}
void movejRx_IS_START_movejRx () {
__movejRx_V9 = _true;
}
void movejRx_I_FinTransite_Rx90 () {
__movejRx_V10 = _true;
}
void movejRx_IS_FinTransite_Rx90 () {
__movejRx_V10 = _true;
}
void movejRx_I_Abort_Local_movejRx () {
__movejRx_V11 = _true;
}
void movejRx_IS_Abort_Local_movejRx () {
__movejRx_V11 = _true;
}

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int movejRx_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

static int __movejRx_A1 () {
__movejRx_GENERIC_TEST(__movejRx_V0);
}
static int __movejRx_Check1 [] = {1,0,0};
static int __movejRx_A2 () {
__movejRx_GENERIC_TEST(__movejRx_V1);
}
static int __movejRx_Check2 [] = {1,0,0};
static int __movejRx_A3 () {
__movejRx_GENERIC_TEST(__movejRx_V2);
}
static int __movejRx_Check3 [] = {1,0,0};
static int __movejRx_A4 () {
__movejRx_GENERIC_TEST(__movejRx_V3);
}
static int __movejRx_Check4 [] = {1,0,0};
static int __movejRx_A5 () {
__movejRx_GENERIC_TEST(__movejRx_V4);
}
static int __movejRx_Check5 [] = {1,0,0};
static int __movejRx_A6 () {
__movejRx_GENERIC_TEST(__movejRx_V5);
}
static int __movejRx_Check6 [] = {1,0,0};
static int __movejRx_A7 () {
__movejRx_GENERIC_TEST(__movejRx_V6);
}
static int __movejRx_Check7 [] = {1,0,0};
static int __movejRx_A8 () {
__movejRx_GENERIC_TEST(__movejRx_V8);
}
static int __movejRx_Check8 [] = {1,0,0};
static int __movejRx_A9 () {
__movejRx_GENERIC_TEST(__movejRx_V9);
}
static int __movejRx_Check9 [] = {1,0,0};
static int __movejRx_A10 () {
__movejRx_GENERIC_TEST(__movejRx_V10);
}
static int __movejRx_Check10 [] = {1,0,0};
static int __movejRx_A11 () {
__movejRx_GENERIC_TEST(__movejRx_V11);
}
static int __movejRx_Check11 [] = {1,0,0};

/* OUTPUT ACTIONS */

static void __movejRx_A12 () {
#ifdef __OUTPUT
movejRx_O_STARTED_movejRx();
#endif
__AppendToList(__movejRx_EmittedList,11);
}
static int __movejRx_Check12 [] = {1,0,0};
static void __movejRx_A13 () {
#ifdef __OUTPUT
movejRx_O_GoodEnd_movejRx();
#endif
__AppendToList(__movejRx_EmittedList,12);
}
static int __movejRx_Check13 [] = {1,0,0};
static void __movejRx_A14 () {
#ifdef __OUTPUT
movejRx_O_ActivatemovejRx_Rx90(__movejRx_V12);
#endif
__AppendToList(__movejRx_EmittedList,13);
}
static int __movejRx_Check14 [] = {1,0,0};
static void __movejRx_A15 () {
#ifdef __OUTPUT
movejRx_O_T2_movejRx();
#endif
__AppendToList(__movejRx_EmittedList,14);
}
static int __movejRx_Check15 [] = {1,0,0};
static void __movejRx_A16 () {
#ifdef __OUTPUT
movejRx_O_movejRxTransite(__movejRx_V13);
#endif
__AppendToList(__movejRx_EmittedList,15);
}
static int __movejRx_Check16 [] = {1,0,0};
static void __movejRx_A17 () {
#ifdef __OUTPUT
movejRx_O_Abort_movejRx(__movejRx_V14);
#endif
__AppendToList(__movejRx_EmittedList,16);
}
static int __movejRx_Check17 [] = {1,0,0};
static void __movejRx_A18 () {
#ifdef __OUTPUT
movejRx_O_BF_movejRx();
#endif
__AppendToList(__movejRx_EmittedList,17);
}
static int __movejRx_Check18 [] = {1,0,0};
static void __movejRx_A19 () {
#ifdef __OUTPUT
movejRx_O_T3_movejRx();
#endif
__AppendToList(__movejRx_EmittedList,18);
}
static int __movejRx_Check19 [] = {1,0,0};
static void __movejRx_A20 () {
#ifdef __OUTPUT
movejRx_O_StartTransite_Rx90();
#endif
__AppendToList(__movejRx_EmittedList,19);
}
static int __movejRx_Check20 [] = {1,0,0};
static void __movejRx_A21 () {
#ifdef __OUTPUT
movejRx_O_Prev_rt_Rx90(__movejRx_V15);
#endif
__AppendToList(__movejRx_EmittedList,20);
}
static int __movejRx_Check21 [] = {1,0,0};
static void __movejRx_A22 () {
#ifdef __OUTPUT
movejRx_O_ReadyToStart_Rx90();
#endif
__AppendToList(__movejRx_EmittedList,21);
}
static int __movejRx_Check22 [] = {1,0,0};

/* ASSIGNMENTS */

static void __movejRx_A23 () {
__movejRx_V0 = _false;
}
static int __movejRx_Check23 [] = {1,0,1,0};
static void __movejRx_A24 () {
__movejRx_V1 = _false;
}
static int __movejRx_Check24 [] = {1,0,1,1};
static void __movejRx_A25 () {
__movejRx_V2 = _false;
}
static int __movejRx_Check25 [] = {1,0,1,2};
static void __movejRx_A26 () {
__movejRx_V3 = _false;
}
static int __movejRx_Check26 [] = {1,0,1,3};
static void __movejRx_A27 () {
__movejRx_V4 = _false;
}
static int __movejRx_Check27 [] = {1,0,1,4};
static void __movejRx_A28 () {
__movejRx_V5 = _false;
}
static int __movejRx_Check28 [] = {1,0,1,5};
static void __movejRx_A29 () {
__movejRx_V6 = _false;
}
static int __movejRx_Check29 [] = {1,0,1,6};
static void __movejRx_A30 () {
__movejRx_V8 = _false;
}
static int __movejRx_Check30 [] = {1,0,1,8};
static void __movejRx_A31 () {
__movejRx_V9 = _false;
}
static int __movejRx_Check31 [] = {1,0,1,9};
static void __movejRx_A32 () {
__movejRx_V10 = _false;
}
static int __movejRx_Check32 [] = {1,0,1,10};
static void __movejRx_A33 () {
__movejRx_V11 = _false;
}
static int __movejRx_Check33 [] = {1,0,1,11};
static void __movejRx_A34 () {
_orcStrlPtr(&__movejRx_V14,__movejRx_V7);
}
static int __movejRx_Check34 [] = {1,1,7,1,14};
static void __movejRx_A35 () {
_orcStrlPtr(&__movejRx_V12,__movejRx_V7);
}
static int __movejRx_Check35 [] = {1,1,7,1,12};
static void __movejRx_A36 () {
_orcStrlPtr(&__movejRx_V15,__movejRx_V7);
}
static int __movejRx_Check36 [] = {1,1,7,1,15};
static void __movejRx_A37 () {
_orcStrlPtr(&__movejRx_V13,__movejRx_V7);
}
static int __movejRx_Check37 [] = {1,1,7,1,13};
static void __movejRx_A38 () {
_orcStrlPtr(&__movejRx_V13,__movejRx_V7);
}
static int __movejRx_Check38 [] = {1,1,7,1,13};
static void __movejRx_A39 () {
_orcStrlPtr(&__movejRx_V13,__movejRx_V7);
}
static int __movejRx_Check39 [] = {1,1,7,1,13};
static void __movejRx_A40 () {
_orcStrlPtr(&__movejRx_V13,__movejRx_V7);
}
static int __movejRx_Check40 [] = {1,1,7,1,13};

/* PROCEDURE CALLS */

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

static void __movejRx_A41 () {
;
__ResetVariable(7);
}
static int __movejRx_Check41 [] = {1,0,0};
static void __movejRx_A42 () {
;
__ResetVariable(12);
}
static int __movejRx_Check42 [] = {1,0,0};
static void __movejRx_A43 () {
;
__ResetVariable(13);
}
static int __movejRx_Check43 [] = {1,0,0};
static void __movejRx_A44 () {
;
__ResetVariable(14);
}
static int __movejRx_Check44 [] = {1,0,0};
static void __movejRx_A45 () {
;
__ResetVariable(15);
}
static int __movejRx_Check45 [] = {1,0,0};

/* ACTION SEQUENCES */


static int *__movejRx_CheckArray[] = {
0,
__movejRx_Check1,
__movejRx_Check2,
__movejRx_Check3,
__movejRx_Check4,
__movejRx_Check5,
__movejRx_Check6,
__movejRx_Check7,
__movejRx_Check8,
__movejRx_Check9,
__movejRx_Check10,
__movejRx_Check11,
__movejRx_Check12,
__movejRx_Check13,
__movejRx_Check14,
__movejRx_Check15,
__movejRx_Check16,
__movejRx_Check17,
__movejRx_Check18,
__movejRx_Check19,
__movejRx_Check20,
__movejRx_Check21,
__movejRx_Check22,
__movejRx_Check23,
__movejRx_Check24,
__movejRx_Check25,
__movejRx_Check26,
__movejRx_Check27,
__movejRx_Check28,
__movejRx_Check29,
__movejRx_Check30,
__movejRx_Check31,
__movejRx_Check32,
__movejRx_Check33,
__movejRx_Check34,
__movejRx_Check35,
__movejRx_Check36,
__movejRx_Check37,
__movejRx_Check38,
__movejRx_Check39,
__movejRx_Check40,
__movejRx_Check41,
__movejRx_Check42,
__movejRx_Check43,
__movejRx_Check44,
__movejRx_Check45
};
static int **__movejRx_PCheckArray =  __movejRx_CheckArray;

/* INIT FUNCTION */

#ifndef NO_INIT
void movejRx_initialize () {
}
#endif

/* SHOW VARIABLE FUNCTION */

char* __movejRx_show_variable (int __V) {
extern struct __VariableEntry __movejRx_VariableTable[];
struct __VariableEntry* p_var = &__movejRx_VariableTable[__V];
if (p_var->type_code < 0) {return __PredefinedTypeToText(p_var->type_code, p_var->p_variable);
} else {
switch (p_var->type_code) {
case 0: return _orcStrlPtr_to_text(*(orcStrlPtr*)p_var->p_variable);
default: return 0;
}
}
}

/* SET VARIABLE FUNCTION */

static void __movejRx_set_variable(int __Type, char* __pVar, char* __Text) {
}

/* CHECK VALUE FUNCTION */

static int __movejRx_check_value (int __Type, char* __Text) {
return 0;
}

/* SIMULATION TABLES */

struct __InstanceEntry __movejRx_InstanceTable [] = {
{"movejRx",0,"/local/projets/robotique/Rx90/orccad/Spec/RobotProcedures/movejRx.TC","movejRx.strl",{1,1,1,0},{1,114,1,0},{0,0,0,0}},
};

struct __SignalEntry __movejRx_SignalTable [] = {
{"jointlimit",33,0,0,{1,5,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"endtraj",33,0,0,{1,6,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"endtime",33,0,0,{1,7,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"movejRxTimeoutgoodtraj",33,0,0,{1,8,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"goodtraj",33,0,0,{1,9,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"trackerr",33,0,0,{1,10,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"InitOK_Rx90",33,0,0,{1,11,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"movejRx_Start",1,7,0,{1,12,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"START_movejRx",33,0,0,{1,13,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FinTransite_Rx90",33,0,0,{1,14,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_Local_movejRx",33,0,0,{1,15,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"STARTED_movejRx",34,0,0,{1,18,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEnd_movejRx",34,0,0,{1,19,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ActivatemovejRx_Rx90",2,12,0,{1,20,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2_movejRx",34,0,0,{1,21,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"movejRxTransite",2,13,0,{1,22,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_movejRx",2,14,0,{1,23,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_movejRx",34,0,0,{1,24,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_movejRx",34,0,0,{1,25,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"StartTransite_Rx90",34,0,0,{1,26,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Prev_rt_Rx90",2,15,0,{1,27,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ReadyToStart_Rx90",34,0,0,{1,28,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3",48,0,0,{1,42,6,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort",48,0,0,{1,46,8,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2",48,0,0,{1,73,8,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF",48,0,0,{1,74,9,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL}};

struct __InputEntry __movejRx_InputTable [] = {
{"jointlimit",81,0,0,-1,0,0,0,movejRx_IS_jointlimit,0,{1,5,4,0}},
{"endtraj",37,0,0,-1,0,1,0,movejRx_IS_endtraj,0,{1,6,4,0}},
{"endtime",35,0,0,-1,0,2,0,movejRx_IS_endtime,0,{1,7,4,0}},
{"movejRxTimeoutgoodtraj",25,0,0,-1,0,3,0,movejRx_IS_movejRxTimeoutgoodtraj,0,{1,8,4,0}},
{"goodtraj",50,0,0,-1,0,4,0,movejRx_IS_goodtraj,0,{1,9,4,0}},
{"trackerr",54,0,0,-1,0,5,0,movejRx_IS_trackerr,0,{1,10,4,0}},
{"InitOK_Rx90",51,0,0,-1,0,6,0,movejRx_IS_InitOK_Rx90,0,{1,11,4,0}},
{"movejRx_Start",55,"orcStrlPtr",0,0,0,7,_check_orcStrlPtr,movejRx_IS_movejRx_Start,0,{1,12,4,0}},
{"START_movejRx",28,0,0,-1,0,8,0,movejRx_IS_START_movejRx,0,{1,13,4,0}},
{"FinTransite_Rx90",14,0,0,-1,0,9,0,movejRx_IS_FinTransite_Rx90,0,{1,14,4,0}},
{"Abort_Local_movejRx",13,0,0,-1,0,10,0,movejRx_IS_Abort_Local_movejRx,0,{1,15,4,0}}};

static int __movejRx_ExclusionEntry0[] = {10,0,1,2,3,4,5,6,8,10,9};

struct __ExclusionEntry __movejRx_ExclusionTable [] = {
{__movejRx_ExclusionEntry0,{1,31,4,0}}
};

struct __VariableEntry __movejRx_VariableTable [] = {
{"__movejRx_V0","V0","boolean",-2,2,0,(char*)&__movejRx_V0,"jointlimit",0,0,0,{1,5,4,0}},
{"__movejRx_V1","V1","boolean",-2,2,0,(char*)&__movejRx_V1,"endtraj",0,0,0,{1,6,4,0}},
{"__movejRx_V2","V2","boolean",-2,2,0,(char*)&__movejRx_V2,"endtime",0,0,0,{1,7,4,0}},
{"__movejRx_V3","V3","boolean",-2,2,0,(char*)&__movejRx_V3,"movejRxTimeoutgoodtraj",0,0,0,{1,8,4,0}},
{"__movejRx_V4","V4","boolean",-2,2,0,(char*)&__movejRx_V4,"goodtraj",0,0,0,{1,9,4,0}},
{"__movejRx_V5","V5","boolean",-2,2,0,(char*)&__movejRx_V5,"trackerr",0,0,0,{1,10,4,0}},
{"__movejRx_V6","V6","boolean",-2,2,0,(char*)&__movejRx_V6,"InitOK_Rx90",0,0,0,{1,11,4,0}},
{"__movejRx_V7","V7","orcStrlPtr",0,1,0,(char*)&__movejRx_V7,"movejRx_Start",0,0,0,{1,12,4,0}},
{"__movejRx_V8","V8","boolean",-2,2,0,(char*)&__movejRx_V8,"movejRx_Start",0,0,0,{1,12,4,0}},
{"__movejRx_V9","V9","boolean",-2,2,0,(char*)&__movejRx_V9,"START_movejRx",0,0,0,{1,13,4,0}},
{"__movejRx_V10","V10","boolean",-2,2,0,(char*)&__movejRx_V10,"FinTransite_Rx90",0,0,0,{1,14,4,0}},
{"__movejRx_V11","V11","boolean",-2,2,0,(char*)&__movejRx_V11,"Abort_Local_movejRx",0,0,0,{1,15,4,0}},
{"__movejRx_V12","V12","orcStrlPtr",0,1,0,(char*)&__movejRx_V12,"ActivatemovejRx_Rx90",0,0,0,{1,20,4,0}},
{"__movejRx_V13","V13","orcStrlPtr",0,1,0,(char*)&__movejRx_V13,"movejRxTransite",0,0,0,{1,22,4,0}},
{"__movejRx_V14","V14","orcStrlPtr",0,1,0,(char*)&__movejRx_V14,"Abort_movejRx",0,0,0,{1,23,4,0}},
{"__movejRx_V15","V15","orcStrlPtr",0,1,0,(char*)&__movejRx_V15,"Prev_rt_Rx90",0,0,0,{1,27,4,0}}
};

struct __HaltEntry __movejRx_HaltTable [] = {
{{1,114,1,0}},
{{1,43,9,0}},
{{1,50,5,0}},
{{1,55,4,0}},
{{1,64,4,0}},
{{1,77,5,0}},
{{1,83,4,0}},
{{1,85,4,0}},
{{1,87,4,0}},
{{1,91,4,0}}
};


static void __movejRx__reset_input () {
__movejRx_V0 = _false;
__movejRx_V1 = _false;
__movejRx_V2 = _false;
__movejRx_V3 = _false;
__movejRx_V4 = _false;
__movejRx_V5 = _false;
__movejRx_V6 = _false;
__movejRx_V8 = _false;
__movejRx_V9 = _false;
__movejRx_V10 = _false;
__movejRx_V11 = _false;
}


/* MODULE DATA FOR SIMULATION */

int movejRx();
int movejRx_reset();

static struct __ModuleEntry __movejRx_ModuleData = {
"Simulation interface release 5","movejRx",
1,0,26,11,0,0,11,0,4,0,1,16,0,10,0,0,0,
__movejRx_HaltList,
__movejRx_AwaitedList,
__movejRx_EmittedList,
__movejRx_StartedList,
__movejRx_KilledList,
__movejRx_SuspendedList,
__movejRx_ActiveList,
0,0,
movejRx_initialize,movejRx,movejRx_reset,
__movejRx_show_variable,__movejRx_set_variable,__movejRx_check_value,0,
__movejRx_InstanceTable,
0,
__movejRx_SignalTable,__movejRx_InputTable,0,
0,__movejRx_ExclusionTable,
__movejRx_VariableTable,
0,
__movejRx_HaltTable,
0};

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __movejRx_R[10] = {_true,_false,_false,_false,_false,_false,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int movejRx () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[35];

__movejRx_ModuleData.awaited_list = __movejRx_AwaitedList;
__ResetModuleEntryBeforeReaction();
E[0] = (__movejRx_R[9]&&!(__movejRx_R[0]));
E[1] = (E[0]&&(__CheckVariables(__movejRx_CheckArray[2]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__movejRx_A2()));
if (E[1]) {
__AppendToList(__movejRx_EmittedList,25);
}
E[2] = ((((__movejRx_R[5]||__movejRx_R[6])||__movejRx_R[7])||__movejRx_R[8])||__movejRx_R[9]);
E[3] = (__movejRx_R[2]||__movejRx_R[3]);
E[4] = (E[3]&&!(__movejRx_R[0]));
E[5] = (E[4]&&!((__CheckVariables(__movejRx_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11())));
E[6] = (__movejRx_R[2]&&E[5]);
E[7] = (E[6]&&!((__CheckVariables(__movejRx_CheckArray[4]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__movejRx_A4())));
E[7] = (__movejRx_R[2]&&E[7]);
E[8] = (E[7]&&(__CheckVariables(__movejRx_CheckArray[5]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__movejRx_A5()));
E[5] = (__movejRx_R[3]&&E[5]);
E[9] = (E[5]&&(__CheckVariables(__movejRx_CheckArray[7]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__movejRx_A7()));
E[10] = (((E[3]&&!(__movejRx_R[2])))||E[8]);
E[11] = (((E[3]&&!(__movejRx_R[3])))||E[9]);
E[9] = ((((E[8]||E[9]))&&E[10])&&E[11]);
if (E[9]) {
__AppendToList(__movejRx_EmittedList,21);
}
E[8] = (E[9]&&(__CheckVariables(__movejRx_CheckArray[10]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__movejRx_A10()));
E[12] = (__movejRx_R[4]&&!(__movejRx_R[0]));
E[13] = (E[12]&&!((__CheckVariables(__movejRx_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11())));
E[13] = (__movejRx_R[4]&&E[13]);
E[14] = (E[13]&&(__CheckVariables(__movejRx_CheckArray[10]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__movejRx_A10()));
E[14] = (E[8]||E[14]);
if (E[14]) {
__AppendToList(__movejRx_EmittedList,13);
__AppendToList(__movejRx_EmittedList,20);
}
if (E[14]) {
__CheckVariables(__movejRx_CheckArray[35]);__movejRx_A35();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A35\n");
#endif
}
if (E[14]) {
__CheckVariables(__movejRx_CheckArray[36]);__movejRx_A36();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A36\n");
#endif
}
E[8] = (__movejRx_R[5]&&!(__movejRx_R[0]));
E[15] = (E[8]&&!((__CheckVariables(__movejRx_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11())));
E[15] = (E[14]||((__movejRx_R[5]&&E[15])));
E[16] = (((E[2]&&!(__movejRx_R[5])))||E[15]);
E[17] = (__movejRx_R[6]&&!(__movejRx_R[0]));
E[18] = (E[17]&&!((__CheckVariables(__movejRx_CheckArray[3]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__movejRx_A3())));
E[18] = (E[14]||((__movejRx_R[6]&&E[18])));
E[19] = (((E[2]&&!(__movejRx_R[6])))||E[18]);
E[20] = (__movejRx_R[7]&&!(__movejRx_R[0]));
E[21] = (E[20]&&!((__CheckVariables(__movejRx_CheckArray[6]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__movejRx_A6())));
E[21] = (E[14]||((__movejRx_R[7]&&E[21])));
E[22] = (((E[2]&&!(__movejRx_R[7])))||E[21]);
E[23] = (__movejRx_R[8]&&!(__movejRx_R[0]));
E[24] = (E[23]&&!((__CheckVariables(__movejRx_CheckArray[1]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__movejRx_A1())));
E[24] = (E[14]||((__movejRx_R[8]&&E[24])));
E[25] = (((E[2]&&!(__movejRx_R[8])))||E[24]);
E[0] = (E[0]&&!((__CheckVariables(__movejRx_CheckArray[2]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__movejRx_A2())));
E[0] = (E[14]||((__movejRx_R[9]&&E[0])));
E[26] = (((E[2]&&!(__movejRx_R[9])))||E[0]);
E[27] = (E[26]||E[1]);
E[1] = (((((E[1]&&E[16])&&E[19])&&E[22])&&E[25])&&E[27]);
if (E[1]) {
__AppendToList(__movejRx_EmittedList,12);
__AppendToList(__movejRx_EmittedList,17);
}
if (E[1]) {
__CheckVariables(__movejRx_CheckArray[39]);__movejRx_A39();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A39\n");
#endif
}
E[8] = (E[8]&&(__CheckVariables(__movejRx_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11()));
if (E[8]) {
__CheckVariables(__movejRx_CheckArray[38]);__movejRx_A38();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A38\n");
#endif
}
E[28] = (E[16]||E[8]);
E[29] = (((((E[8]&&E[28])&&E[19])&&E[22])&&E[25])&&E[27]);
E[12] = (E[12]&&(__CheckVariables(__movejRx_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11()));
if (E[12]) {
__CheckVariables(__movejRx_CheckArray[37]);__movejRx_A37();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A37\n");
#endif
}
E[4] = (E[4]&&(__CheckVariables(__movejRx_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__movejRx_A11()));
if (E[4]) {
__AppendToList(__movejRx_EmittedList,16);
}
if (E[4]) {
__CheckVariables(__movejRx_CheckArray[34]);__movejRx_A34();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A34\n");
#endif
}
E[30] = (__movejRx_R[0]&&!((__CheckVariables(__movejRx_CheckArray[8]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__movejRx_A8())));
if (E[30]) {
__CheckVariables(__movejRx_CheckArray[41]);__movejRx_A41();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A41\n");
#endif
}
if (__movejRx_R[0]) {
__CheckVariables(__movejRx_CheckArray[42]);__movejRx_A42();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A42\n");
#endif
}
if (__movejRx_R[0]) {
__CheckVariables(__movejRx_CheckArray[43]);__movejRx_A43();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A43\n");
#endif
}
if (__movejRx_R[0]) {
__CheckVariables(__movejRx_CheckArray[44]);__movejRx_A44();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A44\n");
#endif
}
if (__movejRx_R[0]) {
__CheckVariables(__movejRx_CheckArray[45]);__movejRx_A45();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A45\n");
#endif
}
E[31] = (((((E[1]||E[29])||E[12])||E[4]))||__movejRx_R[0]);
E[32] = (E[31]&&(__CheckVariables(__movejRx_CheckArray[9]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__movejRx_A9()));
E[33] = (__movejRx_R[1]&&!(__movejRx_R[0]));
E[34] = (E[33]&&(__CheckVariables(__movejRx_CheckArray[9]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__movejRx_A9()));
E[34] = (E[32]||E[34]);
if (E[34]) {
__AppendToList(__movejRx_EmittedList,11);
}
if (E[34]) {
__CheckVariables(__movejRx_CheckArray[12]);__movejRx_A12();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A12\n");
#endif
}
if (E[1]) {
__CheckVariables(__movejRx_CheckArray[13]);__movejRx_A13();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A13\n");
#endif
}
if (E[14]) {
__CheckVariables(__movejRx_CheckArray[14]);__movejRx_A14();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A14\n");
#endif
}
if (!(_true)) {
__CheckVariables(__movejRx_CheckArray[15]);__movejRx_A15();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A15\n");
#endif
}
if (((E[8]||E[1])||E[12])) {
__AppendToList(__movejRx_EmittedList,15);
}
if ((((E[8]||E[1])||E[12]))) {
__CheckVariables(__movejRx_CheckArray[16]);__movejRx_A16();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A16\n");
#endif
}
if (E[4]) {
__CheckVariables(__movejRx_CheckArray[17]);__movejRx_A17();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A17\n");
#endif
}
if (E[1]) {
__CheckVariables(__movejRx_CheckArray[18]);__movejRx_A18();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A18\n");
#endif
}
E[17] = (E[17]&&(__CheckVariables(__movejRx_CheckArray[3]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__movejRx_A3()));
E[20] = (E[20]&&(__CheckVariables(__movejRx_CheckArray[6]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__movejRx_A6()));
E[23] = (E[23]&&(__CheckVariables(__movejRx_CheckArray[1]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__movejRx_A1()));
E[27] = ((((((((E[17]||E[20])||E[23]))&&E[28])&&((E[19]||E[17])))&&((E[22]||E[20])))&&((E[25]||E[23])))&&E[27]);
E[6] = (E[6]&&(__CheckVariables(__movejRx_CheckArray[4]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__movejRx_A4()));
E[7] = (E[7]&&!((__CheckVariables(__movejRx_CheckArray[5]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__movejRx_A5())));
E[7] = (__movejRx_R[2]&&E[7]);
E[10] = (E[10]||E[7]);
E[5] = (E[5]&&!((__CheckVariables(__movejRx_CheckArray[7]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__movejRx_A7())));
E[5] = (__movejRx_R[3]&&E[5]);
E[11] = (E[11]||E[5]);
E[28] = ((E[6]&&((E[10]||E[6])))&&E[11]);
E[32] = (E[27]||E[28]);
if (E[32]) {
__AppendToList(__movejRx_EmittedList,18);
}
if (E[32]) {
__CheckVariables(__movejRx_CheckArray[19]);__movejRx_A19();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A19\n");
#endif
}
if (((E[8]||E[1])||E[12])) {
__AppendToList(__movejRx_EmittedList,19);
}
if ((((E[8]||E[1])||E[12]))) {
__CheckVariables(__movejRx_CheckArray[20]);__movejRx_A20();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A20\n");
#endif
}
if (E[14]) {
__CheckVariables(__movejRx_CheckArray[21]);__movejRx_A21();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A21\n");
#endif
}
if (E[9]) {
__CheckVariables(__movejRx_CheckArray[22]);__movejRx_A22();
#ifdef TRACE_ACTION
fprintf(stderr, "__movejRx_A22\n");
#endif
}
E[23] = (((E[6]||E[17])||E[20])||E[23]);
if (E[23]) {
__AppendToList(__movejRx_EmittedList,22);
}
E[20] = ((E[8]||E[12])||E[4]);
if (E[20]) {
__AppendToList(__movejRx_EmittedList,23);
}
E[17] = E[32];
E[6] = (E[9]&&!((__CheckVariables(__movejRx_CheckArray[10]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__movejRx_A10())));
E[13] = (E[13]&&!((__CheckVariables(__movejRx_CheckArray[10]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__movejRx_A10())));
E[13] = (E[6]||((__movejRx_R[4]&&E[13])));
E[31] = (E[31]&&!((__CheckVariables(__movejRx_CheckArray[9]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__movejRx_A9())));
E[33] = (E[33]&&!((__CheckVariables(__movejRx_CheckArray[9]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__movejRx_A9())));
E[33] = (E[31]||((__movejRx_R[1]&&E[33])));
E[11] = (((E[34]||((((((((((((((E[15]||E[18])||E[21])||E[24])||E[0]))&&E[16])&&E[19])&&E[22])&&E[25])&&E[26]))||E[13])||(((((E[7]||E[5]))&&E[10])&&E[11])))))||E[33]));
E[3] = ((((E[2]||__movejRx_R[4])||E[3]))||__movejRx_R[1]);
E[28] = (E[28]||E[32]);
__movejRx_R[2] = (E[34]||((E[7]&&!(E[28]))));
__movejRx_R[3] = (E[34]||((E[5]&&!(E[28]))));
__movejRx_R[4] = (E[13]&&!(E[32]));
E[27] = (((((((((E[32]||E[29])||E[27]))||E[29])||E[27]))||E[1])||E[29])||E[27]);
__movejRx_R[5] = (E[15]&&!(E[27]));
__movejRx_R[6] = (E[18]&&!(E[27]));
__movejRx_R[7] = (E[21]&&!(E[27]));
__movejRx_R[8] = (E[24]&&!(E[27]));
__movejRx_R[9] = (E[0]&&!(E[27]));
__movejRx_R[0] = !(_true);
__movejRx_R[1] = E[33];
if (__movejRx_R[1]) { __AppendToList(__movejRx_HaltList,1); }
if (__movejRx_R[2]) { __AppendToList(__movejRx_HaltList,2); }
if (__movejRx_R[3]) { __AppendToList(__movejRx_HaltList,3); }
if (__movejRx_R[4]) { __AppendToList(__movejRx_HaltList,4); }
if (__movejRx_R[5]) { __AppendToList(__movejRx_HaltList,5); }
if (__movejRx_R[6]) { __AppendToList(__movejRx_HaltList,6); }
if (__movejRx_R[7]) { __AppendToList(__movejRx_HaltList,7); }
if (__movejRx_R[8]) { __AppendToList(__movejRx_HaltList,8); }
if (__movejRx_R[9]) { __AppendToList(__movejRx_HaltList,9); }
if (!E[11]) { __AppendToList(__movejRx_HaltList,0); }
__ResetModuleEntryAfterReaction();
__movejRx_ModuleData.awaited_list = __movejRx_AllAwaitedList;
__movejRx__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int movejRx_reset () {
__movejRx_ModuleData.awaited_list = __movejRx_AwaitedList;
__ResetModuleEntry();
__movejRx_ModuleData.awaited_list = __movejRx_AllAwaitedList;
__movejRx_ModuleData.state = 0;
__movejRx_R[0] = _true;
__movejRx_R[1] = _false;
__movejRx_R[2] = _false;
__movejRx_R[3] = _false;
__movejRx_R[4] = _false;
__movejRx_R[5] = _false;
__movejRx_R[6] = _false;
__movejRx_R[7] = _false;
__movejRx_R[8] = _false;
__movejRx_R[9] = _false;
__movejRx__reset_input();
return 0;
}
char* CompilationType = "Compiled Sorted Equations";

int __NumberOfModules = 1;
struct __ModuleEntry* __ModuleTable[] = {
&__movejRx_ModuleData
};
