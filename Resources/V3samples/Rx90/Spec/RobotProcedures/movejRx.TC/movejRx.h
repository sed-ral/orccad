/*--------------------------------------
 * definitions for simulation purposes 
 *--------------------------------------
 */
typedef void* orcStrlPtr;
#define _orcStrlPtr(x,y)(*(x)=y)
#define _check_orcStrlPtr _check_integer
#define _text_to_orcStrlPtr  _text_to_integer
#define _orcStrlPtr_to_text _integer_to_text
