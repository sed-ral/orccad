// Ilv Version: 2.4
// File generated: Thu Sep 17 16:36:36 1998
// Creator class: IlvGraphOutput
Palettes 6
3 "blue" "blue" "default" "9x15" 0 solid solid 0 0 0
1 0 0 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
4 0 49087 65535 16448 57568 53456 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 5 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 11
3 0 { 0 0 OrcIlvModAtr 2
4 IlvFilledRectangle 110 70 200 240 
5 IlvMessageLabel 110 70 201 241 0 "jatr" 16 [Box
0 1 1 jatr 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 100 180 20 20 2 
2 IlvMessageLabel 122 176 76 28 0 "endtraj" 1 0
[Port
0 1 1 endtraj 110 190 122 176 76 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 100 260 20 20 2 
2 IlvMessageLabel 122 256 86 28 0 "endtime" 1 0
[Port
0 1 2 endtime 110 270 122 256 86 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 240 60 20 20 2 
2 IlvMessageLabel 229 75 86 28 0 "trackerr" 1 0
[Port
0 1 3 trackerr 250 70 229 75 86 28 2  2 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 300 260 20 20 2 
2 IlvMessageLabel 252 256 92 28 0 "jointlimit" 1 0
[Port
0 1 4 jointlimit 310 270 252 256 92 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 100 100 20 20 2 
2 IlvMessageLabel 122 96 90 28 0 "goodtraj" 1 0
[Port
0 1 5 goodtraj 110 110 122 96 90 28 1  1 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
