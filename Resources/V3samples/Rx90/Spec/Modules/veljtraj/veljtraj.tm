// Ilv Version: 2.4
// File generated: Wed Sep 16 14:42:51 1998
// Creator class: IlvGraphOutput
Palettes 7
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
5 0 49087 65535 45232 50372 57054 "default" "9x15" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 0 49087 65535 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 6 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 65535 0 0 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
4 0 49087 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
IlvObjects 13
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 120 70 180 240 
6 IlvMessageLabel 120 70 181 241 0 "veljtraj" 16 [Box
0 1 1 veljtraj 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 290 110 20 20 
2 IlvMessageLabel 256 106 64 28 0 "posjD" 1 0
[Port
0 1 1 posjD 300 120 256 106 64 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 150 300 20 20 2 
2 IlvMessageLabel 142 281 72 28 0 "endTR" 1 0
[Port
0 1 2 endTR 160 310 142 281 72 28 8  8 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 260 300 20 20 2 
2 IlvMessageLabel 250 281 82 28 0 "endPrR" 1 0
[Port
0 1 3 endPrR 270 310 250 281 82 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortPrm 2
4 IlvPolyline 11
232 62 248 78 240 70 248 62 232 78 240 70 248 70 232 70 240 70 240 62
240 78 
2 IlvMessageLabel 230 75 42 28 0 "end" 1 0
[Port
0 1 4 end 240 70 230 75 42 28 2  2 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortPrm 2
4 IlvPolyline 11
152 62 168 78 160 70 168 62 152 78 160 70 168 70 152 70 160 70 160 62
160 78 
2 IlvMessageLabel 146 75 58 28 0 "veljD" 1 0
[Port
0 1 5 veljD 160 70 146 75 58 28 2  2 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortVar 2
4 IlvFilledEllipse 110 240 20 20 
2 IlvMessageLabel 132 236 46 28 0 "posj" 1 0
[Port
0 1 6 posj 120 250 132 236 46 28 1  1 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
