// Orccad Version: 3.0 alpha   
// Module : veljtraj
// Include and Definition File
// Date of creation : Wed Sep 16 11:01:57 1998

// Utilities
#define RADTODEG(rad)  ((rad)*(180./3.14159265358979323846))
#define DEGTORAD(deg)  ((deg)*(3.14159265358979323846/180.))
#define Abs(a)         (((a) < 0) ? -(a) : (a))
#define Sign(a)        (((a) < 0) ? -(1.0) : (1.0))

// Axis limits minus five degrees...
#define J1bMAX  DEGTORAD(155)
#define J2bMAX  DEGTORAD(130)
#define J3bMAX  DEGTORAD(135)
#define J4bMAX  DEGTORAD(265)
#define J5bMAX  DEGTORAD(95)
#define J6bMAX  DEGTORAD(265)
#define J1bMIN  (- J1bMAX)
#define J2bMIN  (- J2bMAX)
#define J3bMIN  (- J3bMAX)
#define J4bMIN  (- J4bMAX)
#define J5bMIN  (- DEGTORAD(85))
#define J6bMIN  (- J6bMAX)

//Definition
#define OLD_VALUE 1000.0
#define TIMEOUT   2.0
// in rad/s
#define VMAX      0.5

typedef enum {END_NO, END_TR, END_PRR} END_TYPE;

