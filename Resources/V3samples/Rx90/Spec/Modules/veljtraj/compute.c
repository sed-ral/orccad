// Orccad Version: 3.0 alpha   
// Module : veljtraj
// Computation File 
// Date of creation : Wed Sep 16 11:01:57 1998
// Date of Modif 1  : Mon Jun 21 18:01:13 1999:
//                    Reduce the limits on axis 5

// Verification
if ( (posj[0]<J1bMIN) || (posj[0]>J1bMAX) || 
     (posj[1]<J2bMIN) || (posj[1]>J2bMAX) || 
     (posj[2]<J3bMIN) || (posj[2]>J3bMAX) || 
     (posj[3]<J4bMIN) || (posj[3]>J4bMAX) || 
     (posj[4]<J5bMIN) || (posj[4]>J5bMAX) || 
     (posj[5]<J6bMIN) || (posj[5]>J6bMAX) )
{
  // Final position is out of bound
  if (endPrR == NO_EVENT){
    endPrR = SET_EVENT;
    printf("veljRx:: Joint Limit Error");
    return;
  }
}

switch(P_end){
default:
case END_NO:
    break;
case END_TR:
    if (endTR == NO_EVENT)
	endTR = SET_EVENT;
    break;
case END_PRR:
    if (endPrR == NO_EVENT)
	endPrR = SET_EVENT;
    break;
};

// Peak up parameters
if (P_veljD[0]==OLD_VALUE){
    // Parameters are not updated
    timeout += dt;
    if (timeout >= TIMEOUT){
	// Print message only one time
	if (timeout < (TIMEOUT+dt) )
	    cout <<"veljRx:: Parameters timeout (=> v=0 )"<<endl;
	for(i=0;i<6;i++)
	    veljDlocal[i]= 0.0;
    }
}
else {
    // Parameters are updated
    timeout = 0.0; 
    for(i=0;i<6;i++){
	// velocities saturation
	if (Abs(P_veljD[i]) < VMAX)
	    veljDlocal[i] = P_veljD[i];
	else {
	    cout <<"veljRx:: Saturation on joint = "<<i<<endl;
	    veljDlocal[i] = Sign(P_veljD[i])*VMAX;
	}
    }
//    cout <<" "<<veljDlocal[0]<<endl;
    P_veljD[0] = OLD_VALUE;
}

// Compute joints position
for(i=0;i<6;i++)
    posjD[i] += veljDlocal[i] * dt ;

