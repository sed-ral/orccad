// Orccad Version: 3.0 alpha   
// Module : jtraj
// Computation File 
// Date of creation : Wed May  7 08:14:22 1997

// Compute parameter

// Take in account a future point ?


count ++;

if ((TrajTyp>NONDEF_TRAJ)&&(TrajTyp<=POLY_CACC_TRAJ_NRZ))
{
  if (Bank==0)
    cout <<"DBG::Bank=1:"<<time<<" "<<count<<endl;
  Bank=1;
}

// Compute the trajectory
if (tf > 0.0)
    r1 = time/tf;
else
    r1 = 0.0;

switch (trajtype){
case POLY_CPOS_TRAJ:
    r = r1; 
    // Modif RPG/AE 17/04/03
    alpha=beta=0.0;
    break;
case POLY_CVEL_TRAJ:
    r2 = r1*r1;
    r = 3*r2 -2*r1*r2;
    // Modif RPG/AE 17/04/03
    alpha=beta=0.0;
    break;
case POLY_CACC_TRAJ:
    r2 = r1*r1;
    r3 = r2*r1;
    r4 = r3*r1;
    r =  10*r3 - 15*r4 + 6*r1*r4; 
    // Modif RPG/AE 17/04/03
    alpha=beta=0.0;
    break; 
// Modif RPG/AE 17/04/03
 case POLY_CVEL_TRAJ_NRZ:
    r2= r1*r1;
    r = 3*r2 - 2*r1*r2;
    alpha = (r1-1)*r2*tf;
    beta  = (r2- 2*r1+1)*r1*tf;
   break;
 case POLY_CACC_TRAJ_NRZ:
    r2= r1*r1;
    r3= r2*r1;
    r4 = r3*r1;
    r =  10*r3 - 15*r4 + 6*r1*r4;
    alpha = (7*r1 - 3*r2 -4)*r3*tf;
    beta  = (8*r3 -6*r2-3*r4+1)*r1*tf;
   break;
default:
    r = 0.0;
    alpha=beta=0.0;
}

// Compute next points

for(i=0;i<6;i++){
// Modif RPG/AE 17/04/03 add alpha and beta
     posD[i] = posi[i] + r*D[i] + alpha*velf[i] + beta*veli[i];
}

/*
if (count==250)
{
  count=0;
  cout <<"Bank "<<endl;
}
else count++;
*/

// Time update

if (time >= tf){
    // End is required ? 
   if (TrajQ[0]==T_END){
    // CASE 1 = for the next step, a robot end is required
     if (endtraj==NO_EVENT)
	 endtraj=SET_EVENT;
     time = tf;
     // Modif RPG/AE 17/04/03
     for(i=0;i<6;i++)
       velf[i] = 0.0;
   }
   else {
    if ((TrajQ[0]==posf[0])&&(TrajQ[1]==posf[1])&&
	(TrajQ[2]==posf[2])&&(TrajQ[3]==posf[3])&&
	(TrajQ[4]==posf[4])&&(TrajQ[5]==posf[5])){
      // CASE 2 = for the next step, a end trajectory is required
	time = tf;
    }
    else{
      // CASE 3 = for the next step, a new trajectory is required
       trajtype = TrajTyp;
      if ((trajtype<=NONDEF_TRAJ)||(trajtype>POLY_CACC_TRAJ_NRZ))
	{
	   // The type of trajectory required is not valid
	  endtraj=SET_EVENT;
	  // Bug on precondition ?? (NO_EVENT -> SET_EVENT)
	  goodtraj=SET_EVENT;   	
	  cout <<"movejRx:: Trajectory Type Error"<<endl;
	}
     
      
	time = 0.0;
	Dmax = r=r1=r2=r3=r4=alpha=beta=0.0;	
	for(i=0;i<6;i++){
	    //BUG WITH posI !!!! TO SEE
	    posi[i] = posf[i];
	    // Modif RPG/AE 17/04/03
	    veli[i] = velf[i];
	    velf[i] =  TrajQ[i+6];
	    posf[i] = TrajQ[i];
	    D[i] = posf[i] - posi[i]; 
	    if (Abs(D[i]) > Dmax)
		Dmax = Abs(D[i]);
	}
	
	// Position and Velocity Verification
	if ( (posf[0]<J1MIN) || (posf[0]>J1MAX) || 
	     (posf[1]<J2MIN) || (posf[1]>J2MAX) || 
	     (posf[2]<J3MIN) || (posf[2]>J3MAX) || 
	     (posf[3]<J4MIN) || (posf[3]>J4MAX) || 
	     (posf[4]<J5MIN) || (posf[4]>J5MAX) || 
	     (posf[5]<J6MIN) || (posf[5]>J6MAX) )
	    {
		// Final position is out of bound
		endtraj=SET_EVENT; 
		cout <<"movejRx:: Trajectory Error"<<endl;	      
		cout <<posf[0]<<" "<<posf[1]<<" "<<posf[2]<<endl;
	    }
	       
	vel =TrajQ[6]; 
        if (trajtype<=POLY_CACC_TRAJ) {
	  if ((vel<VMIN) || (vel>VMAX)) {
	    // Final velocity is out of bound
	      endtraj=SET_EVENT; 
	      cout <<"movejRx:: Trajectory Velocity Error"<<endl;
	  }
	  else
	    {
	      // Compute final time
	      tf = Dmax/vel;
	      if (tf< TMIN)
		tf = TMIN;
	    }
	}
	else
	  if ((velf[0]<VQMIN) || (velf[0]>VQMAX) || 
	     (velf[1]<VQMIN) || (velf[1]>VQMAX) || 
	     (velf[2]<VQMIN) || (velf[2]>VQMAX) || 
	     (velf[3]<VQMIN) || (velf[3]>VQMAX) || 
	     (velf[4]<VQMIN) || (velf[4]>VQMAX)|| 
	     (velf[5]<VQMIN) || (velf[5]>VQMAX) )
	    {
	      // Final position is out of bound
	      endtraj=SET_EVENT; 
	      cout <<"movejRx:: Trajectory Velocity Error"<<endl;
	    }
	  else {
	    tf=TrajQ[12];
	    if (tf<0.0)
	      {
	      // Time is out of bound
	      endtraj=SET_EVENT; 
	      cout <<"movejRx:: Trajectory Time Error"<<endl;
	    }
	  }
    }
   }

// RAZ Bank
   if (Bank==1)
     {
       cout <<"DBG::Bank=0:"<<time<<" "<<count<<endl;
     }
   Bank=0;
   TrajTyp=NONDEF_TRAJ;
   if (Param->set("TypTraj", INTEGER,1,1,(char*)&(TrajTyp)) == ERROR) 
     {
       EventStorage->displayError(RT_ERROR2,0.0);
       PrR->Reset();
     }
}
else 
{
// CASE 4 = for the next step, keep the trajectory
    time += dt;
}
