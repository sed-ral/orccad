// Orccad Version: 3.0 alpha   
// Module : jtraj
// Include and Definition File
// Date of creation : Wed May  7 08:14:22 1997

#define RADTODEG(rad)  ((rad)*(180./3.14159265358979323846))
#define DEGTORAD(deg)  ((deg)*(3.14159265358979323846/180.))
#define Abs(a)         (((a) < 0) ? -(a) : (a))

#define J1MAX  DEGTORAD(159.5)
#define J2MAX  DEGTORAD(137.0)
#define J3MAX  DEGTORAD(142.0)
#define J4MAX  DEGTORAD(269.5)
#define J5MAX  DEGTORAD(100.5)
#define J6MAX  DEGTORAD(269.5)
#define J1MIN  (- J1MAX)
#define J2MIN  (- J2MAX)
#define J3MIN  (- J3MAX)
#define J4MIN  (- J4MAX)
#define J5MIN  (- DEGTORAD(90.5))
#define J6MIN  (- J6MAX)

#define VMIN  0.02  // in rad/s
#define VMAX  0.5
#define VDEF  0.2
#define VQMIN 0.0
#define VQMAX 0.5   
#define TMIN  0.01  // Tmin
#define T_END 1000.0

typedef enum{
    NONDEF_TRAJ,
    POLY_CPOS_TRAJ,
    POLY_CVEL_TRAJ,
    POLY_CACC_TRAJ,
      // Modif RPG/AE 17/04/03
    POLY_CVEL_TRAJ_NRZ,
    POLY_CACC_TRAJ_NRZ} TrajectoryType;

