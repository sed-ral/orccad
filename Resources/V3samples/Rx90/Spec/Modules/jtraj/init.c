// Orccad Version: 3.0 alpha
// Module : jtraj
// Initialisation File
// Date of creation : Wed May  7 08:14:22 1997

cout <<"I-> "<<RADTODEG(posI[0])<<" "<<RADTODEG(posI[1])<<"  "
             <<RADTODEG(posI[2])<<" "<<RADTODEG(posI[3])<<" "
             <<RADTODEG(posI[4])<<"  "<<RADTODEG(posI[5])<<endl;
     
//
//Trajectory Specification
//

for(i=0;i<6;i++){
    posi[i] = posf[i] = TrajQ[i] = posI[i];
    // Modif RPG/AE 17/04/03
    veli[i]=0.0;
    velf[i]=TrajQ[i+6];
    D[i]    = 0.0;
}

// Copy the velocity max trajectory
vel = TrajQ[6];


// Modif RPG/AE 17/04/03 - copy the trajectory type && verify it
trajtype = TrajTyp;
Bank= 0;
TrajTyp=NONDEF_TRAJ;
if (Param->set("TypTraj", INTEGER,1,1,(char*)&(TrajTyp)) == ERROR) 
{
  EventStorage->displayError(RT_ERROR2,0.0);
  PrR->Reset();
}

if ( (trajtype <=NONDEF_TRAJ) || (trajtype>POLY_CACC_TRAJ_NRZ) )
{
   // The type of trajectory required is not valid
    endtraj=SET_EVENT;
    // Bug on precondition ?? (NO_EVENT -> SET_EVENT)
    goodtraj=SET_EVENT;   	
    cout <<"movejRx:: Trajectory Type Error"<<endl;
}


//
// Other parameters
//
Dmax=r=r1=r2=r3=r4=0.0;
time = tf = 0.0;
dt   = getSampleTime();

// Modif RPG/AE 17/04/03
alpha=beta=0.0;

//
// Verification
//
if ( (posf[0]<J1MIN) || (posf[0]>J1MAX) || 
     (posf[1]<J2MIN) || (posf[1]>J2MAX) || 
     (posf[2]<J3MIN) || (posf[2]>J3MAX) || 
     (posf[3]<J4MIN) || (posf[3]>J4MAX) || 
     (posf[4]<J5MIN) || (posf[4]>J5MAX)|| 
     (posf[5]<J6MIN) || (posf[5]>J6MAX) ||
     (vel <VMIN)     || (vel > VMAX) )
{
    // Final position is out of bound
    endtraj=SET_EVENT;
    // Bug on precondition ?? (NO_EVENT -> SET_EVENT)
    goodtraj=SET_EVENT;   	
    cout <<"movejRx:: Trajectory Error"<<endl;
}
else
{
    // Good final position
    endtraj=NO_EVENT;
    goodtraj=SET_EVENT;
}
     

count=0;
