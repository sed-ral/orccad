// Ilv Version: 3.1
// File generated: Fri Apr 18 17:23:36 2003
// Creator class: IlvGraphOutput
Palettes 10
7 "deepskyblue" "red" "default" "9x15" 0 solid solid 0 0 0
4 0 49087 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 9 "deepskyblue" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 0 49087 65535 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
8 0 49087 65535 45232 50372 57054 "default" "9x15" 0 solid solid 0 0 0
5 "deepskyblue" "blue" "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
3 65535 0 0 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
6 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 15
3 0 { 0 0 OrcIlvModAlgo 2
8 IlvFilledRectangle 100 70 160 190 
9 IlvMessageLabel 100 70 161 191 F8 0 1 16 4 "jtraj"   [Box
0 1 1 jtraj 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 250 110 20 20 
2 IlvMessageLabel 219 106 58 28 F8 0 25 16 4 "posD"   0
[Port
0 1 1 posD 260 120 219 106 58 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 90 100 20 20 2 
2 IlvMessageLabel 112 96 76 28 F8 0 25 16 4 "endtraj"   0
[Port
0 1 2 endtraj 100 110 112 96 76 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 90 210 20 20 2 
2 IlvMessageLabel 112 206 90 28 F8 0 25 16 4 "goodtraj"   0
[Port
0 1 3 goodtraj 100 220 112 206 90 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
4 IlvFilledEllipse 200 250 20 20 
2 IlvMessageLabel 199 231 46 28 F8 0 25 16 4 "posI"   0
[Port
0 1 4 posI 210 260 199 231 46 28 8  8 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortPrm 2
4 IlvPolyline 11
200 60 216 76 208 68 216 60 200 76 208 68 216 68 200 68 208 68 208 60
208 76 
2 IlvMessageLabel 193 73 62 28 F8 0 25 16 4 "TrajQ"   0
[Port
0 1 5 TrajQ 208 68 193 73 62 28 2  2 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortPrm 2
5 IlvPolyline 11
140 60 156 76 148 68 156 60 140 76 148 68 156 68 140 68 148 68 148 60
148 76 
6 IlvMessageLabel 127 73 84 28 F8 0 25 16 4 "TrajTyp"   0
[Port
0 1 6 TrajTyp 148 68 127 73 84 28 2  2 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortVar 2
7 IlvFilledEllipse 250 210 20 20 
6 IlvMessageLabel 222 206 56 28 F8 0 25 16 4 "Bank"   0
[Port
0 1 7 Bank 260 220 222 206 56 28 4  4 
Port]

 } 20
1 1 { 14 0 OrcIlvVoidLink 1 0 13 } 26
EOF
