// Ilv Version: 2.4
// File generated: Thu Sep 17 16:22:52 1998
// Creator class: IlvGraphOutput
Palettes 6
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 0 49087 65535 16448 57568 53456 "default" "9x15" 0 solid solid 0 0 0
3 0 0 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 5 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "blue" "blue" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAtr 2
4 IlvFilledRectangle 130 90 160 210 
5 IlvMessageLabel 130 90 161 211 0 "veljatr" 16 [Box
0 1 1 veljatr 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 230 80 20 20 2 
2 IlvMessageLabel 219 95 86 28 0 "trackerr" 1 0
[Port
0 1 1 trackerr 240 90 219 95 86 28 2  2 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 280 250 20 20 2 
2 IlvMessageLabel 232 246 92 28 0 "jointlimit" 1 0
[Port
0 1 2 jointlimit 290 260 232 246 92 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 120 130 20 20 2 
2 IlvMessageLabel 142 126 72 28 0 "endTR" 1 0
[Port
0 1 3 endTR 130 140 142 126 72 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 120 250 20 20 2 
2 IlvMessageLabel 142 246 82 28 0 "endPrR" 1 0
[Port
0 1 4 endPrR 130 260 142 246 82 28 1  1 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
