// Ilv Version: 2.3
// File generated: Wed May  7 18:29:30 1997
// Creator class: IlvGraphOutput
Palettes 7
3 "deepskyblue" "red" "default" "9x15" 0 solid solid 0 0 0
6 "deepskyblue" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "deepskyblue" "blue" "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 "deepskyblue" "lightsteelblue" "default" "9x15" 0 solid solid 0 0 0
4 "red" "red" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 110 80 160 190 
6 IlvMessageLabel 110 80 161 191 0 "jerror" 16 [Box
0 1 1 jerror 
Box]

 } 0
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 110 20 20 
2 IlvMessageLabel 122 106 58 28 0 "posD" 1 0
[Port
0 1 1 posD 110 120 122 106 58 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 220 20 20 
2 IlvMessageLabel 122 216 58 28 0 "posC" 1 0
[Port
0 1 2 posC 110 230 122 216 58 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
3 IlvFilledEllipse 260 110 20 20 
2 IlvMessageLabel 232 106 52 28 0 "error" 1 0
[Port
0 1 3 error 270 120 232 106 52 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 210 260 20 20 2 
2 IlvMessageLabel 199 241 84 28 0 "trackerr" 1 0
[Port
0 1 4 trackerr 220 270 199 241 84 28 8  8 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
