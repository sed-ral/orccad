// Orccad Version: 3.0 alpha   
// Module : jerror
// Computation File 
// Date of creation : Wed May  7 08:16:00 1997

flagok = TRUE;

for (i=0;i<6;i++){
    error[i] = posD[i] - posC[i];
    if ((error[i] > TRACK_ERROR)||(error[i] < - TRACK_ERROR))
	flagok = FALSE;
}

// BEGIN RPG
flagok = TRUE;
for (i=0;i<6;i++)
     error[i] = 0.0;
// END RPG


if ((flagok == FALSE)&&(trackerr == NO_EVENT)){
    trackerr = SET_EVENT;
    cout <<"movejRx:: Tracking Error = "
	 <<error[0] <<" "<<error[1] <<" "<<error[2]<<" "
	 <<error[3] <<" "<<error[4] <<" "<<error[5]<<endl;
}

if (trackerr == SET_EVENT){
    for (i=0;i<6;i++)
	error[i] = 0.0;
}
