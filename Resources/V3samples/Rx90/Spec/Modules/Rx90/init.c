// Orccad Version: 3.0 alpha
// Module : Rx90
// Initialisation Driver File
// Date of creation : Wed May  7 08:08:48 1997

if (rx90Init()==ERROR)
{
    printf("PhR-Rx90:: Init Robot Error");
    jointlimit = SET_EVENT;
}
else
{
    rx90On();
    jointlimit = NO_EVENT;
}
