// Ilv Version: 2.3
// File generated: Wed May  7 08:08:48 1997
// Creator class: IlvGraphOutput
Palettes 7
4 "red" "red" "default" "9x15" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "deepskyblue" "blue" "default" "9x15" 0 solid solid 0 0 0
6 "deepskyblue" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "deepskyblue" "red" "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
5 "deepskyblue" "wheat" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModPr 2
5 IlvFilledRectangle 110 80 180 210 
6 IlvMessageLabel 110 80 181 211 0 "Rx90" 16 [Box
0 1 1 Rx90 
Box]

 } 0
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 100 120 20 20 2 
2 IlvMessageLabel 122 116 70 28 0 "torque" 1 0
[Port
0 1 1 torque 110 130 122 116 70 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 280 120 20 20 2 
2 IlvMessageLabel 255 116 46 28 0 "joint" 1 0
[Port
0 1 2 joint 290 130 255 116 46 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 190 280 20 20 2 
2 IlvMessageLabel 178 261 88 28 0 "jointlimit" 1 0
[Port
0 1 3 jointlimit 200 290 178 261 88 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
