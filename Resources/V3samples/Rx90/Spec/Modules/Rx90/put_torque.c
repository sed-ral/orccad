// Orccad Version: 3.0 alpha
// Module :  put_torque.c
// Call Driver File for variable torque
// Date of creation : Wed May  7 08:08:48 1997

if (jointlimit==NO_EVENT) 
   rx90PutTorque(torque); 
else 
   rx90ClearTorque();
