// Ilv Version: 2.3
// File generated: Sat Aug  9 11:28:59 1997
// Creator class: IlvGraphOutput
Palettes 6
4 "deepskyblue" "lightsteelblue" "default" "9x15" 0 solid solid 0 0 0
5 "deepskyblue" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "red" "red" "default" "9x15" 0 solid solid 0 0 0
1 "deepskyblue" "red" "default" "9x15" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 130 90 160 190 
5 IlvMessageLabel 130 90 161 191 0 "gtc" 16 [Box
0 1 1 gtc 
Box]

 } 0
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 280 120 20 20 
2 IlvMessageLabel 264 116 28 28 0 "Td" 1 0
[Port
0 1 1 Td 290 130 264 116 28 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 200 270 20 20 2 
2 IlvMessageLabel 195 251 60 28 0 "velok" 1 0
[Port
0 1 2 velok 210 280 195 251 60 28 8  8 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
