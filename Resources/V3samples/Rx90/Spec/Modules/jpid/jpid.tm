// Ilv Version: 2.3
// File generated: Wed May  7 08:17:49 1997
// Creator class: IlvGraphOutput
Palettes 6
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "deepskyblue" "blue" "default" "9x15" 0 solid solid 0 0 0
5 "deepskyblue" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "deepskyblue" "lightsteelblue" "default" "9x15" 0 solid solid 0 0 0
3 "deepskyblue" "red" "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 120 80 160 190 
5 IlvMessageLabel 120 80 161 191 0 "jpid" 16 [Box
0 1 1 jpid 
Box]

 } 0
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 110 120 20 20 
2 IlvMessageLabel 132 116 52 28 0 "error" 1 0
[Port
0 1 1 error 120 130 132 116 52 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 270 120 20 20 
2 IlvMessageLabel 233 116 70 28 0 "torque" 1 0
[Port
0 1 2 torque 280 130 233 116 70 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
