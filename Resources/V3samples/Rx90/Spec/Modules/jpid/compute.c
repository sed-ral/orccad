// Orccad Version: 3.0 alpha   
// Module : jpid
// Computation File 
// Date of creation : Wed May  7 08:17:48 1997

for (i = 0 ; i<6; i++)
{

// Compute error velocity
de[i] = 0.4 * de[i] + 0.6 * (error[i] - errold[i]) / dt;

// Compute integral error
ie[i] += error[i];
// Integral Saturation
if (ie[i] > INTEGR_MAX)
    ie[i] = INTEGR_MAX;
else
if (ie[i] < - INTEGR_MAX)
    ie[i] = - INTEGR_MAX;

// Compute Torque
torque[i] = kp[i]*error[i] + kd[i]*de[i] + ki[i]*ie[i];

// Torque Saturation
if (torque[i] > TORQUE_MAX)
    torque[i] = TORQUE_MAX;
else
if (torque[i] < - TORQUE_MAX)
    torque[i] = - TORQUE_MAX;

errold[i] = error[i];

}
