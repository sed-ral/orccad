// Orccad Version: 3.0 alpha
// Module : jpid
// Initialisation File
// Date of creation : Wed May  7 08:17:48 1997

kp[0] = KP1;
kp[1] = KP2;
kp[2] = KP3;
kp[3] = KP4;
kp[4] = KP5;
kp[5] = KP6;

kd[0] = KD1;
kd[1] = KD2;
kd[2] = KD3;
kd[3] = KD4;
kd[4] = KD5;
kd[5] = KD6;

ki[0] = KI1;
ki[1] = KI2;
ki[2] = KI3;
ki[3] = KI4;
ki[4] = KI5;
ki[5] = KI6;

dt = getSampleTime();

for(i=0;i<6;i++){
     de[i] = ie[i] = torque[i] = errold[i] = 0.0;
    
}
