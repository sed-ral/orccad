// Orccad Version: 3.0 alpha   
// Module : jpid
// Include and Definition File
// Date of creation : Wed May  7 08:17:48 1997

#define KP1 365.0
#define KP2 160.0
#define KP3  78.0
#define KP4  62.0
#define KP5  80.0
#define KP6  16.0

#define KD1  6.0
#define KD2 10.0
#define KD3  1.8
#define KD4  2.0
#define KD5  1.0
#define KD6  0.2

#define KI1 4.0
#define KI2 4.0
#define KI3 3.0
#define KI4 3.0
#define KI5 3.0
#define KI6 3.0

#define INTEGR_MAX 0.05
#define TORQUE_MAX 1000.0




