// Ilv Version: 2.3
// File generated: Sun Aug 10 11:12:07 1997
// Creator class: IlvGraphOutput
Palettes 7
6 "deepskyblue" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "deepskyblue" "red" "default" "9x15" 0 solid solid 0 0 0
4 "red" "red" "default" "9x15" 0 solid solid 0 0 0
5 "deepskyblue" "lightsteelblue" "default" "9x15" 0 solid solid 0 0 0
1 "deepskyblue" "blue" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 110 80 170 190 
6 IlvMessageLabel 110 80 171 191 0 "ijrx" 16 [Box
0 1 1 ijrx 
Box]

 } 0
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 110 20 20 
2 IlvMessageLabel 122 106 28 28 0 "Tc" 1 0
[Port
0 1 1 Tc 110 120 122 106 28 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 220 20 20 
2 IlvMessageLabel 122 216 54 28 0 "Joint" 1 0
[Port
0 1 2 Joint 110 230 122 216 54 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
3 IlvFilledEllipse 270 110 20 20 
2 IlvMessageLabel 248 106 40 28 0 "velj" 1 0
[Port
0 1 3 velj 280 120 248 106 40 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 220 260 20 20 2 
2 IlvMessageLabel 218 241 50 28 0 "Sing" 1 0
[Port
0 1 4 Sing 230 270 218 241 50 28 8  8 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
