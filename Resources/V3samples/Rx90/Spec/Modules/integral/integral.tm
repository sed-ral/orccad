// Ilv Version: 2.3
// File generated: Sat Aug  9 11:26:03 1997
// Creator class: IlvGraphOutput
Palettes 6
4 "deepskyblue" "lightsteelblue" "default" "9x15" 0 solid solid 0 0 0
3 "deepskyblue" "blue" "default" "9x15" 0 solid solid 0 0 0
5 "deepskyblue" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "deepskyblue" "red" "default" "9x15" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 110 90 190 190 
5 IlvMessageLabel 110 90 191 191 0 "integral" 16 [Box
0 1 1 integral 
Box]

 } 0
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 290 120 20 20 
2 IlvMessageLabel 268 116 40 28 0 "pos" 1 0
[Port
0 1 1 pos 300 130 268 116 40 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 100 120 20 20 
2 IlvMessageLabel 122 116 34 28 0 "vel" 1 0
[Port
0 1 2 vel 110 130 122 116 34 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
