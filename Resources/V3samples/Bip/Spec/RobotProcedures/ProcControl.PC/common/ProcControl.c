/* sscc : C CODE OF SORTED EQUATIONS ProcControl - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __ProcControl_GENERIC_TEST(TEST) return TEST;
typedef void (*__ProcControl_APF)();
static __ProcControl_APF *__ProcControl_PActionArray;

#include "ProcControl.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _bipControl_controler_DEFINED
#ifndef bipControl_controler
extern void bipControl_controler(orcStrlPtr);
#endif
#endif
#ifndef _bipControl_fileparameter_DEFINED
#ifndef bipControl_fileparameter
extern void bipControl_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _bipControl_parameter_DEFINED
#ifndef bipControl_parameter
extern void bipControl_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __ProcControl_V0;
static boolean __ProcControl_V1;
static orcStrlPtr __ProcControl_V2;
static boolean __ProcControl_V3;
static boolean __ProcControl_V4;
static boolean __ProcControl_V5;
static boolean __ProcControl_V6;


/* INPUT FUNCTIONS */

void ProcControl_I_START_ProcControl () {
__ProcControl_V0 = _true;
}
void ProcControl_I_Abort_Local_ProcControl () {
__ProcControl_V1 = _true;
}
void ProcControl_I_bipControl_Start (orcStrlPtr __V) {
_orcStrlPtr(&__ProcControl_V2,__V);
__ProcControl_V3 = _true;
}
void ProcControl_I_USERSTART_bipControl () {
__ProcControl_V4 = _true;
}
void ProcControl_I_BF_bipControl () {
__ProcControl_V5 = _true;
}
void ProcControl_I_T3_bipControl () {
__ProcControl_V6 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __ProcControl_A1 \
__ProcControl_V0
#define __ProcControl_A2 \
__ProcControl_V1
#define __ProcControl_A3 \
__ProcControl_V3
#define __ProcControl_A4 \
__ProcControl_V4
#define __ProcControl_A5 \
__ProcControl_V5
#define __ProcControl_A6 \
__ProcControl_V6

/* OUTPUT ACTIONS */

#define __ProcControl_A7 \
ProcControl_O_BF_ProcControl()
#define __ProcControl_A8 \
ProcControl_O_STARTED_ProcControl()
#define __ProcControl_A9 \
ProcControl_O_GoodEnd_ProcControl()
#define __ProcControl_A10 \
ProcControl_O_Abort_ProcControl()
#define __ProcControl_A11 \
ProcControl_O_T3_ProcControl()
#define __ProcControl_A12 \
ProcControl_O_START_bipControl()
#define __ProcControl_A13 \
ProcControl_O_Abort_Local_bipControl()

/* ASSIGNMENTS */

#define __ProcControl_A14 \
__ProcControl_V0 = _false
#define __ProcControl_A15 \
__ProcControl_V1 = _false
#define __ProcControl_A16 \
__ProcControl_V3 = _false
#define __ProcControl_A17 \
__ProcControl_V4 = _false
#define __ProcControl_A18 \
__ProcControl_V5 = _false
#define __ProcControl_A19 \
__ProcControl_V6 = _false

/* PROCEDURE CALLS */

#define __ProcControl_A20 \
bipControl_parameter(__ProcControl_V2)
#define __ProcControl_A21 \
bipControl_controler(__ProcControl_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __ProcControl_A22 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int ProcControl_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __ProcControl__reset_input () {
__ProcControl_V0 = _false;
__ProcControl_V1 = _false;
__ProcControl_V3 = _false;
__ProcControl_V4 = _false;
__ProcControl_V5 = _false;
__ProcControl_V6 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __ProcControl_R[4] = {_true,_false,_false,_false};

/* AUTOMATON ENGINE */

int ProcControl () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = (__ProcControl_R[2]&&!(__ProcControl_R[0]));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__ProcControl_A5));
E[2] = (__ProcControl_R[2]||__ProcControl_R[3]);
E[3] = (__ProcControl_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__ProcControl_A3)));
if (E[3]) {
__ProcControl_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcControl_A22\n");
#endif
}
E[4] = (__ProcControl_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ProcControl_A1));
E[5] = (__ProcControl_R[1]&&!(__ProcControl_R[0]));
E[6] = (E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ProcControl_A1));
E[6] = (E[4]||E[6]);
if (E[6]) {
__ProcControl_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcControl_A20\n");
#endif
}
if (E[6]) {
__ProcControl_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcControl_A21\n");
#endif
}
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__ProcControl_A5)));
E[4] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__ProcControl_A6)));
E[4] = (E[6]||((__ProcControl_R[2]&&E[4])));
E[7] = (((E[2]&&!(__ProcControl_R[2])))||E[4]);
E[8] = (E[7]||E[1]);
E[9] = (__ProcControl_R[3]&&!(__ProcControl_R[0]));
E[10] = (E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__ProcControl_A2)));
E[10] = (E[6]||((__ProcControl_R[3]&&E[10])));
E[11] = (((E[2]&&!(__ProcControl_R[3])))||E[10]);
E[1] = ((E[1]&&E[8])&&E[11]);
if (E[1]) {
__ProcControl_A7;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcControl_A7\n");
#endif
}
if (E[6]) {
__ProcControl_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcControl_A8\n");
#endif
}
if (E[1]) {
__ProcControl_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcControl_A9\n");
#endif
}
E[9] = (E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__ProcControl_A2));
if (E[9]) {
__ProcControl_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcControl_A10\n");
#endif
}
E[0] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__ProcControl_A6));
E[8] = (E[8]||E[0]);
E[0] = ((E[0]&&E[8])&&E[11]);
if (E[0]) {
__ProcControl_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcControl_A11\n");
#endif
}
if (E[6]) {
__ProcControl_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcControl_A12\n");
#endif
}
if (E[9]) {
__ProcControl_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcControl_A13\n");
#endif
}
E[8] = ((E[9]&&E[8])&&((E[11]||E[9])));
E[12] = ((((E[1]||E[0]))||E[8]));
E[13] = (__ProcControl_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ProcControl_A1)));
E[5] = (E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ProcControl_A1)));
E[5] = (E[13]||((__ProcControl_R[1]&&E[5])));
E[11] = (((((((E[4]||E[10]))&&E[7])&&E[11]))||E[5]));
E[2] = (E[2]||__ProcControl_R[1]);
E[8] = (((((E[8]||E[0]))||E[1])||E[0])||E[8]);
__ProcControl_R[2] = (E[4]&&!(E[8]));
__ProcControl_R[3] = (E[10]&&!(E[8]));
__ProcControl_R[0] = !(_true);
__ProcControl_R[1] = E[5];
__ProcControl__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int ProcControl_reset () {
__ProcControl_R[0] = _true;
__ProcControl_R[1] = _false;
__ProcControl_R[2] = _false;
__ProcControl_R[3] = _false;
__ProcControl__reset_input();
return 0;
}
