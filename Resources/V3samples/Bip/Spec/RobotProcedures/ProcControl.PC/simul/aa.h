/*--------------------------------------
 * definitions for simulation purposes 
 *--------------------------------------
 */
void bipDep2_parameter() {}
void bipDep2_fileparameter() {}
void bipDep2_controler() {}
void bipControl_parameter() {}
void bipControl_fileparameter() {}
void bipControl_controler() {}
typedef void* orcStrlPtr;
#define _orcStrlPtr(x,y)(*(x)=y)
#define _check_orcStrlPtr _check_integer
#define _text_to_orcStrlPtr  _text_to_integer
#define _orcStrlPtr_to_text _integer_to_text
#define _NO_PROCEDURE_DEFINITIONS
