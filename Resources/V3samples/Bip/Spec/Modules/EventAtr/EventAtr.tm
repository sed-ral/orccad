// Ilv Version: 3.1
// File generated: Fri Jul 16 15:36:52 2004
// Creator class: IlvGraphOutput
Palettes 5
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "turquoise" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 4 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAtr 2
3 IlvFilledRectangle 50 70 240 190 
4 IlvMessageLabel 50 70 241 191 F8 0 1 16 4 "EventAtr"   [Box
0 1 1 EventAtr 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 40 190 20 20 2 
2 IlvMessageLabel 60 186 118 28 F8 0 25 16 4 "SocketFail"   0
[Port
0 1 1 SocketFail 50 200 60 186 118 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 40 120 20 20 2 
2 IlvMessageLabel 60 116 86 28 F8 0 25 16 4 "EndTraj"   0
[Port
0 1 2 EndTraj 50 130 60 116 86 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 280 120 20 20 2 
2 IlvMessageLabel 239 116 82 28 F8 0 25 16 4 "DrvFail"   0
[Port
0 1 3 DrvFail 290 130 239 116 82 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 280 200 20 20 2 
2 IlvMessageLabel 215 196 130 28 F8 0 25 16 4 "SockInitFail"   0
[Port
0 1 4 SockInitFail 290 210 215 196 130 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
EOF
