// Orccad Version : 3.2
// Module : Controller
// Include and Definition File
// Date of creation : Thu Jul 15 17:35:12 2004

#include "Model/bipJoints.h"
#include "Exec/bipComLayer.h"
#include "drvBip.h"
#include "bipInitPhR.h"

#define PORT 			 5001
#define BIP_BUF_SIZE  		10000
#define BIP_DEF_RECEIVE_TIMEOUT   100

#ifndef ERROR
#define ERROR -1
#endif

#ifndef OK
#define OK 0
#endif 

extern int *nTab;
extern int nCptFin;
extern ssySockId_t sockClientFd;
