// Orccad Version: 3.2
// Module : Controller
// Computation File 
// 
// Module Ports :
// 	output DOUBLE CurrentO[15]
// 	input DOUBLE CurrentObsI[15]
// 	input DOUBLE FeetI[6]
// 	input DOUBLE EncodersI[15]
// 	input DOUBLE GaugesI[8]
// 	input DOUBLE PotarsI[15]
// 	output EVENT SocketFailE
// 	output EVENT EndTrajE
// 
// 
// Date of creation : Thu Jul 15 17:35:12 2004

if ( (Mt_ptr->GetRobotTaskPtr()->GetState() != INITSTATE) && 
     (Mt_ptr->GetRobotTaskPtr()->GetState() != NOCMDACTIV)) {
     
  for(i=0;i<BIP_DEF_BIP_DDL;i++) {
	bipState.EncodersI[i] = EncodersI[i];
	bipState.PotarsI[i] = PotarsI[i];
  }
  for(i=0;i<BIP_DEF_NB_SENSORS_FEET;i++)
	bipState.FeetI[i] = FeetI[i];
  
  if (EndTraj == NO_EVENT) {
    gettimeofday(&t1, NULL);
    /** Send State to the host via socket */
    nStatus = bipSendStateToHost(sockClientFd, sBipTarget, nPort, &bipXdrData2Buf, &bipState);
    if (nStatus == BIP_DEF_HOSTCOMUTILS_ERROR) {
      bipInhibit();
      fprintf(stdout, "Controller :: Sending of State Msg Failed\n");
      SocketFail = SET_EVENT;
    }    
    #ifdef _DEBUG
      fprintf(stdout, "Controller :: State sent to the host...\n");
      bip_xdr_bipState_print(&bipState);
    #endif
    
    /** Receive Command from socket */
    nStatus = bipReceiveCmdFromHost(sockClientFd, sBipTarget, &nPort, &bipXdrBuf2Data, &bipCmd, BIP_DEF_RECEIVE_TIMEOUT);
    if (nStatus == BIP_DEF_HOSTCOMUTILS_ERROR) {
      bipInhibit();
      fprintf(stdout, "Controller :: Receipt of Command Msg Failed\n");
      SocketFail = SET_EVENT;
    }         
    #ifdef _DEBUG
      fprintf(stdout, "bipControlTR :: Command received...\n");
      bip_xdr_bipCmd_print(&bipCmd);
    #endif
    
    gettimeofday(&t2, NULL);
    if (bipState.RobotCount >= BIP_BUF_SIZE)
      nCountBug = BIP_BUF_SIZE;
    else
      nCountBug = bipState.RobotCount;

    nTab[nCountBug] = t2.tv_usec - t1.tv_usec;
    
    /** recopie des donnees recu */
    SocketFail = bipCmd.SocketFail;
    EndTraj = bipCmd.EndTraj;
    
    for(i=0;i<BIP_DEF_BIP_DDL ;i++)
      CurrentO[i] = bipCmd.CurrentO[i];
  }
  
  if (SocketFail == SET_EVENT) {
      	bipInhibit();
	printf("Synchronization with the host lost on iteration #%d\n",bipState.RobotCount);
  }

  if (EndTraj == SET_EVENT)
    nCptFin = nCountBug;
  
  bipState.RobotCount ++;
}
  
    
