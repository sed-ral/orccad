// Orccad Version: 3.2
// Module : Controller
// Variables declaration File
// Date of creation : Thu Jul 15 17:35:12 2004

int i;
double dt;
int nStatus;
int nPort;
int nCountBug;
char sBipTarget[SSY_DEF_BUFFERSIZE];
struct timeval t1, t2;

/** Xdr Data Structures to send and receive */
bipXdrStream_t bipXdrData2Buf;
bipXdrStream_t bipXdrBuf2Data;

/** bip State and Commande data structures */ 
bipState_t bipState;
bipCmd_t bipCmd;
