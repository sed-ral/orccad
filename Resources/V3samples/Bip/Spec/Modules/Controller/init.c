// Orccad Version: 3.2
// Module : Controller
// Initialisation File
// 
// Module Ports :
// 	output DOUBLE CurrentO[15]
// 	input DOUBLE CurrentObsI[15]
// 	input DOUBLE FeetI[6]
// 	input DOUBLE EncodersI[15]
// 	input DOUBLE GaugesI[8]
// 	input DOUBLE PotarsI[15]
// 	output EVENT SocketFailE
// 	output EVENT EndTrajE
// 
// Time Methods usable in module files:
// 	 GetLocalTime(): return relative period time as double value
// 	 GetCurrentTime(): return global period time as double value 
// 
// Date of creation : Thu Jul 15 17:35:12 2004

EndTraj = NO_EVENT;
SocketFail = NO_EVENT;

nPort = PORT;
strcpy(sBipTarget,"194.199.21.187");
fprintf(stdout, "\nConnection to Host %s (%s) on Port %d \n", "akila", sBipTarget, nPort);

/** Initialize Data Structures */
bipCreateEncodeXdrStream(&bipXdrData2Buf, BIP_DEF_XDRSTATE_SIZE);
bipCreateDecodeXdrStream(&bipXdrBuf2Data, BIP_DEF_XDRCMD_SIZE);

bipState.dt = getSampleTime();
bipState.TR = 2;
bipState.RobotCount = 0;

nTab = (int *)malloc(BIP_BUF_SIZE*sizeof(int));
