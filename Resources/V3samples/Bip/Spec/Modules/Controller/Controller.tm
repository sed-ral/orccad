// Ilv Version: 3.1
// File generated: Fri Jul 16 15:51:04 2004
// Creator class: IlvGraphOutput
Palettes 7
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 6 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 19
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 40 70 190 260 
6 IlvMessageLabel 40 70 191 261 F8 0 1 16 4 "Controller"   [Box
0 1 1 Controller 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 220 90 20 20 
2 IlvMessageLabel 179 86 82 28 F8 0 25 16 4 "Current"   0
[Port
0 1 1 Current 230 100 179 86 82 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 30 90 20 20 
2 IlvMessageLabel 50 86 128 28 F8 0 25 16 4 "CurrentObs"   0
[Port
0 1 2 CurrentObs 40 100 50 86 128 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
3 IlvFilledEllipse 30 290 20 20 
2 IlvMessageLabel 50 286 50 28 F8 0 25 16 4 "Feet"   0
[Port
0 1 3 Feet 40 300 50 286 50 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
3 IlvFilledEllipse 130 60 20 20 
2 IlvMessageLabel 114 75 106 28 F8 0 25 16 4 "Encoders"   0
[Port
0 1 4 Encoders 140 70 114 75 106 28 2  2 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortVar 2
3 IlvFilledEllipse 30 190 20 20 
2 IlvMessageLabel 50 186 86 28 F8 0 25 16 4 "Gauges"   0
[Port
0 1 5 Gauges 40 200 50 186 86 28 1  1 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortVar 2
3 IlvFilledEllipse 30 140 20 20 
2 IlvMessageLabel 50 136 70 28 F8 0 25 16 4 "Potars"   0
[Port
0 1 6 Potars 40 150 50 136 70 28 1  1 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 90 320 20 20 2 
2 IlvMessageLabel 71 306 118 28 F8 0 25 16 4 "SocketFail"   0
[Port
0 1 7 SocketFail 100 330 71 306 118 28 8  8 
Port]

 } 20
1 1 { 14 0 OrcIlvVoidLink 1 0 13 } 30
2 0 { 15 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 160 320 20 20 2 
2 IlvMessageLabel 149 306 86 28 F8 0 25 16 4 "EndTraj"   0
[Port
0 1 8 EndTraj 170 330 149 306 86 28 8  8 
Port]

 } 20
1 1 { 16 0 OrcIlvVoidLink 1 0 15 } 30
2 0 { 17 0 OrcIlvPortVar 2
3 IlvFilledEllipse 30 240 20 20 
2 IlvMessageLabel 50 236 74 28 F8 0 25 16 4 "Inclino"   0
[Port
0 1 9 Inclino 40 250 50 236 74 28 1  1 
Port]

 } 20
1 1 { 18 0 OrcIlvVoidLink 1 0 17 } 30
EOF
