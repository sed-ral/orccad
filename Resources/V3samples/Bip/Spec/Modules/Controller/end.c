// Orccad Version: 3.2
// Module :  Controller
// End File 
// 
// Module Ports :
// 	output DOUBLE CurrentO[15]
// 	input DOUBLE CurrentObsI[15]
// 	input DOUBLE FeetI[6]
// 	input DOUBLE EncodersI[15]
// 	input DOUBLE GaugesI[8]
// 	input DOUBLE PotarsI[15]
// 	output EVENT SocketFailE
// 	output EVENT EndTrajE
// 
// 
// Date of creation : Thu Jul 15 17:35:12 2004

