// Orccad Version: 3.2
// Module :  putCurrent.c
// Call Driver File for variable: 
// 	 input DOUBLE CurrentI
// Date of creation : Tue Jul 13 17:57:03 2004

if (bipPutCurrent((double*) CurrentI)==ERROR) {
  if (DrvFail == NO_EVENT){
    printf("Bip PhR :: bipPutCurrent driver failed\n");
    DrvFail = SET_EVENT;
  }
}
