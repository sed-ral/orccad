// Orccad Version: 3.2
// Module :  getEncoders.c
// Call Driver File for variable: 
// 	 output DOUBLE EncodersO
// Date of creation : Tue Jul 13 17:57:03 2004

if (bipGetEncoders(EncodersO)==ERROR) {
  if (DrvFail == NO_EVENT) {
    printf("Bip PhR :: bipGetEncoders driver failed\n");
    DrvFail = SET_EVENT;
  }
}
