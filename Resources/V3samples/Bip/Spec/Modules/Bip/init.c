// Orccad Version: 3.2
// Module : Bip
// Initialisation Driver File
// 
// Driver Ports : 
// 	input DOUBLE CurrentI[15]
// 	output DOUBLE CurrentObsO[15]
// 	output DOUBLE EncodersO[15]
// 	output DOUBLE PotarsO[15]
// 	output DOUBLE GaugesO[8]
// 	output DOUBLE FeetO[6]
// 	output EVENT DrvFailE
// 	output EVENT SockInitFailE
// 
// Date of creation : Tue Jul 13 17:57:03 2004

nPort = PORT;

DrvFail = NO_EVENT;
SockInitFail = NO_EVENT;

nStatus = ssyUDPSocketCreate(&sockClientFd, nPort);
if (nStatus == SSY_DEF_SOCK_CREATE_ERROR) {
  fprintf(stderr, "PhRInit :: ssyUDPClientOpen Failed errno = %d \n", errno);
  SockInitFail = SET_EVENT;
}
sprintf(sMsg,"%s","Synchronization with host...");
nStatus = ssyUDPSendMsg(sockClientFd, sMsg, strlen(sMsg)+1, "194.199.21.187", nPort);
if (nStatus == SSY_DEF_SOCK_ERROR) {
  fprintf(stderr, "PhRInit :: ssyUDPSendMsg Failed errno = %d\n", errno);
  SockInitFail = SET_EVENT;
}
nStatus = ssyUDPReceiveMsg(sockClientFd, sMsg, SSY_DEF_BUFFERSIZE, sBipTarget, &nPort);
if (nStatus == SSY_DEF_SOCK_ERROR) {
  fprintf(stderr, "PhRInit :: ssyUDPReceiveMsg Failed errno = %d\n", errno);
  SockInitFail = SET_EVENT;
}
sscanf(sMsg,"%d %d",&SockInitFail,&nMode);

if (SockInitFail==NO_EVENT) {
  if (bipInitPhR(sockClientFd, sBipTarget, nPort, nMode)!=BIP_INIT_OK) {
    fprintf(stderr, "PhRInit :: bipInitPhR Failed errno = %d\n",errno);
    SockInitFail = SET_EVENT;
  }
  else {
    bipResetInhibit();
  }
}
else {
  bipInhibit();
  printf("PhRInit :: SockInitFail is active...\n");
}
	

