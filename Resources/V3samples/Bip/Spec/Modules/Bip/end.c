// Orccad Version: 3.2
// Module :  Bip
// End File 
// 
// Driver Ports : 
// 	input DOUBLE CurrentI[15]
// 	output DOUBLE CurrentObsO[15]
// 	output DOUBLE EncodersO[15]
// 	output DOUBLE PotarsO[15]
// 	output DOUBLE GaugesO[8]
// 	output DOUBLE FeetO[6]
// 	output EVENT DrvFailE
// 	output EVENT SensorFailE
// 
// Date of creation : Tue Jul 13 17:57:03 2004

bipInhibit();
ssyUDPClose(sockClientFd);

if (bipClose()==ERROR)
     printf("PhREnd :: Close Bip driver failed\n");

sprintf(name, "%s/time.data", CHEMIN);  /* attribue a name le chemin et nom de fichier de sauvegarde */ 
if (bipSaveTab(name, nTab, nCptFin) == OK);
	printf("Backup of communication time in file: %s\n",name);
	

printf("End driver\n");
