// Orccad Version: 3.2
// Module :  getFeet.c
// Call Driver File for variable: 
// 	 output DOUBLE FeetO
// Date of creation : Tue Jul 13 17:57:03 2004

if (bipGetFeetForce(FeetO)==ERROR) {
  if (DrvFail == NO_EVENT) {
    printf("Bip PhR :: bipGetFeet driver failed\n");
    DrvFail = SET_EVENT;
  }
}
