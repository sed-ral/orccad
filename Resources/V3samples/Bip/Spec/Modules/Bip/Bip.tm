// Ilv Version: 3.1
// File generated: Fri Jul 16 15:36:02 2004
// Creator class: IlvGraphOutput
Palettes 7
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "wheat" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 6 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 19
3 0 { 0 0 OrcIlvModPr 2
5 IlvFilledRectangle 40 70 190 260 
6 IlvMessageLabel 40 70 191 261 F8 0 1 16 4 "Bip"   [Box
0 1 4 Bip 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 30 90 20 20 2 
2 IlvMessageLabel 50 86 82 28 F8 0 25 16 4 "Current"   0
[Port
0 1 2 Current 40 100 50 86 82 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 220 90 20 20 2 
2 IlvMessageLabel 156 86 128 28 F8 0 25 16 4 "CurrentObs"   0
[Port
0 1 3 CurrentObs 230 100 156 86 128 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 130 60 20 20 2 
2 IlvMessageLabel 114 75 106 28 F8 0 25 16 4 "Encoders"   0
[Port
0 1 4 Encoders 140 70 114 75 106 28 2  2 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 220 140 20 20 2 
2 IlvMessageLabel 185 136 70 28 F8 0 25 16 4 "Potars"   0
[Port
0 1 5 Potars 230 150 185 136 70 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 220 190 20 20 2 
2 IlvMessageLabel 177 186 86 28 F8 0 25 16 4 "Gauges"   0
[Port
0 1 6 Gauges 230 200 177 186 86 28 4  4 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 220 290 20 20 2 
2 IlvMessageLabel 195 286 50 28 F8 0 25 16 4 "Feet"   0
[Port
0 1 7 Feet 230 300 195 286 50 28 4  4 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 90 320 20 20 2 
2 IlvMessageLabel 80 306 82 28 F8 0 25 16 4 "DrvFail"   0
[Port
0 1 8 DrvFail 100 330 80 306 82 28 8  8 
Port]

 } 20
1 1 { 14 0 OrcIlvVoidLink 1 0 13 } 30
2 0 { 15 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 160 320 20 20 2 
2 IlvMessageLabel 138 306 130 28 F8 0 25 16 4 "SockInitFail"   0
[Port
0 1 9 SockInitFail 170 330 138 306 130 28 8  8 
Port]

 } 20
1 1 { 16 0 OrcIlvVoidLink 1 0 15 } 30
2 0 { 17 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 220 240 20 20 2 
2 IlvMessageLabel 183 236 74 28 F8 0 25 16 4 "Inclino"   0
[Port
0 1 10 Inclino 230 250 183 236 74 28 4  4 
Port]

 } 20
1 1 { 18 0 OrcIlvVoidLink 1 0 17 } 30
EOF
