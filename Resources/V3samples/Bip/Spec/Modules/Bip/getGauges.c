// Orccad Version: 3.2
// Module :  getGauges.c
// Call Driver File for variable: 
// 	 output DOUBLE GaugesO
// Date of creation : Tue Jul 13 17:57:03 2004

/*
if (bipGetGauges(GaugesO)==ERROR) {
  if (DrvFail == NO_EVENT) {
    printf("Bip PhR :: bipGetGauges driver failed\n");
    DrvFail = SET_EVENT;
  }
}
*/

int i;
for(i=0;i<8;i++)
  GaugesO[i] = 0.0;
