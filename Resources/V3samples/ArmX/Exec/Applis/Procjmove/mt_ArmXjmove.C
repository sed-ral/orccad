#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/clockgen.h"
#include "Exec/datadbg.h"

#include "rts.h"
#include "interface.h"
#include "mt_ArmXjmove.h"

void orc_Mt_ArmXjmove::InitTrace()
{

}
void fct_ArmXjmove(orc_Mt_ArmXjmove* mt)
{
	 int Current_Iteration; 
	 int status_rt, access_drv, do_reinit = FALSE;
	 //init all index 
	 int _mi_jtraj_8 = mt->jtraj_8.getIndexClk();
	 int _mi_jobs_11 = mt->jobs_11.getIndexClk();
	 int _mi_error_5 = mt->error_5.getIndexClk();
	 int _mi_command_9 = mt->command_9.getIndexClk();
	 while (!mt->CondKill) {
	       if (mt->SemaphoreGive(Create_ok)==ERROR) return;
	       if (mt->SemaphoreTake(Init,WAIT_FOREVER)==ERROR) return;
	       access_drv = (mt->RT->GetPhR())->GetState();
	       // Begin Param
	       mt->jtraj_8.param();
	       mt->jobs_11.param();
	       mt->error_5.param();
	       mt->command_9.param();
	       mt->WinX->param();
	       mt->EndReparam();
	       // End Param
	       // Begin Init
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	       mt->jtraj_8.init(mt->jsecure_10.typetraj, mt->WinX->pos);
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_limit();
	                   mt->WinX->GET_key();
	              }
	       mt->jobs_11.init(mt->WinX->limit, mt->WinX->key);
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	       mt->error_5.init(mt->jtraj_8.pos, mt->WinX->pos);
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	       mt->command_9.init(mt->error_5.errpos, mt->WinX->pos);
	       // End Init
	       if (mt->jtraj_8.endtraj == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(endtraj,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->jtraj_8.endtraj = TAKE_EVENT; 
	       }
	       if (mt->jtraj_8.badtraj == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(badtraj,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->jtraj_8.badtraj = TAKE_EVENT; 
	       }
	       if (mt->jobs_11.redbut == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(redbut,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->jobs_11.redbut = TAKE_EVENT; 
	       }
	       if (mt->jobs_11.outbound == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(outbound,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->jobs_11.outbound = TAKE_EVENT; 
	       }
	       if (mt->jobs_11.typetraj == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(typetraj,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->jobs_11.typetraj = TAKE_EVENT; 
	       }
	       if (mt->error_5.errtrack == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(errtrack,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->error_5.errtrack = TAKE_EVENT; 
	       }
	       //End write Port-Event
	       if (mt->SemaphoreGive(Init_ok)==ERROR) return;
	       if (mt->SemaphoreTake(Compute,WAIT_FOREVER)==ERROR) return;
	       Current_Iteration=0;
	       for (;;) {
	            if (mt->SemaphoreTake(Synchro,WAIT_FOREVER)==ERROR) return;
	            if (mt->CondEnd) break;
	            status_rt =  mt->RT->GetState();
	            // Take Unlock Parameters
	            if (mt->IsReparam()) {
	                mt->jtraj_8.reparam();
	                mt->jobs_11.reparam();
	                mt->error_5.reparam();
	                mt->command_9.reparam();
	                mt->EndReparam();
	            }
	            // End re-Parameters
	            if (mt->Filter_Iteration == Current_Iteration)
	                mt->SemaphoreGive(Filter_ok);
	                Current_Iteration++;
	                access_drv = (mt->RT->GetPhR())->GetState();
	                //Begin computation
	                if ((access_drv == PHR_ACCESS) &&
	                   ((status_rt==ACTIVE)||(status_rt==TRANSITE)) &&
	                   (do_reinit == FALSE)) {
	                   mt->WinX->reinit();
	                   do_reinit = TRUE;
	                }
	                //T1 Exceptions Handling 
	                mt->RT->SetT1Exceptions();
	               // Compute jtraj_8
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	               if ((mt->Clock)->IsClkActive(_mi_jtraj_8) == 0) {
	                   mt->jtraj_8.compute(mt->jsecure_10.typetraj, mt->WinX->pos);
	               } 
	       if (mt->jtraj_8.endtraj == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(endtraj,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->jtraj_8.endtraj = TAKE_EVENT; 
	       }
	       if (mt->jtraj_8.badtraj == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(badtraj,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->jtraj_8.badtraj = TAKE_EVENT; 
	       }
	               status_rt =  mt->RT->GetState();
	               // Compute jobs_11
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_limit();
	                   mt->WinX->GET_key();
	              }
	               if ((mt->Clock)->IsClkActive(_mi_jobs_11) == 0) {
	                   mt->jobs_11.compute(mt->WinX->limit, mt->WinX->key);
	               } 
	       if (mt->jobs_11.redbut == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(redbut,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->jobs_11.redbut = TAKE_EVENT; 
	       }
	       if (mt->jobs_11.outbound == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(outbound,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->jobs_11.outbound = TAKE_EVENT; 
	       }
	       if (mt->jobs_11.typetraj == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(typetraj,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->jobs_11.typetraj = TAKE_EVENT; 
	       }
	               status_rt =  mt->RT->GetState();
	               // Compute error_5
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	               if ((mt->Clock)->IsClkActive(_mi_error_5) == 0) {
	                   mt->error_5.compute(mt->jtraj_8.pos, mt->WinX->pos);
	               } 
	       if (mt->error_5.errtrack == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(errtrack,"RTArmXjmove",mt->RT->GetGlobalTime());
	           mt->error_5.errtrack = TAKE_EVENT; 
	       }
	               status_rt =  mt->RT->GetState();
	               // Compute command_9
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	               if ((mt->Clock)->IsClkActive(_mi_command_9) == 0) {
	                   mt->command_9.compute(mt->error_5.errpos, mt->WinX->pos);
	               } 
	               status_rt =  mt->RT->GetState();
	               if ((access_drv == PHR_ACCESS) &&
	                   ((status_rt == ACTIVE) || (status_rt == TRANSITE))) {
	                   mt->WinX->PUT_torque(mt->command_9.torque);
	               }
	               mt->EndCompute();
	        }
	       // Begin end
	      mt->jtraj_8.end();
	      mt->jobs_11.end();
	      mt->error_5.end();
	      mt->command_9.end();
 	      // End end
 	      if (mt->SemaphoreGive(End_ok)==ERROR) return;
 	      if (mt->SemaphoreTake(Continue,WAIT_FOREVER)==ERROR) return;
 	 }
 	 if (mt->SemaphoreTake(Kill,WAIT_FOREVER)==ERROR) return; 
 	 mt->CondKill = mt->CondEnd = mt->RequestParam = FALSE; 

 	 if (mt->SemaphoreGive(Kill_ok)==ERROR) return; 

}
