
/* Required by C code constructed by esterel */ 

/* Holds the ORCCAD user type functions */
typedef void* orcStrlPtr;
#define _orcStrlPtr(x,y)(*(x)=y)
_eq_orcStrlPtr(orcStrlPtr x,orcStrlPtr y)
{return(x == y);}
_ne_orcStrlPtr(orcStrlPtr x,orcStrlPtr y)
{return(x != y);}
