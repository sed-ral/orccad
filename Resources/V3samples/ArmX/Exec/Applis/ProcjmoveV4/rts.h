#ifndef __rts_h
#define __rts_h

#include "Exec/utils.h"
#include "Exec/rt.h"
#include "Exec/param.h"

#include "mt_ArmXjmove.h"

class orc_RT_ArmXjmove: public RobotTask 
{
    protected:
	 char cfgFile[MAXFILENAME]; 
	 //Robot task Output parameters
	 //Robot task Input parameters
	 double joint[2];
	 // Module Tasks
	 orc_Mt_ArmXjmove mt_ArmXjmove;

    public: 
	 //Type 1 Exceptions
	 int aut_output_typetraj;
	 orc_RT_ArmXjmove(char* ,ProcedureRobot *, PhR *,char *);
	 ~orc_RT_ArmXjmove();
	 int SetParam(); 
	 int TestTimeout(); 
	 int SetT1Exceptions(); 
	 int SetParam(double _joint[2] );
	 int ChangeParam(double _joint[2] );
};
#endif
