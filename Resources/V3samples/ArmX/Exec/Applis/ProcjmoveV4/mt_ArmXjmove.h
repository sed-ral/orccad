#ifndef __mt_ArmXjmove_h
#define __mt_ArmXjmove_h

#include "Exec/rt.h"

#include "module_error.h"
#include "module_jtraj.h"
#include "module_command.h"
#include "module_jobs.h"
#include "PhR_WinX.h"

class RobotTask; 
class orc_Mt_ArmXjmove;
extern void fct_ArmXjmove(orc_Mt_ArmXjmove*);

class orc_Mt_ArmXjmove: public ModuleTask
{
	 friend void fct_ArmXjmove(orc_Mt_ArmXjmove*);
protected:
	 orc_Mod_error error_5;
	 orc_Mod_jtraj jtraj_8;
	 orc_Mod_command command_9;
	 orc_Mod_jobs jobs_11;
	 orc_PhR_WinX* WinX_2;
public: 
	
	 void InitTrace();
public:
#ifndef SIMPARC_RUN
	 orc_Mt_ArmXjmove(RobotTask *rt, char *name, int itFilter)
	 :ModuleTask((FUNCPTR) fct_ArmXjmove,rt,name,itFilter,0), 
#else
	 orc_Mt_ArmXjmove(RobotTask *rt, char *name, int itFilter)
	 :ModuleTask((VOIDFUNCPTRVOID) fct_ArmXjmove,rt,name,itFilter,0), 
#endif /* SIMPARC_RUN */
      error_5(this,0)
           ,jtraj_8(this,0)
           ,command_9(this,0)
           ,jobs_11(this,0)
	 {
	    InitTrace();
#ifdef SIMPARC_RUN
    error_5.vxwSet(vxwGet());
    jtraj_8.vxwSet(vxwGet());
    command_9.vxwSet(vxwGet());
    jobs_11.vxwSet(vxwGet());
#endif /* SIMPARC_RUN */
	    WinX_2 = (orc_PhR_WinX*) RT->GetPhR();
	 }
	 ~orc_Mt_ArmXjmove(){};

	 void Param();
	 void ReParam();
};
#endif
