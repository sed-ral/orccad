//
// This file defines the API functions
// to interfere with the generated Orccad program
//
// FOR SOLARIS USERS:"
// You can use it including it in the main program"
// launching the Orccad run time tasks:"
// e.g: 
// 	    #include "ProcjmoveAPI.h"
//		main() { 
// 			orcProcjmoveInit();
//  		orcProcjmoveLaunch();
//      // Launch task calling the others API Functions 
//      // ...add code here...
//      }
//
#ifndef __orc_ProcjmoveApi_h
#define __orc_ProcjmoveApi_h
// C function Interfaces
extern "C" {
extern int orcProcjmoveInit();
extern int orcProcjmoveLaunch();
extern int orcProcjmoveTaskLaunch();
extern int orcProcjmoveGo();
extern int orcProcjmoveStop();
  extern int orcArmXjmovePrm(double _joint[2]); 
} // extern "C"

#endif
