//----------------------------------------------------------
// Module jobs
//----------------------------------------------------------
#ifndef __orc_Mod_jobs_h
#define __orc_Mod_jobs_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"

#include "module_jobs_Inc.h"

class orc_Mod_jobs: public ModuleAlgo { 
protected: 
	// Internal Variables 
// Orccad Version: 3.0 alpha
// Module : jobs
// Variables declaration File
// Date of creation : Mon Nov  3 13:23:55 1997

int mode;

public: 
	orc_Mod_jobs(ModuleTask *mt,int indexclk):
	  ModuleAlgo("orc_Mod_jobs",mt,indexclk) {};
	~orc_Mod_jobs() {};
	 // Output Ports declaration
	 // Output Event Ports declaration
	 int outbound;
	 int redbut;
	 int typetraj;
	 // Output param Ports declaration

	 // Methods of computation 
	 void init( const int limit[2], const int &key ); 
	 void param(); 
	 void reparam(); 
	 void compute( const int limit[2], const int &key ); 
	 void end(); 
 }; 
#endif 
// End class  orc_Mod_jobs

