//----------------------------------------------------------
// Physical Resource WinX
//----------------------------------------------------------

#include "PhR_WinX.h"

	 // Methods to call functions driver

void orc_PhR_WinX::PUT_torque(const double torque[2]){

// Orccad Version: 3.0 alpha
// Module :  puttorque.c
// Call Driver File for variable: 
// 	 input DOUBLE torque
// Date of creation : Tue Mar 16 18:58:11 1999
winPutTau((const double *) torque);
}

void orc_PhR_WinX::GET_pos(){

// Orccad Version: 3.0 alpha
// Module :  readpos.c
// Call Driver File for variable pos
// Date of creation : Mon Nov  3 13:16:03 1997
winGetPos(pos);
}

void orc_PhR_WinX::GET_key(){

// Orccad Version: 3.0 alpha
// Module :  readkey.c
// Call Driver File for variable Port_Var
// Date of creation : Mon Nov  3 13:19:59 1997
key = (int) winKey();
}

void orc_PhR_WinX::GET_limit(){

// Orccad Version: 3.0 alpha
// Module :  readlimit.c
// Call Driver File for variable limit
// Date of creation : Mon Nov  3 13:19:59 1997
winGetLimit(limit);
}

void orc_PhR_WinX::GET_sensor(){

// Orccad Version: 3.0 alpha
// Module :  readsensor.c
// Call Driver File for variable: 
// 	 output DOUBLE sensor
// Date of creation : Tue Mar 16 18:58:11 1999
winGetSensor(sensor);
}


int orc_PhR_WinX::Init()
{ 
// Orccad Version: 3.0 alpha
// Module : WinX
// Initialisation Driver File
// Date of creation : Mon Nov  3 13:16:03 1997

if (winInit(450, 450)== ERROR){
    fprintf(stderr, "winInit:: Cannot window initialize\n");
}
return OK;
}

int orc_PhR_WinX::Close()
{ 
// Orccad Version: 3.0 alpha
// Module :  WinX
// End File 
// Date of creation : Mon Nov  3 13:16:03 1997

winKill();
return OK;
}

void orc_PhR_WinX::reinit()
{
// Orccad Version: 3.0 alpha
// Module : WinX
// Re-Init. Driver File
// Date of creation : Mon Nov  3 13:16:03 1997


} 
void orc_PhR_WinX::param()
{

} 
// End class orc_PhR_WinX
