#ifndef __orc_Procjmove_h
#define __orc_Procjmove_h

#include "Exec/utils.h"
#include "Exec/prr.h"

#include "interface.h"
#include "rts.h"

#include "PhR_WinX.h"
#define NBCLKS 1
#define BASECLK 0.1

class orc_Procjmove:public ProcedureRobot {
protected: 
   // Index Tab for clocks
   int *TabClks;
   // Physical Ressources
   orc_PhR_WinX *orcWinX;
public:
   // Robot-Tasks
   orc_RT_ArmXjmove *ArmXjmove;
   orc_RT_ArmXjmove *GetRTArmXjmovePtr() { return ArmXjmove;}
   orc_Procjmove();
   ~orc_Procjmove();
   void Launch();
}; 

#endif// End class orc_Procjmove

