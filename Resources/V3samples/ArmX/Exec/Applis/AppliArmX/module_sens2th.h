//----------------------------------------------------------
// Module sens2th
//----------------------------------------------------------
#ifndef __orc_Mod_sens2th_h
#define __orc_Mod_sens2th_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"

#include "module_sens2th_Inc.h"

class orc_Mod_sens2th: public ModuleAlgo { 
protected: 
	// Internal Variables 
// Orccad Version: 3.0 alpha
// Module : sens2th
// Variables declaration File
// Date of creation : Wed Mar 17 14:44:30 1999

double k1,k2;
double x[2];
double s12,c12;
int    top;

public: 
	orc_Mod_sens2th(ModuleTask *mt,int indexclk):
	  ModuleAlgo("orc_Mod_sens2th",mt,indexclk) {};
	~orc_Mod_sens2th() {};
	 // Output Ports declaration
	 double posd[2];
	 // Output Event Ports declaration
	 int inwork;
	 // Output param Ports declaration

	 // Methods of computation 
	 void init(const double sensor[2], const double pos[2]); 
	 void param(); 
	 void reparam(); 
	 void compute(const double sensor[2], const double pos[2] ); 
	 void end(); 
 }; 
#endif 
// End class  orc_Mod_sens2th

