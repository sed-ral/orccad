//----------------------------------------------------------
// Module ctraj
//----------------------------------------------------------

#include "module_ctraj.h"
#include "Exec/prr.h"


void orc_Mod_ctraj::init(const double posd[2], const double pos[2])
{
// Orccad Version: 3.0 alpha
// Module : ctraj
// Initialisation File
//
// Module Ports :
// 	input DOUBLE posd[2]
// 	input DOUBLE posi[2]
// 	input DOUBLE pos[2]
//
// Date of creation : Wed Mar 17 14:52:13 1999

for(index=0;index <2;index++)
    posi[index] = pos[index];

} 
void orc_Mod_ctraj::param()
{
	 plugParam();



} 
void orc_Mod_ctraj::reparam()
{

} 
void orc_Mod_ctraj::compute(const double posd[2], const double pos[2] )
{
// Orccad Version: 3.0 alpha   
// Module : ctraj
// Computation File 
//
// Module Ports :
// 	input  DOUBLE posd[2]
// 	input  DOUBLE posi[2]
// 	output DOUBLE pos[2]
//
// Date of creation : Wed Mar 17 14:52:13 1999

for(index=0;index <2;index++){
  if (posi[index] < posd[index]){
    posi[index] += STEP_POS;
    if (posi[index] > posd[index])
      posi[index] = posd[index] ;
  }
  else{
    if (posi[index] > posd[index]){
      posi[index] -= STEP_POS; 
      if (posi[index] < posd[index])
	posi[index] = posd[index];
    }
  }
}


} 
void orc_Mod_ctraj::end()
{
// Orccad Version: 3.0 alpha
// Module :  ctraj
// End File 
//
// Module Ports :
// 	input DOUBLE posd[2]
// 	input DOUBLE posi[2]
// 	input DOUBLE pos[2]
//
// Date of creation : Wed Mar 17 14:52:13 1999


} 
// End class orc_Mod_ctraj

