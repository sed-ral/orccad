#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/clockgen.h"
#include "Exec/datadbg.h"

#include "rts.h"
#include "interface.h"
#include "mt_ArmXcmove.h"

void orc_Mt_ArmXcmove::InitTrace()
{

}
void fct_ArmXcmove(orc_Mt_ArmXcmove* mt)
{
	 int Current_Iteration; 
	 int status_rt, access_drv, do_reinit = FALSE;
	 //init all index 
	 int _mi_sens2x_6 = mt->sens2x_6.getIndexClk();
	 int _mi_cobs_13 = mt->cobs_13.getIndexClk();
	 int _mi_invcart_10 = mt->invcart_10.getIndexClk();
	 int _mi_ctraj_11 = mt->ctraj_11.getIndexClk();
	 int _mi_error_4 = mt->error_4.getIndexClk();
	 int _mi_command_3 = mt->command_3.getIndexClk();
	 while (!mt->CondKill) {
	       if (mt->SemaphoreGive(Create_ok)==ERROR) return;
	       if (mt->SemaphoreTake(Init,WAIT_FOREVER)==ERROR) return;
	       access_drv = (mt->RT->GetPhR())->GetState();
	       // Begin Param
	       mt->sens2x_6.param();
	       mt->cobs_13.param();
	       mt->invcart_10.param();
	       mt->ctraj_11.param();
	       mt->error_4.param();
	       mt->command_3.param();
	       mt->WinX->param();
	       mt->EndReparam();
	       // End Param
	       // Begin Init
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_sensor();
	                   mt->WinX->GET_pos();
	              }
	       mt->sens2x_6.init(mt->WinX->sensor, mt->WinX->pos);
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_limit();
	                   mt->WinX->GET_key();
	              }
	       mt->cobs_13.init(mt->WinX->limit, mt->WinX->key);
	       mt->invcart_10.init(mt->sens2x_6.x);
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	       mt->ctraj_11.init(mt->invcart_10.jointd, mt->WinX->pos);
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	       mt->error_4.init(mt->ctraj_11.posi, mt->WinX->pos);
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	       mt->command_3.init(mt->error_4.errpos, mt->WinX->pos);
	       // End Init
	       if (mt->sens2x_6.outwork == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(outwork,"RTArmXcmove",mt->RT->GetGlobalTime());
	           mt->sens2x_6.outwork = TAKE_EVENT; 
	       }
	       if (mt->cobs_13.redbut == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(redbut,"RTArmXcmove",mt->RT->GetGlobalTime());
	           mt->cobs_13.redbut = TAKE_EVENT; 
	       }
	       if (mt->cobs_13.outbound == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(outbound,"RTArmXcmove",mt->RT->GetGlobalTime());
	           mt->cobs_13.outbound = TAKE_EVENT; 
	       }
	       if (mt->invcart_10.reconf == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(reconf,"RTArmXcmove",mt->RT->GetGlobalTime());
	           mt->invcart_10.reconf = TAKE_EVENT; 
	       }
	       if (mt->error_4.errtrack == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(errtrack,"RTArmXcmove",mt->RT->GetGlobalTime());
	           mt->error_4.errtrack = TAKE_EVENT; 
	       }
	       //End write Port-Event
	       if (mt->SemaphoreGive(Init_ok)==ERROR) return;
	       if (mt->SemaphoreTake(Compute,WAIT_FOREVER)==ERROR) return;
	       Current_Iteration=0;
	       for (;;) {
	            if (mt->SemaphoreTake(Synchro,WAIT_FOREVER)==ERROR) return;
	            if (mt->CondEnd) break;
	            status_rt =  mt->RT->GetState();
	            // Take Unlock Parameters
	            if (mt->IsReparam()) {
	                mt->sens2x_6.reparam();
	                mt->cobs_13.reparam();
	                mt->invcart_10.reparam();
	                mt->ctraj_11.reparam();
	                mt->error_4.reparam();
	                mt->command_3.reparam();
	                mt->EndReparam();
	            }
	            // End re-Parameters
	            if (mt->Filter_Iteration == Current_Iteration)
	                mt->SemaphoreGive(Filter_ok);
	                Current_Iteration++;
	                access_drv = (mt->RT->GetPhR())->GetState();
	                //Begin computation
	                if ((access_drv == PHR_ACCESS) &&
	                   ((status_rt==ACTIVE)||(status_rt==TRANSITE)) &&
	                   (do_reinit == FALSE)) {
	                   mt->WinX->reinit();
	                   do_reinit = TRUE;
	                }
	               // Compute sens2x_6
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_sensor();
	                   mt->WinX->GET_pos();
	              }
	               if ((mt->Clock)->IsClkActive(_mi_sens2x_6) == 0) {
	                   mt->sens2x_6.compute(mt->WinX->sensor, mt->WinX->pos);
	               } 
	       if (mt->sens2x_6.outwork == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(outwork,"RTArmXcmove",mt->RT->GetGlobalTime());
	           mt->sens2x_6.outwork = TAKE_EVENT; 
	       }
	               status_rt =  mt->RT->GetState();
	               // Compute cobs_13
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_limit();
	                   mt->WinX->GET_key();
	              }
	               if ((mt->Clock)->IsClkActive(_mi_cobs_13) == 0) {
	                   mt->cobs_13.compute(mt->WinX->limit, mt->WinX->key);
	               } 
	       if (mt->cobs_13.redbut == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(redbut,"RTArmXcmove",mt->RT->GetGlobalTime());
	           mt->cobs_13.redbut = TAKE_EVENT; 
	       }
	       if (mt->cobs_13.outbound == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(outbound,"RTArmXcmove",mt->RT->GetGlobalTime());
	           mt->cobs_13.outbound = TAKE_EVENT; 
	       }
	               status_rt =  mt->RT->GetState();
	               // Compute invcart_10
	               if ((mt->Clock)->IsClkActive(_mi_invcart_10) == 0) {
	                   mt->invcart_10.compute(mt->sens2x_6.x);
	               } 
	       if (mt->invcart_10.reconf == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(reconf,"RTArmXcmove",mt->RT->GetGlobalTime());
	           mt->invcart_10.reconf = TAKE_EVENT; 
	       }
	               status_rt =  mt->RT->GetState();
	               // Compute ctraj_11
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	               if ((mt->Clock)->IsClkActive(_mi_ctraj_11) == 0) {
	                   mt->ctraj_11.compute(mt->invcart_10.jointd, mt->WinX->pos);
	               } 
	               status_rt =  mt->RT->GetState();
	               // Compute error_4
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	               if ((mt->Clock)->IsClkActive(_mi_error_4) == 0) {
	                   mt->error_4.compute(mt->ctraj_11.posi, mt->WinX->pos);
	               } 
	       if (mt->error_4.errtrack == SET_EVENT) {
	           (mt->EventInterface)->SendEvent(errtrack,"RTArmXcmove",mt->RT->GetGlobalTime());
	           mt->error_4.errtrack = TAKE_EVENT; 
	       }
	               status_rt =  mt->RT->GetState();
	               // Compute command_3
	               if (access_drv == PHR_ACCESS) {
	                   mt->WinX->GET_pos();
	              }
	               if ((mt->Clock)->IsClkActive(_mi_command_3) == 0) {
	                   mt->command_3.compute(mt->error_4.errpos, mt->WinX->pos);
	               } 
	               status_rt =  mt->RT->GetState();
	               if ((access_drv == PHR_ACCESS) &&
	                   ((status_rt == ACTIVE) || (status_rt == TRANSITE))) {
	                   mt->WinX->PUT_torque(mt->command_3.torque);
	               }
	               mt->EndCompute();
	        }
	       // Begin end
	      mt->sens2x_6.end();
	      mt->cobs_13.end();
	      mt->invcart_10.end();
	      mt->ctraj_11.end();
	      mt->error_4.end();
	      mt->command_3.end();
 	      // End end
 	      if (mt->SemaphoreGive(End_ok)==ERROR) return;
 	      if (mt->SemaphoreTake(Continue,WAIT_FOREVER)==ERROR) return;
 	 }
 	 if (mt->SemaphoreTake(Kill,WAIT_FOREVER)==ERROR) return; 
 	 mt->CondKill = mt->CondEnd = mt->RequestParam = FALSE; 

 	 if (mt->SemaphoreGive(Kill_ok)==ERROR) return; 

}
