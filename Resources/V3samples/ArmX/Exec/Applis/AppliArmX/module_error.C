//----------------------------------------------------------
// Module error
//----------------------------------------------------------

#include "module_error.h"
#include "Exec/prr.h"


void orc_Mod_error::init(const double posd[2], const double posc[2])
{
// Orccad Version: 3.0 alpha
// Module : error
// Initialisation File
// Date of creation : Mon Nov  3 13:07:23 1997

errtrack = NO_EVENT;
flagtrack= FALSE;

for(index=0;index<2;index++)
    errpos[index] = posd[index]-posc[index];


} 
void orc_Mod_error::param()
{
	 plugParam();



} 
void orc_Mod_error::reparam()
{

} 
void orc_Mod_error::compute(const double posd[2], const double posc[2] )
{
// Orccad Version: 3.0 alpha   
// Module : error
// Computation File 
// Date of creation : Mon Nov  3 13:07:23 1997

flagtrack = FALSE;

for(index=0;index<2;index++){
    errpos[index] = posd[index] - posc[index];
    if ((errpos[index]>TRACKERROR)||(errpos[index]<-TRACKERROR))
	flagtrack= TRUE;
}

if ( (flagtrack==TRUE)&&(errtrack==NO_EVENT) ){
     errtrack = SET_EVENT;
     fprintf(stdout, "WinXmove::Tracking Error Event\n");
}

} 
void orc_Mod_error::end()
{
// Orccad Version: 3.0 alpha
// Module :  error
// End File 
// Date of creation : Mon Nov  3 13:07:23 1997


} 
// End class orc_Mod_error

