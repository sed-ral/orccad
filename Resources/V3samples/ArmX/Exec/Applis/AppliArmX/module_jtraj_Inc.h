//
// Module jtraj
//
#ifndef __module_jtraj_Inc_h
#define __module_jtraj_Inc_h

// Orccad Version: 3.0 alpha   
// Module : trajectory
// Include and Definition File
// Date of creation : Mon Nov  3 13:05:11 1997

#include <math.h>

#define LIMITMAX   M_PI
#define LIMITMIN  -M_PI
#define STEP_POS   0.01
#define EPSILON    0.01

#define MODE_SUSPEND 0
#define MODE_ACTIVE  1


#endif
