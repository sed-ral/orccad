//----------------------------------------------------------
// Module jtraj
//----------------------------------------------------------

#include "module_jtraj.h"
#include "Exec/prr.h"


void orc_Mod_jtraj::init(const int &typtraj, const double posI[2])
{
// Orccad Version: 3.0 alpha
// Module : jtraj
// Initialisation File
// Date of creation : Mon Nov  3 13:05:11 1997

endtraj = NO_EVENT;
badtraj = NO_EVENT;
mode    = MODE_ACTIVE;
stop    = 0;

for(index=0;index <2;index++){
  if ( (posd[index] > LIMITMAX)||( posd[index] < LIMITMIN) ) {
    pos[index] = posI[index];
    if (badtraj == NO_EVENT)
      badtraj = SET_EVENT;
  }
  else {
    pos[index]=posI[index];
  }
}

fprintf(stdout, "jtraj:: Posd = (%f, %f)\n", posd[0], posd[1]);

} 
void orc_Mod_jtraj::param()
{
	 plugParam();


	 if (Param->get("joint", DOUBLE,1,2,(char*)(posd) ) == ERROR) {
	    fprintf(stdout, (char*)"Error during parametrisation\n");
	 }


} 
void orc_Mod_jtraj::reparam()
{
	 if (Param->get("joint", DOUBLE,1,2,(char*)(posd)) == ERROR) {
	    EventStorage->displayError(RT_ERROR2,0.0);
	    PrR->Reset();
	 }


} 
void orc_Mod_jtraj::compute(const int &typtraj, const double posI[2] )
{
// Orccad Version: 3.0 alpha   
// Module : trajectory
// Computation File 
// Date of creation : Mon Nov  3 13:05:11 1997


if (mode == MODE_ACTIVE) {
  for(index=0;index <2;index++){
    if (pos[index] < posd[index]){
      pos[index] += STEP_POS;
      stop++;
      if (pos[index] > posd[index])
	pos[index] = posd[index] ;
    }
    else{
      if (pos[index] > posd[index]){
	pos[index] -= STEP_POS; 
	stop++;
	if (pos[index] < posd[index])
	  pos[index] = posd[index];
      }
    }
  }
}

stop = 0;
for(index=0;index <2;index++){
  if  ( ( (posI[index]-posd[index])<=EPSILON )&&
	( (posI[index]-posd[index])>=-EPSILON )  )
    stop++;
}

if ((stop==2)&&(endtraj==NO_EVENT)){
  fprintf(stdout, "jtraj:: Endtraj\n");
  endtraj = SET_EVENT;
}


if (typtraj==SET_EVENT){
  if (mode==MODE_ACTIVE)
    mode=MODE_SUSPEND;
  else
    mode=MODE_ACTIVE;
}



} 
void orc_Mod_jtraj::end()
{
// Orccad Version: 3.0 alpha
// Module :  jtraj
// End File 
// Date of creation : Mon Nov  3 13:05:11 1997


} 
// End class orc_Mod_jtraj

