#include "rts.h"


orc_RT_ArmXjmove::orc_RT_ArmXjmove(char *name,ProcedureRobot* prr,PhR *phr,char *cfgfile)	 :RobotTask(name,prr,phr,10000),mt_ArmXjmove(this, (char *)"mt_ArmXjmove",0)
{
	 Param = new Parameters(MAX_RT_PARAM);
	 strcpy(cfgFile,cfgfile); 
	 Parametrized = NON_PARAMETRIZED; 
	 insert((ModuleTask *) &mt_ArmXjmove);
}

orc_RT_ArmXjmove::~orc_RT_ArmXjmove()
{ 
	 delete Param;
}

int orc_RT_ArmXjmove::SetParam()
{
	 // Set RT Parameters by file
	 return(OK);
}
int orc_RT_ArmXjmove::SetParam(double _joint[2])
{ 
	 // Set RT Parameters 

	     for (int j0=0; j0<2; j0++) {
	        joint[j0] = _joint[j0];
	     }
	 // Enter Parameters in symbol table 
	 Param->add("joint", DOUBLE,1,2,(char*)&joint);
	 Parametrized = PARAMETRIZED;
	 return OK; 
}

int orc_RT_ArmXjmove::ChangeParam(double _joint[2])
{ 
	 // Set Parameters 

	 if (_joint != NULL) { 
	     for (int j = 0 ; j < 2 ; j++) {
	        joint[j] = _joint[j];
	     }
	 }

	  // Set Flag Request to the module Task
	  mt_ArmXjmove.SetReparam();
	  return OK; 
}
int orc_RT_ArmXjmove::SetT1Exceptions()
{ 
	 mt_ArmXjmove.jsecure_10.Puttypetraj(aut_output_typetraj);
	 aut_output_typetraj = 0; 
	 return OK; 
}

int orc_RT_ArmXjmove::TestTimeout()
{ 
	 return OK; 
}

orc_RT_ArmXcmove::orc_RT_ArmXcmove(char *name,ProcedureRobot* prr,PhR *phr,char *cfgfile)	 :RobotTask(name,prr,phr,10000),mt_ArmXcmove(this, (char *)"mt_ArmXcmove",0)
{
	 Param = new Parameters(MAX_RT_PARAM);
	 strcpy(cfgFile,cfgfile); 
	 Parametrized = NON_PARAMETRIZED; 
	 insert((ModuleTask *) &mt_ArmXcmove);
}

orc_RT_ArmXcmove::~orc_RT_ArmXcmove()
{ 
	 delete Param;
}

int orc_RT_ArmXcmove::SetParam()
{
	 // Set RT Parameters by file
	 return(OK);
}
int orc_RT_ArmXcmove::SetT1Exceptions()
{ 
	 return OK; 
}

int orc_RT_ArmXcmove::TestTimeout()
{ 
	 return OK; 
}

