//----------------------------------------------------------
// Module jsecure
//----------------------------------------------------------
#ifndef __orc_Mod_jsecure_h
#define __orc_Mod_jsecure_h

#include "Exec/module.h"

class orc_Mod_jsecure: public ModuleBeh { 
protected: 
	// No Internal Variables 

public: 
	orc_Mod_jsecure():
	  ModuleBeh("orc_Mod_jsecure") {
	 typetraj = 0; 
};
	~orc_Mod_jsecure() {};
	 // Output Ports declaration
	 int typetraj;
	 void Puttypetraj(int _typetraj) { typetraj = _typetraj; } 

 }; 
#endif 
// End class  orc_Mod_jsecure

