/* sscc : C CODE OF SORTED EQUATIONS aa - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __aa_GENERIC_TEST(TEST) return TEST;
typedef void (*__aa_APF)();
static __aa_APF *__aa_PActionArray;

#include "aa.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _ArmXjmove_controler_DEFINED
#ifndef ArmXjmove_controler
extern void ArmXjmove_controler(orcStrlPtr);
#endif
#endif
#ifndef _ArmXjmove_fileparameter_DEFINED
#ifndef ArmXjmove_fileparameter
extern void ArmXjmove_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXjmove_parameter_DEFINED
#ifndef ArmXjmove_parameter
extern void ArmXjmove_parameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXfmove_controler_DEFINED
#ifndef ArmXfmove_controler
extern void ArmXfmove_controler(orcStrlPtr);
#endif
#endif
#ifndef _ArmXfmove_fileparameter_DEFINED
#ifndef ArmXfmove_fileparameter
extern void ArmXfmove_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXfmove_parameter_DEFINED
#ifndef ArmXfmove_parameter
extern void ArmXfmove_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static orcStrlPtr __aa_V0;
static boolean __aa_V1;
static boolean __aa_V2;
static boolean __aa_V3;
static boolean __aa_V4;
static boolean __aa_V5;
static boolean __aa_V6;
static boolean __aa_V7;
static orcStrlPtr __aa_V8;
static boolean __aa_V9;
static orcStrlPtr __aa_V10;
static boolean __aa_V11;
static boolean __aa_V12;
static boolean __aa_V13;
static boolean __aa_V14;
static boolean __aa_V15;
static boolean __aa_V16;
static boolean __aa_V17;
static boolean __aa_V18;
static boolean __aa_V19;
static boolean __aa_V20;
static boolean __aa_V21;
static boolean __aa_V22;
static boolean __aa_V23;
static boolean __aa_V24;
static boolean __aa_V25;
static orcStrlPtr __aa_V26;
static orcStrlPtr __aa_V27;
static orcStrlPtr __aa_V28;
static orcStrlPtr __aa_V29;
static orcStrlPtr __aa_V30;
static orcStrlPtr __aa_V31;
static orcStrlPtr __aa_V32;
static orcStrlPtr __aa_V33;
static orcStrlPtr __aa_V34;
static orcStrlPtr __aa_V35;
static orcStrlPtr __aa_V36;
static orcStrlPtr __aa_V37;
static boolean __aa_V38;


/* INPUT FUNCTIONS */

void aa_I_Prr_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
void aa_I_outbound () {
__aa_V2 = _true;
}
void aa_I_KillOK_WinX () {
__aa_V3 = _true;
}
void aa_I_endtraj () {
__aa_V4 = _true;
}
void aa_I_inwork () {
__aa_V5 = _true;
}
void aa_I_errtrack () {
__aa_V6 = _true;
}
void aa_I_ActivateOK_WinX () {
__aa_V7 = _true;
}
void aa_I_ArmXjmove_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V8,__V);
__aa_V9 = _true;
}
void aa_I_ArmXfmove_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V10,__V);
__aa_V11 = _true;
}
void aa_I_ReadyToStop_WinX () {
__aa_V12 = _true;
}
void aa_I_InitOK_WinX () {
__aa_V13 = _true;
}
void aa_I_badtraj () {
__aa_V14 = _true;
}
void aa_I_T2_ArmXfmove () {
__aa_V15 = _true;
}
void aa_I_T2_ArmXjmove () {
__aa_V16 = _true;
}
void aa_I_USERSTART_ArmXjmove () {
__aa_V17 = _true;
}
void aa_I_USERSTART_ArmXfmove () {
__aa_V18 = _true;
}
void aa_I_redbut () {
__aa_V19 = _true;
}
void aa_I_typetraj () {
__aa_V20 = _true;
}
void aa_I_CmdStopOK_WinX () {
__aa_V21 = _true;
}
void aa_I_ROBOT_FAIL () {
__aa_V22 = _true;
}
void aa_I_SOFT_FAIL () {
__aa_V23 = _true;
}
void aa_I_SENSOR_FAIL () {
__aa_V24 = _true;
}
void aa_I_CPU_OVERLOAD () {
__aa_V25 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __aa_A1 \
__aa_V1
#define __aa_A2 \
__aa_V2
#define __aa_A3 \
__aa_V3
#define __aa_A4 \
__aa_V4
#define __aa_A5 \
__aa_V5
#define __aa_A6 \
__aa_V6
#define __aa_A7 \
__aa_V7
#define __aa_A8 \
__aa_V9
#define __aa_A9 \
__aa_V11
#define __aa_A10 \
__aa_V12
#define __aa_A11 \
__aa_V13
#define __aa_A12 \
__aa_V14
#define __aa_A13 \
__aa_V15
#define __aa_A14 \
__aa_V16
#define __aa_A15 \
__aa_V17
#define __aa_A16 \
__aa_V18
#define __aa_A17 \
__aa_V19
#define __aa_A18 \
__aa_V20
#define __aa_A19 \
__aa_V21
#define __aa_A20 \
__aa_V22
#define __aa_A21 \
__aa_V23
#define __aa_A22 \
__aa_V24
#define __aa_A23 \
__aa_V25

/* OUTPUT ACTIONS */

#define __aa_A24 \
aa_O_Activate(__aa_V26)
#define __aa_A25 \
aa_O_STARTED_Procfmove()
#define __aa_A26 \
aa_O_Treattypetraj(__aa_V27)
#define __aa_A27 \
aa_O_GoodEnd_Procfmove()
#define __aa_A28 \
aa_O_ActivateArmXfmove_WinX(__aa_V28)
#define __aa_A29 \
aa_O_ArmXfmoveTransite(__aa_V29)
#define __aa_A30 \
aa_O_Abort_ArmXfmove(__aa_V30)
#define __aa_A31 \
aa_O_STARTED_ArmXfmove()
#define __aa_A32 \
aa_O_GoodEnd_ArmXfmove()
#define __aa_A33 \
aa_O_Abort_Procfmove()
#define __aa_A34 \
aa_O_ActivateArmXjmove_WinX(__aa_V31)
#define __aa_A35 \
aa_O_ArmXjmoveTransite(__aa_V32)
#define __aa_A36 \
aa_O_Abort_ArmXjmove(__aa_V33)
#define __aa_A37 \
aa_O_STARTED_ArmXjmove()
#define __aa_A38 \
aa_O_GoodEnd_ArmXjmove()
#define __aa_A39 \
aa_O_EndKill(__aa_V34)
#define __aa_A40 \
aa_O_FinBF(__aa_V35)
#define __aa_A41 \
aa_O_FinT3(__aa_V36)
#define __aa_A42 \
aa_O_GoodEndRPr()
#define __aa_A43 \
aa_O_T3RPr()

/* ASSIGNMENTS */

#define __aa_A44 \
__aa_V1 = _false
#define __aa_A45 \
__aa_V2 = _false
#define __aa_A46 \
__aa_V3 = _false
#define __aa_A47 \
__aa_V4 = _false
#define __aa_A48 \
__aa_V5 = _false
#define __aa_A49 \
__aa_V6 = _false
#define __aa_A50 \
__aa_V7 = _false
#define __aa_A51 \
__aa_V9 = _false
#define __aa_A52 \
__aa_V11 = _false
#define __aa_A53 \
__aa_V12 = _false
#define __aa_A54 \
__aa_V13 = _false
#define __aa_A55 \
__aa_V14 = _false
#define __aa_A56 \
__aa_V15 = _false
#define __aa_A57 \
__aa_V16 = _false
#define __aa_A58 \
__aa_V17 = _false
#define __aa_A59 \
__aa_V18 = _false
#define __aa_A60 \
__aa_V19 = _false
#define __aa_A61 \
__aa_V20 = _false
#define __aa_A62 \
__aa_V21 = _false
#define __aa_A63 \
__aa_V22 = _false
#define __aa_A64 \
__aa_V23 = _false
#define __aa_A65 \
__aa_V24 = _false
#define __aa_A66 \
__aa_V25 = _false
#define __aa_A67 \
_orcStrlPtr(&__aa_V34,__aa_V37)
#define __aa_A68 \
_orcStrlPtr(&__aa_V35,__aa_V0)
#define __aa_A69 \
_orcStrlPtr(&__aa_V36,__aa_V0)
#define __aa_A70 \
_orcStrlPtr(&__aa_V33,__aa_V8)
#define __aa_A71 \
_orcStrlPtr(&__aa_V31,__aa_V8)
#define __aa_A72 \
_orcStrlPtr(&__aa_V37,__aa_V8)
#define __aa_A73 \
_orcStrlPtr(&__aa_V32,__aa_V8)
#define __aa_A74 \
_orcStrlPtr(&__aa_V32,__aa_V8)
#define __aa_A75 \
_orcStrlPtr(&__aa_V27,__aa_V8)
#define __aa_A76 \
_orcStrlPtr(&__aa_V32,__aa_V8)
#define __aa_A77 \
_orcStrlPtr(&__aa_V32,__aa_V8)
#define __aa_A78 \
_orcStrlPtr(&__aa_V30,__aa_V10)
#define __aa_A79 \
_orcStrlPtr(&__aa_V28,__aa_V10)
#define __aa_A80 \
_orcStrlPtr(&__aa_V37,__aa_V10)
#define __aa_A81 \
_orcStrlPtr(&__aa_V29,__aa_V10)
#define __aa_A82 \
_orcStrlPtr(&__aa_V29,__aa_V10)
#define __aa_A83 \
_orcStrlPtr(&__aa_V29,__aa_V10)
#define __aa_A84 \
_orcStrlPtr(&__aa_V29,__aa_V10)
#define __aa_A85 \
_orcStrlPtr(&__aa_V34,__aa_V37)

/* PROCEDURE CALLS */

#define __aa_A86 \
ArmXjmove_parameter(__aa_V8,"0.5 0.5")
#define __aa_A87 \
ArmXjmove_controler(__aa_V8)
#define __aa_A88 \
ArmXfmove_parameter(__aa_V10)
#define __aa_A89 \
ArmXfmove_controler(__aa_V10)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __aa_A90 \

#define __aa_A91 \

#define __aa_A92 \

#define __aa_A93 \

#define __aa_A94 \

#define __aa_A95 \

#define __aa_A96 \

#define __aa_A97 \

#define __aa_A98 \

#define __aa_A99 \

#define __aa_A100 \

#define __aa_A101 \

#define __aa_A102 \

#define __aa_A103 \

#define __aa_A104 \

#define __aa_A105 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int aa_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __aa__reset_input () {
__aa_V1 = _false;
__aa_V2 = _false;
__aa_V3 = _false;
__aa_V4 = _false;
__aa_V5 = _false;
__aa_V6 = _false;
__aa_V7 = _false;
__aa_V9 = _false;
__aa_V11 = _false;
__aa_V12 = _false;
__aa_V13 = _false;
__aa_V14 = _false;
__aa_V15 = _false;
__aa_V16 = _false;
__aa_V17 = _false;
__aa_V18 = _false;
__aa_V19 = _false;
__aa_V20 = _false;
__aa_V21 = _false;
__aa_V22 = _false;
__aa_V23 = _false;
__aa_V24 = _false;
__aa_V25 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __aa_R[31] = {_true,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int aa () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[88];
if (!(_true)) {
__aa_A24;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A24\n");
#endif
}
E[0] = (__aa_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1)));
if (E[0]) {
__aa_A90;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A90\n");
#endif
}
E[1] = (__aa_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8)));
if (E[1]) {
__aa_A91;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A91\n");
#endif
}
E[2] = (__aa_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9)));
if (E[2]) {
__aa_A92;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A92\n");
#endif
}
if (__aa_R[0]) {
__aa_A93;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A93\n");
#endif
}
if (__aa_R[0]) {
__aa_A94;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A94\n");
#endif
}
if (__aa_R[0]) {
__aa_A95;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A95\n");
#endif
}
if (__aa_R[0]) {
__aa_A96;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A96\n");
#endif
}
if (__aa_R[0]) {
__aa_A97;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A97\n");
#endif
}
if (__aa_R[0]) {
__aa_A98;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A98\n");
#endif
}
if (__aa_R[0]) {
__aa_A99;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A99\n");
#endif
}
if (__aa_R[0]) {
__aa_A100;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A100\n");
#endif
}
if (__aa_R[0]) {
__aa_A101;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A101\n");
#endif
}
if (__aa_R[0]) {
__aa_A102;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A102\n");
#endif
}
if (__aa_R[0]) {
__aa_A103;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A103\n");
#endif
}
if (__aa_R[0]) {
__aa_A104;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A104\n");
#endif
}
if (__aa_R[0]) {
__aa_A105;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A105\n");
#endif
}
E[3] = (__aa_R[2]&&!(__aa_R[0]));
E[4] = (E[3]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8));
E[5] = (__aa_R[3]&&!(__aa_R[0]));
E[6] = (E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9));
E[7] = (__aa_R[4]&&!(__aa_R[0]));
E[8] = (E[7]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1));
E[9] = ((__aa_R[2]||__aa_R[3])||__aa_R[4]);
E[10] = (((E[9]&&!(__aa_R[2])))||E[4]);
E[11] = (((E[9]&&!(__aa_R[3])))||E[6]);
E[12] = (((E[9]&&!(__aa_R[4])))||E[8]);
E[8] = ((((((E[4]||E[6])||E[8]))&&E[10])&&E[11])&&E[12]);
E[6] = (__aa_R[23]&&!(__aa_R[0]));
E[4] = (((__aa_R[0]&&E[8]))||((E[6]&&E[8])));
if (E[4]) {
__aa_A25;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A25\n");
#endif
}
E[13] = (__aa_R[9]&&!(__aa_R[0]));
E[14] = (E[13]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__aa_A18));
if (E[14]) {
__aa_A75;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A75\n");
#endif
}
if (E[14]) {
__aa_A26;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A26\n");
#endif
}
E[15] = (__aa_R[25]&&!(__aa_R[0]));
E[16] = (__aa_R[22]&&!(__aa_R[0]));
E[17] = (E[16]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5));
E[18] = ((((__aa_R[18]||__aa_R[19])||__aa_R[20])||__aa_R[21])||__aa_R[22]);
E[19] = (__aa_R[16]&&((__aa_R[16]&&!(__aa_R[0]))));
E[20] = (E[19]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11));
E[21] = (__aa_R[27]&&!(__aa_R[0]));
E[22] = (__aa_R[6]&&((__aa_R[6]&&!(__aa_R[0]))));
E[23] = (E[22]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11));
E[24] = (E[23]||E[20]);
E[25] = (E[21]&&E[24]);
E[26] = (__aa_R[30]&&!(__aa_R[0]));
E[27] = (E[26]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__aa_A19));
E[28] = (E[25]||E[27]);
E[29] = (__aa_R[17]&&((__aa_R[17]&&!(__aa_R[0]))));
E[30] = (((E[20]&&E[28]))||((E[29]&&E[28])));
if (E[30]) {
__aa_A79;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A79\n");
#endif
}
if (E[30]) {
__aa_A80;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A80\n");
#endif
}
E[31] = (E[30]||((__aa_R[18]&&((__aa_R[18]&&!(__aa_R[0]))))));
E[32] = (((E[18]&&!(__aa_R[18])))||E[31]);
E[33] = (__aa_R[19]&&!(__aa_R[0]));
E[34] = (E[33]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__aa_A6)));
E[34] = (E[30]||((__aa_R[19]&&E[34])));
E[35] = (((E[18]&&!(__aa_R[19])))||E[34]);
E[36] = (__aa_R[20]&&!(__aa_R[0]));
E[37] = (E[36]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2)));
E[37] = (E[30]||((__aa_R[20]&&E[37])));
E[38] = (((E[18]&&!(__aa_R[20])))||E[37]);
E[39] = (__aa_R[21]&&!(__aa_R[0]));
E[40] = (E[39]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__aa_A17)));
E[40] = (E[30]||((__aa_R[21]&&E[40])));
E[41] = (((E[18]&&!(__aa_R[21])))||E[40]);
E[16] = (E[16]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5)));
E[16] = (E[30]||((__aa_R[22]&&E[16])));
E[42] = (((E[18]&&!(__aa_R[22])))||E[16]);
E[43] = (E[42]||E[17]);
E[17] = (((((E[17]&&E[32])&&E[35])&&E[38])&&E[41])&&E[43]);
if (E[17]) {
__aa_A83;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A83\n");
#endif
}
E[44] = (E[15]&&E[17]);
E[45] = (__aa_R[24]||__aa_R[25]);
E[46] = (E[45]||__aa_R[26]);
if (E[4]) {
__aa_A86;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A86\n");
#endif
}
if (E[4]) {
__aa_A87;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A87\n");
#endif
}
E[47] = (__aa_R[24]&&!(__aa_R[0]));
E[48] = (__aa_R[14]&&!(__aa_R[0]));
E[49] = (E[48]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4));
E[50] = ((((((__aa_R[8]||__aa_R[9])||__aa_R[10])||__aa_R[11])||__aa_R[12])||__aa_R[13])||__aa_R[14]);
E[51] = (__aa_R[7]&&((__aa_R[7]&&!(__aa_R[0]))));
E[52] = (((E[23]&&E[28]))||((E[51]&&E[28])));
if (E[52]) {
__aa_A71;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A71\n");
#endif
}
if (E[52]) {
__aa_A72;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A72\n");
#endif
}
E[53] = (E[52]||((__aa_R[8]&&((__aa_R[8]&&!(__aa_R[0]))))));
E[54] = (((E[50]&&!(__aa_R[8])))||E[53]);
E[13] = (E[13]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__aa_A18)));
E[13] = ((E[52]||E[14])||((__aa_R[9]&&E[13])));
E[55] = (((E[50]&&!(__aa_R[9])))||E[13]);
E[56] = (__aa_R[10]&&!(__aa_R[0]));
E[57] = (E[56]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__aa_A6)));
E[57] = (E[52]||((__aa_R[10]&&E[57])));
E[58] = (((E[50]&&!(__aa_R[10])))||E[57]);
E[59] = (__aa_R[11]&&!(__aa_R[0]));
E[60] = (E[59]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2)));
E[60] = (E[52]||((__aa_R[11]&&E[60])));
E[61] = (((E[50]&&!(__aa_R[11])))||E[60]);
E[62] = (__aa_R[12]&&!(__aa_R[0]));
E[63] = (E[62]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12)));
E[63] = (E[52]||((__aa_R[12]&&E[63])));
E[64] = (((E[50]&&!(__aa_R[12])))||E[63]);
E[65] = (__aa_R[13]&&!(__aa_R[0]));
E[66] = (E[65]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__aa_A17)));
E[66] = (E[52]||((__aa_R[13]&&E[66])));
E[67] = (((E[50]&&!(__aa_R[13])))||E[66]);
E[48] = (E[48]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4)));
E[48] = (E[52]||((__aa_R[14]&&E[48])));
E[68] = (((E[50]&&!(__aa_R[14])))||E[48]);
E[69] = (E[68]||E[49]);
E[49] = (((((((E[49]&&E[54])&&E[55])&&E[58])&&E[61])&&E[64])&&E[67])&&E[69]);
if (E[49]) {
__aa_A76;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A76\n");
#endif
}
E[70] = (E[47]&&!(E[49]));
E[56] = (E[56]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__aa_A6));
E[59] = (E[59]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2));
E[62] = (E[62]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12));
E[65] = (E[65]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__aa_A17));
if (!(_true)) {
__aa_A74;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A74\n");
#endif
}
E[69] = (((((((((((E[56]||E[59])||E[62])||E[65]))&&E[54])&&E[55])&&((E[58]||E[56])))&&((E[61]||E[59])))&&((E[64]||E[62])))&&((E[67]||E[65])))&&E[69]);
E[71] = (E[4]||((__aa_R[24]&&((E[70]&&!(E[69]))))));
E[47] = (E[47]&&E[49]);
if (E[47]) {
__aa_A88;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A88\n");
#endif
}
if (E[47]) {
__aa_A89;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A89\n");
#endif
}
E[15] = (E[15]&&!(E[17]));
E[33] = (E[33]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__aa_A6));
E[36] = (E[36]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2));
E[39] = (E[39]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__aa_A17));
if (!(_true)) {
__aa_A82;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A82\n");
#endif
}
E[43] = ((((((((E[33]||E[36])||E[39]))&&E[32])&&((E[35]||E[33])))&&((E[38]||E[36])))&&((E[41]||E[39])))&&E[43]);
E[72] = (E[47]||((__aa_R[25]&&((E[15]&&!(E[43]))))));
E[45] = ((((E[46]&&!(E[45])))||E[71])||E[72]);
E[73] = (E[45]||E[44]);
E[74] = (E[4]||((__aa_R[26]&&((__aa_R[26]&&!(__aa_R[0]))))));
E[75] = (((E[46]&&!(__aa_R[26])))||E[74]);
E[44] = ((E[44]&&E[73])&&E[75]);
if (E[44]) {
__aa_A27;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A27\n");
#endif
}
if (E[30]) {
__aa_A28;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A28\n");
#endif
}
if (!(_true)) {
__aa_A81;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A81\n");
#endif
}
if (E[17]) {
__aa_A29;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A29\n");
#endif
}
if (!(_true)) {
__aa_A78;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A78\n");
#endif
}
if (!(_true)) {
__aa_A30;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A30\n");
#endif
}
E[76] = (E[17]||__aa_R[0]);
E[77] = (__aa_R[15]&&!(__aa_R[0]));
E[78] = (((E[76]&&E[47]))||((E[77]&&E[47])));
if (E[78]) {
__aa_A31;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A31\n");
#endif
}
if (E[17]) {
__aa_A32;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A32\n");
#endif
}
if (!(_true)) {
__aa_A33;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A33\n");
#endif
}
if (E[52]) {
__aa_A34;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A34\n");
#endif
}
if (!(_true)) {
__aa_A73;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A73\n");
#endif
}
if (E[49]) {
__aa_A35;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A35\n");
#endif
}
if (!(_true)) {
__aa_A70;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A70\n");
#endif
}
if (!(_true)) {
__aa_A36;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A36\n");
#endif
}
E[79] = (E[49]||__aa_R[0]);
E[80] = (__aa_R[5]&&!(__aa_R[0]));
E[81] = (((E[79]&&E[4]))||((E[80]&&E[4])));
if (E[81]) {
__aa_A37;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A37\n");
#endif
}
if (E[49]) {
__aa_A38;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A38\n");
#endif
}
E[82] = (__aa_R[29]&&!(__aa_R[0]));
E[83] = (E[82]&&E[24]);
if (E[83]) {
__aa_A85;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A85\n");
#endif
}
E[84] = (__aa_R[1]&&!(__aa_R[0]));
E[85] = (E[84]&&E[44]);
E[50] = ((((__aa_R[6]||E[50])||__aa_R[7]))||__aa_R[5]);
E[18] = ((((__aa_R[16]||E[18])||__aa_R[17]))||__aa_R[15]);
E[46] = (E[46]||__aa_R[23]);
E[86] = (((__aa_R[27]||__aa_R[28])||__aa_R[29])||__aa_R[30]);
E[87] = (((((E[9]||E[50])||E[18])||E[46])||E[86])||__aa_R[1]);
E[3] = (E[3]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8)));
E[3] = (__aa_R[0]||((__aa_R[2]&&E[3])));
E[5] = (E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9)));
E[5] = (__aa_R[0]||((__aa_R[3]&&E[5])));
E[7] = (E[7]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1)));
E[7] = (__aa_R[0]||((__aa_R[4]&&E[7])));
E[12] = ((((((E[3]||E[5])||E[7]))&&((E[10]||E[3])))&&((E[11]||E[5])))&&((E[12]||E[7])));
E[9] = (((((E[87]&&!(E[9])))||E[8]))||E[12]);
E[22] = (E[22]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11)));
E[22] = (__aa_R[6]&&E[22]);
E[51] = (((E[23]&&!(E[28])))||((__aa_R[7]&&((E[51]&&!(E[28]))))));
E[80] = (((E[79]&&!(E[4])))||((__aa_R[5]&&((E[80]&&!(E[4]))))));
E[68] = ((E[81]||(((E[22]||(((((((((((((((E[53]||E[13])||E[57])||E[60])||E[63])||E[66])||E[48]))&&E[54])&&E[55])&&E[58])&&E[61])&&E[64])&&E[67])&&E[68])))||E[51])))||E[80]);
E[50] = (((((E[87]&&!(E[50])))||E[69]))||E[68]);
E[19] = (E[19]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11)));
E[19] = (__aa_R[16]&&E[19]);
E[28] = (((E[20]&&!(E[28])))||((__aa_R[17]&&((E[29]&&!(E[28]))))));
E[77] = (((E[76]&&!(E[47])))||((__aa_R[15]&&((E[77]&&!(E[47]))))));
E[42] = ((E[78]||(((E[19]||(((((((((((E[31]||E[34])||E[37])||E[40])||E[16]))&&E[32])&&E[35])&&E[38])&&E[41])&&E[42])))||E[28])))||E[77]);
E[18] = (((((E[87]&&!(E[18])))||E[43]))||E[42]);
E[15] = (E[15]&&E[43]);
E[70] = (E[70]&&E[69]);
E[73] = ((((E[15]||E[70]))&&(((E[73]||E[15])||E[70])))&&E[75]);
E[8] = (((__aa_R[0]&&!(E[8])))||((__aa_R[23]&&((E[6]&&!(E[8]))))));
E[75] = (((((((E[71]||E[72])||E[74]))&&E[45])&&E[75]))||E[8]);
E[46] = (((((E[87]&&!(E[46])))||((E[44]||E[73]))))||E[75]);
E[21] = (__aa_R[0]||((__aa_R[27]&&((E[21]&&!(E[24]))))));
E[45] = (__aa_R[28]&&!(__aa_R[0]));
E[6] = (E[49]||E[17]);
E[27] = ((E[25]||E[27])||((__aa_R[28]&&((E[45]&&!(E[6]))))));
E[24] = (((E[45]&&E[6]))||((__aa_R[29]&&((E[82]&&!(E[24]))))));
E[26] = (E[26]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__aa_A19)));
E[26] = (E[83]||((__aa_R[30]&&E[26])));
E[86] = ((((((E[87]&&!(E[86])))||E[21])||E[27])||E[24])||E[26]);
E[84] = (E[84]&&!(E[44]));
E[82] = (E[84]&&!(E[73]));
E[6] = (E[82]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 20\n"),
#endif
__aa_A20)));
E[45] = (E[6]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__aa_A21)));
E[25] = (E[45]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22)));
E[41] = (E[25]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 23\n"),
#endif
__aa_A23)));
E[41] = (__aa_R[0]||((__aa_R[1]&&E[41])));
E[87] = (((E[87]&&!(__aa_R[1])))||E[41]);
E[38] = (E[87]||E[85]);
E[85] = ((((((E[85]&&E[9])&&E[50])&&E[18])&&E[46])&&E[86])&&E[38]);
if (E[85]) {
__aa_A67;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A67\n");
#endif
}
if (((E[83]||E[85]))) {
__aa_A39;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A39\n");
#endif
}
if (E[85]) {
__aa_A68;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A68\n");
#endif
}
if (E[85]) {
__aa_A40;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A40\n");
#endif
}
E[84] = (E[84]&&E[73]);
E[82] = (E[82]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 20\n"),
#endif
__aa_A20));
E[6] = (E[6]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__aa_A21));
E[45] = (E[45]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22));
E[25] = (E[25]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 23\n"),
#endif
__aa_A23));
E[38] = (((((((((((E[84]||E[82])||E[6])||E[45])||E[25]))&&E[9])&&E[50])&&E[18])&&E[46])&&E[86])&&((((((E[38]||E[84])||E[82])||E[6])||E[45])||E[25])));
if (E[38]) {
__aa_A69;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A69\n");
#endif
}
if (E[38]) {
__aa_A41;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A41\n");
#endif
}
if (E[85]) {
__aa_A42;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A42\n");
#endif
}
if (E[38]) {
__aa_A43;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A43\n");
#endif
}
E[35] = (E[52]||E[30]);
E[25] = ((((E[84]||E[82])||E[6])||E[45])||E[25]);
E[65] = (((E[56]||E[59])||E[62])||E[65]);
E[39] = ((E[33]||E[36])||E[39]);
E[70] = (E[15]||E[70]);
E[15] = ((E[85]||E[38]));
E[87] = ((((((((((((((((E[12]||E[68])||E[42])||E[75])||E[21])||E[27])||E[24])||E[26])||E[41]))&&E[9])&&E[50])&&E[18])&&E[46])&&E[86])&&E[87]));
E[86] = (E[38]||E[85]);
__aa_R[1] = (E[41]&&!(E[86]));
__aa_R[2] = (E[3]&&!(E[86]));
__aa_R[3] = (E[5]&&!(E[86]));
__aa_R[4] = (E[7]&&!(E[86]));
__aa_R[5] = (E[80]&&!(E[86]));
E[80] = (E[69]||E[86]);
__aa_R[6] = (((E[81]&&!(E[86])))||((E[22]&&!(E[80]))));
__aa_R[7] = (E[51]&&!(E[80]));
E[69] = ((((((E[80]||E[69]))||E[69]))||E[49])||E[69]);
__aa_R[8] = (E[53]&&!(E[69]));
__aa_R[9] = (E[13]&&!(E[69]));
__aa_R[10] = (E[57]&&!(E[69]));
__aa_R[11] = (E[60]&&!(E[69]));
__aa_R[12] = (E[63]&&!(E[69]));
__aa_R[13] = (E[66]&&!(E[69]));
__aa_R[14] = (E[48]&&!(E[69]));
__aa_R[15] = (E[77]&&!(E[86]));
E[77] = (E[43]||E[86]);
__aa_R[16] = (((E[78]&&!(E[86])))||((E[19]&&!(E[77]))));
__aa_R[17] = (E[28]&&!(E[77]));
E[43] = ((((((E[77]||E[43]))||E[43]))||E[17])||E[43]);
__aa_R[18] = (E[31]&&!(E[43]));
__aa_R[19] = (E[34]&&!(E[43]));
__aa_R[20] = (E[37]&&!(E[43]));
__aa_R[21] = (E[40]&&!(E[43]));
__aa_R[22] = (E[16]&&!(E[43]));
__aa_R[23] = (E[8]&&!(E[86]));
E[73] = ((((E[86]||E[73]))||E[44])||E[73]);
__aa_R[24] = (E[71]&&!(E[73]));
__aa_R[25] = (E[72]&&!(E[73]));
__aa_R[26] = (E[74]&&!(E[73]));
__aa_R[27] = (E[21]&&!(E[86]));
__aa_R[28] = (E[27]&&!(E[86]));
__aa_R[29] = (E[24]&&!(E[86]));
__aa_R[30] = (E[26]&&!(E[86]));
__aa_R[0] = !(_true);
__aa__reset_input();
return E[87];
}

/* AUTOMATON RESET */

int aa_reset () {
__aa_R[0] = _true;
__aa_R[1] = _false;
__aa_R[2] = _false;
__aa_R[3] = _false;
__aa_R[4] = _false;
__aa_R[5] = _false;
__aa_R[6] = _false;
__aa_R[7] = _false;
__aa_R[8] = _false;
__aa_R[9] = _false;
__aa_R[10] = _false;
__aa_R[11] = _false;
__aa_R[12] = _false;
__aa_R[13] = _false;
__aa_R[14] = _false;
__aa_R[15] = _false;
__aa_R[16] = _false;
__aa_R[17] = _false;
__aa_R[18] = _false;
__aa_R[19] = _false;
__aa_R[20] = _false;
__aa_R[21] = _false;
__aa_R[22] = _false;
__aa_R[23] = _false;
__aa_R[24] = _false;
__aa_R[25] = _false;
__aa_R[26] = _false;
__aa_R[27] = _false;
__aa_R[28] = _false;
__aa_R[29] = _false;
__aa_R[30] = _false;
__aa__reset_input();
return 0;
}
