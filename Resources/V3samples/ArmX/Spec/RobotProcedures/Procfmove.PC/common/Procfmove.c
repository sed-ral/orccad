/* sscc : C CODE OF SORTED EQUATIONS Procfmove - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __Procfmove_GENERIC_TEST(TEST) return TEST;
typedef void (*__Procfmove_APF)();
static __Procfmove_APF *__Procfmove_PActionArray;

#include "Procfmove.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _ArmXjmove_controler_DEFINED
#ifndef ArmXjmove_controler
extern void ArmXjmove_controler(orcStrlPtr);
#endif
#endif
#ifndef _ArmXjmove_fileparameter_DEFINED
#ifndef ArmXjmove_fileparameter
extern void ArmXjmove_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXjmove_parameter_DEFINED
#ifndef ArmXjmove_parameter
extern void ArmXjmove_parameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXfmove_controler_DEFINED
#ifndef ArmXfmove_controler
extern void ArmXfmove_controler(orcStrlPtr);
#endif
#endif
#ifndef _ArmXfmove_fileparameter_DEFINED
#ifndef ArmXfmove_fileparameter
extern void ArmXfmove_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXfmove_parameter_DEFINED
#ifndef ArmXfmove_parameter
extern void ArmXfmove_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __Procfmove_V0;
static boolean __Procfmove_V1;
static orcStrlPtr __Procfmove_V2;
static boolean __Procfmove_V3;
static boolean __Procfmove_V4;
static boolean __Procfmove_V5;
static boolean __Procfmove_V6;
static orcStrlPtr __Procfmove_V7;
static boolean __Procfmove_V8;
static boolean __Procfmove_V9;
static boolean __Procfmove_V10;
static boolean __Procfmove_V11;


/* INPUT FUNCTIONS */

void Procfmove_I_START_Procfmove () {
__Procfmove_V0 = _true;
}
void Procfmove_I_Abort_Local_Procfmove () {
__Procfmove_V1 = _true;
}
void Procfmove_I_ArmXjmove_Start (orcStrlPtr __V) {
_orcStrlPtr(&__Procfmove_V2,__V);
__Procfmove_V3 = _true;
}
void Procfmove_I_USERSTART_ArmXjmove () {
__Procfmove_V4 = _true;
}
void Procfmove_I_BF_ArmXjmove () {
__Procfmove_V5 = _true;
}
void Procfmove_I_T3_ArmXjmove () {
__Procfmove_V6 = _true;
}
void Procfmove_I_ArmXfmove_Start (orcStrlPtr __V) {
_orcStrlPtr(&__Procfmove_V7,__V);
__Procfmove_V8 = _true;
}
void Procfmove_I_USERSTART_ArmXfmove () {
__Procfmove_V9 = _true;
}
void Procfmove_I_BF_ArmXfmove () {
__Procfmove_V10 = _true;
}
void Procfmove_I_T3_ArmXfmove () {
__Procfmove_V11 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __Procfmove_A1 \
__Procfmove_V0
#define __Procfmove_A2 \
__Procfmove_V1
#define __Procfmove_A3 \
__Procfmove_V3
#define __Procfmove_A4 \
__Procfmove_V4
#define __Procfmove_A5 \
__Procfmove_V5
#define __Procfmove_A6 \
__Procfmove_V6
#define __Procfmove_A7 \
__Procfmove_V8
#define __Procfmove_A8 \
__Procfmove_V9
#define __Procfmove_A9 \
__Procfmove_V10
#define __Procfmove_A10 \
__Procfmove_V11

/* OUTPUT ACTIONS */

#define __Procfmove_A11 \
Procfmove_O_BF_Procfmove()
#define __Procfmove_A12 \
Procfmove_O_STARTED_Procfmove()
#define __Procfmove_A13 \
Procfmove_O_GoodEnd_Procfmove()
#define __Procfmove_A14 \
Procfmove_O_Abort_Procfmove()
#define __Procfmove_A15 \
Procfmove_O_T3_Procfmove()
#define __Procfmove_A16 \
Procfmove_O_START_ArmXjmove()
#define __Procfmove_A17 \
Procfmove_O_Abort_Local_ArmXjmove()
#define __Procfmove_A18 \
Procfmove_O_START_ArmXfmove()
#define __Procfmove_A19 \
Procfmove_O_Abort_Local_ArmXfmove()

/* ASSIGNMENTS */

#define __Procfmove_A20 \
__Procfmove_V0 = _false
#define __Procfmove_A21 \
__Procfmove_V1 = _false
#define __Procfmove_A22 \
__Procfmove_V3 = _false
#define __Procfmove_A23 \
__Procfmove_V4 = _false
#define __Procfmove_A24 \
__Procfmove_V5 = _false
#define __Procfmove_A25 \
__Procfmove_V6 = _false
#define __Procfmove_A26 \
__Procfmove_V8 = _false
#define __Procfmove_A27 \
__Procfmove_V9 = _false
#define __Procfmove_A28 \
__Procfmove_V10 = _false
#define __Procfmove_A29 \
__Procfmove_V11 = _false

/* PROCEDURE CALLS */

#define __Procfmove_A30 \
ArmXjmove_parameter(__Procfmove_V2,"0.5 0.5")
#define __Procfmove_A31 \
ArmXjmove_controler(__Procfmove_V2)
#define __Procfmove_A32 \
ArmXfmove_parameter(__Procfmove_V7)
#define __Procfmove_A33 \
ArmXfmove_controler(__Procfmove_V7)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __Procfmove_A34 \

#define __Procfmove_A35 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int Procfmove_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __Procfmove__reset_input () {
__Procfmove_V0 = _false;
__Procfmove_V1 = _false;
__Procfmove_V3 = _false;
__Procfmove_V4 = _false;
__Procfmove_V5 = _false;
__Procfmove_V6 = _false;
__Procfmove_V8 = _false;
__Procfmove_V9 = _false;
__Procfmove_V10 = _false;
__Procfmove_V11 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __Procfmove_R[5] = {_true,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int Procfmove () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[18];
E[0] = (__Procfmove_R[3]&&!(__Procfmove_R[0]));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__Procfmove_A9));
E[2] = (__Procfmove_R[2]||__Procfmove_R[3]);
E[3] = (E[2]||__Procfmove_R[4]);
E[4] = (__Procfmove_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__Procfmove_A3)));
if (E[4]) {
__Procfmove_A34;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A34\n");
#endif
}
E[5] = (__Procfmove_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__Procfmove_A7)));
if (E[5]) {
__Procfmove_A35;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A35\n");
#endif
}
E[6] = (__Procfmove_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Procfmove_A1));
E[7] = (__Procfmove_R[1]&&!(__Procfmove_R[0]));
E[8] = (E[7]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Procfmove_A1));
E[8] = (E[6]||E[8]);
if (E[8]) {
__Procfmove_A30;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A30\n");
#endif
}
if (E[8]) {
__Procfmove_A31;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A31\n");
#endif
}
E[6] = (__Procfmove_R[2]&&!(__Procfmove_R[0]));
E[9] = (E[6]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Procfmove_A5)));
E[10] = (E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Procfmove_A6)));
E[10] = (E[8]||((__Procfmove_R[2]&&E[10])));
E[6] = (E[6]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Procfmove_A5));
if (E[6]) {
__Procfmove_A32;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A32\n");
#endif
}
if (E[6]) {
__Procfmove_A33;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A33\n");
#endif
}
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__Procfmove_A9)));
E[11] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__Procfmove_A10)));
E[11] = (E[6]||((__Procfmove_R[3]&&E[11])));
E[2] = ((((E[3]&&!(E[2])))||E[10])||E[11]);
E[12] = (E[2]||E[1]);
E[13] = (__Procfmove_R[4]&&!(__Procfmove_R[0]));
E[14] = (E[13]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Procfmove_A2)));
E[14] = (E[8]||((__Procfmove_R[4]&&E[14])));
E[15] = (((E[3]&&!(__Procfmove_R[4])))||E[14]);
E[1] = ((E[1]&&E[12])&&E[15]);
if (E[1]) {
__Procfmove_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A11\n");
#endif
}
if (E[8]) {
__Procfmove_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A12\n");
#endif
}
if (E[1]) {
__Procfmove_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A13\n");
#endif
}
E[13] = (E[13]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Procfmove_A2));
if (E[13]) {
__Procfmove_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A14\n");
#endif
}
E[0] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__Procfmove_A10));
E[9] = (E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Procfmove_A6));
E[12] = ((E[12]||E[0])||E[9]);
E[16] = ((((E[0]||E[9]))&&E[12])&&E[15]);
if (E[16]) {
__Procfmove_A15;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A15\n");
#endif
}
if (E[8]) {
__Procfmove_A16;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A16\n");
#endif
}
if (E[13]) {
__Procfmove_A17;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A17\n");
#endif
}
if (E[6]) {
__Procfmove_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A18\n");
#endif
}
if (E[13]) {
__Procfmove_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procfmove_A19\n");
#endif
}
E[9] = (E[0]||E[9]);
E[12] = ((E[13]&&E[12])&&((E[15]||E[13])));
E[0] = ((((E[1]||E[16]))||E[12]));
E[17] = (__Procfmove_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Procfmove_A1)));
E[7] = (E[7]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Procfmove_A1)));
E[7] = (E[17]||((__Procfmove_R[1]&&E[7])));
E[15] = ((((((((E[10]||E[11])||E[14]))&&E[2])&&E[15]))||E[7]));
E[3] = (E[3]||__Procfmove_R[1]);
E[12] = (((((E[12]||E[16]))||E[1])||E[16])||E[12]);
__Procfmove_R[2] = (E[10]&&!(E[12]));
__Procfmove_R[3] = (E[11]&&!(E[12]));
__Procfmove_R[4] = (E[14]&&!(E[12]));
__Procfmove_R[0] = !(_true);
__Procfmove_R[1] = E[7];
__Procfmove__reset_input();
return E[15];
}

/* AUTOMATON RESET */

int Procfmove_reset () {
__Procfmove_R[0] = _true;
__Procfmove_R[1] = _false;
__Procfmove_R[2] = _false;
__Procfmove_R[3] = _false;
__Procfmove_R[4] = _false;
__Procfmove__reset_input();
return 0;
}
