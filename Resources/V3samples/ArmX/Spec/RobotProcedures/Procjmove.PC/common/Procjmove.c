/* sscc : C CODE OF SORTED EQUATIONS Procjmove - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __Procjmove_GENERIC_TEST(TEST) return TEST;
typedef void (*__Procjmove_APF)();
static __Procjmove_APF *__Procjmove_PActionArray;

#include "Procjmove.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _ArmXjmove_controler_DEFINED
#ifndef ArmXjmove_controler
extern void ArmXjmove_controler(orcStrlPtr);
#endif
#endif
#ifndef _ArmXjmove_fileparameter_DEFINED
#ifndef ArmXjmove_fileparameter
extern void ArmXjmove_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXjmove_parameter_DEFINED
#ifndef ArmXjmove_parameter
extern void ArmXjmove_parameter(orcStrlPtr ,string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __Procjmove_V0;
static boolean __Procjmove_V1;
static orcStrlPtr __Procjmove_V2;
static boolean __Procjmove_V3;
static boolean __Procjmove_V4;
static boolean __Procjmove_V5;
static boolean __Procjmove_V6;


/* INPUT FUNCTIONS */

void Procjmove_I_START_Procjmove () {
__Procjmove_V0 = _true;
}
void Procjmove_I_Abort_Local_Procjmove () {
__Procjmove_V1 = _true;
}
void Procjmove_I_ArmXjmove_Start (orcStrlPtr __V) {
_orcStrlPtr(&__Procjmove_V2,__V);
__Procjmove_V3 = _true;
}
void Procjmove_I_USERSTART_ArmXjmove () {
__Procjmove_V4 = _true;
}
void Procjmove_I_BF_ArmXjmove () {
__Procjmove_V5 = _true;
}
void Procjmove_I_T3_ArmXjmove () {
__Procjmove_V6 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __Procjmove_A1 \
__Procjmove_V0
#define __Procjmove_A2 \
__Procjmove_V1
#define __Procjmove_A3 \
__Procjmove_V3
#define __Procjmove_A4 \
__Procjmove_V4
#define __Procjmove_A5 \
__Procjmove_V5
#define __Procjmove_A6 \
__Procjmove_V6

/* OUTPUT ACTIONS */

#define __Procjmove_A7 \
Procjmove_O_BF_Procjmove()
#define __Procjmove_A8 \
Procjmove_O_STARTED_Procjmove()
#define __Procjmove_A9 \
Procjmove_O_GoodEnd_Procjmove()
#define __Procjmove_A10 \
Procjmove_O_Abort_Procjmove()
#define __Procjmove_A11 \
Procjmove_O_T3_Procjmove()
#define __Procjmove_A12 \
Procjmove_O_START_ArmXjmove()
#define __Procjmove_A13 \
Procjmove_O_Abort_Local_ArmXjmove()

/* ASSIGNMENTS */

#define __Procjmove_A14 \
__Procjmove_V0 = _false
#define __Procjmove_A15 \
__Procjmove_V1 = _false
#define __Procjmove_A16 \
__Procjmove_V3 = _false
#define __Procjmove_A17 \
__Procjmove_V4 = _false
#define __Procjmove_A18 \
__Procjmove_V5 = _false
#define __Procjmove_A19 \
__Procjmove_V6 = _false

/* PROCEDURE CALLS */

#define __Procjmove_A20 \
ArmXjmove_parameter(__Procjmove_V2,"0.5 0.5")
#define __Procjmove_A21 \
ArmXjmove_controler(__Procjmove_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __Procjmove_A22 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int Procjmove_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __Procjmove__reset_input () {
__Procjmove_V0 = _false;
__Procjmove_V1 = _false;
__Procjmove_V3 = _false;
__Procjmove_V4 = _false;
__Procjmove_V5 = _false;
__Procjmove_V6 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __Procjmove_R[4] = {_true,_false,_false,_false};

/* AUTOMATON ENGINE */

int Procjmove () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = (__Procjmove_R[2]&&!(__Procjmove_R[0]));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Procjmove_A5));
E[2] = (__Procjmove_R[2]||__Procjmove_R[3]);
E[3] = (__Procjmove_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__Procjmove_A3)));
if (E[3]) {
__Procjmove_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procjmove_A22\n");
#endif
}
E[4] = (__Procjmove_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Procjmove_A1));
E[5] = (__Procjmove_R[1]&&!(__Procjmove_R[0]));
E[6] = (E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Procjmove_A1));
E[6] = (E[4]||E[6]);
if (E[6]) {
__Procjmove_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procjmove_A20\n");
#endif
}
if (E[6]) {
__Procjmove_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procjmove_A21\n");
#endif
}
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Procjmove_A5)));
E[4] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Procjmove_A6)));
E[4] = (E[6]||((__Procjmove_R[2]&&E[4])));
E[7] = (((E[2]&&!(__Procjmove_R[2])))||E[4]);
E[8] = (E[7]||E[1]);
E[9] = (__Procjmove_R[3]&&!(__Procjmove_R[0]));
E[10] = (E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Procjmove_A2)));
E[10] = (E[6]||((__Procjmove_R[3]&&E[10])));
E[11] = (((E[2]&&!(__Procjmove_R[3])))||E[10]);
E[1] = ((E[1]&&E[8])&&E[11]);
if (E[1]) {
__Procjmove_A7;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procjmove_A7\n");
#endif
}
if (E[6]) {
__Procjmove_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procjmove_A8\n");
#endif
}
if (E[1]) {
__Procjmove_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procjmove_A9\n");
#endif
}
E[9] = (E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Procjmove_A2));
if (E[9]) {
__Procjmove_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procjmove_A10\n");
#endif
}
E[0] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Procjmove_A6));
E[8] = (E[8]||E[0]);
E[0] = ((E[0]&&E[8])&&E[11]);
if (E[0]) {
__Procjmove_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procjmove_A11\n");
#endif
}
if (E[6]) {
__Procjmove_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procjmove_A12\n");
#endif
}
if (E[9]) {
__Procjmove_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__Procjmove_A13\n");
#endif
}
E[8] = ((E[9]&&E[8])&&((E[11]||E[9])));
E[12] = ((((E[1]||E[0]))||E[8]));
E[13] = (__Procjmove_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Procjmove_A1)));
E[5] = (E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Procjmove_A1)));
E[5] = (E[13]||((__Procjmove_R[1]&&E[5])));
E[11] = (((((((E[4]||E[10]))&&E[7])&&E[11]))||E[5]));
E[2] = (E[2]||__Procjmove_R[1]);
E[8] = (((((E[8]||E[0]))||E[1])||E[0])||E[8]);
__Procjmove_R[2] = (E[4]&&!(E[8]));
__Procjmove_R[3] = (E[10]&&!(E[8]));
__Procjmove_R[0] = !(_true);
__Procjmove_R[1] = E[5];
__Procjmove__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int Procjmove_reset () {
__Procjmove_R[0] = _true;
__Procjmove_R[1] = _false;
__Procjmove_R[2] = _false;
__Procjmove_R[3] = _false;
__Procjmove__reset_input();
return 0;
}
