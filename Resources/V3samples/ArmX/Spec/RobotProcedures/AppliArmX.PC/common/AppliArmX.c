/* sscc : C CODE OF SORTED EQUATIONS AppliArmX - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __AppliArmX_GENERIC_TEST(TEST) return TEST;
typedef void (*__AppliArmX_APF)();
static __AppliArmX_APF *__AppliArmX_PActionArray;

#include "AppliArmX.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _ArmXjmove_controler_DEFINED
#ifndef ArmXjmove_controler
extern void ArmXjmove_controler(orcStrlPtr);
#endif
#endif
#ifndef _ArmXjmove_fileparameter_DEFINED
#ifndef ArmXjmove_fileparameter
extern void ArmXjmove_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXjmove_parameter_DEFINED
#ifndef ArmXjmove_parameter
extern void ArmXjmove_parameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXcmove_controler_DEFINED
#ifndef ArmXcmove_controler
extern void ArmXcmove_controler(orcStrlPtr);
#endif
#endif
#ifndef _ArmXcmove_fileparameter_DEFINED
#ifndef ArmXcmove_fileparameter
extern void ArmXcmove_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXcmove_parameter_DEFINED
#ifndef ArmXcmove_parameter
extern void ArmXcmove_parameter(orcStrlPtr);
#endif
#endif
#ifndef _ArmXfmove_controler_DEFINED
#ifndef ArmXfmove_controler
extern void ArmXfmove_controler(orcStrlPtr);
#endif
#endif
#ifndef _ArmXfmove_fileparameter_DEFINED
#ifndef ArmXfmove_fileparameter
extern void ArmXfmove_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXfmove_parameter_DEFINED
#ifndef ArmXfmove_parameter
extern void ArmXfmove_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __AppliArmX_V0;
static boolean __AppliArmX_V1;
static orcStrlPtr __AppliArmX_V2;
static boolean __AppliArmX_V3;
static boolean __AppliArmX_V4;
static boolean __AppliArmX_V5;
static boolean __AppliArmX_V6;
static orcStrlPtr __AppliArmX_V7;
static boolean __AppliArmX_V8;
static boolean __AppliArmX_V9;
static boolean __AppliArmX_V10;
static boolean __AppliArmX_V11;
static orcStrlPtr __AppliArmX_V12;
static boolean __AppliArmX_V13;
static boolean __AppliArmX_V14;
static boolean __AppliArmX_V15;
static boolean __AppliArmX_V16;
static boolean __AppliArmX_V17;
static boolean __AppliArmX_V18;


/* INPUT FUNCTIONS */

void AppliArmX_I_START_AppliArmX () {
__AppliArmX_V0 = _true;
}
void AppliArmX_I_Abort_Local_AppliArmX () {
__AppliArmX_V1 = _true;
}
void AppliArmX_I_ArmXjmove_Start (orcStrlPtr __V) {
_orcStrlPtr(&__AppliArmX_V2,__V);
__AppliArmX_V3 = _true;
}
void AppliArmX_I_USERSTART_ArmXjmove () {
__AppliArmX_V4 = _true;
}
void AppliArmX_I_BF_ArmXjmove () {
__AppliArmX_V5 = _true;
}
void AppliArmX_I_T3_ArmXjmove () {
__AppliArmX_V6 = _true;
}
void AppliArmX_I_ArmXcmove_Start (orcStrlPtr __V) {
_orcStrlPtr(&__AppliArmX_V7,__V);
__AppliArmX_V8 = _true;
}
void AppliArmX_I_USERSTART_ArmXcmove () {
__AppliArmX_V9 = _true;
}
void AppliArmX_I_BF_ArmXcmove () {
__AppliArmX_V10 = _true;
}
void AppliArmX_I_T3_ArmXcmove () {
__AppliArmX_V11 = _true;
}
void AppliArmX_I_ArmXfmove_Start (orcStrlPtr __V) {
_orcStrlPtr(&__AppliArmX_V12,__V);
__AppliArmX_V13 = _true;
}
void AppliArmX_I_USERSTART_ArmXfmove () {
__AppliArmX_V14 = _true;
}
void AppliArmX_I_BF_ArmXfmove () {
__AppliArmX_V15 = _true;
}
void AppliArmX_I_T3_ArmXfmove () {
__AppliArmX_V16 = _true;
}
void AppliArmX_I_T2_reconf () {
__AppliArmX_V17 = _true;
}
void AppliArmX_I_UserStop () {
__AppliArmX_V18 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __AppliArmX_A1 \
__AppliArmX_V0
#define __AppliArmX_A2 \
__AppliArmX_V1
#define __AppliArmX_A3 \
__AppliArmX_V3
#define __AppliArmX_A4 \
__AppliArmX_V4
#define __AppliArmX_A5 \
__AppliArmX_V5
#define __AppliArmX_A6 \
__AppliArmX_V6
#define __AppliArmX_A7 \
__AppliArmX_V8
#define __AppliArmX_A8 \
__AppliArmX_V9
#define __AppliArmX_A9 \
__AppliArmX_V10
#define __AppliArmX_A10 \
__AppliArmX_V11
#define __AppliArmX_A11 \
__AppliArmX_V13
#define __AppliArmX_A12 \
__AppliArmX_V14
#define __AppliArmX_A13 \
__AppliArmX_V15
#define __AppliArmX_A14 \
__AppliArmX_V16
#define __AppliArmX_A15 \
__AppliArmX_V17
#define __AppliArmX_A16 \
__AppliArmX_V18

/* OUTPUT ACTIONS */

#define __AppliArmX_A17 \
AppliArmX_O_BF_AppliArmX()
#define __AppliArmX_A18 \
AppliArmX_O_STARTED_AppliArmX()
#define __AppliArmX_A19 \
AppliArmX_O_GoodEnd_AppliArmX()
#define __AppliArmX_A20 \
AppliArmX_O_Abort_AppliArmX()
#define __AppliArmX_A21 \
AppliArmX_O_T3_AppliArmX()
#define __AppliArmX_A22 \
AppliArmX_O_START_ArmXjmove()
#define __AppliArmX_A23 \
AppliArmX_O_Abort_Local_ArmXjmove()
#define __AppliArmX_A24 \
AppliArmX_O_START_ArmXcmove()
#define __AppliArmX_A25 \
AppliArmX_O_Abort_Local_ArmXcmove()
#define __AppliArmX_A26 \
AppliArmX_O_START_ArmXfmove()
#define __AppliArmX_A27 \
AppliArmX_O_Abort_Local_ArmXfmove()

/* ASSIGNMENTS */

#define __AppliArmX_A28 \
__AppliArmX_V0 = _false
#define __AppliArmX_A29 \
__AppliArmX_V1 = _false
#define __AppliArmX_A30 \
__AppliArmX_V3 = _false
#define __AppliArmX_A31 \
__AppliArmX_V4 = _false
#define __AppliArmX_A32 \
__AppliArmX_V5 = _false
#define __AppliArmX_A33 \
__AppliArmX_V6 = _false
#define __AppliArmX_A34 \
__AppliArmX_V8 = _false
#define __AppliArmX_A35 \
__AppliArmX_V9 = _false
#define __AppliArmX_A36 \
__AppliArmX_V10 = _false
#define __AppliArmX_A37 \
__AppliArmX_V11 = _false
#define __AppliArmX_A38 \
__AppliArmX_V13 = _false
#define __AppliArmX_A39 \
__AppliArmX_V14 = _false
#define __AppliArmX_A40 \
__AppliArmX_V15 = _false
#define __AppliArmX_A41 \
__AppliArmX_V16 = _false
#define __AppliArmX_A42 \
__AppliArmX_V17 = _false
#define __AppliArmX_A43 \
__AppliArmX_V18 = _false

/* PROCEDURE CALLS */

#define __AppliArmX_A44 \
ArmXjmove_parameter(__AppliArmX_V2,"0.0 0.0")
#define __AppliArmX_A45 \
ArmXjmove_controler(__AppliArmX_V2)
#define __AppliArmX_A46 \
ArmXfmove_parameter(__AppliArmX_V12)
#define __AppliArmX_A47 \
ArmXfmove_controler(__AppliArmX_V12)
#define __AppliArmX_A48 \
ArmXcmove_parameter(__AppliArmX_V7)
#define __AppliArmX_A49 \
ArmXcmove_controler(__AppliArmX_V7)
#define __AppliArmX_A50 \
ArmXjmove_parameter(__AppliArmX_V2,"0.0 0.0")
#define __AppliArmX_A51 \
ArmXjmove_controler(__AppliArmX_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __AppliArmX_A52 \

#define __AppliArmX_A53 \

#define __AppliArmX_A54 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int AppliArmX_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __AppliArmX__reset_input () {
__AppliArmX_V0 = _false;
__AppliArmX_V1 = _false;
__AppliArmX_V3 = _false;
__AppliArmX_V4 = _false;
__AppliArmX_V5 = _false;
__AppliArmX_V6 = _false;
__AppliArmX_V8 = _false;
__AppliArmX_V9 = _false;
__AppliArmX_V10 = _false;
__AppliArmX_V11 = _false;
__AppliArmX_V13 = _false;
__AppliArmX_V14 = _false;
__AppliArmX_V15 = _false;
__AppliArmX_V16 = _false;
__AppliArmX_V17 = _false;
__AppliArmX_V18 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __AppliArmX_R[9] = {_true,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int AppliArmX () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[32];
E[0] = __AppliArmX_R[7]&&!(__AppliArmX_R[0]);
E[1] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 16\n"),
#endif
__AppliArmX_A16);
E[2] = __AppliArmX_R[4]||__AppliArmX_R[5];
E[3] = __AppliArmX_R[2]||__AppliArmX_R[3]||__AppliArmX_R[6]||E[2];
E[4] = E[3]||__AppliArmX_R[7];
E[5] = __AppliArmX_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__AppliArmX_A3));
if (E[5]) {
__AppliArmX_A52;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A52\n");
#endif
}
E[6] = __AppliArmX_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__AppliArmX_A7));
if (E[6]) {
__AppliArmX_A53;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A53\n");
#endif
}
E[7] = __AppliArmX_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__AppliArmX_A11));
if (E[7]) {
__AppliArmX_A54;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A54\n");
#endif
}
E[8] = __AppliArmX_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__AppliArmX_A1);
E[9] = __AppliArmX_R[1]&&!(__AppliArmX_R[0]);
E[10] = E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__AppliArmX_A1);
E[10] = E[8]||E[10];
if (E[10]) {
__AppliArmX_A44;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A44\n");
#endif
}
if (E[10]) {
__AppliArmX_A45;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A45\n");
#endif
}
E[8] = __AppliArmX_R[2]&&!(__AppliArmX_R[0]);
E[11] = E[8]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__AppliArmX_A5));
E[12] = E[11]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__AppliArmX_A6));
E[12] = E[10]||(__AppliArmX_R[2]&&E[12]);
E[13] = __AppliArmX_R[6]&&!(__AppliArmX_R[0]);
E[14] = E[13]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__AppliArmX_A5);
E[8] = E[8]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__AppliArmX_A5);
E[8] = E[14]||E[8];
if (E[8]) {
__AppliArmX_A46;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A46\n");
#endif
}
if (E[8]) {
__AppliArmX_A47;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A47\n");
#endif
}
E[14] = __AppliArmX_R[3]&&!(__AppliArmX_R[0]);
E[15] = E[14]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__AppliArmX_A13));
E[16] = E[15]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__AppliArmX_A14));
E[16] = E[8]||(__AppliArmX_R[3]&&E[16]);
E[14] = E[14]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__AppliArmX_A13);
if (E[14]) {
__AppliArmX_A48;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A48\n");
#endif
}
if (E[14]) {
__AppliArmX_A49;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A49\n");
#endif
}
E[17] = __AppliArmX_R[4]&&!(__AppliArmX_R[0]);
E[18] = E[17]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__AppliArmX_A9));
E[19] = E[18]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__AppliArmX_A10));
E[19] = E[14]||(__AppliArmX_R[4]&&E[19]);
E[20] = __AppliArmX_R[5]&&!(__AppliArmX_R[0]);
E[21] = E[20]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__AppliArmX_A15));
E[21] = E[14]||(__AppliArmX_R[5]&&E[21]);
E[17] = E[17]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__AppliArmX_A9);
E[17] = (E[2]&&!(__AppliArmX_R[4]))||E[17]||E[19];
E[2] = (E[2]&&!(__AppliArmX_R[5]))||E[21];
E[20] = E[20]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__AppliArmX_A15);
E[22] = E[2]||E[20];
E[20] = E[20]&&E[17]&&E[22];
if (E[20]) {
__AppliArmX_A50;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A50\n");
#endif
}
if (E[20]) {
__AppliArmX_A51;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A51\n");
#endif
}
E[13] = E[13]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__AppliArmX_A5));
E[23] = E[13]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__AppliArmX_A6));
E[23] = E[20]||(__AppliArmX_R[6]&&E[23]);
E[2] = ((E[19]||E[21])&&E[17]&&E[2])||E[23];
E[3] = (E[4]&&!(E[3]))||E[12]||E[16]||E[2];
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 16\n"),
#endif
__AppliArmX_A16));
E[0] = E[10]||(__AppliArmX_R[7]&&E[0]);
E[24] = (E[4]&&!(__AppliArmX_R[7]))||E[0];
E[25] = E[24]||E[1];
E[26] = E[1]&&E[3]&&E[25];
E[27] = E[4]||__AppliArmX_R[8];
E[24] = (E[12]||E[16]||E[2]||E[0])&&E[3]&&E[24];
E[4] = (E[27]&&!(E[4]))||E[24];
E[2] = E[4]||E[26];
E[28] = __AppliArmX_R[8]&&!(__AppliArmX_R[0]);
E[29] = E[28]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__AppliArmX_A2));
E[29] = E[10]||(__AppliArmX_R[8]&&E[29]);
E[30] = (E[27]&&!(__AppliArmX_R[8]))||E[29];
E[31] = E[26]&&E[2]&&E[30];
if (E[31]) {
__AppliArmX_A17;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A17\n");
#endif
}
if (E[10]) {
__AppliArmX_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A18\n");
#endif
}
if (E[31]) {
__AppliArmX_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A19\n");
#endif
}
E[28] = E[28]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__AppliArmX_A2);
if (E[28]) {
__AppliArmX_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A20\n");
#endif
}
E[13] = E[13]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__AppliArmX_A6);
E[18] = E[18]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__AppliArmX_A10);
E[22] = E[18]&&(E[17]||E[18])&&E[22];
E[17] = E[13]||E[22];
E[15] = E[15]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__AppliArmX_A14);
E[11] = E[11]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__AppliArmX_A6);
E[25] = (E[17]||E[15]||E[11])&&(E[3]||E[17]||E[15]||E[11])&&E[25];
E[2] = E[2]||E[25];
E[3] = E[25]&&E[2]&&E[30];
if (E[3]) {
__AppliArmX_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A21\n");
#endif
}
if (E[10]||E[20]) {
__AppliArmX_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A22\n");
#endif
}
if (E[1]||E[28]) {
__AppliArmX_A23;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A23\n");
#endif
}
if (E[14]) {
__AppliArmX_A24;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A24\n");
#endif
}
if (E[20]||E[1]||E[28]) {
__AppliArmX_A25;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A25\n");
#endif
}
if (E[8]) {
__AppliArmX_A26;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A26\n");
#endif
}
if (E[1]||E[28]) {
__AppliArmX_A27;
#ifdef TRACE_ACTION
fprintf(stderr, "__AppliArmX_A27\n");
#endif
}
E[11] = E[18]||E[13]||E[15]||E[11];
E[2] = E[28]&&E[2]&&(E[30]||E[28]);
E[15] = E[31]||E[3]||E[2];
E[13] = __AppliArmX_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__AppliArmX_A1));
E[9] = E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__AppliArmX_A1));
E[9] = E[13]||(__AppliArmX_R[1]&&E[9]);
E[30] = ((E[24]||E[29])&&E[4]&&E[30])||E[9];
E[27] = E[27]||__AppliArmX_R[1];
E[2] = E[2]||E[3]||E[31]||E[3]||E[2];
E[25] = E[2]||E[25]||E[26]||E[25];
__AppliArmX_R[2] = E[12]&&!(E[25]);
__AppliArmX_R[3] = E[16]&&!(E[25]);
E[17] = E[25]||E[17];
E[22] = E[17]||E[20]||E[22];
__AppliArmX_R[4] = E[19]&&!(E[22]);
__AppliArmX_R[5] = E[21]&&!(E[22]);
__AppliArmX_R[6] = E[23]&&!(E[17]);
__AppliArmX_R[7] = E[0]&&!(E[25]);
__AppliArmX_R[8] = E[29]&&!(E[2]);
__AppliArmX_R[0] = !(_true);
__AppliArmX_R[1] = E[9];
__AppliArmX__reset_input();
return E[30];
}

/* AUTOMATON RESET */

int AppliArmX_reset () {
__AppliArmX_R[0] = _true;
__AppliArmX_R[1] = _false;
__AppliArmX_R[2] = _false;
__AppliArmX_R[3] = _false;
__AppliArmX_R[4] = _false;
__AppliArmX_R[5] = _false;
__AppliArmX_R[6] = _false;
__AppliArmX_R[7] = _false;
__AppliArmX_R[8] = _false;
__AppliArmX__reset_input();
return 0;
}
