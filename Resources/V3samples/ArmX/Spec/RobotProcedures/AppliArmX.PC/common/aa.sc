sc6:
	-- closed (or fully linked)

module: aa

instances: 7
root: 0
0: aa 0 "" "aa.strl" %lc: 1 1 0% %lc_end: 150 1 0%
1: RTs_Init 0 "" "aa.strl" %lc: 174 1 1% %lc_end: 194 1 1% %instance: 117 1 0%
2: ArmXfmove 0 "" "ArmXfmove.strl" %lc: 1 1 2% %lc_end: 107 1 2% %instance: 119 1 0%
3: ArmXcmove 0 "" "ArmXcmove.strl" %lc: 1 1 3% %lc_end: 112 1 3% %instance: 121 1 0%
4: ArmXjmove 0 "" "ArmXjmove.strl" %lc: 1 1 4% %lc_end: 115 1 4% %instance: 123 1 0%
5: AppliArmX 0 "" "AppliArmX.strl" %lc: 1 1 5% %lc_end: 163 1 5% %instance: 125 1 0%
6: TransiteRobot 0 "" "aa.strl" %lc: 153 1 6% %lc_end: 171 1 6% %instance: 127 1 0%
end:

types: 1
0: orcStrlPtr 0 - - - %lc: 3 7 0%
end:

procedures: 10
0: _assign_orcStrlPtr (0) (0) %lc: 3 7 0%
1: ArmXjmove_controler () (0) %lc: 17 11 5%
2: ArmXjmove_fileparameter () (0,$2) %lc: 18 12 5%
3: ArmXjmove_parameter () (0,$2) %lc: 19 11 5%
4: ArmXfmove_controler () (0) %lc: 26 11 5%
5: ArmXfmove_fileparameter () (0,$2) %lc: 27 12 5%
6: ArmXfmove_parameter () (0) %lc: 28 11 5%
7: ArmXcmove_controler () (0) %lc: 36 11 5%
8: ArmXcmove_fileparameter () (0,$2) %lc: 37 12 5%
9: ArmXcmove_parameter () (0) %lc: 38 11 5%
end:

signals: 104
0: input: Prr_Start 0 single: 0 bool: 1 %previous: first:% %lc: 4 7 0%
1: input: typetraj 1 pure: bool: 2 %previous: 0% %lc: 5 4 0%
2: input: inwork 2 pure: bool: 3 %previous: 1% %lc: 6 4 0%
3: input: outwork 3 pure: bool: 4 %previous: 2% %lc: 7 4 0%
4: input: USERSTART_ArmXfmove 4 pure: bool: 5 %previous: 3% %lc: 8 4 0%
5: input: USERSTART_ArmXcmove 5 pure: bool: 6 %previous: 4% %lc: 9 4 0%
6: input: USERSTART_ArmXjmove 6 pure: bool: 7 %previous: 5% %lc: 10 4 0%
7: input: reconf 7 pure: bool: 8 %previous: 6% %lc: 11 4 0%
8: input: InitOK_WinX 8 pure: bool: 9 %previous: 7% %lc: 12 4 0%
9: input: ArmXfmove_Start 9 single: 10 bool: 11 %previous: 8% %lc: 13 4 0%
10: input: ArmXcmove_Start 10 single: 12 bool: 13 %previous: 9% %lc: 14 4 0%
11: input: ArmXjmove_Start 11 single: 14 bool: 15 %previous: 10% %lc: 15 4 0%
12: input: CmdStopOK_WinX 12 pure: bool: 16 %previous: 11% %lc: 16 4 0%
13: input: ActivateOK_WinX 13 pure: bool: 17 %previous: 12% %lc: 17 4 0%
14: input: T2_ArmXcmove 14 pure: bool: 18 %previous: 13% %lc: 18 4 0%
15: input: KillOK_WinX 15 pure: bool: 19 %previous: 14% %lc: 19 4 0%
16: input: ReadyToStop_WinX 16 pure: bool: 20 %previous: 15% %lc: 20 4 0%
17: input: badtraj 17 pure: bool: 21 %previous: 16% %lc: 21 4 0%
18: input: endtraj 18 pure: bool: 22 %previous: 17% %lc: 22 4 0%
19: input: errtrack 19 pure: bool: 23 %previous: 18% %lc: 23 4 0%
20: input: outbound 20 pure: bool: 24 %previous: 19% %lc: 24 4 0%
21: input: redbut 21 pure: bool: 25 %previous: 20% %lc: 25 4 0%
22: input: T2_ArmXfmove 22 pure: bool: 26 %previous: 21% %lc: 26 4 0%
23: input: T2_ArmXjmove 23 pure: bool: 27 %previous: 22% %lc: 27 4 0%
24: input: ROBOT_FAIL 24 pure: bool: 28 %previous: 23% %lc: 28 4 0%
25: input: SOFT_FAIL 25 pure: bool: 29 %previous: 24% %lc: 29 4 0%
26: input: SENSOR_FAIL 26 pure: bool: 30 %previous: 25% %lc: 30 4 0%
27: input: CPU_OVERLOAD 27 pure: bool: 31 %previous: 26% %lc: 31 4 0%
28: output: Activate 28 single: 32 %previous: 27% %lc: 33 8 0%
29: output: ActivateArmXcmove_WinX 29 single: 33 %previous: 28% %lc: 33 33 0%
30: output: ArmXcmoveTransite 30 single: 34 %previous: 29% %lc: 34 4 0%
31: output: Abort_ArmXcmove 31 single: 35 %previous: 30% %lc: 35 4 0%
32: output: STARTED_ArmXcmove 32 pure: %previous: 31% %lc: 36 4 0%
33: output: GoodEnd_ArmXcmove 33 pure: %previous: 32% %lc: 37 4 0%
34: output: STARTED_AppliArmX 34 pure: %previous: 33% %lc: 38 4 0%
35: output: Abort_AppliArmX 35 pure: %previous: 34% %lc: 39 4 0%
36: output: ActivateArmXfmove_WinX 36 single: 36 %previous: 35% %lc: 40 4 0%
37: output: ArmXfmoveTransite 37 single: 37 %previous: 36% %lc: 41 4 0%
38: output: Abort_ArmXfmove 38 single: 38 %previous: 37% %lc: 42 4 0%
39: output: STARTED_ArmXfmove 39 pure: %previous: 38% %lc: 43 4 0%
40: output: GoodEnd_ArmXfmove 40 pure: %previous: 39% %lc: 44 4 0%
41: output: ActivateArmXjmove_WinX 41 single: 39 %previous: 40% %lc: 45 4 0%
42: output: ArmXjmoveTransite 42 single: 40 %previous: 41% %lc: 46 4 0%
43: output: Abort_ArmXjmove 43 single: 41 %previous: 42% %lc: 47 4 0%
44: output: STARTED_ArmXjmove 44 pure: %previous: 43% %lc: 48 4 0%
45: output: GoodEnd_ArmXjmove 45 pure: %previous: 44% %lc: 49 4 0%
46: output: Treattypetraj 46 single: 42 %previous: 45% %lc: 50 4 0%
47: output: GoodEnd_AppliArmX 47 pure: %previous: 46% %lc: 51 4 0%
48: output: EndKill 48 single: 43 %previous: 47% %lc: 52 4 0%
49: output: FinBF 49 single: 44 %previous: 48% %lc: 53 4 0%
50: output: FinT3 50 single: 45 %previous: 49% %lc: 54 4 0%
51: output: GoodEndRPr 51 pure: %previous: 50% %lc: 55 4 0%
52: output: T3RPr 52 pure: %previous: 51% %lc: 56 4 0%
53: local: pure: %name: BF_ArmXcmove% %previous: 52% %lc: 85 4 0%
54: local: pure: %name: START_AppliArmX% %previous: 53% %lc: 86 4 0%
55: local: pure: %name: T2_outwork% %previous: 54% %lc: 87 4 0%
56: local: pure: %name: T3_ArmXcmove% %previous: 55% %lc: 88 4 0%
57: local: pure: %name: BF_ArmXfmove% %previous: 56% %lc: 89 4 0%
58: local: pure: %name: BF_ArmXjmove% %previous: 57% %lc: 90 4 0%
59: local: pure: %name: T3_AppliArmX% %previous: 58% %lc: 91 4 0%
60: local: single: 46 %name: Prev_rt_WinX% %previous: 59% %lc: 92 4 0%
61: local: pure: %name: StartTransite_WinX% %previous: 60% %lc: 93 4 0%
62: local: pure: %name: FinTransite_WinX% %previous: 61% %lc: 94 4 0%
63: local: pure: %name: FirstRT_WinX% %previous: 62% %lc: 95 4 0%
64: local: single: 47 %name: Reparam_WinX% %previous: 63% %lc: 96 4 0%
65: local: pure: %name: Abort_Local_AppliArmX% %previous: 64% %lc: 97 4 0%
66: local: pure: %name: Abort_Local_ArmXjmove% %previous: 65% %lc: 98 4 0%
67: local: pure: %name: START_ArmXfmove% %previous: 66% %lc: 99 4 0%
68: local: pure: %name: Abort_Local_ArmXfmove% %previous: 67% %lc: 100 4 0%
69: local: pure: %name: START_ArmXcmove% %previous: 68% %lc: 101 4 0%
70: local: pure: %name: Abort_Local_ArmXcmove% %previous: 69% %lc: 102 4 0%
71: local: pure: %name: START_ArmXjmove% %previous: 70% %lc: 103 4 0%
72: local: pure: %name: T2_inwork% %previous: 71% %lc: 104 4 0%
73: local: pure: %name: T2_reconf% %previous: 72% %lc: 105 4 0%
74: local: pure: %name: T3_ArmXfmove% %previous: 73% %lc: 106 4 0%
75: local: pure: %name: T3_ArmXjmove% %previous: 74% %lc: 107 4 0%
76: local: pure: %name: ReadyToStart_WinX% %previous: 75% %lc: 108 4 0%
77: local: pure: %name: BF_AppliArmX% %previous: 76% %lc: 109 4 0%
78: local: pure: %name: T2% %previous: 77% %lc: 110 4 0%
79: local: pure: %name: BF_controle_prr% %previous: 78% %lc: 111 4 0%
80: local: pure: %name: T3_controle_prr% %previous: 79% %lc: 112 4 0%
81: exception: pure: %name: T3_prr% %lc: 114 6 0%
82: exception: pure: %name: BF_prr% %lc: 115 6 0%
83: exception: pure: %name: T3% %lc: 39 6 2%
84: exception: pure: %name: Abort% %lc: 43 8 2%
85: exception: pure: %name: T2% %lc: 64 8 2%
86: exception: pure: %name: BF% %lc: 65 9 2%
87: exception: pure: %name: T3% %lc: 42 6 3%
88: exception: pure: %name: Abort% %lc: 46 8 3%
89: exception: pure: %name: T2% %lc: 67 8 3%
90: exception: pure: %name: BF% %lc: 68 9 3%
91: exception: pure: %name: T3% %lc: 43 6 4%
92: exception: pure: %name: Abort% %lc: 47 8 4%
93: exception: pure: %name: T2% %lc: 68 8 4%
94: exception: pure: %name: BF% %lc: 69 9 4%
95: local: pure: %name: L_T2_outwork% %previous: 80% %lc: 56 8 5%
96: local: pure: %name: L_T2_inwork% %previous: 95% %lc: 57 2 5%
97: local: pure: %name: L_T2_reconf% %previous: 96% %lc: 58 2 5%
98: local: pure: %name: dummylocalsignal% %previous: 97% %lc: 59 3 5%
99: exception: pure: %name: Abort% %lc: 65 6 5%
100: exception: pure: %name: T3_controle_AppliArmX% %lc: 66 7 5%
101: exception: pure: %name: BF_controle_AppliArmX% %lc: 67 8 5%
102: exception: pure: %name: E_T2_reconf% %lc: 84 6 5%
103: exception: pure: %name: E_T2_outwork% %lc: 85 6 5%
end:

exclusions: 1
0: (1,2,3,7,10,8,12,9,13,14,15,16,17,18,19,11,20,21,22,23,24,25,26,27)  %lc: 59 4 0%
end:

variables: 48
0: 0 %sigval: 0% %lc: 4 7 0%
1: $0 %sigbool: 0% %lc: 4 7 0%
2: $0 %sigbool: 1% %lc: 5 4 0%
3: $0 %sigbool: 2% %lc: 6 4 0%
4: $0 %sigbool: 3% %lc: 7 4 0%
5: $0 %sigbool: 4% %lc: 8 4 0%
6: $0 %sigbool: 5% %lc: 9 4 0%
7: $0 %sigbool: 6% %lc: 10 4 0%
8: $0 %sigbool: 7% %lc: 11 4 0%
9: $0 %sigbool: 8% %lc: 12 4 0%
10: 0 %sigval: 9% %lc: 13 4 0%
11: $0 %sigbool: 9% %lc: 13 4 0%
12: 0 %sigval: 10% %lc: 14 4 0%
13: $0 %sigbool: 10% %lc: 14 4 0%
14: 0 %sigval: 11% %lc: 15 4 0%
15: $0 %sigbool: 11% %lc: 15 4 0%
16: $0 %sigbool: 12% %lc: 16 4 0%
17: $0 %sigbool: 13% %lc: 17 4 0%
18: $0 %sigbool: 14% %lc: 18 4 0%
19: $0 %sigbool: 15% %lc: 19 4 0%
20: $0 %sigbool: 16% %lc: 20 4 0%
21: $0 %sigbool: 17% %lc: 21 4 0%
22: $0 %sigbool: 18% %lc: 22 4 0%
23: $0 %sigbool: 19% %lc: 23 4 0%
24: $0 %sigbool: 20% %lc: 24 4 0%
25: $0 %sigbool: 21% %lc: 25 4 0%
26: $0 %sigbool: 22% %lc: 26 4 0%
27: $0 %sigbool: 23% %lc: 27 4 0%
28: $0 %sigbool: 24% %lc: 28 4 0%
29: $0 %sigbool: 25% %lc: 29 4 0%
30: $0 %sigbool: 26% %lc: 30 4 0%
31: $0 %sigbool: 27% %lc: 31 4 0%
32: 0 %sigval: 28% %lc: 33 8 0%
33: 0 %sigval: 29% %lc: 33 33 0%
34: 0 %sigval: 30% %lc: 34 4 0%
35: 0 %sigval: 31% %lc: 35 4 0%
36: 0 %sigval: 36% %lc: 40 4 0%
37: 0 %sigval: 37% %lc: 41 4 0%
38: 0 %sigval: 38% %lc: 42 4 0%
39: 0 %sigval: 41% %lc: 45 4 0%
40: 0 %sigval: 42% %lc: 46 4 0%
41: 0 %sigval: 43% %lc: 47 4 0%
42: 0 %sigval: 46% %lc: 50 4 0%
43: 0 %sigval: 48% %lc: 52 4 0%
44: 0 %sigval: 49% %lc: 53 4 0%
45: 0 %sigval: 50% %lc: 54 4 0%
46: 0 %sigval: 60% %lc: 92 4 0%
47: $0 %sigval: 64% %lc: 96 4 0%
end:

actions: 89
0: present: 0 %lc: 4 7 0%
1: present: 1 %lc: 5 4 0%
2: present: 2 %lc: 6 4 0%
3: present: 3 %lc: 7 4 0%
4: present: 4 %lc: 8 4 0%
5: present: 5 %lc: 9 4 0%
6: present: 6 %lc: 10 4 0%
7: present: 7 %lc: 11 4 0%
8: present: 8 %lc: 12 4 0%
9: present: 9 %lc: 13 4 0%
10: present: 10 %lc: 14 4 0%
11: present: 11 %lc: 15 4 0%
12: present: 12 %lc: 16 4 0%
13: present: 13 %lc: 17 4 0%
14: present: 14 %lc: 18 4 0%
15: present: 15 %lc: 19 4 0%
16: present: 16 %lc: 20 4 0%
17: present: 17 %lc: 21 4 0%
18: present: 18 %lc: 22 4 0%
19: present: 19 %lc: 23 4 0%
20: present: 20 %lc: 24 4 0%
21: present: 21 %lc: 25 4 0%
22: present: 22 %lc: 26 4 0%
23: present: 23 %lc: 27 4 0%
24: present: 24 %lc: 28 4 0%
25: present: 25 %lc: 29 4 0%
26: present: 26 %lc: 30 4 0%
27: present: 27 %lc: 31 4 0%
28: output: 28 %lc: 33 8 0%
29: output: 29 %lc: 33 33 0%
30: output: 30 %lc: 34 4 0%
31: output: 31 %lc: 35 4 0%
32: output: 32 %lc: 36 4 0%
33: output: 33 %lc: 37 4 0%
34: output: 34 %lc: 38 4 0%
35: output: 35 %lc: 39 4 0%
36: output: 36 %lc: 40 4 0%
37: output: 37 %lc: 41 4 0%
38: output: 38 %lc: 42 4 0%
39: output: 39 %lc: 43 4 0%
40: output: 40 %lc: 44 4 0%
41: output: 41 %lc: 45 4 0%
42: output: 42 %lc: 46 4 0%
43: output: 43 %lc: 47 4 0%
44: output: 44 %lc: 48 4 0%
45: output: 45 %lc: 49 4 0%
46: output: 46 %lc: 50 4 0%
47: output: 47 %lc: 51 4 0%
48: output: 48 %lc: 52 4 0%
49: output: 49 %lc: 53 4 0%
50: output: 50 %lc: 54 4 0%
51: output: 51 %lc: 55 4 0%
52: output: 52 %lc: 56 4 0%
53: reset: 46 %lc: 92 4 0%
54: reset: 47 %lc: 96 4 0%
55: call: 0 (43) (46) %lc: 144 1 0%
56: call: 0 (44) (0) %lc: 145 19 0%
57: call: 0 (45) (0) %lc: 147 19 0%
58: call: 0 (38) (10) %lc: 49 12 2%
59: call: 0 (36) (10) %lc: 56 4 2%
60: call: 0 (46) (10) %lc: 57 4 2%
61: call: 0 (37) (10) %lc: 61 12 2%
62: call: 0 (37) (10) %lc: 70 5 2%
63: call: 0 (37) (10) %lc: 89 5 2%
64: call: 0 (37) (10) %lc: 97 4 2%
65: call: 0 (35) (12) %lc: 52 12 3%
66: call: 0 (33) (12) %lc: 59 4 3%
67: call: 0 (46) (12) %lc: 60 4 3%
68: call: 0 (34) (12) %lc: 64 12 3%
69: call: 0 (34) (12) %lc: 73 5 3%
70: call: 0 (34) (12) %lc: 94 5 3%
71: call: 0 (34) (12) %lc: 102 4 3%
72: call: 0 (41) (14) %lc: 53 12 4%
73: call: 0 (39) (14) %lc: 60 4 4%
74: call: 0 (46) (14) %lc: 61 4 4%
75: call: 0 (40) (14) %lc: 65 12 4%
76: call: 0 (40) (14) %lc: 74 5 4%
77: call: 0 (42) (14) %lc: 79 20 4%
78: call: 0 (40) (14) %lc: 97 5 4%
79: call: 0 (40) (14) %lc: 105 4 4%
80: call: 3 () (14,#"0.0 0.0") %lc: 73 10 5%
81: call: 1 () (14) %lc: 74 10 5%
82: call: 9 () (12) %lc: 89 12 5%
83: call: 7 () (12) %lc: 90 12 5%
84: call: 6 () (10) %lc: 108 10 5%
85: call: 4 () (10) %lc: 109 10 5%
86: call: 3 () (14,#"0.0 0.0") %lc: 123 10 5%
87: call: 1 () (14) %lc: 124 10 5%
88: call: 0 (43) (46) %lc: 166 6 6%
end:

halts: 46
0: %lc: 150 1 0%
1: %lc: 134 1 0%
2: %lc: 183 2 1%
3: %lc: 185 2 1%
4: %lc: 187 2 1%
5: %lc: 189 2 1%
6: %lc: 40 9 2%
7: %lc: 46 4 2%
8: %lc: 55 4 2%
9: %lc: 68 5 2%
10: %lc: 74 4 2%
11: %lc: 76 4 2%
12: %lc: 78 4 2%
13: %lc: 80 4 2%
14: %lc: 84 2 2%
15: %lc: 43 9 3%
16: %lc: 49 4 3%
17: %lc: 58 4 3%
18: %lc: 71 5 3%
19: %lc: 77 4 3%
20: %lc: 79 4 3%
21: %lc: 81 4 3%
22: %lc: 83 4 3%
23: %lc: 85 4 3%
24: %lc: 89 2 3%
25: %lc: 44 9 4%
26: %lc: 50 4 4%
27: %lc: 59 4 4%
28: %lc: 72 5 4%
29: %lc: 79 4 4%
30: %lc: 82 4 4%
31: %lc: 84 4 4%
32: %lc: 86 4 4%
33: %lc: 88 4 4%
34: %lc: 92 4 4%
35: %lc: 62 1 5%
36: %lc: 76 5 5%
37: %lc: 92 7 5%
38: %lc: 100 1 5%
39: %lc: 111 5 5%
40: %lc: 126 5 5%
41: %lc: 145 6 5%
42: %lc: 160 3 6%
43: %lc: 162 6 6%
44: %lc: 164 8 6%
45: %lc: 167 6 6%
end:

nets: 916
registers: 46
signals: 104

-- Signal 0
0: Prr_Start__I_ in: 0 ift: 0
%inst: 0%
1: PRESENT_S0_0_ 
0
%inst: 0%
%sip: 0 0 4 7 0%
2: UPDATED_S0_0_ 
access: (0 )
$1
%inst: 0%
%siu: 0 0 4 7 0%
3: TRACE_S0_ sig: 0
1
%inst: 0%
%lc: 4 7 0%
-- Signal 1
4: typetraj__I_ in: 1 ift: 1
%inst: 0%
5: PRESENT_S1_0_ 
4
%inst: 0%
%sip: 1 0 5 4 0%
6: TRACE_S1_ sig: 1
5
%inst: 0%
%lc: 5 4 0%
-- Signal 2
7: inwork__I_ in: 2 ift: 2
%inst: 0%
8: PRESENT_S2_0_ 
7
%inst: 0%
%sip: 2 0 6 4 0%
9: TRACE_S2_ sig: 2
8
%inst: 0%
%lc: 6 4 0%
-- Signal 3
10: outwork__I_ in: 3 ift: 3
%inst: 0%
11: PRESENT_S3_0_ 
10
%inst: 0%
%sip: 3 0 7 4 0%
12: TRACE_S3_ sig: 3
11
%inst: 0%
%lc: 7 4 0%
-- Signal 4
13: USERSTART_ArmXfmove__I_ in: 4 ift: 4
%inst: 0%
14: TRACE_S4_ sig: 4
$0
%inst: 0%
%lc: 8 4 0%
-- Signal 5
15: USERSTART_ArmXcmove__I_ in: 5 ift: 5
%inst: 0%
16: TRACE_S5_ sig: 5
$0
%inst: 0%
%lc: 9 4 0%
-- Signal 6
17: USERSTART_ArmXjmove__I_ in: 6 ift: 6
%inst: 0%
18: TRACE_S6_ sig: 6
$0
%inst: 0%
%lc: 10 4 0%
-- Signal 7
19: reconf__I_ in: 7 ift: 7
%inst: 0%
20: PRESENT_S7_0_ 
19
%inst: 0%
%sip: 7 0 11 4 0%
21: TRACE_S7_ sig: 7
20
%inst: 0%
%lc: 11 4 0%
-- Signal 8
22: InitOK_WinX__I_ in: 8 ift: 8
%inst: 0%
23: PRESENT_S8_0_ 
22
%inst: 0%
%sip: 8 0 12 4 0%
24: TRACE_S8_ sig: 8
23
%inst: 0%
%lc: 12 4 0%
-- Signal 9
25: ArmXfmove_Start__I_ in: 9 ift: 9
%inst: 0%
26: PRESENT_S9_0_ 
25
%inst: 0%
%sip: 9 0 13 4 0%
27: UPDATED_S9_0_ 
access: (25 )
$1
%inst: 0%
%siu: 9 0 13 4 0%
28: TRACE_S9_ sig: 9
26
%inst: 0%
%lc: 13 4 0%
-- Signal 10
29: ArmXcmove_Start__I_ in: 10 ift: 10
%inst: 0%
30: PRESENT_S10_0_ 
29
%inst: 0%
%sip: 10 0 14 4 0%
31: UPDATED_S10_0_ 
access: (29 )
$1
%inst: 0%
%siu: 10 0 14 4 0%
32: TRACE_S10_ sig: 10
30
%inst: 0%
%lc: 14 4 0%
-- Signal 11
33: ArmXjmove_Start__I_ in: 11 ift: 11
%inst: 0%
34: PRESENT_S11_0_ 
33
%inst: 0%
%sip: 11 0 15 4 0%
35: UPDATED_S11_0_ 
access: (33 )
$1
%inst: 0%
%siu: 11 0 15 4 0%
36: TRACE_S11_ sig: 11
34
%inst: 0%
%lc: 15 4 0%
-- Signal 12
37: CmdStopOK_WinX__I_ in: 12 ift: 12
%inst: 0%
38: PRESENT_S12_0_ 
37
%inst: 0%
%sip: 12 0 16 4 0%
39: TRACE_S12_ sig: 12
38
%inst: 0%
%lc: 16 4 0%
-- Signal 13
40: ActivateOK_WinX__I_ in: 13 ift: 13
%inst: 0%
41: TRACE_S13_ sig: 13
$0
%inst: 0%
%lc: 17 4 0%
-- Signal 14
42: T2_ArmXcmove__I_ in: 14 ift: 14
%inst: 0%
43: PRESENT_S14_0_ 
or: (42 556)
%inst: 0%
%sip: 14 0 18 4 0%
44: TRACE_S14_ sig: 14
43
%inst: 0%
%lc: 18 4 0%
-- Signal 15
45: KillOK_WinX__I_ in: 15 ift: 15
%inst: 0%
46: TRACE_S15_ sig: 15
$0
%inst: 0%
%lc: 19 4 0%
-- Signal 16
47: ReadyToStop_WinX__I_ in: 16 ift: 16
%inst: 0%
48: TRACE_S16_ sig: 16
$0
%inst: 0%
%lc: 20 4 0%
-- Signal 17
49: badtraj__I_ in: 17 ift: 17
%inst: 0%
50: PRESENT_S17_0_ 
49
%inst: 0%
%sip: 17 0 21 4 0%
51: TRACE_S17_ sig: 17
50
%inst: 0%
%lc: 21 4 0%
-- Signal 18
52: endtraj__I_ in: 18 ift: 18
%inst: 0%
53: PRESENT_S18_0_ 
52
%inst: 0%
%sip: 18 0 22 4 0%
54: TRACE_S18_ sig: 18
53
%inst: 0%
%lc: 22 4 0%
-- Signal 19
55: errtrack__I_ in: 19 ift: 19
%inst: 0%
56: PRESENT_S19_0_ 
55
%inst: 0%
%sip: 19 0 23 4 0%
57: TRACE_S19_ sig: 19
56
%inst: 0%
%lc: 23 4 0%
-- Signal 20
58: outbound__I_ in: 20 ift: 20
%inst: 0%
59: PRESENT_S20_0_ 
58
%inst: 0%
%sip: 20 0 24 4 0%
60: TRACE_S20_ sig: 20
59
%inst: 0%
%lc: 24 4 0%
-- Signal 21
61: redbut__I_ in: 21 ift: 21
%inst: 0%
62: PRESENT_S21_0_ 
61
%inst: 0%
%sip: 21 0 25 4 0%
63: TRACE_S21_ sig: 21
62
%inst: 0%
%lc: 25 4 0%
-- Signal 22
64: T2_ArmXfmove__I_ in: 22 ift: 22
%inst: 0%
65: PRESENT_S22_0_ 
or: (64 433)
%inst: 0%
%sip: 22 0 26 4 0%
66: TRACE_S22_ sig: 22
65
%inst: 0%
%lc: 26 4 0%
-- Signal 23
67: T2_ArmXjmove__I_ in: 23 ift: 23
%inst: 0%
68: TRACE_S23_ sig: 23
$0
%inst: 0%
%lc: 27 4 0%
-- Signal 24
69: ROBOT_FAIL__I_ in: 24 ift: 24
%inst: 0%
70: PRESENT_S24_0_ 
69
%inst: 0%
%sip: 24 0 28 4 0%
71: TRACE_S24_ sig: 24
70
%inst: 0%
%lc: 28 4 0%
-- Signal 25
72: SOFT_FAIL__I_ in: 25 ift: 25
%inst: 0%
73: PRESENT_S25_0_ 
72
%inst: 0%
%sip: 25 0 29 4 0%
74: TRACE_S25_ sig: 25
73
%inst: 0%
%lc: 29 4 0%
-- Signal 26
75: SENSOR_FAIL__I_ in: 26 ift: 26
%inst: 0%
76: PRESENT_S26_0_ 
75
%inst: 0%
%sip: 26 0 30 4 0%
77: TRACE_S26_ sig: 26
76
%inst: 0%
%lc: 30 4 0%
-- Signal 27
78: CPU_OVERLOAD__I_ in: 27 ift: 27
%inst: 0%
79: PRESENT_S27_0_ 
78
%inst: 0%
%sip: 27 0 31 4 0%
80: TRACE_S27_ sig: 27
79
%inst: 0%
%lc: 31 4 0%
-- Signal 28
81: Activate__O_ out: 28 act: 28
82
%inst: 0%
82: TRACE_S28_ sig: 28
$0
%inst: 0%
%lc: 33 8 0%
-- Signal 29
83: ActivateArmXcmove_WinX__O_ out: 29 act: 29
access: (85 )
86
%inst: 0%
84: PRESENT_S29_0_ 
528
%inst: 0%
%sip: 29 0 33 33 0%
85: UPDATED_S29_0_ 
access: (530 )
$1
%inst: 0%
%siu: 29 0 33 33 0%
86: TRACE_S29_ sig: 29
84
%inst: 0%
%lc: 33 33 0%
-- Signal 30
87: ArmXcmoveTransite__O_ out: 30 act: 30
access: (89 )
90
%inst: 0%
88: PRESENT_S30_0_ single: 30
or: (573 556 534)
%inst: 0%
%sip: 30 0 34 4 0%
89: UPDATED_S30_0_ 
access: (576 623 537 )
$1
%inst: 0%
%siu: 30 0 34 4 0%
90: TRACE_S30_ sig: 30
88
%inst: 0%
%lc: 34 4 0%
-- Signal 31
91: Abort_ArmXcmove__O_ out: 31 act: 31
access: (93 )
94
%inst: 0%
92: PRESENT_S31_0_ 
514
%inst: 0%
%sip: 31 0 35 4 0%
93: UPDATED_S31_0_ 
access: (517 )
$1
%inst: 0%
%siu: 31 0 35 4 0%
94: TRACE_S31_ sig: 31
92
%inst: 0%
%lc: 35 4 0%
-- Signal 32
95: STARTED_ArmXcmove__O_ out: 32 act: 32
97
%inst: 0%
96: PRESENT_S32_0_ 
504
%inst: 0%
%sip: 32 0 36 4 0%
97: TRACE_S32_ sig: 32
96
%inst: 0%
%lc: 36 4 0%
-- Signal 33
98: GoodEnd_ArmXcmove__O_ out: 33 act: 33
99
%inst: 0%
99: TRACE_S33_ sig: 33
$0
%inst: 0%
%lc: 37 4 0%
-- Signal 34
100: STARTED_AppliArmX__O_ out: 34 act: 34
102
%inst: 0%
101: PRESENT_S34_0_ 
776
%inst: 0%
%sip: 34 0 38 4 0%
102: TRACE_S34_ sig: 34
101
%inst: 0%
%lc: 38 4 0%
-- Signal 35
103: Abort_AppliArmX__O_ out: 35 act: 35
105
%inst: 0%
104: PRESENT_S35_0_ 
880
%inst: 0%
%sip: 35 0 39 4 0%
105: TRACE_S35_ sig: 35
104
%inst: 0%
%lc: 39 4 0%
-- Signal 36
106: ActivateArmXfmove_WinX__O_ out: 36 act: 36
access: (108 )
109
%inst: 0%
107: PRESENT_S36_0_ 
408
%inst: 0%
%sip: 36 0 40 4 0%
108: UPDATED_S36_0_ 
access: (410 )
$1
%inst: 0%
%siu: 36 0 40 4 0%
109: TRACE_S36_ sig: 36
107
%inst: 0%
%lc: 40 4 0%
-- Signal 37
110: ArmXfmoveTransite__O_ out: 37 act: 37
access: (112 )
113
%inst: 0%
111: PRESENT_S37_0_ single: 37
or: (449 433 414)
%inst: 0%
%sip: 37 0 41 4 0%
112: UPDATED_S37_0_ 
access: (452 491 417 )
$1
%inst: 0%
%siu: 37 0 41 4 0%
113: TRACE_S37_ sig: 37
111
%inst: 0%
%lc: 41 4 0%
-- Signal 38
114: Abort_ArmXfmove__O_ out: 38 act: 38
access: (116 )
117
%inst: 0%
115: PRESENT_S38_0_ 
394
%inst: 0%
%sip: 38 0 42 4 0%
116: UPDATED_S38_0_ 
access: (397 )
$1
%inst: 0%
%siu: 38 0 42 4 0%
117: TRACE_S38_ sig: 38
115
%inst: 0%
%lc: 42 4 0%
-- Signal 39
118: STARTED_ArmXfmove__O_ out: 39 act: 39
120
%inst: 0%
119: PRESENT_S39_0_ 
384
%inst: 0%
%sip: 39 0 43 4 0%
120: TRACE_S39_ sig: 39
119
%inst: 0%
%lc: 43 4 0%
-- Signal 40
121: GoodEnd_ArmXfmove__O_ out: 40 act: 40
122
%inst: 0%
122: TRACE_S40_ sig: 40
$0
%inst: 0%
%lc: 44 4 0%
-- Signal 41
123: ActivateArmXjmove_WinX__O_ out: 41 act: 41
access: (125 )
126
%inst: 0%
124: PRESENT_S41_0_ 
660
%inst: 0%
%sip: 41 0 45 4 0%
125: UPDATED_S41_0_ 
access: (662 )
$1
%inst: 0%
%siu: 41 0 45 4 0%
126: TRACE_S41_ sig: 41
124
%inst: 0%
%lc: 45 4 0%
-- Signal 42
127: ArmXjmoveTransite__O_ out: 42 act: 42
access: (129 )
130
%inst: 0%
128: PRESENT_S42_0_ single: 42
or: (704 687 666)
%inst: 0%
%sip: 42 0 46 4 0%
129: UPDATED_S42_0_ 
access: (707 759 669 )
$1
%inst: 0%
%siu: 42 0 46 4 0%
130: TRACE_S42_ sig: 42
128
%inst: 0%
%lc: 46 4 0%
-- Signal 43
131: Abort_ArmXjmove__O_ out: 43 act: 43
access: (133 )
134
%inst: 0%
132: PRESENT_S43_0_ 
646
%inst: 0%
%sip: 43 0 47 4 0%
133: UPDATED_S43_0_ 
access: (649 )
$1
%inst: 0%
%siu: 43 0 47 4 0%
134: TRACE_S43_ sig: 43
132
%inst: 0%
%lc: 47 4 0%
-- Signal 44
135: STARTED_ArmXjmove__O_ out: 44 act: 44
137
%inst: 0%
136: PRESENT_S44_0_ 
636
%inst: 0%
%sip: 44 0 48 4 0%
137: TRACE_S44_ sig: 44
136
%inst: 0%
%lc: 48 4 0%
-- Signal 45
138: GoodEnd_ArmXjmove__O_ out: 45 act: 45
140
%inst: 0%
139: PRESENT_S45_0_ 
759
%inst: 0%
%sip: 45 0 49 4 0%
140: TRACE_S45_ sig: 45
139
%inst: 0%
%lc: 49 4 0%
-- Signal 46
141: Treattypetraj__O_ out: 46 act: 46
access: (143 )
144
%inst: 0%
142: PRESENT_S46_0_ 
714
%inst: 0%
%sip: 46 0 50 4 0%
143: UPDATED_S46_0_ 
access: (717 )
$1
%inst: 0%
%siu: 46 0 50 4 0%
144: TRACE_S46_ sig: 46
142
%inst: 0%
%lc: 50 4 0%
-- Signal 47
145: GoodEnd_AppliArmX__O_ out: 47 act: 47
146
%inst: 0%
146: TRACE_S47_ sig: 47
$0
%inst: 0%
%lc: 51 4 0%
-- Signal 48
147: EndKill__O_ out: 48 act: 48
access: (149 )
150
%inst: 0%
148: PRESENT_S48_0_ single: 48
or: (904 293)
%inst: 0%
%sip: 48 0 52 4 0%
149: UPDATED_S48_0_ 
access: (907 318 )
$1
%inst: 0%
%siu: 48 0 52 4 0%
150: TRACE_S48_ sig: 48
148
%inst: 0%
%lc: 52 4 0%
-- Signal 49
151: FinBF__O_ out: 49 act: 49
access: (153 )
154
%inst: 0%
152: PRESENT_S49_0_ 
318
%inst: 0%
%sip: 49 0 53 4 0%
153: UPDATED_S49_0_ 
access: (320 )
$1
%inst: 0%
%siu: 49 0 53 4 0%
154: TRACE_S49_ sig: 49
152
%inst: 0%
%lc: 53 4 0%
-- Signal 50
155: FinT3__O_ out: 50 act: 50
access: (157 )
158
%inst: 0%
156: PRESENT_S50_0_ 
296
%inst: 0%
%sip: 50 0 54 4 0%
157: UPDATED_S50_0_ 
access: (322 )
$1
%inst: 0%
%siu: 50 0 54 4 0%
158: TRACE_S50_ sig: 50
156
%inst: 0%
%lc: 54 4 0%
-- Signal 51
159: GoodEndRPr__O_ out: 51 act: 51
161
%inst: 0%
160: PRESENT_S51_0_ 
320
%inst: 0%
%sip: 51 0 55 4 0%
161: TRACE_S51_ sig: 51
160
%inst: 0%
%lc: 55 4 0%
-- Signal 52
162: T3RPr__O_ out: 52 act: 52
164
%inst: 0%
163: PRESENT_S52_0_ 
322
%inst: 0%
%sip: 52 0 56 4 0%
164: TRACE_S52_ sig: 52
163
%inst: 0%
%lc: 56 4 0%
-- Signal 53
165: PRESENT_S53_0_ 
$0
%inst: 0%
%sip: 53 0 85 4 0%
166: TRACE_S53_ sig: 53
165
%inst: 0%
%lc: 85 4 0%
-- Signal 54
167: PRESENT_S54_0_ 
329
%inst: 0%
%sip: 54 0 86 4 0%
168: TRACE_S54_ sig: 54
167
%inst: 0%
%lc: 86 4 0%
-- Signal 55
169: PRESENT_S55_0_ 
583
%inst: 0%
%sip: 55 0 87 4 0%
170: TRACE_S55_ sig: 55
169
%inst: 0%
%lc: 87 4 0%
-- Signal 56
171: PRESENT_S56_0_ 
562
%inst: 0%
%sip: 56 0 88 4 0%
172: TRACE_S56_ sig: 56
171
%inst: 0%
%lc: 88 4 0%
-- Signal 57
173: PRESENT_S57_0_ 
$0
%inst: 0%
%sip: 57 0 89 4 0%
174: TRACE_S57_ sig: 57
173
%inst: 0%
%lc: 89 4 0%
-- Signal 58
175: PRESENT_S58_0_ 
759
%inst: 0%
%sip: 58 0 90 4 0%
176: TRACE_S58_ sig: 58
175
%inst: 0%
%lc: 90 4 0%
-- Signal 59
177: PRESENT_S59_0_ 
786
%inst: 0%
%sip: 59 0 91 4 0%
178: TRACE_S59_ sig: 59
177
%inst: 0%
%lc: 91 4 0%
-- Signal 60
179: PRESENT_S60_0_ single: 60
or: (410 530 662)
%inst: 0%
%sip: 60 0 92 4 0%
180: UPDATED_S60_0_ 
access: (412 532 664 )
$1
%inst: 0%
%siu: 60 0 92 4 0%
181: TRACE_S60_ sig: 60
179
%inst: 0%
%lc: 92 4 0%
-- Signal 61
182: PRESENT_S61_0_ 
or: (449 433 414 573 556 534 704 759 666)
%inst: 0%
%sip: 61 0 93 4 0%
183: TRACE_S61_ sig: 61
182
%inst: 0%
%lc: 93 4 0%
-- Signal 62
184: PRESENT_S62_0_ 
or: (888 914)
%inst: 0%
%sip: 62 0 94 4 0%
185: TRACE_S62_ sig: 62
184
%inst: 0%
%lc: 94 4 0%
-- Signal 63
186: PRESENT_S63_0_ 
329
%inst: 0%
%sip: 63 0 95 4 0%
187: TRACE_S63_ sig: 63
186
%inst: 0%
%lc: 95 4 0%
-- Signal 64
188: TRACE_S64_ sig: 64
$0
%inst: 0%
%lc: 96 4 0%
-- Signal 65
189: PRESENT_S65_0_ 
$0
%inst: 0%
%sip: 65 0 97 4 0%
190: TRACE_S65_ sig: 65
189
%inst: 0%
%lc: 97 4 0%
-- Signal 66
191: PRESENT_S66_0_ 
880
%inst: 0%
%sip: 66 0 98 4 0%
192: TRACE_S66_ sig: 66
191
%inst: 0%
%lc: 98 4 0%
-- Signal 67
193: PRESENT_S67_0_ 
848
%inst: 0%
%sip: 67 0 99 4 0%
194: TRACE_S67_ sig: 67
193
%inst: 0%
%lc: 99 4 0%
-- Signal 68
195: PRESENT_S68_0_ 
880
%inst: 0%
%sip: 68 0 100 4 0%
196: TRACE_S68_ sig: 68
195
%inst: 0%
%lc: 100 4 0%
-- Signal 69
197: PRESENT_S69_0_ 
824
%inst: 0%
%sip: 69 0 101 4 0%
198: TRACE_S69_ sig: 69
197
%inst: 0%
%lc: 101 4 0%
-- Signal 70
199: PRESENT_S70_0_ 
or: (814 816 880)
%inst: 0%
%sip: 70 0 102 4 0%
200: TRACE_S70_ sig: 70
199
%inst: 0%
%lc: 102 4 0%
-- Signal 71
201: PRESENT_S71_0_ 
or: (794 862)
%inst: 0%
%sip: 71 0 103 4 0%
202: TRACE_S71_ sig: 71
201
%inst: 0%
%lc: 103 4 0%
-- Signal 72
203: PRESENT_S72_0_ 
459
%inst: 0%
%sip: 72 0 104 4 0%
204: TRACE_S72_ sig: 72
203
%inst: 0%
%lc: 104 4 0%
-- Signal 73
205: PRESENT_S73_0_ 
591
%inst: 0%
%sip: 73 0 105 4 0%
206: TRACE_S73_ sig: 73
205
%inst: 0%
%lc: 105 4 0%
-- Signal 74
207: PRESENT_S74_0_ 
438
%inst: 0%
%sip: 74 0 106 4 0%
208: TRACE_S74_ sig: 74
207
%inst: 0%
%lc: 106 4 0%
-- Signal 75
209: PRESENT_S75_0_ 
692
%inst: 0%
%sip: 75 0 107 4 0%
210: TRACE_S75_ sig: 75
209
%inst: 0%
%lc: 107 4 0%
-- Signal 76
211: PRESENT_S76_0_ 
or: (391 511 643)
%inst: 0%
%sip: 76 0 108 4 0%
212: TRACE_S76_ sig: 76
211
%inst: 0%
%lc: 108 4 0%
-- Signal 77
213: PRESENT_S77_0_ 
$0
%inst: 0%
%sip: 77 0 109 4 0%
214: TRACE_S77_ sig: 77
213
%inst: 0%
%lc: 109 4 0%
-- Signal 78
215: TRACE_S78_ sig: 78
$0
%inst: 0%
%lc: 110 4 0%
-- Signal 79
216: TRACE_S79_ sig: 79
$0
%inst: 0%
%lc: 111 4 0%
-- Signal 80
217: TRACE_S80_ sig: 80
$0
%inst: 0%
%lc: 112 4 0%
-- Signal 81
218: PRESENT_S81_0_ 
or: (307 309 311 313 315)
%inst: 0%
%sip: 81 0 114 6 0%
219: TRACE_S81_ sig: 81
218
%inst: 0%
%lc: 114 6 0%
-- Signal 82
220: PRESENT_S82_0_ 
305
%inst: 0%
%sip: 82 0 115 6 0%
221: TRACE_S82_ sig: 82
220
%inst: 0%
%lc: 115 6 0%
-- Signal 83
222: PRESENT_S83_0_ 
or: (467 475 483)
%inst: 2%
%sip: 83 0 39 6 2%
223: TRACE_S83_ sig: 83
222
%inst: 2%
%lc: 39 6 2%
-- Signal 84
224: PRESENT_S84_0_ 
or: (397 452 417)
%inst: 2%
%sip: 84 0 43 8 2%
225: TRACE_S84_ sig: 84
224
%inst: 2%
%lc: 43 8 2%
-- Signal 85
226: PRESENT_S85_0_ 
459
%inst: 2%
%sip: 85 0 64 8 2%
227: TRACE_S85_ sig: 85
226
%inst: 2%
%lc: 64 8 2%
-- Signal 86
228: TRACE_S86_ sig: 86
$0
%inst: 2%
%lc: 65 9 2%
-- Signal 87
229: PRESENT_S87_0_ 
or: (599 607 615)
%inst: 3%
%sip: 87 0 42 6 3%
230: TRACE_S87_ sig: 87
229
%inst: 3%
%lc: 42 6 3%
-- Signal 88
231: PRESENT_S88_0_ 
or: (517 576 537)
%inst: 3%
%sip: 88 0 46 8 3%
232: TRACE_S88_ sig: 88
231
%inst: 3%
%lc: 46 8 3%
-- Signal 89
233: PRESENT_S89_0_ 
or: (583 591)
%inst: 3%
%sip: 89 0 67 8 3%
234: TRACE_S89_ sig: 89
233
%inst: 3%
%lc: 67 8 3%
-- Signal 90
235: TRACE_S90_ sig: 90
$0
%inst: 3%
%lc: 68 9 3%
-- Signal 91
236: PRESENT_S91_0_ 
or: (724 732 740 748)
%inst: 4%
%sip: 91 0 43 6 4%
237: TRACE_S91_ sig: 91
236
%inst: 4%
%lc: 43 6 4%
-- Signal 92
238: PRESENT_S92_0_ 
or: (649 707 669)
%inst: 4%
%sip: 92 0 47 8 4%
239: TRACE_S92_ sig: 92
238
%inst: 4%
%lc: 47 8 4%
-- Signal 93
240: TRACE_S93_ sig: 93
$0
%inst: 4%
%lc: 68 8 4%
-- Signal 94
241: PRESENT_S94_0_ 
756
%inst: 4%
%sip: 94 0 69 9 4%
242: TRACE_S94_ sig: 94
241
%inst: 4%
%lc: 69 9 4%
-- Signal 95
243: TRACE_S95_ sig: 95
$0
%inst: 5%
%lc: 56 8 5%
-- Signal 96
244: TRACE_S96_ sig: 96
$0
%inst: 5%
%lc: 57 2 5%
-- Signal 97
245: TRACE_S97_ sig: 97
$0
%inst: 5%
%lc: 58 2 5%
-- Signal 98
246: TRACE_S98_ sig: 98
$0
%inst: 5%
%lc: 59 3 5%
-- Signal 99
247: PRESENT_S99_0_ 
880
%inst: 5%
%sip: 99 0 65 6 5%
248: TRACE_S99_ sig: 99
247
%inst: 5%
%lc: 65 6 5%
-- Signal 100
249: PRESENT_S100_0_ 
or: (833 857 871 803)
%inst: 5%
%sip: 100 0 66 7 5%
250: TRACE_S100_ sig: 100
249
%inst: 5%
%lc: 66 7 5%
-- Signal 101
251: TRACE_S101_ sig: 101
$0
%inst: 5%
%lc: 67 8 5%
-- Signal 102
252: PRESENT_S102_0_ 
843
%inst: 5%
%sip: 102 0 84 6 5%
253: TRACE_S102_ sig: 102
252
%inst: 5%
%lc: 84 6 5%
-- Signal 103
254: PRESENT_S103_0_ 
841
%inst: 5%
%sip: 103 0 85 6 5%
255: TRACE_S103_ sig: 103
254
%inst: 5%
%lc: 85 6 5%
-- Root 0
256: BOOT_REGISTER_ 
reg: $1 $0
%inst: 0%
%lc: 84 1 0%%lc: 85 4 0%%lc: 86 4 0%%lc: 87 4 0%%lc: 88 4 0%%lc: 89 4 0%%lc: 90 4 0%%lc: 91 4 0%%lc: 92 4 0%
257: ROOT_RESUME_ 
$1
%inst: 0%
258: ROOT_KILL_ 
$0
%inst: 0%
259: ROOT_STAY_ 
$0
%inst: 0%
260: ROOT_RETURN_ return: 0
263
%inst: 0%
%lc: 150 1 0%
261: ROOT_HALTING_ halting:
285
%inst: 0%
-- Fork 1
-- Parallel 2
262: DEAD_2_B0_ 
and: (267 !267)
%inst: 0%
263: PARALLEL_2_UNION_K0_0_ 
or: (320 322)
%inst: 0%
264: PARALLEL_2_KILL_0_ 
or: (258 258)
%inst: 0%
-- Statement 3
-- Statement 4
-- Statement 5
-- Statement 6
-- Statement 7
-- Statement 8
-- Statement 9
-- Statement 10
-- Action 11 [53]
265: ACT_53_0_0_ act: 53
256
%inst: 0%
%lc: 92 4 0%%lc: 93 4 0%%lc: 94 4 0%%lc: 95 4 0%%lc: 96 4 0%
%incarn: 0 0%
-- Statement 12
-- Statement 13
-- Statement 14
-- Statement 15
-- Action 16 [54]
266: ACT_54_0_0_ act: 54
265
%inst: 0%
%lc: 96 4 0%%lc: 97 4 0%%lc: 98 4 0%%lc: 99 4 0%%lc: 100 4 0%%lc: 101 4 0%%lc: 102 4 0%%lc: 103 4 0%%lc: 104 4 0%%lc: 105 4 0%%lc: 106 4 0%%lc: 107 4 0%%lc: 108 4 0%%lc: 109 4 0%%lc: 110 4 0%%lc: 111 4 0%%lc: 112 4 0%%lc: 114 6 0%%lc: 115 6 0%%lc: 118 1 0%%lc: 117 1 0%%lc: 119 1 0%%lc: 121 1 0%%lc: 123 1 0%%lc: 125 1 0%%lc: 127 1 0%%lc: 184 2 1%%lc: 39 6 2%%lc: 42 6 3%%lc: 43 6 4%%lc: 56 1 5%%lc: 56 8 5%%lc: 57 2 5%%lc: 58 2 5%%lc: 59 3 5%%lc: 62 7 5%
%incarn: 0 0%
-- Statement 17
-- Statement 18
-- Statement 19
-- Statement 20
-- Statement 21
-- Statement 22
-- Statement 23
-- Statement 24
-- Statement 25
-- Statement 26
-- Statement 27
-- Statement 28
-- Statement 29
-- Statement 30
-- Statement 31
-- Statement 32
-- Statement 33
-- Statement 34
-- Fork 35
-- Parallel 36
267: SELECT_36_ 
or: (323 269 271 273 761 276 301)
%inst: 0%
268: DEAD_36_B0_ 
and: (267 !323)
%inst: 0%
269: SELECT_36_B1_ selectinc:
or: (387 418 400 375)
%inst: 0%
270: DEAD_36_B1_ 
and: (267 !269)
%inst: 0%
271: SELECT_36_B2_ selectinc:
or: (507 538 520 495)
%inst: 0%
272: DEAD_36_B2_ 
and: (267 !271)
%inst: 0%
273: SELECT_36_B3_ selectinc:
or: (639 670 652 627)
%inst: 0%
274: DEAD_36_B3_ 
and: (267 !273)
%inst: 0%
275: DEAD_36_B4_ 
and: (267 !761)
%inst: 0%
276: SELECT_36_B5_ selectinc:
or: (884 892 900 910)
%inst: 0%
277: DEAD_36_B5_ 
and: (267 !276)
%inst: 0%
278: DEAD_36_B6_ 
and: (267 !301)
%inst: 0%
279: PARALLEL_36_MIN_K0_B0_0_ 
or: (268 329)
%inst: 0%
280: PARALLEL_36_MIN_K0_B1_0_ 
or: (270 438)
%inst: 0%
281: PARALLEL_36_MIN_K0_B2_0_ 
or: (272 562)
%inst: 0%
282: PARALLEL_36_MIN_K0_B3_0_ 
or: (274 692)
%inst: 0%
283: PARALLEL_36_MIN_K0_B4_0_ 
or: (275 763)
%inst: 0%
284: PARALLEL_36_UNION_K1_0_ 
or: (335
         388
         426
         401
         376
         508
         547
         521
         496
         640
         679
         653
         628
         764
         885
         893
         901
         911
         302)
%inst: 0%
285: PARALLEL_36_CONT_K1_0_ 
and: (284 286 287 288 289 290 291 292)
%inst: 0%
286: PARALLEL_36_MIN_K1_B0_0_ 
or: (279 335)
%inst: 0%
287: PARALLEL_36_MIN_K1_B1_0_ 
or: (280 388 426 401 376)
%inst: 0%
288: PARALLEL_36_MIN_K1_B2_0_ 
or: (281 508 547 521 496)
%inst: 0%
289: PARALLEL_36_MIN_K1_B3_0_ 
or: (282 640 679 653 628)
%inst: 0%
290: PARALLEL_36_MIN_K1_B4_0_ 
or: (283 764)
%inst: 0%
291: PARALLEL_36_MIN_K1_B5_0_ 
or: (277 885 893 901 911)
%inst: 0%
292: PARALLEL_36_MIN_K1_B6_0_ 
or: (278 302)
%inst: 0%
293: PARALLEL_36_CONT_K2_0_ 
and: (305 286 287 288 289 290 291 294)
%inst: 0%
%lc: 144 1 0%%sies: 48 0 144 1 0%%sias: 60 0 144 14 0%
294: PARALLEL_36_MIN_K2_B6_0_ 
or: (292 305)
%inst: 0%
295: PARALLEL_36_UNION_K3_0_ 
or: (307 309 311 313 315)
%inst: 0%
296: PARALLEL_36_CONT_K3_0_ 
and: (295 286 287 288 289 290 291 297)
%inst: 0%
%lc: 147 19 0%%sies: 50 0 147 19 0%%sias: 0 0 147 30 0%
297: PARALLEL_36_MIN_K3_B6_0_ 
or: (294 307 309 311 313 315)
%inst: 0%
298: PARALLEL_36_KILL_0_ 
or: (264 293 296 264)
%inst: 0%
-- Statement 37
-- Statement 38
-- Statement 39
-- Statement 40
-- Statement 41
-- Statement 42
-- Halt 43 rank 1
299: RESUME_43_ 
or: (259 316)
%inst: 0%
300: MAINTAIN_43_ 
and: (299 301)
%inst: 0%
301: HALT_REG_43_ halt: 1
reg: $0 303
%inst: 0%
302: GO_43_0_ 
or: (266 300)
%inst: 0%
303: TO_REG_43_0_ 
and: (302 !298)
%inst: 0%
%lc: 134 1 0%
-- Watchdog 44
304: TO_PRESENT_44_ 
and: (301 257)
%inst: 0%
%lc: 135 3 0%
-- Present 45
305: THEN_45_0_ 
and: (304 213)
%inst: 0%
%lc: 135 21 0%%sips: 77 0 135 3 0%%lc: 135 24 0%%sies: 82 0 135 24 0%
306: ELSE_45_0_ 
and: (304 !213)
%inst: 0%
%sips: 77 0 135 3 0%%lc: 136 3 0%
-- Statement 46
-- Present 48
307: THEN_48_0_ 
and: (306 177)
%inst: 0%
%lc: 136 21 0%%sips: 59 0 136 3 0%%lc: 136 24 0%%sies: 81 0 136 24 0%
308: ELSE_48_0_ 
and: (306 !177)
%inst: 0%
%sips: 59 0 136 3 0%%lc: 137 3 0%
-- Statement 49
-- Present 51
309: THEN_51_0_ 
and: (308 70)
%inst: 0%
%lc: 137 19 0%%sips: 24 0 137 3 0%%lc: 137 22 0%%sies: 81 0 137 22 0%
310: ELSE_51_0_ 
and: (308 !70)
%inst: 0%
%sips: 24 0 137 3 0%%lc: 138 3 0%
-- Statement 52
-- Present 54
311: THEN_54_0_ 
and: (310 73)
%inst: 0%
%lc: 138 18 0%%sips: 25 0 138 3 0%%lc: 138 21 0%%sies: 81 0 138 21 0%
312: ELSE_54_0_ 
and: (310 !73)
%inst: 0%
%sips: 25 0 138 3 0%%lc: 139 3 0%
-- Statement 55
-- Present 57
313: THEN_57_0_ 
and: (312 76)
%inst: 0%
%lc: 139 20 0%%sips: 26 0 139 3 0%%lc: 139 23 0%%sies: 81 0 139 23 0%
314: ELSE_57_0_ 
and: (312 !76)
%inst: 0%
%sips: 26 0 139 3 0%%lc: 140 3 0%
-- Statement 58
-- Present 60
315: THEN_60_0_ 
and: (314 79)
%inst: 0%
%lc: 140 21 0%%sips: 27 0 140 3 0%%lc: 140 24 0%%sies: 81 0 140 24 0%
316: ELSE_60_0_ 
and: (314 !79)
%inst: 0%
%lc: 134 1 0%%sips: 27 0 140 3 0%
-- Statement 61
-- Statement 64
-- Access 65 [60]
317: CONT_65_0_ 
and: (293 180)
%inst: 0%
-- Action 66 [55]
318: ACT_55_0_0_ act: 55
317
%inst: 0%
%lc: 144 1 0%%sius: 48 0 144 1 0%%lc: 145 19 0%%sies: 49 0 145 19 0%%sias: 0 0 145 30 0%
%incarn: 0 0%
-- Statement 67
-- Statement 68
-- Access 69 [0]
319: CONT_69_0_ 
and: (318 2)
%inst: 0%
-- Action 70 [56]
320: ACT_56_0_0_ act: 56
319
%inst: 0%
%lc: 145 19 0%%sius: 49 0 145 19 0%%lc: 145 43 0%%sies: 51 0 145 43 0%
%incarn: 0 0%
-- Statement 71
-- Statement 72
-- Statement 73
-- Access 74 [0]
321: CONT_74_0_ 
and: (296 2)
%inst: 0%
-- Action 75 [57]
322: ACT_57_0_0_ act: 57
321
%inst: 0%
%lc: 147 19 0%%sius: 50 0 147 19 0%%lc: 147 43 0%%sies: 52 0 147 43 0%
%incarn: 0 0%
-- Statement 76
-- Statement 77
-- Fork 79
-- Parallel 80
323: SELECT_80_ 
or: (343 351 359 367)
%inst: 1%
324: DEAD_80_B0_ 
and: (323 !343)
%inst: 1%
325: DEAD_80_B1_ 
and: (323 !351)
%inst: 1%
326: DEAD_80_B2_ 
and: (323 !359)
%inst: 1%
327: DEAD_80_B3_ 
and: (323 !367)
%inst: 1%
328: PARALLEL_80_UNION_K0_0_ 
or: (347 355 363 371)
%inst: 1%
329: PARALLEL_80_CONT_K0_0_ 
and: (328 330 331 332 333)
%inst: 1%
%lc: 192 1 1%%sies: 63 0 192 1 1%%lc: 193 1 1%%sies: 54 0 193 1 1%
330: PARALLEL_80_MIN_K0_B0_0_ 
or: (324 347)
%inst: 1%
331: PARALLEL_80_MIN_K0_B1_0_ 
or: (325 355)
%inst: 1%
332: PARALLEL_80_MIN_K0_B2_0_ 
or: (326 363)
%inst: 1%
333: PARALLEL_80_MIN_K0_B3_0_ 
or: (327 371)
%inst: 1%
334: PARALLEL_80_UNION_K1_0_ 
or: (344 352 360 368)
%inst: 1%
335: PARALLEL_80_CONT_K1_0_ 
and: (334 336 337 338 339)
%inst: 1%
336: PARALLEL_80_MIN_K1_B0_0_ 
or: (330 344)
%inst: 1%
337: PARALLEL_80_MIN_K1_B1_0_ 
or: (331 352)
%inst: 1%
338: PARALLEL_80_MIN_K1_B2_0_ 
or: (332 360)
%inst: 1%
339: PARALLEL_80_MIN_K1_B3_0_ 
or: (333 368)
%inst: 1%
340: PARALLEL_80_KILL_0_ 
or: (298 298)
%inst: 1%
-- Halt 81 rank 2
341: RESUME_81_ 
or: (259 348)
%inst: 1%
342: MAINTAIN_81_ 
and: (341 343)
%inst: 1%
343: HALT_REG_81_ halt: 2
reg: $0 345
%inst: 1%
344: GO_81_0_ 
or: (266 342)
%inst: 1%
345: TO_REG_81_0_ 
and: (344 !340)
%inst: 1%
%lc: 183 2 1%
-- Watchdog 82
346: TO_PRESENT_82_ 
and: (343 257)
%inst: 1%
%lc: 183 2 1%
-- Present 83
347: THEN_83_0_ 
and: (346 26)
%inst: 1%
%sips: 9 0 183 2 1%
348: ELSE_83_0_ 
and: (346 !26)
%inst: 1%
%sips: 9 0 183 2 1%
-- Halt 84 rank 3
349: RESUME_84_ 
or: (259 356)
%inst: 1%
350: MAINTAIN_84_ 
and: (349 351)
%inst: 1%
351: HALT_REG_84_ halt: 3
reg: $0 353
%inst: 1%
352: GO_84_0_ 
or: (266 350)
%inst: 1%
353: TO_REG_84_0_ 
and: (352 !340)
%inst: 1%
%lc: 185 2 1%
-- Watchdog 85
354: TO_PRESENT_85_ 
and: (351 257)
%inst: 1%
%lc: 185 2 1%
-- Present 86
355: THEN_86_0_ 
and: (354 30)
%inst: 1%
%sips: 10 0 185 2 1%
356: ELSE_86_0_ 
and: (354 !30)
%inst: 1%
%sips: 10 0 185 2 1%
-- Halt 87 rank 4
357: RESUME_87_ 
or: (259 364)
%inst: 1%
358: MAINTAIN_87_ 
and: (357 359)
%inst: 1%
359: HALT_REG_87_ halt: 4
reg: $0 361
%inst: 1%
360: GO_87_0_ 
or: (266 358)
%inst: 1%
361: TO_REG_87_0_ 
and: (360 !340)
%inst: 1%
%lc: 187 2 1%
-- Watchdog 88
362: TO_PRESENT_88_ 
and: (359 257)
%inst: 1%
%lc: 187 2 1%
-- Present 89
363: THEN_89_0_ 
and: (362 34)
%inst: 1%
%sips: 11 0 187 2 1%
364: ELSE_89_0_ 
and: (362 !34)
%inst: 1%
%sips: 11 0 187 2 1%
-- Halt 90 rank 5
365: RESUME_90_ 
or: (259 372)
%inst: 1%
366: MAINTAIN_90_ 
and: (365 367)
%inst: 1%
367: HALT_REG_90_ halt: 5
reg: $0 369
%inst: 1%
368: GO_90_0_ 
or: (266 366)
%inst: 1%
369: TO_REG_90_0_ 
and: (368 !340)
%inst: 1%
%lc: 189 2 1%
-- Watchdog 91
370: TO_PRESENT_91_ 
and: (367 257)
%inst: 1%
%lc: 189 2 1%
-- Present 92
371: THEN_92_0_ 
and: (370 1)
%inst: 1%
%sips: 0 0 189 2 1%
372: ELSE_92_0_ 
and: (370 !1)
%inst: 1%
%sips: 0 0 189 2 1%
-- Statement 94
-- Statement 95
-- Statement 96
-- Halt 97 rank 6
373: RESUME_97_ 
or: (259 380)
%inst: 2%
374: MAINTAIN_97_ 
and: (373 375)
%inst: 2%
375: HALT_REG_97_ halt: 6
reg: $0 377
%inst: 2%
376: GO_97_0_ 
or: (383 374)
%inst: 2%
377: TO_REG_97_0_ 
and: (376 !298)
%inst: 2%
%lc: 40 9 2%
-- Watchdog 98
378: TO_PRESENT_98_ 
and: (375 257)
%inst: 2%
%lc: 40 9 2%
-- Present 99
379: THEN_99_0_ 
and: (378 193)
%inst: 2%
%sips: 67 0 40 9 2%
380: ELSE_99_0_ 
and: (378 !193)
%inst: 2%
%sips: 67 0 40 9 2%
-- Present 100
381: GO_100_0_ 
or: (492 266)
%inst: 2%
%lc: 40 15 2%
382: THEN_100_0_ 
and: (381 193)
%inst: 2%
%sips: 67 0 40 15 2%
383: ELSE_100_0_ 
and: (381 !193)
%inst: 2%
%sips: 67 0 40 15 2%
-- Statement 101
384: GO_101_0_ 
or: (382 379)
%inst: 2%
%lc: 41 3 2%%sies: 39 0 41 3 2%%lc: 43 8 2%
-- Statement 102
-- Halt 103 rank 7
385: RESUME_103_ 
or: (259 392)
%inst: 2%
386: MAINTAIN_103_ 
and: (385 387)
%inst: 2%
387: HALT_REG_103_ halt: 7
reg: $0 389
%inst: 2%
388: GO_103_0_ 
or: (384 386)
%inst: 2%
389: TO_REG_103_0_ 
and: (388 !298)
%inst: 2%
%lc: 46 4 2%
-- Watchdog 104
390: TO_PRESENT_104_ 
and: (387 395)
%inst: 2%
%lc: 46 4 2%
-- Present 105
391: THEN_105_0_ 
and: (390 23)
%inst: 2%
%sips: 8 0 46 4 2%%lc: 54 4 2%%sies: 76 0 54 4 2%%lc: 55 10 2%
392: ELSE_105_0_ 
and: (390 !23)
%inst: 2%
%sips: 8 0 46 4 2%
-- Watchdog 106
393: TO_PRESENT_106_ 
and: (387 257)
%inst: 2%
%lc: 48 3 2%
-- Present 107
394: THEN_107_0_ 
and: (393 195)
%inst: 2%
%lc: 49 4 2%%sips: 68 0 48 3 2%%lc: 49 12 2%%sies: 38 0 49 12 2%%sias: 9 0 49 33 2%
395: ELSE_107_0_ 
and: (393 !195)
%inst: 2%
%lc: 44 3 2%%sips: 68 0 48 3 2%
-- Statement 108
-- Access 109 [9]
396: CONT_109_0_ 
and: (394 27)
%inst: 2%
-- Action 110 [58]
397: ACT_58_0_0_ act: 58
396
%inst: 2%
%lc: 49 12 2%%sius: 38 0 49 12 2%%lc: 49 52 2%%sies: 84 0 49 52 2%
%incarn: 0 0%
-- Statement 111
-- Statement 112
-- Statement 113
-- Halt 114 rank 8
398: RESUME_114_ 
or: (259 405)
%inst: 2%
399: MAINTAIN_114_ 
and: (398 400)
%inst: 2%
400: HALT_REG_114_ halt: 8
reg: $0 402
%inst: 2%
401: GO_114_0_ 
or: (407 399)
%inst: 2%
402: TO_REG_114_0_ 
and: (401 !298)
%inst: 2%
%lc: 55 4 2%
-- Watchdog 115
403: TO_PRESENT_115_ 
and: (400 415)
%inst: 2%
%lc: 55 4 2%
-- Present 116
404: THEN_116_0_ 
and: (403 184)
%inst: 2%
%sips: 62 0 55 4 2%
405: ELSE_116_0_ 
and: (403 !184)
%inst: 2%
%sips: 62 0 55 4 2%
-- Present 117
406: THEN_117_0_ 
and: (391 184)
%inst: 2%
%sips: 62 0 55 10 2%
407: ELSE_117_0_ 
and: (391 !184)
%inst: 2%
%sips: 62 0 55 10 2%
-- Statement 118
408: GO_118_0_ 
or: (406 404)
%inst: 2%
%lc: 56 4 2%%sies: 36 0 56 4 2%%sias: 9 0 56 32 2%
-- Access 119 [9]
409: CONT_119_0_ 
and: (408 27)
%inst: 2%
-- Action 120 [59]
410: ACT_59_0_0_ act: 59
409
%inst: 2%
%lc: 56 4 2%%sius: 36 0 56 4 2%%lc: 57 4 2%%sies: 60 0 57 4 2%%sias: 9 0 57 22 2%
%incarn: 0 0%
-- Statement 121
-- Statement 122
-- Access 123 [9]
411: CONT_123_0_ 
and: (410 27)
%inst: 2%
-- Action 124 [60]
412: ACT_60_0_0_ act: 60
411
%inst: 2%
%lc: 57 4 2%%sius: 60 0 57 4 2%%lc: 64 8 2%%lc: 65 9 2%%lc: 73 3 2%
%incarn: 0 0%
-- Statement 125
-- Watchdog 126
413: TO_PRESENT_126_ 
and: (400 257)
%inst: 2%
%lc: 59 3 2%
-- Present 127
414: THEN_127_0_ 
and: (413 195)
%inst: 2%
%lc: 60 4 2%%sips: 68 0 59 3 2%%lc: 60 12 2%%sies: 61 0 60 12 2%%lc: 61 12 2%%sies: 37 0 61 12 2%%sias: 9 0 61 35 2%
415: ELSE_127_0_ 
and: (413 !195)
%inst: 2%
%lc: 52 3 2%%sips: 68 0 59 3 2%
-- Statement 128
-- Statement 129
-- Access 130 [9]
416: CONT_130_0_ 
and: (414 27)
%inst: 2%
-- Action 131 [61]
417: ACT_61_0_0_ act: 61
416
%inst: 2%
%lc: 61 12 2%%sius: 37 0 61 12 2%%lc: 61 54 2%%sies: 84 0 61 54 2%
%incarn: 0 0%
-- Statement 132
-- Statement 133
-- Statement 134
-- Statement 135
-- Fork 136
-- Parallel 137
418: SELECT_137_ 
or: (445 455 463 471 479 487)
%inst: 2%
419: DEAD_137_B0_ 
and: (418 !445)
%inst: 2%
420: DEAD_137_B1_ 
and: (418 !455)
%inst: 2%
421: DEAD_137_B2_ 
and: (418 !463)
%inst: 2%
422: DEAD_137_B3_ 
and: (418 !471)
%inst: 2%
423: DEAD_137_B4_ 
and: (418 !479)
%inst: 2%
424: DEAD_137_B5_ 
and: (418 !487)
%inst: 2%
425: PARALLEL_137_UNION_K1_0_ 
or: (446 456 464 472 480 488)
%inst: 2%
426: PARALLEL_137_CONT_K1_0_ 
and: (425 427 428 429 430 431 432)
%inst: 2%
427: PARALLEL_137_MIN_K1_B0_0_ 
or: (419 446)
%inst: 2%
428: PARALLEL_137_MIN_K1_B1_0_ 
or: (420 456)
%inst: 2%
429: PARALLEL_137_MIN_K1_B2_0_ 
or: (421 464)
%inst: 2%
430: PARALLEL_137_MIN_K1_B3_0_ 
or: (422 472)
%inst: 2%
431: PARALLEL_137_MIN_K1_B4_0_ 
or: (423 480)
%inst: 2%
432: PARALLEL_137_MIN_K1_B5_0_ 
or: (424 488)
%inst: 2%
433: PARALLEL_137_CONT_K3_0_ 
and: (459 427 434 429 430 431 432)
%inst: 2%
%lc: 95 4 2%%sies: 22 0 95 4 2%%lc: 96 4 2%%sies: 61 0 96 4 2%%lc: 97 4 2%%sies: 37 0 97 4 2%%sias: 9 0 97 27 2%
434: PARALLEL_137_MIN_K3_B1_0_ 
or: (428 459)
%inst: 2%
435: PARALLEL_137_CONT_K4_0_ 
and: (452 436 434 429 430 431 432)
%inst: 2%
436: PARALLEL_137_MIN_K4_B0_0_ 
or: (427 452)
%inst: 2%
437: PARALLEL_137_UNION_K5_0_ 
or: (467 475 483)
%inst: 2%
438: PARALLEL_137_CONT_K5_0_ 
and: (437 436 434 439 440 441 432)
%inst: 2%
%lc: 103 14 2%%sies: 74 0 103 14 2%
439: PARALLEL_137_MIN_K5_B2_0_ 
or: (429 467)
%inst: 2%
440: PARALLEL_137_MIN_K5_B3_0_ 
or: (430 475)
%inst: 2%
441: PARALLEL_137_MIN_K5_B4_0_ 
or: (431 483)
%inst: 2%
442: PARALLEL_137_KILL_0_ 
or: (298 433 435 438 298)
%inst: 2%
-- Halt 138 rank 9
443: RESUME_138_ 
or: (259 450)
%inst: 2%
444: MAINTAIN_138_ 
and: (443 445)
%inst: 2%
445: HALT_REG_138_ halt: 9
reg: $0 447
%inst: 2%
446: GO_138_0_ 
or: (412 444)
%inst: 2%
447: TO_REG_138_0_ 
and: (446 !442)
%inst: 2%
%lc: 68 5 2%
-- Watchdog 139
448: TO_PRESENT_139_ 
and: (445 257)
%inst: 2%
%lc: 68 5 2%
-- Present 140
449: THEN_140_0_ 
and: (448 195)
%inst: 2%
%sips: 68 0 68 5 2%%lc: 69 5 2%%sies: 61 0 69 5 2%%lc: 70 5 2%%sies: 37 0 70 5 2%%sias: 9 0 70 28 2%
450: ELSE_140_0_ 
and: (448 !195)
%inst: 2%
%sips: 68 0 68 5 2%
-- Statement 141
-- Statement 142
-- Access 143 [9]
451: CONT_143_0_ 
and: (449 27)
%inst: 2%
-- Action 144 [62]
452: ACT_62_0_0_ act: 62
451
%inst: 2%
%lc: 70 5 2%%sius: 37 0 70 5 2%%lc: 71 5 2%%sies: 84 0 71 5 2%
%incarn: 0 0%
-- Statement 145
-- Statement 146
-- Halt 148 rank 10
453: RESUME_148_ 
or: (259 460)
%inst: 2%
454: MAINTAIN_148_ 
and: (453 455)
%inst: 2%
455: HALT_REG_148_ halt: 10
reg: $0 457
%inst: 2%
456: GO_148_0_ 
or: (412 454)
%inst: 2%
457: TO_REG_148_0_ 
and: (456 !442)
%inst: 2%
%lc: 74 4 2%
-- Watchdog 149
458: TO_PRESENT_149_ 
and: (455 257)
%inst: 2%
%lc: 74 4 2%
-- Present 150
459: THEN_150_0_ 
and: (458 8)
%inst: 2%
%sips: 2 0 74 4 2%%lc: 74 18 2%%sies: 72 0 74 18 2%%lc: 74 34 2%%sies: 85 0 74 34 2%
460: ELSE_150_0_ 
and: (458 !8)
%inst: 2%
%sips: 2 0 74 4 2%
-- Statement 151
-- Statement 152
-- Halt 154 rank 11
461: RESUME_154_ 
or: (259 468)
%inst: 2%
462: MAINTAIN_154_ 
and: (461 463)
%inst: 2%
463: HALT_REG_154_ halt: 11
reg: $0 465
%inst: 2%
464: GO_154_0_ 
or: (412 462)
%inst: 2%
465: TO_REG_154_0_ 
and: (464 !442)
%inst: 2%
%lc: 76 4 2%
-- Watchdog 155
466: TO_PRESENT_155_ 
and: (463 257)
%inst: 2%
%lc: 76 4 2%
-- Present 156
467: THEN_156_0_ 
and: (466 56)
%inst: 2%
%sips: 19 0 76 4 2%%lc: 76 20 2%%sies: 83 0 76 20 2%
468: ELSE_156_0_ 
and: (466 !56)
%inst: 2%
%sips: 19 0 76 4 2%
-- Statement 157
-- Halt 159 rank 12
469: RESUME_159_ 
or: (259 476)
%inst: 2%
470: MAINTAIN_159_ 
and: (469 471)
%inst: 2%
471: HALT_REG_159_ halt: 12
reg: $0 473
%inst: 2%
472: GO_159_0_ 
or: (412 470)
%inst: 2%
473: TO_REG_159_0_ 
and: (472 !442)
%inst: 2%
%lc: 78 4 2%
-- Watchdog 160
474: TO_PRESENT_160_ 
and: (471 257)
%inst: 2%
%lc: 78 4 2%
-- Present 161
475: THEN_161_0_ 
and: (474 59)
%inst: 2%
%sips: 20 0 78 4 2%%lc: 78 20 2%%sies: 83 0 78 20 2%
476: ELSE_161_0_ 
and: (474 !59)
%inst: 2%
%sips: 20 0 78 4 2%
-- Statement 162
-- Halt 164 rank 13
477: RESUME_164_ 
or: (259 484)
%inst: 2%
478: MAINTAIN_164_ 
and: (477 479)
%inst: 2%
479: HALT_REG_164_ halt: 13
reg: $0 481
%inst: 2%
480: GO_164_0_ 
or: (412 478)
%inst: 2%
481: TO_REG_164_0_ 
and: (480 !442)
%inst: 2%
%lc: 80 4 2%
-- Watchdog 165
482: TO_PRESENT_165_ 
and: (479 257)
%inst: 2%
%lc: 80 4 2%
-- Present 166
483: THEN_166_0_ 
and: (482 62)
%inst: 2%
%sips: 21 0 80 4 2%%lc: 80 18 2%%sies: 83 0 80 18 2%
484: ELSE_166_0_ 
and: (482 !62)
%inst: 2%
%sips: 21 0 80 4 2%
-- Statement 167
-- Halt 169 rank 14
485: RESUME_169_ 
or: (259 257)
%inst: 2%
486: MAINTAIN_169_ 
and: (485 487)
%inst: 2%
487: HALT_REG_169_ halt: 14
reg: $0 489
%inst: 2%
488: GO_169_0_ 
or: (412 486)
%inst: 2%
489: TO_REG_169_0_ 
and: (488 !442)
%inst: 2%
%lc: 84 2 2%
-- Statement 180
-- Statement 181
-- Statement 182
-- Access 183 [9]
490: CONT_183_0_ 
and: (433 27)
%inst: 2%
-- Action 184 [64]
491: ACT_64_0_0_ act: 64
490
%inst: 2%
%lc: 97 4 2%%sius: 37 0 97 4 2%
%incarn: 0 0%
-- Statement 185
-- Statement 186
492: GO_186_0_ 
or: (397 491 435 417)
%inst: 2%
%lc: 40 2 2%
-- Statement 187
-- Statement 188
-- Halt 189 rank 15
493: RESUME_189_ 
or: (259 500)
%inst: 3%
494: MAINTAIN_189_ 
and: (493 495)
%inst: 3%
495: HALT_REG_189_ halt: 15
reg: $0 497
%inst: 3%
496: GO_189_0_ 
or: (503 494)
%inst: 3%
497: TO_REG_189_0_ 
and: (496 !298)
%inst: 3%
%lc: 43 9 3%
-- Watchdog 190
498: TO_PRESENT_190_ 
and: (495 257)
%inst: 3%
%lc: 43 9 3%
-- Present 191
499: THEN_191_0_ 
and: (498 197)
%inst: 3%
%sips: 69 0 43 9 3%
500: ELSE_191_0_ 
and: (498 !197)
%inst: 3%
%sips: 69 0 43 9 3%
-- Present 192
501: GO_192_0_ 
or: (624 266)
%inst: 3%
%lc: 43 15 3%
502: THEN_192_0_ 
and: (501 197)
%inst: 3%
%sips: 69 0 43 15 3%
503: ELSE_192_0_ 
and: (501 !197)
%inst: 3%
%sips: 69 0 43 15 3%
-- Statement 193
504: GO_193_0_ 
or: (502 499)
%inst: 3%
%lc: 44 3 3%%sies: 32 0 44 3 3%%lc: 46 8 3%
-- Statement 194
-- Halt 195 rank 16
505: RESUME_195_ 
or: (259 512)
%inst: 3%
506: MAINTAIN_195_ 
and: (505 507)
%inst: 3%
507: HALT_REG_195_ halt: 16
reg: $0 509
%inst: 3%
508: GO_195_0_ 
or: (504 506)
%inst: 3%
509: TO_REG_195_0_ 
and: (508 !298)
%inst: 3%
%lc: 49 4 3%
-- Watchdog 196
510: TO_PRESENT_196_ 
and: (507 515)
%inst: 3%
%lc: 49 4 3%
-- Present 197
511: THEN_197_0_ 
and: (510 23)
%inst: 3%
%sips: 8 0 49 4 3%%lc: 57 4 3%%sies: 76 0 57 4 3%%lc: 58 10 3%
512: ELSE_197_0_ 
and: (510 !23)
%inst: 3%
%sips: 8 0 49 4 3%
-- Watchdog 198
513: TO_PRESENT_198_ 
and: (507 257)
%inst: 3%
%lc: 51 3 3%
-- Present 199
514: THEN_199_0_ 
and: (513 199)
%inst: 3%
%lc: 52 4 3%%sips: 70 0 51 3 3%%lc: 52 12 3%%sies: 31 0 52 12 3%%sias: 10 0 52 33 3%
515: ELSE_199_0_ 
and: (513 !199)
%inst: 3%
%lc: 47 3 3%%sips: 70 0 51 3 3%
-- Statement 200
-- Access 201 [10]
516: CONT_201_0_ 
and: (514 31)
%inst: 3%
-- Action 202 [65]
517: ACT_65_0_0_ act: 65
516
%inst: 3%
%lc: 52 12 3%%sius: 31 0 52 12 3%%lc: 52 52 3%%sies: 88 0 52 52 3%
%incarn: 0 0%
-- Statement 203
-- Statement 204
-- Statement 205
-- Halt 206 rank 17
518: RESUME_206_ 
or: (259 525)
%inst: 3%
519: MAINTAIN_206_ 
and: (518 520)
%inst: 3%
520: HALT_REG_206_ halt: 17
reg: $0 522
%inst: 3%
521: GO_206_0_ 
or: (527 519)
%inst: 3%
522: TO_REG_206_0_ 
and: (521 !298)
%inst: 3%
%lc: 58 4 3%
-- Watchdog 207
523: TO_PRESENT_207_ 
and: (520 535)
%inst: 3%
%lc: 58 4 3%
-- Present 208
524: THEN_208_0_ 
and: (523 184)
%inst: 3%
%sips: 62 0 58 4 3%
525: ELSE_208_0_ 
and: (523 !184)
%inst: 3%
%sips: 62 0 58 4 3%
-- Present 209
526: THEN_209_0_ 
and: (511 184)
%inst: 3%
%sips: 62 0 58 10 3%
527: ELSE_209_0_ 
and: (511 !184)
%inst: 3%
%sips: 62 0 58 10 3%
-- Statement 210
528: GO_210_0_ 
or: (526 524)
%inst: 3%
%lc: 59 4 3%%sies: 29 0 59 4 3%%sias: 10 0 59 32 3%
-- Access 211 [10]
529: CONT_211_0_ 
and: (528 31)
%inst: 3%
-- Action 212 [66]
530: ACT_66_0_0_ act: 66
529
%inst: 3%
%lc: 59 4 3%%sius: 29 0 59 4 3%%lc: 60 4 3%%sies: 60 0 60 4 3%%sias: 10 0 60 22 3%
%incarn: 0 0%
-- Statement 213
-- Statement 214
-- Access 215 [10]
531: CONT_215_0_ 
and: (530 31)
%inst: 3%
-- Action 216 [67]
532: ACT_67_0_0_ act: 67
531
%inst: 3%
%lc: 60 4 3%%sius: 60 0 60 4 3%%lc: 67 8 3%%lc: 68 9 3%%lc: 76 3 3%
%incarn: 0 0%
-- Statement 217
-- Watchdog 218
533: TO_PRESENT_218_ 
and: (520 257)
%inst: 3%
%lc: 62 3 3%
-- Present 219
534: THEN_219_0_ 
and: (533 199)
%inst: 3%
%lc: 63 4 3%%sips: 70 0 62 3 3%%lc: 63 12 3%%sies: 61 0 63 12 3%%lc: 64 12 3%%sies: 30 0 64 12 3%%sias: 10 0 64 35 3%
535: ELSE_219_0_ 
and: (533 !199)
%inst: 3%
%lc: 55 3 3%%sips: 70 0 62 3 3%
-- Statement 220
-- Statement 221
-- Access 222 [10]
536: CONT_222_0_ 
and: (534 31)
%inst: 3%
-- Action 223 [68]
537: ACT_68_0_0_ act: 68
536
%inst: 3%
%lc: 64 12 3%%sius: 30 0 64 12 3%%lc: 64 54 3%%sies: 88 0 64 54 3%
%incarn: 0 0%
-- Statement 224
-- Statement 225
-- Statement 226
-- Statement 227
-- Fork 228
-- Parallel 229
538: SELECT_229_ 
or: (569 579 587 595 603 611 619)
%inst: 3%
539: DEAD_229_B0_ 
and: (538 !569)
%inst: 3%
540: DEAD_229_B1_ 
and: (538 !579)
%inst: 3%
541: DEAD_229_B2_ 
and: (538 !587)
%inst: 3%
542: DEAD_229_B3_ 
and: (538 !595)
%inst: 3%
543: DEAD_229_B4_ 
and: (538 !603)
%inst: 3%
544: DEAD_229_B5_ 
and: (538 !611)
%inst: 3%
545: DEAD_229_B6_ 
and: (538 !619)
%inst: 3%
546: PARALLEL_229_UNION_K1_0_ 
or: (570 580 588 596 604 612 620)
%inst: 3%
547: PARALLEL_229_CONT_K1_0_ 
and: (546 548 549 550 551 552 553 554)
%inst: 3%
548: PARALLEL_229_MIN_K1_B0_0_ 
or: (539 570)
%inst: 3%
549: PARALLEL_229_MIN_K1_B1_0_ 
or: (540 580)
%inst: 3%
550: PARALLEL_229_MIN_K1_B2_0_ 
or: (541 588)
%inst: 3%
551: PARALLEL_229_MIN_K1_B3_0_ 
or: (542 596)
%inst: 3%
552: PARALLEL_229_MIN_K1_B4_0_ 
or: (543 604)
%inst: 3%
553: PARALLEL_229_MIN_K1_B5_0_ 
or: (544 612)
%inst: 3%
554: PARALLEL_229_MIN_K1_B6_0_ 
or: (545 620)
%inst: 3%
555: PARALLEL_229_UNION_K3_0_ 
or: (583 591)
%inst: 3%
556: PARALLEL_229_CONT_K3_0_ 
and: (555 548 557 558 551 552 553 554)
%inst: 3%
%lc: 100 4 3%%sies: 14 0 100 4 3%%lc: 101 4 3%%sies: 61 0 101 4 3%%lc: 102 4 3%%sies: 30 0 102 4 3%%sias: 10 0 102 27 3%
557: PARALLEL_229_MIN_K3_B1_0_ 
or: (549 583)
%inst: 3%
558: PARALLEL_229_MIN_K3_B2_0_ 
or: (550 591)
%inst: 3%
559: PARALLEL_229_CONT_K4_0_ 
and: (576 560 557 558 551 552 553 554)
%inst: 3%
560: PARALLEL_229_MIN_K4_B0_0_ 
or: (548 576)
%inst: 3%
561: PARALLEL_229_UNION_K5_0_ 
or: (599 607 615)
%inst: 3%
562: PARALLEL_229_CONT_K5_0_ 
and: (561 560 557 558 563 564 565 554)
%inst: 3%
%lc: 108 14 3%%sies: 56 0 108 14 3%
563: PARALLEL_229_MIN_K5_B3_0_ 
or: (551 599)
%inst: 3%
564: PARALLEL_229_MIN_K5_B4_0_ 
or: (552 607)
%inst: 3%
565: PARALLEL_229_MIN_K5_B5_0_ 
or: (553 615)
%inst: 3%
566: PARALLEL_229_KILL_0_ 
or: (298 556 559 562 298)
%inst: 3%
-- Halt 230 rank 18
567: RESUME_230_ 
or: (259 574)
%inst: 3%
568: MAINTAIN_230_ 
and: (567 569)
%inst: 3%
569: HALT_REG_230_ halt: 18
reg: $0 571
%inst: 3%
570: GO_230_0_ 
or: (532 568)
%inst: 3%
571: TO_REG_230_0_ 
and: (570 !566)
%inst: 3%
%lc: 71 5 3%
-- Watchdog 231
572: TO_PRESENT_231_ 
and: (569 257)
%inst: 3%
%lc: 71 5 3%
-- Present 232
573: THEN_232_0_ 
and: (572 199)
%inst: 3%
%sips: 70 0 71 5 3%%lc: 72 5 3%%sies: 61 0 72 5 3%%lc: 73 5 3%%sies: 30 0 73 5 3%%sias: 10 0 73 28 3%
574: ELSE_232_0_ 
and: (572 !199)
%inst: 3%
%sips: 70 0 71 5 3%
-- Statement 233
-- Statement 234
-- Access 235 [10]
575: CONT_235_0_ 
and: (573 31)
%inst: 3%
-- Action 236 [69]
576: ACT_69_0_0_ act: 69
575
%inst: 3%
%lc: 73 5 3%%sius: 30 0 73 5 3%%lc: 74 5 3%%sies: 88 0 74 5 3%
%incarn: 0 0%
-- Statement 237
-- Statement 238
-- Halt 240 rank 19
577: RESUME_240_ 
or: (259 584)
%inst: 3%
578: MAINTAIN_240_ 
and: (577 579)
%inst: 3%
579: HALT_REG_240_ halt: 19
reg: $0 581
%inst: 3%
580: GO_240_0_ 
or: (532 578)
%inst: 3%
581: TO_REG_240_0_ 
and: (580 !566)
%inst: 3%
%lc: 77 4 3%
-- Watchdog 241
582: TO_PRESENT_241_ 
and: (579 257)
%inst: 3%
%lc: 77 4 3%
-- Present 242
583: THEN_242_0_ 
and: (582 11)
%inst: 3%
%sips: 3 0 77 4 3%%lc: 77 19 3%%sies: 55 0 77 19 3%%lc: 77 36 3%%sies: 89 0 77 36 3%
584: ELSE_242_0_ 
and: (582 !11)
%inst: 3%
%sips: 3 0 77 4 3%
-- Statement 243
-- Statement 244
-- Halt 246 rank 20
585: RESUME_246_ 
or: (259 592)
%inst: 3%
586: MAINTAIN_246_ 
and: (585 587)
%inst: 3%
587: HALT_REG_246_ halt: 20
reg: $0 589
%inst: 3%
588: GO_246_0_ 
or: (532 586)
%inst: 3%
589: TO_REG_246_0_ 
and: (588 !566)
%inst: 3%
%lc: 79 4 3%
-- Watchdog 247
590: TO_PRESENT_247_ 
and: (587 257)
%inst: 3%
%lc: 79 4 3%
-- Present 248
591: THEN_248_0_ 
and: (590 20)
%inst: 3%
%sips: 7 0 79 4 3%%lc: 79 18 3%%sies: 73 0 79 18 3%%lc: 79 34 3%%sies: 89 0 79 34 3%
592: ELSE_248_0_ 
and: (590 !20)
%inst: 3%
%sips: 7 0 79 4 3%
-- Statement 249
-- Statement 250
-- Halt 252 rank 21
593: RESUME_252_ 
or: (259 600)
%inst: 3%
594: MAINTAIN_252_ 
and: (593 595)
%inst: 3%
595: HALT_REG_252_ halt: 21
reg: $0 597
%inst: 3%
596: GO_252_0_ 
or: (532 594)
%inst: 3%
597: TO_REG_252_0_ 
and: (596 !566)
%inst: 3%
%lc: 81 4 3%
-- Watchdog 253
598: TO_PRESENT_253_ 
and: (595 257)
%inst: 3%
%lc: 81 4 3%
-- Present 254
599: THEN_254_0_ 
and: (598 56)
%inst: 3%
%sips: 19 0 81 4 3%%lc: 81 20 3%%sies: 87 0 81 20 3%
600: ELSE_254_0_ 
and: (598 !56)
%inst: 3%
%sips: 19 0 81 4 3%
-- Statement 255
-- Halt 257 rank 22
601: RESUME_257_ 
or: (259 608)
%inst: 3%
602: MAINTAIN_257_ 
and: (601 603)
%inst: 3%
603: HALT_REG_257_ halt: 22
reg: $0 605
%inst: 3%
604: GO_257_0_ 
or: (532 602)
%inst: 3%
605: TO_REG_257_0_ 
and: (604 !566)
%inst: 3%
%lc: 83 4 3%
-- Watchdog 258
606: TO_PRESENT_258_ 
and: (603 257)
%inst: 3%
%lc: 83 4 3%
-- Present 259
607: THEN_259_0_ 
and: (606 59)
%inst: 3%
%sips: 20 0 83 4 3%%lc: 83 20 3%%sies: 87 0 83 20 3%
608: ELSE_259_0_ 
and: (606 !59)
%inst: 3%
%sips: 20 0 83 4 3%
-- Statement 260
-- Halt 262 rank 23
609: RESUME_262_ 
or: (259 616)
%inst: 3%
610: MAINTAIN_262_ 
and: (609 611)
%inst: 3%
611: HALT_REG_262_ halt: 23
reg: $0 613
%inst: 3%
612: GO_262_0_ 
or: (532 610)
%inst: 3%
613: TO_REG_262_0_ 
and: (612 !566)
%inst: 3%
%lc: 85 4 3%
-- Watchdog 263
614: TO_PRESENT_263_ 
and: (611 257)
%inst: 3%
%lc: 85 4 3%
-- Present 264
615: THEN_264_0_ 
and: (614 62)
%inst: 3%
%sips: 21 0 85 4 3%%lc: 85 18 3%%sies: 87 0 85 18 3%
616: ELSE_264_0_ 
and: (614 !62)
%inst: 3%
%sips: 21 0 85 4 3%
-- Statement 265
-- Halt 267 rank 24
617: RESUME_267_ 
or: (259 257)
%inst: 3%
618: MAINTAIN_267_ 
and: (617 619)
%inst: 3%
619: HALT_REG_267_ halt: 24
reg: $0 621
%inst: 3%
620: GO_267_0_ 
or: (532 618)
%inst: 3%
621: TO_REG_267_0_ 
and: (620 !566)
%inst: 3%
%lc: 89 2 3%
-- Statement 278
-- Statement 279
-- Statement 280
-- Access 281 [10]
622: CONT_281_0_ 
and: (556 31)
%inst: 3%
-- Action 282 [71]
623: ACT_71_0_0_ act: 71
622
%inst: 3%
%lc: 102 4 3%%sius: 30 0 102 4 3%
%incarn: 0 0%
-- Statement 283
-- Statement 284
624: GO_284_0_ 
or: (517 623 559 537)
%inst: 3%
%lc: 43 2 3%
-- Statement 285
-- Statement 286
-- Halt 287 rank 25
625: RESUME_287_ 
or: (259 632)
%inst: 4%
626: MAINTAIN_287_ 
and: (625 627)
%inst: 4%
627: HALT_REG_287_ halt: 25
reg: $0 629
%inst: 4%
628: GO_287_0_ 
or: (635 626)
%inst: 4%
629: TO_REG_287_0_ 
and: (628 !298)
%inst: 4%
%lc: 44 9 4%
-- Watchdog 288
630: TO_PRESENT_288_ 
and: (627 257)
%inst: 4%
%lc: 44 9 4%
-- Present 289
631: THEN_289_0_ 
and: (630 201)
%inst: 4%
%sips: 71 0 44 9 4%
632: ELSE_289_0_ 
and: (630 !201)
%inst: 4%
%sips: 71 0 44 9 4%
-- Present 290
633: GO_290_0_ 
or: (760 266)
%inst: 4%
%lc: 44 15 4%
634: THEN_290_0_ 
and: (633 201)
%inst: 4%
%sips: 71 0 44 15 4%
635: ELSE_290_0_ 
and: (633 !201)
%inst: 4%
%sips: 71 0 44 15 4%
-- Statement 291
636: GO_291_0_ 
or: (634 631)
%inst: 4%
%lc: 45 3 4%%sies: 44 0 45 3 4%%lc: 47 8 4%
-- Statement 292
-- Halt 293 rank 26
637: RESUME_293_ 
or: (259 644)
%inst: 4%
638: MAINTAIN_293_ 
and: (637 639)
%inst: 4%
639: HALT_REG_293_ halt: 26
reg: $0 641
%inst: 4%
640: GO_293_0_ 
or: (636 638)
%inst: 4%
641: TO_REG_293_0_ 
and: (640 !298)
%inst: 4%
%lc: 50 4 4%
-- Watchdog 294
642: TO_PRESENT_294_ 
and: (639 647)
%inst: 4%
%lc: 50 4 4%
-- Present 295
643: THEN_295_0_ 
and: (642 23)
%inst: 4%
%sips: 8 0 50 4 4%%lc: 58 4 4%%sies: 76 0 58 4 4%%lc: 59 10 4%
644: ELSE_295_0_ 
and: (642 !23)
%inst: 4%
%sips: 8 0 50 4 4%
-- Watchdog 296
645: TO_PRESENT_296_ 
and: (639 257)
%inst: 4%
%lc: 52 3 4%
-- Present 297
646: THEN_297_0_ 
and: (645 191)
%inst: 4%
%lc: 53 4 4%%sips: 66 0 52 3 4%%lc: 53 12 4%%sies: 43 0 53 12 4%%sias: 11 0 53 33 4%
647: ELSE_297_0_ 
and: (645 !191)
%inst: 4%
%lc: 48 3 4%%sips: 66 0 52 3 4%
-- Statement 298
-- Access 299 [11]
648: CONT_299_0_ 
and: (646 35)
%inst: 4%
-- Action 300 [72]
649: ACT_72_0_0_ act: 72
648
%inst: 4%
%lc: 53 12 4%%sius: 43 0 53 12 4%%lc: 53 52 4%%sies: 92 0 53 52 4%
%incarn: 0 0%
-- Statement 301
-- Statement 302
-- Statement 303
-- Halt 304 rank 27
650: RESUME_304_ 
or: (259 657)
%inst: 4%
651: MAINTAIN_304_ 
and: (650 652)
%inst: 4%
652: HALT_REG_304_ halt: 27
reg: $0 654
%inst: 4%
653: GO_304_0_ 
or: (659 651)
%inst: 4%
654: TO_REG_304_0_ 
and: (653 !298)
%inst: 4%
%lc: 59 4 4%
-- Watchdog 305
655: TO_PRESENT_305_ 
and: (652 667)
%inst: 4%
%lc: 59 4 4%
-- Present 306
656: THEN_306_0_ 
and: (655 184)
%inst: 4%
%sips: 62 0 59 4 4%
657: ELSE_306_0_ 
and: (655 !184)
%inst: 4%
%sips: 62 0 59 4 4%
-- Present 307
658: THEN_307_0_ 
and: (643 184)
%inst: 4%
%sips: 62 0 59 10 4%
659: ELSE_307_0_ 
and: (643 !184)
%inst: 4%
%sips: 62 0 59 10 4%
-- Statement 308
660: GO_308_0_ 
or: (658 656)
%inst: 4%
%lc: 60 4 4%%sies: 41 0 60 4 4%%sias: 11 0 60 32 4%
-- Access 309 [11]
661: CONT_309_0_ 
and: (660 35)
%inst: 4%
-- Action 310 [73]
662: ACT_73_0_0_ act: 73
661
%inst: 4%
%lc: 60 4 4%%sius: 41 0 60 4 4%%lc: 61 4 4%%sies: 60 0 61 4 4%%sias: 11 0 61 22 4%
%incarn: 0 0%
-- Statement 311
-- Statement 312
-- Access 313 [11]
663: CONT_313_0_ 
and: (662 35)
%inst: 4%
-- Action 314 [74]
664: ACT_74_0_0_ act: 74
663
%inst: 4%
%lc: 61 4 4%%sius: 60 0 61 4 4%%lc: 68 8 4%%lc: 69 9 4%%lc: 77 3 4%
%incarn: 0 0%
-- Statement 315
-- Watchdog 316
665: TO_PRESENT_316_ 
and: (652 257)
%inst: 4%
%lc: 63 3 4%
-- Present 317
666: THEN_317_0_ 
and: (665 191)
%inst: 4%
%lc: 64 4 4%%sips: 66 0 63 3 4%%lc: 64 12 4%%sies: 61 0 64 12 4%%lc: 65 12 4%%sies: 42 0 65 12 4%%sias: 11 0 65 35 4%
667: ELSE_317_0_ 
and: (665 !191)
%inst: 4%
%lc: 56 3 4%%sips: 66 0 63 3 4%
-- Statement 318
-- Statement 319
-- Access 320 [11]
668: CONT_320_0_ 
and: (666 35)
%inst: 4%
-- Action 321 [75]
669: ACT_75_0_0_ act: 75
668
%inst: 4%
%lc: 65 12 4%%sius: 42 0 65 12 4%%lc: 65 54 4%%sies: 92 0 65 54 4%
%incarn: 0 0%
-- Statement 322
-- Statement 323
-- Statement 324
-- Statement 325
-- Fork 326
-- Parallel 327
670: SELECT_327_ 
or: (700 710 720 728 736 744 752)
%inst: 4%
671: DEAD_327_B0_ 
and: (670 !700)
%inst: 4%
672: DEAD_327_B1_ 
and: (670 !710)
%inst: 4%
673: DEAD_327_B2_ 
and: (670 !720)
%inst: 4%
674: DEAD_327_B3_ 
and: (670 !728)
%inst: 4%
675: DEAD_327_B4_ 
and: (670 !736)
%inst: 4%
676: DEAD_327_B5_ 
and: (670 !744)
%inst: 4%
677: DEAD_327_B6_ 
and: (670 !752)
%inst: 4%
678: PARALLEL_327_UNION_K1_0_ 
or: (701 711 721 729 737 745 753)
%inst: 4%
679: PARALLEL_327_CONT_K1_0_ 
and: (678 680 681 682 683 684 685 686)
%inst: 4%
680: PARALLEL_327_MIN_K1_B0_0_ 
or: (671 701)
%inst: 4%
681: PARALLEL_327_MIN_K1_B1_0_ 
or: (672 711)
%inst: 4%
682: PARALLEL_327_MIN_K1_B2_0_ 
or: (673 721)
%inst: 4%
683: PARALLEL_327_MIN_K1_B3_0_ 
or: (674 729)
%inst: 4%
684: PARALLEL_327_MIN_K1_B4_0_ 
or: (675 737)
%inst: 4%
685: PARALLEL_327_MIN_K1_B5_0_ 
or: (676 745)
%inst: 4%
686: PARALLEL_327_MIN_K1_B6_0_ 
or: (677 753)
%inst: 4%
687: PARALLEL_327_CONT_K2_0_ 
and: (756 680 681 682 683 684 685 688)
%inst: 4%
%lc: 97 5 4%%sies: 42 0 97 5 4%%sias: 11 0 97 28 4%
688: PARALLEL_327_MIN_K2_B6_0_ 
or: (686 756)
%inst: 4%
689: PARALLEL_327_CONT_K4_0_ 
and: (707 690 681 682 683 684 685 688)
%inst: 4%
690: PARALLEL_327_MIN_K4_B0_0_ 
or: (680 707)
%inst: 4%
691: PARALLEL_327_UNION_K5_0_ 
or: (724 732 740 748)
%inst: 4%
692: PARALLEL_327_CONT_K5_0_ 
and: (691 690 681 693 694 695 696 688)
%inst: 4%
%lc: 111 14 4%%sies: 75 0 111 14 4%
693: PARALLEL_327_MIN_K5_B2_0_ 
or: (682 724)
%inst: 4%
694: PARALLEL_327_MIN_K5_B3_0_ 
or: (683 732)
%inst: 4%
695: PARALLEL_327_MIN_K5_B4_0_ 
or: (684 740)
%inst: 4%
696: PARALLEL_327_MIN_K5_B5_0_ 
or: (685 748)
%inst: 4%
697: PARALLEL_327_KILL_0_ 
or: (298 687 689 692 298)
%inst: 4%
-- Halt 328 rank 28
698: RESUME_328_ 
or: (259 705)
%inst: 4%
699: MAINTAIN_328_ 
and: (698 700)
%inst: 4%
700: HALT_REG_328_ halt: 28
reg: $0 702
%inst: 4%
701: GO_328_0_ 
or: (664 699)
%inst: 4%
702: TO_REG_328_0_ 
and: (701 !697)
%inst: 4%
%lc: 72 5 4%
-- Watchdog 329
703: TO_PRESENT_329_ 
and: (700 257)
%inst: 4%
%lc: 72 5 4%
-- Present 330
704: THEN_330_0_ 
and: (703 191)
%inst: 4%
%sips: 66 0 72 5 4%%lc: 73 5 4%%sies: 61 0 73 5 4%%lc: 74 5 4%%sies: 42 0 74 5 4%%sias: 11 0 74 28 4%
705: ELSE_330_0_ 
and: (703 !191)
%inst: 4%
%sips: 66 0 72 5 4%
-- Statement 331
-- Statement 332
-- Access 333 [11]
706: CONT_333_0_ 
and: (704 35)
%inst: 4%
-- Action 334 [76]
707: ACT_76_0_0_ act: 76
706
%inst: 4%
%lc: 74 5 4%%sius: 42 0 74 5 4%%lc: 75 5 4%%sies: 92 0 75 5 4%
%incarn: 0 0%
-- Statement 335
-- Statement 336
-- Halt 338 rank 29
708: RESUME_338_ 
or: (259 715)
%inst: 4%
709: MAINTAIN_338_ 
and: (708 710)
%inst: 4%
710: HALT_REG_338_ halt: 29
reg: $0 712
%inst: 4%
711: GO_338_0_ 
or: (664 709 717)
%inst: 4%
712: TO_REG_338_0_ 
and: (711 !697)
%inst: 4%
%lc: 79 4 4%
-- Watchdog 339
713: TO_PRESENT_339_ 
and: (710 257)
%inst: 4%
%lc: 79 4 4%
-- Present 340
714: THEN_340_0_ 
and: (713 5)
%inst: 4%
%sips: 1 0 79 4 4%%lc: 79 20 4%%sies: 46 0 79 20 4%%sias: 11 0 79 39 4%
715: ELSE_340_0_ 
and: (713 !5)
%inst: 4%
%sips: 1 0 79 4 4%
-- Statement 341
-- Access 342 [11]
716: CONT_342_0_ 
and: (714 35)
%inst: 4%
-- Action 343 [77]
717: ACT_77_0_0_ act: 77
716
%inst: 4%
%lc: 79 20 4%%sius: 46 0 79 20 4%%lc: 78 3 4%
%incarn: 0 0%
-- Statement 344
-- Statement 345
-- Halt 346 rank 30
718: RESUME_346_ 
or: (259 725)
%inst: 4%
719: MAINTAIN_346_ 
and: (718 720)
%inst: 4%
720: HALT_REG_346_ halt: 30
reg: $0 722
%inst: 4%
721: GO_346_0_ 
or: (664 719)
%inst: 4%
722: TO_REG_346_0_ 
and: (721 !697)
%inst: 4%
%lc: 82 4 4%
-- Watchdog 347
723: TO_PRESENT_347_ 
and: (720 257)
%inst: 4%
%lc: 82 4 4%
-- Present 348
724: THEN_348_0_ 
and: (723 56)
%inst: 4%
%sips: 19 0 82 4 4%%lc: 82 20 4%%sies: 91 0 82 20 4%
725: ELSE_348_0_ 
and: (723 !56)
%inst: 4%
%sips: 19 0 82 4 4%
-- Statement 349
-- Halt 351 rank 31
726: RESUME_351_ 
or: (259 733)
%inst: 4%
727: MAINTAIN_351_ 
and: (726 728)
%inst: 4%
728: HALT_REG_351_ halt: 31
reg: $0 730
%inst: 4%
729: GO_351_0_ 
or: (664 727)
%inst: 4%
730: TO_REG_351_0_ 
and: (729 !697)
%inst: 4%
%lc: 84 4 4%
-- Watchdog 352
731: TO_PRESENT_352_ 
and: (728 257)
%inst: 4%
%lc: 84 4 4%
-- Present 353
732: THEN_353_0_ 
and: (731 59)
%inst: 4%
%sips: 20 0 84 4 4%%lc: 84 20 4%%sies: 91 0 84 20 4%
733: ELSE_353_0_ 
and: (731 !59)
%inst: 4%
%sips: 20 0 84 4 4%
-- Statement 354
-- Halt 356 rank 32
734: RESUME_356_ 
or: (259 741)
%inst: 4%
735: MAINTAIN_356_ 
and: (734 736)
%inst: 4%
736: HALT_REG_356_ halt: 32
reg: $0 738
%inst: 4%
737: GO_356_0_ 
or: (664 735)
%inst: 4%
738: TO_REG_356_0_ 
and: (737 !697)
%inst: 4%
%lc: 86 4 4%
-- Watchdog 357
739: TO_PRESENT_357_ 
and: (736 257)
%inst: 4%
%lc: 86 4 4%
-- Present 358
740: THEN_358_0_ 
and: (739 50)
%inst: 4%
%sips: 17 0 86 4 4%%lc: 86 19 4%%sies: 91 0 86 19 4%
741: ELSE_358_0_ 
and: (739 !50)
%inst: 4%
%sips: 17 0 86 4 4%
-- Statement 359
-- Halt 361 rank 33
742: RESUME_361_ 
or: (259 749)
%inst: 4%
743: MAINTAIN_361_ 
and: (742 744)
%inst: 4%
744: HALT_REG_361_ halt: 33
reg: $0 746
%inst: 4%
745: GO_361_0_ 
or: (664 743)
%inst: 4%
746: TO_REG_361_0_ 
and: (745 !697)
%inst: 4%
%lc: 88 4 4%
-- Watchdog 362
747: TO_PRESENT_362_ 
and: (744 257)
%inst: 4%
%lc: 88 4 4%
-- Present 363
748: THEN_363_0_ 
and: (747 62)
%inst: 4%
%sips: 21 0 88 4 4%%lc: 88 18 4%%sies: 91 0 88 18 4%
749: ELSE_363_0_ 
and: (747 !62)
%inst: 4%
%sips: 21 0 88 4 4%
-- Statement 364
-- Halt 366 rank 34
750: RESUME_366_ 
or: (259 757)
%inst: 4%
751: MAINTAIN_366_ 
and: (750 752)
%inst: 4%
752: HALT_REG_366_ halt: 34
reg: $0 754
%inst: 4%
753: GO_366_0_ 
or: (664 751)
%inst: 4%
754: TO_REG_366_0_ 
and: (753 !697)
%inst: 4%
%lc: 92 4 4%
-- Watchdog 367
755: TO_PRESENT_367_ 
and: (752 257)
%inst: 4%
%lc: 92 4 4%
-- Present 368
756: THEN_368_0_ 
and: (755 53)
%inst: 4%
%sips: 18 0 92 4 4%%lc: 94 5 4%%sies: 94 0 94 5 4%
757: ELSE_368_0_ 
and: (755 !53)
%inst: 4%
%sips: 18 0 92 4 4%
-- Statement 369
-- Statement 372
-- Access 373 [11]
758: CONT_373_0_ 
and: (687 35)
%inst: 4%
-- Action 374 [78]
759: ACT_78_0_0_ act: 78
758
%inst: 4%
%lc: 97 5 4%%sius: 42 0 97 5 4%%lc: 98 5 4%%sies: 58 0 98 5 4%%lc: 98 24 4%%sies: 45 0 98 24 4%%lc: 99 5 4%%sies: 61 0 99 5 4%
%incarn: 0 0%
-- Statement 375
-- Statement 376
-- Statement 377
-- Statement 378
-- Statement 385
760: GO_385_0_ 
or: (649 759 689 669)
%inst: 4%
%lc: 44 2 4%
-- Statement 386
-- Fork 387
-- Parallel 388
761: SELECT_388_B0_ selectinc:
or: (777 768)
%inst: 5%
762: DEAD_388_B0_ 
and: (761 !761)
%inst: 5%
763: PARALLEL_388_UNION_K0_0_ 
or: (786 788)
%inst: 5%
764: PARALLEL_388_UNION_K1_0_ 
or: (782 769)
%inst: 5%
765: PARALLEL_388_KILL_0_ 
or: (298 298)
%inst: 5%
-- Statement 389
-- Statement 390
-- Statement 391
-- Statement 392
-- Halt 393 rank 35
766: RESUME_393_ 
or: (259 773)
%inst: 5%
767: MAINTAIN_393_ 
and: (766 768)
%inst: 5%
768: HALT_REG_393_ halt: 35
reg: $0 770
%inst: 5%
769: GO_393_0_ 
or: (775 767)
%inst: 5%
770: TO_REG_393_0_ 
and: (769 !765)
%inst: 5%
%lc: 62 1 5%
-- Watchdog 394
771: TO_PRESENT_394_ 
and: (768 257)
%inst: 5%
%lc: 62 1 5%
-- Present 395
772: THEN_395_0_ 
and: (771 167)
%inst: 5%
%sips: 54 0 62 1 5%
773: ELSE_395_0_ 
and: (771 !167)
%inst: 5%
%sips: 54 0 62 1 5%
-- Present 396
774: THEN_396_0_ 
and: (266 167)
%inst: 5%
%sips: 54 0 62 7 5%
775: ELSE_396_0_ 
and: (266 !167)
%inst: 5%
%sips: 54 0 62 7 5%
-- Statement 397
776: GO_397_0_ 
or: (774 772)
%inst: 5%
%lc: 63 1 5%%sies: 34 0 63 1 5%%lc: 65 6 5%%lc: 66 7 5%%lc: 67 8 5%%lc: 142 3 5%%sias: 11 0 73 32 5%
-- Statement 398
-- Statement 399
-- Statement 400
-- Fork 401
-- Parallel 402
777: SELECT_402_ 
or: (778 876)
%inst: 5%
778: SELECT_402_B0_ selectinc:
or: (797 851 865 806)
%inst: 5%
779: DEAD_402_B0_ 
and: (777 !778)
%inst: 5%
780: DEAD_402_B1_ 
and: (777 !876)
%inst: 5%
781: PARALLEL_402_UNION_K1_0_ 
or: (798 811 852 866 877)
%inst: 5%
782: PARALLEL_402_CONT_K1_0_ 
and: (781 783 784)
%inst: 5%
783: PARALLEL_402_MIN_K1_B0_0_ 
or: (779 798 811 852 866)
%inst: 5%
784: PARALLEL_402_MIN_K1_B1_0_ 
or: (780 877)
%inst: 5%
785: PARALLEL_402_UNION_K3_0_ 
or: (857 871 818 803)
%inst: 5%
786: PARALLEL_402_CONT_K3_0_ 
and: (785 787 784)
%inst: 5%
%lc: 158 34 5%%sies: 59 0 158 34 5%
787: PARALLEL_402_MIN_K3_B0_0_ 
or: (783 857 871 818 803)
%inst: 5%
788: PARALLEL_402_CONT_K4_0_ 
and: (880 787 789)
%inst: 5%
789: PARALLEL_402_MIN_K4_B1_0_ 
or: (784 880)
%inst: 5%
790: PARALLEL_402_KILL_0_ 
or: (765 786 788 765)
%inst: 5%
-- Access 403 [11]
791: CONT_403_0_ 
and: (776 35)
%inst: 5%
-- Action 404 [80]
792: ACT_80_0_0_ act: 80
791
%inst: 5%
%lc: 73 5 5%%sias: 11 0 74 32 5%
%incarn: 0 0%
-- Access 405 [11]
793: CONT_405_0_ 
and: (792 35)
%inst: 5%
-- Action 406 [81]
794: ACT_81_0_0_ act: 81
793
%inst: 5%
%lc: 74 5 5%%lc: 75 5 5%%sies: 71 0 75 5 5%
%incarn: 0 0%
-- Statement 407
-- Halt 408 rank 36
795: RESUME_408_ 
or: (259 804)
%inst: 5%
796: MAINTAIN_408_ 
and: (795 797)
%inst: 5%
797: HALT_REG_408_ halt: 36
reg: $0 799
%inst: 5%
798: GO_408_0_ 
or: (794 796)
%inst: 5%
799: TO_REG_408_0_ 
and: (798 !790)
%inst: 5%
%lc: 76 5 5%
-- Watchdog 409
800: TO_PRESENT_409_ 
and: (797 257)
%inst: 5%
%lc: 77 7 5%
-- Present 410
801: THEN_410_0_ 
and: (800 175)
%inst: 5%
%sips: 58 0 77 7 5%
802: ELSE_410_0_ 
and: (800 !175)
%inst: 5%
%sips: 58 0 77 7 5%%lc: 78 7 5%
-- Present 411
803: THEN_411_0_ 
and: (802 209)
%inst: 5%
%lc: 78 25 5%%sips: 75 0 78 7 5%%lc: 78 28 5%%sies: 100 0 78 28 5%
804: ELSE_411_0_ 
and: (802 !209)
%inst: 5%
%lc: 76 5 5%%sips: 75 0 78 7 5%
-- Statement 412
-- Statement 414
805: GO_414_0_ 
or: (801 873)
%inst: 5%
%lc: 84 6 5%%lc: 85 6 5%%lc: 99 1 5%%sias: 10 0 89 34 5%
-- Statement 415
-- Fork 416
-- Parallel 417
806: SELECT_417_ 
or: (827 837)
%inst: 5%
807: DEAD_417_B0_ 
and: (806 !827)
%inst: 5%
808: DEAD_417_B1_ 
and: (806 !837)
%inst: 5%
809: PARALLEL_417_MIN_K0_B0_0_ 
or: (807 831)
%inst: 5%
810: PARALLEL_417_UNION_K1_0_ 
or: (828 838)
%inst: 5%
811: PARALLEL_417_CONT_K1_0_ 
and: (810 812 813)
%inst: 5%
812: PARALLEL_417_MIN_K1_B0_0_ 
or: (809 828)
%inst: 5%
813: PARALLEL_417_MIN_K1_B1_0_ 
or: (808 838)
%inst: 5%
814: PARALLEL_417_CONT_K2_0_ 
and: (841 812 815)
%inst: 5%
%lc: 104 1 5%%sies: 70 0 104 1 5%%sias: 9 0 108 32 5%
815: PARALLEL_417_MIN_K2_B1_0_ 
or: (813 841)
%inst: 5%
816: PARALLEL_417_CONT_K3_0_ 
and: (843 812 817)
%inst: 5%
%lc: 119 1 5%%sies: 70 0 119 1 5%%sias: 11 0 123 32 5%
817: PARALLEL_417_MIN_K3_B1_0_ 
or: (815 843)
%inst: 5%
818: PARALLEL_417_CONT_K5_0_ 
and: (833 819 817)
%inst: 5%
819: PARALLEL_417_MIN_K5_B0_0_ 
or: (812 833)
%inst: 5%
820: PARALLEL_417_KILL_0_ 
or: (790 814 816 818 790)
%inst: 5%
-- Access 418 [10]
821: CONT_418_0_ 
and: (805 31)
%inst: 5%
-- Action 419 [82]
822: ACT_82_0_0_ act: 82
821
%inst: 5%
%lc: 89 7 5%%sias: 10 0 90 34 5%
%incarn: 0 0%
-- Access 420 [10]
823: CONT_420_0_ 
and: (822 31)
%inst: 5%
-- Action 421 [83]
824: ACT_83_0_0_ act: 83
823
%inst: 5%
%lc: 90 7 5%%lc: 91 7 5%%sies: 69 0 91 7 5%
%incarn: 0 0%
-- Statement 422
-- Halt 423 rank 37
825: RESUME_423_ 
or: (259 834)
%inst: 5%
826: MAINTAIN_423_ 
and: (825 827)
%inst: 5%
827: HALT_REG_423_ halt: 37
reg: $0 829
%inst: 5%
828: GO_423_0_ 
or: (824 826)
%inst: 5%
829: TO_REG_423_0_ 
and: (828 !820)
%inst: 5%
%lc: 92 7 5%
-- Watchdog 424
830: TO_PRESENT_424_ 
and: (827 257)
%inst: 5%
%lc: 93 9 5%
-- Present 425
831: THEN_425_0_ 
and: (830 165)
%inst: 5%
%sips: 53 0 93 9 5%
832: ELSE_425_0_ 
and: (830 !165)
%inst: 5%
%sips: 53 0 93 9 5%%lc: 94 9 5%
-- Present 426
833: THEN_426_0_ 
and: (832 171)
%inst: 5%
%lc: 94 27 5%%sips: 56 0 94 9 5%%lc: 94 30 5%%sies: 100 0 94 30 5%
834: ELSE_426_0_ 
and: (832 !171)
%inst: 5%
%lc: 92 7 5%%sips: 56 0 94 9 5%
-- Statement 427
-- Halt 429 rank 38
835: RESUME_429_ 
or: (259 844)
%inst: 5%
836: MAINTAIN_429_ 
and: (835 837)
%inst: 5%
837: HALT_REG_429_ halt: 38
reg: $0 839
%inst: 5%
838: GO_429_0_ 
or: (805 836)
%inst: 5%
839: TO_REG_429_0_ 
and: (838 !820)
%inst: 5%
%lc: 100 1 5%
-- Watchdog 430
840: TO_PRESENT_430_ 
and: (837 257)
%inst: 5%
%lc: 100 7 5%
-- Present 431
841: THEN_431_0_ 
and: (840 169)
%inst: 5%
%lc: 100 23 5%%sips: 55 0 100 7 5%%lc: 100 26 5%%sies: 103 0 100 26 5%
842: ELSE_431_0_ 
and: (840 !169)
%inst: 5%
%sips: 55 0 100 7 5%%lc: 101 7 5%
-- Statement 432
-- Present 434
843: THEN_434_0_ 
and: (842 205)
%inst: 5%
%lc: 101 22 5%%sips: 73 0 101 7 5%%lc: 101 25 5%%sies: 102 0 101 25 5%
844: ELSE_434_0_ 
and: (842 !205)
%inst: 5%
%lc: 100 1 5%%sips: 73 0 101 7 5%
-- Statement 435
-- Statement 438
-- Access 439 [9]
845: CONT_439_0_ 
and: (814 27)
%inst: 5%
-- Action 440 [84]
846: ACT_84_0_0_ act: 84
845
%inst: 5%
%lc: 108 5 5%%sias: 9 0 109 32 5%
%incarn: 0 0%
-- Access 441 [9]
847: CONT_441_0_ 
and: (846 27)
%inst: 5%
-- Action 442 [85]
848: ACT_85_0_0_ act: 85
847
%inst: 5%
%lc: 109 5 5%%lc: 110 5 5%%sies: 67 0 110 5 5%
%incarn: 0 0%
-- Statement 443
-- Halt 444 rank 39
849: RESUME_444_ 
or: (259 858)
%inst: 5%
850: MAINTAIN_444_ 
and: (849 851)
%inst: 5%
851: HALT_REG_444_ halt: 39
reg: $0 853
%inst: 5%
852: GO_444_0_ 
or: (848 850)
%inst: 5%
853: TO_REG_444_0_ 
and: (852 !790)
%inst: 5%
%lc: 111 5 5%
-- Watchdog 445
854: TO_PRESENT_445_ 
and: (851 257)
%inst: 5%
%lc: 112 7 5%
-- Present 446
855: THEN_446_0_ 
and: (854 173)
%inst: 5%
%sips: 57 0 112 7 5%
856: ELSE_446_0_ 
and: (854 !173)
%inst: 5%
%sips: 57 0 112 7 5%%lc: 113 7 5%
-- Present 447
857: THEN_447_0_ 
and: (856 207)
%inst: 5%
%lc: 113 25 5%%sips: 74 0 113 7 5%%lc: 113 28 5%%sies: 100 0 113 28 5%
858: ELSE_447_0_ 
and: (856 !207)
%inst: 5%
%lc: 111 5 5%%sips: 74 0 113 7 5%
-- Statement 448
-- Statement 450
-- Access 451 [11]
859: CONT_451_0_ 
and: (816 35)
%inst: 5%
-- Action 452 [86]
860: ACT_86_0_0_ act: 86
859
%inst: 5%
%lc: 123 5 5%%sias: 11 0 124 32 5%
%incarn: 0 0%
-- Access 453 [11]
861: CONT_453_0_ 
and: (860 35)
%inst: 5%
-- Action 454 [87]
862: ACT_87_0_0_ act: 87
861
%inst: 5%
%lc: 124 5 5%%lc: 125 5 5%%sies: 71 0 125 5 5%
%incarn: 0 0%
-- Statement 455
-- Halt 456 rank 40
863: RESUME_456_ 
or: (259 872)
%inst: 5%
864: MAINTAIN_456_ 
and: (863 865)
%inst: 5%
865: HALT_REG_456_ halt: 40
reg: $0 867
%inst: 5%
866: GO_456_0_ 
or: (862 864)
%inst: 5%
867: TO_REG_456_0_ 
and: (866 !790)
%inst: 5%
%lc: 126 5 5%
-- Watchdog 457
868: TO_PRESENT_457_ 
and: (865 257)
%inst: 5%
%lc: 127 7 5%
-- Present 458
869: THEN_458_0_ 
and: (868 175)
%inst: 5%
%sips: 58 0 127 7 5%
870: ELSE_458_0_ 
and: (868 !175)
%inst: 5%
%sips: 58 0 127 7 5%%lc: 128 7 5%
-- Present 459
871: THEN_459_0_ 
and: (870 209)
%inst: 5%
%lc: 128 25 5%%sips: 75 0 128 7 5%%lc: 128 28 5%%sies: 100 0 128 28 5%
872: ELSE_459_0_ 
and: (870 !209)
%inst: 5%
%lc: 126 5 5%%sips: 75 0 128 7 5%
-- Statement 460
-- Statement 462
873: GO_462_0_ 
or: (855 869)
%inst: 5%
%lc: 83 1 5%
-- Halt 465 rank 41
874: RESUME_465_ 
or: (259 881)
%inst: 5%
875: MAINTAIN_465_ 
and: (874 876)
%inst: 5%
876: HALT_REG_465_ halt: 41
reg: $0 878
%inst: 5%
877: GO_465_0_ 
or: (776 875)
%inst: 5%
878: TO_REG_465_0_ 
and: (877 !790)
%inst: 5%
%lc: 145 6 5%
-- Watchdog 466
879: TO_PRESENT_466_ 
and: (876 257)
%inst: 5%
%lc: 145 6 5%
-- Present 467
880: THEN_467_0_ 
and: (879 189)
%inst: 5%
%sips: 65 0 145 6 5%%lc: 146 6 5%%sies: 35 0 146 6 5%%lc: 147 6 5%%sies: 66 0 147 6 5%%lc: 148 6 5%%sies: 68 0 148 6 5%%lc: 149 6 5%%sies: 70 0 149 6 5%%lc: 150 6 5%%sies: 99 0 150 6 5%
881: ELSE_467_0_ 
and: (879 !189)
%inst: 5%
%sips: 65 0 145 6 5%
-- Statement 468
-- Statement 469
-- Statement 470
-- Statement 471
-- Statement 472
-- Statement 478
-- Halt 480 rank 42
882: RESUME_480_ 
or: (259 889)
%inst: 6%
883: MAINTAIN_480_ 
and: (882 884)
%inst: 6%
884: HALT_REG_480_ halt: 42
reg: $0 886
%inst: 6%
885: GO_480_0_ 
or: (266 883)
%inst: 6%
886: TO_REG_480_0_ 
and: (885 !298)
%inst: 6%
%lc: 160 3 6%
-- Watchdog 481
887: TO_PRESENT_481_ 
and: (884 257)
%inst: 6%
%lc: 160 3 6%
-- Present 482
888: THEN_482_0_ 
and: (887 211)
%inst: 6%
%sips: 76 0 160 3 6%%lc: 160 23 6%%sies: 62 0 160 23 6%
889: ELSE_482_0_ 
and: (887 !211)
%inst: 6%
%sips: 76 0 160 3 6%
-- Statement 483
-- Halt 484 rank 43
890: RESUME_484_ 
or: (259 897)
%inst: 6%
891: MAINTAIN_484_ 
and: (890 892)
%inst: 6%
892: HALT_REG_484_ halt: 43
reg: $0 894
%inst: 6%
893: GO_484_0_ 
or: (888 891 914)
%inst: 6%
894: TO_REG_484_0_ 
and: (893 !298)
%inst: 6%
%lc: 162 6 6%
-- Watchdog 485
895: TO_PRESENT_485_ 
and: (892 257)
%inst: 6%
%lc: 162 6 6%
-- Present 486
896: THEN_486_0_ 
and: (895 182)
%inst: 6%
%sips: 61 0 162 6 6%
897: ELSE_486_0_ 
and: (895 !182)
%inst: 6%
%sips: 61 0 162 6 6%
-- Halt 487 rank 44
898: RESUME_487_ 
or: (259 905)
%inst: 6%
899: MAINTAIN_487_ 
and: (898 900)
%inst: 6%
900: HALT_REG_487_ halt: 44
reg: $0 902
%inst: 6%
901: GO_487_0_ 
or: (896 899)
%inst: 6%
902: TO_REG_487_0_ 
and: (901 !298)
%inst: 6%
%lc: 164 8 6%
-- Watchdog 488
903: TO_PRESENT_488_ 
and: (900 257)
%inst: 6%
%lc: 164 8 6%
-- Present 489
904: THEN_489_0_ 
and: (903 211)
%inst: 6%
%sips: 76 0 164 8 6%%lc: 166 6 6%%sies: 48 0 166 6 6%%sias: 60 0 166 19 6%
905: ELSE_489_0_ 
and: (903 !211)
%inst: 6%
%sips: 76 0 164 8 6%
-- Statement 490
-- Access 491 [60]
906: CONT_491_0_ 
and: (904 180)
%inst: 6%
-- Action 492 [88]
907: ACT_88_0_0_ act: 88
906
%inst: 6%
%lc: 166 6 6%%sius: 48 0 166 6 6%
%incarn: 0 0%
-- Statement 493
-- Halt 494 rank 45
908: RESUME_494_ 
or: (259 915)
%inst: 6%
909: MAINTAIN_494_ 
and: (908 910)
%inst: 6%
910: HALT_REG_494_ halt: 45
reg: $0 912
%inst: 6%
911: GO_494_0_ 
or: (907 909)
%inst: 6%
912: TO_REG_494_0_ 
and: (911 !298)
%inst: 6%
%lc: 167 6 6%
-- Watchdog 495
913: TO_PRESENT_495_ 
and: (910 257)
%inst: 6%
%lc: 167 6 6%
-- Present 496
914: THEN_496_0_ 
and: (913 38)
%inst: 6%
%sips: 12 0 167 6 6%%lc: 168 6 6%%sies: 62 0 168 6 6%%lc: 161 4 6%
915: ELSE_496_0_ 
and: (913 !38)
%inst: 6%
%sips: 12 0 167 6 6%
-- Statement 497
-- Statement 498

end:

endmodule:
