// Orccad Version: 3.0 alpha
// Module : sens2x
// Initialisation File
//
// Module Ports :
// 	input DOUBLE sensor[2]
// 	output DOUBLE x[2]
// 	input DOUBLE pos[2]
//
// Date of creation : Wed Mar 17 14:44:30 1999

k1 = k2 = c12 = s12 = 0.0;

posd[0] = 0.0;
posd[1] = 0.0;

inwork = NO_EVENT;
top    = 0;
