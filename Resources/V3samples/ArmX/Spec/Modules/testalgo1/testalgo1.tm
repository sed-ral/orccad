// Ilv Version: 3.1
// File generated: Thu Jan 19 15:16:11 2006
// Creator class: IlvGraphOutput
Palettes 6
3 "red" "red" "default" "fixed" 0 solid solid 0 0 0
2 "white" 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 59110 59110 59110 "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 5 59110 59110 59110 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 59110 59110 59110 0 0 0 "default" "fixed" 0 solid solid 0 0 0
1 59110 59110 59110 "yellow" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 90 70 90 100 
5 IlvMessageLabel 90 70 91 101 F8 0 1 16 4 "testalgo1"   [Box
0 1 1 testalgo1 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 80 80 20 20 
2 IlvMessageLabel 100 76 98 28 F8 0 25 16 4 "Port_Var"   0
[Port
0 1 1 Port_Var 90 90 100 86 98 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 170 130 20 20 2 
2 IlvMessageLabel 123 126 94 28 F8 0 25 16 4 "Port_Evt"   0
[Port
0 1 2 Port_Evt 180 140 123 126 94 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
EOF
