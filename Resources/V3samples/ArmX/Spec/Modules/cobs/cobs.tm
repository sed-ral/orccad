// Ilv Version: 2.4
// File generated: Thu Mar 18 09:00:52 1999
// Creator class: IlvGraphOutput
Palettes 7
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 0 49087 65535 45232 50372 57054 "default" "9x15" 0 solid solid 0 0 0
3 65535 0 0 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
4 0 49087 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 6 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "red" "red" "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 80 90 140 170 
6 IlvMessageLabel 80 90 141 171 0 "cobs" 16 [Box
0 1 1 cobs 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 70 180 20 20 2 
2 IlvMessageLabel 92 176 106 28 0 "outbound" 1 0
[Port
0 1 1 outbound 80 190 92 176 106 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 70 230 20 20 2 
2 IlvMessageLabel 92 226 72 28 0 "redbut" 1 0
[Port
0 1 2 redbut 80 240 92 226 72 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
4 IlvFilledEllipse 210 120 20 20 
2 IlvMessageLabel 187 116 42 28 0 "limit" 1 0
[Port
0 1 3 limit 220 130 187 116 42 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
4 IlvFilledEllipse 210 220 20 20 
2 IlvMessageLabel 188 216 40 28 0 "key" 1 0
[Port
0 1 4 key 220 230 188 216 40 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
