// Ilv Version: 2.4
// File generated: Wed Mar 17 11:18:51 1999
// Creator class: IlvGraphOutput
Palettes 7
3 0 49087 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
1 0 49087 65535 "blue" "default" "9x15" 0 solid solid 0 0 0
4 0 49087 65535 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
5 0 49087 65535 45232 50372 57054 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 6 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 110 80 150 190 
6 IlvMessageLabel 110 80 151 191 0 "command" 16 [Box
0 1 1 command 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 110 20 20 
2 IlvMessageLabel 122 106 54 28 0 "pose" 1 0
[Port
0 1 1 pose 110 120 122 106 54 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 100 220 20 20 
2 IlvMessageLabel 122 216 40 28 0 "pos" 1 0
[Port
0 1 2 pos 110 230 122 216 40 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
4 IlvFilledEllipse 250 110 20 20 
2 IlvMessageLabel 212 106 72 28 0 "torque" 1 0
[Port
0 1 3 torque 260 120 212 106 72 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
