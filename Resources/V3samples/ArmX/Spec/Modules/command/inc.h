// Orccad Version: 3.0 alpha   
// Module : command
// Include and Definition File
// Date of creation : Tue Mar 16 19:12:49 1999


// Robot parameters
#define ARM_L1   1.0
#define ARM_L2   1.0
#define ARM_M1   10.0
#define ARM_M2   5.0
#define ARM_GRAV 9.81
// Command gain
#define KX  20.0
#define KY  20.0

