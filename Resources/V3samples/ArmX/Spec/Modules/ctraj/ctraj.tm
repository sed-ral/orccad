// Ilv Version: 2.4
// File generated: Wed Mar 17 14:56:12 1999
// Creator class: IlvGraphOutput
Palettes 6
4 "deepskyblue" "lightsteelblue" "default" "9x15" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "deepskyblue" "red" "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
5 "deepskyblue" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "deepskyblue" "blue" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 100 90 160 190 
5 IlvMessageLabel 100 90 161 191 0 "ctraj" 16 [Box
0 1 1 ctraj 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 120 20 20 
2 IlvMessageLabel 112 116 54 28 0 "posd" 1 0
[Port
0 1 1 posd 100 130 112 116 54 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 250 120 20 20 
2 IlvMessageLabel 225 116 46 28 0 "posi" 1 0
[Port
0 1 2 posi 260 130 225 116 46 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 230 20 20 
2 IlvMessageLabel 112 226 40 28 0 "pos" 1 0
[Port
0 1 3 pos 100 240 112 226 40 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
