// Orccad Version: 3.0 alpha   
// Module : error
// Computation File 
// Date of creation : Mon Nov  3 13:07:23 1997

flagtrack = FALSE;

for(index=0;index<2;index++){
    errpos[index] = posd[index] - posc[index];
    if ((errpos[index]>TRACKERROR)||(errpos[index]<-TRACKERROR))
	flagtrack= TRUE;
}

if ( (flagtrack==TRUE)&&(errtrack==NO_EVENT) ){
     errtrack = SET_EVENT;
     fprintf(stdout, "WinXmove::Tracking Error Event\n");
}
