// Ilv Version: 2.4
// File generated: Mon Nov  3 16:51:33 1997
// Creator class: IlvGraphOutput
Palettes 7
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
5 0 49087 65535 45232 50372 57054 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 6 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 65535 0 0 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
1 0 49087 65535 "blue" "default" "9x15" 0 solid solid 0 0 0
3 0 49087 65535 "red" "default" "9x15" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 100 80 139 169 
6 IlvMessageLabel 100 80 140 170 0 "error" 16 [Box
0 1 1 error 
Box]

 } 0
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 110 20 20 
2 IlvMessageLabel 112 106 54 28 0 "posd" 1 0
[Port
0 1 1 posd 100 120 112 106 54 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 230 110 20 20 
2 IlvMessageLabel 193 106 70 28 0 "errpos" 1 0
[Port
0 1 2 errpos 240 120 193 106 70 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 200 20 20 
2 IlvMessageLabel 112 196 54 28 0 "posc" 1 0
[Port
0 1 3 posc 100 210 112 196 54 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 160 240 20 20 2 
2 IlvMessageLabel 149 221 84 28 0 "errtrack" 1 0
[Port
0 1 5 errtrack 170 250 149 221 84 28 8  8 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
