// Orccad Version: 3.0 alpha
// Module : invcart
// Initialisation File
//
// Module Ports :
// 	input DOUBLE xd[2]
// 	output DOUBLE jointd[2]
//
// Date of creation : Wed Mar 17 14:19:49 1999

k1 = 0.0;
k2 = 0.0;

jointd[0] = jointd[1] = 0.0;

reconf = NO_EVENT;
