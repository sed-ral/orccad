// Orccad Version: 3.0 alpha   
// Module : sens2x
// Computation File 
//
// Module Ports :
// 	input DOUBLE sensor[2]
// 	output DOUBLE x[2]
// 	input DOUBLE pos[2]
//
// Date of creation : Wed Mar 17 14:44:30 1999

k1    = sensor[0] + ARM_L1*cos(pos[1]) + ARM_L2;
k2    = sensor[1] - ARM_L1*sin(pos[1]);
s12   = sin(pos[0]+pos[1]);
c12   = cos(pos[0]+pos[1]);
x[0]  = c12*k1 - s12*k2;
x[1]  = s12*k1 + c12*k2;

if (outwork == NO_EVENT){
  k1 = sqrt(x[0]*x[0] + x[1]*x[1]);
  if  ( (k1 > (ARM_L1+ARM_L2))&& (top!=0) ){
       outwork = SET_EVENT;
       fprintf(stdout, "sens2x:: Out Workspace event Post-condition %d\n", top);
  }
  else
    if ( ((x[0] == 0.0)&&(x[1] == 0.0)) && (top!=0) ){
	outwork = SET_EVENT;
	fprintf(stdout, "sens2x:: Out Workspace event Post-condition %d\n", top);
    }
}

top++;
