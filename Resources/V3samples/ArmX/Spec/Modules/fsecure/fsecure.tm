// Ilv Version: 2.4
// File generated: Mon Mar 22 17:35:08 1999
// Creator class: IlvGraphOutput
Palettes 6
4 0 49087 65535 16448 57568 53456 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 5 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "blue" "blue" "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
1 0 0 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAtr 2
4 IlvFilledRectangle 90 80 140 170 
5 IlvMessageLabel 90 80 141 171 0 "fsecure" 16 [Box
0 1 1 fsecure 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 150 70 20 20 2 
2 IlvMessageLabel 139 85 86 28 0 "errtrack" 1 0
[Port
0 1 1 errtrack 160 80 139 85 86 28 2  2 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 220 170 20 20 2 
2 IlvMessageLabel 165 166 106 28 0 "outbound" 1 0
[Port
0 1 2 outbound 230 180 165 166 106 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 80 170 20 20 2 
2 IlvMessageLabel 102 166 72 28 0 "inwork" 1 0
[Port
0 1 3 inwork 90 180 102 166 72 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 220 220 20 20 2 
2 IlvMessageLabel 182 216 72 28 0 "redbut" 1 0
[Port
0 1 5 redbut 230 230 182 216 72 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
