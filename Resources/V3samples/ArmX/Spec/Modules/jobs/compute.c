// Orccad Version: 3.0 alpha   
// Module : jobs
// Computation File 
// Date of creation : Mon Nov  3 13:23:55 1997

if ( ((key == 'q')||(key == 'Q'))&& redbut==NO_EVENT){
  redbut = SET_EVENT;
  fprintf(stdout, "WinXmove::Quit Event\n");
}

if ( ((limit[0]==TRUE)||(limit[1]==TRUE)) && outbound == NO_EVENT){
  outbound = SET_EVENT;
  fprintf(stdout, "WinXmove::Joint Limit Event\n");
}

if ( (mode==MODE_ACTIVE)&&((key=='s')||(key=='S')) ){
  typetraj = SET_EVENT;
  mode = MODE_SUSPEND;
  fprintf(stdout, "WinXmove::T1 Event suspend\n");
}

if ( (mode==MODE_SUSPEND)&&((key=='r')||(key=='R')) ){
  typetraj = SET_EVENT;
  mode = MODE_ACTIVE;
  fprintf(stdout, "WinXmove::T1 Event active\n");
}

