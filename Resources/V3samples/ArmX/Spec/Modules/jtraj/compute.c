// Orccad Version: 3.0 alpha   
// Module : trajectory
// Computation File 
// Date of creation : Mon Nov  3 13:05:11 1997


if (mode == MODE_ACTIVE) {
  for(index=0;index <2;index++){
    if (pos[index] < posd[index]){
      pos[index] += STEP_POS;
      stop++;
      if (pos[index] > posd[index])
	pos[index] = posd[index] ;
    }
    else{
      if (pos[index] > posd[index]){
	pos[index] -= STEP_POS; 
	stop++;
	if (pos[index] < posd[index])
	  pos[index] = posd[index];
      }
    }
  }
}

stop = 0;
for(index=0;index <2;index++){
  if  ( ( (posI[index]-posd[index])<=EPSILON )&&
	( (posI[index]-posd[index])>=-EPSILON )  )
    stop++;
}

if ((stop==2)&&(endtraj==NO_EVENT)){
  fprintf(stdout, "jtraj:: Endtraj\n");
  endtraj = SET_EVENT;
}


if (typtraj==SET_EVENT){
  if (mode==MODE_ACTIVE)
    mode=MODE_SUSPEND;
  else
    mode=MODE_ACTIVE;
}


