// Ilv Version: 2.4
// File generated: Thu Mar 18 10:46:55 1999
// Creator class: IlvGraphOutput
Palettes 6
1 0 0 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 5 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 0 49087 65535 "red" "default" "9x15" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
4 0 49087 65535 16448 57568 53456 "default" "9x15" 0 solid solid 0 0 0
IlvObjects 15
3 0 { 0 0 OrcIlvModAtr 2
4 IlvFilledRectangle 90 80 140 170 
5 IlvMessageLabel 90 80 141 171 0 "jsecure" 16 [Box
0 1 1 jsecure 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 150 70 20 20 2 
2 IlvMessageLabel 139 85 86 28 0 "errtrack" 1 0
[Port
0 1 1 errtrack 160 80 139 85 86 28 2  2 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 220 170 20 20 2 
2 IlvMessageLabel 165 166 106 28 0 "outbound" 1 0
[Port
0 1 2 outbound 230 180 165 166 106 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 80 170 20 20 2 
2 IlvMessageLabel 102 166 78 28 0 "endtraj" 1 0
[Port
0 1 3 endtraj 90 180 102 166 78 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 80 220 20 20 2 
2 IlvMessageLabel 102 216 78 28 0 "badtraj" 1 0
[Port
0 1 4 badtraj 90 230 102 216 78 28 1  1 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 220 220 20 20 2 
2 IlvMessageLabel 182 216 72 28 0 "redbut" 1 0
[Port
0 1 5 redbut 230 230 182 216 72 28 4  4 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortVar 2
3 IlvFilledEllipse 80 110 20 20 
2 IlvMessageLabel 102 106 66 28 0 "swtraj" 1 0
[Port
0 1 6 swtraj 90 120 102 106 66 28 1  1 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 220 110 20 20 2 
2 IlvMessageLabel 175 106 86 28 0 "typetraj" 1 0
[Port
0 1 7 typetraj 230 120 175 106 86 28 4  4 
Port]

 } 20
1 1 { 14 0 OrcIlvVoidLink 1 0 13 } 30
