// Ilv Version: 2.4
// File generated: Tue Dec  9 15:59:33 1997
// Creator class: IlvGraphOutput
Palettes 6
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 48830 48830 48830 "red" "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 48830 48830 48830 "blue" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 100 80 230 180 
5 IlvMessageLabel 100 80 231 181 0 "Mapping" 16 [Box
0 1 1 Mapping 
Box]

 } 0
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 160 20 20 
2 IlvMessageLabel 112 156 66 28 0 "screw" 1 0
[Port
0 1 1 screw 100 170 112 156 66 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 320 160 20 20 
2 IlvMessageLabel 269 156 98 28 0 "thrusters" 1 0
[Port
0 1 3 thrusters 330 170 269 156 98 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
