// Orccad Version: 3.0 alpha   
// Module : Mapping
// Computation File 
// Date of creation : Tue Dec  9 11:48:38 1997


for (i=0; i<8; i++){
  pux[i] = puy[i] = puz[i] = 
  puphi[i] = putheta[i] = pupsi[i] = 0;
}

fx = screw[0];
fy = screw[1];
fz = screw[2];
phitorque = screw[3];
thetatorque = screw[4];
psitorque = screw[5];

// Fx()
  pux[0] = pux[1] = -.36 * fx;
  pux[2] = pux[3] = .36 *fx;

//  Fy()
  
  puy[1] = puy[3] = .36 * fy;
  puy[2] = puy[0] = -.36 * fy;

// Fz()
  
  puz[4] = puz[5] = puz[6] = puz[7] = -.25 *fz;

// phi_torque()
//torque:  couple de roulis demande
//*puphi;  poussee des moteurs

    puphi[7] = phitorque * 5.;
    puphi[6] = -puphi[7];
    
// psi_torque()
//double torque; /*couple de lacet demande*/
//double *pupsi;        /*poussee des moteurs*/

    pupsi[1] = pupsi[2] = psitorque * 1.6;
    pupsi[0] = pupsi[3] = -pupsi[1];

// theta_torque()
//double torque; /*couple de tangage demande*/
//double *putheta;        /*poussee des moteurs*/

    putheta[4] = thetatorque * 2.5;
    putheta[5] = -putheta[4];
    
// saturation()
/*controle de la saturation*/

     for(i=0;i<4;i++)	
	{
	    u[i] = pupsi[i] + pux[i] + puy[i];
	    pumax = MAX(pumax, ABS(u[i]));
	}
    if(pumax > UMAX)
	{
	    punormal = UMAX/pumax;
	    for (i=0;i<4;i++)
		u[i] *= punormal * .9999;
	}
    pumax = punormal = 0.0;
    for(i=4;i<8;i++)	
	{
	    u[i] = puphi[i] + putheta[i] + puz[i];
	    pumax = MAX(pumax, ABS(u[i]));
	}
    if(pumax > UMAX)
	{
	    punormal = UMAX/pumax;
	    for (i=4;i<8;i++)
		u[i] *= punormal * .9999;
	}
   
//
// passage de u calculee a la variable de sortie control
//
cout << " control: "; 
for(i=0; i<8; i++){
   control[i]= u[i];
   cout  << control[i] << " " ;
}
cout << endl;
