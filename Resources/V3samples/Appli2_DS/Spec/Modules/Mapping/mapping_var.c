// Orccad Version: 3.0 alpha
// Module : Mapping
// Variables declaration File
// Date of creation : Tue Dec  9 11:48:38 1997

double fx, fy, fz, 
       phitorque, thetatorque, psitorque;

int i;

double u[8];

double pux[8],
       puy[8], 
       puz[8],
       puphi[8],
       putheta[8],
       pupsi[8];
 
double pumax, 
       punormal;

double UMAX;
