// Orccad Version: 3.0 alpha   
// Module : Mapping
// Include and Definition File
// Date of creation : Tue Dec  9 11:48:38 1997


#ifndef _MAXABS_
#define _MAXABS_

#define MAX(x,y) (((x) < (y)) ? (y) : (x))
#define ABS(x) (((x) < 0) ? -(x) : (x))

#endif
