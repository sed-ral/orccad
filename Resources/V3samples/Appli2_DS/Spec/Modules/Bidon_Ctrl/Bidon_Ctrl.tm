// Ilv Version: 2.4
// File generated: Fri Apr 23 10:01:51 1999
// Creator class: IlvGraphOutput
Palettes 6
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 130 100 160 160 
5 IlvMessageLabel 130 100 161 161 0 "Bidon_Ctrl" 16 [Box
0 1 1 Bidon_Ctrl 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 120 170 20 20 
2 IlvMessageLabel 142 166 18 28 0 "X" 1 0
[Port
0 1 1 X 130 180 142 166 18 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 280 170 20 20 
2 IlvMessageLabel 270 166 16 28 0 "U" 1 0
[Port
0 1 2 U 290 180 270 166 16 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
