// Ilv Version: 2.4
// File generated: Tue Sep 22 14:56:41 1998
// Creator class: IlvGraphOutput
Palettes 8
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
6 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
7 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
5 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
6 IlvFilledRectangle 60 80 160 150 
7 IlvMessageLabel 60 80 161 151 0 "CruiseControl" 16 [Box
0 1 1 CruiseControl 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 50 150 20 20 
2 IlvMessageLabel 72 146 96 28 0 "Mes_vel" 1 0
[Port
0 1 1 Mes_vel 60 160 72 146 96 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 210 150 20 20 
2 IlvMessageLabel 162 146 92 28 0 "Des_vel" 1 0
[Port
0 1 2 Des_vel 220 160 162 146 92 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 210 200 20 20 2 
5 IlvMessageLabel 232 196 44 28 0 "End" 1 0
[Port
0 1 3 End 220 210 232 196 44 28 1  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
