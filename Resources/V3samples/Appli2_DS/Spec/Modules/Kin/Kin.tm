// Ilv Version: 2.4
// File generated: Tue Feb 22 12:02:28 2000
// Creator class: IlvGraphOutput
Palettes 7
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
6 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 120 90 180 160 
6 IlvMessageLabel 120 90 181 161 0 "Kin" 16 [Box
0 1 1 Kin 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 110 160 20 20 
2 IlvMessageLabel 132 156 54 28 0 "Input" 1 0
[Port
0 1 1 Input 120 170 132 156 54 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 290 160 20 20 
2 IlvMessageLabel 251 156 74 28 0 "Output" 1 0
[Port
0 1 2 Output 300 170 251 156 74 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 200 240 20 20 2 
2 IlvMessageLabel 222 236 100 28 0 "End_Traj" 1 0
[Port
0 1 3 End_Traj 210 250 222 236 100 28 1  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
