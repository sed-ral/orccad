// Ilv Version: 2.4
// File generated: Tue Feb 22 11:57:29 2000
// Creator class: IlvGraphOutput
Palettes 6
5 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 48830 48830 48830 "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 48830 48830 48830 "blue" "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 48830 48830 48830 62965 57054 46003 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModPr 2
4 IlvFilledRectangle 120 90 170 150 
5 IlvMessageLabel 120 90 171 151 0 "Excavator" 16 [Box
0 1 1 Excavator 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 280 160 20 20 2 
2 IlvMessageLabel 251 156 54 28 0 "Input" 1 0
[Port
0 1 1 Input 290 170 251 156 54 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 110 160 20 20 2 
2 IlvMessageLabel 132 156 74 28 0 "Output" 1 0
[Port
0 1 2 Output 120 170 132 156 74 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
