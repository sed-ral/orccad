// Ilv Version: 3.1
// File generated: Wed Mar 22 17:16:55 2000
// Creator class: IlvGraphOutput
Palettes 8
5 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
7 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 65535 0 0 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
6 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
1 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
6 IlvFilledRectangle 130 110 160 150 
7 IlvMessageLabel 130 110 161 151 F8 0 1 16 4 "Inv_Kin_SE3"   [Box
0 1 1 Inv_Kin_SE3 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 120 180 20 20 
2 IlvMessageLabel 142 176 110 28 F8 0 25 16 4 "Joint_Traj"   0
[Port
0 1 1 Joint_Traj 130 190 142 176 110 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 160 250 20 20 2 
2 IlvMessageLabel 182 246 118 28 F8 0 25 16 4 "Joint_Limit"   0
[Port
0 1 3 Joint_Limit 170 260 182 246 118 28 8  8 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 230 250 20 20 2 
2 IlvMessageLabel 252 246 50 28 F8 0 25 16 4 "Sing"   0
[Port
0 1 4 Sing 240 260 252 246 50 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
4 IlvFilledEllipse 280 170 20 20 
5 IlvMessageLabel 227 166 102 28 F8 0 25 16 4 "SE3_Traj"   0
[Port
0 1 5 SE3_Traj 290 180 227 166 102 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 26
EOF
