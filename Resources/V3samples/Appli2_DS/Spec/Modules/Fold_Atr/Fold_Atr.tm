// Ilv Version: 2.4
// File generated: Fri Dec  4 15:52:53 1998
// Creator class: IlvGraphOutput
Palettes 6
4 48830 48830 48830 16448 57568 53456 "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
3 0 0 65535 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAtr 2
4 IlvFilledRectangle 130 110 150 120 
5 IlvMessageLabel 130 110 151 121 0 "Fold_Atr" 16 [Box
0 1 1 Fold_Atr 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 120 160 20 20 2 
2 IlvMessageLabel 142 156 102 28 0 "FOLDED" 1 0
[Port
0 1 1 FOLDED 130 170 142 156 102 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 120 120 20 20 2 
2 IlvMessageLabel 142 116 114 28 0 "Storm_ON" 1 0
[Port
0 1 2 Storm_ON 130 130 142 116 114 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 120 200 20 20 2 
2 IlvMessageLabel 142 196 128 28 0 "Storm_OFF" 1 0
[Port
0 1 3 Storm_OFF 130 210 142 196 128 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
