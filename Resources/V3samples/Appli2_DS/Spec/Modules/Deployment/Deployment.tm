// Ilv Version: 2.4
// File generated: Fri Dec  4 16:48:12 1998
// Creator class: IlvGraphOutput
Palettes 7
6 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 65535 0 0 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 48830 48830 48830 "red" "default" "fixed" 0 solid solid 0 0 0
4 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 130 110 170 140 
6 IlvMessageLabel 130 110 171 141 0 "Deployment" 16 [Box
0 1 1 Deployment 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 120 140 20 20 
2 IlvMessageLabel 142 136 76 28 0 "control" 1 0
[Port
0 1 1 control 130 150 142 136 76 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 290 170 20 20 2 
2 IlvMessageLabel 220 166 136 28 0 "DEPLOYED" 1 0
[Port
0 1 4 DEPLOYED 300 180 220 166 136 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
4 IlvFilledEllipse 120 200 20 20 
2 IlvMessageLabel 142 196 64 28 0 "status" 1 0
[Port
0 1 5 status 130 210 142 196 64 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
