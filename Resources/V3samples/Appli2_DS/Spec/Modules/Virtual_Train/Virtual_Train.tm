// Ilv Version: 2.4
// File generated: Tue Sep 22 16:53:51 1998
// Creator class: IlvGraphOutput
Palettes 7
6 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 100 70 160 160 
6 IlvMessageLabel 100 70 161 161 0 "Virtual_Train" 16 [Box
0 1 1 Virtual_Train 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 130 20 20 
2 IlvMessageLabel 112 126 100 28 0 "Mes_dist" 1 0
[Port
0 1 1 Mes_dist 100 140 112 126 100 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 250 130 20 20 
2 IlvMessageLabel 200 126 96 28 0 "Des_dist" 1 0
[Port
0 1 2 Des_dist 260 140 200 126 96 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 250 170 20 20 2 
2 IlvMessageLabel 272 166 44 28 0 "End" 1 0
[Port
0 1 3 End 260 180 272 166 44 28 1  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
