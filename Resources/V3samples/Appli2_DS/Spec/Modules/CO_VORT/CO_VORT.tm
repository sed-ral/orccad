// Ilv Version: 2.21
// File generated: Mon Mar  3 19:59:52 1997
// Creator class: IlvGraphOutput
Palettes 6
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "LightBlue" "blue" "default" "9x15" 0 solid solid 0 0 0
"default" 0 "LightBlue" "black" "default" "9x15" 0 solid solid 0 0 0
4 "LightBlue" "lightsteelblue" "default" "9x15" 0 solid solid 0 0 0
5 "LightBlue" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "LightBlue" "red" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 130 120 130 130 
5 IlvMessageLabel 130 120 131 131 0 "CO_VORT" 16 [Box
0 1 1 CO_VORT 
Box]

 } 0
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 120 170 20 20 
2 IlvMessageLabel 142 166 18 28 0 "X" 1 0
[Port
0 1 1 X 130 180 142 166 18 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 250 170 20 20 
2 IlvMessageLabel 240 166 16 28 0 "U" 1 0
[Port
0 1 2 U 260 180 240 166 16 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
