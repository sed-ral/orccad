// Ilv Version: 2.4
// File generated: Fri Apr 23 10:00:18 1999
// Creator class: IlvGraphOutput
Palettes 7
6 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "wheat" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModPr 2
5 IlvFilledRectangle 110 80 160 160 
6 IlvMessageLabel 110 80 161 161 0 "Bidon_RP" 16 [Box
0 1 1 Bidon_RP 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 100 150 20 20 2 
2 IlvMessageLabel 122 146 16 28 0 "U" 1 0
[Port
0 1 1 U 110 160 122 146 16 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 260 150 20 20 2 
2 IlvMessageLabel 282 146 18 28 0 "X" 1 0
[Port
0 1 2 X 270 160 282 146 18 28 1  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 190 230 20 20 2 
2 IlvMessageLabel 212 226 98 28 0 "PreCond" 1 0
[Port
0 1 3 PreCond 200 240 212 226 98 28 1  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
