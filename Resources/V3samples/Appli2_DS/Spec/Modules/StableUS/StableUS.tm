// Ilv Version: 2.4
// File generated: Wed Jan 28 16:44:41 1998
// Creator class: IlvGraphOutput
Palettes 7
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
6 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 110 70 210 160 
6 IlvMessageLabel 110 70 211 161 0 "StableUS" 16 [Box
0 1 1 StableUS 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 140 20 20 
2 IlvMessageLabel 122 136 144 28 0 "State_vortex" 1 0
[Port
0 1 1 State_vortex 110 150 122 136 144 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 310 140 20 20 
2 IlvMessageLabel 241 136 134 28 0 "Com_vortex" 1 0
[Port
0 1 2 Com_vortex 320 150 241 136 134 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 310 190 20 20 2 
2 IlvMessageLabel 253 186 110 28 0 "Stabilized" 1 0
[Port
0 1 3 Stabilized 320 200 253 186 110 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
