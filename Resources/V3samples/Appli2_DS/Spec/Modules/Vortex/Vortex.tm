// Ilv Version: 2.4
// File generated: Tue Dec  9 16:39:02 1997
// Creator class: IlvGraphOutput
Palettes 6
4 "gray" "wheat" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModPr 2
4 IlvFilledRectangle 140 120 160 160 
5 IlvMessageLabel 140 120 161 161 0 "Vortex" 16 [Box
0 1 1 Vortex 
Box]

 } 0
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 290 220 20 20 2 
2 IlvMessageLabel 260 216 56 28 0 "state" 1 0
[Port
0 1 1 state 300 230 260 216 56 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 130 210 20 20 2 
2 IlvMessageLabel 152 206 16 28 0 "U" 1 0
[Port
0 1 2 U 140 220 152 206 16 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
