// Ilv Version: 2.4
// File generated: Fri Dec  4 13:29:05 1998
// Creator class: IlvGraphOutput
Palettes 7
4 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
3 65535 0 0 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
6 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 48830 48830 48830 "blue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 120 80 180 150 
6 IlvMessageLabel 120 80 181 151 0 "See_LEDs" 16 [Box
0 1 1 See_LEDs 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 110 150 20 20 
2 IlvMessageLabel 132 146 80 28 0 "4points" 1 0
[Port
0 1 1 4points 120 160 132 146 80 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 290 180 20 20 2 
2 IlvMessageLabel 263 176 50 28 0 "LOS" 1 0
[Port
0 1 3 LOS 300 190 263 176 50 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
4 IlvFilledEllipse 290 100 20 20 
2 IlvMessageLabel 237 96 102 28 0 "LED_pos" 1 0
[Port
0 1 4 LED_pos 300 110 237 96 102 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
