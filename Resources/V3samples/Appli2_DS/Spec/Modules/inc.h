// Orccad Version: 3.0 alpha   
// Module : TG
// Include and Definition File
// Date of creation : Mon May 26 13:38:53 1997

#include <stdlib.h>
#include <math.h>


typedef enum { 
    POLY_CPOS_TRAJ,
    POLY_CVEL_TRAJ,
    POLY_CACC_TRAJ,
    BANGBANG, 
    TRAPEZOIDAL,
    TRANS_PARABOLIQUE,
    POLY_VEL_LOCAL3,
    POLY_VEL_LOCAL5,
    SPLINE_CUBIC } Method_Interpolation;

#define SizeMatTrans    4
#define nbPointMax      25
#define nbAxeMax        15
#define PI              3.14159265358979323846

#define RADTODEG(rad)   ((rad)*(180./PI))
#define DEGTORAD(deg)   ((deg)*(PI/180.0))
#define MAX(a,b)        (a>b?a:b)
#define MIN(a,b)        (a<b?a:b)
#define SIGN(a)         (a>=0?1:-1)
#define ABS(a)          (a>=0?a:-a)

#define FALSE           0
#define TRUE            1

#define NO_EVENT        0
#define SET_EVENT       1

