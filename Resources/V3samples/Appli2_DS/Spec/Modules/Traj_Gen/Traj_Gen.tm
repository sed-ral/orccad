// Ilv Version: 3.1
// File generated: Thu Feb 24 10:50:45 2000
// Creator class: IlvGraphOutput
Palettes 6
3 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 110 90 160 140 
5 IlvMessageLabel 110 90 161 141 F8 0 1 16 4 "Traj_Gen"   [Box
0 1 1 Traj_Gen 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 150 20 20 
2 IlvMessageLabel 122 146 48 28 F8 0 25 16 4 "q_t_"   0
[Port
0 1 1 q_t_ 110 160 122 146 48 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 260 150 20 20 
2 IlvMessageLabel 251 146 14 28 F8 0 25 16 4 "q"   0
[Port
0 1 2 q 270 160 251 146 14 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortPrm 2
3 IlvPolyline 11
182 82 198 98 190 90 198 82 182 98 190 90 198 90 182 90 190 90 190 82
190 98 
2 IlvMessageLabel 180 95 42 28 F8 0 25 16 4 "Traj"   0
[Port
0 1 3 Traj 190 90 180 95 42 28 2  2 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
EOF
