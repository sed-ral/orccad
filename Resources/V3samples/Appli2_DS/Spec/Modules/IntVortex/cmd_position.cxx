#include "c_union.hxx"
#include "simul.h"

//mapping des forces et couples demandes sur l'ensemble d'actionneurs
void PROG::Fx()
{
    int i;

    pux[0] = pux[1] = -.36 * fx;
    pux[2] = pux[3] = .36 *fx;
    
    return;
}
void PROG::Fy()
{
    int i;
    
    puy[1] = puy[3] = .36 * fy;
    puy[2] = puy[0] = -.36 * fy;
    
    return;
}

void PROG::Fz()
{
    
    puz[4] = puz[5] = puz[6] = puz[7] = -.25 *fz;
    
    return;
}

void PROG::phi_torque()
//torque; /*couple de roulis demande*/
//*puphi;        /*poussee des moteurs*/
{
    puphi[7] = phitorque * 5.;
    puphi[6] = -puphi[7];
    
    return;
}

void PROG::psi_torque()
//double torque; /*couple de lacet demande*/
//double *pupsi;        /*poussee des moteurs*/
{
    pupsi[1] = pupsi[2] = psitorque * 1.6;
    pupsi[0] = pupsi[3] = -pupsi[1];

    return;
}

void PROG::theta_torque()
//double torque; /*couple de tangage demande*/
//double *putheta;        /*poussee des moteurs*/
{
    putheta[4] = thetatorque * 2.5;
    putheta[5] = -putheta[4];
    
    return;
}

//boucles de commande en position PD sur chaque axe
void PROG::autocap()
{

    psitorque = kppsi * (yc[11] - y[11]) - kdpsi * y[5];
    
    psi_torque();

    return;
}

void PROG::autoroulis()
{
    if((ABS(u[6])<Pmax)&&(ABS(u[7])<Pmax)) phiint += (yc[9] - y[9])*DT_PID*10.0;

    phitorque = kpphi * (yc[9] - y[9]) - kdphi * y[3] + kiphi*phiint;
	
    phi_torque();

    return;
}

void PROG::autotangage()
{
    if((ABS(u[4])<Pmax)&&(ABS(u[5])<Pmax)) thetaint += (yc[10] - y[10])*DT_PID*10.0;

    thetatorque = kptheta * (yc[10] - y[10]) - kdtheta * y[4] + kitheta*thetaint;
    
    theta_torque();
    
    return;
}

void PROG::autoxy()

{
    double dx,dy,dxr,dyr;

    //erreur en position
    dx = yc[6]-y[6];
    dy = yc[7]-y[7];

    dxr = dx*cos(y[11])+dy*sin(y[11]);
    dyr = -dx*sin(y[11])+dy*cos(y[11]);

    fx = kpx*dxr - kdx*y[0];
    fy = kpy*dyr - kdy*y[1];
  
    Fx();
    Fy();
    
    return;
}
void PROG::autoxyz()

{
    double dx,dy,dz,dxr,dyr,dzr;
    double cphi,ctheta,cpsi;
    double sphi,stheta,spsi;
    //erreur en position
    dx = yc[6] - y[6];
    dy = yc[7] - y[7];
    dz = yc[8] - y[8];

    cphi = cos(y[9]);
    sphi = sin(y[9]);
    ctheta = cos(y[10]);
    stheta = sin(y[10]);
    cpsi = cos(y[11]);
    spsi = sin(y[11]);
    

    dxr = dx*ctheta*cpsi + dy*ctheta*spsi - dz*stheta;
    dyr = dx*(sphi*stheta*cpsi-cphi*spsi) + dy*(ctheta*cpsi+sphi*spsi*stheta)
	  + dz*sphi*ctheta;
    dzr = dx*(sphi*spsi+cphi*stheta*cpsi) + dy*(cphi*stheta*spsi-cpsi*sphi)
	  + dz*cphi*ctheta;

    if((ABS(u[4])<Pmax)&&(ABS(u[5])<Pmax)&&(ABS(u[6])<Pmax)&&(ABS(u[7])<Pmax))
	zint += dz*DT_PID*10.0;

    fx = kpx*dxr - kdx*y[0];
    fy = kpy*dyr - kdy*y[1];
    fz = kpz*dzr - kdz*y[2] + kiz*zint;
  
    Fx();
    Fy();
    Fz();
    
    return;
}
void PROG::autoxyz_simple()

{
    double dx,dy,dz,dxr,dyr,dzr;
    double cphi,ctheta,cpsi;
    double sphi,stheta,spsi;
    //erreur en position
    dx = yc[6] - y[6];
    dy = yc[7] - y[7];
    dz = yc[8] - y[8];

    cpsi = cos(y[11]);
    spsi = sin(y[11]);
    

    dxr = dx*cpsi + dy*spsi;
    dyr = dx*(-spsi) + dy*(cpsi);

    fx = kpx*dxr - kdx*y[0];
    fy = kpy*dyr - kdy*y[1];
    fz = kpz*dz - kdz*y[2];
  
    Fx();
    Fy();
    Fz();
    
    return;
}

void PROG::autoz()
{
    double dz,dxr,dyr,dzr; 
    double cphi,ctheta,cpsi;
    double sphi,stheta,spsi; 

    dz = yc[8] - y[8];

    cphi = cos(y[9]);
    sphi = sin(y[9]);
    ctheta = cos(y[10]);
    stheta = sin(y[10]);
    cpsi = cos(y[11]);
    spsi = sin(y[11]);

    dxr =  - dz*stheta;
    dyr =  dz*sphi*ctheta;
    dzr =  dz*cphi*ctheta;

    if((ABS(u[4])<Pmax)&&(ABS(u[5])<Pmax)&&(ABS(u[6])<Pmax)&&(ABS(u[7])<Pmax))
	zint += dz*DT_PID*10.0; //mefiance..., que se passe-t-il si phi et theta!=0?
    
    fx = kpx*dxr - kdx*y[0];
    fy = kpy*dyr - kdy*y[1];
    fz = kpz*dzr - kdz*y[2] +kiz*zint;
  
    Fx();
    Fy();
    Fz();

    return;
}



    void PROG::raz()
{
int i;
    for(i=0;i<8;i++)	
	pupsi[i] = putheta[i] = puphi[i] = pux[i] = puy[i] = puz[i] = 0.0;
    pumax = punormal = 0.0;
    fx=fy=fz=phitorque=thetatorque=psitorque=0.0;

return;
}
void PROG::pdposition()
/* ------------------------------------------------------------
// Description de la commande du sous marin
// La commande implementee ici est une commande pd sur tous les axes
//
// ------------------------------------------------------------ */
//double *yc; /* Consign applied given for the command of the system */
//double *y; /* Vector issued from the process */
{
    /*autocap*/
     autocap();
   /*tangage*/
     autotangage();
/*roulis*/
     autoroulis();
/*position absolue*/
     autoxyz();
COMP_TIME_L("pdposition",2 MS);
}

void PROG::saturation()
/*controle de la saturation*/
{
int i;
     for(i=0;i<4;i++)	
	{
	    u[i] = pupsi[i] + pux[i] + puy[i];
	    pumax = MAX(pumax, ABS(u[i]));
	}
    if(pumax > UMAX)
	{
	    punormal = UMAX/pumax;
	    for (i=0;i<4;i++)
		u[i] *= punormal * .9999;
	}
    pumax = punormal = 0.0;
    for(i=4;i<8;i++)	
	{
	    u[i] = puphi[i] + putheta[i] + puz[i];
	    pumax = MAX(pumax, ABS(u[i]));
	}
    if(pumax > UMAX)
	{
	    punormal = UMAX/pumax;
	    for (i=4;i<8;i++)
		u[i] *= punormal * .9999;
	}
   
   
    return;
}

void PROG::left_wall()//suivi du mur gauche
{
autoz();
autotangage();
autoroulis();

 if(sensor_status == ALL_OK){
        L_IMA_RAY *us0_cpt = us0_header_pt->ray_pt;
        L_IMA_RAY *us1_cpt = us1_header_pt->ray_pt;
        L_IMA_RAY *us2_cpt = us2_header_pt->ray_pt;
        L_IMA_RAY *us3_cpt = us3_header_pt->ray_pt;
        L_IMA_RAY *us4_cpt = us4_header_pt->ray_pt;
        L_IMA_RAY *us5_cpt = us5_header_pt->ray_pt;
        L_IMA_RAY *us6_cpt = us6_header_pt->ray_pt;
        L_IMA_RAY *us7_cpt = us7_header_pt->ray_pt;

//psitorque = kpw*((us6_cpt[0].ima_ray[0]+bruit[6]-us5_cpt[0].ima_ray[0])-bruit[5]+ (us4_cpt[0].ima_ray[0]+bruit[4]-us3_cpt[0].ima_ray[0]-bruit[3])) -kdw*y[5];

psitorque = kpw*(us6_cpt[0].ima_ray[0]+bruit[6]-us5_cpt[0].ima_ray[0]-bruit[5]) -kdw*y[5];

fx = -kpx*(yc[6]-(us4_cpt[0].ima_ray[0]+bruit[4]+us3_cpt[0].ima_ray[0]+bruit[3])/2.0) -kdx*y[0];

fy = kpy*(yc[7]-(us6_cpt[0].ima_ray[0]+bruit[6]+us5_cpt[0].ima_ray[0]+bruit[5])/2.0) -kdy*y[1];

	psi_torque();
	Fx();
	Fy();}
return;
}

void PROG::right_wall()//suivi du mur droit
{
autoz();
autotangage();
autoroulis();

 if(sensor_status == ALL_OK){
        L_IMA_RAY *us0_cpt = us0_header_pt->ray_pt;
        L_IMA_RAY *us1_cpt = us1_header_pt->ray_pt;
        L_IMA_RAY *us2_cpt = us2_header_pt->ray_pt;
        L_IMA_RAY *us3_cpt = us3_header_pt->ray_pt;
        L_IMA_RAY *us4_cpt = us4_header_pt->ray_pt;
        L_IMA_RAY *us5_cpt = us5_header_pt->ray_pt;
        L_IMA_RAY *us6_cpt = us6_header_pt->ray_pt;
        L_IMA_RAY *us7_cpt = us7_header_pt->ray_pt;

psitorque = kpw*((us2_cpt[0].ima_ray[0]+bruit[2]-us1_cpt[0].ima_ray[0])-bruit[1]+
		 (us4_cpt[0].ima_ray[0]+bruit[4]-us3_cpt[0].ima_ray[0]-bruit[3])) -kdw*y[5];
fx = -kpx*(yc[6]-(us4_cpt[0].ima_ray[0]+bruit[4]+us3_cpt[0].ima_ray[0]+bruit[3])/2.0) -kdx*y[0];
fy = -kpy*(yc[7]-(us2_cpt[0].ima_ray[0]+bruit[2]+us1_cpt[0].ima_ray[0]+bruit[1])/2.0) -kdy*y[1];

	psi_torque();
	Fx();
	Fy();}
return;
}
