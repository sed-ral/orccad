// Ilv Version: 2.4
// File generated: Tue Jan 13 11:12:14 1998
// Creator class: IlvGraphOutput
Palettes 6
5 44461 55512 59110 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 44461 55512 59110 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
1 44461 55512 59110 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 44461 55512 59110 62965 57054 46003 "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModPr 2
4 IlvFilledRectangle 130 110 150 160 
5 IlvMessageLabel 130 110 151 161 0 "IntVortex" 16 [Box
0 1 1 IntVortex 
Box]

 } 0
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 270 200 20 20 2 
2 IlvMessageLabel 229 196 78 28 0 "Output" 1 0
[Port
0 1 1 Output 280 210 229 196 78 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 120 200 20 20 2 
2 IlvMessageLabel 142 196 16 28 0 "U" 1 0
[Port
0 1 2 U 130 210 142 196 16 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
