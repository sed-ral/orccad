// Ilv Version: 2.4
// File generated: Fri Dec  4 12:34:50 1998
// Creator class: IlvGraphOutput
Palettes 5
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "red" "red" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
3 IlvFilledRectangle 120 110 170 130 
4 IlvMessageLabel 120 110 171 131 0 "Wind_detector" 16 [Box
0 1 1 Wind_detector 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 280 130 20 20 2 
2 IlvMessageLabel 302 126 114 28 0 "Storm_ON" 1 0
[Port
0 1 1 Storm_ON 290 140 302 126 114 28 1  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 280 190 20 20 2 
2 IlvMessageLabel 302 186 128 28 0 "Storm_OFF" 1 0
[Port
0 1 2 Storm_OFF 290 200 302 186 128 28 1  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
