// Ilv Version: 2.4
// File generated: Fri Feb 12 14:39:17 1999
// Creator class: IlvGraphOutput
Palettes 7
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
6 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 65535 0 0 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
3 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 110 100 180 150 
6 IlvMessageLabel 110 100 181 151 0 "Servoing" 16 [Box
0 1 1 Servoing 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 160 20 20 
2 IlvMessageLabel 122 156 70 28 0 "Vision" 1 0
[Port
0 1 1 Vision 110 170 122 146 70 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 280 160 20 20 
2 IlvMessageLabel 243 156 70 28 0 "Screw" 1 0
[Port
0 1 2 Screw 290 170 243 146 70 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 280 210 20 20 2 
2 IlvMessageLabel 302 206 150 28 0 "UnStableCam" 1 0
[Port
0 1 3 UnStableCam 290 220 302 196 150 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 190 240 20 20 2 
2 IlvMessageLabel 212 236 102 28 0 "Centered" 1 0
[Port
0 1 4 Centered 200 250 212 226 102 28 8  8 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
