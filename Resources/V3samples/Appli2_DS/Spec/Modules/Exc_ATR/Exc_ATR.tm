// Ilv Version: 2.4
// File generated: Tue Feb 22 12:05:13 2000
// Creator class: IlvGraphOutput
Palettes 5
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "gray" "turquoise" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAtr 2
3 IlvFilledRectangle 130 110 160 150 
4 IlvMessageLabel 130 110 161 151 0 "Exc_ATR" 16 [Box
0 1 1 Exc_ATR 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 120 130 20 20 2 
2 IlvMessageLabel 142 126 100 28 0 "End_Traj" 1 0
[Port
0 1 1 End_Traj 130 140 142 126 100 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 120 180 20 20 2 
2 IlvMessageLabel 142 176 118 28 0 "Joint_Limit" 1 0
[Port
0 1 2 Joint_Limit 130 190 142 176 118 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 120 220 20 20 2 
2 IlvMessageLabel 142 216 94 28 0 "Non_Inv" 1 0
[Port
0 1 3 Non_Inv 130 230 142 216 94 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
