// Ilv Version: 2.4
// File generated: Fri Feb 12 14:39:05 1999
// Creator class: IlvGraphOutput
Palettes 6
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
4 48830 48830 48830 62965 57054 46003 "default" "fixed" 0 solid solid 0 0 0
3 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModPr 2
4 IlvFilledRectangle 100 90 180 170 
5 IlvMessageLabel 100 90 181 171 0 "ROV" 16 [Box
0 1 1 ROV 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 90 170 20 20 2 
2 IlvMessageLabel 112 166 16 28 0 "U" 1 0
[Port
0 1 1 U 100 180 122 156 16 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 270 120 20 20 2 
2 IlvMessageLabel 292 116 32 28 0 "US" 1 0
[Port
0 1 2 US 280 130 302 106 32 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 270 210 20 20 2 
2 IlvMessageLabel 292 206 84 28 0 "VISION" 1 0
[Port
0 1 3 VISION 280 220 302 196 84 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
