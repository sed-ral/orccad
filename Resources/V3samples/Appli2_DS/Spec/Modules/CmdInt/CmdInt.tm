// Ilv Version: 2.4
// File generated: Mon Dec 15 11:20:54 1997
// Creator class: IlvGraphOutput
Palettes 7
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
3 44461 55512 59110 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
6 44461 55512 59110 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 44461 55512 59110 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
5 44461 55512 59110 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 120 110 150 140 
6 IlvMessageLabel 120 110 151 141 0 "CmdInt" 16 [Box
0 1 1 CmdInt 
Box]

 } 0
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 110 190 20 20 
2 IlvMessageLabel 132 186 56 28 0 "state" 1 0
[Port
0 1 1 state 120 200 132 186 56 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 260 140 20 20 
2 IlvMessageLabel 250 136 16 28 0 "U" 1 0
[Port
0 1 2 U 270 150 250 136 16 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 260 210 20 20 2 
2 IlvMessageLabel 206 206 104 28 0 "EndCond" 1 0
[Port
0 1 3 EndCond 270 220 206 206 104 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
