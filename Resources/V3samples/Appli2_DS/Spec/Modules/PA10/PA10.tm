// Ilv Version: 2.4
// File generated: Fri Jan 30 15:29:35 1998
// Creator class: IlvGraphOutput
Palettes 8
1 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
5 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
3 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
6 48830 48830 48830 62965 57054 46003 "default" "fixed" 0 solid solid 0 0 0
7 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModPr 2
6 IlvFilledRectangle 140 120 160 170 
7 IlvMessageLabel 140 120 161 171 0 "PA10" 16 [Box
0 1 1 PA10 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 130 200 20 20 2 
2 IlvMessageLabel 152 196 16 28 0 "U" 1 0
[Port
0 1 1 U 140 210 152 196 16 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 290 200 20 20 2 
2 IlvMessageLabel 279 196 18 28 0 "X" 1 0
[Port
0 1 2 X 300 210 279 196 18 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 290 250 20 20 2 
5 IlvMessageLabel 227 246 122 28 0 "Joint_Limit" 1 0
[Port
0 1 3 Joint_Limit 300 260 227 246 122 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
