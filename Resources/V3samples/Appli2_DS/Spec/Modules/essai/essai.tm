// Ilv Version: 2.4
// File generated: Wed Jul 29 14:29:07 1998
// Creator class: IlvGraphOutput
Palettes 6
3 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "gray" "turquoise" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAtr 2
4 IlvFilledRectangle 110 110 180 190 
5 IlvMessageLabel 110 110 181 191 0 "essai" 16 [Box
0 1 1 essai 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 280 220 20 20 
2 IlvMessageLabel 228 216 100 28 0 "Port_Var" 1 0
[Port
0 1 1 Port_Var 290 230 228 216 100 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 100 200 20 20 2 
2 IlvMessageLabel 122 196 98 28 0 "Port_Evt" 1 0
[Port
0 1 2 Port_Evt 110 210 122 196 98 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 100 130 20 20 2 
2 IlvMessageLabel 122 126 60 28 0 "Port2" 1 0
[Port
0 1 3 Port2 110 140 122 126 60 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
