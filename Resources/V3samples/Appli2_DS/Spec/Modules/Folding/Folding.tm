// Ilv Version: 2.4
// File generated: Fri Dec  4 16:47:52 1998
// Creator class: IlvGraphOutput
Palettes 7
1 48830 48830 48830 "red" "default" "fixed" 0 solid solid 0 0 0
6 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 65535 0 0 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 110 110 170 130 
6 IlvMessageLabel 110 110 171 131 0 "Folding" 16 [Box
0 1 1 Folding 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 130 20 20 
2 IlvMessageLabel 122 126 76 28 0 "control" 1 0
[Port
0 1 1 control 110 140 122 126 76 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 270 160 20 20 2 
2 IlvMessageLabel 292 156 102 28 0 "FOLDED" 1 0
[Port
0 1 2 FOLDED 280 170 292 156 102 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
4 IlvFilledEllipse 100 200 20 20 
2 IlvMessageLabel 122 196 64 28 0 "status" 1 0
[Port
0 1 3 status 110 210 122 196 64 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
