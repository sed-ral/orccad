// Ilv Version: 2.4
// File generated: Fri Dec  4 16:57:17 1998
// Creator class: IlvGraphOutput
Palettes 6
5 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 0 0 65535 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
1 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 48830 48830 48830 16448 57568 53456 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAtr 2
4 IlvFilledRectangle 110 90 170 150 
5 IlvMessageLabel 110 90 171 151 0 "Deploy_Atr" 16 [Box
0 1 1 Deploy_Atr 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 100 160 20 20 2 
2 IlvMessageLabel 122 156 136 28 0 "DEPLOYED" 1 0
[Port
0 1 1 DEPLOYED 110 170 122 156 136 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 100 110 20 20 2 
2 IlvMessageLabel 122 106 114 28 0 "Storm_ON" 1 0
[Port
0 1 2 Storm_ON 110 120 122 106 114 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 100 200 20 20 2 
2 IlvMessageLabel 122 196 128 28 0 "Storm_OFF" 1 0
[Port
0 1 3 Storm_OFF 110 210 122 196 128 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
