// Ilv Version: 2.21
// File generated: Thu Mar 20 13:49:39 1997
// Creator class: IlvGraphOutput
Palettes 5
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "blue" "blue" "default" "9x15" 0 solid solid 0 0 0
"default" 0 "LightBlue" "black" "default" "9x15" 0 solid solid 0 0 0
3 "LightBlue" "turquoise" "default" "9x15" 0 solid solid 0 0 0
4 "LightBlue" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 3
3 0 { 0 0 OrcIlvModAtr 2
3 IlvFilledRectangle 120 100 150 160 
4 IlvMessageLabel 120 100 151 161 0 "ATR1_VORT" 16 [Box
0 1 1 ATR1_VORT 
Box]

 } 0
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 110 190 20 20 2 
2 IlvMessageLabel 132 186 102 28 0 "StabPost" 1 0
[Port
0 1 1 StabPost 120 200 132 186 102 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
