// Ilv Version: 2.4
// File generated: Tue Feb  3 13:16:53 1998
// Creator class: IlvGraphOutput
Palettes 9
3 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
6 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 65535 0 0 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
7 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
8 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 "red" "red" "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
7 IlvFilledRectangle 100 80 230 200 
8 IlvMessageLabel 100 80 231 201 0 "StableCam" 16 [Box
0 1 1 StableCam 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 170 20 20 
2 IlvMessageLabel 112 166 144 28 0 "State_vortex" 1 0
[Port
0 1 1 State_vortex 100 180 112 166 144 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 320 120 20 20 
2 IlvMessageLabel 251 116 134 28 0 "Com_vortex" 1 0
[Port
0 1 4 Com_vortex 330 130 251 116 134 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 320 170 20 20 2 
2 IlvMessageLabel 239 166 158 28 0 "UnStableBase" 1 0
[Port
0 1 5 UnStableBase 330 180 239 166 158 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
5 IlvReliefDiamond 320 230 20 20 2 
6 IlvMessageLabel 266 226 104 28 0 "Centered" 1 0
[Port
0 1 6 Centered 330 240 266 226 104 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 26
