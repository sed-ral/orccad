// Ilv Version: 2.4
// File generated: Tue Sep 22 14:46:59 1998
// Creator class: IlvGraphOutput
Palettes 6
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "wheat" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModPr 2
4 IlvFilledRectangle 90 100 150 160 
5 IlvMessageLabel 90 100 151 161 0 "Praxicar" 16 [Box
0 1 1 Praxicar 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 80 170 20 20 2 
2 IlvMessageLabel 102 166 54 28 0 "Input" 1 0
[Port
0 1 1 Input 90 180 102 166 54 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 230 170 20 20 2 
2 IlvMessageLabel 252 166 74 28 0 "Output" 1 0
[Port
0 1 2 Output 240 180 252 166 74 28 1  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
