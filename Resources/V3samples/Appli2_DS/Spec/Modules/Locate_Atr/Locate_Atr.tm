// Ilv Version: 2.4
// File generated: Fri Dec  4 18:07:12 1998
// Creator class: IlvGraphOutput
Palettes 6
1 0 0 65535 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
3 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 48830 48830 48830 16448 57568 53456 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAtr 2
4 IlvFilledRectangle 120 100 170 130 
5 IlvMessageLabel 120 100 171 131 0 "Locate_Atr" 16 [Box
0 1 1 Locate_Atr 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 110 130 20 20 2 
2 IlvMessageLabel 132 126 114 28 0 "Storm_ON" 1 0
[Port
0 1 1 Storm_ON 120 140 132 126 114 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 110 180 20 20 2 
2 IlvMessageLabel 132 176 128 28 0 "Storm_OFF" 1 0
[Port
0 1 2 Storm_OFF 120 190 132 176 128 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 200 90 20 20 2 
2 IlvMessageLabel 198 105 50 28 0 "LOS" 1 0
[Port
0 1 3 LOS 210 100 198 105 50 28 2  2 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
