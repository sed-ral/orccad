// Ilv Version: 2.4
// File generated: Wed Jan 28 14:54:52 1998
// Creator class: IlvGraphOutput
Palettes 6
1 44461 55512 59110 "red" "default" "fixed" 0 solid solid 0 0 0
5 44461 55512 59110 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 44461 55512 59110 "blue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 44461 55512 59110 62965 57054 46003 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModPr 2
4 IlvFilledRectangle 130 110 140 130 
5 IlvMessageLabel 130 110 141 131 0 "VORTEX" 16 [Box
0 1 1 VORTEX 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 260 170 20 20 2 
2 IlvMessageLabel 249 166 18 28 0 "X" 1 0
[Port
0 1 1 X 270 180 249 166 18 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 120 170 20 20 2 
2 IlvMessageLabel 142 166 16 28 0 "U" 1 0
[Port
0 1 2 U 130 180 142 166 16 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
