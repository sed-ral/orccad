// Ilv Version: 3.1
// File generated: Mon Feb 28 10:01:39 2000
// Creator class: IlvGraphOutput
Palettes 6
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
3 "red" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 90 90 150 130 
5 IlvMessageLabel 90 90 151 131 F8 0 1 16 4 "Check_Init"   [Box
0 1 1 Check_Init 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 80 140 20 20 
2 IlvMessageLabel 102 136 54 28 F8 0 25 16 4 "Input"   0
[Port
0 1 1 Input 90 150 102 136 54 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 230 140 20 20 2 
2 IlvMessageLabel 167 136 122 28 F8 0 25 16 4 "Check_OK"   0
[Port
0 1 3 Check_OK 240 150 167 136 122 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
EOF
