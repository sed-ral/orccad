// Ilv Version: 2.4
// File generated: Fri Dec  4 11:58:07 1998
// Creator class: IlvGraphOutput
Palettes 6
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "wheat" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModPr 2
4 IlvFilledRectangle 110 90 180 160 
5 IlvMessageLabel 110 90 181 161 0 "Stereo" 16 [Box
0 1 1 Stereo 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 100 160 20 20 2 
2 IlvMessageLabel 122 156 80 28 0 "Control" 1 0
[Port
0 1 1 Control 110 170 122 156 80 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 280 130 20 20 2 
2 IlvMessageLabel 302 126 68 28 0 "Status" 1 0
[Port
0 1 2 Status 290 140 302 126 68 28 1  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 280 190 20 20 2 
2 IlvMessageLabel 302 186 66 28 0 "Image" 1 0
[Port
0 1 3 Image 290 200 302 186 66 28 1  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
