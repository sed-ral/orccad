// Ilv Version: 3.1
// File generated: Wed Jun 14 16:26:24 2006
// Creator class: IlvGraphOutput
Palettes 5
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 4 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "turquoise" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAtr 2
3 IlvFilledRectangle 120 90 210 170 
4 IlvMessageLabel 120 90 211 171 F8 0 1 16 4 "StableCam_ATR"   [Box
0 1 1 StableCam_ATR 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 110 120 20 20 2 
2 IlvMessageLabel 130 116 150 28 F8 0 25 16 4 "UnStableCam"   0
[Port
0 1 1 UnStableCam 120 130 130 116 150 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 110 210 20 20 2 
2 IlvMessageLabel 130 206 102 28 F8 0 25 16 4 "Centered"   0
[Port
0 1 2 Centered 120 220 130 206 102 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
EOF
