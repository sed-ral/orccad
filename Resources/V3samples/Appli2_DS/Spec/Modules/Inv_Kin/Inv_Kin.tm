// Ilv Version: 2.4
// File generated: Tue Feb 22 15:04:35 2000
// Creator class: IlvGraphOutput
Palettes 7
6 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
1 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
4 65535 0 0 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
3 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 130 110 160 150 
6 IlvMessageLabel 130 110 161 151 0 "Inv_Kin" 16 [Box
0 1 1 Inv_Kin 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 120 180 20 20 
2 IlvMessageLabel 142 176 110 28 0 "Joint_Traj" 1 0
[Port
0 1 1 Joint_Traj 130 190 142 176 110 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortPrm 2
3 IlvPolyline 11
282 182 298 198 290 190 298 182 282 198 290 190 298 190 282 190 290 190 290 182
290 198 
2 IlvMessageLabel 241 176 74 28 0 "X_Traj" 1 0
[Port
0 1 2 X_Traj 290 190 241 176 74 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 160 250 20 20 2 
2 IlvMessageLabel 182 246 118 28 0 "Joint_Limit" 1 0
[Port
0 1 3 Joint_Limit 170 260 182 246 118 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 230 250 20 20 2 
2 IlvMessageLabel 252 246 50 28 0 "Sing" 1 0
[Port
0 1 4 Sing 240 260 252 246 50 28 8  8 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
