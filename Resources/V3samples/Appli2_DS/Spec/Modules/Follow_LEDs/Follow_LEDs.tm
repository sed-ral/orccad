// Ilv Version: 2.4
// File generated: Fri Dec  4 15:15:26 1998
// Creator class: IlvGraphOutput
Palettes 6
3 48830 48830 48830 "red" "default" "fixed" 0 solid solid 0 0 0
4 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 120 80 180 150 
5 IlvMessageLabel 120 80 181 151 0 "Follow_LEDs" 16 [Box
0 1 1 Follow_LEDs 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 110 190 20 20 
2 IlvMessageLabel 132 186 120 28 0 "Rover_pos" 1 0
[Port
0 1 1 Rover_pos 120 200 132 186 120 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 290 150 20 20 
2 IlvMessageLabel 250 146 76 28 0 "control" 1 0
[Port
0 1 4 control 300 160 250 146 76 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
1 IlvFilledEllipse 110 100 20 20 
2 IlvMessageLabel 132 96 68 28 0 "Status" 1 0
[Port
0 1 5 Status 120 110 132 96 68 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
