// Ilv Version: 3.1
// File generated: Fri Jul 21 17:22:12 2000
// Creator class: IlvGraphOutput
Palettes 6
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 70 60 210 210 
5 IlvMessageLabel 70 60 211 211 F8 0 1 16 4 "Brake_On"   [Box
0 1 1 Brake_On 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 60 160 20 20 
2 IlvMessageLabel 82 156 18 28 F8 0 25 16 4 "Y"   0
[Port
0 1 1 Y 70 170 82 156 18 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 270 160 20 20 
2 IlvMessageLabel 260 156 16 28 F8 0 25 16 4 "U"   0
[Port
0 1 2 U 280 170 260 156 16 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
EOF
