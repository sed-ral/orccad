// Orccad Version: 3.0 alpha  
// Module generated with specialised hierarchy: TG_SE3
// Computation file for path generation in cartesian space 

// WARNING: in the following lines, the expression Ai%Auo.t()
//          is the multiplication of matrix Ai and Auo and
//          not the multiplication of matrix Ai and Auo.t()
//          where t() is the transposition operation.
//          This, because of the % operator of the matrix
//          class used (see C++ Matrix class of UCLA university).


// to allow user to change trajectory points
// during computation of first trajectory
if (NbPoint!=0) init();

if (tfi[itraj])  r1 = time/tfi[itraj];
else  r1 = 0.0;


switch(methode) {
/**********  velocity null on via-point ***********/ 
/* linear interpolation : calcul of linear        */
/* interpolation.                                 */
/**************************************************/
case POLY_CPOS_TRAJ: {
    Pt.resize( traj[itraj].p() + r1*D[itraj] );
    Ai.resize( traj[itraj].a() );
    doubleMatrix Auo( &uteta[itraj][0], r1*uteta[itraj][3] );
    At.resize( Ai%Auo.t() );
}
break;

/**********  velocity null on via-point ***********/ 
/* Interpolation with 3 degree polynomial : this  */
/* law allow continu velocity.                    */
/**************************************************/
case POLY_CVEL_TRAJ: {
    r2 = r1*r1;
    Rt = 3*r2 -2*r1*r2;
    Pt.resize( traj[itraj].p() + r1*D[itraj] );
    Ai.resize( traj[itraj].a() );
    doubleMatrix Auo( &uteta[itraj][0], r1*uteta[itraj][3] );
    At.resize( Ai%Auo.t() );
}
break;

/**********  velocity null on via-point ***********/ 
/* Interpolation with 5 degree polynomial : this  */
/* law allow continu acceleration.                */
/**************************************************/
case POLY_CACC_TRAJ: {
    r2 = r1*r1;
    r3 = r2*r1;
    r4 = r3*r1;
    Rt = 10*r3 - 15*r4 + 6*r1*r4;
    Pt.resize( traj[itraj].p() + r1*D[itraj] );
    Ai.resize( traj[itraj].a() );
    doubleMatrix Auo( &uteta[itraj][0], r1*uteta[itraj][3] );
    At.resize( Ai%Auo.t() );
}
break;

/************  velocity null on via-point *************/ 
/* Interpolation with Bang-Bang law :                 */
/* constant acceleration up to tf/2 and constant      */
/* deceleration up to the end of the path portion.    */
/******************************************************/
case BANGBANG: {
    r2 = r1*r1;
    if ((time>=0.0) && (time<=(tfi[itraj]/2)))
        Rt = 2*r2;  
    else Rt = -1 + 4*r1 - 2*r2;
    Pt.resize( traj[itraj].p() + r1*D[itraj] );
    Ai.resize( traj[itraj].a() );
    doubleMatrix Auo( &uteta[itraj][0], r1*uteta[itraj][3] );
    At.resize( Ai%Auo.t() );
}
break;

/************  velocity null on via-point *************/ 
/* Trapezoidal interpolation law in cartesian space   */
/* This law allow the minus time to have. The         */
/* acceleration and slowing down times are the same   */
/* for each joint.                                    */
/******************************************************/
case TRAPEZOIDAL: {
    Pt.resize(1, 3, 0.0);
    if ((time>=0.0) && (time<=Topt[itraj])) {
	for (i=0; i<3; i++) 
	    Pt.X[i] = traj[itraj].P[i] 
		+ 0.5*Ka[0]*Uopt[itraj][i]*time*time*SIGN(D[itraj].X[i]);
	teta = 0.5*time*time*Ka[1]*Uopt[itraj][3]*SIGN(uteta[itraj][3]);
	
    }
    if ((time>Topt[itraj]) && (time<=(tfi[itraj]-Topt[itraj]))) {  
	for (i=0; i<3; i++) 
	    Pt.X[i] = traj[itraj].P[i] 
		+ (time-0.5*Topt[itraj])*Kv[0]*Lopt[itraj][i]*SIGN(D[itraj].X[i]);
	teta = (time-0.5*Topt[itraj])*Kv[1]*Lopt[itraj][3]*SIGN(uteta[itraj][3]);
    }
    if ((time>(tfi[itraj]-Topt[itraj])) && (time<=(tfi[itraj]+dt))){ 
	for (i=0; i<3; i++) 
	    Pt.X[i] = traj[itraj+1].P[i] 
		- 0.5*(tfi[itraj]-time)*(tfi[itraj]-time)
		*Ka[0]*Uopt[itraj][i]*SIGN(D[itraj].X[i]);
	teta = uteta[itraj][3] - 0.5*(tfi[itraj]-time)*(tfi[itraj]-time)
	    *Ka[1]*Uopt[itraj][3]*SIGN(uteta[itraj][3]);
    }
    Ai.resize(traj[itraj].a());
    doubleMatrix Auo(&uteta[itraj][0],teta);
    At.resize(Ai%Auo.t());
}
break;

/************  no null velocity on via-point *************/ 
/* Linear interpolation and parabolic blend in cartesian */
/* space.                                                */
/* The mouvement is composed of constant velocity parts  */
/* and  constant acceleration parts. The trajectory      */
/* don't passe on via-points but near them.              */
/*********************************************************/
case TRANS_PARABOLIQUE:
    if ((time>=0.0) && (time<=taux[itraj])) {
	     /* calcul parabolic part at the beginning of trajectory segment */
	if (!itraj) {   
	    // the path began with a linear part, this to start at the
            // present position
	    Pt.resize(traj[itraj].p() + r1*D[itraj]);    
	    Ai.resize(traj[itraj].a());
	    doubleMatrix Auo(&uteta[itraj][0],r1*uteta[itraj][3]);
	    At.resize(Ai%Auo.t());
	}
	else {
	    TmoinsTaux = time - taux[itraj];      
	    TplusTaux = time + taux[itraj];
	    val = 4 * taux[itraj];
	    Rt1 = TmoinsTaux*TmoinsTaux/(val*tfi[itraj-1]);
	    Rt2 = TplusTaux*TplusTaux/(val*tfi[itraj]);
	    Pt.resize(traj[itraj].p() - Rt1*D[itraj-1] + Rt2*D[itraj]);    
	    Ai.resize(traj[itraj].a());
	    doubleMatrix Auo(&uteta[itraj-1][0],-Rt1*uteta[itraj-1][3]);
	    At.resize(Ai%Auo.t());
	    Auo.resize( doubleMatrix(&uteta[itraj][0],Rt2*uteta[itraj][3]) );
	    At.resize(At%Auo.t()); 
	}
    } 
                                          /* calcul linear interpolation part */
    if ((time>taux[itraj]) && (time<=(tfi[itraj]-taux[itraj]))) {	
	Pt.resize(traj[itraj].p() + r1*D[itraj]);  
	Ai.resize(traj[itraj].a());
	doubleMatrix Auo(&uteta[itraj][0],r1*uteta[itraj][3]);
	At.resize(Ai%Auo.t());
    }
                      /* calcul parabolic blend at the end of trajectory part */
    
/*     cout << "time = " << time << " itraj = " << itraj  */
/* 	 << " tfi[itraj] = " << tfi[itraj] << endl; */
                                /* le +dt en dessous,c'est de la bidouille !!! */
                           /* mais sinon qd time = tfi[itraj], le test est nul */
    if ( (time>(tfi[itraj]-taux[itraj])) && (time<= (tfi[itraj]+dt)) ) {
	if (itraj==(nbPoint-2) ) {  
	    // to reach the last point, the last blend is linear
	    Pt.resize(traj[itraj].p() + r1*D[itraj]);
	    Ai.resize(traj[itraj].a());
	    doubleMatrix Auo(&uteta[itraj][0],r1*uteta[itraj][3]);
	    At.resize(Ai%Auo.t());
	}
	else {    
	    TmoinsTaux = time - taux[itraj] - tfi[itraj];
	    TplusTaux =  time + taux[itraj] - tfi[itraj];
	    val = 4 * taux[itraj];
	    Rt1 = TmoinsTaux*TmoinsTaux/(val*tfi[itraj]);
	    Rt2 = TplusTaux*TplusTaux/(val*tfi[itraj+1]);
	    Pt.resize(traj[itraj+1].p() - Rt1*D[itraj] + Rt2*D[itraj+1]);
	    Ai.resize(traj[itraj+1].a());
	    doubleMatrix Auo(&uteta[itraj][0],-Rt1*uteta[itraj][3]);
	    At.resize(Ai%Auo.t());
	    Auo.resize( doubleMatrix(&uteta[itraj+1][0],Rt2*uteta[itraj+1][3]) );
	    At.resize(At%Auo.t());
	}
    }
    break;


/************  no null velocity on via-point *************/ 
/* Use of cubic polynomial to interpolate path parts.    */
/* This method allow continu velocity all along the path.*/
/* Velocity on via-points are equals to the medium slope */
/* between the n-1 and the n+1 points.                   */
/*********************************************************/
case POLY_VEL_LOCAL3: {
    r2 = r1*r1;
    r3 = r2*r1;
    Pt.resize( traj[itraj].p() + time*VelTrans[itraj]
	       + r2*( 3*D[itraj] - tfi[itraj]*(2*VelTrans[itraj]
					       + VelTrans[itraj+1]))
	       + r3*( -2*D[itraj] + tfi[itraj]*(VelTrans[itraj]
						+ VelTrans[itraj+1])));
    teta = 0.0 + time*VelRot[itraj]
	+ r2*( 3*uteta[itraj][3] - tfi[itraj]*(2*VelRot[itraj] 
					    + VelRot[itraj+1]))
	+ r3*( -2*uteta[itraj][3] + tfi[itraj]*(VelRot[itraj] 
					     + VelRot[itraj+1]));
    doubleMatrix Auo(&uteta[itraj][0], teta);
    Ai.resize( traj[itraj].a() );
    At.resize(Ai%Auo.t());
}
break;

/************  no null velocity on via-points ************/ 
/* Use of quintic polynomial to interpolate path parts.  */
/* This method allow linear velocity and continu         */
/* acceleration (accelerations on via-points are null).  */
/* Velocity on via-points are equals to the medium slope */
/* between the n-1 and the n+1 two points.               */
/*********************************************************/
case POLY_VEL_LOCAL5: {
    r2 = r1*r1;
    r3 = r2*r1;
    r4 = r3*r1;
    r5 = r4*r1;
    Pt.resize( traj[itraj].p() + time*VelTrans[itraj]
	       + r3*( 10*D[itraj] - tfi[itraj]*(6*VelTrans[itraj]
					       + 4*VelTrans[itraj+1]))
	       + r4*( -15*D[itraj] + tfi[itraj]*(8*VelTrans[itraj]
						+ 7*VelTrans[itraj+1]))
	       + r5*( 6*D[itraj] - 3* tfi[itraj]*(VelTrans[itraj]
						+ VelTrans[itraj+1])));
    teta = 0.0 + time*VelRot[itraj]
	+ r3*( 10*uteta[itraj][3] - tfi[itraj]*(6*VelRot[itraj] + 4*VelRot[itraj+1]))
	+ r4*( -15*uteta[itraj][3] + tfi[itraj]*(8*VelRot[itraj] + 7*VelRot[itraj+1]))
	+ r5*( 6*uteta[itraj][3] - 3*tfi[itraj]*(VelRot[itraj] + VelRot[itraj+1]));
    doubleMatrix Auo(&uteta[itraj][0], teta);
    Ai.resize( traj[itraj].a() );
    At.resize(Ai%Auo.t());
}
break;

/************  no null velocity on via-points ************/ 
/* Use of cubic spline function to interpolate path      */
/* parts. Here velocities on via-points are calculed to  */
/* have linear velocity and continu acceleration all     */
/* along the path. Velocity matrix is calculed at        */
/* initialisation time.                                  */
/*********************************************************/
case SPLINE_CUBIC: {
    r2 = r1*r1;
    r3 = r2*r1;
    r4 = r3*r1;
    cout << "dans compute.c" << endl;
    if (!itraj) {    /* for the first part of the trajectory */
	Pt.resize(traj[itraj].p() 
		  + r3*(4*D[itraj] - tfi[itraj]*VelTrans[itraj])
		  - r4*(3*D[itraj] + tfi[itraj]*VelTrans[itraj]));
	teta = r3*( 4*uteta[itraj][3] - tfi[itraj]*VelRot[itraj] )
 	    + r4*( -3*uteta[itraj][3] + tfi[itraj]*VelRot[itraj] );
	doubleMatrix Auo(&uteta[itraj][0],teta);
	Ai.resize( traj[itraj].a() );
 	At.resize(Ai%Auo.t()); 
    }
    else {         
	if (itraj==nbPoint-2) {  /* for the last part */
	    Pt.resize(traj[itraj].p() + time*VelTrans[itraj]
		      + r2*( 6*D[itraj] - 3*tfi[itraj]*VelTrans[itraj] )
		      + r3*( -8*D[itraj] + 3*tfi[itraj]*VelTrans[itraj] )
		      + r4*( 3*D[itraj] - tfi[itraj]*VelTrans[itraj] ));    
	    teta = time*VelRot[itraj]
		+ r2*( 6*uteta[itraj][3] - 3*tfi[itraj]*VelRot[itraj] )
		+ r3*( -8*uteta[itraj][3] + 3*tfi[itraj]*VelRot[itraj] )
		+ r4*( 3*uteta[itraj][3] - tfi[itraj]*VelRot[itraj] );    
	    doubleMatrix Auo(&uteta[itraj][0],teta);
	    Ai.resize( traj[itraj].a() );
	    At.resize(Ai%Auo.t());
	}
	else {               /* for other parts */
	    Pt.resize( traj[itraj].p() + time*VelTrans[itraj]
		       + r2*( 3*D[itraj] - tfi[itraj]*(2*VelTrans[itraj]
						       + VelTrans[itraj+1]))
		       + r3*( -2*D[itraj] + tfi[itraj]*(VelTrans[itraj]	
							+ VelTrans[itraj+1])));
	    teta = time*VelRot[itraj]
		+ r2*( 3*uteta[itraj][3] 
		       - tfi[itraj]*(2*VelRot[itraj] + VelRot[itraj+1]))
		+ r3*( -2*uteta[itraj][3] 
		       + tfi[itraj]*(VelRot[itraj] + VelRot[itraj+1]));
	    doubleMatrix Auo(&uteta[itraj][0], teta);
	    Ai.resize( traj[itraj].a() );
	    At.resize(Ai%Auo.t()); 
	}
    }
}
break;

default: Rt=0.0;  
}  /* end of switch(methode) */

HMatrix Result(At.x(), Pt.x());
cout << Result << endl;


/* ecrire la matrice resultat sur le port de sortie */



if ( time>=tfi[itraj] ) {
    if ( itraj>=(nbPoint-2) ) {  
	/* end of trajectory */
        if (endtraj==NO_EVENT) endtraj=SET_EVENT;
    }
    else {   /* end of trajetory part */
	itraj++;
	time = 0.0;
    }	
}
time += dt;
 /* end of compute.c */
	 
     

    
    
