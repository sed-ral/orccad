// Orccad Version: 3.0 alpha
// Module : TG
// Variables declaration File
// Date of creation : Mon May 26 13:38:53 1997


int i, j, itraj;
double tfi[nbPointMax-1];
double taux[nbPointMax-1];
double tfij[nbPointMax-1][nbAxeMax];



// parametres modifies lors de la generation du module
int methode;         // methode d interpolation
int nbPoint;         // nombre de point intermediaire
double duration;     // duree d un segment de trajectoire


double uteta[nbPointMax-1][4];


double Kv[2];
double Ka[2];    

// variable interne
int   badtraj;
double r1,r2,r3,r4,r5,a1,a2,a3,a4,a5,val,teta;
double dt;      
double time;
double TplusTaux, TmoinsTaux;
double Rt, Rt1, Rt2;    // fonction d interpolation 

double Lopt[nbPointMax-1][4];
double Uopt[nbPointMax-1][4];
double Topt[nbPointMax-1];
double VelRot[nbPointMax-1];

HMatrix traj[nbPointMax];
HMatrix Result();

doubleMatrix D[nbPointMax-1];
doubleMatrix VelTrans[nbPointMax-1];
doubleMatrix Pt;
doubleMatrix At;
doubleMatrix Ai;
