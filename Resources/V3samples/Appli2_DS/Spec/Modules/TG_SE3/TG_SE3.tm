// Ilv Version: 2.4
// File generated: Thu Sep 18 15:44:49 1997
// Creator class: IlvGraphOutput
Palettes 7
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 0 49087 65535 45232 50372 57054 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 6 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 65535 0 0 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
3 0 49087 65535 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
1 0 49087 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 13
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 100 60 200 250 
6 IlvMessageLabel 100 60 201 251 0 "TG_SE3" 16 [Box
0 1 1 TG_SE3 
Box]

 } 0
2 0 { 1 0 OrcIlvPortPrm 2
1 IlvPolyline 11
120 50 136 66 128 58 136 50 120 66 128 58 136 58 120 58 128 58 128 50
128 66 
2 IlvMessageLabel 106 63 88 28 0 "NbPoint" 1 0
[Port
0 1 1 NbPoint 128 58 106 63 88 28 2  2 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortPrm 2
1 IlvPolyline 11
262 52 278 68 270 60 278 52 262 68 270 60 278 60 262 60 270 60 270 52
270 68 
2 IlvMessageLabel 258 65 50 28 0 "Ptraj" 1 0
[Port
0 1 2 Ptraj 270 60 258 65 50 28 2  2 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 260 20 20 
2 IlvMessageLabel 112 256 46 28 0 "posi" 1 0
[Port
0 1 3 posi 100 270 112 256 46 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
3 IlvFilledEllipse 290 120 20 20 
2 IlvMessageLabel 236 116 104 28 0 "trajectory" 1 0
[Port
0 1 4 trajectory 300 130 236 116 104 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 130 300 20 20 2 
2 IlvMessageLabel 121 281 76 28 0 "endtraj" 1 0
[Port
0 1 5 endtraj 140 310 121 281 76 28 8  8 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 250 300 20 20 2 
2 IlvMessageLabel 241 281 76 28 0 "badtraj" 1 0
[Port
0 1 6 badtraj 260 310 241 281 76 28 8  8 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
