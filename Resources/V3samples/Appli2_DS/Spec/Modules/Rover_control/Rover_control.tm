// Ilv Version: 2.4
// File generated: Fri Dec  4 12:26:44 1998
// Creator class: IlvGraphOutput
Palettes 6
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 120 100 170 150 
5 IlvMessageLabel 120 100 171 151 0 "Rover_control" 16 [Box
0 1 1 Rover_control 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 110 170 20 20 
2 IlvMessageLabel 132 166 76 28 0 "control" 1 0
[Port
0 1 1 control 120 180 132 166 76 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 280 160 20 20 
2 IlvMessageLabel 246 156 64 28 0 "status" 1 0
[Port
0 1 2 status 290 170 246 156 64 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
