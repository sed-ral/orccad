// Ilv Version: 2.4
// File generated: Tue Jan 13 11:23:32 1998
// Creator class: IlvGraphOutput
Palettes 6
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "red" "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 150 90 190 170 
5 IlvMessageLabel 150 90 191 171 0 "toto" 16 [Box
0 1 1 toto 
Box]

 } 0
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 330 200 20 20 2 
2 IlvMessageLabel 298 196 60 28 0 "sortie" 1 0
[Port
0 1 1 sortie 340 210 298 196 60 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 140 160 20 20 
2 IlvMessageLabel 162 156 98 28 0 "Port_Var" 1 0
[Port
0 1 2 Port_Var 150 170 162 156 98 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
