// Ilv Version: 3.1
// File generated: Mon Feb 28 09:59:23 2000
// Creator class: IlvGraphOutput
Palettes 6
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 120 100 160 140 
5 IlvMessageLabel 120 100 161 141 F8 0 1 16 4 "Init"   [Box
0 1 1 Init 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 110 160 20 20 
2 IlvMessageLabel 132 156 74 28 F8 0 25 16 4 "Output"   0
[Port
0 1 1 Output 120 170 132 156 74 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortPrm 2
3 IlvPolyline 11
272 162 288 178 280 170 288 162 272 178 280 170 288 170 272 170 280 170 280 162
280 178 
2 IlvMessageLabel 224 156 88 28 F8 0 25 16 4 "Init_Prm"   0
[Port
0 1 2 Init_Prm 280 170 224 156 88 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
EOF
