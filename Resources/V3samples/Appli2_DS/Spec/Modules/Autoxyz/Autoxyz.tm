// Ilv Version: 2.4
// File generated: Tue Dec  9 16:34:47 1997
// Creator class: IlvGraphOutput
Palettes 6
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 130 110 180 170 
5 IlvMessageLabel 130 110 181 171 0 "Autoxyz" 16 [Box
0 1 2 Autoxyz 
Box]

 } 0
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 120 230 20 20 
2 IlvMessageLabel 142 226 56 28 0 "state" 1 0
[Port
0 1 2 state 130 240 142 226 56 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 300 210 20 20 
2 IlvMessageLabel 265 206 66 28 0 "screw" 1 0
[Port
0 1 3 screw 310 220 265 206 66 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortPrm 2
1 IlvPolyline 11
122 142 138 158 130 150 138 142 122 158 130 150 138 150 122 150 130 150 130 142
130 158 
2 IlvMessageLabel 142 136 60 28 0 "gains" 1 0
[Port
0 1 4 gains 130 150 142 136 60 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
2 0 { 7 0 OrcIlvPortPrm 2
1 IlvPolyline 11
122 172 138 188 130 180 138 172 122 188 130 180 138 180 122 180 130 180 130 172
130 188 
2 IlvMessageLabel 142 166 102 28 0 "consigne" 1 0
[Port
0 1 5 consigne 130 180 142 166 102 28 1  1 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 26
