// Orccad Version: 3.0 alpha   
// Module : Autoxyz
// Include and Definition File
// Date of creation : Tue Dec  9 16:15:07 1997


#ifndef _MAXABS_
#define _MAXABS_

#define MAX(x,y) (((x) < (y)) ? (y) : (x))
#define ABS(x) (((x) < 0) ? -(x) : (x))

#endif
