// Orccad Version: 3.0 alpha   
// Module : Autoxyz
// Computation File 
// Date of creation : Tue Dec  9 16:15:07 1997


kpx = gains[0];
kdx = gains[1];
kix = gains[2];
kpy = gains[3];
kdy = gains[4];
kiy = gains[5];
kpz = gains[6];
kdz = gains[7];
kiz = gains[8];
 
// autoxyz()


    //erreur en position
    dx = yc[6] - y[6];
    dy = yc[7] - y[7];
    dz = yc[8] - y[8];

    cphi = cos(y[9]);
    sphi = sin(y[9]);
    ctheta = cos(y[10]);
    stheta = sin(y[10]);
    cpsi = cos(y[11]);
    spsi = sin(y[11]);
    

    dxr = dx*ctheta*cpsi + dy*ctheta*spsi - dz*stheta;
    dyr = dx*(sphi*stheta*cpsi-cphi*spsi) + dy*(ctheta*cpsi+sphi*spsi*stheta)
	  + dz*sphi*ctheta;
    dzr = dx*(sphi*spsi+cphi*stheta*cpsi) + dy*(cphi*stheta*spsi-cpsi*sphi)
	  + dz*cphi*ctheta;

//  if((ABS(u[4])<Pmax)&&(ABS(u[5])<Pmax)&&(ABS(u[6])<Pmax)&&(ABS(u[7])<Pmax))
//  zint += dz*DT_PID*10.0;
    zint = 0.0;

    fx = kpx*dxr - kdx*y[0];
    fy = kpy*dyr - kdy*y[1];
    fz = kpz*dzr - kdz*y[2] + kiz*zint;
  
    
   screw[0] = fx;
   screw[1] = fy;
   screw[2] = fz;

   screw[3] =  screw[4] =  screw[5] = 0.0;
