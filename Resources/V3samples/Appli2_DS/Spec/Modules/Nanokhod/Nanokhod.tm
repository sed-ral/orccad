// Ilv Version: 2.4
// File generated: Fri Dec  4 11:54:15 1998
// Creator class: IlvGraphOutput
Palettes 6
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "wheat" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModPr 2
4 IlvFilledRectangle 120 90 180 170 
5 IlvMessageLabel 120 90 181 171 0 "Nanokhod" 16 [Box
0 1 1 Nanokhod 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 110 130 20 20 2 
2 IlvMessageLabel 132 126 80 28 0 "Control" 1 0
[Port
0 1 1 Control 120 140 132 126 80 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 110 210 20 20 2 
2 IlvMessageLabel 132 206 48 28 0 "LED" 1 0
[Port
0 1 2 LED 120 220 132 206 48 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 290 130 20 20 2 
2 IlvMessageLabel 312 126 52 28 0 "Data" 1 0
[Port
0 1 3 Data 300 140 312 126 52 28 1  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
