// Ilv Version: 2.4
// File generated: Fri Dec  4 13:13:01 1998
// Creator class: IlvGraphOutput
Palettes 6
3 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
4 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 130 110 150 130 
5 IlvMessageLabel 130 110 151 131 0 "Locate" 16 [Box
0 1 1 Locate 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 120 170 20 20 
2 IlvMessageLabel 142 166 102 28 0 "LED_pos" 1 0
[Port
0 1 1 LED_pos 130 180 142 166 102 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 270 170 20 20 
2 IlvMessageLabel 208 166 120 28 0 "Rover_pos" 1 0
[Port
0 1 2 Rover_pos 280 180 208 166 120 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
