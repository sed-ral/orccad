// Ilv Version: 2.4
// File generated: Tue Nov 16 17:02:14 1999
// Creator class: IlvGraphOutput
Palettes 7
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 6 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
1 0 0 65535 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
5 0 49087 65535 16448 57568 53456 "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 19
3 0 { 0 0 OrcIlvModAtr 2
5 IlvFilledRectangle 90 60 210 240 
6 IlvMessageLabel 90 60 211 241 0 "trAtr" 16 [Box
0 1 1 trAtr 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 80 200 20 20 2 
2 IlvMessageLabel 102 196 176 28 0 "EmergencyStop" 1 0
[Port
0 1 2 EmergencyStop 90 210 102 196 176 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 220 290 20 20 2 
2 IlvMessageLabel 208 271 88 28 0 "DirOver" 1 0
[Port
0 1 6 DirOver 230 300 208 271 88 28 8  8 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 140 290 20 20 2 
2 IlvMessageLabel 127 271 94 28 0 "PbMotor" 1 0
[Port
0 1 7 PbMotor 150 300 127 271 94 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 80 140 20 20 2 
2 IlvMessageLabel 102 136 148 28 0 "ManualMode" 1 0
[Port
0 1 8 ManualMode 90 150 102 136 148 28 1  1 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 130 50 20 20 2 
2 IlvMessageLabel 116 65 98 28 0 "BadInput" 1 0
[Port
0 1 10 BadInput 140 60 116 65 98 28 2  2 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 230 50 20 20 2 
2 IlvMessageLabel 206 65 136 28 0 "LooseComm" 1 0
[Port
0 1 11 LooseComm 240 60 206 65 136 28 2  2 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 290 120 20 20 2 
2 IlvMessageLabel 229 116 118 28 0 "LooseCam" 1 0
[Port
0 1 15 LooseCam 300 130 229 116 118 28 4  4 
Port]

 } 20
1 1 { 14 0 OrcIlvVoidLink 1 0 13 } 30
2 0 { 15 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 80 240 20 20 2 
2 IlvMessageLabel 102 236 84 28 0 "EmStop" 1 0
[Port
0 1 16 EmStop 90 250 102 236 84 28 1  1 
Port]

 } 20
1 1 { 16 0 OrcIlvVoidLink 1 0 15 } 30
2 0 { 17 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 80 100 20 20 2 
4 IlvMessageLabel 102 96 138 28 0 "LooseTarget" 1 0
[Port
0 1 17 LooseTarget 90 110 102 96 138 28 1  1 
Port]

 } 20
1 1 { 18 0 OrcIlvVoidLink 1 0 17 } 26
