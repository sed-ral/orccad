// Ilv Version: 3.1
// File generated: Mon Apr 23 12:15:03 2001
// Creator class: IlvGraphOutput
Palettes 6
"IlvStText" 5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 150 90 160 140 
5 IlvMessageLabel 150 90 161 141 F8 0 1 16 4 "Init"   [Box
0 1 1 Init 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 300 190 20 20 
2 IlvMessageLabel 281 186 38 28 F8 0 25 16 4 "Vel"   0
[Port
0 1 2 Vel 310 200 281 186 38 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 230 220 20 20 
2 IlvMessageLabel 201 206 156 28 F8 0 25 16 4 "LinearCamera"   0
[Port
0 1 3 LinearCamera 240 230 201 206 156 28 8  8 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
3 IlvFilledEllipse 140 120 20 20 
2 IlvMessageLabel 160 116 122 28 F8 0 25 16 4 "Vel_Wheel"   0
[Port
0 1 6 Vel_Wheel 150 130 160 116 122 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
1 IlvFilledEllipse 300 110 20 20 
2 IlvMessageLabel 264 106 72 28 F8 0 25 16 4 "Speed"   0
[Port
0 1 7 Speed 310 120 264 106 72 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 26
EOF
