// Orccad Version: 3.1    
// Module : Init
// Computation File 
// 
// Module Ports :
// 	output DOUBLE VelO[4]
// 	output DOUBLE SpeedO[1]
//      input DOUBLE LinearCameraI[5]
// 	input DOUBLE Vel_WheelI[4]
// 
// 
// Date of creation : Mon Apr 23 11:27:11 2001


bool TargetDetected = (LinearCameraI[0]==0.0); // true if the target is visible
double alpha      = LinearCameraI[1];
double beta       = LinearCameraI[2];
double dist       = LinearCameraI[3];
double dev        = LinearCameraI[4];


double Wheel_Vel;
double distance   = sqrt(dist*dist + dev*dev);


Wheel_Vel = (Vel_WheelI[0]+Vel_WheelI[1]+Vel_WheelI[2]+Vel_WheelI[3])/4.0; 



if (!TargetDetected) 
{
    cout << "TestRules: Pas de target"  << endl << endl;
}
else
{
  if (( distance - MinDistance < 0 ) && ( CondInit1 != true ))
    {
      SpeedO = 0.0;
      VelO[0] = VelO[1] = VelO[2] = VelO[3] = 0.0;
    }
  else 
    if (( distance - MinDistance > 0 ) && ( CondInit1 != true ))
    {
      if ( Wheel_Vel - MinWheel_Vel < 0 )
	{
	  SpeedO = Wheel_Vel + 5;
	  VelO[0] = VelO[1] = VelO[2] = VelO[3] = 0.0;
	   CondInit1 = true;
	   cout << " CondInit1 = true" <<endl;
        }
      else
	if ( Wheel_Vel - MinWheel_Vel > 0 )
	{
	  SpeedO = MinWheel_Vel;
	  VelO[0] = VelO[1] = VelO[2] = VelO[3] = Wheel_Vel;
	  CondInit1 = true;
        }

    }
  else
    if  (( distance - MinDistance > 0 ) && ( CondInit1 == true ))
    {
      VelO[0] = VelO[1] = VelO[2] = VelO[3] = Wheel_Vel;
      SpeedO = 0.0;
    }
 
}
