// Ilv Version: 3.1
// File generated: Mon Jul 16 14:32:52 2001
// Creator class: IlvGraphOutput
Palettes 8
3 "Gray" "blue" "default" "fixed" 0 solid solid 0 0 0
1 "Gray" 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
6 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
0 "Gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
7 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 2 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 "Gray" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 11
3 0 { 0 0 OrcIlvModAlgo 2
0 IlvFilledRectangle 100 70 200 210 
1 IlvMessageLabel 100 70 201 211 F8 0 1 16 4 "AccelRule2"   [Box
0 1 1 AccelRule2 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
3 IlvFilledEllipse 90 120 20 20 
4 IlvMessageLabel 110 116 160 28 F8 0 25 16 4 "Speed_Follow"   0
[Port
0 1 6 Speed_Follow 100 130 110 116 160 28 1  1 
Port]

 } 20
1 1 { 2 2 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 90 170 20 20 
4 IlvMessageLabel 110 166 150 28 F8 0 25 16 4 "Diff_Distance"   0
[Port
0 1 7 Diff_Distance 100 180 110 166 150 28 1  1 
Port]

 } 20
1 1 { 4 2 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
3 IlvFilledEllipse 90 220 20 20 
4 IlvMessageLabel 110 216 122 28 F8 0 25 16 4 "Diff_Speed"   0
[Port
0 1 8 Diff_Speed 100 230 110 216 122 28 1  1 
Port]

 } 20
1 1 { 6 2 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
5 IlvFilledEllipse 290 170 20 20 
4 IlvMessageLabel 220 166 140 28 F8 0 25 16 4 "AccelFollow"   0
[Port
0 1 10 AccelFollow 300 180 220 166 140 28 4  4 
Port]

 } 20
1 1 { 8 2 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortVar 2
6 IlvFilledEllipse 190 270 20 20 
7 IlvMessageLabel 169 256 124 28 F8 0 25 16 4 "RatioValue"   0
[Port
0 1 11 RatioValue 200 280 169 256 124 28 8  8 
Port]

 } 20
1 1 { 10 2 OrcIlvVoidLink 1 0 9 } 30
EOF
