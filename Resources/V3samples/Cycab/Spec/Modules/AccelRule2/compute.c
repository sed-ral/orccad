// Orccad Version: 3.1   
// Module : TestRules
// Computation File 
//
// Module Ports :
//      input DOUBLE Speed_FollowI
// 	input DOUBLE Diff_DistanceI
//      input DOUBLE Diff_SpeedI
// 	output DOUBLE AccelFollowO
//      output DOUBLE RatioValueO
//
// Date of creation : Wed Apr 28 11:09:13 1999


double TempAcceleration;
double r;
double DiffDistance;
double SpeedFollow;
double DiffSpeed;

DiffDistance = Diff_DistanceI;        
SpeedFollow = Speed_FollowI;
DiffSpeed = Diff_SpeedI;


if (SpeedFollow == 0.0 && DiffDistance == 0.0 && DiffSpeed == 0.0)

  AccelFollowO = 0.0;

else 
{
  r = (( Diff_DistanceI ) / ( H * Speed_FollowI ));
  if ( r < 0.1 || r > 8)
   {
     r = 0.9;
     TempAcceleration = ( DiffSpeed / H) + Lambda * r - Lambda; 
     AccelFollowO = TempAcceleration;
   }
 else
   {
     r = (( Diff_DistanceI ) / ( H * Speed_FollowI ));
     TempAcceleration = ( Diff_SpeedI / H ) + Lambda * r - Lambda; 
     AccelFollowO = TempAcceleration;
   }
}
RatioValueO = r;
cout << "r: " << r <<endl;
cout << "acceleration: "<< TempAcceleration << endl;
cout << "DiffSpeed: " << DiffSpeed << endl;
cout << "SpeedFollow: " << Speed_FollowI << endl;
cout << "DiffDistance: "<< Diff_DistanceI << endl;
