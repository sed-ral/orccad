// Ilv Version: 2.4
// File generated: Mon Apr 12 14:13:43 1999
// Creator class: IlvGraphOutput
Palettes 7
5 0 49087 65535 19660 54021 65535 "default" "fixed" 0 solid solid 0 0 0
3 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
6 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 0 0 65535 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 13
3 0 { 0 0 OrcIlvModAtr 2
5 IlvFilledRectangle 130 90 150 150 
6 IlvMessageLabel 130 90 151 151 0 "jdAtr" 16 [Box
0 1 1 jdAtr 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 120 110 20 20 2 
2 IlvMessageLabel 142 106 178 28 0 "EmergencyStop" 1 0
[Port
0 1 1 EmergencyStop 130 120 142 106 178 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 120 180 20 20 2 
2 IlvMessageLabel 142 176 96 28 0 "PbMotor" 1 0
[Port
0 1 2 PbMotor 130 190 142 176 96 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 150 230 20 20 2 
2 IlvMessageLabel 138 211 88 28 0 "DirOver" 1 0
[Port
0 1 3 DirOver 160 240 138 211 88 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 270 130 20 20 2 
2 IlvMessageLabel 218 126 100 28 0 "BadInput" 1 0
[Port
0 1 4 BadInput 280 140 218 126 100 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 270 200 20 20 2 
2 IlvMessageLabel 200 196 136 28 0 "LooseComm" 1 0
[Port
0 1 5 LooseComm 280 210 200 196 136 28 4  4 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 240 80 20 20 2 
4 IlvMessageLabel 225 95 100 28 0 "Badinput" 1 0
[Port
0 1 6 Badinput 250 90 225 95 100 28 2  2 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 26
