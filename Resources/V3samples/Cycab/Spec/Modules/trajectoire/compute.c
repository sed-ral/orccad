// Orccad Version: 3.1    
// Module : trajectoire
// Computation File 
// 
// Module Ports :
// 	output EVENT FinTrajE
// 	output DOUBLE WdO[2]
// 
// 
// Date of creation : Mon Mar  5 13:16:44 2001

// Module Algorithmique Trajectoire
       //
       // Fichier compute.c

       WdO[0] = 10.0 sin(t);
       WdO[1] =  6.0 sin(t+1);

       t+=0.01;

       if (t>=10.0)
            FinTrajE=SET_EVENT;
