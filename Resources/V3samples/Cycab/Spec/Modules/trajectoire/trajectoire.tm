// Ilv Version: 3.1
// File generated: Mon Mar  5 13:16:44 2001
// Creator class: IlvGraphOutput
Palettes 6
"IlvStText" 5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "red" "red" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 100 140 100 90 
5 IlvMessageLabel 100 140 101 91 F8 0 1 16 4 "trajectoire"   [Box
0 1 1 trajectoire 
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 140 220 20 20 2 
2 IlvMessageLabel 131 206 78 28 F8 0 25 16 4 "FinTraj"   0
[Port
0 1 1 FinTraj 150 230 131 206 78 28 8  8 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 190 150 20 20 
2 IlvMessageLabel 172 146 36 28 F8 0 25 16 4 "Wd"   0
[Port
0 1 2 Wd 200 160 172 146 36 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
EOF
