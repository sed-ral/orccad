// Orccad Version: 3.1   
// Module : TestRules
// Include and Definition File
// Date of creation : Wed Apr 28 11:09:13 1999

#include<stdio.h>


#ifndef Lambda
#define Lambda   15  // Pour satisfaire aux conditions initiales 
#endif

#ifndef H
#define H  1.2
#endif


#ifndef Alpha
#define Alpha   0.3
#endif

#ifndef Te
#define Te   0.1
#endif
