// Orccad Version: 3.0 alpha
// Module :  getVitRadSec.c
// Call Driver File for variable: 
// 	 output DOUBLE VitRadSecO
// Date of creation : Wed Jul 21 10:57:50 1999

if (cycabGyroRead(&VitRadSecO) == ERROR) {
  fprintf(stderr, "cycabGyroRead return ERROR\n");
  VitRadSecO = 0.0;
}

