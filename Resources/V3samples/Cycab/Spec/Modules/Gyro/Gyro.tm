// Ilv Version: 2.4
// File generated: Wed Jul 21 10:57:50 1999
// Creator class: IlvGraphOutput
Palettes 5
3 "gray" "wheat" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 3
3 0 { 0 0 OrcIlvModPr 2
3 IlvFilledRectangle 100 100 170 100 
4 IlvMessageLabel 100 100 171 101 0 "Gyro" 16 [Box
0 1 1 Gyro 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 170 190 20 20 2 
2 IlvMessageLabel 192 186 120 28 0 "VitRadSec" 1 0
[Port
0 1 1 VitRadSec 180 200 192 186 120 28 1  8 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
