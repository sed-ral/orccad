// Ilv Version: 3.1
// File generated: Wed Aug 23 22:43:00 2006
// Creator class: IlvGraphOutput
Palettes 7
4 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "blue" "blue" "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"IlvStText" 6 61166 61166 59110 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 0 49087 65535 16448 57568 53456 "default" "fixed" 0 solid solid 0 0 0
1 0 0 65535 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
"default" 0 61166 61166 59110 0 0 0 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 19
3 0 { 0 0 OrcIlvModAtr 2
5 IlvFilledRectangle 90 60 210 240 
6 IlvMessageLabel 90 60 211 241 F8 0 1 16 4 "trAtrPost"   [Box
0 1 1 trAtrPost
Box]

 } 16
2 0 { 1 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 80 200 20 20 2 
2 IlvMessageLabel 102 196 176 28 F8 0 25 16 4 "EmergencyStop"   0
[Port
0 1 2 EmergencyStop 90 210 102 196 176 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 220 290 20 20 2 
2 IlvMessageLabel 208 271 88 28 F8 0 25 16 4 "DirOver"   0
[Port
0 1 6 DirOver 230 300 208 271 88 28 8  8 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 140 290 20 20 2 
2 IlvMessageLabel 127 271 94 28 F8 0 25 16 4 "PbMotor"   0
[Port
0 1 7 PbMotor 150 300 127 271 94 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 130 50 20 20 2 
2 IlvMessageLabel 116 65 98 28 F8 0 25 16 4 "BadInput"   0
[Port
0 1 10 BadInput 140 60 116 65 98 28 2  2 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 230 50 20 20 2 
2 IlvMessageLabel 206 65 136 28 F8 0 25 16 4 "LooseComm"   0
[Port
0 1 11 LooseComm 240 60 206 65 136 28 2  2 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 290 120 20 20 2 
2 IlvMessageLabel 229 116 118 28 F8 0 25 16 4 "LooseCam"   0
[Port
0 1 15 LooseCam 300 130 229 116 118 28 4  4 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortEvt 2
1 IlvReliefDiamond 80 240 20 20 2 
2 IlvMessageLabel 102 236 84 28 F8 0 25 16 4 "EmStop"   0
[Port
0 1 16 EmStop 90 250 102 236 84 28 1  1 
Port]

 } 20
1 1 { 14 0 OrcIlvVoidLink 1 0 13 } 30
2 0 { 15 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 80 100 20 20 2 
4 IlvMessageLabel 102 96 138 28 F8 0 25 16 4 "LooseTarget"   0
[Port
0 1 17 LooseTarget 90 110 102 96 138 28 1  1 
Port]

 } 20
1 1 { 16 0 OrcIlvVoidLink 1 0 15 } 30
2 0 { 17 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 290 170 20 20 2 
4 IlvMessageLabel 214 171 152 18 F8 0 25 16 4 "ArriveMinPos"   0
[Port
0 1 18 ArriveMinPos 300 180 214 171 152 18 4  4 
Port]

 } 20
1 1 { 18 0 OrcIlvVoidLink 1 0 17 } 30
EOF
