// Ilv Version: 2.4
// File generated: Mon Apr 12 11:23:59 1999
// Creator class: IlvGraphOutput
Palettes 7
5 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
4 48830 48830 48830 "red" "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 48830 48830 48830 "blue" "default" "fixed" 0 solid solid 0 0 0
6 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
3 "red" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 120 90 170 170 
6 IlvMessageLabel 120 90 171 171 0 "JoystickToVel" 16 [Box
0 1 1 JoystickToVel 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 110 130 20 20 
2 IlvMessageLabel 132 126 94 28 0 "Joystick" 1 0
[Port
0 1 1 Joystick 120 140 132 126 94 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 190 250 20 20 2 
2 IlvMessageLabel 175 231 100 28 0 "BadInput" 1 0
[Port
0 1 8 BadInput 200 260 175 231 100 28 8  8 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
4 IlvFilledEllipse 280 120 20 20 
2 IlvMessageLabel 218 116 120 28 0 "VelWheels" 1 0
[Port
0 1 9 VelWheels 290 130 218 116 120 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
4 IlvFilledEllipse 280 210 20 20 
2 IlvMessageLabel 227 206 102 28 0 "Direction" 1 0
[Port
0 1 10 Direction 290 220 227 206 102 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
