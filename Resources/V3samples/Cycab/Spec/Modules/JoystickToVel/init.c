// Orccad Version: 3.0 alpha
// Module : JoystickToVel
// Initialisation File
//
// Module Ports :
// 	input DOUBLE Port_Var[2]
// 	output EVENT End
// 	output DOUBLE VelWheels[4]
// 	output DOUBLE Direction
//
// Date of creation : Tue Feb 23 08:43:17 1999

#ifdef HM_INDENT
void init()
{
#endif
// Port initialization
BadInputE = NO_EVENT;

DirectionO =  0.0;
VelWheelsO[0] = VelWheelsO[1] = VelWheelsO[2] = VelWheelsO[3] = 0.0;

tra_consigne = 0.0;

#ifdef HM_INDENT
}
#endif
