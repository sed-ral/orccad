// Orccad Version: 3.0 alpha   
// Module : JoystickToVel
// Computation File 
//
// Module Ports :
// 	input DOUBLE Joystick[2]
// 	output EVENT End
// 	output DOUBLE VelWheels[4]
// 	output DOUBLE Direction
//
// Date of creation : Tue Feb 23 08:43:17 1999

#ifdef HM_INDENT
void compute()
{
#endif

  if (((JoystickI[0] >= 300.0) ||
       (JoystickI[0] <= -300.0) ||   
       (JoystickI[1] >= 300.0) ||   
       (JoystickI[1] <= -300.0)) &&  
      (BadInputE == NO_EVENT)) {
    BadInputE = SET_EVENT;
  } else {

    /* Direction Loop. */
    /* 20, passage de +/-300 a +/-2100 centieme de degre */
    DirectionO = 7.0 * JoystickI[0]; /* 20.0 */
    /* passage de 1/100 de degre a radian */
    DirectionO = M_PI * DirectionO / 18000.0;

    /* passage du joystick (-300/+300) a une vitesse en radian par
       seconde (-4.5PI/+4.5PI par seconde) */
    if (fabs(JoystickI[1]) < 3.0)
      tra_consigne = 0.0;
    else
      tra_consigne = (1.5 * M_PI * JoystickI[1] / 100.0); /* 4.0 */

    /* les accelerations sont controlees par le module qui sature
       l'acceleration */

    /* on ne gere pas le fait que dans les virages, chaque roues devrait
       avoir une vitesse differente */
    VelWheelsO[0]=VelWheelsO[1]=VelWheelsO[2]=VelWheelsO[3] = tra_consigne;
  }
#ifdef HM_INDENT
}
#endif
