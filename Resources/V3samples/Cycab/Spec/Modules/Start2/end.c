// Orccad Version: 3.1
// Module :  TestRules
// End File 
//
// Module Ports :
// 	input DOUBLE Pos_Dir
// 	input DOUBLE Vel_Wheel[4]
// 	input DOUBLE Pos_Wheel[4]
// 	param DOUBLE DesirePosition[4]
// 	output DOUBLE Velocity[4]
// 	output DOUBLE Direction
// 	input DOUBLE LinearCamera[5]
// 	input DOUBLE Joystick[2]
//
// Date of creation : Wed Apr 28 11:09:13 1999

//cible = false;
