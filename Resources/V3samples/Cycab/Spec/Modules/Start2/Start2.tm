// Ilv Version: 3.1
// File generated: Fri Jul 13 15:20:03 2001
// Creator class: IlvGraphOutput
Palettes 10
7 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 "red" "default" "fixed" 0 solid solid 0 0 0
3 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
1 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 2 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
0 "Gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
6 48830 48830 48830 "blue" "default" "fixed" 0 solid solid 0 0 0
9 "red" "red" "default" "fixed" 0 solid solid 0 0 0
8 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 23
3 0 { 0 0 OrcIlvModAlgo 2
0 IlvFilledRectangle 100 70 200 210 
1 IlvMessageLabel 100 70 201 211 F8 0 1 16 4 "Start2"   [Box
0 1 1 Start2 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
3 IlvFilledEllipse 190 60 20 20 
4 IlvMessageLabel 177 75 94 28 F8 0 25 16 4 "Joystick"   0
[Port
0 1 1 Joystick 200 70 177 75 94 28 2  2 
Port]

 } 20
1 1 { 2 2 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 230 270 20 20 
4 IlvMessageLabel 201 251 156 28 F8 0 25 16 4 "LinearCamera"   0
[Port
0 1 2 LinearCamera 240 280 201 251 156 28 8  8 
Port]

 } 20
1 1 { 4 2 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
5 IlvFilledEllipse 290 130 20 20 
4 IlvMessageLabel 239 131 102 18 F8 0 25 16 4 "Direction"   0
[Port
0 1 3 Direction 300 140 239 131 102 18 4  4 
Port]

 } 20
1 1 { 6 2 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
5 IlvFilledEllipse 290 90 20 20 
4 IlvMessageLabel 243 88 94 24 F8 0 25 16 4 "Velocity"   0
[Port
0 1 4 Velocity 300 100 243 88 94 24 4  4 
Port]

 } 20
1 1 { 8 2 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortPrm 2
3 IlvPolyline 11
162 272 178 288 170 280 178 272 162 288 170 280 178 280 162 280 170 280 170 272
170 288 
4 IlvMessageLabel 130 251 162 28 F8 0 25 16 4 "DesirePosition"   0
[Port
0 1 5 DesirePosition 170 280 130 251 162 28 8  8 
Port]

 } 20
1 1 { 10 2 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortVar 2
6 IlvFilledEllipse 90 120 20 20 
4 IlvMessageLabel 110 119 126 22 F8 0 25 16 4 "Pos_Wheel"   0
[Port
0 1 6 Pos_Wheel 100 130 110 119 126 22 1  1 
Port]

 } 20
1 1 { 12 2 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortVar 2
6 IlvFilledEllipse 90 170 20 20 
4 IlvMessageLabel 110 169 122 22 F8 0 25 16 4 "Vel_Wheel"   0
[Port
0 1 7 Vel_Wheel 100 180 110 169 122 22 1  1 
Port]

 } 20
1 1 { 14 2 OrcIlvVoidLink 1 0 13 } 30
2 0 { 15 0 OrcIlvPortVar 2
6 IlvFilledEllipse 90 220 20 20 
4 IlvMessageLabel 110 219 88 22 F8 0 25 16 4 "Pos_Dir"   0
[Port
0 1 8 Pos_Dir 100 230 110 219 88 22 1  1 
Port]

 } 20
1 1 { 16 2 OrcIlvVoidLink 1 0 15 } 30
2 0 { 17 0 OrcIlvPortVar 2
7 IlvFilledEllipse 290 180 20 20 
8 IlvMessageLabel 264 181 52 18 F8 0 25 16 4 "Time"   0
[Port
0 1 18 Time 300 190 264 181 52 18 4  4 
Port]

 } 20
1 1 { 18 2 OrcIlvVoidLink 1 0 17 } 30
2 0 { 19 0 OrcIlvPortVar 2
7 IlvFilledEllipse 290 220 20 20 
8 IlvMessageLabel 240 221 100 18 F8 0 25 16 4 "Distance"   0
[Port
0 1 19 Distance 300 230 240 221 100 18 4  4 
Port]

 } 20
1 1 { 20 2 OrcIlvVoidLink 1 0 19 } 30
2 0 { 21 0 OrcIlvPortEvt 2
9 IlvReliefDiamond 110 270 20 20 2 
8 IlvMessageLabel 82 261 152 18 F8 0 25 16 4 "ArriveMinPos"   0
[Port
0 1 20 ArriveMinPos 120 280 82 261 152 18 8  8 
Port]

 } 20
1 1 { 22 2 OrcIlvVoidLink 1 0 21 } 26
EOF
