// Orccad Version: 3.1
// Module :  getjoystick.c
// Call Driver File for variable joystick
// Date of creation : Mon Apr  6 14:27:55 1998

#ifdef HM_INDENT
void getjoystick()
{
#endif

#ifdef SIMPARC_RUN
  vxw->getjoystick(joystick);
#else /* SIMPARC_RUN */

#ifdef ORCSOLARIS
  robctl(CYCAB, GET_SENSOR_1, (void *)joystick);
#endif /* ORCSOLARIS */
#ifdef ORCVXWORKS
  if (cycabGetJoystick(&joy_dir_l, &joy_tra_l) == ERROR)
    loose_comm = SET_EVENT;
  else {
    joystick[0] = (double)joy_dir_l;
    joystick[1] = (double)joy_tra_l;
  }
#endif /* ORCVXWORKS */
#endif /* SIMPARC_RUN */
#ifdef HM_INDENT
}
#endif

