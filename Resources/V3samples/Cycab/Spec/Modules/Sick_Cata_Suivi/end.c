// Orccad Version: 3.0 alpha
// Module :  Sick
// End File 
// 
// Driver Ports : 
// 	output EVENT LooseCommE
// 	output INTEGER DataO[100]
//
// Date of creation : Mon Mar  6 13:33:55 2000

#ifdef HM_INDENT
void end()
{
#endif

  (void)sickSocketClose();

#ifdef HM_INDENT
}
#endif
