// Orccad Version: 3.0 alpha
// Module :  getData.c
// Call Driver File for variable: 
// 	 output INTEGER DataO
// Date of creation : Mon Mar  6 13:33:55 2000

#ifdef HM_INDENT
void getData()
{
#endif

    (void)sickSocketMsgHandle("810", answer);
    (void)sickReadMeasuredCataAnswer(answer,&sickMeasuredCata);


  DataO[0]=sickMeasuredCata.distance[0];
  DataO[1]=sickMeasuredCata.angle[0];
  DataO[2]=sickMeasuredCata.angleR[0];
  DataO[3]=sickMeasuredCata.Xaxis[0];
  DataO[4]=sickMeasuredCata.Yaxis[0];
  DataO[5]=sickMeasuredCata.localTime;
  DataO[6]=sickMeasuredCata.status;

(void)deleteMeasuredCata(&sickMeasuredCata);

#ifdef HM_INDENT
}
#endif
