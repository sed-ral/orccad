// Orccad Version: 3.1 
// Module : Derivation
// Variables declaration File
// Date of creation : Tue Mar 13 10:42:19 2001

double dt;
double No_target_time;
bool CondInit;
double Vel_old;

int NbValPhi;
double distance0; 
double Velocity;
double OldTempSpeedFollow;


double TabCurv2Phi[(int)(PHI_MAX*2/PAS_PHI)][2]; // tableau pour trouver l'angle de braquage 
                           // en fonction du rayon de courbure
                           // TabCurv2Phi[X][0] --> Curv (en 1/m)
                           // TabCurv2Phi[X][1] --> phi  (en rad)

