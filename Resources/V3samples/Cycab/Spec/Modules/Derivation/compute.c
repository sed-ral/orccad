// Orccad Version: 3.1    
// Module : Derivation
// Computation File 
// 
// Module Ports :
// 	input DOUBLE Pos_DirI
// 	input DOUBLE Vel_WheelI[4]
// 	input DOUBLE Pos_WheelI[4]
// 	param DOUBLE DesirePosition[4]
// 	output DOUBLE SpeedFollowO[1]
//      output DOUBLE DiffDistanceO[1]
//      output DOUBLE DiffSpeedO[1]
// 	output DOUBLE Direction
// 	input DOUBLE LinearCamera[5]
// 
// 
// Date of creation : Tue Mar 13 10:42:19 2001


double Wheel_Vel;
double distance1;
double TempDiffSpeed;
double gap;
double atanTmp;
double TempDir;
double TempSpeedFollow;

bool TargetDetected = (LinearCamera[0]==0.0); // true if the target is visible
double alpha    = LinearCamera[1];
double beta     = LinearCamera[2];
double dist       = LinearCamera[3];
double dev        = LinearCamera[4];
double distance   = sqrt(dist*dist + dev*dev);


 Wheel_Vel = (Vel_WheelI[0]+Vel_WheelI[1]+Vel_WheelI[2]+Vel_WheelI[3])/4.0; 




//printf("\n module derivation \n");
if (!TargetDetected) 
     // No detection of target 
{} 
else 
     // Target detected 
  {
// Controle du suivi lateral du cycab
// mode de suivi (0=normal; 1=avec calcul de phi)
    bool SUIVI_COMPLEXE = 0;

    if (((alpha*beta)>0.0) && (alpha!=0.0) && (SUIVI_COMPLEXE==1)) 
     {
        // calcul du rayon de courbure
	double rayon = dev + dist*tan(M_PI/2.0 - alpha);
	// recherche dichotomique du braquage en fonction du rayon de coubure
	int debut = 1;
	int fin = NbValPhi;
	int milieu;
	while (debut < fin) 
	 {
	    milieu = (debut+fin)/2;
	    if (TabCurv2Phi[milieu][0] > rayon) 
	     {
	       fin = milieu - 1;
	     } 
	    else
	    if (TabCurv2Phi[milieu][0] < rayon) 
	     {
	       debut = milieu + 1;
	     }
	    else
	       fin = debut;
	     }
	     // approximation lineaire de phi en fonction du rayon de courbure
	     TempDir = TabCurv2Phi[debut][1] +
	     ((rayon - TabCurv2Phi[debut][0]) * 
	     (TabCurv2Phi[fin][1] - TabCurv2Phi[debut][1]) / 
	     (TabCurv2Phi[fin][0] - TabCurv2Phi[debut][0]));
  
	        // affichage de debug
	      cout << "ANGLE DE BRAQUAGE PRECALCULE : " << RAD2DEG * TempDir << " degres" << endl;
	  }
	  else  // Suivi elementaire ameliore
	  {
	   // Lateral distance Using P Control
	   atanTmp = dev / (2*dist);
	   if (atanTmp)
	   TempDir = - atan (atanTmp);
	   else
	   TempDir = 0.0;
	                 
		     
	   if (TempDir>= 0.3)   
	      Direction = 0.3;
	   else if (TempDir<= -0.3)  
	      Direction = - 0.3;
	   else  
	      Direction = TempDir;
		  
	   
	    }
             
    if (( distance - MinDistance < 0 )) 
      {
	SpeedFollowO = 0.0;
	DiffDistanceO = 0.0;
	DiffSpeedO = 0.0;
      }
    else
    if (( distance - MinDistance > 0 ))
      {            
        No_target_time = 0.0;

        if (Vel_old>=0) //the Car is Running Forward
          {
            if (( Wheel_Vel ) >= 0 )
	        { 
                   TempSpeedFollow =( R * Wheel_Vel );
	           SpeedFollowO = TempSpeedFollow;		     
	                                                                            
       
                   if (distance0 == 0.0)
	             {
	                cout << "reference...\n";               // Determination of the initial gap between the two vehicules
	                distance0 = distance;
	             }
                   else 
	           if (CondInit == false)                                                                   // if the initial conditions are not verified
                     {
                        gap=distance;
                        DiffDistanceO = ( gap - 1 );                                                         // safety margin = 1M 
                        distance1 = ( distance - 0.05 );
                        TempDiffSpeed = ( distance0 - distance1 );               // calculation of the difference of speed between the two vehicles by                                                                                                         // derivation of the difference of positions of each vehicle.
	                                                                                          // recall formulate   derivation:f`(x) = lim ((f(x+h)-f(x))/h)


                        TempDiffSpeed = ( TempDiffSpeed / Te );
                        if ( TempDiffSpeed <= 2.9 || TempDiffSpeed >= -1.2 )                      // Initial conditions :
	                                                                                          //  Amax < (z`(t0)-x`(t0))/0.6 < Amin
	                 {
			    cout<<"conditions initiales verifiees \n";
		            CondInit = true;
		            distance0 = distance;
	                 }
                        else
	                 {
		            CondInit = false;
		            distance0 = distance;
			    distance1 = (distance - 0.5);
	                 }
                     }
	           else
                     {
                        gap = distance;
                        DiffDistanceO = gap-1; 
                                                                                                  // calculate of difference of speed; DiffSpeed calcule                                                                                                          //a la sortie du module va servir de base a la loi de
                                                                                                  // commande implementee dans le module suiva  
                        distance1 = distance - Te*SpeedFollowO;
			TempDiffSpeed =  distance - distance1 ;
	                TempDiffSpeed = ( TempDiffSpeed / Te );
			DiffSpeedO = TempDiffSpeed;
                        distance0 = distance;
	     	        


			 }
		}
           } 
       }
    }  

OldTempSpeedFollow = TempSpeedFollow;
