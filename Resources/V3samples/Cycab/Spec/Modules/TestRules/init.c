// Orccad Version: 3.1
// Module : TestRules
// Initialisation File
//
// Module Ports :
// 	input DOUBLE Pos_Dir
// 	input DOUBLE Vel_Wheel[4]
// 	input DOUBLE Pos_Wheel[4]
// 	param DOUBLE DesirePosition[4]
// 	output DOUBLE Velocity[4]
// 	output DOUBLE Direction
// 	input DOUBLE LinearCamera[5]
// 	input DOUBLE Joystick[2]
//
// Date of creation : Wed Apr 28 11:09:13 1999

DesirePosition[0] = 2.0;
DesirePosition[1] = 2.0;
DesirePosition[2] = 2.0;
DesirePosition[3] = 2.0;
Vel_old           = 0.0;
Direction         = 0.0;
Velocity[0]       = 0.0;
Velocity[1]       = 0.0;
Velocity[2]       = 0.0;
Velocity[3]       = 0.0;

                              
   NbValPhi = (int)(PHI_MAX*2/PAS_PHI);
No_target_time    = 0.0;
dt = getSampleTime();

// boucle de remplissage du tableau "TabCurv2Phi"
double TempPhi  = -PHI_MAX;
int    i;
for (i=0; i<NbValPhi;i++) {
  TabCurv2Phi[i][1] = TempPhi;
  TabCurv2Phi[i][0] = (LW * cos(TempPhi) * cos(TempPhi*KCYCAB)) / sin(TempPhi + (TempPhi*KCYCAB)); 
  TempPhi += PAS_PHI;
}

// boucle de tri du tableau "TabCurv2Phi" par ordre croissant des "curv"
double Buffer;
int j;
for (i=1; i<(NbValPhi-1);i++) {
 for (j=(i+1); j<NbValPhi;j++) {
    if (TabCurv2Phi[i][0] > TabCurv2Phi[j][0]) {
      Buffer = TabCurv2Phi[i][0];
      TabCurv2Phi[i][0] = TabCurv2Phi[j][0];
      TabCurv2Phi[j][0]	= Buffer;
      Buffer = TabCurv2Phi[i][1];
      TabCurv2Phi[i][1] = TabCurv2Phi[j][1];
      TabCurv2Phi[j][1]	= Buffer;
    }
  }
}

