// Orccad Version: 3.1   
// Module : TestRules
// Computation File 
//
// Module Ports :
//      input DOUBLE Acceleration[4]
// 	input DOUBLE Pos_Dir
// 	input DOUBLE Vel_Wheel[4]
// 	input DOUBLE Pos_Wheel[4]
// 	param DOUBLE DesirePosition[4]
// 	output DOUBLE Velocity[4]
// 	output DOUBLE Direction
// 	input DOUBLE LinearCamera[5]
// 	input DOUBLE Joystick[2]
//
// Date of creation : Wed Apr 28 11:09:13 1999

double TempDir;
double TempVel;
double Wheel_Vel;
double atanTmp;
double r;
double Vx;
double Vz;
double TempAcceleration;
double Acceleration;

bool TargetDetected = (LinearCamera[0]==0.0); // true if the target is visible
double alpha    = LinearCamera[1];
double beta     = LinearCamera[2];
double dist     = LinearCamera[3];
double dev      = LinearCamera[4];
double distance = sqrt(dist*dist + dev*dev);

printf("\n>>>>>> alpha=%f, beta=%f, dist=%f, dev=%f, distance=%f, target=%d\n\n",alpha, beta, dist, dev, distance, (TargetDetected)?1:0);

  if (!TargetDetected) 
    // No detection of target 
    { 
    No_target_time += dt;
    cout << "TestRules: Pas de target depuis " << No_target_time << " sec" << endl << endl;
    } 
  else 
    // Target detected
    {  
    cout << "TestRules: Target detectee " << endl;
    cout << "  --> Distance cible en metres   : " << distance << endl;
    cout << "  --> Direction cible en degres : " << alpha * RAD2DEG;
    cout << endl << endl;

                           
    No_target_time = 0.0;
    
    if (Vel_old>=0) //the Car is Running Forward
     
  

/*
            //------------------------------------------
           // loi de controle longi modifiee version 2 |
	   //-------------------------------------------

 {
      if ( (distance-MinDistance)>=0)
      {
         Wheel_Vel=(Vel_Wheel[0]+Vel_Wheel[1]+Vel_Wheel[2]+Vel_Wheel[3])/4.0;  // rad/s
      
         if ((Wheel_Vel)>=VMAX)
	   Wheel_Vel=VMAX;
	 else
	 if ((Wheel_Vel<=0.0))
	   Wheel_Vel=VMIN;
	 else
	   Wheel_Vel=Wheel_Vel;

           TempVel=Wheel_Vel;                                              
           Vx=TempVel*R;                                           // m/s
           Vz=0;         

           //(z-x)=distance 
         
           r=(distance/(h*Vx)); // with z=position of the leading vehicle     
                                //       x=position of the following vehicle
                                //       Vx=speed of the following vehicle 
                                //       h=constant time headway

         if ((r-1)>0)
         {
            cout<<r<<" le vehicule suiveur est trop loin de la cible"; 
            TempAcceleration=((Vz-Vx)/h)+LANDA*(r-1);
	    Acceleration[0]=Acceleration[1]= Acceleration[2]=Acceleration[3] = TempAcceleration; 
         }    
         else    
         if ((r-1)<1)
         {
            cout<<r<<" Le vehicule suiveur est trop proche de la cible";
            TempAcceleration=((Vz-Vx)/h)+LANDA*(r-1);
	    Acceleration[0]=Acceleration[1]= Acceleration[2]=Acceleration[3] = TempAcceleration;
         }
	   else  
         if (r=1)
	  {
            cout<<r<<" Le vehicule est a la bonne distance";
	    TempAcceleration=((Vz-Vx)/h)+LANDA*(r-1);
	    Acceleration[0]=Acceleration[1]= Acceleration[2]=Acceleration[3] = TempAcceleration;
          }
     }
     else 
       {
       cout<<"le vehicule est trop proche";
       Velocity[0] = Velocity[1] = Velocity[2] = Velocity[3] = 0.0;
       } 
 }
else
 {
  cout<<"La voiture recule";      
  Velocity[0] = Velocity[1] = Velocity[2] = Velocity[3] = 0.0;
 }

Vel_old = Velocity[0];
}





     
-------------------------------------------------------------------------------------------------------

            //------------------------------------------
           // loi de controle longi modifiee version 1 |
	   //-------------------------------------------


 {
      if ( (distance-MinDistance)>=0)
      {
         Wheel_Vel=(Vel_Wheel[0]+Vel_Wheel[1]+Vel_Wheel[2]+Vel_Wheel[3])/4.0;  // rad/s
         
	 if (Wheel_Vel=0.0)
	 {
	   Wheel_Vel=VMAX;
         }
	 else
	 {
           Wheel_Vel=Wheel_Vel;
         }
         

	   TempVel=Wheel_Vel*6.0;                                                // tr/min
           Vx=TempVel*((2*M_PI*R)/30);                                           // m/s
         
           //(z-x)=distance 
         
           r=(distance/(h*Vx)); // with z=position of the leading vehicle     
                              //       x=position of the following vehicle
                              //       Vx=speed of the following vehicle 
                              //       h=constant time headway

         if ((r-1)>0)
         {
            cout<<r<<" le vehicule suiveur est trop loin de la cible"; 
            TempAcceleration=((Vz-Vx)/h)+LANDA*(r-1);
            Acceleration=(0.2*G);
            Vx=-h*(Acceleration-LANDA*(r-1));
            TempVel=((30*Vx)/(2*M_PI*R));
            TempVel=6*TempVel;
            Velocity[0] = Velocity[1] = Velocity[2] = Velocity[3] = TempVel;
         }    
         else    
         if ((r-1)<1)
         {
            cout<<r<<" Le vehicule suiveur est trop proche de la cible";
            TempAcceleration=((Vz-Vx)/h)+LANDA*(r-1);
            Acceleration=(-0.5*G);
            Vx=-h*(Acceleration-LANDA*(r-1));
            TempVel=((30*Vx)/(2*M_PI*R));
            TempVel=6*TempVel;
            Velocity[0] = Velocity[1] = Velocity[2] = Velocity[3] = TempVel;
         }
	   else  
         if (r=1)
	  {
            cout<<r<<" Le vehicule est a la bonne distance";
            Velocity[0] = Velocity[1] = Velocity[2] = Velocity[3] = TempVel;
	  }

     }
     else 
       {
       cout<<"le vehicule est trop proche";
       Velocity[0] = Velocity[1] = Velocity[2] = Velocity[3] = 0.0;
       } 
 }
else
 {
  cout<<"La voiture recule";      
  Velocity[0] = Velocity[1] = Velocity[2] = Velocity[3] = 0.0;
 }

Vel_old = Velocity[0];
}

---------------------------------------------------------------------------------------------------
*/
            //-----------------------------------
           // loi de controle longi a  modifier |
	   //------------------------------------


 {
      if ( (distance-MinDistance)>=0) // the car don't arrive the min position
	                              // (3.0 meters between the car and the target)
   
  
	{
           // Longitual distance Using PD Control
	  Wheel_Vel= (Vel_Wheel[0]+Vel_Wheel[1]+Vel_Wheel[2]+Vel_Wheel[3])/4;
	  TempVel=2*M_PI*(distance-MinDistance) - (VMAX/10)*Wheel_Vel;
	  if (TempVel >= VMAX) 
	    TempVel=VMAX;
	  if (TempVel <= 0.0) 
	    TempVel=0.0;
	  Velocity[0] = Velocity[1] = Velocity[2] = Velocity[3] =TempVel;
       
	  // mode de suivi (0=normal; 1=avec calcul de phi)
	  bool SUIVI_COMPLEXE = 0;

	  if (((alpha*beta)>0.0) && (alpha!=0.0) && (SUIVI_COMPLEXE==1)) {
	    // calcul du rayon de courbure
	    double rayon = dev + dist*tan(M_PI/2.0 - alpha);
	    cout << "rayon : " << rayon << endl;
	    // recherche dichotomique du braquage en fonction du rayon de coubure
	    int debut = 1;
	    int fin = NbValPhi;
	    int milieu;
	    while (debut < fin) 
	      {
		milieu = (debut+fin)/2;
		if (TabCurv2Phi[milieu][0] > rayon) 
		  {
		    fin = milieu - 1;
		  } 
		else
		  if (TabCurv2Phi[milieu][0] < rayon) 
		    {
		      debut = milieu + 1;
		    }
		  else
		    fin = debut;
	      }
	    
	    // approximation lineaire de phi en fonction du rayon de courbure
	    TempDir = TabCurv2Phi[debut][1] +
	              ((rayon - TabCurv2Phi[debut][0]) * 
	               (TabCurv2Phi[fin][1] - TabCurv2Phi[debut][1]) / 
	               (TabCurv2Phi[fin][0] - TabCurv2Phi[debut][0]));
  
	    // affichage de debug
	    cout << "ANGLE DE BRAQUAGE PRECALCULE : " << RAD2DEG * TempDir << " degres" << endl;
	    
	  }
	  else  // Suivi elementaire ameliore
	    {
	      // Lateral distance Using P Control
	      atanTmp = dev / (2*dist);
	      if (atanTmp)
		TempDir = - atan (atanTmp);
	      else
		TempDir = 0.0;
	    }
	  
	  if (TempDir>= 0.3)   
	    Direction = 0.3;
	  else if (TempDir<= -0.3)  
	    Direction = - 0.3;
	  else  
	    Direction = TempDir;
        }
	else//the car arrived the min position
	  {
	   
	  }
	}
    else // the car is running backward
	{
	  Velocity[0] = Velocity[1] = Velocity[2] = Velocity[3] = 0.0;
	}
      }
 
  // Test No_target_time to stop Cycab if superior to TIME_WITHOUT_TARGET seconds
  if (No_target_time > TIME_WITHOUT_TARGET)    
    { 
      Wheel_Vel= (Vel_Wheel[0]+Vel_Wheel[1]+Vel_Wheel[2]+Vel_Wheel[3])/4;
      if ((Wheel_Vel>0.0) && (No_target_time < (TIME_WITHOUT_TARGET+1)))
	{
	  Velocity[0] = Velocity[1] = Velocity[2] = Velocity[3] = Wheel_Vel/1.5;
	}
      else
	{
	  Velocity[0] = Velocity[1] = Velocity[2] = Velocity[3] = 0.0;
	}

    }


 
    
