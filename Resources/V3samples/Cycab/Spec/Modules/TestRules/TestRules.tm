// Ilv Version: 2.4
// File generated: Wed Apr 28 11:09:13 1999
// Creator class: IlvGraphOutput
Palettes 6
3 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 "Gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 17
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 100 70 200 210 
5 IlvMessageLabel 100 70 201 211 0 "TestRules" 16 [Box
0 1 1 TestRules 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 190 60 20 20 
2 IlvMessageLabel 177 75 94 28 0 "Joystick" 1 0
[Port
0 1 1 Joystick 200 70 177 75 94 28 2  2 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
1 IlvFilledEllipse 230 270 20 20 
2 IlvMessageLabel 201 251 156 28 0 "LinearCamera" 1 0
[Port
0 1 2 LinearCamera 240 280 201 251 156 28 8  8 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
3 IlvFilledEllipse 290 220 20 20 
2 IlvMessageLabel 237 216 102 28 0 "Direction" 1 0
[Port
0 1 3 Direction 300 230 227 216 102 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
3 IlvFilledEllipse 290 110 20 20 
2 IlvMessageLabel 241 106 94 28 0 "Velocity" 1 0
[Port
0 1 4 Velocity 300 120 231 106 94 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortPrm 2
1 IlvPolyline 11
162 272 178 288 170 280 178 272 162 288 170 280 178 280 162 280 170 280 170 272
170 288 
2 IlvMessageLabel 130 251 162 28 0 "DesirePosition" 1 0
[Port
0 1 5 DesirePosition 170 280 130 251 162 28 8  8 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 120 20 20 
2 IlvMessageLabel 112 116 126 28 0 "Pos_Wheel" 1 0
[Port
0 1 6 Pos_Wheel 100 130 112 116 126 28 1  1 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 170 20 20 
2 IlvMessageLabel 112 166 122 28 0 "Vel_Wheel" 1 0
[Port
0 1 7 Vel_Wheel 100 180 112 166 122 28 1  1 
Port]

 } 20
1 1 { 14 0 OrcIlvVoidLink 1 0 13 } 30
2 0 { 15 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 220 20 20 
2 IlvMessageLabel 112 216 88 28 0 "Pos_Dir" 1 0
[Port
0 1 8 Pos_Dir 100 230 112 216 88 28 1  1 
Port]

 } 20
1 1 { 16 0 OrcIlvVoidLink 1 0 15 } 30
