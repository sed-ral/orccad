// Orccad Version: 3.1   
// Module : TestRules
// Include and Definition File
// Date of creation : Wed Apr 28 11:09:13 1999

#ifndef M_PI
#define M_PI 3.141592
#endif

#ifndef RAD2DEG
#define RAD2DEG 180/M_PI
#endif

#ifndef VMAX
#define VMAX  15.0 // vitesse angulaire maximale sur chaque roue (en rad/s)
#endif


#ifndef VMIN
#define VMIN  15.0 // vitesse angulaire maximale sur chaque roue (en rad/s)
#endif




#ifndef MinDistance
#define MinDistance  3.0 // distace minimale de suivi (en m)
#endif

#ifndef KCYCAB
#define KCYCAB   0.69         // rapport entre braquage avant et braquage arriere
#endif

#ifndef LW
#define LW   1.20        // empatement du vehicule (en m)
#endif


#ifndef PHI_MAX
#define PHI_MAX   0.4        // braquage maxi (en rad)
#endif

#ifndef PAS_PHI
#define PAS_PHI   0.01       // precision du "calcul" de phi (en rad)
#endif


#ifndef TIME_WITHOUT_TARGET
#define TIME_WITHOUT_TARGET  3.0 // temps maxi sans detection de cible avant arret
#endif 


#ifndef h
#define h   0.6  // constant time headway
#endif

#ifndef R
#define R   0.20  // rayon de roue du cycab, suppose constant (en m)
#endif

#ifndef LANDA
#define LANDA   5  //gain de la boucle de commande (en m/s^2) 
#endif 

#ifndef G
#define G   9.81  // (en m/s^2) 
#endif

