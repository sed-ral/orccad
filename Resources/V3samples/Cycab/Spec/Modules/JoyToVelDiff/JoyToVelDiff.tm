// Ilv Version: 2.4
// File generated: Thu Jul 22 16:37:09 1999
// Creator class: IlvGraphOutput
Palettes 7
4 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 65535 0 0 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
3 48830 48830 48830 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
0 48830 48830 48830 45232 50372 57054 "default" "fixed" 0 solid solid 0 0 0
"default" 2 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
6 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
1 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
0 IlvFilledRectangle 120 90 170 170 
1 IlvMessageLabel 120 90 171 171 0 "JoyToVelDiff" 16 [Box
0 1 1 JoyToVelDiff 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
3 IlvFilledEllipse 110 130 20 20 
4 IlvMessageLabel 132 126 94 28 0 "Joystick" 1 0
[Port
0 1 1 Joystick 120 140 132 126 94 28 1  1 
Port]

 } 20
1 1 { 2 2 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
5 IlvReliefDiamond 190 250 20 20 2 
4 IlvMessageLabel 175 231 100 28 0 "BadInput" 1 0
[Port
0 1 8 BadInput 200 260 175 231 100 28 8  8 
Port]

 } 20
1 1 { 4 2 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
6 IlvFilledEllipse 280 120 20 20 
4 IlvMessageLabel 218 116 120 28 0 "VelWheels" 1 0
[Port
0 1 9 VelWheels 290 130 218 116 120 28 4  4 
Port]

 } 20
1 1 { 6 2 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
6 IlvFilledEllipse 280 210 20 20 
4 IlvMessageLabel 227 206 102 28 0 "Direction" 1 0
[Port
0 1 10 Direction 290 220 227 206 102 28 4  4 
Port]

 } 20
1 1 { 8 2 OrcIlvVoidLink 1 0 7 } 30
