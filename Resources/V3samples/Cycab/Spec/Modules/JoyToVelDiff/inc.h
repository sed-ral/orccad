// Orccad Version: 3.0 alpha   
// Module : JoystickToVel
// Include and Definition File
// Date of creation : Tue Feb 23 08:43:17 1999

#include <math.h>
 
#ifndef M_PI
#define M_PI 3.1415927
#endif
 
#ifndef AV_G
#define AV_G 0
#endif
 
#ifndef AV_D
#define AV_D 1
#endif
 
#ifndef AR_G
#define AR_G 2
#endif
 
#ifndef AR_D
#define AR_D 3
#endif
 
#ifndef CYCAB_STEERING_LOCK_RATIO
#define CYCAB_STEERING_LOCK_RATIO 0.69
#endif
 
#ifndef CYCAB_WHEELBASE
#define CYCAB_WHEELBASE 1.210       /* distance entrre les essieux */
#endif
 
#ifndef CYCAB_WHEEL_AXLE_DIST
#define CYCAB_WHEEL_AXLE_DIST 1.100 /* empattement */
#endif 
 
#ifndef CYCAB_WHEEL_RADIUS
#define CYCAB_WHEEL_RADIUS 0.2      /* rayon nominal des roues */
#endif
 
#ifndef CYCAB_WHEEL_AXLE__WHEEL_JOINT_DIST
#define CYCAB_WHEEL_AXLE__WHEEL_JOINT_DIST 0.171 /* rayon apparent */
#endif
 
/* Quelques macros utiles */
#define SQR(x) (x*x)

