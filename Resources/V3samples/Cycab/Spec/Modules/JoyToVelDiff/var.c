// Orccad Version: 3.0 alpha
// Module : JoystickToVel
// Variables declaration File
// Date of creation : Tue Feb 23 08:43:17 1999

double tra_consigne; /* consigne vitesse joystick : cmde essieu avant */
double pro_consigne; /* vitesse esieu arriere */
double dtheta;       /* rotation instantannee */
double deltaV;       /* difference de vitesse roue virtuelle/roue reelle */
double pro_base;
double pro_off;
double tra_base;
double tra_off;
double rear_dir;
