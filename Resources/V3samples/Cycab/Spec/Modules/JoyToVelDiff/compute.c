// Orccad Version: 3.0 alpha   
// Module : JoystickToVel
// Computation File 
//
// Module Ports :
// 	input DOUBLE Joystick[2]
// 	output EVENT End
// 	output DOUBLE VelWheels[4]
// 	output DOUBLE Direction
//
// Date of creation : Tue Feb 23 08:43:17 1999

#ifdef HM_INDENT
void compute()
{
#endif

  if (((JoystickI[0] > 300.0) ||
       (JoystickI[0] < -300.0) ||   
       (JoystickI[1] > 300.0) ||   
       (JoystickI[1] < -300.0)) &&  
      (BadInputE == NO_EVENT)) {
    BadInputE = SET_EVENT;
  } else {

    /* Direction Loop. */
    /* 20, passage de +/-300 a +/-2100 centieme de degre */
    DirectionO = 7.0 * JoystickI[0]; /* 20.0 */
    /* passage de 1/100 de degre a radian */
    DirectionO = M_PI * DirectionO / 18000.0;

    /* passage du joystick (-300/+300) a une vitesse en radian par
       seconde (-4.5PI/+4.5PI par seconde) */
    if (fabs(JoystickI[1]) < 3.0)
      tra_consigne = 0.0;
    else
      tra_consigne = (1.5 * M_PI * JoystickI[1] / 100.0); /* 4.0 */

    /* les accelerations sont controlees par le module qui sature
       l'acceleration */

    /* consigne de vitesse de rotation pour la roue virtuelle arriere */
    pro_consigne = tra_consigne * (cos(DirectionO) / 
                   cos(CYCAB_STEERING_LOCK_RATIO * DirectionO));
 
    /* vitesse de rotation du cycab d'apres le modele cinematique */
    dtheta = CYCAB_WHEEL_AXLE__WHEEL_JOINT_DIST*tra_consigne * 
             (sin(DirectionO * (1 + CYCAB_STEERING_LOCK_RATIO))
             / (CYCAB_WHEELBASE*cos(CYCAB_STEERING_LOCK_RATIO * DirectionO)));
    
    /* ecart de vitesse de rotation entre roue virtuelle et roue reelle */
    deltaV =(CYCAB_WHEEL_AXLE_DIST*dtheta)/CYCAB_WHEEL_AXLE__WHEEL_JOINT_DIST;
 
    /* Les variables ci-dessous sont des regroupements de termes appairaissant
     * dans les formules ce qui evite de les recalculer */
    rear_dir = CYCAB_STEERING_LOCK_RATIO * DirectionO;
    pro_base = SQR(pro_consigne) + SQR(deltaV)/4;
    pro_off = pro_consigne * deltaV*cos(rear_dir);
    tra_base = SQR(tra_consigne) + SQR(deltaV)/4;
    tra_off = tra_consigne * deltaV*cos(DirectionO);
    
 
    /* vitesse angulaire des roues: le signe depend du signe de la cons. */
    VelWheelsO[AR_G] = (tra_consigne > 0)?
                       sqrt(pro_base + pro_off):
                       -(sqrt(pro_base + pro_off));
    VelWheelsO[AR_D] = (tra_consigne > 0)? 
                       sqrt(pro_base - pro_off):
                       -(sqrt(pro_base - pro_off));
    VelWheelsO[AV_G] = (tra_consigne > 0)? 
                       sqrt(tra_base + tra_off):
                       -(sqrt(tra_base + tra_off));
    VelWheelsO[AV_D] = (tra_consigne > 0)?
                       sqrt(tra_base - tra_off):
                       -(sqrt(tra_base - tra_off));

  }
#ifdef HM_INDENT
}
#endif
