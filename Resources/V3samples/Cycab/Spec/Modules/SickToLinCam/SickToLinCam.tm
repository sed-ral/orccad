// Ilv Version: 3.1
// File generated: Fri Jun 23 18:44:34 2000
// Creator class: IlvGraphOutput
Palettes 7
5 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 6 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 80 110 230 160 
6 IlvMessageLabel 80 110 231 161 F8 0 1 16 4 "SickToLinCam"   [Box
0 1 1 SickToLinCam 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 70 180 20 20 
2 IlvMessageLabel 92 176 96 28 F8 0 25 16 4 "sickData"   0
[Port
0 1 1 sickData 80 190 92 176 96 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 300 180 20 20 
2 IlvMessageLabel 230 176 136 28 F8 0 25 16 4 "LinCamData"   0
[Port
0 1 2 LinCamData 310 190 230 176 136 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 180 260 20 20 2 
2 IlvMessageLabel 161 241 118 28 F8 0 25 16 4 "LooseCam"   0
[Port
0 1 3 LooseCam 190 270 161 241 118 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
EOF
