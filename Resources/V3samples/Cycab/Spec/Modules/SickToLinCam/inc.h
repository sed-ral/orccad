// Orccad Version: 3.0 alpha   
// Module : SickToLinCam
// Include and Definition File
// Date of creation : Thu Jun 22 19:53:41 2000


#ifndef M_PI
#define M_PI 3.141592
#endif

#ifndef DEG2RAD
#define DEG2RAD M_PI/180
#endif

#ifndef RAD2DEG
#define RAD2DEG 180/M_PI
#endif
