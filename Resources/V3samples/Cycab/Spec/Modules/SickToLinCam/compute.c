// Orccad Version: 3.0 alpha   
// Module : SickToLinCam
// Computation File 
//
// Module Ports :
// 	input INTEGER sickDataIn[7]
// 	output DOUBLE LinCamDataO[5]
//
// Date of creation : Thu Jun 22 19:53:41 2000

localTime=sickDataIn[5]; 
status=sickDataIn[6];

if (WatchDog==10){
   //test
   if(localTimeTest==localTime){
     WatchDogstatus=0;
   }
   else {
     WatchDogstatus=1;
   }
   WatchDog=0;
   localTimeTest=localTime;
}
else {
   WatchDog++;
}

status=sickDataIn[6]*WatchDogstatus;
 

LinCamDataO[0] = (double)(status==0);                  // target
LinCamDataO[1] = -(double)(sickDataIn[2])/100*DEG2RAD;  // alpha
LinCamDataO[2] = -(double)(sickDataIn[1])/100*DEG2RAD;  // beta
LinCamDataO[3] = (double)(sickDataIn[4])/1000;         // dist
LinCamDataO[4] = -(double)(sickDataIn[3])/1000;         // dev
