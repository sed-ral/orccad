// Orccad Version: 3.0 alpha
// Module : Cycab
// Initialisation Driver File
// Date of creation : Mon Apr  6 10:59:31 1998

/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void init()
{
#endif
  cout << "Cycab Initialization... " << endl;

//#ifdef ORCVXWORKS
  LooseCommE = NO_EVENT;
  BadInputE = NO_EVENT;

  StatusO = 0;

  PosDirO = 0;

  PosWheelsO[0] = 0.0;
  PosWheelsO[1] = 0.0;
  PosWheelsO[2] = 0.0;
  PosWheelsO[3] = 0.0;

  VelWheelsO[0] = 0.0;
  VelWheelsO[1] = 0.0;
  VelWheelsO[2] = 0.0;
  VelWheelsO[3] = 0.0;

  etat_ava = 0;
  etat_arr = 0;
  etat_dir = 0;
  pos_avg = 0;
  pos_avd = 0;
  pos_arg = 0;
  pos_ard = 0;
  pos_dir = 0;
  joy_dir = 0;
  joy_tra = 0;
  vel_avg = 0;
  vel_avd = 0;
  vel_arg = 0;
  vel_ard = 0;

  /* Init du Cycab (On regarde si les noeuds sont OK) */
  if (canInit() == ERROR) {
    fprintf(stderr, "can not initialized\n");
    LooseCommE = SET_EVENT;
  } else {
    LooseCommE = NO_EVENT;
  }
  if (cycabPutBrake(CYCAB_OFF) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  }
  if (cycabPutMotor(CYCAB_ON) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  }
  if (cycabPutDir(CYCAB_ON) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  }
  if (cycabPutVel(CYCAB_ON) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  }

// #endif  ORCVXWORKS 

#ifdef HM_INDENT
}
#endif
