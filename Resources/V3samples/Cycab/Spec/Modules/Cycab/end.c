// Orccad Version: 3.0 alpha
// Module :  Cycab
// End File 
// Date of creation : Mon Apr  6 10:59:31 1998

/* HM_INDENT mustn't be define, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void end()
{
#endif

#ifdef ORCVXWORKS  
  (void)cycabPutMotor(CYCAB_OFF);
  (void)cycabPutDir(CYCAB_OFF);
  (void)cycabPutVel(CYCAB_OFF);
  (void)cycabPutBrake(CYCAB_ON);
#endif /* ORCVXWORKS */

#ifdef HM_INDENT
}
#endif

