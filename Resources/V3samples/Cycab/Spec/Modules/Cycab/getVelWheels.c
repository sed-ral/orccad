// Orccad Version: 3.0 alpha
// Module :  vel_wheels.c
// Call Driver File for variable: 
// 	 output DOUBLE vel_wheels
// Date of creation : Tue Feb 23 08:00:02 1999

/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void getposwheel()
{
#endif

//#ifdef ORCVXWORKS
  if (cycabGetVelTop(&vel_avg, &vel_avd, &vel_arg, &vel_ard) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  } else {
    /* TOP_TO_RADIAN defini dans cycabModule.h inclus dans drvCycab.h */
    VelWheelsO[0] = TOP_TO_RADIAN(vel_avg);
    VelWheelsO[1] = TOP_TO_RADIAN(vel_avd);
    VelWheelsO[2] = TOP_TO_RADIAN(vel_arg);
    VelWheelsO[3] = TOP_TO_RADIAN(vel_ard);
  }
//#endif /* ORCVXWORKS */

#ifdef HM_INDENT
}
#endif
