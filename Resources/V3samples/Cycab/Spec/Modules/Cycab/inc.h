// Orccad Version: 3.0 alpha   
// Module : Cycab
// Include and Definition File
// Date of creation : Mon Apr  6 10:59:31 1998

#include <Rcan.h>
#include <drvCycab.h>

#ifndef M_PI
#define M_PI 3.1415927
#endif

#define TOP_TO_RADIAN(t) (M_PI * ((double)t) / 20480.0)

