// Ilv Version: 2.4
// File generated: Fri Apr  9 16:44:26 1999
// Creator class: IlvGraphOutput
Palettes 8
6 0 49087 65535 62965 57054 46003 "default" "fixed" 0 solid solid 0 0 0
5 48830 48830 48830 "red" "default" "fixed" 0 solid solid 0 0 0
3 0 49087 65535 "red" "default" "fixed" 0 solid solid 0 0 0
"IlvStText" 7 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
1 0 49087 65535 "blue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 19
3 0 { 0 0 OrcIlvModPr 2
6 IlvFilledRectangle 110 100 200 220 
7 IlvMessageLabel 110 100 201 221 0 "Cycab" 16 [Box
0 1 1 Cycab 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 100 140 20 20 2 
2 IlvMessageLabel 122 136 82 28 0 "Wheels" 1 0
[Port
0 1 1 Wheels 110 150 122 136 82 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 100 260 20 20 2 
2 IlvMessageLabel 122 256 32 28 0 "Dir" 1 0
[Port
0 1 2 Dir 110 270 122 256 32 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 300 140 20 20 2 
2 IlvMessageLabel 236 136 124 28 0 "PosWheels" 1 0
[Port
0 1 3 PosWheels 310 150 236 136 124 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 300 260 20 20 2 
2 IlvMessageLabel 261 256 74 28 0 "PosDir" 1 0
[Port
0 1 4 PosDir 310 270 261 256 74 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 250 90 20 20 2 
2 IlvMessageLabel 237 105 94 28 0 "Joystick" 1 0
[Port
0 1 5 Joystick 260 100 237 105 94 28 2  2 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 250 310 20 20 2 
2 IlvMessageLabel 226 291 136 28 0 "LooseComm" 1 0
[Port
0 1 6 LooseComm 260 320 226 291 136 28 8  8 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 150 90 20 20 2 
2 IlvMessageLabel 142 105 72 28 0 "Status" 1 0
[Port
0 1 7 Status 160 100 142 105 72 28 2  2 
Port]

 } 20
1 1 { 14 0 OrcIlvVoidLink 1 0 13 } 30
2 0 { 15 0 OrcIlvPortDrv 2
5 IlvFilledRoundRectangle 300 180 20 20 2 
2 IlvMessageLabel 238 176 120 28 0 "VelWheels" 1 0
[Port
0 1 8 VelWheels 310 190 238 176 120 28 4  4 
Port]

 } 20
1 1 { 16 0 OrcIlvVoidLink 1 0 15 } 30
2 0 { 17 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 150 310 20 20 2 
2 IlvMessageLabel 135 291 100 28 0 "BadInput" 1 0
[Port
0 1 9 BadInput 160 320 135 291 100 28 8  8 
Port]

 } 20
1 1 { 18 0 OrcIlvVoidLink 1 0 17 } 30
