// Orccad Version: 3.0 alpha
// Module :  getjoystick.c
// Call Driver File for variable joystick
// Date of creation : Mon Apr  6 14:27:55 1998

/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void getjoystick()
{
#endif

//#ifdef ORCVXWORKS
  if (cycabGetJoystick(&joy_dir, &joy_tra) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  } else {
    JoystickO[0] = (double)joy_dir;
    JoystickO[1] = (double)joy_tra;
  }
//#endif /* ORCVXWORKS */

#ifdef HM_INDENT
}
#endif

