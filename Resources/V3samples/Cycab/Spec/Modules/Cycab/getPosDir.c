// Orccad Version: 3.0 alpha
// Module :  getpos_dir.c
// Call Driver File for variable pos_dir
// Date of creation : Mon Apr  6 14:27:55 1998

/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void getposdir()
{
#endif

//#ifdef ORCVXWORKS
  if (cycabGetDirPos(&pos_dir) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  } else {
    /* passage de centieme de degre a des radians */
    PosDirO = M_PI * ((double)pos_dir) / 18000.0;
  }
//#endif /* ORCVXWORKS */

#ifdef HM_INDENT
}
#endif
