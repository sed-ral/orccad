// Orccad Version: 3.0 alpha
// Module :  getpos_wheels.c
// Call Driver File for variable pos_wheels
// Date of creation : Mon Apr  6 14:27:17 1998

/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void getposwheel()
{
#endif

//#ifdef ORCVXWORKS
  if (cycabGetPos(&pos_avg, &pos_avd, &pos_arg, &pos_ard) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  } else {
    /* TOP_TO_RADIAN definit dans cycabModule.h inclus dans drvCycab.h */
    PosWheelsO[0] = TOP_TO_RADIAN(pos_avg);
    PosWheelsO[1] = TOP_TO_RADIAN(pos_avd);
    PosWheelsO[2] = TOP_TO_RADIAN(pos_arg);
    PosWheelsO[3] = TOP_TO_RADIAN(pos_ard);
  }
//#endif /* ORCVXWORKS */

#ifdef HM_INDENT
}
#endif
