// Orccad Version: 3.0 alpha
// Module :  getstatus.c
// Call Driver File for variable status
// Date of creation : Mon Apr  6 14:27:17 1998

/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void getstatus()
{
#endif

//#ifdef ORCVXWORKS
  if (cycabGetState(&etat_ava, &etat_arr, &etat_dir) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  } else {
    StatusO = (etat_ava & 0xFF) + (etat_arr & 0xFF)*256 + 
      (etat_dir & 0xFF)*256*256;
  }
//#endif /* ORCVXWORKS */

#ifdef HM_INDENT
}
#endif
