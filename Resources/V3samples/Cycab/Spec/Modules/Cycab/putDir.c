// Orccad Version: 3.0 alpha
// Module :  putDir.c
// Call Driver File for variable dir
// Date of creation : Mon Apr  6 14:27:17 1998

/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void putdir()
{
#endif

//#ifdef ORCVXWORKS
#define MAX_DIR (M_PI / 3.0)
  if ((DirI > MAX_DIR) || (DirI < -MAX_DIR)) {
    if (BadInputE == NO_EVENT)
      BadInputE = SET_EVENT;
  } else {
    if (cycabPutDirPos((int)(18000.0 * DirI / M_PI)) == ERROR) {
      if (LooseCommE == NO_EVENT)
	LooseCommE = SET_EVENT;
    }
  }
//#endif /* ORCVXWORKS */

#ifdef HM_INDENT
}
#endif


