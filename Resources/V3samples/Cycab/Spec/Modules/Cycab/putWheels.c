// Orccad Version: 3.0 alpha
// Module :  putwheels.c
// Call Driver File for variable wheels
// Date of creation : Mon Apr  6 14:27:17 1998

/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void putwheels()
{
#endif

//#ifdef ORCVXWORKS
  /* La ressource physique CYCAB recoit des
     consignes en vitesse (radian par seconde). */
#define MAX_SPEED (6*M_PI)
  if ((WheelsI[0] > MAX_SPEED) || (WheelsI[0] < -MAX_SPEED) ||
      (WheelsI[1] > MAX_SPEED) || (WheelsI[1] < -MAX_SPEED) ||
      (WheelsI[2] > MAX_SPEED) || (WheelsI[2] < -MAX_SPEED) ||
      (WheelsI[3] > MAX_SPEED) || (WheelsI[3] < -MAX_SPEED)) {
    if (BadInputE == NO_EVENT)
      BadInputE = SET_EVENT;
  } else {
    if (cycabPutVelTop((int)(RADIAN_TO_TOP(WheelsI[0])),
		       (int)(RADIAN_TO_TOP(WheelsI[1])), 
		       (int)(RADIAN_TO_TOP(WheelsI[2])), 
		       (int)(RADIAN_TO_TOP(WheelsI[3]))) == ERROR) {
      if (LooseCommE == NO_EVENT)
	LooseCommE = SET_EVENT;
    }
  }
//#endif /* ORCVXWORKS */

#ifdef HM_INDENT
}
#endif
