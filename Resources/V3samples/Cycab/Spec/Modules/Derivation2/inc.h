// Orccad Version: 3.1    
// Module : Derivation
// Include and Definition File
// Date of creation : Tue Mar 13 10:42:19 2001

#ifndef M_PI
#define M_PI 3.141593
#endif

#ifndef VMAX
#define VMAX  15.0 // vitesse angulaire maximale sur chaque roue (en rad/s)
#endif


#ifndef VMIN
#define VMIN  15.0 // vitesse angulaire maximale sur chaque roue (en rad/s)
#endif


#ifndef MinDistance
#define MinDistance  2.5 // distace minimale de suivi (en m)
#endif


#ifndef TIME_WITHOUT_TARGET
#define TIME_WITHOUT_TARGET  9.0 // temps maxi sans detection de cible avant arret
#endif


#ifndef R
#define R   0.2  // rayon de roue du cycab, suppose constant (en m)
#endif

#ifndef PHI_MAX
#define PHI_MAX   0.4        // braquage maxi (en rad)
#endif

#ifndef PAS_PHI
#define PAS_PHI   0.01       // precision du "calcul" de phi (en rad)
#endif

#ifndef KCYCAB
#define KCYCAB   0.69         // rapport entre braquage avant et braquage arriere
#endif

#ifndef LW
#define LW   1.20        // empatement du vehicule (en m)
#endif


#ifndef RAD2DEG
#define RAD2DEG 180/M_PI
#endif


#ifndef Te
#define Te  0.1
#endif

#ifndef Vd
#define Vd  4  //(en rad/s)
#endif

#ifndef Dd
#define Dd  5  // (en m)
#endif
