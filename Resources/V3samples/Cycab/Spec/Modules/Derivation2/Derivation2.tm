// Ilv Version: 3.1
// File generated: Mon Jul 16 14:32:03 2001
// Creator class: IlvGraphOutput
Palettes 10
7 "Gray" "blue" "default" "fixed" 0 solid solid 0 0 0
1 "Gray" 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 "Gray" 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
8 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
0 "Gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
3 "Gray" 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
9 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 2 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
6 "Gray" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 21
3 0 { 0 0 OrcIlvModAlgo 2
0 IlvFilledRectangle 100 70 200 210 
1 IlvMessageLabel 100 70 201 211 F8 0 1 16 4 "Derivation2"   [Box
0 1 1 Derivation2 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
3 IlvFilledEllipse 190 60 20 20 
4 IlvMessageLabel 177 75 94 28 F8 0 25 16 4 "Joystick"   0
[Port
0 1 1 Joystick 200 70 177 75 94 28 2  2 
Port]

 } 20
1 1 { 2 2 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 230 270 20 20 
4 IlvMessageLabel 201 251 156 28 F8 0 25 16 4 "LinearCamera"   0
[Port
0 1 2 LinearCamera 240 280 201 251 156 28 8  8 
Port]

 } 20
1 1 { 4 2 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
5 IlvFilledEllipse 290 100 20 20 
4 IlvMessageLabel 239 96 102 28 F8 0 25 16 4 "Direction"   0
[Port
0 1 3 Direction 300 110 239 96 102 28 4  4 
Port]

 } 20
1 1 { 6 2 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
6 IlvFilledEllipse 290 140 20 20 
4 IlvMessageLabel 217 136 146 28 F8 0 25 16 4 "SpeedFollow"   0
[Port
0 1 4 SpeedFollow 300 150 217 136 146 28 4  4 
Port]

 } 20
1 1 { 8 2 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortPrm 2
3 IlvPolyline 11
162 272 178 288 170 280 178 272 162 288 170 280 178 280 162 280 170 280 170 272
170 288 
4 IlvMessageLabel 130 251 162 28 F8 0 25 16 4 "DesirePosition"   0
[Port
0 1 5 DesirePosition 170 280 130 251 162 28 8  8 
Port]

 } 20
1 1 { 10 2 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortVar 2
7 IlvFilledEllipse 90 100 20 20 
4 IlvMessageLabel 110 96 126 28 F8 0 25 16 4 "Pos_Wheel"   0
[Port
0 1 6 Pos_Wheel 100 110 110 96 126 28 1  1 
Port]

 } 20
1 1 { 12 2 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortVar 2
7 IlvFilledEllipse 90 170 20 20 
4 IlvMessageLabel 110 166 122 28 F8 0 25 16 4 "Vel_Wheel"   0
[Port
0 1 7 Vel_Wheel 100 180 110 166 122 28 1  1 
Port]

 } 20
1 1 { 14 2 OrcIlvVoidLink 1 0 13 } 30
2 0 { 15 0 OrcIlvPortVar 2
7 IlvFilledEllipse 90 220 20 20 
4 IlvMessageLabel 110 216 88 28 F8 0 25 16 4 "Pos_Dir"   0
[Port
0 1 8 Pos_Dir 100 230 110 216 88 28 1  1 
Port]

 } 20
1 1 { 16 2 OrcIlvVoidLink 1 0 15 } 30
2 0 { 17 0 OrcIlvPortVar 2
8 IlvFilledEllipse 290 180 20 20 
9 IlvMessageLabel 222 176 136 28 F8 0 25 16 4 "DiffDistance"   0
[Port
0 1 9 DiffDistance 300 190 222 176 136 28 4  4 
Port]

 } 20
1 1 { 18 2 OrcIlvVoidLink 1 0 17 } 30
2 0 { 19 0 OrcIlvPortVar 2
8 IlvFilledEllipse 290 220 20 20 
9 IlvMessageLabel 236 216 108 28 F8 0 25 16 4 "DiffSpeed"   0
[Port
0 1 10 DiffSpeed 300 230 236 216 108 28 4  4 
Port]

 } 20
1 1 { 20 2 OrcIlvVoidLink 1 0 19 } 30
EOF
