// Orccad Version: 3.1 
// Module :  Derivation
// End File 
// 
// Module Ports :
// 	input DOUBLE Pos_DirI
// 	input DOUBLE Vel_WheelI[4]
// 	input DOUBLE Pos_WheelI[4]
// 	param DOUBLE DesirePosition[4]
// 	output DOUBLE SpeedFollowO
// 	output DOUBLE Direction[1]
//      output DOUBLE DiffDistanceO[1]
//      output DOUBLE DiffSpeedO
// 	input DOUBLE LinearCamera[5]
// 	input DOUBLE Joystick[2]
// 
// 
// Date of creation : Tue Mar 13 10:42:19 2001

CondInit = false;
distance0 = 0.0;
