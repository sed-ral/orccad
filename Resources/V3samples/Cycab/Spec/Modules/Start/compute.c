// Orccad Version: 3.1   
// Module : TestRules
// Computation File 
//
// Module Ports :
//    
// 	input DOUBLE Pos_DirI
// 	input DOUBLE Vel_WheelI[4]
// 	input DOUBLE Pos_WheelI[4]
// 	param DOUBLE DesirePosition[4]
// 	output DOUBLE VelocityO[4]
// 	output DOUBLE DirectionO
// 	input DOUBLE LinearCamera[5]
// 	input DOUBLE Joystick[2]
//      output DOUBLE TimeO
//      output DOUBLE DistanceO
//
// Date of creation : Wed Apr 28 11:09:13 1999

double TempDir;
double TempVel;
double Wheel_Vel;
double atanTmp;
double Time;

bool TargetDetected = (LinearCamera[0]==0.0); // true if the target is visible
double alpha    = LinearCamera[1];
double beta     = LinearCamera[2];
double dist     = LinearCamera[3];
double dev      = LinearCamera[4];
double distance = sqrt(dist*dist + dev*dev);
bool cible;
double TempDistance;
double DistanceInitiale;




//printf("\n>>>>>> alpha=%f, beta=%f, dist=%f, dev=%f, distance=%f, target=%d\n\n",alpha, beta, dist, dev, distance, (TargetDetected)?1:0);
printf("Prog a JP");
  if (!TargetDetected) 
    // No detection of target 
    { 
    No_target_time += dt;
    cout << "TestRules: Pas de target depuis " << No_target_time << " sec" << endl << endl;
    } 
  else 
    // Target detected
    {  
    
      //  cout << "TestRules: Target detectee " << endl;
      //cout << "  --> Distance cible en metres   : " << distance << endl;
      //cout << "  --> Direction cible en degres : " << alpha * RAD2DEG;
      //cout << endl << endl;
      if(cible == 0)
	{
        DistanceInitiale =  ( Pos_WheelI[0] + Pos_WheelI[1] + Pos_WheelI[2] + Pos_WheelI[3] )/4;
	cible = 1;
	printf("############################################");
	cout << endl;
	}
      else
         {

    No_target_time = 0.0;
    
    if (Vel_old>=0) //the Car is Running Forward
     
  



 {
      if ( (distance-MinDistance)>=0) // the car don't arrive the min position
	                              // (3.0 meters between the car and the target)
   
  
	{
           // Longitual distance Using PD Control
	  Wheel_Vel= (Vel_WheelI[0]+Vel_WheelI[1]+Vel_WheelI[2]+Vel_WheelI[3])/4;
	  if ( Wheel_Vel < Vd )
	    { 
	      TempVel = Wheel_Vel+1.5;
	      cout << "etat de la variable cible " << cible <<endl;
	        TempDistance = ( Pos_WheelI[0] + Pos_WheelI[1] + Pos_WheelI[2] + Pos_WheelI[3] )/4;
		// DistanceO = - TempDistance + DistanceInitiale;
                DistanceO = TempDistance;

	    }
	  if ( TempVel > Vd)
	    {
	        TempVel = Vd;
		//DistanceO = ( Pos_WheelI[0] + Pos_WheelI[1] + Pos_WheelI[2] + Pos_WheelI[3] )/4;
                TempDistance = ( Pos_WheelI[0] + Pos_WheelI[1] + Pos_WheelI[2] + Pos_WheelI[3] )/4;
	        DistanceO = - TempDistance + DistanceInitiale;
		cout << "etat de la variable cible " << cible;

            } 
           cout << TempVel << "rad/s" << endl;
	   cout << "distance: "<< DistanceO <<endl;
	  VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = TempVel;



	  //Time + = dt;
          TimeO =distance;
       
	  // mode de suivi (0=normal; 1=avec calcul de phi)
	  bool SUIVI_COMPLEXE = 0;

	  if (((alpha*beta)>0.0) && (alpha!=0.0) && (SUIVI_COMPLEXE==1)) {
	    // calcul du rayon de courbure
	    double rayon = dev + dist*tan(M_PI/2.0 - alpha);
	    cout << "rayon : " << rayon << endl;
	    // recherche dichotomique du braquage en fonction du rayon de coubure
	    int debut = 1;
	    int fin = NbValPhi;
	    int milieu;
	    while (debut < fin) 
	      {
		milieu = (debut+fin)/2;
		if (TabCurv2Phi[milieu][0] > rayon) 
		  {
		    fin = milieu - 1;
		  } 
		else
		  if (TabCurv2Phi[milieu][0] < rayon) 
		    {
		      debut = milieu + 1;
		    }
		  else
		    fin = debut;
	      }
	    
	    // approximation lineaire de phi en fonction du rayon de courbure
	    TempDir = TabCurv2Phi[debut][1] +
	              ((rayon - TabCurv2Phi[debut][0]) * 
	               (TabCurv2Phi[fin][1] - TabCurv2Phi[debut][1]) / 
	               (TabCurv2Phi[fin][0] - TabCurv2Phi[debut][0]));
  
	    // affichage de debug
	    cout << "ANGLE DE BRAQUAGE PRECALCULE : " << RAD2DEG * TempDir << " degres" << endl;
	    
	  }
	  else  // Suivi elementaire ameliore
	    {
	      // Lateral distance Using P Control
	      atanTmp = dev / (2*dist);
	      if (atanTmp)
		TempDir = - atan (atanTmp);
	      else
		TempDir = 0.0;
	    }
	  
	  if (TempDir>= 0.3)   
	    DirectionO = 0.3;
	  else if (TempDir<= -0.3)  
	    DirectionO = - 0.3;
	  else  
	    DirectionO = TempDir;
        }
	else//the car arrived the min position
	  {
	    printf("on est a 3 m");
	    // = SET_EVENT;
	     VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = 0.0;
	      cout << endl;
	  }
	}
    else // the car is running backward
	{
	  VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = 0.0;
	}
      }
    }
 /*
  // Test No_target_time to stop Cycab if superior to TIME_WITHOUT_TARGET seconds
  if (No_target_time > TIME_WITHOUT_TARGET)    
    { 
      Wheel_Vel = (Vel_WheelI[0]+Vel_WheelI[1]+Vel_WheelI[2]+Vel_WheelI[3])/4;
      if ((Wheel_Vel>0.0) && (No_target_time < (TIME_WITHOUT_TARGET+1)))
	{
	  VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = Wheel_Vel/1.5;
	}
      else
	{
	  VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = 0.0;
	}

    }
*/

 
    
