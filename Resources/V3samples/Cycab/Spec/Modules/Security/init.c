// Orccad Version: 3.0 alpha
// Module : Security
// Initialisation File
//
// Module Ports :
// 	input INTEGER StatusI
// 	output EVENT EmergencyStopE
// 	output EVENT PbMotorE
// 	output EVENT DirOverE
//
// Date of creation : Mon Apr 12 12:01:39 1999

/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void init()
{
#endif

  PbMotorE = NO_EVENT;
  DirOverE = NO_EVENT;
  EmergencyStopE = NO_EVENT;

#ifdef HM_INDENT
}
#endif
