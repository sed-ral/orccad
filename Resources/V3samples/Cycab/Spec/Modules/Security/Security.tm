// Ilv Version: 2.4
// File generated: Mon Apr 12 12:01:39 1999
// Creator class: IlvGraphOutput
Palettes 6
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
3 "red" "red" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 130 70 160 230 
5 IlvMessageLabel 130 70 161 231 0 "Security" 16 [Box
0 1 1 Security 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 120 140 20 20 
2 IlvMessageLabel 142 136 72 28 0 "Status" 1 0
[Port
0 1 1 Status 130 150 142 136 72 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 280 100 20 20 2 
2 IlvMessageLabel 189 96 178 28 0 "EmergencyStop" 1 0
[Port
0 1 2 EmergencyStop 290 110 189 96 178 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 280 200 20 20 2 
2 IlvMessageLabel 230 196 96 28 0 "PbMotor" 1 0
[Port
0 1 3 PbMotor 290 210 230 196 96 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
2 0 { 7 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 280 250 20 20 2 
2 IlvMessageLabel 234 246 88 28 0 "DirOver" 1 0
[Port
0 1 4 DirOver 290 260 234 246 88 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 26
