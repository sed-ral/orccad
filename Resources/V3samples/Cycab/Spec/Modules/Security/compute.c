// Orccad Version: 3.0 alpha   
// Module : Security
// Computation File 
//
// Module Ports :
// 	input INTEGER StatusI
// 	output EVENT EmergencyStopE
// 	output EVENT PbMotorE
// 	output EVENT DirOverE
//
// Date of creation : Mon Apr 12 12:01:39 1999

#define BIT(a,b) ((a >> b) & 0x1)

/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void compute()
{
#endif
  if ((BIT(StatusI, 0) != 1) || (BIT(StatusI, 1) != 1) ||
      (BIT(StatusI, 3) != 1) || (BIT(StatusI, 4) != 1)) {
    if (PbMotorE == NO_EVENT) {
      PbMotorE = SET_EVENT;
    }
  }

  if ((BIT(StatusI, 5) != 1) || (BIT(StatusI, 6) != 1)) {
    if (DirOverE == NO_EVENT) {
      DirOverE = SET_EVENT;
    }
  }

  if ((BIT(StatusI, 2) != 1) || (BIT(StatusI, 7) != 1)) {
    if (EmergencyStopE == NO_EVENT) {
      EmergencyStopE = SET_EVENT;
    }
  }

#ifdef HM_INDENT
}
#endif
