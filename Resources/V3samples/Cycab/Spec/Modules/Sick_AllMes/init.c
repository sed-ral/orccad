// Orccad Version: 3.1 
// Module : Sick_Base
// Initialisation Driver File
// 
// Driver Ports : 
// 	output INTEGER DataO[361][2]
//
// Date of creation : Thu Nov 23 15:25:20 2000

#ifdef HM_INDENT
void init()
{
#endif

  (void)sickSocketInit();
  
  //send message for setting continue measure mode
  (void)sickSocketMsgHandle("931", answer);

  //send message for setting sick configuration wtis a file
  (void)sickSocketMsgHandle("740 0 0 0 SickAllMes.cf", answer);

  //send message for setting Monitoring measure mode (Minimum measured values per segment(on request)
  (void)sickSocketMsgHandle("356", answer);


#ifdef HM_INDENT
}
#endif
