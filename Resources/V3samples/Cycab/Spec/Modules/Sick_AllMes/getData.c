// Orccad Version: 3.1 
// Module :  getData.c
// Call Driver File for variable: 
// 	 output INTEGER DataO[361][2]
// Date of creation : Thu Nov 23 15:25:20 2000

#ifdef HM_INDENT
void getData()
{
#endif

  (void)sickSocketMsgHandle("611", answer);
  
  (void)sickReadMeasuredValuesAnswer(answer, &sickMeasuredValues); 
  
  for (j = 0 ; j < sickMeasuredValues.nbMesValues ; j++) {
    DataO[j][0] = sickMeasuredValues.distance[j];
    DataO[j][1] = sickMeasuredValues.flags[j];
    
  }
  
  (void)deleteMeasuredValues(&sickMeasuredValues);
  
  
  
#ifdef HM_INDENT
}
#endif
