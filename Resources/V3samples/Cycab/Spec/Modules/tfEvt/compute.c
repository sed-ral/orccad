// Orccad Version: 3.1   
// Module : tfEvt
// Computation File 
//
// Module Ports :
// 	output EVENT ManualModeE
// 	input DOUBLE PosTarget[5]
// 	output EVENT EmStopE
// 	output EVENT LooseTargetE
// 	input DOUBLE Joystick[2]
//      
//
// Date of creation : Wed Apr 14 09:32:44 1999

// notes :
// Joystick[0] --> left(+) - right(-)
// Joystick[1] --> forward(+) - backward(-)
// returned values -->  +/- 300
// PosTarget[0] --> Target Detected(0) - Target NOT Detected(-1)
// PosTarget[1] --> alpha
// PosTarget[2] --> beta
// PosTarget[3] --> dist
// PosTarget[4] --> dev

#ifdef HM_INDENT
void compute()
{
#endif

  bool TargetDetected = (PosTarget[0]==0.0); // true if the target is visible

  if (TargetDetected)  // Target detected 
     No_target_time = 0.0; 
  else // No target detection
     No_target_time += dt;

  if (ManualModeE == NO_EVENT)
    {
      if ((fabs(Joystick[0]) >= EPSILON_JOY) || //Joystick detected
	   (fabs(Joystick[1]) >= EPSILON_JOY)) 
              ManualModeE = SET_EVENT;  
    }
 
//IF NO TARGET for more than "DelayTarget" seconds then stop Cycab 
   if (LooseTargetE == NO_EVENT) 
     {
       if(No_target_time > DelayTarget)    
	   LooseTargetE = SET_EVENT;
     }

/********************** ARRET D'URGENCE **********************/

// generation d'un arret d'urgence lorsque le Joystick est dans n'importe
// quelle position extreme MAXPOS
if (EmStopE == NO_EVENT) 
  {
     if (fabs(Joystick[1]) >= MAXPOS) { // MAXPOS definie en inc.h
          EmStopE = SET_EVENT;
     }

  }

#ifdef HM_INDENT
}
#endif
