// Orccad Version: 3.1
// Module : tfEvt
// Initialisation File
//
// Module Ports :
// 	output EVENT ManualModeE
// 	input DOUBLE PosTarget[5]
// 	output EVENT EmergencyStopE
// 	output EVENT LooseTargetE
// 	input DOUBLE Joystick[2]
//
// Date of creation : Wed Apr 14 09:32:44 1999

// Initialisation du parametre (provisoire)
DelayTarget = 2.0;

// Initialisation des variables
dt= getSampleTime();
No_target_time=0.0;
LooseTargetE = NO_EVENT;
ManualModeE = NO_EVENT;
EmStopE = NO_EVENT;
