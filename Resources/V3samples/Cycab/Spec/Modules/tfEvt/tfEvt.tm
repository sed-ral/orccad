// Ilv Version: 2.4
// File generated: Tue Nov 16 16:28:54 1999
// Creator class: IlvGraphOutput
Palettes 7
5 "Gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"IlvStText" 6 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 65535 0 0 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
4 "red" "red" "default" "fixed" 0 solid solid 0 0 0
1 0 40097 65535 0 0 65535 "default" "fixed" 0 solid solid 0 0 0
IlvObjects 11
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 110 70 170 190 
6 IlvMessageLabel 110 70 171 191 0 "tfEvt" 16 [Box
0 1 1 tfEvt 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 120 20 20 
2 IlvMessageLabel 122 116 92 28 0 "Joystick" 1 0
[Port
0 1 1 Joystick 110 130 122 116 92 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 270 110 20 20 2 
2 IlvMessageLabel 198 106 140 28 0 "LooseTarget" 1 0
[Port
0 1 2 LooseTarget 280 120 198 106 140 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 190 250 20 20 2 
2 IlvMessageLabel 179 231 84 28 0 "EmStop" 1 0
[Port
0 1 3 EmStop 200 260 179 231 84 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 200 20 20 
2 IlvMessageLabel 122 196 112 28 0 "PosTarget" 1 0
[Port
0 1 5 PosTarget 110 210 122 196 112 28 1  1 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 270 180 20 20 2 
2 IlvMessageLabel 194 176 148 28 0 "ManualMode" 1 0
[Port
0 1 7 ManualMode 280 190 194 176 148 28 4  4 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
