// Orccad Version: 3.0 alpha
// Module : Sick
// Initialisation Driver File
// 
// Driver Ports : 
// 	output EVENT LooseCommE
// 	output INTEGER DataO[100]
//
// Date of creation : Mon Mar  6 13:33:55 2000

#ifdef HM_INDENT
void init()
{
#endif

for(segment=0;segment<9;segment++){
  DataO[segment]=0;
}

  (void)sickSocketInit();
  
  //send message for setting continue measure mode
  (void)sickSocketMsgHandle("931", answer);

  //send message for setting sick configuration wtis a file
  (void)sickSocketMsgHandle("740 0 0 0 Sick9Seg.cf", answer);

  //send message for setting Monitoring measure mode (Minimum measured values per segment(on request)
  (void)sickSocketMsgHandle("356", answer);

#ifdef HM_INDENT
}
#endif
