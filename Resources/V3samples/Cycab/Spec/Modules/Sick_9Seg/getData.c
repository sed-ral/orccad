// Orccad Version: 3.0 alpha
// Module :  getData.c
// Call Driver File for variable: 
// 	 output INTEGER DataO
// Date of creation : Mon Mar  6 13:33:55 2000

#ifdef HM_INDENT
void getData()
{
#endif

(void)sickSocketMsgHandle("615 9", answer);

(void)sickReadMeasuredValuesAnswer(answer,&sickMeasuredValues); 


for(segment=0;segment<9;segment++){
  DataO[segment]=sickMeasuredValues.distance[segment];
}

//printf("\ntime:%d",sickMeasuredValues.localTime);

(void)deleteMeasuredValues(&sickMeasuredValues);


#ifdef HM_INDENT
}
#endif
