// Ilv Version: 2.4
// File generated: Fri Oct  1 15:40:32 1999
// Creator class: IlvGraphOutput
Palettes 6
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 48830 48830 48830 65535 0 0 "default" "fixed" 0 solid solid 0 0 0
4 48830 48830 48830 62965 57054 46003 "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 48830 48830 48830 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "red" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModPr 2
4 IlvFilledRectangle 120 80 150 100 
5 IlvMessageLabel 120 80 151 101 0 "LinearCamera" 16 [Box
0 1 1 LinearCamera 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 210 170 20 20 2 
2 IlvMessageLabel 192 151 114 28 0 "PosTarget" 1 0
[Port
0 1 1 PosTarget 220 180 192 151 114 28 8  8 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 160 70 20 20 2 
2 IlvMessageLabel 141 85 118 28 0 "LooseCam" 1 0
[Port
0 1 2 LooseCam 170 80 141 85 118 28 2  2 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
