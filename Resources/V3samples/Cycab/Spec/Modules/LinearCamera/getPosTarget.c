// Orccad Version: 3.0 alpha
// Module :  cl.c
// Call Driver File for variable: 
// 	 output DOUBLE Results
// Date of creation : Wed Feb 24 08:32:35 1999


/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void cl()
{
#endif

#ifdef ORCVXWORKS
  retour = cycabClReadResult(&PosTargetO[1], &PosTargetO[2], 
			     &PosTargetO[3], &PosTargetO[4]);

  if (retour == ERROR) {
    if (LooseCamE == NO_EVENT)
      LooseCamE = SET_EVENT;
  } else {
    if (retour == CYCAB_CL_NO_RESULT) {
      PosTargetO[0] = -1;
    } else {
      PosTargetO[0] = 0;
    }
  }
#endif /* ORCVXWORKS */

#ifdef HM_INDENT
}
#endif
