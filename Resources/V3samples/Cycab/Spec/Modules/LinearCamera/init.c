// Orccad Version: 3.0 alpha
// Module : LinearCamera
// Initialisation Driver File
// 
// Driver Ports : 
// 	output DOUBLE Results[5]
// 	output EVENT LooseComm
//
// Date of creation : Wed Feb 24 08:32:35 1999

#ifdef HM_INDENT
void init()
{
#endif
  cout << "Cycab Linear Camera Initialization... " << endl;
  retour = 0;
  PosTargetO[0] = -1.0;
  PosTargetO[1] = 0.0;
  PosTargetO[2] = 0.0;
  PosTargetO[3] = 0.0;
  PosTargetO[4] = 0.0;

  if (cycabClStart() == ERROR) {
    LooseCamE = SET_EVENT;
  } else {
    LooseCamE = NO_EVENT;
  }
#ifdef HM_INDENT
}
#endif
