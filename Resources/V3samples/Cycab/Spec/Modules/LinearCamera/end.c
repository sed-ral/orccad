// Orccad Version: 3.0 alpha
// Module :  LinearCamera
// End File 
// 
// Driver Ports : 
// 	output DOUBLE Results[5]
// 	output EVENT LooseCommEvt
//
// Date of creation : Wed Feb 24 08:32:35 1999

/* HM_INDENT mustn't be defined, it's just to help emacs to indent correctly */
#ifdef HM_INDENT
void end()
{
#endif
  cout << "Cycab Linear Camera End. " << endl;
  if (cycabClStop() == ERROR) {
    if (LooseCamE == NO_EVENT)
      LooseCamE = SET_EVENT;
  }
#ifdef HM_INDENT
}
#endif
