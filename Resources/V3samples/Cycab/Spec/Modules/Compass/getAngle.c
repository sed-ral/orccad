// Orccad Version: 3.0 alpha
// Module :  getAngle.c
// Call Driver File for variable: 
// 	 output DOUBLE AngleO
// Date of creation : Tue Nov 30 16:20:44 1999

if (cycabCompassRead(&AngleO) == ERROR) {
  fprintf(stderr, "cycabCompassRead return ERROR\n");
  AngleO = -1.0;
}
