// Orccad Version: 3.1    
// Module : Integator_100ms
// Include and Definition File
// Date of creation : Tue Mar 13 10:37:40 2001



#ifndef DELTA_T
#define DELTA_T  0.1  // (en s) Depend de la periode d`echantillonage choisie
#endif               //  pour le module. DELTA_T vaut obligatoirement cette
                      //  periode


#ifndef VMAX
#define VMAX   3     // en rad/s
#endif

#ifndef Alpha
#define Alpha  0.6
#endif
