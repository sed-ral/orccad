// Ilv Version: 3.1
// File generated: Wed May  9 15:07:59 2001
// Creator class: IlvGraphOutput
Palettes 8
3 "Gray" "red" "default" "fixed" 0 solid solid 0 0 0
7 "Gray" 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "Gray" "blue" "default" "fixed" 0 solid solid 0 0 0
5 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
6 "Gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
6 IlvFilledRectangle 100 70 200 210 
7 IlvMessageLabel 100 70 201 211 F8 0 1 16 4 "Integator_100ms"   [Box
0 1 1 Integator_100ms 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 120 20 20 
2 IlvMessageLabel 110 116 154 28 F8 0 25 16 4 "Accel_Follow"   0
[Port
0 1 6 Accel_Follow 100 130 110 116 154 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 220 20 20 
2 IlvMessageLabel 110 216 174 28 F8 0 25 16 4 "Speed_Follow2"   0
[Port
0 1 8 Speed_Follow2 100 230 110 216 174 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
3 IlvFilledEllipse 290 170 20 20 
2 IlvMessageLabel 243 166 94 28 F8 0 25 16 4 "Velocity"   0
[Port
0 1 10 Velocity 300 180 243 166 94 28 4  4 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
4 IlvFilledEllipse 90 160 20 20 
5 IlvMessageLabel 110 156 72 28 F8 0 25 16 4 "Speed"   0
[Port
0 1 11 Speed 100 170 110 156 72 28 1  1 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
EOF
