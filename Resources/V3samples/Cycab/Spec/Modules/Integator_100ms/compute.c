// Orccad Version: 3.1    
// Module : Integator_100ms
// Computation File 
// 
// Module Ports :
// 	output DOUBLE VelocityO
// 	input DOUBLE Accel_FollowI
// 	input DOUBLE Speed_Follow2I
//      input DOUBLE SpeedI
//      input DOUBLE RatioValueI
// 
// Date of creation : Tue Mar 13 10:37:40 2001


// Longitudinal Control Law

// integration of the acceleration in order to give a condition about speed to // the Cycab
// Pay  attention. This integration depends on the period used in the
// integrator module



double Vel;


if (( SpeedI == 0.0 ) && (( Speed_Follow2I == 0.0 )||( Accel_FollowI <= 0.0 )))

  VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = 0.0;

else
if(( SpeedI > 0.0 ) && ( Accel_FollowI <= 0.0 ))
{ 
  if ( SpeedI > VMAX)  //bridage de la vitesse due aux limites physiques supportees par le cycab. Limites reelles a 20 rad/s. 
  Vel = VMAX;
  VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = Vel;
}
if (( SpeedI > 0.0 ) && ( Accel_FollowI > 0.0 ))
{
   if (SpeedI > VMAX)
   {
     Vel = VMAX;
     VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = Vel;
   }
  else
   {
     VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = Speed_Follow2I;
   }
}
else 
if (( SpeedI == 0.0 ) && ( Accel_FollowI > 0.0 ))
{
  if (Speed_Follow2I == 0.0 )
   {
     VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = 0.0;
   }
  else 
   {
    if ( acc_old  - Accel_FollowI <= 0 )
      {
         Vel = ( Speed_Follow2I + ( DELTA_T * Accel_FollowI ));
         Vel = ( Vel / 0.2 ); // radial velocity
         if ( Vel > VMAX )
          {
            Vel = VMAX;
            VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = Vel;
          }
         else
          {
            VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = Vel;
          }
      }
    else
      {
	Vel = ( Speed_Follow2I - ( DELTA_T * Accel_FollowI ) );
         Vel = ( Vel / 0.2 );
         if ( Vel > VMAX)
          {
            Vel = VMAX;
            VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = Vel;
          }
         else
          {
            VelocityO[0] = VelocityO[1] = VelocityO[2] = VelocityO[3] = Vel;
          }
     }
   }
}
