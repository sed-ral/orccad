// Ilv Version: 3.1
// File generated: Wed May  9 14:28:32 2001
// Creator class: IlvGraphOutput
Palettes 8
7 "Gray" 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
6 "Gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
5 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "Gray" "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 "Gray" "blue" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 11
3 0 { 0 0 OrcIlvModAlgo 2
6 IlvFilledRectangle 100 70 200 210 
7 IlvMessageLabel 100 70 201 211 F8 0 1 16 4 "AccelRule"   [Box
0 1 1 AccelRule 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 120 20 20 
2 IlvMessageLabel 110 116 160 28 F8 0 25 16 4 "Speed_Follow"   0
[Port
0 1 6 Speed_Follow 100 130 110 116 160 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 170 20 20 
2 IlvMessageLabel 110 166 150 28 F8 0 25 16 4 "Diff_Distance"   0
[Port
0 1 7 Diff_Distance 100 180 110 166 150 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 220 20 20 
2 IlvMessageLabel 110 216 122 28 F8 0 25 16 4 "Diff_Speed"   0
[Port
0 1 8 Diff_Speed 100 230 110 216 122 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
3 IlvFilledEllipse 290 170 20 20 
2 IlvMessageLabel 220 166 140 28 F8 0 25 16 4 "AccelFollow"   0
[Port
0 1 10 AccelFollow 300 180 220 166 140 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortVar 2
4 IlvFilledEllipse 190 270 20 20 
5 IlvMessageLabel 169 256 124 28 F8 0 25 16 4 "RatioValue"   0
[Port
0 1 11 RatioValue 200 280 169 256 124 28 8  8 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 26
EOF
