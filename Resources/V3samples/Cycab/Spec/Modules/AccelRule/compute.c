// Orccad Version: 3.1   
// Module : TestRules
// Computation File 
//
// Module Ports :
//      input DOUBLE Speed_FollowI
// 	input DOUBLE Diff_DistanceI
//      input DOUBLE Diff_SpeedI
// 	output DOUBLE AccelFollowO
//      output DOUBLE RatioValueO
//
// Date of creation : Wed Apr 28 11:09:13 1999


double TempAcceleration;
double r;
double DiffDistance;
double SpeedFollow;
double DiffSpeed;

DiffDistance = Diff_DistanceI;        
SpeedFollow = Speed_FollowI;
DiffSpeed = Diff_SpeedI;

    
if (DiffSpeed > 0.0)
{
  DiffSpeed = Alpha * Diff_SpeedI + (1-Alpha) * OldDiffSpeed;
}

if (DiffDistance > 0.0)
{
  DiffDistance = Alpha * Diff_DistanceI + (1-Alpha) * OldDiffDistance;
}

if (SpeedFollow > 0.0)
{
  SpeedFollow = Alpha * Speed_FollowI+ (1-Alpha) * OldSpeedFollow;  
}



if (( Diff_DistanceI == 0.0 ) && ( Diff_SpeedI == 0.0 ) && ( Speed_FollowI == 0.0 ))
{
  AccelFollowO=0.0;
}
else
{
  if (Speed_FollowI == 0.0)
  {
    Speed_Follow = Speed_FollowI;
    r = 1.1;
  }
  else
   {
     r = (( Diff_DistanceI ) / ( H * Speed_FollowI ));
     cout << "r:" << r << endl;
     RatioValueO = r;
     TempAcceleration = ( Diff_SpeedI / H) + Lambda * r - Lambda; 
     AccelFollowO = TempAcceleration;
   }
}
OldDiffDistance = DiffDistance;
OldSpeedFollow = SpeedFollow;
OldDiffSpeed = DiffSpeed;
