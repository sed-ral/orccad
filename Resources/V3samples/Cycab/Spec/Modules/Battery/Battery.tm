// Ilv Version: 2.4
// File generated: Wed Jul 21 13:39:41 1999
// Creator class: IlvGraphOutput
Palettes 5
3 "gray" "wheat" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 3
3 0 { 0 0 OrcIlvModPr 2
3 IlvFilledRectangle 110 120 170 90 
4 IlvMessageLabel 110 120 171 91 0 "Battery" 16 [Box
0 1 1 Battery 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 190 200 20 20 2 
2 IlvMessageLabel 212 196 58 28 0 "Volts" 1 0
[Port
0 1 1 Volts 200 210 212 196 58 28 1  8 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
