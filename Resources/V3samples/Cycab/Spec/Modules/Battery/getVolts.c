// Orccad Version: 3.0 alpha
// Module :  getVolts.c
// Call Driver File for variable: 
// 	 output DOUBLE VoltsO
// Date of creation : Wed Jul 21 13:39:41 1999


if (cycabBatteryRead(&VoltsO) == ERROR) {
  fprintf(stderr, "cycabBatteryRead return ERROR\n");
  VoltsO = 0.0;
}
