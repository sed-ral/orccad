// Orccad Version: 3.1   
// Module : TestRules
// Computation File 
//
// Module Ports :
//      input DOUBLE Speed_FollowI
// 	input DOUBLE Diff_DistanceI
//      input DOUBLE Diff_SpeedI
// 	output DOUBLE AccelFollowFO
//      output DOUBLE RatioO
//      output DOUBLE DiffDistanceFO
//      output DOUBLE DiffSpeedFO
// Date of creation : Wed Apr 28 11:09:13 1999


double TempAcceleration;
double r;
double DiffDistance;
double SpeedFollow;
double DiffSpeed;

DiffDistance = Diff_DistanceI;        
SpeedFollow = Speed_FollowI;
DiffSpeed = Diff_SpeedI;

if (DiffSpeed > 0.0)
{
  DiffSpeed = Alpha * Diff_SpeedI + (1-Alpha) * OldDiffSpeed;
}
else
  DiffSpeed = DiffSpeed;

if (DiffDistance > 0.0)
{
  DiffDistance = Alpha * Diff_DistanceI + (1-Alpha) * OldDiffDistance;
}
else
  DiffDistance = DiffDistance;

if (SpeedFollow > 0.0)
{
  SpeedFollow = Alpha * Speed_FollowI+ (1-Alpha) * OldSpeedFollow;  
}
else 
  SpeedFollow = SpeedFollow;

DiffDistanceFO = Diff_DistanceI;
DiffSpeedFO = Diff_SpeedI;

//calcul du ratio a partir de donnees filtrees par un PB numerique
     
     r = (( DiffDistance ) / ( H * SpeedFollow ));
     RatioO = r;
     

//calcul du ratio a partir de donnees filtrees par un PB numerique

     TempAcceleration = ( DiffSpeed / H) + Lambda * r - Lambda; 
     AccelFollowFO = TempAcceleration;




OldDiffDistance = DiffDistance;
OldSpeedFollow = SpeedFollow;
OldDiffSpeed = DiffSpeed;














