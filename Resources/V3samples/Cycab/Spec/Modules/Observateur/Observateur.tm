// Ilv Version: 3.1
// File generated: Mon May 21 15:20:25 2001
// Creator class: IlvGraphOutput
Palettes 8
3 "Gray" "red" "default" "fixed" 0 solid solid 0 0 0
7 "Gray" 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
1 "Gray" "blue" "default" "fixed" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
6 "Gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
5 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 15
3 0 { 0 0 OrcIlvModAlgo 2
6 IlvFilledRectangle 150 90 200 210 
7 IlvMessageLabel 150 90 201 211 F8 0 1 16 4 "Observateur"   [Box
0 1 1 Observateur 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 140 140 20 20 
2 IlvMessageLabel 160 136 160 28 F8 0 25 16 4 "Speed_Follow"   0
[Port
0 1 6 Speed_Follow 150 150 110 116 160 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
1 IlvFilledEllipse 140 190 20 20 
2 IlvMessageLabel 160 186 150 28 F8 0 25 16 4 "Diff_Distance"   0
[Port
0 1 7 Diff_Distance 150 200 110 166 150 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
1 IlvFilledEllipse 140 240 20 20 
2 IlvMessageLabel 160 236 122 28 F8 0 25 16 4 "Diff_Speed"   0
[Port
0 1 8 Diff_Speed 150 250 110 216 122 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
3 IlvFilledEllipse 340 190 20 20 
2 IlvMessageLabel 262 186 156 28 F8 0 25 16 4 "AccelFollowF"   0
[Port
0 1 10 AccelFollowF 350 200 262 186 156 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortVar 2
4 IlvFilledEllipse 340 100 20 20 
5 IlvMessageLabel 264 96 152 28 F8 0 25 16 4 "DiffDistanceF"   0
[Port
0 1 12 DiffDistanceF 350 110 214 76 152 28 4  4 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortVar 2
4 IlvFilledEllipse 340 140 20 20 
5 IlvMessageLabel 278 136 124 28 F8 0 25 16 4 "DiffSpeedF"   0
[Port
0 1 13 DiffSpeedF 350 150 278 136 124 28 4  4 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 30
2 0 { 13 0 OrcIlvPortVar 2
4 IlvFilledEllipse 340 240 20 20 
5 IlvMessageLabel 311 236 58 28 F8 0 25 16 4 "Ratio"   0
[Port
0 1 14 Ratio 350 250 311 236 58 28 4  4 
Port]

 } 20
1 1 { 14 0 OrcIlvVoidLink 1 0 13 } 30
EOF
