// Orccad Version: 3.1
// Module :  TestRules
// End File 
//
// Module Ports :
// 	input DOUBLE Speed_FollowI
// 	input DOUBLE Diff_DistanceI
// 	input DOUBLE Diff_SpeedI
// 	output DOUBLE AccelFollowO
//      output DOUBLE RatioValueO
//
// Date of creation : Wed Apr 28 11:09:13 1999

OldDiffDistance = 0.0;
Oldr = 0.0;
OldSpeedFollow=0.0;
OldTempAcceleration = 0.0;
OldDiffSpeed = 0.0;
