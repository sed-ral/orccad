/* scc : C CODE OF EQUATIONS ProcJoystickDriving */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
int (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define __BASIC_ACT(i) ((*__ProcJoystickDriving_PActionArray[i])())
#ifdef TRACE_ACTION
#define __ACT(i) (fprintf(stderr, "__ProcJoystickDriving_A%d\n", i),__BASIC_ACT(i))
#else
#define __ACT(i) __BASIC_ACT(i)
#endif
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __ProcJoystickDriving_GENERIC_TEST(TEST) return TEST;
typedef (*__ProcJoystickDriving_APF)();
extern __ProcJoystickDriving_APF *__ProcJoystickDriving_PActionArray;

#include "ProcJoystickDriving.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr();
#endif
#endif
#ifndef _JoystickDriving_controler_DEFINED
#ifndef JoystickDriving_controler
extern void JoystickDriving_controler();
#endif
#endif
#ifndef _JoystickDriving_fileparameter_DEFINED
#ifndef JoystickDriving_fileparameter
extern void JoystickDriving_fileparameter();
#endif
#endif
#ifndef _JoystickDriving_parameter_DEFINED
#ifndef JoystickDriving_parameter
extern void JoystickDriving_parameter();
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __ProcJoystickDriving_V0;
static boolean __ProcJoystickDriving_V1;
static orcStrlPtr __ProcJoystickDriving_V2;
static boolean __ProcJoystickDriving_V3;
static boolean __ProcJoystickDriving_V4;
static boolean __ProcJoystickDriving_V5;
static boolean __ProcJoystickDriving_V6;
static boolean __ProcJoystickDriving_V7;


/* INPUT FUNCTIONS */

ProcJoystickDriving_I_START_ProcJoystickDriving () {
__ProcJoystickDriving_V0 = _true;
}
ProcJoystickDriving_I_Abort_Local_ProcJoystickDriving () {
__ProcJoystickDriving_V1 = _true;
}
ProcJoystickDriving_I_JoystickDriving_Start (__V) orcStrlPtr __V; {
_orcStrlPtr(&__ProcJoystickDriving_V2,__V);
__ProcJoystickDriving_V3 = _true;
}
ProcJoystickDriving_I_USERSTART_JoystickDriving () {
__ProcJoystickDriving_V4 = _true;
}
ProcJoystickDriving_I_BF_JoystickDriving () {
__ProcJoystickDriving_V5 = _true;
}
ProcJoystickDriving_I_T3_JoystickDriving () {
__ProcJoystickDriving_V6 = _true;
}
ProcJoystickDriving_I_T2_DirOverE () {
__ProcJoystickDriving_V7 = _true;
}

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int ProcJoystickDriving_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

static int __ProcJoystickDriving_A1 () {
__ProcJoystickDriving_GENERIC_TEST(__ProcJoystickDriving_V0);
}
static int __ProcJoystickDriving_A2 () {
__ProcJoystickDriving_GENERIC_TEST(__ProcJoystickDriving_V1);
}
static int __ProcJoystickDriving_A3 () {
__ProcJoystickDriving_GENERIC_TEST(__ProcJoystickDriving_V3);
}
static int __ProcJoystickDriving_A4 () {
__ProcJoystickDriving_GENERIC_TEST(__ProcJoystickDriving_V4);
}
static int __ProcJoystickDriving_A5 () {
__ProcJoystickDriving_GENERIC_TEST(__ProcJoystickDriving_V5);
}
static int __ProcJoystickDriving_A6 () {
__ProcJoystickDriving_GENERIC_TEST(__ProcJoystickDriving_V6);
}
static int __ProcJoystickDriving_A7 () {
__ProcJoystickDriving_GENERIC_TEST(__ProcJoystickDriving_V7);
}

/* OUTPUT ACTIONS */

static int __ProcJoystickDriving_A8 () {
ProcJoystickDriving_O_BF_ProcJoystickDriving();
return 0;
}
static int __ProcJoystickDriving_A9 () {
ProcJoystickDriving_O_STARTED_ProcJoystickDriving();
return 0;
}
static int __ProcJoystickDriving_A10 () {
ProcJoystickDriving_O_GoodEnd_ProcJoystickDriving();
return 0;
}
static int __ProcJoystickDriving_A11 () {
ProcJoystickDriving_O_Abort_ProcJoystickDriving();
return 0;
}
static int __ProcJoystickDriving_A12 () {
ProcJoystickDriving_O_T3_ProcJoystickDriving();
return 0;
}
static int __ProcJoystickDriving_A13 () {
ProcJoystickDriving_O_START_JoystickDriving();
return 0;
}
static int __ProcJoystickDriving_A14 () {
ProcJoystickDriving_O_Abort_Local_JoystickDriving();
return 0;
}

/* ASSIGNMENTS */

/* PROCEDURE CALLS */

static int __ProcJoystickDriving_A15 () {
JoystickDriving_parameter(__ProcJoystickDriving_V2);
return 0;
}
static int __ProcJoystickDriving_A16 () {
JoystickDriving_controler(__ProcJoystickDriving_V2);
return 0;
}

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

/* ACTION SEQUENCES */

/* THE ACTION ARRAY */

static __ProcJoystickDriving_APF __ProcJoystickDriving_ActionArray[] = {
0,__ProcJoystickDriving_A1,__ProcJoystickDriving_A2,__ProcJoystickDriving_A3,__ProcJoystickDriving_A4,__ProcJoystickDriving_A5,__ProcJoystickDriving_A6,__ProcJoystickDriving_A7,__ProcJoystickDriving_A8,__ProcJoystickDriving_A9,__ProcJoystickDriving_A10,__ProcJoystickDriving_A11,__ProcJoystickDriving_A12,__ProcJoystickDriving_A13,__ProcJoystickDriving_A14,__ProcJoystickDriving_A15,__ProcJoystickDriving_A16
};
static __ProcJoystickDriving_APF *__ProcJoystickDriving_PActionArray  = __ProcJoystickDriving_ActionArray;


static __ProcJoystickDriving__reset_input () {
__ProcJoystickDriving_V0 = _false;
__ProcJoystickDriving_V1 = _false;
__ProcJoystickDriving_V3 = _false;
__ProcJoystickDriving_V4 = _false;
__ProcJoystickDriving_V5 = _false;
__ProcJoystickDriving_V6 = _false;
__ProcJoystickDriving_V7 = _false;
}

static int  __SCRUN__();
static void __SCRESET__();

#define __KIND 0
#define __AUX 1
#define __KNOWN 2
#define __DEFAULT_VALUE 3
#define __VALUE 4
#define __ARITY 5
#define __PREDECESSOR_COUNT 6
#define __ACCESS_ARITY  7
#define __ACCESS_COUNT 8
#define __LISTS 9

#define __NET_TYPE__ int
enum {__STANDARD, __SELECTINC, __RETURN, __SINGLE, __SIGTRACE, __ACTION, __TEST, __REG, __HALT} __NET_KIND__;

#define  __MODULE_NAME__ "ProcJoystickDriving"

/* THE TABLE OF NETS */

static __NET_TYPE__ __ProcJoystickDriving_nets [] = {
/* BF_ProcJoystickDriving__O_*/
__ACTION,8,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S0_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,2,0,
/* TRACE_S0_*/
__SIGTRACE,0,0,1,1,1,1,0,0,0,1,0,0,
/* STARTED_ProcJoystickDriving__O_*/
__ACTION,9,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S1_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,5,0,
/* TRACE_S1_*/
__SIGTRACE,1,0,1,1,1,1,0,0,0,1,3,0,
/* GoodEnd_ProcJoystickDriving__O_*/
__ACTION,10,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S2_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,8,0,
/* TRACE_S2_*/
__SIGTRACE,2,0,1,1,1,1,0,0,0,1,6,0,
/* Abort_ProcJoystickDriving__O_*/
__ACTION,11,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S3_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,11,0,
/* TRACE_S3_*/
__SIGTRACE,3,0,1,1,1,1,0,0,0,1,9,0,
/* T3_ProcJoystickDriving__O_*/
__ACTION,12,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S4_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,14,0,
/* TRACE_S4_*/
__SIGTRACE,4,0,1,1,1,1,0,0,0,1,12,0,
/* START_ProcJoystickDriving__I_*/
__TEST,1,0,1,1,0,0,0,0,0,1,16,0,
/* PRESENT_S5_0_*/
__STANDARD,0,0,1,1,1,1,0,0,2,68,70,3,17,69,71,0,
/* TRACE_S5_*/
__SIGTRACE,5,0,1,1,1,1,0,0,0,0,0,
/* Abort_Local_ProcJoystickDriving__I_*/
__TEST,2,0,1,1,0,0,0,0,0,1,19,0,
/* PRESENT_S6_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,110,2,20,111,0,
/* TRACE_S6_*/
__SIGTRACE,6,0,1,1,1,1,0,0,0,0,0,
/* START_JoystickDriving__O_*/
__ACTION,13,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S7_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,23,0,
/* TRACE_S7_*/
__SIGTRACE,7,0,1,1,1,1,0,0,0,1,21,0,
/* Abort_Local_JoystickDriving__O_*/
__ACTION,14,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S8_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,26,0,
/* TRACE_S8_*/
__SIGTRACE,8,0,1,1,1,1,0,0,0,1,24,0,
/* JoystickDriving_Start__I_*/
__TEST,3,0,1,1,0,0,0,0,0,1,28,1,29,
/* PRESENT_S9_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,30,0,
/* UPDATED_S9_0_*/
__STANDARD,0,0,1,1,1,1,1,1,2,87,89,0,0,
/* TRACE_S9_*/
__SIGTRACE,9,0,1,1,1,1,0,0,0,0,0,
/* USERSTART_JoystickDriving__I_*/
__TEST,4,0,1,1,0,0,0,0,0,0,0,
/* TRACE_S10_*/
__SIGTRACE,10,0,1,1,1,1,0,0,0,0,0,
/* BF_JoystickDriving__I_*/
__TEST,5,0,1,1,0,0,0,0,0,1,34,0,
/* PRESENT_S11_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,97,2,35,98,0,
/* TRACE_S11_*/
__SIGTRACE,11,0,1,1,1,1,0,0,0,0,0,
/* T3_JoystickDriving__I_*/
__TEST,6,0,1,1,0,0,0,0,0,1,37,0,
/* PRESENT_S12_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,101,2,38,102,0,
/* TRACE_S12_*/
__SIGTRACE,12,0,1,1,1,1,0,0,0,0,0,
/* T2_DirOverE__I_*/
__TEST,7,0,1,1,0,0,0,0,0,1,40,0,
/* PRESENT_S13_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,99,2,41,100,0,
/* TRACE_S13_*/
__SIGTRACE,13,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S14_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,43,0,
/* TRACE_S14_*/
__SIGTRACE,14,0,1,1,1,1,0,0,0,0,0,
/* TRACE_S15_*/
__SIGTRACE,15,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S16_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,46,0,
/* TRACE_S16_*/
__SIGTRACE,16,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S17_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,48,0,
/* TRACE_S17_*/
__SIGTRACE,17,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S18_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,50,0,
/* TRACE_S18_*/
__SIGTRACE,18,0,1,1,1,1,0,0,0,0,0,
/* BOOT_REGISTER_*/
__REG,0,0,1,1,1,1,0,0,2,70,71,0,0,
/* ROOT_RESUME_*/
__STANDARD,0,0,1,1,1,1,0,0,3,67,96,109,0,0,
/* ROOT_KILL_*/
__STANDARD,0,0,1,1,1,1,0,0,0,2,61,61,0,
/* ROOT_STAY_*/
__STANDARD,0,0,1,1,1,1,0,0,0,3,62,91,104,0,
/* ROOT_RETURN_*/
__RETURN,0,0,1,1,1,1,0,0,0,0,0,
/* ROOT_HALTING_*/
__STANDARD,0,0,1,1,1,1,0,0,0,0,0,
/* SELECT_2_B0_*/
__SELECTINC,0,0,1,1,2,2,0,0,1,58,1,58,0,
/* DEAD_2_B0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,0,0,
/* PARALLEL_2_UNION_K0_0_*/
__STANDARD,0,0,1,1,3,3,0,0,0,1,55,0,
/* PARALLEL_2_UNION_K1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,1,56,0,
/* PARALLEL_2_KILL_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,3,66,86,86,0,
/* RESUME_5_*/
__STANDARD,0,0,1,1,2,2,0,0,1,63,0,0,
/* MAINTAIN_5_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,65,0,
/* HALT_REG_5_*/
__HALT,1,0,1,0,1,1,0,0,2,63,67,1,57,0,
/* GO_5_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,66,1,60,0,
/* TO_REG_5_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,64,0,
/* TO_PRESENT_6_*/
__STANDARD,0,0,0,0,2,2,0,0,2,68,69,0,0,
/* THEN_7_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,72,0,
/* ELSE_7_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,62,0,
/* THEN_8_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,72,0,
/* ELSE_8_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,65,0,
/* GO_9_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,87,2,4,107,0,
/* SELECT_14_*/
__STANDARD,0,0,1,1,2,2,0,0,2,74,75,1,57,0,
/* DEAD_14_B0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,78,0,
/* DEAD_14_B1_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,79,0,
/* PARALLEL_14_UNION_K1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,77,0,0,
/* PARALLEL_14_CONT_K1_0_*/
__STANDARD,0,0,0,0,3,3,0,0,0,1,60,0,
/* PARALLEL_14_MIN_K1_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,77,1,81,0,
/* PARALLEL_14_MIN_K1_B1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,3,77,80,82,1,85,0,
/* PARALLEL_14_CONT_K2_0_*/
__STANDARD,0,0,0,0,3,3,0,0,0,4,1,7,59,86,0,
/* PARALLEL_14_MIN_K2_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,80,1,83,0,
/* PARALLEL_14_CONT_K3_0_*/
__STANDARD,0,0,0,0,3,3,0,0,0,3,13,59,86,0,
/* PARALLEL_14_MIN_K3_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,2,82,84,0,0,
/* PARALLEL_14_CONT_K4_0_*/
__STANDARD,0,0,0,0,3,3,0,0,0,2,59,86,0,
/* PARALLEL_14_MIN_K4_B1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,84,0,0,
/* PARALLEL_14_KILL_0_*/
__STANDARD,0,0,1,1,5,5,0,0,0,2,95,108,0,
/* CONT_15_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,88,0,
/* ACT_14_0_0_*/
__ACTION,15,0,1,1,1,1,0,0,1,89,0,0,
/* CONT_17_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,90,0,
/* ACT_15_0_0_*/
__ACTION,16,0,1,1,1,1,0,0,0,2,22,94,0,
/* RESUME_20_*/
__STANDARD,0,0,1,1,2,2,0,0,1,92,0,0,
/* MAINTAIN_20_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,94,0,
/* HALT_REG_20_*/
__HALT,2,0,1,0,1,1,0,0,2,92,96,2,73,74,0,
/* GO_20_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,95,2,76,78,0,
/* TO_REG_20_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,93,0,
/* TO_PRESENT_21_*/
__STANDARD,0,0,0,0,2,2,0,0,2,97,98,0,0,
/* THEN_22_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,103,0,
/* ELSE_22_0_*/
__STANDARD,0,0,0,0,2,2,0,0,2,99,100,0,0,
/* THEN_23_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,2,42,103,0,
/* ELSE_23_0_*/
__STANDARD,0,0,0,0,2,2,0,0,2,101,102,0,0,
/* THEN_25_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,82,2,47,83,0,
/* ELSE_25_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,91,0,
/* GO_28_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,80,2,49,81,0,
/* RESUME_30_*/
__STANDARD,0,0,1,1,2,2,0,0,1,105,0,0,
/* MAINTAIN_30_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,107,0,
/* HALT_REG_30_*/
__HALT,3,0,1,0,1,1,0,0,2,105,109,2,73,75,0,
/* GO_30_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,108,2,76,79,0,
/* TO_REG_30_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,106,0,
/* TO_PRESENT_31_*/
__STANDARD,0,0,0,0,2,2,0,0,2,110,111,0,0,
/* THEN_32_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,84,4,10,25,45,85,0,
/* ELSE_32_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,104,0,
/* __FALSE__*/
__STANDARD,0,0,0,0,0,0,0,0,0,5,32,44,51,53,54,0,
/* __TRUE__*/
__STANDARD,0,0,1,1,0,0,0,0,0,2,29,52,0};

/* THE NET ARRAY */

static __NET_TYPE__* __ProcJoystickDriving_net_array[] = {
__ProcJoystickDriving_nets+0,__ProcJoystickDriving_nets+12,__ProcJoystickDriving_nets+25,__ProcJoystickDriving_nets+38,__ProcJoystickDriving_nets+50,__ProcJoystickDriving_nets+63,__ProcJoystickDriving_nets+76,__ProcJoystickDriving_nets+88,__ProcJoystickDriving_nets+101,__ProcJoystickDriving_nets+114,__ProcJoystickDriving_nets+126,__ProcJoystickDriving_nets+139,__ProcJoystickDriving_nets+152,__ProcJoystickDriving_nets+164,__ProcJoystickDriving_nets+177,__ProcJoystickDriving_nets+190,__ProcJoystickDriving_nets+203,__ProcJoystickDriving_nets+220,__ProcJoystickDriving_nets+232,__ProcJoystickDriving_nets+245,__ProcJoystickDriving_nets+260,__ProcJoystickDriving_nets+272,__ProcJoystickDriving_nets+284,__ProcJoystickDriving_nets+297,__ProcJoystickDriving_nets+310,__ProcJoystickDriving_nets+322,__ProcJoystickDriving_nets+335,__ProcJoystickDriving_nets+348,__ProcJoystickDriving_nets+362,__ProcJoystickDriving_nets+375,__ProcJoystickDriving_nets+389,__ProcJoystickDriving_nets+401,__ProcJoystickDriving_nets+413,__ProcJoystickDriving_nets+425,__ProcJoystickDriving_nets+438,__ProcJoystickDriving_nets+453,__ProcJoystickDriving_nets+465,__ProcJoystickDriving_nets+478,__ProcJoystickDriving_nets+493,__ProcJoystickDriving_nets+505,__ProcJoystickDriving_nets+518,__ProcJoystickDriving_nets+533,__ProcJoystickDriving_nets+545,__ProcJoystickDriving_nets+558,__ProcJoystickDriving_nets+570,__ProcJoystickDriving_nets+582,__ProcJoystickDriving_nets+595,__ProcJoystickDriving_nets+607,__ProcJoystickDriving_nets+620,__ProcJoystickDriving_nets+632,__ProcJoystickDriving_nets+645,__ProcJoystickDriving_nets+657,__ProcJoystickDriving_nets+671,__ProcJoystickDriving_nets+686,__ProcJoystickDriving_nets+700,__ProcJoystickDriving_nets+715,__ProcJoystickDriving_nets+727,__ProcJoystickDriving_nets+739,__ProcJoystickDriving_nets+753,__ProcJoystickDriving_nets+765,__ProcJoystickDriving_nets+778,__ProcJoystickDriving_nets+791,__ProcJoystickDriving_nets+806,__ProcJoystickDriving_nets+819,__ProcJoystickDriving_nets+832,__ProcJoystickDriving_nets+847,__ProcJoystickDriving_nets+861,__ProcJoystickDriving_nets+874,__ProcJoystickDriving_nets+888,__ProcJoystickDriving_nets+901,__ProcJoystickDriving_nets+914,__ProcJoystickDriving_nets+927,__ProcJoystickDriving_nets+940,__ProcJoystickDriving_nets+955,__ProcJoystickDriving_nets+970,__ProcJoystickDriving_nets+983,__ProcJoystickDriving_nets+996,__ProcJoystickDriving_nets+1009,__ProcJoystickDriving_nets+1022,__ProcJoystickDriving_nets+1036,__ProcJoystickDriving_nets+1052,__ProcJoystickDriving_nets+1068,__ProcJoystickDriving_nets+1082,__ProcJoystickDriving_nets+1097,__ProcJoystickDriving_nets+1111,__ProcJoystickDriving_nets+1125,__ProcJoystickDriving_nets+1138,__ProcJoystickDriving_nets+1152,__ProcJoystickDriving_nets+1165,__ProcJoystickDriving_nets+1178,__ProcJoystickDriving_nets+1191,__ProcJoystickDriving_nets+1205,__ProcJoystickDriving_nets+1218,__ProcJoystickDriving_nets+1231,__ProcJoystickDriving_nets+1247,__ProcJoystickDriving_nets+1262,__ProcJoystickDriving_nets+1275,__ProcJoystickDriving_nets+1289,__ProcJoystickDriving_nets+1302,__ProcJoystickDriving_nets+1316,__ProcJoystickDriving_nets+1330,__ProcJoystickDriving_nets+1344,__ProcJoystickDriving_nets+1359,__ProcJoystickDriving_nets+1372,__ProcJoystickDriving_nets+1387,__ProcJoystickDriving_nets+1400,__ProcJoystickDriving_nets+1413,__ProcJoystickDriving_nets+1429,__ProcJoystickDriving_nets+1444,__ProcJoystickDriving_nets+1457,__ProcJoystickDriving_nets+1471,__ProcJoystickDriving_nets+1488,__ProcJoystickDriving_nets+1501,__ProcJoystickDriving_nets+1518,
};
#define __NET_ARRAY__ __ProcJoystickDriving_net_array

 /* THE QUEUE */

static __NET_TYPE__ __ProcJoystickDriving_queue[114] = {
15,18,27,31,33,36,39,51,64,93,106,112,113
};
#define __QUEUE__ __ProcJoystickDriving_queue
#define __NUMBER_OF_NETS__ 114
#define __NUMBER_OF_INITIAL_NETS__ 13
#define __NUMBER_OF_REGISTERS__ 4
#define __NUMBER_OF_HALTS__ 4

int ProcJoystickDriving () {

__SCRUN__();
__ProcJoystickDriving__reset_input();
return __ProcJoystickDriving_net_array[56][__VALUE];
}

/* AUTOMATON RESET */

int ProcJoystickDriving_reset () {
__SCRESET__();
__ProcJoystickDriving_net_array[51][__VALUE] = 1;
__ProcJoystickDriving_net_array[64][__VALUE] = 0;
__ProcJoystickDriving_net_array[93][__VALUE] = 0;
__ProcJoystickDriving_net_array[106][__VALUE] = 0;
__ProcJoystickDriving__reset_input();
return 0;
}
#ifdef __SIMUL_SCRUN
#include <assert.h>
#ifndef TRACE_ACTION
#include <stdio.h>
#endif
#endif
 

#define __KIND 0
#define __AUX 1
#define __KNOWN 2
#define __DEFAULT_VALUE 3
#define __VALUE 4
#define __ARITY 5
#define __PREDECESSOR_COUNT 6
#define __ACCESS_ARITY  7
#define __ACCESS_COUNT 8
#define __LISTS 9

#define __SIMUL_VALUE 0
#define __SIMUL_KNOWN 1

static int __free_queue_position;
#ifdef __SIMUL_SCRUN
static int __TESTRES__[__NUMBER_OF_NETS__];
#endif
static int __REGVAL[__NUMBER_OF_NETS__];
static int __NUMBER_OF_REGS_KNOWN;


static void __SET_VALUE (netNum, value) 
int netNum; 
int value; {
   int* net = __NET_ARRAY__[netNum];
#ifdef __SIMUL_SCRUN
   if (net[__KNOWN] && net[__KIND] == __SINGLE && net[__VALUE] && value) {
       __SINGLE_SIGNAL_EMITTED_TWICE_ERROR__(net[__AUX]);
   }
#endif
   if (net[__KNOWN]) return;
   net[__KNOWN] =  1;
   switch (net[__KIND]) {
      case __REG:
      case __HALT:
         __REGVAL[netNum] = value;
         __NUMBER_OF_REGS_KNOWN++;
#ifdef TRACE_SCRUN
            fprintf(stderr, "Save register %d with value %d\n", netNum, value);
#endif
         break;
      default:
         net[__VALUE] =  value;
         if (! net[__ACCESS_COUNT]) {
            __QUEUE__[__free_queue_position++] = netNum;
#ifdef TRACE_SCRUN
            fprintf(stderr, "Enqueue net %d with value %d\n", netNum, value);
#endif
         }
   }
}

static void __DECR_ARITY (netNum)
int netNum; {
   int* net = __NET_ARRAY__[netNum];
   if (! (--(net[__PREDECESSOR_COUNT]))) {
      __SET_VALUE(netNum, !net[__DEFAULT_VALUE]);
   }
}

static void __DECR_ACCESS_ARITY (netNum) 
int netNum; {
   int* net = __NET_ARRAY__[netNum];
   if (   ! (--(net[__ACCESS_COUNT])) 
       && net[__KNOWN]) {
      __QUEUE__[__free_queue_position++] = netNum;
#ifdef TRACE_SCRUN
      fprintf(stderr, "Enqueue %d by freeing last access\n", netNum);
#endif
   }
}

static void __SCFOLLOW (netNum, value)
int netNum;
int value; {
   int* net = __NET_ARRAY__[netNum];
   int* lists = net + __LISTS;
   int count;
   int i;  /* list index */
   int followerNum;

#ifdef __SIMUL_SCRUN
   __SIMUL_NET_TABLE__[netNum].known = 1;
   __SIMUL_NET_TABLE__[netNum].value = value;
#endif
#ifdef __SIMUL_SCRUN
   if (net[__KIND] == __TEST) {
       /* set __KNOWN for inputs (one has already __KNOWN==1 for other tests
          when reaching this point. This is only useful to print correctly 
          the causality error message.
       */
       net[__KNOWN] = 1;
   }
#endif
   count = lists[0];
   lists++;
   if (value) {
      for (i=0; i<count; i++) {
         followerNum = lists[0];
         lists++;
         if (! __NET_ARRAY__[followerNum][__KNOWN]) {
            __DECR_ARITY(followerNum);
         }
      }
      count = lists[0];
      lists++;
      for (i=0; i<count; i++) {
         followerNum =lists[0];
         lists++;
         __SET_VALUE(followerNum, 
                     __NET_ARRAY__[followerNum][__DEFAULT_VALUE]);
      }
  } else {
      for (i=0; i<count; i++) {
         followerNum = lists[0];
         lists++;
         __SET_VALUE(followerNum,
                     __NET_ARRAY__[followerNum][__DEFAULT_VALUE]);
      }
      count = lists[0];
      lists++;
      for (i=0; i<count; i++) {
         followerNum = lists[0];
         lists++;
         if (! __NET_ARRAY__[followerNum][__KNOWN]) {
            __DECR_ARITY(followerNum);
         }
      }
   }
   count = lists[0];
   lists++;
   for (i=0; i<count; i++) {
      followerNum = lists[0];
      lists++;
      __DECR_ACCESS_ARITY(followerNum);
   }
}

static int __SCRUN__ () {
   int queuePosition;
   int netNum;
   int* net;
   int value;    /* current net value */
   int testres;  /* result of test action */
   __free_queue_position = __NUMBER_OF_INITIAL_NETS__;

#ifdef TRACE_SCRUN
            fprintf(stderr, "\n***************************\n");
#endif

   /* Reset predecessor counts, access counts, and known flags */
   for (netNum=0; netNum< __NUMBER_OF_NETS__; netNum++) {
      net = __NET_ARRAY__[netNum];
      net[__PREDECESSOR_COUNT] = net[__ARITY];
      net[__ACCESS_COUNT] = net[__ACCESS_ARITY];
      net[__KNOWN] = 0;
#ifdef __SIMUL_SCRUN
      __SIMUL_NET_TABLE__[netNum].known = 0;
      __TESTRES__[netNum] = 0;
#endif
   }
   __NUMBER_OF_REGS_KNOWN = 0;

   /* Run main algorithm */
   for (queuePosition=0; 
        queuePosition < __free_queue_position; 
        queuePosition++) {
      netNum = __QUEUE__[queuePosition];
      net = __NET_ARRAY__[netNum];
      value = net[__VALUE];
#ifdef TRACE_SCRUN
      fprintf(stderr,
              "Step %d : processing net %d value %d\n",
              queuePosition, netNum, value);
#endif

      /* Decode kind and perform action if necessary */
      switch (net[__KIND]) {
         case __STANDARD :
         case __SELECTINC :
         case __SINGLE :   /* TO BE CHANGED FOR SINGLE SIGNAL TEST! */
         case __REG :
         case __HALT :
#ifdef __SIMUL_SCRUN
   __SIMUL_NET_TABLE__[netNum].value = value;
#endif
            __SCFOLLOW(netNum, value);
            break;
         case __RETURN :
#ifdef __SIMUL_SCRUN
            if (value) {
               __AppendToList(__HALT_LIST__, net[__AUX]);
            }
#endif
            __SCFOLLOW(netNum, value);
            break;
         case __SIGTRACE :
#ifdef __SIMUL_SCRUN
            if (value) {
               __AppendToList(__EMITTED_LIST__, net[__AUX]);
           }
#endif
            __SCFOLLOW(netNum, value);
            break;
         case __ACTION :
            if (value) {
               __ACT(net[__AUX]);
            }
            __SCFOLLOW(netNum, value);
            break;
         case __TEST :
            if (value) {
               testres = __ACT(net[__AUX]);
#ifdef __SIMUL_SCRUN
               __TESTRES__[netNum] = testres;
#endif
            }
            __SCFOLLOW(netNum, value && testres);
            break;
      }
   }                     
         
   /* check that all nets have been explored */ 
   {
      int seen = queuePosition + __NUMBER_OF_REGS_KNOWN;
      int tosee = __NUMBER_OF_NETS__ + __NUMBER_OF_REGISTERS__;
      if (seen != tosee) {
#ifdef __SIMUL_SCRUN
      __CAUSALITY_ERROR__;
#endif
         return -1;
     }
   }
  
   /* Set registers. The computed values were temporarily stored in
                     the auxiliary __REGVAL  array.
                     All the register nums are initially in the queue */

   for (queuePosition=0; 
        queuePosition < __NUMBER_OF_INITIAL_NETS__;
        queuePosition++) {
      netNum = __QUEUE__[queuePosition];
      net = __NET_ARRAY__[netNum];
      switch (net[__KIND]) {
         case __REG :
         case __HALT :
            net[__VALUE] = __REGVAL[netNum];
#ifdef TRACE_SCRUN
            fprintf(stderr, "Register %d set to %d\n", 
                            netNum, 
                            __REGVAL[netNum]);
#endif
#ifdef __SIMUL_SCRUN
            if (net[__KIND] == __HALT &&net[__VALUE]) {
               __AppendToList(__HALT_LIST__, net[__AUX]);
           }
#endif
            break;
        default:
            break;
        }
   }
   return 0;
}


static void __SCRESET__ () {
#ifdef __SIMUL_SCRUN
   int netNum;
   for (netNum=0; netNum < __NUMBER_OF_NETS__; netNum++) {
      __SIMUL_NET_TABLE__[netNum].known = 1;
      __SIMUL_NET_TABLE__[netNum].value = 0;
   }
#endif
}



