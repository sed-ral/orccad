/* scc : C CODE OF EQUATIONS aa */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
int (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define __BASIC_ACT(i) ((*__aa_PActionArray[i])())
#ifdef TRACE_ACTION
#define __ACT(i) (fprintf(stderr, "__aa_A%d\n", i),__BASIC_ACT(i))
#else
#define __ACT(i) __BASIC_ACT(i)
#endif
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __aa_GENERIC_TEST(TEST) return TEST;
typedef (*__aa_APF)();
extern __aa_APF *__aa_PActionArray;

#include "aa.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr();
#endif
#endif
#ifndef _JoystickDriving_controler_DEFINED
#ifndef JoystickDriving_controler
extern void JoystickDriving_controler();
#endif
#endif
#ifndef _JoystickDriving_fileparameter_DEFINED
#ifndef JoystickDriving_fileparameter
extern void JoystickDriving_fileparameter();
#endif
#endif
#ifndef _JoystickDriving_parameter_DEFINED
#ifndef JoystickDriving_parameter
extern void JoystickDriving_parameter();
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static orcStrlPtr __aa_V0;
static boolean __aa_V1;
static boolean __aa_V2;
static boolean __aa_V3;
static boolean __aa_V4;
static boolean __aa_V5;
static orcStrlPtr __aa_V6;
static boolean __aa_V7;
static boolean __aa_V8;
static boolean __aa_V9;
static boolean __aa_V10;
static boolean __aa_V11;
static boolean __aa_V12;
static boolean __aa_V13;
static boolean __aa_V14;
static boolean __aa_V15;
static boolean __aa_V16;
static boolean __aa_V17;
static boolean __aa_V18;
static boolean __aa_V19;
static boolean __aa_V20;
static orcStrlPtr __aa_V21;
static orcStrlPtr __aa_V22;
static orcStrlPtr __aa_V23;
static orcStrlPtr __aa_V24;
static orcStrlPtr __aa_V25;
static orcStrlPtr __aa_V26;
static orcStrlPtr __aa_V27;
static orcStrlPtr __aa_V28;
static boolean __aa_V29;


/* INPUT FUNCTIONS */

aa_I_Prr_Start (__V) orcStrlPtr __V; {
_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
aa_I_EmergencyStopE () {
__aa_V2 = _true;
}
aa_I_BadInputE () {
__aa_V3 = _true;
}
aa_I_BadinputE () {
__aa_V4 = _true;
}
aa_I_LooseCommE () {
__aa_V5 = _true;
}
aa_I_JoystickDriving_Start (__V) orcStrlPtr __V; {
_orcStrlPtr(&__aa_V6,__V);
__aa_V7 = _true;
}
aa_I_ActivateOK_Cycab () {
__aa_V8 = _true;
}
aa_I_KillOK_Cycab () {
__aa_V9 = _true;
}
aa_I_ReadyToStop_Cycab () {
__aa_V10 = _true;
}
aa_I_CmdStopOK_Cycab () {
__aa_V11 = _true;
}
aa_I_DirOverE () {
__aa_V12 = _true;
}
aa_I_InitOK_Cycab () {
__aa_V13 = _true;
}
aa_I_PbMotorE () {
__aa_V14 = _true;
}
aa_I_USERSTART_JoystickDriving () {
__aa_V15 = _true;
}
aa_I_T2_JoystickDriving () {
__aa_V16 = _true;
}
aa_I_ROBOT_FAIL () {
__aa_V17 = _true;
}
aa_I_SOFT_FAIL () {
__aa_V18 = _true;
}
aa_I_SENSOR_FAIL () {
__aa_V19 = _true;
}
aa_I_CPU_OVERLOAD () {
__aa_V20 = _true;
}

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int aa_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

static int __aa_A1 () {
__aa_GENERIC_TEST(__aa_V1);
}
static int __aa_A2 () {
__aa_GENERIC_TEST(__aa_V2);
}
static int __aa_A3 () {
__aa_GENERIC_TEST(__aa_V3);
}
static int __aa_A4 () {
__aa_GENERIC_TEST(__aa_V4);
}
static int __aa_A5 () {
__aa_GENERIC_TEST(__aa_V5);
}
static int __aa_A6 () {
__aa_GENERIC_TEST(__aa_V7);
}
static int __aa_A7 () {
__aa_GENERIC_TEST(__aa_V8);
}
static int __aa_A8 () {
__aa_GENERIC_TEST(__aa_V9);
}
static int __aa_A9 () {
__aa_GENERIC_TEST(__aa_V10);
}
static int __aa_A10 () {
__aa_GENERIC_TEST(__aa_V11);
}
static int __aa_A11 () {
__aa_GENERIC_TEST(__aa_V12);
}
static int __aa_A12 () {
__aa_GENERIC_TEST(__aa_V13);
}
static int __aa_A13 () {
__aa_GENERIC_TEST(__aa_V14);
}
static int __aa_A14 () {
__aa_GENERIC_TEST(__aa_V15);
}
static int __aa_A15 () {
__aa_GENERIC_TEST(__aa_V16);
}
static int __aa_A16 () {
__aa_GENERIC_TEST(__aa_V17);
}
static int __aa_A17 () {
__aa_GENERIC_TEST(__aa_V18);
}
static int __aa_A18 () {
__aa_GENERIC_TEST(__aa_V19);
}
static int __aa_A19 () {
__aa_GENERIC_TEST(__aa_V20);
}

/* OUTPUT ACTIONS */

static int __aa_A20 () {
aa_O_Activate(__aa_V21);
return 0;
}
static int __aa_A21 () {
aa_O_Abort_ProcJoystickDriving();
return 0;
}
static int __aa_A22 () {
aa_O_GoodEnd_ProcJoystickDriving();
return 0;
}
static int __aa_A23 () {
aa_O_STARTED_ProcJoystickDriving();
return 0;
}
static int __aa_A24 () {
aa_O_ActivateJoystickDriving_Cycab(__aa_V22);
return 0;
}
static int __aa_A25 () {
aa_O_JoystickDrivingTransite(__aa_V23);
return 0;
}
static int __aa_A26 () {
aa_O_Abort_JoystickDriving(__aa_V24);
return 0;
}
static int __aa_A27 () {
aa_O_STARTED_JoystickDriving();
return 0;
}
static int __aa_A28 () {
aa_O_GoodEnd_JoystickDriving();
return 0;
}
static int __aa_A29 () {
aa_O_EndKill(__aa_V25);
return 0;
}
static int __aa_A30 () {
aa_O_FinBF(__aa_V26);
return 0;
}
static int __aa_A31 () {
aa_O_FinT3(__aa_V27);
return 0;
}
static int __aa_A32 () {
aa_O_GoodEndRPr();
return 0;
}
static int __aa_A33 () {
aa_O_T3RPr();
return 0;
}

/* ASSIGNMENTS */

static int __aa_A34 () {
_orcStrlPtr(&__aa_V25,__aa_V28);
return 0;
}
static int __aa_A35 () {
_orcStrlPtr(&__aa_V26,__aa_V0);
return 0;
}
static int __aa_A36 () {
_orcStrlPtr(&__aa_V27,__aa_V0);
return 0;
}
static int __aa_A37 () {
_orcStrlPtr(&__aa_V24,__aa_V6);
return 0;
}
static int __aa_A38 () {
_orcStrlPtr(&__aa_V22,__aa_V6);
return 0;
}
static int __aa_A39 () {
_orcStrlPtr(&__aa_V28,__aa_V6);
return 0;
}
static int __aa_A40 () {
_orcStrlPtr(&__aa_V23,__aa_V6);
return 0;
}
static int __aa_A41 () {
_orcStrlPtr(&__aa_V23,__aa_V6);
return 0;
}
static int __aa_A42 () {
_orcStrlPtr(&__aa_V23,__aa_V6);
return 0;
}
static int __aa_A43 () {
_orcStrlPtr(&__aa_V23,__aa_V6);
return 0;
}
static int __aa_A44 () {
_orcStrlPtr(&__aa_V25,__aa_V28);
return 0;
}

/* PROCEDURE CALLS */

static int __aa_A45 () {
JoystickDriving_parameter(__aa_V6);
return 0;
}
static int __aa_A46 () {
JoystickDriving_controler(__aa_V6);
return 0;
}

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

static int __aa_A47 () {
0;
return 0;
}
static int __aa_A48 () {
0;
return 0;
}

/* ACTION SEQUENCES */

/* THE ACTION ARRAY */

static __aa_APF __aa_ActionArray[] = {
0,__aa_A1,__aa_A2,__aa_A3,__aa_A4,__aa_A5,__aa_A6,__aa_A7,__aa_A8,__aa_A9,__aa_A10,__aa_A11,__aa_A12,__aa_A13,__aa_A14,__aa_A15,__aa_A16,__aa_A17,__aa_A18,__aa_A19,__aa_A20,__aa_A21,__aa_A22,__aa_A23,__aa_A24,__aa_A25,__aa_A26,__aa_A27,__aa_A28,__aa_A29,__aa_A30,__aa_A31,__aa_A32,__aa_A33,__aa_A34,__aa_A35,__aa_A36,__aa_A37,__aa_A38,__aa_A39,__aa_A40,__aa_A41,__aa_A42,__aa_A43,__aa_A44,__aa_A45,__aa_A46,__aa_A47,__aa_A48
};
static __aa_APF *__aa_PActionArray  = __aa_ActionArray;


static __aa__reset_input () {
__aa_V1 = _false;
__aa_V2 = _false;
__aa_V3 = _false;
__aa_V4 = _false;
__aa_V5 = _false;
__aa_V7 = _false;
__aa_V8 = _false;
__aa_V9 = _false;
__aa_V10 = _false;
__aa_V11 = _false;
__aa_V12 = _false;
__aa_V13 = _false;
__aa_V14 = _false;
__aa_V15 = _false;
__aa_V16 = _false;
__aa_V17 = _false;
__aa_V18 = _false;
__aa_V19 = _false;
__aa_V20 = _false;
}

static int  __SCRUN__();
static void __SCRESET__();

#define __KIND 0
#define __AUX 1
#define __KNOWN 2
#define __DEFAULT_VALUE 3
#define __VALUE 4
#define __ARITY 5
#define __PREDECESSOR_COUNT 6
#define __ACCESS_ARITY  7
#define __ACCESS_COUNT 8
#define __LISTS 9

#define __NET_TYPE__ int
enum {__STANDARD, __SELECTINC, __RETURN, __SINGLE, __SIGTRACE, __ACTION, __TEST, __REG, __HALT} __NET_KIND__;

#define  __MODULE_NAME__ "aa"

/* THE TABLE OF NETS */

static __NET_TYPE__ __aa_nets [] = {
/* Prr_Start__I_*/
__TEST,1,0,1,1,0,0,0,0,0,1,1,1,2,
/* PRESENT_S0_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,239,2,3,240,0,
/* UPDATED_S0_0_*/
__STANDARD,0,0,1,1,1,1,1,1,2,209,211,0,0,
/* TRACE_S0_*/
__SIGTRACE,0,0,1,1,1,1,0,0,0,0,0,
/* EmergencyStopE__I_*/
__TEST,2,0,1,1,0,0,0,0,0,1,5,0,
/* PRESENT_S1_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,341,2,6,342,0,
/* TRACE_S1_*/
__SIGTRACE,1,0,1,1,1,1,0,0,0,0,0,
/* BadInputE__I_*/
__TEST,3,0,1,1,0,0,0,0,0,1,8,0,
/* PRESENT_S2_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,357,2,9,358,0,
/* TRACE_S2_*/
__SIGTRACE,2,0,1,1,1,1,0,0,0,0,0,
/* BadinputE__I_*/
__TEST,4,0,1,1,0,0,0,0,0,1,11,0,
/* PRESENT_S3_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,373,2,12,374,0,
/* TRACE_S3_*/
__SIGTRACE,3,0,1,1,1,1,0,0,0,0,0,
/* LooseCommE__I_*/
__TEST,5,0,1,1,0,0,0,0,0,1,14,0,
/* PRESENT_S4_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,365,2,15,366,0,
/* TRACE_S4_*/
__SIGTRACE,4,0,1,1,1,1,0,0,0,0,0,
/* JoystickDriving_Start__I_*/
__TEST,6,0,1,1,0,0,0,0,0,1,17,1,18,
/* PRESENT_S5_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,231,2,19,232,0,
/* UPDATED_S5_0_*/
__STANDARD,0,0,1,1,1,1,1,1,8,264,277,279,284,325,380,413,415,0,0,
/* TRACE_S5_*/
__SIGTRACE,5,0,1,1,1,1,0,0,0,0,0,
/* ActivateOK_Cycab__I_*/
__TEST,7,0,1,1,0,0,0,0,0,0,0,
/* TRACE_S6_*/
__SIGTRACE,6,0,1,1,1,1,0,0,0,0,0,
/* KillOK_Cycab__I_*/
__TEST,8,0,1,1,0,0,0,0,0,0,0,
/* TRACE_S7_*/
__SIGTRACE,7,0,1,1,1,1,0,0,0,0,0,
/* ReadyToStop_Cycab__I_*/
__TEST,9,0,1,1,0,0,0,0,0,0,0,
/* TRACE_S8_*/
__SIGTRACE,8,0,1,1,1,1,0,0,0,0,0,
/* CmdStopOK_Cycab__I_*/
__TEST,10,0,1,1,0,0,0,0,0,1,27,0,
/* PRESENT_S9_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,470,2,28,471,0,
/* TRACE_S9_*/
__SIGTRACE,9,0,1,1,1,1,0,0,0,0,0,
/* DirOverE__I_*/
__TEST,11,0,1,1,0,0,0,0,0,1,30,0,
/* PRESENT_S10_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,333,2,31,334,0,
/* TRACE_S10_*/
__SIGTRACE,10,0,1,1,1,1,0,0,0,0,0,
/* InitOK_Cycab__I_*/
__TEST,12,0,1,1,0,0,0,0,0,1,33,0,
/* PRESENT_S11_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,259,2,34,260,0,
/* TRACE_S11_*/
__SIGTRACE,11,0,1,1,1,1,0,0,0,0,0,
/* PbMotorE__I_*/
__TEST,13,0,1,1,0,0,0,0,0,1,36,0,
/* PRESENT_S12_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,349,2,37,350,0,
/* TRACE_S12_*/
__SIGTRACE,12,0,1,1,1,1,0,0,0,0,0,
/* USERSTART_JoystickDriving__I_*/
__TEST,14,0,1,1,0,0,0,0,0,0,0,
/* TRACE_S13_*/
__SIGTRACE,13,0,1,1,1,1,0,0,0,0,0,
/* T2_JoystickDriving__I_*/
__TEST,15,0,1,1,0,0,0,0,0,1,41,0,
/* PRESENT_S14_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,1,42,0,
/* TRACE_S14_*/
__SIGTRACE,14,0,1,1,1,1,0,0,0,0,0,
/* ROBOT_FAIL__I_*/
__TEST,16,0,1,1,0,0,0,0,0,1,44,0,
/* PRESENT_S15_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,199,2,45,200,0,
/* TRACE_S15_*/
__SIGTRACE,15,0,1,1,1,1,0,0,0,0,0,
/* SOFT_FAIL__I_*/
__TEST,17,0,1,1,0,0,0,0,0,1,47,0,
/* PRESENT_S16_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,201,2,48,202,0,
/* TRACE_S16_*/
__SIGTRACE,16,0,1,1,1,1,0,0,0,0,0,
/* SENSOR_FAIL__I_*/
__TEST,18,0,1,1,0,0,0,0,0,1,50,0,
/* PRESENT_S17_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,203,2,51,204,0,
/* TRACE_S17_*/
__SIGTRACE,17,0,1,1,1,1,0,0,0,0,0,
/* CPU_OVERLOAD__I_*/
__TEST,19,0,1,1,0,0,0,0,0,1,53,0,
/* PRESENT_S18_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,205,2,54,206,0,
/* TRACE_S18_*/
__SIGTRACE,18,0,1,1,1,1,0,0,0,0,0,
/* Activate__O_*/
__ACTION,20,0,1,1,1,1,0,0,0,0,0,
/* TRACE_S19_*/
__SIGTRACE,19,0,1,1,1,1,0,0,0,1,55,0,
/* Abort_ProcJoystickDriving__O_*/
__ACTION,21,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S20_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,59,0,
/* TRACE_S20_*/
__SIGTRACE,20,0,1,1,1,1,0,0,0,1,57,0,
/* GoodEnd_ProcJoystickDriving__O_*/
__ACTION,22,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S21_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,62,0,
/* TRACE_S21_*/
__SIGTRACE,21,0,1,1,1,1,0,0,0,1,60,0,
/* STARTED_ProcJoystickDriving__O_*/
__ACTION,23,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S22_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,65,0,
/* TRACE_S22_*/
__SIGTRACE,22,0,1,1,1,1,0,0,0,1,63,0,
/* ActivateJoystickDriving_Cycab__O_*/
__ACTION,24,0,1,1,1,1,1,1,0,0,0,
/* PRESENT_S23_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,69,0,
/* UPDATED_S23_0_*/
__STANDARD,0,0,1,1,1,1,1,1,0,0,1,66,
/* TRACE_S23_*/
__SIGTRACE,23,0,1,1,1,1,0,0,0,1,66,0,
/* JoystickDrivingTransite__O_*/
__ACTION,25,0,1,1,1,1,1,1,0,0,0,
/* PRESENT_S24_0_*/
__SINGLE,24,0,1,1,3,3,0,0,0,1,73,0,
/* UPDATED_S24_0_*/
__STANDARD,0,0,1,1,1,1,3,3,0,0,1,70,
/* TRACE_S24_*/
__SIGTRACE,24,0,1,1,1,1,0,0,0,1,70,0,
/* Abort_JoystickDriving__O_*/
__ACTION,26,0,1,1,1,1,1,1,0,0,0,
/* PRESENT_S25_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,77,0,
/* UPDATED_S25_0_*/
__STANDARD,0,0,1,1,1,1,1,1,0,0,1,74,
/* TRACE_S25_*/
__SIGTRACE,25,0,1,1,1,1,0,0,0,1,74,0,
/* STARTED_JoystickDriving__O_*/
__ACTION,27,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S26_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,80,0,
/* TRACE_S26_*/
__SIGTRACE,26,0,1,1,1,1,0,0,0,1,78,0,
/* GoodEnd_JoystickDriving__O_*/
__ACTION,28,0,1,1,1,1,0,0,0,0,0,
/* TRACE_S27_*/
__SIGTRACE,27,0,1,1,1,1,0,0,0,1,81,0,
/* EndKill__O_*/
__ACTION,29,0,1,1,1,1,1,1,0,0,0,
/* PRESENT_S28_0_*/
__SINGLE,28,0,1,1,2,2,0,0,0,1,86,0,
/* UPDATED_S28_0_*/
__STANDARD,0,0,1,1,1,1,2,2,0,0,1,83,
/* TRACE_S28_*/
__SIGTRACE,28,0,1,1,1,1,0,0,0,1,83,0,
/* FinBF__O_*/
__ACTION,30,0,1,1,1,1,1,1,0,0,0,
/* PRESENT_S29_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,90,0,
/* UPDATED_S29_0_*/
__STANDARD,0,0,1,1,1,1,1,1,0,0,1,87,
/* TRACE_S29_*/
__SIGTRACE,29,0,1,1,1,1,0,0,0,1,87,0,
/* FinT3__O_*/
__ACTION,31,0,1,1,1,1,1,1,0,0,0,
/* PRESENT_S30_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,94,0,
/* UPDATED_S30_0_*/
__STANDARD,0,0,1,1,1,1,1,1,0,0,1,91,
/* TRACE_S30_*/
__SIGTRACE,30,0,1,1,1,1,0,0,0,1,91,0,
/* GoodEndRPr__O_*/
__ACTION,32,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S31_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,97,0,
/* TRACE_S31_*/
__SIGTRACE,31,0,1,1,1,1,0,0,0,1,95,0,
/* T3RPr__O_*/
__ACTION,33,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S32_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,100,0,
/* TRACE_S32_*/
__SIGTRACE,32,0,1,1,1,1,0,0,0,1,98,0,
/* PRESENT_S33_0_*/
__STANDARD,0,0,1,1,1,1,0,0,3,262,282,323,4,102,263,283,324,0,
/* TRACE_S33_*/
__SIGTRACE,33,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S34_0_*/
__STANDARD,0,0,1,1,1,1,0,0,2,394,396,3,104,395,397,0,
/* TRACE_S34_*/
__SIGTRACE,34,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S35_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,425,2,106,426,0,
/* TRACE_S35_*/
__SIGTRACE,35,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S36_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,195,2,108,196,0,
/* TRACE_S36_*/
__SIGTRACE,36,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S37_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,197,2,110,198,0,
/* TRACE_S37_*/
__SIGTRACE,37,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S38_0_*/
__STANDARD,0,0,1,1,1,1,0,0,2,444,460,3,112,445,461,0,
/* TRACE_S38_*/
__SIGTRACE,38,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S39_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,427,2,114,428,0,
/* TRACE_S39_*/
__SIGTRACE,39,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S40_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,117,0,
/* UPDATED_S40_0_*/
__STANDARD,0,0,1,1,1,1,1,1,2,207,462,0,0,
/* TRACE_S40_*/
__SIGTRACE,40,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S41_0_*/
__STANDARD,0,0,1,1,3,3,0,0,1,452,2,119,453,0,
/* TRACE_S41_*/
__SIGTRACE,41,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S42_0_*/
__STANDARD,0,0,1,1,2,2,0,0,2,272,274,3,121,273,275,0,
/* TRACE_S42_*/
__SIGTRACE,42,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S43_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,123,0,
/* TRACE_S43_*/
__SIGTRACE,43,0,1,1,1,1,0,0,0,0,0,
/* TRACE_S44_*/
__SIGTRACE,44,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S45_0_*/
__STANDARD,0,0,1,1,1,1,0,0,2,247,250,3,126,248,251,0,
/* TRACE_S45_*/
__SIGTRACE,45,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S46_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,423,2,128,424,0,
/* TRACE_S46_*/
__SIGTRACE,46,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S47_0_*/
__STANDARD,0,0,1,1,1,1,0,0,1,436,2,130,437,0,
/* TRACE_S47_*/
__SIGTRACE,47,0,1,1,1,1,0,0,0,0,0,
/* TRACE_S48_*/
__SIGTRACE,48,0,1,1,1,1,0,0,0,0,0,
/* TRACE_S49_*/
__SIGTRACE,49,0,1,1,1,1,0,0,0,0,0,
/* TRACE_S50_*/
__SIGTRACE,50,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S51_0_*/
__STANDARD,0,0,1,1,5,5,0,0,0,1,135,0,
/* TRACE_S51_*/
__SIGTRACE,51,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S52_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,137,0,
/* TRACE_S52_*/
__SIGTRACE,52,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S53_0_*/
__STANDARD,0,0,1,1,5,5,0,0,0,1,139,0,
/* TRACE_S53_*/
__SIGTRACE,53,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S54_0_*/
__STANDARD,0,0,1,1,3,3,0,0,0,1,141,0,
/* TRACE_S54_*/
__SIGTRACE,54,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S55_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,143,0,
/* TRACE_S55_*/
__SIGTRACE,55,0,1,1,1,1,0,0,0,0,0,
/* TRACE_S56_*/
__SIGTRACE,56,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S57_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,146,0,
/* TRACE_S57_*/
__SIGTRACE,57,0,1,1,1,1,0,0,0,0,0,
/* TRACE_S58_*/
__SIGTRACE,58,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S59_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,149,0,
/* TRACE_S59_*/
__SIGTRACE,59,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S60_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,151,0,
/* TRACE_S60_*/
__SIGTRACE,60,0,1,1,1,1,0,0,0,0,0,
/* PRESENT_S61_0_*/
__STANDARD,0,0,1,1,1,1,0,0,0,1,153,0,
/* TRACE_S61_*/
__SIGTRACE,61,0,1,1,1,1,0,0,0,0,0,
/* BOOT_REGISTER_*/
__REG,0,0,1,1,1,1,0,0,0,1,163,0,
/* ROOT_RESUME_*/
__STANDARD,0,0,1,1,1,1,0,0,20,194,230,238,246,261,281,322,332,340,348,356,364,372,393,422,435,443,451,459,469,1,375,0,
/* ROOT_KILL_*/
__STANDARD,0,0,1,1,1,1,0,0,0,2,162,162,0,
/* ROOT_STAY_*/
__STANDARD,0,0,1,1,1,1,0,0,0,21,189,225,233,241,253,266,317,327,335,343,351,359,367,375,388,417,430,438,446,454,464,0,
/* ROOT_RETURN_*/
__RETURN,0,0,1,1,1,1,0,0,0,0,0,
/* ROOT_HALTING_*/
__STANDARD,0,0,1,1,1,1,0,0,0,0,0,
/* DEAD_2_B0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,0,0,
/* PARALLEL_2_UNION_K0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,1,158,0,
/* PARALLEL_2_KILL_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,2,188,188,0,
/* ACT_33_0_0_*/
__ACTION,47,0,1,1,1,1,0,0,0,1,164,0,
/* ACT_34_0_0_*/
__ACTION,48,0,1,1,1,1,0,0,2,396,397,5,192,228,236,249,441,0,
/* SELECT_26_*/
__STANDARD,0,0,1,1,5,5,0,0,6,160,166,168,169,171,172,1,160,0,
/* DEAD_26_B0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,173,0,
/* SELECT_26_B1_*/
__SELECTINC,0,0,1,1,4,4,0,0,0,2,165,168,0,
/* DEAD_26_B1_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,174,0,
/* DEAD_26_B2_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,175,0,
/* SELECT_26_B3_*/
__SELECTINC,0,0,1,1,4,4,0,0,0,2,165,171,0,
/* DEAD_26_B3_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,181,0,
/* DEAD_26_B4_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,182,0,
/* PARALLEL_26_MIN_K0_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,1,178,0,
/* PARALLEL_26_MIN_K0_B1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,1,179,0,
/* PARALLEL_26_MIN_K0_B2_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,1,180,0,
/* PARALLEL_26_UNION_K1_0_*/
__STANDARD,0,0,1,1,11,11,0,0,1,177,0,0,
/* PARALLEL_26_CONT_K1_0_*/
__STANDARD,0,0,0,0,6,6,0,0,0,1,159,0,
/* PARALLEL_26_MIN_K1_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,3,177,183,186,0,0,
/* PARALLEL_26_MIN_K1_B1_0_*/
__STANDARD,0,0,1,1,5,5,0,0,3,177,183,186,0,0,
/* PARALLEL_26_MIN_K1_B2_0_*/
__STANDARD,0,0,1,1,2,2,0,0,3,177,183,186,0,0,
/* PARALLEL_26_MIN_K1_B3_0_*/
__STANDARD,0,0,1,1,5,5,0,0,3,177,183,186,0,0,
/* PARALLEL_26_MIN_K1_B4_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,177,1,184,0,
/* PARALLEL_26_CONT_K2_0_*/
__STANDARD,0,0,0,0,6,6,0,0,1,207,2,84,188,0,
/* PARALLEL_26_MIN_K2_B4_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,183,1,187,0,
/* PARALLEL_26_UNION_K3_0_*/
__STANDARD,0,0,1,1,5,5,0,0,1,186,0,0,
/* PARALLEL_26_CONT_K3_0_*/
__STANDARD,0,0,0,0,6,6,0,0,1,211,2,92,188,0,
/* PARALLEL_26_MIN_K3_B4_0_*/
__STANDARD,0,0,1,1,6,6,0,0,1,186,0,0,
/* PARALLEL_26_KILL_0_*/
__STANDARD,0,0,1,1,4,4,0,0,0,14,193,224,224,245,257,270,316,316,387,387,442,450,458,468,0,
/* RESUME_31_*/
__STANDARD,0,0,1,1,2,2,0,0,1,190,0,0,
/* MAINTAIN_31_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,192,0,
/* HALT_REG_31_*/
__HALT,1,0,1,0,1,1,0,0,2,190,194,2,165,172,0,
/* GO_31_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,193,2,176,182,0,
/* TO_REG_31_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,191,0,
/* TO_PRESENT_32_*/
__STANDARD,0,0,0,0,2,2,0,0,2,195,196,0,0,
/* THEN_33_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,183,2,136,184,0,
/* ELSE_33_0_*/
__STANDARD,0,0,0,0,2,2,0,0,2,197,198,0,0,
/* THEN_36_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,3,134,185,187,0,
/* ELSE_36_0_*/
__STANDARD,0,0,0,0,2,2,0,0,2,199,200,0,0,
/* THEN_39_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,3,134,185,187,0,
/* ELSE_39_0_*/
__STANDARD,0,0,0,0,2,2,0,0,2,201,202,0,0,
/* THEN_42_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,3,134,185,187,0,
/* ELSE_42_0_*/
__STANDARD,0,0,0,0,2,2,0,0,2,203,204,0,0,
/* THEN_45_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,3,134,185,187,0,
/* ELSE_45_0_*/
__STANDARD,0,0,0,0,2,2,0,0,2,205,206,0,0,
/* THEN_48_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,3,134,185,187,0,
/* ELSE_48_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,189,0,
/* CONT_53_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,208,0,
/* ACT_35_0_0_*/
__ACTION,34,0,1,1,1,1,0,0,1,209,1,88,1,85,
/* CONT_57_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,210,0,
/* ACT_36_0_0_*/
__ACTION,35,0,1,1,1,1,0,0,0,2,96,161,1,89,
/* CONT_62_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,212,0,
/* ACT_37_0_0_*/
__ACTION,36,0,1,1,1,1,0,0,0,2,99,161,1,93,
/* SELECT_68_*/
__STANDARD,0,0,1,1,2,2,0,0,2,214,215,2,165,166,0,
/* DEAD_68_B0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,218,0,
/* DEAD_68_B1_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,219,0,
/* PARALLEL_68_UNION_K0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,217,0,0,
/* PARALLEL_68_CONT_K0_0_*/
__STANDARD,0,0,0,0,3,3,0,0,0,3,103,122,173,0,
/* PARALLEL_68_MIN_K0_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,217,1,222,0,
/* PARALLEL_68_MIN_K0_B1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,217,1,223,0,
/* PARALLEL_68_UNION_K1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,221,0,0,
/* PARALLEL_68_CONT_K1_0_*/
__STANDARD,0,0,0,0,3,3,0,0,0,2,176,178,0,
/* PARALLEL_68_MIN_K1_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,221,0,0,
/* PARALLEL_68_MIN_K1_B1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,221,0,0,
/* PARALLEL_68_KILL_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,2,229,237,0,
/* RESUME_69_*/
__STANDARD,0,0,1,1,2,2,0,0,1,226,0,0,
/* MAINTAIN_69_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,228,0,
/* HALT_REG_69_*/
__HALT,2,0,1,0,1,1,0,0,2,226,230,2,213,214,0,
/* GO_69_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,229,2,220,222,0,
/* TO_REG_69_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,227,0,
/* TO_PRESENT_70_*/
__STANDARD,0,0,0,0,2,2,0,0,2,231,232,0,0,
/* THEN_71_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,2,216,218,0,
/* ELSE_71_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,225,0,
/* RESUME_72_*/
__STANDARD,0,0,1,1,2,2,0,0,1,234,0,0,
/* MAINTAIN_72_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,236,0,
/* HALT_REG_72_*/
__HALT,3,0,1,0,1,1,0,0,2,234,238,2,213,215,0,
/* GO_72_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,237,2,220,223,0,
/* TO_REG_72_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,235,0,
/* TO_PRESENT_73_*/
__STANDARD,0,0,0,0,2,2,0,0,2,239,240,0,0,
/* THEN_74_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,2,216,219,0,
/* ELSE_74_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,233,0,
/* RESUME_79_*/
__STANDARD,0,0,1,1,2,2,0,0,1,242,0,0,
/* MAINTAIN_79_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,244,0,
/* HALT_REG_79_*/
__HALT,4,0,1,0,1,1,0,0,2,242,246,1,167,0,
/* GO_79_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,245,2,176,179,0,
/* TO_REG_79_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,243,0,
/* TO_PRESENT_80_*/
__STANDARD,0,0,0,0,2,2,0,0,2,247,248,0,0,
/* THEN_81_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,252,0,
/* ELSE_81_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,241,0,
/* GO_82_0_*/
__STANDARD,0,0,1,1,2,2,0,0,2,250,251,0,0,
/* THEN_82_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,252,0,
/* ELSE_82_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,244,0,
/* GO_83_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,2,79,256,0,
/* RESUME_85_*/
__STANDARD,0,0,1,1,2,2,0,0,1,254,0,0,
/* MAINTAIN_85_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,256,0,
/* HALT_REG_85_*/
__HALT,5,0,1,0,1,1,0,0,3,254,258,261,1,167,0,
/* GO_85_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,257,2,176,179,0,
/* TO_REG_85_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,255,0,
/* TO_PRESENT_86_*/
__STANDARD,0,0,0,0,2,2,0,0,2,259,260,0,0,
/* THEN_87_0_*/
__STANDARD,0,0,0,0,2,2,0,0,2,274,275,1,111,0,
/* ELSE_87_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,253,0,
/* TO_PRESENT_88_*/
__STANDARD,0,0,0,0,2,2,0,0,2,262,263,0,0,
/* THEN_89_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,264,1,75,0,
/* ELSE_89_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,258,0,0,
/* CONT_91_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,265,0,
/* ACT_38_0_0_*/
__ACTION,37,0,1,1,1,1,0,0,0,2,140,382,1,76,
/* RESUME_96_*/
__STANDARD,0,0,1,1,2,2,0,0,1,267,0,0,
/* MAINTAIN_96_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,269,0,
/* HALT_REG_96_*/
__HALT,6,0,1,0,1,1,0,0,3,267,271,281,1,167,0,
/* GO_96_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,270,2,176,179,0,
/* TO_REG_96_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,268,0,
/* TO_PRESENT_97_*/
__STANDARD,0,0,0,0,2,2,0,0,2,272,273,0,0,
/* THEN_98_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,276,0,
/* ELSE_98_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,266,0,
/* THEN_99_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,276,0,
/* ELSE_99_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,269,0,
/* GO_100_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,277,1,67,0,
/* CONT_101_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,278,0,
/* ACT_39_0_0_*/
__ACTION,38,0,1,1,1,1,0,0,1,279,1,115,1,68,
/* CONT_105_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,280,0,
/* ACT_40_0_0_*/
__ACTION,39,0,1,1,1,1,0,0,0,8,320,330,338,346,354,362,370,378,1,116,
/* TO_PRESENT_108_*/
__STANDARD,0,0,0,0,2,2,0,0,2,282,283,0,0,
/* THEN_109_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,284,2,71,118,0,
/* ELSE_109_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,271,0,0,
/* CONT_112_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,285,0,
/* ACT_41_0_0_*/
__ACTION,40,0,1,1,1,1,0,0,0,2,140,382,1,72,
/* SELECT_119_*/
__STANDARD,0,0,1,1,8,8,0,0,8,287,288,289,290,291,292,293,294,1,167,0,
/* DEAD_119_B0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,297,0,
/* DEAD_119_B1_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,298,0,
/* DEAD_119_B2_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,299,0,
/* DEAD_119_B3_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,300,0,
/* DEAD_119_B4_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,301,0,
/* DEAD_119_B5_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,302,0,
/* DEAD_119_B6_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,303,0,
/* DEAD_119_B7_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,304,0,
/* PARALLEL_119_UNION_K1_0_*/
__STANDARD,0,0,1,1,8,8,0,0,1,296,0,0,
/* PARALLEL_119_CONT_K1_0_*/
__STANDARD,0,0,0,0,9,9,0,0,0,2,176,179,0,
/* PARALLEL_119_MIN_K1_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,2,296,305,1,308,0,
/* PARALLEL_119_MIN_K1_B1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,296,1,306,0,
/* PARALLEL_119_MIN_K1_B2_0_*/
__STANDARD,0,0,1,1,2,2,0,0,3,296,305,307,1,311,0,
/* PARALLEL_119_MIN_K1_B3_0_*/
__STANDARD,0,0,1,1,2,2,0,0,3,296,305,307,1,312,0,
/* PARALLEL_119_MIN_K1_B4_0_*/
__STANDARD,0,0,1,1,2,2,0,0,3,296,305,307,1,313,0,
/* PARALLEL_119_MIN_K1_B5_0_*/
__STANDARD,0,0,1,1,2,2,0,0,3,296,305,307,1,314,0,
/* PARALLEL_119_MIN_K1_B6_0_*/
__STANDARD,0,0,1,1,2,2,0,0,3,296,305,307,1,315,0,
/* PARALLEL_119_MIN_K1_B7_0_*/
__STANDARD,0,0,1,1,2,2,0,0,4,296,305,307,310,0,0,
/* PARALLEL_119_CONT_K3_0_*/
__STANDARD,0,0,0,0,9,9,0,0,1,380,4,41,71,118,316,0,
/* PARALLEL_119_MIN_K3_B1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,3,305,307,310,0,0,
/* PARALLEL_119_CONT_K4_0_*/
__STANDARD,0,0,0,0,9,9,0,0,0,2,316,382,0,
/* PARALLEL_119_MIN_K4_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,2,307,310,0,0,
/* PARALLEL_119_UNION_K5_0_*/
__STANDARD,0,0,1,1,5,5,0,0,1,310,0,0,
/* PARALLEL_119_CONT_K5_0_*/
__STANDARD,0,0,0,0,9,9,0,0,0,3,113,174,316,0,
/* PARALLEL_119_MIN_K5_B2_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,310,0,0,
/* PARALLEL_119_MIN_K5_B3_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,310,0,0,
/* PARALLEL_119_MIN_K5_B4_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,310,0,0,
/* PARALLEL_119_MIN_K5_B5_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,310,0,0,
/* PARALLEL_119_MIN_K5_B6_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,310,0,0,
/* PARALLEL_119_KILL_0_*/
__STANDARD,0,0,1,1,5,5,0,0,0,8,321,331,339,347,355,363,371,379,0,
/* RESUME_120_*/
__STANDARD,0,0,1,1,2,2,0,0,1,318,0,0,
/* MAINTAIN_120_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,320,0,
/* HALT_REG_120_*/
__HALT,7,0,1,0,1,1,0,0,2,318,322,2,286,287,0,
/* GO_120_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,321,2,295,297,0,
/* TO_REG_120_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,319,0,
/* TO_PRESENT_121_*/
__STANDARD,0,0,0,0,2,2,0,0,2,323,324,0,0,
/* THEN_122_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,325,2,71,118,0,
/* ELSE_122_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,317,0,
/* CONT_125_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,326,0,
/* ACT_42_0_0_*/
__ACTION,41,0,1,1,1,1,0,0,1,307,2,140,308,1,72,
/* RESUME_130_*/
__STANDARD,0,0,1,1,2,2,0,0,1,328,0,0,
/* MAINTAIN_130_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,330,0,
/* HALT_REG_130_*/
__HALT,8,0,1,0,1,1,0,0,2,328,332,2,286,288,0,
/* GO_130_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,331,2,295,298,0,
/* TO_REG_130_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,329,0,
/* TO_PRESENT_131_*/
__STANDARD,0,0,0,0,2,2,0,0,2,333,334,0,0,
/* THEN_132_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,305,3,105,142,306,0,
/* ELSE_132_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,327,0,
/* RESUME_136_*/
__STANDARD,0,0,1,1,2,2,0,0,1,336,0,0,
/* MAINTAIN_136_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,338,0,
/* HALT_REG_136_*/
__HALT,9,0,1,0,1,1,0,0,2,336,340,2,286,289,0,
/* GO_136_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,339,2,295,299,0,
/* TO_REG_136_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,337,0,
/* TO_PRESENT_137_*/
__STANDARD,0,0,0,0,2,2,0,0,2,341,342,0,0,
/* THEN_138_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,3,138,309,311,0,
/* ELSE_138_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,335,0,
/* RESUME_141_*/
__STANDARD,0,0,1,1,2,2,0,0,1,344,0,0,
/* MAINTAIN_141_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,346,0,
/* HALT_REG_141_*/
__HALT,10,0,1,0,1,1,0,0,2,344,348,2,286,290,0,
/* GO_141_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,347,2,295,300,0,
/* TO_REG_141_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,345,0,
/* TO_PRESENT_142_*/
__STANDARD,0,0,0,0,2,2,0,0,2,349,350,0,0,
/* THEN_143_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,3,138,309,312,0,
/* ELSE_143_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,343,0,
/* RESUME_146_*/
__STANDARD,0,0,1,1,2,2,0,0,1,352,0,0,
/* MAINTAIN_146_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,354,0,
/* HALT_REG_146_*/
__HALT,11,0,1,0,1,1,0,0,2,352,356,2,286,291,0,
/* GO_146_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,355,2,295,301,0,
/* TO_REG_146_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,353,0,
/* TO_PRESENT_147_*/
__STANDARD,0,0,0,0,2,2,0,0,2,357,358,0,0,
/* THEN_148_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,3,138,309,313,0,
/* ELSE_148_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,351,0,
/* RESUME_151_*/
__STANDARD,0,0,1,1,2,2,0,0,1,360,0,0,
/* MAINTAIN_151_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,362,0,
/* HALT_REG_151_*/
__HALT,12,0,1,0,1,1,0,0,2,360,364,2,286,292,0,
/* GO_151_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,363,2,295,302,0,
/* TO_REG_151_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,361,0,
/* TO_PRESENT_152_*/
__STANDARD,0,0,0,0,2,2,0,0,2,365,366,0,0,
/* THEN_153_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,3,138,309,314,0,
/* ELSE_153_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,359,0,
/* RESUME_156_*/
__STANDARD,0,0,1,1,2,2,0,0,1,368,0,0,
/* MAINTAIN_156_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,370,0,
/* HALT_REG_156_*/
__HALT,13,0,1,0,1,1,0,0,2,368,372,2,286,293,0,
/* GO_156_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,371,2,295,303,0,
/* TO_REG_156_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,369,0,
/* TO_PRESENT_157_*/
__STANDARD,0,0,0,0,2,2,0,0,2,373,374,0,0,
/* THEN_158_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,3,138,309,315,0,
/* ELSE_158_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,367,0,
/* RESUME_161_*/
__STANDARD,0,0,1,1,2,2,0,0,1,376,0,0,
/* MAINTAIN_161_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,378,0,
/* HALT_REG_161_*/
__HALT,14,0,1,0,1,1,0,0,1,376,2,286,294,0,
/* GO_161_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,379,2,295,304,0,
/* TO_REG_161_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,377,0,
/* CONT_175_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,381,0,
/* ACT_44_0_0_*/
__ACTION,43,0,1,1,1,1,0,0,0,1,382,1,72,
/* GO_178_0_*/
__STANDARD,0,0,1,1,4,4,0,0,0,1,249,0,
/* SELECT_181_B0_*/
__SELECTINC,0,0,1,1,2,2,0,0,1,384,3,165,169,384,0,
/* DEAD_181_B0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,0,0,
/* PARALLEL_181_UNION_K0_0_*/
__STANDARD,0,0,1,1,3,3,0,0,0,1,175,0,
/* PARALLEL_181_UNION_K1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,2,176,180,0,
/* PARALLEL_181_KILL_0_*/
__STANDARD,0,0,1,1,2,2,0,0,0,3,392,412,412,0,
/* RESUME_184_*/
__STANDARD,0,0,1,1,2,2,0,0,1,389,0,0,
/* MAINTAIN_184_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,391,0,
/* HALT_REG_184_*/
__HALT,15,0,1,0,1,1,0,0,2,389,393,1,383,0,
/* GO_184_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,392,1,386,0,
/* TO_REG_184_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,390,0,
/* TO_PRESENT_185_*/
__STANDARD,0,0,0,0,2,2,0,0,2,394,395,0,0,
/* THEN_186_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,398,0,
/* ELSE_186_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,388,0,
/* THEN_187_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,398,0,
/* ELSE_187_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,391,0,
/* GO_188_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,413,2,64,433,0,
/* SELECT_193_*/
__STANDARD,0,0,1,1,2,2,0,0,2,400,401,1,383,0,
/* DEAD_193_B0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,404,0,
/* DEAD_193_B1_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,405,0,
/* PARALLEL_193_UNION_K1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,403,0,0,
/* PARALLEL_193_CONT_K1_0_*/
__STANDARD,0,0,0,0,3,3,0,0,0,1,386,0,
/* PARALLEL_193_MIN_K1_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,403,1,407,0,
/* PARALLEL_193_MIN_K1_B1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,3,403,406,408,1,411,0,
/* PARALLEL_193_CONT_K2_0_*/
__STANDARD,0,0,0,0,3,3,0,0,0,4,61,107,385,412,0,
/* PARALLEL_193_MIN_K2_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,406,1,409,0,
/* PARALLEL_193_CONT_K3_0_*/
__STANDARD,0,0,0,0,3,3,0,0,0,3,109,385,412,0,
/* PARALLEL_193_MIN_K3_B0_0_*/
__STANDARD,0,0,1,1,2,2,0,0,2,408,410,0,0,
/* PARALLEL_193_CONT_K4_0_*/
__STANDARD,0,0,0,0,3,3,0,0,0,2,385,412,0,
/* PARALLEL_193_MIN_K4_B1_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,410,0,0,
/* PARALLEL_193_KILL_0_*/
__STANDARD,0,0,1,1,5,5,0,0,0,2,421,434,0,
/* CONT_194_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,414,0,
/* ACT_45_0_0_*/
__ACTION,45,0,1,1,1,1,0,0,1,415,0,0,
/* CONT_196_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,416,0,
/* ACT_46_0_0_*/
__ACTION,46,0,1,1,1,1,0,0,0,2,125,420,0,
/* RESUME_199_*/
__STANDARD,0,0,1,1,2,2,0,0,1,418,0,0,
/* MAINTAIN_199_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,420,0,
/* HALT_REG_199_*/
__HALT,16,0,1,0,1,1,0,0,2,418,422,2,399,400,0,
/* GO_199_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,421,2,402,404,0,
/* TO_REG_199_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,419,0,
/* TO_PRESENT_200_*/
__STANDARD,0,0,0,0,2,2,0,0,2,423,424,0,0,
/* THEN_201_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,429,0,
/* ELSE_201_0_*/
__STANDARD,0,0,0,0,2,2,0,0,2,425,426,0,0,
/* THEN_202_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,2,145,429,0,
/* ELSE_202_0_*/
__STANDARD,0,0,0,0,2,2,0,0,2,427,428,0,0,
/* THEN_204_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,408,2,150,409,0,
/* ELSE_204_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,417,0,
/* GO_207_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,406,2,152,407,0,
/* RESUME_209_*/
__STANDARD,0,0,1,1,2,2,0,0,1,431,0,0,
/* MAINTAIN_209_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,433,0,
/* HALT_REG_209_*/
__HALT,17,0,1,0,1,1,0,0,2,431,435,2,399,401,0,
/* GO_209_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,434,2,402,405,0,
/* TO_REG_209_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,432,0,
/* TO_PRESENT_210_*/
__STANDARD,0,0,0,0,2,2,0,0,2,436,437,0,0,
/* THEN_211_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,410,4,58,101,148,411,0,
/* ELSE_211_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,430,0,
/* RESUME_222_*/
__STANDARD,0,0,1,1,2,2,0,0,1,439,0,0,
/* MAINTAIN_222_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,441,0,
/* HALT_REG_222_*/
__HALT,18,0,1,0,1,1,0,0,2,439,443,1,170,0,
/* GO_222_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,442,2,176,181,0,
/* TO_REG_222_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,440,0,
/* TO_PRESENT_223_*/
__STANDARD,0,0,0,0,2,2,0,0,2,444,445,0,0,
/* THEN_224_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,2,120,449,0,
/* ELSE_224_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,438,0,
/* RESUME_226_*/
__STANDARD,0,0,1,1,2,2,0,0,1,447,0,0,
/* MAINTAIN_226_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,449,0,
/* HALT_REG_226_*/
__HALT,19,0,1,0,1,1,0,0,2,447,451,1,170,0,
/* GO_226_0_*/
__STANDARD,0,0,1,1,3,3,0,0,1,450,2,176,181,0,
/* TO_REG_226_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,448,0,
/* TO_PRESENT_227_*/
__STANDARD,0,0,0,0,2,2,0,0,2,452,453,0,0,
/* THEN_228_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,457,0,
/* ELSE_228_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,446,0,
/* RESUME_229_*/
__STANDARD,0,0,1,1,2,2,0,0,1,455,0,0,
/* MAINTAIN_229_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,457,0,
/* HALT_REG_229_*/
__HALT,20,0,1,0,1,1,0,0,2,455,459,1,170,0,
/* GO_229_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,458,2,176,181,0,
/* TO_REG_229_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,456,0,
/* TO_PRESENT_230_*/
__STANDARD,0,0,0,0,2,2,0,0,2,460,461,0,0,
/* THEN_231_0_*/
__STANDARD,0,0,0,0,2,2,0,0,1,462,1,84,0,
/* ELSE_231_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,454,0,
/* CONT_233_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,463,0,
/* ACT_47_0_0_*/
__ACTION,44,0,1,1,1,1,0,0,0,1,467,1,85,
/* RESUME_236_*/
__STANDARD,0,0,1,1,2,2,0,0,1,465,0,0,
/* MAINTAIN_236_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,467,0,
/* HALT_REG_236_*/
__HALT,21,0,1,0,1,1,0,0,2,465,469,1,170,0,
/* GO_236_0_*/
__STANDARD,0,0,1,1,2,2,0,0,1,468,2,176,181,0,
/* TO_REG_236_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,466,0,
/* TO_PRESENT_237_*/
__STANDARD,0,0,0,0,2,2,0,0,2,470,471,0,0,
/* THEN_238_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,2,120,449,0,
/* ELSE_238_0_*/
__STANDARD,0,0,0,0,2,2,0,0,0,1,464,0,
/* __FALSE__*/
__STANDARD,0,0,0,0,0,0,0,0,0,17,21,23,25,39,56,82,124,127,129,131,132,133,144,147,154,156,157,0,
/* __TRUE__*/
__STANDARD,0,0,1,1,0,0,0,0,0,10,2,18,68,72,76,85,89,93,116,155,0};

/* THE NET ARRAY */

static __NET_TYPE__* __aa_net_array[] = {
__aa_nets+0,__aa_nets+14,__aa_nets+29,__aa_nets+43,__aa_nets+55,__aa_nets+68,__aa_nets+83,__aa_nets+95,__aa_nets+108,__aa_nets+123,__aa_nets+135,__aa_nets+148,__aa_nets+163,__aa_nets+175,__aa_nets+188,__aa_nets+203,__aa_nets+215,__aa_nets+229,__aa_nets+244,__aa_nets+264,__aa_nets+276,__aa_nets+288,__aa_nets+300,__aa_nets+312,__aa_nets+324,__aa_nets+336,__aa_nets+348,__aa_nets+361,__aa_nets+376,__aa_nets+388,__aa_nets+401,__aa_nets+416,__aa_nets+428,__aa_nets+441,__aa_nets+456,__aa_nets+468,__aa_nets+481,__aa_nets+496,__aa_nets+508,__aa_nets+520,__aa_nets+532,__aa_nets+545,__aa_nets+558,__aa_nets+570,__aa_nets+583,__aa_nets+598,__aa_nets+610,__aa_nets+623,__aa_nets+638,__aa_nets+650,__aa_nets+663,__aa_nets+678,__aa_nets+690,__aa_nets+703,__aa_nets+718,__aa_nets+730,__aa_nets+742,__aa_nets+755,__aa_nets+767,__aa_nets+780,__aa_nets+793,__aa_nets+805,__aa_nets+818,__aa_nets+831,__aa_nets+843,__aa_nets+856,__aa_nets+869,__aa_nets+881,__aa_nets+894,__aa_nets+907,__aa_nets+920,__aa_nets+932,__aa_nets+945,__aa_nets+958,__aa_nets+971,__aa_nets+983,__aa_nets+996,__aa_nets+1009,__aa_nets+1022,__aa_nets+1034,__aa_nets+1047,__aa_nets+1060,__aa_nets+1072,__aa_nets+1085,__aa_nets+1097,__aa_nets+1110,__aa_nets+1123,__aa_nets+1136,__aa_nets+1148,__aa_nets+1161,__aa_nets+1174,__aa_nets+1187,__aa_nets+1199,__aa_nets+1212,__aa_nets+1225,__aa_nets+1238,__aa_nets+1250,__aa_nets+1263,__aa_nets+1276,__aa_nets+1288,__aa_nets+1301,__aa_nets+1314,__aa_nets+1333,__aa_nets+1345,__aa_nets+1362,__aa_nets+1374,__aa_nets+1389,__aa_nets+1401,__aa_nets+1416,__aa_nets+1428,__aa_nets+1443,__aa_nets+1455,__aa_nets+1472,__aa_nets+1484,__aa_nets+1499,__aa_nets+1511,__aa_nets+1524,__aa_nets+1538,__aa_nets+1550,__aa_nets+1565,__aa_nets+1577,__aa_nets+1594,__aa_nets+1606,__aa_nets+1619,__aa_nets+1631,__aa_nets+1643,__aa_nets+1660,__aa_nets+1672,__aa_nets+1687,__aa_nets+1699,__aa_nets+1714,__aa_nets+1726,__aa_nets+1738,__aa_nets+1750,__aa_nets+1762,__aa_nets+1775,__aa_nets+1787,__aa_nets+1800,__aa_nets+1812,__aa_nets+1825,__aa_nets+1837,__aa_nets+1850,__aa_nets+1862,__aa_nets+1875,__aa_nets+1887,__aa_nets+1899,__aa_nets+1912,__aa_nets+1924,__aa_nets+1936,__aa_nets+1949,__aa_nets+1961,__aa_nets+1974,__aa_nets+1986,__aa_nets+1999,__aa_nets+2011,__aa_nets+2024,__aa_nets+2057,__aa_nets+2071,__aa_nets+2104,__aa_nets+2116,__aa_nets+2128,__aa_nets+2140,__aa_nets+2153,__aa_nets+2167,__aa_nets+2180,__aa_nets+2199,__aa_nets+2218,__aa_nets+2231,__aa_nets+2245,__aa_nets+2258,__aa_nets+2271,__aa_nets+2285,__aa_nets+2298,__aa_nets+2311,__aa_nets+2324,__aa_nets+2337,__aa_nets+2350,__aa_nets+2363,__aa_nets+2376,__aa_nets+2391,__aa_nets+2406,__aa_nets+2421,__aa_nets+2436,__aa_nets+2450,__aa_nets+2465,__aa_nets+2479,__aa_nets+2492,__aa_nets+2507,__aa_nets+2520,__aa_nets+2546,__aa_nets+2559,__aa_nets+2572,__aa_nets+2588,__aa_nets+2603,__aa_nets+2616,__aa_nets+2630,__aa_nets+2645,__aa_nets+2659,__aa_nets+2674,__aa_nets+2688,__aa_nets+2703,__aa_nets+2717,__aa_nets+2732,__aa_nets+2746,__aa_nets+2761,__aa_nets+2775,__aa_nets+2790,__aa_nets+2803,__aa_nets+2816,__aa_nets+2831,__aa_nets+2844,__aa_nets+2859,__aa_nets+2872,__aa_nets+2887,__aa_nets+2903,__aa_nets+2916,__aa_nets+2929,__aa_nets+2942,__aa_nets+2957,__aa_nets+2971,__aa_nets+2985,__aa_nets+2998,__aa_nets+3012,__aa_nets+3025,__aa_nets+3038,__aa_nets+3052,__aa_nets+3065,__aa_nets+3078,__aa_nets+3094,__aa_nets+3109,__aa_nets+3122,__aa_nets+3136,__aa_nets+3150,__aa_nets+3163,__aa_nets+3176,__aa_nets+3189,__aa_nets+3205,__aa_nets+3220,__aa_nets+3233,__aa_nets+3247,__aa_nets+3261,__aa_nets+3274,__aa_nets+3287,__aa_nets+3300,__aa_nets+3315,__aa_nets+3330,__aa_nets+3343,__aa_nets+3357,__aa_nets+3370,__aa_nets+3383,__aa_nets+3397,__aa_nets+3410,__aa_nets+3423,__aa_nets+3437,__aa_nets+3450,__aa_nets+3463,__aa_nets+3479,__aa_nets+3494,__aa_nets+3507,__aa_nets+3521,__aa_nets+3536,__aa_nets+3549,__aa_nets+3563,__aa_nets+3577,__aa_nets+3590,__aa_nets+3603,__aa_nets+3618,__aa_nets+3631,__aa_nets+3644,__aa_nets+3660,__aa_nets+3675,__aa_nets+3688,__aa_nets+3702,__aa_nets+3715,__aa_nets+3728,__aa_nets+3741,__aa_nets+3754,__aa_nets+3768,__aa_nets+3781,__aa_nets+3796,__aa_nets+3809,__aa_nets+3830,__aa_nets+3844,__aa_nets+3859,__aa_nets+3872,__aa_nets+3885,__aa_nets+3900,__aa_nets+3921,__aa_nets+3934,__aa_nets+3947,__aa_nets+3960,__aa_nets+3973,__aa_nets+3986,__aa_nets+3999,__aa_nets+4012,__aa_nets+4025,__aa_nets+4038,__aa_nets+4052,__aa_nets+4067,__aa_nets+4081,__aa_nets+4097,__aa_nets+4113,__aa_nets+4129,__aa_nets+4145,__aa_nets+4161,__aa_nets+4177,__aa_nets+4194,__aa_nets+4209,__aa_nets+4223,__aa_nets+4237,__aa_nets+4250,__aa_nets+4265,__aa_nets+4278,__aa_nets+4291,__aa_nets+4304,__aa_nets+4317,__aa_nets+4330,__aa_nets+4350,__aa_nets+4363,__aa_nets+4376,__aa_nets+4392,__aa_nets+4407,__aa_nets+4420,__aa_nets+4434,__aa_nets+4449,__aa_nets+4462,__aa_nets+4475,__aa_nets+4491,__aa_nets+4504,__aa_nets+4517,__aa_nets+4533,__aa_nets+4548,__aa_nets+4561,__aa_nets+4575,__aa_nets+4591,__aa_nets+4604,__aa_nets+4617,__aa_nets+4630,__aa_nets+4646,__aa_nets+4661,__aa_nets+4674,__aa_nets+4688,__aa_nets+4703,__aa_nets+4716,__aa_nets+4729,__aa_nets+4742,__aa_nets+4758,__aa_nets+4773,__aa_nets+4786,__aa_nets+4800,__aa_nets+4815,__aa_nets+4828,__aa_nets+4841,__aa_nets+4854,__aa_nets+4870,__aa_nets+4885,__aa_nets+4898,__aa_nets+4912,__aa_nets+4927,__aa_nets+4940,__aa_nets+4953,__aa_nets+4966,__aa_nets+4982,__aa_nets+4997,__aa_nets+5010,__aa_nets+5024,__aa_nets+5039,__aa_nets+5052,__aa_nets+5065,__aa_nets+5078,__aa_nets+5094,__aa_nets+5109,__aa_nets+5122,__aa_nets+5136,__aa_nets+5151,__aa_nets+5164,__aa_nets+5177,__aa_nets+5190,__aa_nets+5205,__aa_nets+5220,__aa_nets+5233,__aa_nets+5246,__aa_nets+5260,__aa_nets+5273,__aa_nets+5289,__aa_nets+5301,__aa_nets+5314,__aa_nets+5328,__aa_nets+5343,__aa_nets+5356,__aa_nets+5369,__aa_nets+5384,__aa_nets+5398,__aa_nets+5411,__aa_nets+5425,__aa_nets+5438,__aa_nets+5451,__aa_nets+5464,__aa_nets+5477,__aa_nets+5492,__aa_nets+5507,__aa_nets+5520,__aa_nets+5533,__aa_nets+5546,__aa_nets+5559,__aa_nets+5573,__aa_nets+5589,__aa_nets+5605,__aa_nets+5619,__aa_nets+5634,__aa_nets+5648,__aa_nets+5662,__aa_nets+5675,__aa_nets+5689,__aa_nets+5702,__aa_nets+5715,__aa_nets+5728,__aa_nets+5742,__aa_nets+5755,__aa_nets+5768,__aa_nets+5784,__aa_nets+5799,__aa_nets+5812,__aa_nets+5826,__aa_nets+5839,__aa_nets+5853,__aa_nets+5867,__aa_nets+5881,__aa_nets+5896,__aa_nets+5909,__aa_nets+5924,__aa_nets+5937,__aa_nets+5950,__aa_nets+5966,__aa_nets+5981,__aa_nets+5994,__aa_nets+6008,__aa_nets+6025,__aa_nets+6038,__aa_nets+6051,__aa_nets+6064,__aa_nets+6079,__aa_nets+6094,__aa_nets+6107,__aa_nets+6121,__aa_nets+6135,__aa_nets+6148,__aa_nets+6161,__aa_nets+6174,__aa_nets+6189,__aa_nets+6204,__aa_nets+6217,__aa_nets+6231,__aa_nets+6244,__aa_nets+6257,__aa_nets+6270,__aa_nets+6283,__aa_nets+6298,__aa_nets+6313,__aa_nets+6326,__aa_nets+6340,__aa_nets+6354,__aa_nets+6367,__aa_nets+6380,__aa_nets+6394,__aa_nets+6407,__aa_nets+6420,__aa_nets+6435,__aa_nets+6450,__aa_nets+6463,__aa_nets+6477,__aa_nets+6491,__aa_nets+6504,__aa_nets+6533,
};
#define __NET_ARRAY__ __aa_net_array

 /* THE QUEUE */

static __NET_TYPE__ __aa_queue[474] = {
0,4,7,10,13,16,20,22,24,26,29,32,35,38,40,43,46,49,52,154,191,227,235,243,255,268,319,329,337,345,353,361,369,377,390,419,432,440,448,456,466,472,473
};
#define __QUEUE__ __aa_queue
#define __NUMBER_OF_NETS__ 474
#define __NUMBER_OF_INITIAL_NETS__ 43
#define __NUMBER_OF_REGISTERS__ 22
#define __NUMBER_OF_HALTS__ 22

int aa () {

__SCRUN__();
__aa__reset_input();
return __aa_net_array[159][__VALUE];
}

/* AUTOMATON RESET */

int aa_reset () {
__SCRESET__();
__aa_net_array[154][__VALUE] = 1;
__aa_net_array[191][__VALUE] = 0;
__aa_net_array[227][__VALUE] = 0;
__aa_net_array[235][__VALUE] = 0;
__aa_net_array[243][__VALUE] = 0;
__aa_net_array[255][__VALUE] = 0;
__aa_net_array[268][__VALUE] = 0;
__aa_net_array[319][__VALUE] = 0;
__aa_net_array[329][__VALUE] = 0;
__aa_net_array[337][__VALUE] = 0;
__aa_net_array[345][__VALUE] = 0;
__aa_net_array[353][__VALUE] = 0;
__aa_net_array[361][__VALUE] = 0;
__aa_net_array[369][__VALUE] = 0;
__aa_net_array[377][__VALUE] = 0;
__aa_net_array[390][__VALUE] = 0;
__aa_net_array[419][__VALUE] = 0;
__aa_net_array[432][__VALUE] = 0;
__aa_net_array[440][__VALUE] = 0;
__aa_net_array[448][__VALUE] = 0;
__aa_net_array[456][__VALUE] = 0;
__aa_net_array[466][__VALUE] = 0;
__aa__reset_input();
return 0;
}
#ifdef __SIMUL_SCRUN
#include <assert.h>
#ifndef TRACE_ACTION
#include <stdio.h>
#endif
#endif
 

#define __KIND 0
#define __AUX 1
#define __KNOWN 2
#define __DEFAULT_VALUE 3
#define __VALUE 4
#define __ARITY 5
#define __PREDECESSOR_COUNT 6
#define __ACCESS_ARITY  7
#define __ACCESS_COUNT 8
#define __LISTS 9

#define __SIMUL_VALUE 0
#define __SIMUL_KNOWN 1

static int __free_queue_position;
#ifdef __SIMUL_SCRUN
static int __TESTRES__[__NUMBER_OF_NETS__];
#endif
static int __REGVAL[__NUMBER_OF_NETS__];
static int __NUMBER_OF_REGS_KNOWN;


static void __SET_VALUE (netNum, value) 
int netNum; 
int value; {
   int* net = __NET_ARRAY__[netNum];
#ifdef __SIMUL_SCRUN
   if (net[__KNOWN] && net[__KIND] == __SINGLE && net[__VALUE] && value) {
       __SINGLE_SIGNAL_EMITTED_TWICE_ERROR__(net[__AUX]);
   }
#endif
   if (net[__KNOWN]) return;
   net[__KNOWN] =  1;
   switch (net[__KIND]) {
      case __REG:
      case __HALT:
         __REGVAL[netNum] = value;
         __NUMBER_OF_REGS_KNOWN++;
#ifdef TRACE_SCRUN
            fprintf(stderr, "Save register %d with value %d\n", netNum, value);
#endif
         break;
      default:
         net[__VALUE] =  value;
         if (! net[__ACCESS_COUNT]) {
            __QUEUE__[__free_queue_position++] = netNum;
#ifdef TRACE_SCRUN
            fprintf(stderr, "Enqueue net %d with value %d\n", netNum, value);
#endif
         }
   }
}

static void __DECR_ARITY (netNum)
int netNum; {
   int* net = __NET_ARRAY__[netNum];
   if (! (--(net[__PREDECESSOR_COUNT]))) {
      __SET_VALUE(netNum, !net[__DEFAULT_VALUE]);
   }
}

static void __DECR_ACCESS_ARITY (netNum) 
int netNum; {
   int* net = __NET_ARRAY__[netNum];
   if (   ! (--(net[__ACCESS_COUNT])) 
       && net[__KNOWN]) {
      __QUEUE__[__free_queue_position++] = netNum;
#ifdef TRACE_SCRUN
      fprintf(stderr, "Enqueue %d by freeing last access\n", netNum);
#endif
   }
}

static void __SCFOLLOW (netNum, value)
int netNum;
int value; {
   int* net = __NET_ARRAY__[netNum];
   int* lists = net + __LISTS;
   int count;
   int i;  /* list index */
   int followerNum;

#ifdef __SIMUL_SCRUN
   __SIMUL_NET_TABLE__[netNum].known = 1;
   __SIMUL_NET_TABLE__[netNum].value = value;
#endif
#ifdef __SIMUL_SCRUN
   if (net[__KIND] == __TEST) {
       /* set __KNOWN for inputs (one has already __KNOWN==1 for other tests
          when reaching this point. This is only useful to print correctly 
          the causality error message.
       */
       net[__KNOWN] = 1;
   }
#endif
   count = lists[0];
   lists++;
   if (value) {
      for (i=0; i<count; i++) {
         followerNum = lists[0];
         lists++;
         if (! __NET_ARRAY__[followerNum][__KNOWN]) {
            __DECR_ARITY(followerNum);
         }
      }
      count = lists[0];
      lists++;
      for (i=0; i<count; i++) {
         followerNum =lists[0];
         lists++;
         __SET_VALUE(followerNum, 
                     __NET_ARRAY__[followerNum][__DEFAULT_VALUE]);
      }
  } else {
      for (i=0; i<count; i++) {
         followerNum = lists[0];
         lists++;
         __SET_VALUE(followerNum,
                     __NET_ARRAY__[followerNum][__DEFAULT_VALUE]);
      }
      count = lists[0];
      lists++;
      for (i=0; i<count; i++) {
         followerNum = lists[0];
         lists++;
         if (! __NET_ARRAY__[followerNum][__KNOWN]) {
            __DECR_ARITY(followerNum);
         }
      }
   }
   count = lists[0];
   lists++;
   for (i=0; i<count; i++) {
      followerNum = lists[0];
      lists++;
      __DECR_ACCESS_ARITY(followerNum);
   }
}

static int __SCRUN__ () {
   int queuePosition;
   int netNum;
   int* net;
   int value;    /* current net value */
   int testres;  /* result of test action */
   __free_queue_position = __NUMBER_OF_INITIAL_NETS__;

#ifdef TRACE_SCRUN
            fprintf(stderr, "\n***************************\n");
#endif

   /* Reset predecessor counts, access counts, and known flags */
   for (netNum=0; netNum< __NUMBER_OF_NETS__; netNum++) {
      net = __NET_ARRAY__[netNum];
      net[__PREDECESSOR_COUNT] = net[__ARITY];
      net[__ACCESS_COUNT] = net[__ACCESS_ARITY];
      net[__KNOWN] = 0;
#ifdef __SIMUL_SCRUN
      __SIMUL_NET_TABLE__[netNum].known = 0;
      __TESTRES__[netNum] = 0;
#endif
   }
   __NUMBER_OF_REGS_KNOWN = 0;

   /* Run main algorithm */
   for (queuePosition=0; 
        queuePosition < __free_queue_position; 
        queuePosition++) {
      netNum = __QUEUE__[queuePosition];
      net = __NET_ARRAY__[netNum];
      value = net[__VALUE];
#ifdef TRACE_SCRUN
      fprintf(stderr,
              "Step %d : processing net %d value %d\n",
              queuePosition, netNum, value);
#endif

      /* Decode kind and perform action if necessary */
      switch (net[__KIND]) {
         case __STANDARD :
         case __SELECTINC :
         case __SINGLE :   /* TO BE CHANGED FOR SINGLE SIGNAL TEST! */
         case __REG :
         case __HALT :
#ifdef __SIMUL_SCRUN
   __SIMUL_NET_TABLE__[netNum].value = value;
#endif
            __SCFOLLOW(netNum, value);
            break;
         case __RETURN :
#ifdef __SIMUL_SCRUN
            if (value) {
               __AppendToList(__HALT_LIST__, net[__AUX]);
            }
#endif
            __SCFOLLOW(netNum, value);
            break;
         case __SIGTRACE :
#ifdef __SIMUL_SCRUN
            if (value) {
               __AppendToList(__EMITTED_LIST__, net[__AUX]);
           }
#endif
            __SCFOLLOW(netNum, value);
            break;
         case __ACTION :
            if (value) {
               __ACT(net[__AUX]);
            }
            __SCFOLLOW(netNum, value);
            break;
         case __TEST :
            if (value) {
               testres = __ACT(net[__AUX]);
#ifdef __SIMUL_SCRUN
               __TESTRES__[netNum] = testres;
#endif
            }
            __SCFOLLOW(netNum, value && testres);
            break;
      }
   }                     
         
   /* check that all nets have been explored */ 
   {
      int seen = queuePosition + __NUMBER_OF_REGS_KNOWN;
      int tosee = __NUMBER_OF_NETS__ + __NUMBER_OF_REGISTERS__;
      if (seen != tosee) {
#ifdef __SIMUL_SCRUN
      __CAUSALITY_ERROR__;
#endif
         return -1;
     }
   }
  
   /* Set registers. The computed values were temporarily stored in
                     the auxiliary __REGVAL  array.
                     All the register nums are initially in the queue */

   for (queuePosition=0; 
        queuePosition < __NUMBER_OF_INITIAL_NETS__;
        queuePosition++) {
      netNum = __QUEUE__[queuePosition];
      net = __NET_ARRAY__[netNum];
      switch (net[__KIND]) {
         case __REG :
         case __HALT :
            net[__VALUE] = __REGVAL[netNum];
#ifdef TRACE_SCRUN
            fprintf(stderr, "Register %d set to %d\n", 
                            netNum, 
                            __REGVAL[netNum]);
#endif
#ifdef __SIMUL_SCRUN
            if (net[__KIND] == __HALT &&net[__VALUE]) {
               __AppendToList(__HALT_LIST__, net[__AUX]);
           }
#endif
            break;
        default:
            break;
        }
   }
   return 0;
}


static void __SCRESET__ () {
#ifdef __SIMUL_SCRUN
   int netNum;
   for (netNum=0; netNum < __NUMBER_OF_NETS__; netNum++) {
      __SIMUL_NET_TABLE__[netNum].known = 1;
      __SIMUL_NET_TABLE__[netNum].value = 0;
   }
#endif
}



