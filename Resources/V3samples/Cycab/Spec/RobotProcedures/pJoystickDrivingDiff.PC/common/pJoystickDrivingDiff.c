/* sscc : C CODE OF SORTED EQUATIONS pJoystickDrivingDiff - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
int (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __pJoystickDrivingDiff_GENERIC_TEST(TEST) return TEST;
typedef (*__pJoystickDrivingDiff_APF)();
extern __pJoystickDrivingDiff_APF *__pJoystickDrivingDiff_PActionArray;
extern (*__pJoystickDrivingDiff_scf[])();
extern (*__pJoystickDrivingDiff_dcf[])();

#include "pJoystickDrivingDiff.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr();
#endif
#endif
#ifndef _JoystickDriving_controler_DEFINED
#ifndef JoystickDriving_controler
extern void JoystickDriving_controler();
#endif
#endif
#ifndef _JoystickDriving_fileparameter_DEFINED
#ifndef JoystickDriving_fileparameter
extern void JoystickDriving_fileparameter();
#endif
#endif
#ifndef _JoystickDriving_parameter_DEFINED
#ifndef JoystickDriving_parameter
extern void JoystickDriving_parameter();
#endif
#endif
#ifndef _JoystickDrivingDiff_controler_DEFINED
#ifndef JoystickDrivingDiff_controler
extern void JoystickDrivingDiff_controler();
#endif
#endif
#ifndef _JoystickDrivingDiff_fileparameter_DEFINED
#ifndef JoystickDrivingDiff_fileparameter
extern void JoystickDrivingDiff_fileparameter();
#endif
#endif
#ifndef _JoystickDrivingDiff_parameter_DEFINED
#ifndef JoystickDrivingDiff_parameter
extern void JoystickDrivingDiff_parameter();
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __pJoystickDrivingDiff_V0;
static boolean __pJoystickDrivingDiff_V1;
static orcStrlPtr __pJoystickDrivingDiff_V2;
static boolean __pJoystickDrivingDiff_V3;
static boolean __pJoystickDrivingDiff_V4;
static boolean __pJoystickDrivingDiff_V5;
static boolean __pJoystickDrivingDiff_V6;
static orcStrlPtr __pJoystickDrivingDiff_V7;
static boolean __pJoystickDrivingDiff_V8;
static boolean __pJoystickDrivingDiff_V9;
static boolean __pJoystickDrivingDiff_V10;
static boolean __pJoystickDrivingDiff_V11;
static boolean __pJoystickDrivingDiff_V12;


/* INPUT FUNCTIONS */

pJoystickDrivingDiff_I_START_pJoystickDrivingDiff () {
__pJoystickDrivingDiff_V0 = _true;
}
pJoystickDrivingDiff_I_Abort_Local_pJoystickDrivingDiff () {
__pJoystickDrivingDiff_V1 = _true;
}
pJoystickDrivingDiff_I_JoystickDriving_Start (__V) orcStrlPtr __V; {
_orcStrlPtr(&__pJoystickDrivingDiff_V2,__V);
__pJoystickDrivingDiff_V3 = _true;
}
pJoystickDrivingDiff_I_USERSTART_JoystickDriving () {
__pJoystickDrivingDiff_V4 = _true;
}
pJoystickDrivingDiff_I_BF_JoystickDriving () {
__pJoystickDrivingDiff_V5 = _true;
}
pJoystickDrivingDiff_I_T3_JoystickDriving () {
__pJoystickDrivingDiff_V6 = _true;
}
pJoystickDrivingDiff_I_JoystickDrivingDiff_Start (__V) orcStrlPtr __V; {
_orcStrlPtr(&__pJoystickDrivingDiff_V7,__V);
__pJoystickDrivingDiff_V8 = _true;
}
pJoystickDrivingDiff_I_USERSTART_JoystickDrivingDiff () {
__pJoystickDrivingDiff_V9 = _true;
}
pJoystickDrivingDiff_I_BF_JoystickDrivingDiff () {
__pJoystickDrivingDiff_V10 = _true;
}
pJoystickDrivingDiff_I_T3_JoystickDrivingDiff () {
__pJoystickDrivingDiff_V11 = _true;
}
pJoystickDrivingDiff_I_T2_DirOverE () {
__pJoystickDrivingDiff_V12 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __pJoystickDrivingDiff_A1 \
__pJoystickDrivingDiff_V0
#define __pJoystickDrivingDiff_A2 \
__pJoystickDrivingDiff_V1
#define __pJoystickDrivingDiff_A3 \
__pJoystickDrivingDiff_V3
#define __pJoystickDrivingDiff_A4 \
__pJoystickDrivingDiff_V4
#define __pJoystickDrivingDiff_A5 \
__pJoystickDrivingDiff_V5
#define __pJoystickDrivingDiff_A6 \
__pJoystickDrivingDiff_V6
#define __pJoystickDrivingDiff_A7 \
__pJoystickDrivingDiff_V8
#define __pJoystickDrivingDiff_A8 \
__pJoystickDrivingDiff_V9
#define __pJoystickDrivingDiff_A9 \
__pJoystickDrivingDiff_V10
#define __pJoystickDrivingDiff_A10 \
__pJoystickDrivingDiff_V11
#define __pJoystickDrivingDiff_A11 \
__pJoystickDrivingDiff_V12

/* OUTPUT ACTIONS */

#define __pJoystickDrivingDiff_A12 \
pJoystickDrivingDiff_O_BF_pJoystickDrivingDiff()
#define __pJoystickDrivingDiff_A13 \
pJoystickDrivingDiff_O_STARTED_pJoystickDrivingDiff()
#define __pJoystickDrivingDiff_A14 \
pJoystickDrivingDiff_O_GoodEnd_pJoystickDrivingDiff()
#define __pJoystickDrivingDiff_A15 \
pJoystickDrivingDiff_O_Abort_pJoystickDrivingDiff()
#define __pJoystickDrivingDiff_A16 \
pJoystickDrivingDiff_O_T3_pJoystickDrivingDiff()
#define __pJoystickDrivingDiff_A17 \
pJoystickDrivingDiff_O_START_JoystickDriving()
#define __pJoystickDrivingDiff_A18 \
pJoystickDrivingDiff_O_Abort_Local_JoystickDriving()
#define __pJoystickDrivingDiff_A19 \
pJoystickDrivingDiff_O_START_JoystickDrivingDiff()
#define __pJoystickDrivingDiff_A20 \
pJoystickDrivingDiff_O_Abort_Local_JoystickDrivingDiff()

/* ASSIGNMENTS */

/* PROCEDURE CALLS */

#define __pJoystickDrivingDiff_A21 \
JoystickDrivingDiff_parameter(__pJoystickDrivingDiff_V7)
#define __pJoystickDrivingDiff_A22 \
JoystickDrivingDiff_controler(__pJoystickDrivingDiff_V7)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int pJoystickDrivingDiff_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static __pJoystickDrivingDiff__reset_input () {
__pJoystickDrivingDiff_V0 = _false;
__pJoystickDrivingDiff_V1 = _false;
__pJoystickDrivingDiff_V3 = _false;
__pJoystickDrivingDiff_V4 = _false;
__pJoystickDrivingDiff_V5 = _false;
__pJoystickDrivingDiff_V6 = _false;
__pJoystickDrivingDiff_V8 = _false;
__pJoystickDrivingDiff_V9 = _false;
__pJoystickDrivingDiff_V10 = _false;
__pJoystickDrivingDiff_V11 = _false;
__pJoystickDrivingDiff_V12 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __pJoystickDrivingDiff_R[4] = {_true,_false,_false,_false};

/* AUTOMATON ENGINE */

int pJoystickDrivingDiff () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[12];
E[0] = (__pJoystickDrivingDiff_R[2]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__pJoystickDrivingDiff_A9)));
E[1] = (((__pJoystickDrivingDiff_R[2]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__pJoystickDrivingDiff_A9)))||((E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__pJoystickDrivingDiff_A11))));
E[2] = (__pJoystickDrivingDiff_R[2]||__pJoystickDrivingDiff_R[3]);
E[3] = (((__pJoystickDrivingDiff_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__pJoystickDrivingDiff_A1)))||((__pJoystickDrivingDiff_R[1]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__pJoystickDrivingDiff_A1))));
if (E[3]) {
__pJoystickDrivingDiff_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A21\n");
#endif
}
if (E[3]) {
__pJoystickDrivingDiff_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A22\n");
#endif
}
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__pJoystickDrivingDiff_A11)));
E[4] = (E[3]||((((E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__pJoystickDrivingDiff_A10))))&&__pJoystickDrivingDiff_R[2])));
E[5] = (((E[2]&&!(__pJoystickDrivingDiff_R[2])))||E[4]);
E[6] = (E[5]||E[1]);
E[7] = (E[3]||((((__pJoystickDrivingDiff_R[3]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__pJoystickDrivingDiff_A2))))&&__pJoystickDrivingDiff_R[3])));
E[8] = (((E[2]&&!(__pJoystickDrivingDiff_R[3])))||E[7]);
E[1] = ((E[1]&&E[6])&&E[8]);
if (E[1]) {
__pJoystickDrivingDiff_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A12\n");
#endif
}
if (E[3]) {
__pJoystickDrivingDiff_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A13\n");
#endif
}
if (E[1]) {
__pJoystickDrivingDiff_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A14\n");
#endif
}
E[9] = (__pJoystickDrivingDiff_R[3]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__pJoystickDrivingDiff_A2));
if (E[9]) {
__pJoystickDrivingDiff_A15;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A15\n");
#endif
}
E[0] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__pJoystickDrivingDiff_A10));
E[6] = (E[6]||E[0]);
E[0] = ((E[0]&&E[6])&&E[8]);
if (E[0]) {
__pJoystickDrivingDiff_A16;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A16\n");
#endif
}
if (!(_true)) {
__pJoystickDrivingDiff_A17;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A17\n");
#endif
}
if (E[9]) {
__pJoystickDrivingDiff_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A18\n");
#endif
}
if (E[3]) {
__pJoystickDrivingDiff_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A19\n");
#endif
}
if (E[9]) {
__pJoystickDrivingDiff_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A20\n");
#endif
}
E[6] = ((E[9]&&E[6])&&((E[8]||E[9])));
E[10] = (((E[1]||E[0])||E[6]));
E[11] = (((__pJoystickDrivingDiff_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__pJoystickDrivingDiff_A1))))||((((__pJoystickDrivingDiff_R[1]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__pJoystickDrivingDiff_A1))))&&__pJoystickDrivingDiff_R[1])));
E[8] = (((((((E[4]||E[7]))&&E[5])&&E[8]))||E[11]));
E[2] = (E[2]||__pJoystickDrivingDiff_R[1]);
E[6] = ((E[1]||E[0])||E[6]);
__pJoystickDrivingDiff_R[2] = (E[4]&&!(E[6]));
__pJoystickDrivingDiff_R[3] = (E[7]&&!(E[6]));
__pJoystickDrivingDiff_R[0] = !(_true);
__pJoystickDrivingDiff_R[1] = E[11];
__pJoystickDrivingDiff__reset_input();
return E[8];
}

/* AUTOMATON RESET */

int pJoystickDrivingDiff_reset () {
__pJoystickDrivingDiff_R[0] = _true;
__pJoystickDrivingDiff_R[1] = _false;
__pJoystickDrivingDiff_R[2] = _false;
__pJoystickDrivingDiff_R[3] = _false;
__pJoystickDrivingDiff__reset_input();
return 0;
}
