/* sscc : C CODE OF SORTED EQUATIONS aa - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
int (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __aa_GENERIC_TEST(TEST) return TEST;
typedef (*__aa_APF)();
extern __aa_APF *__aa_PActionArray;
extern (*__aa_scf[])();
extern (*__aa_dcf[])();

#include "aa.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr();
#endif
#endif
#ifndef _JoystickDriving_controler_DEFINED
#ifndef JoystickDriving_controler
extern void JoystickDriving_controler();
#endif
#endif
#ifndef _JoystickDriving_fileparameter_DEFINED
#ifndef JoystickDriving_fileparameter
extern void JoystickDriving_fileparameter();
#endif
#endif
#ifndef _JoystickDriving_parameter_DEFINED
#ifndef JoystickDriving_parameter
extern void JoystickDriving_parameter();
#endif
#endif
#ifndef _JoystickDrivingDiff_controler_DEFINED
#ifndef JoystickDrivingDiff_controler
extern void JoystickDrivingDiff_controler();
#endif
#endif
#ifndef _JoystickDrivingDiff_fileparameter_DEFINED
#ifndef JoystickDrivingDiff_fileparameter
extern void JoystickDrivingDiff_fileparameter();
#endif
#endif
#ifndef _JoystickDrivingDiff_parameter_DEFINED
#ifndef JoystickDrivingDiff_parameter
extern void JoystickDrivingDiff_parameter();
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static orcStrlPtr __aa_V0;
static boolean __aa_V1;
static boolean __aa_V2;
static boolean __aa_V3;
static boolean __aa_V4;
static boolean __aa_V5;
static boolean __aa_V6;
static boolean __aa_V7;
static orcStrlPtr __aa_V8;
static boolean __aa_V9;
static boolean __aa_V10;
static boolean __aa_V11;
static boolean __aa_V12;
static boolean __aa_V13;
static boolean __aa_V14;
static boolean __aa_V15;
static boolean __aa_V16;
static orcStrlPtr __aa_V17;
static boolean __aa_V18;
static boolean __aa_V19;
static boolean __aa_V20;
static boolean __aa_V21;
static boolean __aa_V22;
static boolean __aa_V23;
static boolean __aa_V24;
static orcStrlPtr __aa_V25;
static orcStrlPtr __aa_V26;
static orcStrlPtr __aa_V27;
static orcStrlPtr __aa_V28;
static orcStrlPtr __aa_V29;
static orcStrlPtr __aa_V30;
static orcStrlPtr __aa_V31;
static orcStrlPtr __aa_V32;
static orcStrlPtr __aa_V33;
static orcStrlPtr __aa_V34;
static orcStrlPtr __aa_V35;
static boolean __aa_V36;


/* INPUT FUNCTIONS */

aa_I_Prr_Start (__V) orcStrlPtr __V; {
_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
aa_I_EmergencyStopE () {
__aa_V2 = _true;
}
aa_I_BadInputE () {
__aa_V3 = _true;
}
aa_I_BadinputE () {
__aa_V4 = _true;
}
aa_I_LooseCommE () {
__aa_V5 = _true;
}
aa_I_USERSTART_JoystickDrivingDiff () {
__aa_V6 = _true;
}
aa_I_T2_JoystickDrivingDiff () {
__aa_V7 = _true;
}
aa_I_JoystickDriving_Start (__V) orcStrlPtr __V; {
_orcStrlPtr(&__aa_V8,__V);
__aa_V9 = _true;
}
aa_I_ActivateOK_Cycab () {
__aa_V10 = _true;
}
aa_I_KillOK_Cycab () {
__aa_V11 = _true;
}
aa_I_ReadyToStop_Cycab () {
__aa_V12 = _true;
}
aa_I_CmdStopOK_Cycab () {
__aa_V13 = _true;
}
aa_I_DirOverE () {
__aa_V14 = _true;
}
aa_I_InitOK_Cycab () {
__aa_V15 = _true;
}
aa_I_PbMotorE () {
__aa_V16 = _true;
}
aa_I_JoystickDrivingDiff_Start (__V) orcStrlPtr __V; {
_orcStrlPtr(&__aa_V17,__V);
__aa_V18 = _true;
}
aa_I_USERSTART_JoystickDriving () {
__aa_V19 = _true;
}
aa_I_T2_JoystickDriving () {
__aa_V20 = _true;
}
aa_I_ROBOT_FAIL () {
__aa_V21 = _true;
}
aa_I_SOFT_FAIL () {
__aa_V22 = _true;
}
aa_I_SENSOR_FAIL () {
__aa_V23 = _true;
}
aa_I_CPU_OVERLOAD () {
__aa_V24 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __aa_A1 \
__aa_V1
#define __aa_A2 \
__aa_V2
#define __aa_A3 \
__aa_V3
#define __aa_A4 \
__aa_V4
#define __aa_A5 \
__aa_V5
#define __aa_A6 \
__aa_V6
#define __aa_A7 \
__aa_V7
#define __aa_A8 \
__aa_V9
#define __aa_A9 \
__aa_V10
#define __aa_A10 \
__aa_V11
#define __aa_A11 \
__aa_V12
#define __aa_A12 \
__aa_V13
#define __aa_A13 \
__aa_V14
#define __aa_A14 \
__aa_V15
#define __aa_A15 \
__aa_V16
#define __aa_A16 \
__aa_V18
#define __aa_A17 \
__aa_V19
#define __aa_A18 \
__aa_V20
#define __aa_A19 \
__aa_V21
#define __aa_A20 \
__aa_V22
#define __aa_A21 \
__aa_V23
#define __aa_A22 \
__aa_V24

/* OUTPUT ACTIONS */

#define __aa_A23 \
aa_O_Activate(__aa_V25)
#define __aa_A24 \
aa_O_STARTED_pJoystickDrivingDiff()
#define __aa_A25 \
aa_O_GoodEnd_pJoystickDrivingDiff()
#define __aa_A26 \
aa_O_Abort_pJoystickDrivingDiff()
#define __aa_A27 \
aa_O_ActivateJoystickDrivingDiff_Cycab(__aa_V26)
#define __aa_A28 \
aa_O_JoystickDrivingDiffTransite(__aa_V27)
#define __aa_A29 \
aa_O_Abort_JoystickDrivingDiff(__aa_V28)
#define __aa_A30 \
aa_O_STARTED_JoystickDrivingDiff()
#define __aa_A31 \
aa_O_GoodEnd_JoystickDrivingDiff()
#define __aa_A32 \
aa_O_ActivateJoystickDriving_Cycab(__aa_V29)
#define __aa_A33 \
aa_O_JoystickDrivingTransite(__aa_V30)
#define __aa_A34 \
aa_O_Abort_JoystickDriving(__aa_V31)
#define __aa_A35 \
aa_O_STARTED_JoystickDriving()
#define __aa_A36 \
aa_O_GoodEnd_JoystickDriving()
#define __aa_A37 \
aa_O_EndKill(__aa_V32)
#define __aa_A38 \
aa_O_FinBF(__aa_V33)
#define __aa_A39 \
aa_O_FinT3(__aa_V34)
#define __aa_A40 \
aa_O_GoodEndRPr()
#define __aa_A41 \
aa_O_T3RPr()

/* ASSIGNMENTS */

#define __aa_A42 \
_orcStrlPtr(&__aa_V32,__aa_V35)
#define __aa_A43 \
_orcStrlPtr(&__aa_V33,__aa_V0)
#define __aa_A44 \
_orcStrlPtr(&__aa_V34,__aa_V0)
#define __aa_A45 \
_orcStrlPtr(&__aa_V31,__aa_V8)
#define __aa_A46 \
_orcStrlPtr(&__aa_V29,__aa_V8)
#define __aa_A47 \
_orcStrlPtr(&__aa_V35,__aa_V8)
#define __aa_A48 \
_orcStrlPtr(&__aa_V30,__aa_V8)
#define __aa_A49 \
_orcStrlPtr(&__aa_V30,__aa_V8)
#define __aa_A50 \
_orcStrlPtr(&__aa_V30,__aa_V8)
#define __aa_A51 \
_orcStrlPtr(&__aa_V30,__aa_V8)
#define __aa_A52 \
_orcStrlPtr(&__aa_V28,__aa_V17)
#define __aa_A53 \
_orcStrlPtr(&__aa_V26,__aa_V17)
#define __aa_A54 \
_orcStrlPtr(&__aa_V35,__aa_V17)
#define __aa_A55 \
_orcStrlPtr(&__aa_V27,__aa_V17)
#define __aa_A56 \
_orcStrlPtr(&__aa_V27,__aa_V17)
#define __aa_A57 \
_orcStrlPtr(&__aa_V27,__aa_V17)
#define __aa_A58 \
_orcStrlPtr(&__aa_V27,__aa_V17)
#define __aa_A59 \
_orcStrlPtr(&__aa_V32,__aa_V35)

/* PROCEDURE CALLS */

#define __aa_A60 \
JoystickDrivingDiff_parameter(__aa_V17)
#define __aa_A61 \
JoystickDrivingDiff_controler(__aa_V17)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __aa_A62 \
0
#define __aa_A63 \
0

/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int aa_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static __aa__reset_input () {
__aa_V1 = _false;
__aa_V2 = _false;
__aa_V3 = _false;
__aa_V4 = _false;
__aa_V5 = _false;
__aa_V6 = _false;
__aa_V7 = _false;
__aa_V9 = _false;
__aa_V10 = _false;
__aa_V11 = _false;
__aa_V12 = _false;
__aa_V13 = _false;
__aa_V14 = _false;
__aa_V15 = _false;
__aa_V16 = _false;
__aa_V18 = _false;
__aa_V19 = _false;
__aa_V20 = _false;
__aa_V21 = _false;
__aa_V22 = _false;
__aa_V23 = _false;
__aa_V24 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __aa_R[34] = {_true,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int aa () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[80];
E[0] = (__aa_R[20]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13));
E[1] = (((((((__aa_R[19]||__aa_R[20])||__aa_R[21])||__aa_R[22])||__aa_R[23])||__aa_R[24])||__aa_R[25])||__aa_R[26]);
E[2] = (__aa_R[17]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14));
E[3] = (__aa_R[6]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14));
E[4] = (E[3]||E[2]);
E[5] = (__aa_R[30]&&E[4]);
E[6] = (__aa_R[33]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12));
E[7] = (E[5]||E[6]);
E[8] = (((E[2]&&E[7]))||((__aa_R[18]&&E[7])));
if (E[8]) {
__aa_A53;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A53\n");
#endif
}
if (E[8]) {
__aa_A54;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A54\n");
#endif
}
E[9] = (E[8]||__aa_R[19]);
E[10] = (((E[1]&&!(__aa_R[19])))||E[9]);
E[11] = (E[8]||((((__aa_R[20]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13))))&&__aa_R[20])));
E[12] = (((E[1]&&!(__aa_R[20])))||E[11]);
E[13] = (E[12]||E[0]);
E[14] = (E[8]||((((__aa_R[21]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2))))&&__aa_R[21])));
E[15] = (((E[1]&&!(__aa_R[21])))||E[14]);
E[16] = (E[8]||((((__aa_R[22]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15))))&&__aa_R[22])));
E[17] = (((E[1]&&!(__aa_R[22])))||E[16]);
E[18] = (E[8]||((((__aa_R[23]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3))))&&__aa_R[23])));
E[19] = (((E[1]&&!(__aa_R[23])))||E[18]);
E[20] = (E[8]||((((__aa_R[24]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5))))&&__aa_R[24])));
E[21] = (((E[1]&&!(__aa_R[24])))||E[20]);
E[22] = (E[8]||((((__aa_R[25]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4))))&&__aa_R[25])));
E[23] = (((E[1]&&!(__aa_R[25])))||E[22]);
E[24] = (E[8]||__aa_R[26]);
E[25] = (((E[1]&&!(__aa_R[26])))||E[24]);
E[26] = ((((((((E[0]&&E[10])&&E[13])&&E[15])&&E[17])&&E[19])&&E[21])&&E[23])&&E[25]);
E[27] = ((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7)||E[26]);
E[28] = (__aa_R[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13));
E[29] = (((((((__aa_R[8]||__aa_R[9])||__aa_R[10])||__aa_R[11])||__aa_R[12])||__aa_R[13])||__aa_R[14])||__aa_R[15]);
E[30] = (((E[3]&&E[7]))||((__aa_R[7]&&E[7])));
if (E[30]) {
__aa_A46;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A46\n");
#endif
}
if (E[30]) {
__aa_A47;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A47\n");
#endif
}
E[31] = (E[30]||__aa_R[8]);
E[32] = (((E[29]&&!(__aa_R[8])))||E[31]);
E[33] = (E[30]||((((__aa_R[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13))))&&__aa_R[9])));
E[34] = (((E[29]&&!(__aa_R[9])))||E[33]);
E[35] = (E[34]||E[28]);
E[36] = (E[30]||((((__aa_R[10]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2))))&&__aa_R[10])));
E[37] = (((E[29]&&!(__aa_R[10])))||E[36]);
E[38] = (E[30]||((((__aa_R[11]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15))))&&__aa_R[11])));
E[39] = (((E[29]&&!(__aa_R[11])))||E[38]);
E[40] = (E[30]||((((__aa_R[12]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3))))&&__aa_R[12])));
E[41] = (((E[29]&&!(__aa_R[12])))||E[40]);
E[42] = (E[30]||((((__aa_R[13]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5))))&&__aa_R[13])));
E[43] = (((E[29]&&!(__aa_R[13])))||E[42]);
E[44] = (E[30]||((((__aa_R[14]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4))))&&__aa_R[14])));
E[45] = (((E[29]&&!(__aa_R[14])))||E[44]);
E[46] = (E[30]||__aa_R[15]);
E[47] = (((E[29]&&!(__aa_R[15])))||E[46]);
E[48] = ((((((((E[28]&&E[32])&&E[35])&&E[37])&&E[39])&&E[41])&&E[43])&&E[45])&&E[47]);
E[49] = ((
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__aa_A18)||E[48]);
if (!(_true)) {
__aa_A23;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A23\n");
#endif
}
if (__aa_R[0]) {
__aa_A62;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A62\n");
#endif
}
if (__aa_R[0]) {
__aa_A63;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A63\n");
#endif
}
E[50] = (__aa_R[2]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8));
E[51] = (__aa_R[3]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 16\n"),
#endif
__aa_A16));
E[52] = (__aa_R[4]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1));
E[53] = ((__aa_R[2]||__aa_R[3])||__aa_R[4]);
E[54] = (((E[53]&&!(__aa_R[2])))||E[50]);
E[55] = (((E[53]&&!(__aa_R[3])))||E[51]);
E[56] = (((E[53]&&!(__aa_R[4])))||E[52]);
E[52] = ((((((E[50]||E[51])||E[52]))&&E[54])&&E[55])&&E[56]);
E[51] = (((__aa_R[0]&&E[52]))||((__aa_R[27]&&E[52])));
if (E[51]) {
__aa_A24;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A24\n");
#endif
}
E[0] = (E[28]||E[0]);
E[28] = (__aa_R[28]&&E[0]);
E[50] = (__aa_R[28]||__aa_R[29]);
if (E[51]) {
__aa_A60;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A60\n");
#endif
}
if (E[51]) {
__aa_A61;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A61\n");
#endif
}
E[0] = (__aa_R[28]&&!(E[0]));
E[57] = (__aa_R[21]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2));
E[58] = (__aa_R[22]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15));
E[59] = (__aa_R[23]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3));
E[60] = (__aa_R[24]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5));
E[61] = (__aa_R[25]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4));
if (!(_true)) {
__aa_A56;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A56\n");
#endif
}
E[13] = (((((((((((((E[57]||E[58])||E[59])||E[60])||E[61]))&&E[10])&&E[13])&&((E[15]||E[57])))&&((E[17]||E[58])))&&((E[19]||E[59])))&&((E[21]||E[60])))&&((E[23]||E[61])))&&E[25]);
E[62] = (E[51]||((((E[0]&&!(E[13])))&&__aa_R[28])));
E[63] = (((E[50]&&!(__aa_R[28])))||E[62]);
E[64] = (E[63]||E[28]);
E[65] = (E[51]||__aa_R[29]);
E[66] = (((E[50]&&!(__aa_R[29])))||E[65]);
E[28] = ((E[28]&&E[64])&&E[66]);
if (E[28]) {
__aa_A25;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A25\n");
#endif
}
if (!(_true)) {
__aa_A26;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A26\n");
#endif
}
if (E[8]) {
__aa_A27;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A27\n");
#endif
}
if (E[26]) {
__aa_A58;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A58\n");
#endif
}
if (!(_true)) {
__aa_A55;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A55\n");
#endif
}
if (E[26]) {
__aa_A28;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A28\n");
#endif
}
if (!(_true)) {
__aa_A52;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A52\n");
#endif
}
if (!(_true)) {
__aa_A29;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A29\n");
#endif
}
E[67] = (E[26]||__aa_R[0]);
E[68] = (((E[67]&&E[51]))||((__aa_R[16]&&E[51])));
if (E[68]) {
__aa_A30;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A30\n");
#endif
}
if (!(_true)) {
__aa_A31;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A31\n");
#endif
}
if (E[30]) {
__aa_A32;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A32\n");
#endif
}
if (!(_true)) {
__aa_A49;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A49\n");
#endif
}
if (E[48]) {
__aa_A51;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A51\n");
#endif
}
if (!(_true)) {
__aa_A48;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A48\n");
#endif
}
if (E[48]) {
__aa_A33;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A33\n");
#endif
}
if (!(_true)) {
__aa_A45;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A45\n");
#endif
}
if (!(_true)) {
__aa_A34;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A34\n");
#endif
}
if (!(_true)) {
__aa_A35;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A35\n");
#endif
}
if (!(_true)) {
__aa_A36;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A36\n");
#endif
}
E[69] = (__aa_R[32]&&E[4]);
if (E[69]) {
__aa_A59;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A59\n");
#endif
}
E[70] = (__aa_R[1]&&E[28]);
E[29] = (((__aa_R[6]||E[29])||__aa_R[7])||__aa_R[5]);
E[1] = (((__aa_R[17]||E[1])||__aa_R[18])||__aa_R[16]);
E[50] = (E[50]||__aa_R[27]);
E[71] = (((__aa_R[30]||__aa_R[31])||__aa_R[32])||__aa_R[33]);
E[72] = (((((E[53]||E[29])||E[1])||E[50])||E[71])||__aa_R[1]);
E[73] = (__aa_R[0]||((((__aa_R[2]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8))))&&__aa_R[2])));
E[74] = (__aa_R[0]||((((__aa_R[3]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 16\n"),
#endif
__aa_A16))))&&__aa_R[3])));
E[75] = (__aa_R[0]||((((__aa_R[4]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1))))&&__aa_R[4])));
E[56] = ((((((E[73]||E[74])||E[75]))&&((E[54]||E[73])))&&((E[55]||E[74])))&&((E[56]||E[75])));
E[53] = (((((E[72]&&!(E[53])))||E[52]))||E[56]);
E[55] = (__aa_R[10]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2));
E[54] = (__aa_R[11]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__aa_A15));
E[76] = (__aa_R[12]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3));
E[77] = (__aa_R[13]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__aa_A5));
E[78] = (__aa_R[14]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4));
E[35] = (((((((((((((E[55]||E[54])||E[76])||E[77])||E[78]))&&E[32])&&E[35])&&((E[37]||E[55])))&&((E[39]||E[54])))&&((E[41]||E[76])))&&((E[43]||E[77])))&&((E[45]||E[78])))&&E[47]);
E[79] = (((__aa_R[6]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14))))&&__aa_R[6]);
E[47] = ((((((((((((((((E[31]||E[33])||E[36])||E[38])||E[40])||E[42])||E[44])||E[46]))&&E[32])&&E[34])&&E[37])&&E[39])&&E[41])&&E[43])&&E[45])&&E[47]);
E[3] = (((E[3]&&!(E[7])))||((((__aa_R[7]&&!(E[7])))&&__aa_R[7])));
E[45] = (((E[48]||__aa_R[0]))||__aa_R[5]);
E[29] = ((((((((E[72]&&!(E[29])))||E[35]))||E[79])||E[47])||E[3])||E[45]);
E[43] = (E[68]||((((__aa_R[17]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14))))&&__aa_R[17])));
E[25] = ((((((((((((((((E[9]||E[11])||E[14])||E[16])||E[18])||E[20])||E[22])||E[24]))&&E[10])&&E[12])&&E[15])&&E[17])&&E[19])&&E[21])&&E[23])&&E[25]);
E[7] = (((E[2]&&!(E[7])))||((((__aa_R[18]&&!(E[7])))&&__aa_R[18])));
E[67] = (((E[67]&&!(E[51])))||((((__aa_R[16]&&!(E[51])))&&__aa_R[16])));
E[1] = ((((((((E[72]&&!(E[1])))||E[13]))||E[43])||E[25])||E[7])||E[67]);
E[0] = (E[0]&&E[13]);
E[0] = ((E[0]&&((E[64]||E[0])))&&E[66]);
E[52] = (((__aa_R[0]&&!(E[52])))||((((__aa_R[27]&&!(E[52])))&&__aa_R[27])));
E[66] = ((((((E[62]||E[65]))&&E[63])&&E[66]))||E[52]);
E[50] = (((((E[72]&&!(E[50])))||((E[28]||E[0]))))||E[66]);
E[63] = (__aa_R[0]||((((__aa_R[30]&&!(E[4])))&&__aa_R[30])));
E[64] = (E[48]||E[26]);
E[6] = ((E[5]||((((__aa_R[31]&&!(E[64])))&&__aa_R[31])))||E[6]);
E[4] = (((__aa_R[31]&&E[64]))||((((__aa_R[32]&&!(E[4])))&&__aa_R[32])));
E[64] = (E[69]||((((__aa_R[33]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12))))&&__aa_R[33])));
E[71] = ((((((E[72]&&!(E[71])))||E[63])||E[6])||E[4])||E[64]);
E[5] = (__aa_R[1]&&!(E[28]));
E[2] = (E[5]&&!(E[0]));
E[23] = (E[2]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__aa_A19)));
E[21] = (E[23]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 20\n"),
#endif
__aa_A20)));
E[19] = (E[21]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__aa_A21)));
E[17] = (__aa_R[0]||((((E[19]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22))))&&__aa_R[1])));
E[72] = (((E[72]&&!(__aa_R[1])))||E[17]);
E[15] = (E[72]||E[70]);
E[70] = ((((((E[70]&&E[53])&&E[29])&&E[1])&&E[50])&&E[71])&&E[15]);
if (E[70]) {
__aa_A42;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A42\n");
#endif
}
if (((E[69]||E[70]))) {
__aa_A37;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A37\n");
#endif
}
if (E[70]) {
__aa_A43;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A43\n");
#endif
}
if (E[70]) {
__aa_A38;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A38\n");
#endif
}
E[5] = (E[5]&&E[0]);
E[2] = (E[2]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__aa_A19));
E[23] = (E[23]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 20\n"),
#endif
__aa_A20));
E[21] = (E[21]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__aa_A21));
E[19] = (E[19]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22));
E[15] = (((((((((((E[5]||E[2])||E[23])||E[21])||E[19]))&&E[53])&&E[29])&&E[1])&&E[50])&&E[71])&&((((((E[15]||E[5])||E[2])||E[23])||E[21])||E[19])));
if (E[15]) {
__aa_A44;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A44\n");
#endif
}
if (E[15]) {
__aa_A39;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A39\n");
#endif
}
if (E[70]) {
__aa_A40;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A40\n");
#endif
}
if (E[15]) {
__aa_A41;
#ifdef TRACE_ACTION
fprintf(stderr, "__abc_A41\n");
#endif
}
E[12] = (E[30]||E[8]);
E[19] = ((((E[5]||E[2])||E[23])||E[21])||E[19]);
E[78] = ((((E[55]||E[54])||E[76])||E[77])||E[78]);
E[61] = ((((E[57]||E[58])||E[59])||E[60])||E[61]);
E[60] = ((E[70]||E[15]));
E[72] = ((((((((((((((((((((((E[56]||E[79])||E[47])||E[3])||E[45])||E[43])||E[25])||E[7])||E[67])||E[66])||E[63])||E[6])||E[4])||E[64])||E[17]))&&E[53])&&E[29])&&E[1])&&E[50])&&E[71])&&E[72]));
E[71] = (E[70]||E[15]);
__aa_R[1] = (E[17]&&!(E[71]));
__aa_R[2] = (E[73]&&!(E[71]));
__aa_R[3] = (E[74]&&!(E[71]));
__aa_R[4] = (E[75]&&!(E[71]));
__aa_R[5] = (E[45]&&!(E[71]));
__aa_R[6] = (E[79]&&!(E[71]));
__aa_R[7] = (E[3]&&!(E[71]));
E[35] = ((E[71]||E[48])||E[35]);
__aa_R[8] = (E[31]&&!(E[35]));
__aa_R[9] = (E[33]&&!(E[35]));
__aa_R[10] = (E[36]&&!(E[35]));
__aa_R[11] = (E[38]&&!(E[35]));
__aa_R[12] = (E[40]&&!(E[35]));
__aa_R[13] = (E[42]&&!(E[35]));
__aa_R[14] = (E[44]&&!(E[35]));
__aa_R[15] = (E[46]&&!(E[35]));
__aa_R[16] = (E[67]&&!(E[71]));
__aa_R[17] = (E[43]&&!(E[71]));
__aa_R[18] = (E[7]&&!(E[71]));
E[13] = ((E[71]||E[26])||E[13]);
__aa_R[19] = (E[9]&&!(E[13]));
__aa_R[20] = (E[11]&&!(E[13]));
__aa_R[21] = (E[14]&&!(E[13]));
__aa_R[22] = (E[16]&&!(E[13]));
__aa_R[23] = (E[18]&&!(E[13]));
__aa_R[24] = (E[20]&&!(E[13]));
__aa_R[25] = (E[22]&&!(E[13]));
__aa_R[26] = (E[24]&&!(E[13]));
__aa_R[27] = (E[52]&&!(E[71]));
E[0] = ((E[71]||E[28])||E[0]);
__aa_R[28] = (E[62]&&!(E[0]));
__aa_R[29] = (E[65]&&!(E[0]));
__aa_R[30] = (E[63]&&!(E[71]));
__aa_R[31] = (E[6]&&!(E[71]));
__aa_R[32] = (E[4]&&!(E[71]));
__aa_R[33] = (E[64]&&!(E[71]));
__aa_R[0] = !(_true);
__aa__reset_input();
return E[72];
}

/* AUTOMATON RESET */

int aa_reset () {
__aa_R[0] = _true;
__aa_R[1] = _false;
__aa_R[2] = _false;
__aa_R[3] = _false;
__aa_R[4] = _false;
__aa_R[5] = _false;
__aa_R[6] = _false;
__aa_R[7] = _false;
__aa_R[8] = _false;
__aa_R[9] = _false;
__aa_R[10] = _false;
__aa_R[11] = _false;
__aa_R[12] = _false;
__aa_R[13] = _false;
__aa_R[14] = _false;
__aa_R[15] = _false;
__aa_R[16] = _false;
__aa_R[17] = _false;
__aa_R[18] = _false;
__aa_R[19] = _false;
__aa_R[20] = _false;
__aa_R[21] = _false;
__aa_R[22] = _false;
__aa_R[23] = _false;
__aa_R[24] = _false;
__aa_R[25] = _false;
__aa_R[26] = _false;
__aa_R[27] = _false;
__aa_R[28] = _false;
__aa_R[29] = _false;
__aa_R[30] = _false;
__aa_R[31] = _false;
__aa_R[32] = _false;
__aa_R[33] = _false;
__aa__reset_input();
return 0;
}
