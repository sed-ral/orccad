/* sscc : C CODE OF SORTED EQUATIONS ProcStartPost - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __ProcStartPost_GENERIC_TEST(TEST) return TEST;
typedef void (*__ProcStartPost_APF)();
static __ProcStartPost_APF *__ProcStartPost_PActionArray;

#include "ProcStartPost.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _CycabFollowingRatio_controler_DEFINED
#ifndef CycabFollowingRatio_controler
extern void CycabFollowingRatio_controler(orcStrlPtr);
#endif
#endif
#ifndef _CycabFollowingRatio_fileparameter_DEFINED
#ifndef CycabFollowingRatio_fileparameter
extern void CycabFollowingRatio_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _CycabFollowingRatio_parameter_DEFINED
#ifndef CycabFollowingRatio_parameter
extern void CycabFollowingRatio_parameter(orcStrlPtr);
#endif
#endif
#ifndef _CycabStartPost_controler_DEFINED
#ifndef CycabStartPost_controler
extern void CycabStartPost_controler(orcStrlPtr);
#endif
#endif
#ifndef _CycabStartPost_fileparameter_DEFINED
#ifndef CycabStartPost_fileparameter
extern void CycabStartPost_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _CycabStartPost_parameter_DEFINED
#ifndef CycabStartPost_parameter
extern void CycabStartPost_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __ProcStartPost_V0;
static boolean __ProcStartPost_V1;
static orcStrlPtr __ProcStartPost_V2;
static boolean __ProcStartPost_V3;
static boolean __ProcStartPost_V4;
static boolean __ProcStartPost_V5;
static boolean __ProcStartPost_V6;
static orcStrlPtr __ProcStartPost_V7;
static boolean __ProcStartPost_V8;
static boolean __ProcStartPost_V9;
static boolean __ProcStartPost_V10;
static boolean __ProcStartPost_V11;
static boolean __ProcStartPost_V12;
static boolean __ProcStartPost_V13;
static boolean __ProcStartPost_V14;


/* INPUT FUNCTIONS */

void ProcStartPost_I_START_ProcStartPost () {
__ProcStartPost_V0 = _true;
}
void ProcStartPost_I_Abort_Local_ProcStartPost () {
__ProcStartPost_V1 = _true;
}
void ProcStartPost_I_CycabFollowingRatio_Start (orcStrlPtr __V) {
_orcStrlPtr(&__ProcStartPost_V2,__V);
__ProcStartPost_V3 = _true;
}
void ProcStartPost_I_USERSTART_CycabFollowingRatio () {
__ProcStartPost_V4 = _true;
}
void ProcStartPost_I_BF_CycabFollowingRatio () {
__ProcStartPost_V5 = _true;
}
void ProcStartPost_I_T3_CycabFollowingRatio () {
__ProcStartPost_V6 = _true;
}
void ProcStartPost_I_CycabStartPost_Start (orcStrlPtr __V) {
_orcStrlPtr(&__ProcStartPost_V7,__V);
__ProcStartPost_V8 = _true;
}
void ProcStartPost_I_USERSTART_CycabStartPost () {
__ProcStartPost_V9 = _true;
}
void ProcStartPost_I_BF_CycabStartPost () {
__ProcStartPost_V10 = _true;
}
void ProcStartPost_I_T3_CycabStartPost () {
__ProcStartPost_V11 = _true;
}
void ProcStartPost_I_T2_EmergencyStopE () {
__ProcStartPost_V12 = _true;
}
void ProcStartPost_I_T2_LooseTargetE () {
__ProcStartPost_V13 = _true;
}
void ProcStartPost_I_T2_EmStopE () {
__ProcStartPost_V14 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __ProcStartPost_A1 \
__ProcStartPost_V0
#define __ProcStartPost_A2 \
__ProcStartPost_V1
#define __ProcStartPost_A3 \
__ProcStartPost_V3
#define __ProcStartPost_A4 \
__ProcStartPost_V4
#define __ProcStartPost_A5 \
__ProcStartPost_V5
#define __ProcStartPost_A6 \
__ProcStartPost_V6
#define __ProcStartPost_A7 \
__ProcStartPost_V8
#define __ProcStartPost_A8 \
__ProcStartPost_V9
#define __ProcStartPost_A9 \
__ProcStartPost_V10
#define __ProcStartPost_A10 \
__ProcStartPost_V11
#define __ProcStartPost_A11 \
__ProcStartPost_V12
#define __ProcStartPost_A12 \
__ProcStartPost_V13
#define __ProcStartPost_A13 \
__ProcStartPost_V14

/* OUTPUT ACTIONS */

#define __ProcStartPost_A14 \
ProcStartPost_O_BF_ProcStartPost()
#define __ProcStartPost_A15 \
ProcStartPost_O_STARTED_ProcStartPost()
#define __ProcStartPost_A16 \
ProcStartPost_O_GoodEnd_ProcStartPost()
#define __ProcStartPost_A17 \
ProcStartPost_O_Abort_ProcStartPost()
#define __ProcStartPost_A18 \
ProcStartPost_O_T3_ProcStartPost()
#define __ProcStartPost_A19 \
ProcStartPost_O_START_CycabFollowingRatio()
#define __ProcStartPost_A20 \
ProcStartPost_O_Abort_Local_CycabFollowingRatio()
#define __ProcStartPost_A21 \
ProcStartPost_O_START_CycabStartPost()
#define __ProcStartPost_A22 \
ProcStartPost_O_Abort_Local_CycabStartPost()

/* ASSIGNMENTS */

#define __ProcStartPost_A23 \
__ProcStartPost_V0 = _false
#define __ProcStartPost_A24 \
__ProcStartPost_V1 = _false
#define __ProcStartPost_A25 \
__ProcStartPost_V3 = _false
#define __ProcStartPost_A26 \
__ProcStartPost_V4 = _false
#define __ProcStartPost_A27 \
__ProcStartPost_V5 = _false
#define __ProcStartPost_A28 \
__ProcStartPost_V6 = _false
#define __ProcStartPost_A29 \
__ProcStartPost_V8 = _false
#define __ProcStartPost_A30 \
__ProcStartPost_V9 = _false
#define __ProcStartPost_A31 \
__ProcStartPost_V10 = _false
#define __ProcStartPost_A32 \
__ProcStartPost_V11 = _false
#define __ProcStartPost_A33 \
__ProcStartPost_V12 = _false
#define __ProcStartPost_A34 \
__ProcStartPost_V13 = _false
#define __ProcStartPost_A35 \
__ProcStartPost_V14 = _false

/* PROCEDURE CALLS */

#define __ProcStartPost_A36 \
CycabStartPost_parameter(__ProcStartPost_V7)
#define __ProcStartPost_A37 \
CycabStartPost_controler(__ProcStartPost_V7)
#define __ProcStartPost_A38 \
CycabFollowingRatio_parameter(__ProcStartPost_V2)
#define __ProcStartPost_A39 \
CycabFollowingRatio_controler(__ProcStartPost_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __ProcStartPost_A40 \

#define __ProcStartPost_A41 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int ProcStartPost_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __ProcStartPost__reset_input () {
__ProcStartPost_V0 = _false;
__ProcStartPost_V1 = _false;
__ProcStartPost_V3 = _false;
__ProcStartPost_V4 = _false;
__ProcStartPost_V5 = _false;
__ProcStartPost_V6 = _false;
__ProcStartPost_V8 = _false;
__ProcStartPost_V9 = _false;
__ProcStartPost_V10 = _false;
__ProcStartPost_V11 = _false;
__ProcStartPost_V12 = _false;
__ProcStartPost_V13 = _false;
__ProcStartPost_V14 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __ProcStartPost_R[5] = {_true,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int ProcStartPost () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[23];
E[0] = (__ProcStartPost_R[3]&&!(__ProcStartPost_R[0]));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__ProcStartPost_A5));
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__ProcStartPost_A5)));
E[2] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__ProcStartPost_A11));
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__ProcStartPost_A11)));
E[3] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__ProcStartPost_A13));
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__ProcStartPost_A13)));
E[4] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__ProcStartPost_A12));
E[1] = (((E[1]||E[2])||E[3])||E[4]);
E[5] = (__ProcStartPost_R[2]||__ProcStartPost_R[3]);
E[6] = (E[5]||__ProcStartPost_R[4]);
E[7] = (__ProcStartPost_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__ProcStartPost_A3)));
if (E[7]) {
__ProcStartPost_A40;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A40\n");
#endif
}
E[8] = (__ProcStartPost_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__ProcStartPost_A7)));
if (E[8]) {
__ProcStartPost_A41;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A41\n");
#endif
}
E[9] = (__ProcStartPost_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ProcStartPost_A1));
E[10] = (__ProcStartPost_R[1]&&!(__ProcStartPost_R[0]));
E[11] = (E[10]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ProcStartPost_A1));
E[11] = (E[9]||E[11]);
if (E[11]) {
__ProcStartPost_A36;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A36\n");
#endif
}
if (E[11]) {
__ProcStartPost_A37;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A37\n");
#endif
}
E[9] = (__ProcStartPost_R[2]&&!(__ProcStartPost_R[0]));
E[12] = (E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__ProcStartPost_A9)));
E[13] = (E[12]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__ProcStartPost_A11)));
E[14] = (E[13]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__ProcStartPost_A13)));
E[15] = (E[14]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__ProcStartPost_A12)));
E[16] = (E[15]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__ProcStartPost_A10)));
E[16] = (E[11]||((__ProcStartPost_R[2]&&E[16])));
E[9] = (E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__ProcStartPost_A9));
E[12] = (E[12]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__ProcStartPost_A11));
E[13] = (E[13]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__ProcStartPost_A13));
E[14] = (E[14]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__ProcStartPost_A12));
E[9] = (((E[9]||E[12])||E[13])||E[14]);
if (E[9]) {
__ProcStartPost_A38;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A38\n");
#endif
}
if (E[9]) {
__ProcStartPost_A39;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A39\n");
#endif
}
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__ProcStartPost_A12)));
E[17] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__ProcStartPost_A6)));
E[17] = (E[9]||((__ProcStartPost_R[3]&&E[17])));
E[5] = ((((E[6]&&!(E[5])))||E[16])||E[17]);
E[18] = (E[5]||E[1]);
E[19] = (__ProcStartPost_R[4]&&!(__ProcStartPost_R[0]));
E[20] = (E[19]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__ProcStartPost_A2)));
E[20] = (E[11]||((__ProcStartPost_R[4]&&E[20])));
E[21] = (((E[6]&&!(__ProcStartPost_R[4])))||E[20]);
E[1] = ((E[1]&&E[18])&&E[21]);
if (E[1]) {
__ProcStartPost_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A14\n");
#endif
}
if (E[11]) {
__ProcStartPost_A15;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A15\n");
#endif
}
if (E[1]) {
__ProcStartPost_A16;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A16\n");
#endif
}
E[19] = (E[19]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__ProcStartPost_A2));
if (E[19]) {
__ProcStartPost_A17;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A17\n");
#endif
}
E[0] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__ProcStartPost_A6));
E[15] = (E[15]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__ProcStartPost_A10));
E[18] = ((E[18]||E[0])||E[15]);
E[22] = ((((E[0]||E[15]))&&E[18])&&E[21]);
if (E[22]) {
__ProcStartPost_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A18\n");
#endif
}
if (E[9]) {
__ProcStartPost_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A19\n");
#endif
}
if (E[19]) {
__ProcStartPost_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A20\n");
#endif
}
if (E[11]) {
__ProcStartPost_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A21\n");
#endif
}
if (E[19]) {
__ProcStartPost_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__ProcStartPost_A22\n");
#endif
}
E[13] = (E[3]||E[13]);
E[12] = (E[2]||E[12]);
E[14] = (E[4]||E[14]);
E[15] = (E[0]||E[15]);
E[18] = ((E[19]&&E[18])&&((E[21]||E[19])));
E[0] = ((((E[1]||E[22]))||E[18]));
E[4] = (__ProcStartPost_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ProcStartPost_A1)));
E[10] = (E[10]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__ProcStartPost_A1)));
E[10] = (E[4]||((__ProcStartPost_R[1]&&E[10])));
E[21] = ((((((((E[16]||E[17])||E[20]))&&E[5])&&E[21]))||E[10]));
E[6] = (E[6]||__ProcStartPost_R[1]);
E[18] = (((((E[18]||E[22]))||E[1])||E[22])||E[18]);
__ProcStartPost_R[2] = (E[16]&&!(E[18]));
__ProcStartPost_R[3] = (E[17]&&!(E[18]));
__ProcStartPost_R[4] = (E[20]&&!(E[18]));
__ProcStartPost_R[0] = !(_true);
__ProcStartPost_R[1] = E[10];
__ProcStartPost__reset_input();
return E[21];
}

/* AUTOMATON RESET */

int ProcStartPost_reset () {
__ProcStartPost_R[0] = _true;
__ProcStartPost_R[1] = _false;
__ProcStartPost_R[2] = _false;
__ProcStartPost_R[3] = _false;
__ProcStartPost_R[4] = _false;
__ProcStartPost__reset_input();
return 0;
}
