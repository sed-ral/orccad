/*--------------------------------------
 * definitions for simulation purposes 
 *--------------------------------------
 */
void CycabFollowingSick_parameter() {}
void CycabFollowingSick_fileparameter() {}
void CycabFollowingSick_controler() {}
void Version3_parameter() {}
void Version3_fileparameter() {}
void Version3_controler() {}
void CycabStart_parameter() {}
void CycabStart_fileparameter() {}
void CycabStart_controler() {}
void CycabStart2_parameter() {}
void CycabStart2_fileparameter() {}
void CycabStart2_controler() {}
void CycabFollowingRatio_parameter() {}
void CycabFollowingRatio_fileparameter() {}
void CycabFollowingRatio_controler() {}
void CycabStartPost_parameter() {}
void CycabStartPost_fileparameter() {}
void CycabStartPost_controler() {}
typedef void* orcStrlPtr;
#define _orcStrlPtr(x,y)(*(x)=y)
#define _check_orcStrlPtr _check_integer
#define _text_to_orcStrlPtr  _text_to_integer
#define _orcStrlPtr_to_text _integer_to_text
#define _NO_PROCEDURE_DEFINITIONS
