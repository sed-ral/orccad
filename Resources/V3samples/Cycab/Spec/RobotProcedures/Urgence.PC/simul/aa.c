/* sscc : C CODE OF SORTED EQUATIONS aa - SIMULATION MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define CSIMUL_H_LOADED
typedef struct {char text[STRLEN];} symbolic;
extern void _boolean(boolean*, boolean);
extern boolean _eq_boolean(boolean, boolean);
extern boolean _ne_boolean(boolean, boolean);
extern boolean _cond_boolean(boolean ,boolean ,boolean);
extern char* _boolean_to_text(boolean);
extern int _check_boolean(char*);
extern void _text_to_boolean(boolean*, char*);
extern void _integer(integer*, integer);
extern boolean _eq_integer(integer, integer);
extern boolean _ne_integer(integer, integer);
extern integer _cond_integer(boolean ,integer ,integer);
extern char* _integer_to_text(integer);
extern int _check_integer(char*);
extern void _text_to_integer(integer*, char*);
extern void _string(string, string);
extern boolean _eq_string(string, string);
extern boolean _ne_string(string, string);
extern string _cond_string(boolean ,string ,string);
extern char* _string_to_text(string);
extern int _check_string(char*);
extern void _text_to_string(string, char*);
extern void _float(float*, float);
extern boolean _eq_float(float, float);
extern boolean _ne_float(float, float);
extern float _cond_float(boolean ,float ,float);
extern char* _float_to_text(float);
extern int _check_float(char*);
extern void _text_to_float(float*, char*);
extern void _double(double*, double);
extern boolean _eq_double(double, double);
extern boolean _ne_double(double, double);
extern double _cond_double(boolean ,double ,double);
extern char* _double_to_text(double);
extern int _check_double(char*);
extern void _text_to_double(double*, char*);
extern void _symbolic(symbolic*, symbolic);
extern boolean _eq_symbolic(symbolic, symbolic);
extern boolean _ne_symbolic(symbolic, symbolic);
extern symbolic _cond_symbolic(boolean ,symbolic ,symbolic);
extern char* _symbolic_to_text(symbolic);
extern int _check_symbolic(char*);
extern void _text_to_symbolic(symbolic*, char*);
extern char* __PredefinedTypeToText(int, char*);
#define _true 1
#define _false 0
#define __aa_GENERIC_TEST(TEST) return TEST;
typedef void (*__aa_APF)();
static __aa_APF *__aa_PActionArray;
static int **__aa_PCheckArray;
struct __SourcePoint {
int linkback;
int line;
int column;
int instance_index;
};
struct __InstanceEntry {
char *module_name;
int father_index;
char *dir_name;
char *file_name;
struct __SourcePoint source_point;
struct __SourcePoint end_point;
struct __SourcePoint instance_point;
};
struct __TaskEntry {
char *name;
int   nb_args_ref;
int   nb_args_val;
int  *type_codes_array;
struct __SourcePoint source_point;
};
struct __SignalEntry {
char *name;
int code;
int variable_index;
int present;
struct __SourcePoint source_point;
int number_of_emit_source_points;
struct __SourcePoint* emit_source_point_array;
int number_of_present_source_points;
struct __SourcePoint* present_source_point_array;
int number_of_access_source_points;
struct __SourcePoint* access_source_point_array;
};
struct __InputEntry {
char *name;
int hash;
char *type_name;
int is_a_sensor;
int type_code;
int multiple;
int signal_index;
int (*p_check_input)(char*);
void (*p_input_function)();
int present;
struct __SourcePoint source_point;
};
struct __ReturnEntry {
char *name;
int hash;
char *type_name;
int type_code;
int signal_index;
int exec_index;
int (*p_check_input)(char*);
void (*p_input_function)();
int present;
struct __SourcePoint source_point;
};
struct __ImplicationEntry {
int master;
int slave;
struct __SourcePoint source_point;
};
struct __ExclusionEntry {
int *exclusion_list;
struct __SourcePoint source_point;
};
struct __VariableEntry {
char *full_name;
char *short_name;
char *type_name;
int type_code;
int comment_kind;
int is_initialized;
char *p_variable;
char *source_name;
int written;
unsigned char written_in_transition;
unsigned char read_in_transition;
struct __SourcePoint source_point;
};
struct __ExecEntry {
int task_index;
int *var_indexes_array;
char **p_values_array;
struct __SourcePoint source_point;
};
struct __HaltEntry {
struct __SourcePoint source_point;
};
struct __NetEntry {
int known;
int value;
int number_of_source_points;
struct __SourcePoint* source_point_array;
};
struct __ModuleEntry {
char *version_id;
char *name;
int number_of_instances;
int number_of_tasks;
int number_of_signals;
int number_of_inputs;
int number_of_returns;
int number_of_sensors;
int number_of_outputs;
int number_of_locals;
int number_of_exceptions;
int number_of_implications;
int number_of_exclusions;
int number_of_variables;
int number_of_execs;
int number_of_halts;
int number_of_nets;
int number_of_states;
int state;
unsigned short *halt_list;
unsigned short *awaited_list;
unsigned short *emitted_list;
unsigned short *started_list;
unsigned short *killed_list;
unsigned short *suspended_list;
unsigned short *active_list;
int run_time_error_code;
int error_info;
void (*init)();
int (*run)();
int (*reset)();
char *(*show_variable)(int);
void (*set_variable)(int, char*, char*);
int (*check_value)(int, char*);
int (*execute_action)();
struct __InstanceEntry* instance_table;
struct __TaskEntry* task_table;
struct __SignalEntry* signal_table;
struct __InputEntry* input_table;
struct __ReturnEntry* return_table;
struct __ImplicationEntry* implication_table;
struct __ExclusionEntry* exclusion_table;
struct __VariableEntry* variable_table;
struct __ExecEntry* exec_table;
struct __HaltEntry* halt_table;
struct __NetEntry* net_table;
};

#include "aa.h"

/* EXTERN DECLARATIONS */

#ifndef _orcStrlPtr_to_text
extern char* _orcStrlPtr_to_text(orcStrlPtr);
#endif
#ifndef _check_orcStrlPtr
extern int _check_orcStrlPtr(char*);
#endif
#ifndef _text_to_orcStrlPtr
extern void _text_to_orcStrlPtr(orcStrlPtr*, char*);
#endif
extern int __CheckVariables(int*);
extern void __ResetInput();
extern void __ResetExecs();
extern void __ResetVariables();
extern void __ResetVariableStatus();
extern void __AppendToList(unsigned short*, unsigned short);
extern void __ListCopy(unsigned short*, unsigned short**);
extern void __WriteVariable(int);
extern void __ResetVariable(int);
extern void __ResetModuleEntry();
extern void __ResetModuleEntryBeforeReaction();
extern void __ResetModuleEntryAfterReaction();
#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _CycabFollowingRatio_controler_DEFINED
#ifndef CycabFollowingRatio_controler
extern void CycabFollowingRatio_controler(orcStrlPtr);
#endif
#endif
#ifndef _CycabFollowingRatio_fileparameter_DEFINED
#ifndef CycabFollowingRatio_fileparameter
extern void CycabFollowingRatio_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _CycabFollowingRatio_parameter_DEFINED
#ifndef CycabFollowingRatio_parameter
extern void CycabFollowingRatio_parameter(orcStrlPtr);
#endif
#endif
#ifndef _CycabStartPost_controler_DEFINED
#ifndef CycabStartPost_controler
extern void CycabStartPost_controler(orcStrlPtr);
#endif
#endif
#ifndef _CycabStartPost_fileparameter_DEFINED
#ifndef CycabStartPost_fileparameter
extern void CycabStartPost_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _CycabStartPost_parameter_DEFINED
#ifndef CycabStartPost_parameter
extern void CycabStartPost_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static orcStrlPtr __aa_V0;
static boolean __aa_V1;
static boolean __aa_V2;
static boolean __aa_V3;
static boolean __aa_V4;
static boolean __aa_V5;
static boolean __aa_V6;
static boolean __aa_V7;
static boolean __aa_V8;
static boolean __aa_V9;
static boolean __aa_V10;
static boolean __aa_V11;
static orcStrlPtr __aa_V12;
static boolean __aa_V13;
static boolean __aa_V14;
static boolean __aa_V15;
static boolean __aa_V16;
static boolean __aa_V17;
static orcStrlPtr __aa_V18;
static boolean __aa_V19;
static boolean __aa_V20;
static boolean __aa_V21;
static boolean __aa_V22;
static boolean __aa_V23;
static boolean __aa_V24;
static boolean __aa_V25;
static boolean __aa_V26;
static boolean __aa_V27;
static boolean __aa_V28;
static orcStrlPtr __aa_V29;
static orcStrlPtr __aa_V30;
static orcStrlPtr __aa_V31;
static orcStrlPtr __aa_V32;
static orcStrlPtr __aa_V33;
static orcStrlPtr __aa_V34;
static orcStrlPtr __aa_V35;
static orcStrlPtr __aa_V36;
static orcStrlPtr __aa_V37;
static orcStrlPtr __aa_V38;
static orcStrlPtr __aa_V39;
static boolean __aa_V40;

static unsigned short __aa_HaltList[43];
static unsigned short __aa_AwaitedList[90];
static unsigned short __aa_EmittedList[90];
static unsigned short __aa_StartedList[1];
static unsigned short __aa_KilledList[1];
static unsigned short __aa_SuspendedList[1];
static unsigned short __aa_ActiveList[1];
static unsigned short __aa_AllAwaitedList[90]={26,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25};


/* INPUT FUNCTIONS */

void aa_I_Prr_Start (orcStrlPtr __V) {
__WriteVariable(0);
_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
void aa_IS_Prr_Start (string __V) {
__WriteVariable(0);
_text_to_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
void aa_I_InitOK_Cycab () {
__aa_V2 = _true;
}
void aa_IS_InitOK_Cycab () {
__aa_V2 = _true;
}
void aa_I_CmdStopOK_Cycab () {
__aa_V3 = _true;
}
void aa_IS_CmdStopOK_Cycab () {
__aa_V3 = _true;
}
void aa_I_LooseTargetE () {
__aa_V4 = _true;
}
void aa_IS_LooseTargetE () {
__aa_V4 = _true;
}
void aa_I_USERSTART_CycabStartPost () {
__aa_V5 = _true;
}
void aa_IS_USERSTART_CycabStartPost () {
__aa_V5 = _true;
}
void aa_I_ReadyToStop_Cycab () {
__aa_V6 = _true;
}
void aa_IS_ReadyToStop_Cycab () {
__aa_V6 = _true;
}
void aa_I_EmergencyStopE () {
__aa_V7 = _true;
}
void aa_IS_EmergencyStopE () {
__aa_V7 = _true;
}
void aa_I_ArriveMinPosE () {
__aa_V8 = _true;
}
void aa_IS_ArriveMinPosE () {
__aa_V8 = _true;
}
void aa_I_KillOK_Cycab () {
__aa_V9 = _true;
}
void aa_IS_KillOK_Cycab () {
__aa_V9 = _true;
}
void aa_I_LooseCommE () {
__aa_V10 = _true;
}
void aa_IS_LooseCommE () {
__aa_V10 = _true;
}
void aa_I_LooseCamE () {
__aa_V11 = _true;
}
void aa_IS_LooseCamE () {
__aa_V11 = _true;
}
void aa_I_CycabFollowingRatio_Start (orcStrlPtr __V) {
__WriteVariable(12);
_orcStrlPtr(&__aa_V12,__V);
__aa_V13 = _true;
}
void aa_IS_CycabFollowingRatio_Start (string __V) {
__WriteVariable(12);
_text_to_orcStrlPtr(&__aa_V12,__V);
__aa_V13 = _true;
}
void aa_I_EmStopE () {
__aa_V14 = _true;
}
void aa_IS_EmStopE () {
__aa_V14 = _true;
}
void aa_I_BadInputE () {
__aa_V15 = _true;
}
void aa_IS_BadInputE () {
__aa_V15 = _true;
}
void aa_I_USERSTART_CycabFollowingRatio () {
__aa_V16 = _true;
}
void aa_IS_USERSTART_CycabFollowingRatio () {
__aa_V16 = _true;
}
void aa_I_T2_CycabStartPost () {
__aa_V17 = _true;
}
void aa_IS_T2_CycabStartPost () {
__aa_V17 = _true;
}
void aa_I_CycabStartPost_Start (orcStrlPtr __V) {
__WriteVariable(18);
_orcStrlPtr(&__aa_V18,__V);
__aa_V19 = _true;
}
void aa_IS_CycabStartPost_Start (string __V) {
__WriteVariable(18);
_text_to_orcStrlPtr(&__aa_V18,__V);
__aa_V19 = _true;
}
void aa_I_PbMotorE () {
__aa_V20 = _true;
}
void aa_IS_PbMotorE () {
__aa_V20 = _true;
}
void aa_I_T2_CycabFollowingRatio () {
__aa_V21 = _true;
}
void aa_IS_T2_CycabFollowingRatio () {
__aa_V21 = _true;
}
void aa_I_ManualModeE () {
__aa_V22 = _true;
}
void aa_IS_ManualModeE () {
__aa_V22 = _true;
}
void aa_I_ActivateOK_Cycab () {
__aa_V23 = _true;
}
void aa_IS_ActivateOK_Cycab () {
__aa_V23 = _true;
}
void aa_I_DirOverE () {
__aa_V24 = _true;
}
void aa_IS_DirOverE () {
__aa_V24 = _true;
}
void aa_I_ROBOT_FAIL () {
__aa_V25 = _true;
}
void aa_IS_ROBOT_FAIL () {
__aa_V25 = _true;
}
void aa_I_SOFT_FAIL () {
__aa_V26 = _true;
}
void aa_IS_SOFT_FAIL () {
__aa_V26 = _true;
}
void aa_I_SENSOR_FAIL () {
__aa_V27 = _true;
}
void aa_IS_SENSOR_FAIL () {
__aa_V27 = _true;
}
void aa_I_CPU_OVERLOAD () {
__aa_V28 = _true;
}
void aa_IS_CPU_OVERLOAD () {
__aa_V28 = _true;
}

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int aa_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

static int __aa_A1 () {
__aa_GENERIC_TEST(__aa_V1);
}
static int __aa_Check1 [] = {1,0,0};
static int __aa_A2 () {
__aa_GENERIC_TEST(__aa_V2);
}
static int __aa_Check2 [] = {1,0,0};
static int __aa_A3 () {
__aa_GENERIC_TEST(__aa_V3);
}
static int __aa_Check3 [] = {1,0,0};
static int __aa_A4 () {
__aa_GENERIC_TEST(__aa_V4);
}
static int __aa_Check4 [] = {1,0,0};
static int __aa_A5 () {
__aa_GENERIC_TEST(__aa_V5);
}
static int __aa_Check5 [] = {1,0,0};
static int __aa_A6 () {
__aa_GENERIC_TEST(__aa_V6);
}
static int __aa_Check6 [] = {1,0,0};
static int __aa_A7 () {
__aa_GENERIC_TEST(__aa_V7);
}
static int __aa_Check7 [] = {1,0,0};
static int __aa_A8 () {
__aa_GENERIC_TEST(__aa_V8);
}
static int __aa_Check8 [] = {1,0,0};
static int __aa_A9 () {
__aa_GENERIC_TEST(__aa_V9);
}
static int __aa_Check9 [] = {1,0,0};
static int __aa_A10 () {
__aa_GENERIC_TEST(__aa_V10);
}
static int __aa_Check10 [] = {1,0,0};
static int __aa_A11 () {
__aa_GENERIC_TEST(__aa_V11);
}
static int __aa_Check11 [] = {1,0,0};
static int __aa_A12 () {
__aa_GENERIC_TEST(__aa_V13);
}
static int __aa_Check12 [] = {1,0,0};
static int __aa_A13 () {
__aa_GENERIC_TEST(__aa_V14);
}
static int __aa_Check13 [] = {1,0,0};
static int __aa_A14 () {
__aa_GENERIC_TEST(__aa_V15);
}
static int __aa_Check14 [] = {1,0,0};
static int __aa_A15 () {
__aa_GENERIC_TEST(__aa_V16);
}
static int __aa_Check15 [] = {1,0,0};
static int __aa_A16 () {
__aa_GENERIC_TEST(__aa_V17);
}
static int __aa_Check16 [] = {1,0,0};
static int __aa_A17 () {
__aa_GENERIC_TEST(__aa_V19);
}
static int __aa_Check17 [] = {1,0,0};
static int __aa_A18 () {
__aa_GENERIC_TEST(__aa_V20);
}
static int __aa_Check18 [] = {1,0,0};
static int __aa_A19 () {
__aa_GENERIC_TEST(__aa_V21);
}
static int __aa_Check19 [] = {1,0,0};
static int __aa_A20 () {
__aa_GENERIC_TEST(__aa_V22);
}
static int __aa_Check20 [] = {1,0,0};
static int __aa_A21 () {
__aa_GENERIC_TEST(__aa_V23);
}
static int __aa_Check21 [] = {1,0,0};
static int __aa_A22 () {
__aa_GENERIC_TEST(__aa_V24);
}
static int __aa_Check22 [] = {1,0,0};
static int __aa_A23 () {
__aa_GENERIC_TEST(__aa_V25);
}
static int __aa_Check23 [] = {1,0,0};
static int __aa_A24 () {
__aa_GENERIC_TEST(__aa_V26);
}
static int __aa_Check24 [] = {1,0,0};
static int __aa_A25 () {
__aa_GENERIC_TEST(__aa_V27);
}
static int __aa_Check25 [] = {1,0,0};
static int __aa_A26 () {
__aa_GENERIC_TEST(__aa_V28);
}
static int __aa_Check26 [] = {1,0,0};

/* OUTPUT ACTIONS */

static void __aa_A27 () {
#ifdef __OUTPUT
aa_O_Activate(__aa_V29);
#endif
__AppendToList(__aa_EmittedList,26);
}
static int __aa_Check27 [] = {1,0,0};
static void __aa_A28 () {
#ifdef __OUTPUT
aa_O_ActivateCycabFollowingRatio_Cycab(__aa_V30);
#endif
__AppendToList(__aa_EmittedList,27);
}
static int __aa_Check28 [] = {1,0,0};
static void __aa_A29 () {
#ifdef __OUTPUT
aa_O_CycabFollowingRatioTransite(__aa_V31);
#endif
__AppendToList(__aa_EmittedList,28);
}
static int __aa_Check29 [] = {1,0,0};
static void __aa_A30 () {
#ifdef __OUTPUT
aa_O_Abort_CycabFollowingRatio(__aa_V32);
#endif
__AppendToList(__aa_EmittedList,29);
}
static int __aa_Check30 [] = {1,0,0};
static void __aa_A31 () {
#ifdef __OUTPUT
aa_O_STARTED_CycabFollowingRatio();
#endif
__AppendToList(__aa_EmittedList,30);
}
static int __aa_Check31 [] = {1,0,0};
static void __aa_A32 () {
#ifdef __OUTPUT
aa_O_GoodEnd_CycabFollowingRatio();
#endif
__AppendToList(__aa_EmittedList,31);
}
static int __aa_Check32 [] = {1,0,0};
static void __aa_A33 () {
#ifdef __OUTPUT
aa_O_GoodEnd_Urgence();
#endif
__AppendToList(__aa_EmittedList,32);
}
static int __aa_Check33 [] = {1,0,0};
static void __aa_A34 () {
#ifdef __OUTPUT
aa_O_STARTED_Urgence();
#endif
__AppendToList(__aa_EmittedList,33);
}
static int __aa_Check34 [] = {1,0,0};
static void __aa_A35 () {
#ifdef __OUTPUT
aa_O_Abort_Urgence();
#endif
__AppendToList(__aa_EmittedList,34);
}
static int __aa_Check35 [] = {1,0,0};
static void __aa_A36 () {
#ifdef __OUTPUT
aa_O_ActivateCycabStartPost_Cycab(__aa_V33);
#endif
__AppendToList(__aa_EmittedList,35);
}
static int __aa_Check36 [] = {1,0,0};
static void __aa_A37 () {
#ifdef __OUTPUT
aa_O_CycabStartPostTransite(__aa_V34);
#endif
__AppendToList(__aa_EmittedList,36);
}
static int __aa_Check37 [] = {1,0,0};
static void __aa_A38 () {
#ifdef __OUTPUT
aa_O_Abort_CycabStartPost(__aa_V35);
#endif
__AppendToList(__aa_EmittedList,37);
}
static int __aa_Check38 [] = {1,0,0};
static void __aa_A39 () {
#ifdef __OUTPUT
aa_O_STARTED_CycabStartPost();
#endif
__AppendToList(__aa_EmittedList,38);
}
static int __aa_Check39 [] = {1,0,0};
static void __aa_A40 () {
#ifdef __OUTPUT
aa_O_GoodEnd_CycabStartPost();
#endif
__AppendToList(__aa_EmittedList,39);
}
static int __aa_Check40 [] = {1,0,0};
static void __aa_A41 () {
#ifdef __OUTPUT
aa_O_EndKill(__aa_V36);
#endif
__AppendToList(__aa_EmittedList,40);
}
static int __aa_Check41 [] = {1,0,0};
static void __aa_A42 () {
#ifdef __OUTPUT
aa_O_FinBF(__aa_V37);
#endif
__AppendToList(__aa_EmittedList,41);
}
static int __aa_Check42 [] = {1,0,0};
static void __aa_A43 () {
#ifdef __OUTPUT
aa_O_FinT3(__aa_V38);
#endif
__AppendToList(__aa_EmittedList,42);
}
static int __aa_Check43 [] = {1,0,0};
static void __aa_A44 () {
#ifdef __OUTPUT
aa_O_GoodEndRPr();
#endif
__AppendToList(__aa_EmittedList,43);
}
static int __aa_Check44 [] = {1,0,0};
static void __aa_A45 () {
#ifdef __OUTPUT
aa_O_T3RPr();
#endif
__AppendToList(__aa_EmittedList,44);
}
static int __aa_Check45 [] = {1,0,0};

/* ASSIGNMENTS */

static void __aa_A46 () {
__aa_V1 = _false;
}
static int __aa_Check46 [] = {1,0,1,1};
static void __aa_A47 () {
__aa_V2 = _false;
}
static int __aa_Check47 [] = {1,0,1,2};
static void __aa_A48 () {
__aa_V3 = _false;
}
static int __aa_Check48 [] = {1,0,1,3};
static void __aa_A49 () {
__aa_V4 = _false;
}
static int __aa_Check49 [] = {1,0,1,4};
static void __aa_A50 () {
__aa_V5 = _false;
}
static int __aa_Check50 [] = {1,0,1,5};
static void __aa_A51 () {
__aa_V6 = _false;
}
static int __aa_Check51 [] = {1,0,1,6};
static void __aa_A52 () {
__aa_V7 = _false;
}
static int __aa_Check52 [] = {1,0,1,7};
static void __aa_A53 () {
__aa_V8 = _false;
}
static int __aa_Check53 [] = {1,0,1,8};
static void __aa_A54 () {
__aa_V9 = _false;
}
static int __aa_Check54 [] = {1,0,1,9};
static void __aa_A55 () {
__aa_V10 = _false;
}
static int __aa_Check55 [] = {1,0,1,10};
static void __aa_A56 () {
__aa_V11 = _false;
}
static int __aa_Check56 [] = {1,0,1,11};
static void __aa_A57 () {
__aa_V13 = _false;
}
static int __aa_Check57 [] = {1,0,1,13};
static void __aa_A58 () {
__aa_V14 = _false;
}
static int __aa_Check58 [] = {1,0,1,14};
static void __aa_A59 () {
__aa_V15 = _false;
}
static int __aa_Check59 [] = {1,0,1,15};
static void __aa_A60 () {
__aa_V16 = _false;
}
static int __aa_Check60 [] = {1,0,1,16};
static void __aa_A61 () {
__aa_V17 = _false;
}
static int __aa_Check61 [] = {1,0,1,17};
static void __aa_A62 () {
__aa_V19 = _false;
}
static int __aa_Check62 [] = {1,0,1,19};
static void __aa_A63 () {
__aa_V20 = _false;
}
static int __aa_Check63 [] = {1,0,1,20};
static void __aa_A64 () {
__aa_V21 = _false;
}
static int __aa_Check64 [] = {1,0,1,21};
static void __aa_A65 () {
__aa_V22 = _false;
}
static int __aa_Check65 [] = {1,0,1,22};
static void __aa_A66 () {
__aa_V23 = _false;
}
static int __aa_Check66 [] = {1,0,1,23};
static void __aa_A67 () {
__aa_V24 = _false;
}
static int __aa_Check67 [] = {1,0,1,24};
static void __aa_A68 () {
__aa_V25 = _false;
}
static int __aa_Check68 [] = {1,0,1,25};
static void __aa_A69 () {
__aa_V26 = _false;
}
static int __aa_Check69 [] = {1,0,1,26};
static void __aa_A70 () {
__aa_V27 = _false;
}
static int __aa_Check70 [] = {1,0,1,27};
static void __aa_A71 () {
__aa_V28 = _false;
}
static int __aa_Check71 [] = {1,0,1,28};
static void __aa_A72 () {
_orcStrlPtr(&__aa_V36,__aa_V39);
}
static int __aa_Check72 [] = {1,1,39,1,36};
static void __aa_A73 () {
_orcStrlPtr(&__aa_V37,__aa_V0);
}
static int __aa_Check73 [] = {1,1,0,1,37};
static void __aa_A74 () {
_orcStrlPtr(&__aa_V38,__aa_V0);
}
static int __aa_Check74 [] = {1,1,0,1,38};
static void __aa_A75 () {
_orcStrlPtr(&__aa_V32,__aa_V12);
}
static int __aa_Check75 [] = {1,1,12,1,32};
static void __aa_A76 () {
_orcStrlPtr(&__aa_V30,__aa_V12);
}
static int __aa_Check76 [] = {1,1,12,1,30};
static void __aa_A77 () {
_orcStrlPtr(&__aa_V39,__aa_V12);
}
static int __aa_Check77 [] = {1,1,12,1,39};
static void __aa_A78 () {
_orcStrlPtr(&__aa_V31,__aa_V12);
}
static int __aa_Check78 [] = {1,1,12,1,31};
static void __aa_A79 () {
_orcStrlPtr(&__aa_V31,__aa_V12);
}
static int __aa_Check79 [] = {1,1,12,1,31};
static void __aa_A80 () {
_orcStrlPtr(&__aa_V31,__aa_V12);
}
static int __aa_Check80 [] = {1,1,12,1,31};
static void __aa_A81 () {
_orcStrlPtr(&__aa_V31,__aa_V12);
}
static int __aa_Check81 [] = {1,1,12,1,31};
static void __aa_A82 () {
_orcStrlPtr(&__aa_V35,__aa_V18);
}
static int __aa_Check82 [] = {1,1,18,1,35};
static void __aa_A83 () {
_orcStrlPtr(&__aa_V33,__aa_V18);
}
static int __aa_Check83 [] = {1,1,18,1,33};
static void __aa_A84 () {
_orcStrlPtr(&__aa_V39,__aa_V18);
}
static int __aa_Check84 [] = {1,1,18,1,39};
static void __aa_A85 () {
_orcStrlPtr(&__aa_V34,__aa_V18);
}
static int __aa_Check85 [] = {1,1,18,1,34};
static void __aa_A86 () {
_orcStrlPtr(&__aa_V34,__aa_V18);
}
static int __aa_Check86 [] = {1,1,18,1,34};
static void __aa_A87 () {
_orcStrlPtr(&__aa_V34,__aa_V18);
}
static int __aa_Check87 [] = {1,1,18,1,34};
static void __aa_A88 () {
_orcStrlPtr(&__aa_V34,__aa_V18);
}
static int __aa_Check88 [] = {1,1,18,1,34};
static void __aa_A89 () {
_orcStrlPtr(&__aa_V36,__aa_V39);
}
static int __aa_Check89 [] = {1,1,39,1,36};

/* PROCEDURE CALLS */

static void __aa_A90 () {
CycabStartPost_parameter(__aa_V18);
}
static int __aa_Check90 [] = {1,1,18,0};
static void __aa_A91 () {
CycabStartPost_controler(__aa_V18);
}
static int __aa_Check91 [] = {1,1,18,0};
static void __aa_A92 () {
CycabFollowingRatio_parameter(__aa_V12);
}
static int __aa_Check92 [] = {1,1,12,0};
static void __aa_A93 () {
CycabFollowingRatio_controler(__aa_V12);
}
static int __aa_Check93 [] = {1,1,12,0};

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

static void __aa_A94 () {
;
__ResetVariable(0);
}
static int __aa_Check94 [] = {1,0,0};
static void __aa_A95 () {
;
__ResetVariable(12);
}
static int __aa_Check95 [] = {1,0,0};
static void __aa_A96 () {
;
__ResetVariable(18);
}
static int __aa_Check96 [] = {1,0,0};
static void __aa_A97 () {
;
__ResetVariable(29);
}
static int __aa_Check97 [] = {1,0,0};
static void __aa_A98 () {
;
__ResetVariable(30);
}
static int __aa_Check98 [] = {1,0,0};
static void __aa_A99 () {
;
__ResetVariable(31);
}
static int __aa_Check99 [] = {1,0,0};
static void __aa_A100 () {
;
__ResetVariable(32);
}
static int __aa_Check100 [] = {1,0,0};
static void __aa_A101 () {
;
__ResetVariable(33);
}
static int __aa_Check101 [] = {1,0,0};
static void __aa_A102 () {
;
__ResetVariable(34);
}
static int __aa_Check102 [] = {1,0,0};
static void __aa_A103 () {
;
__ResetVariable(35);
}
static int __aa_Check103 [] = {1,0,0};
static void __aa_A104 () {
;
__ResetVariable(36);
}
static int __aa_Check104 [] = {1,0,0};
static void __aa_A105 () {
;
__ResetVariable(37);
}
static int __aa_Check105 [] = {1,0,0};
static void __aa_A106 () {
;
__ResetVariable(38);
}
static int __aa_Check106 [] = {1,0,0};
static void __aa_A107 () {
;
__ResetVariable(39);
}
static int __aa_Check107 [] = {1,0,0};
static void __aa_A108 () {
;
__ResetVariable(40);
}
static int __aa_Check108 [] = {1,0,0};

/* ACTION SEQUENCES */


static int *__aa_CheckArray[] = {
0,
__aa_Check1,
__aa_Check2,
__aa_Check3,
__aa_Check4,
__aa_Check5,
__aa_Check6,
__aa_Check7,
__aa_Check8,
__aa_Check9,
__aa_Check10,
__aa_Check11,
__aa_Check12,
__aa_Check13,
__aa_Check14,
__aa_Check15,
__aa_Check16,
__aa_Check17,
__aa_Check18,
__aa_Check19,
__aa_Check20,
__aa_Check21,
__aa_Check22,
__aa_Check23,
__aa_Check24,
__aa_Check25,
__aa_Check26,
__aa_Check27,
__aa_Check28,
__aa_Check29,
__aa_Check30,
__aa_Check31,
__aa_Check32,
__aa_Check33,
__aa_Check34,
__aa_Check35,
__aa_Check36,
__aa_Check37,
__aa_Check38,
__aa_Check39,
__aa_Check40,
__aa_Check41,
__aa_Check42,
__aa_Check43,
__aa_Check44,
__aa_Check45,
__aa_Check46,
__aa_Check47,
__aa_Check48,
__aa_Check49,
__aa_Check50,
__aa_Check51,
__aa_Check52,
__aa_Check53,
__aa_Check54,
__aa_Check55,
__aa_Check56,
__aa_Check57,
__aa_Check58,
__aa_Check59,
__aa_Check60,
__aa_Check61,
__aa_Check62,
__aa_Check63,
__aa_Check64,
__aa_Check65,
__aa_Check66,
__aa_Check67,
__aa_Check68,
__aa_Check69,
__aa_Check70,
__aa_Check71,
__aa_Check72,
__aa_Check73,
__aa_Check74,
__aa_Check75,
__aa_Check76,
__aa_Check77,
__aa_Check78,
__aa_Check79,
__aa_Check80,
__aa_Check81,
__aa_Check82,
__aa_Check83,
__aa_Check84,
__aa_Check85,
__aa_Check86,
__aa_Check87,
__aa_Check88,
__aa_Check89,
__aa_Check90,
__aa_Check91,
__aa_Check92,
__aa_Check93,
__aa_Check94,
__aa_Check95,
__aa_Check96,
__aa_Check97,
__aa_Check98,
__aa_Check99,
__aa_Check100,
__aa_Check101,
__aa_Check102,
__aa_Check103,
__aa_Check104,
__aa_Check105,
__aa_Check106,
__aa_Check107,
__aa_Check108
};
static int **__aa_PCheckArray =  __aa_CheckArray;

/* INIT FUNCTION */

#ifndef NO_INIT
void aa_initialize () {
}
#endif

/* SHOW VARIABLE FUNCTION */

char* __aa_show_variable (int __V) {
extern struct __VariableEntry __aa_VariableTable[];
struct __VariableEntry* p_var = &__aa_VariableTable[__V];
if (p_var->type_code < 0) {return __PredefinedTypeToText(p_var->type_code, p_var->p_variable);
} else {
switch (p_var->type_code) {
case 0: return _orcStrlPtr_to_text(*(orcStrlPtr*)p_var->p_variable);
default: return 0;
}
}
}

/* SET VARIABLE FUNCTION */

static void __aa_set_variable(int __Type, char* __pVar, char* __Text) {
}

/* CHECK VALUE FUNCTION */

static int __aa_check_value (int __Type, char* __Text) {
return 0;
}

/* SIMULATION TABLES */

struct __InstanceEntry __aa_InstanceTable [] = {
{"aa",0,"/home/mouflon/jroussel/Orccad/Spec/RobotProcedures/Urgence.PC/common/","aa.strl",{1,1,1,0},{1,135,1,0},{0,0,0,0}},
{"RTs_Init",0,"/home/mouflon/jroussel/Orccad/Spec/RobotProcedures/Urgence.PC/common/","aa.strl",{1,159,1,1},{1,176,1,1},{1,104,1,0}},
{"CycabFollowingRatio",0,"/home/mouflon/jroussel/Orccad/Spec/RobotProcedures/Urgence.PC/common/","CycabFollowingRatio.strl",{1,1,1,2},{1,127,1,2},{1,106,1,0}},
{"CycabStartPost",0,"/home/mouflon/jroussel/Orccad/Spec/RobotProcedures/Urgence.PC/common/","CycabStartPost.strl",{1,1,1,3},{1,127,1,3},{1,108,1,0}},
{"Urgence",0,"/home/mouflon/jroussel/Orccad/Spec/RobotProcedures/Urgence.PC/common/","Urgence.strl",{1,1,1,4},{1,136,1,4},{1,110,1,0}},
{"TransiteRobot",0,"/home/mouflon/jroussel/Orccad/Spec/RobotProcedures/Urgence.PC/common/","aa.strl",{1,138,1,5},{1,156,1,5},{1,112,1,0}},
};

struct __SignalEntry __aa_SignalTable [] = {
{"Prr_Start",1,0,0,{1,4,7,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"InitOK_Cycab",33,0,0,{1,5,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"CmdStopOK_Cycab",33,0,0,{1,6,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"LooseTargetE",33,0,0,{1,7,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"USERSTART_CycabStartPost",33,0,0,{1,8,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ReadyToStop_Cycab",33,0,0,{1,9,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"EmergencyStopE",33,0,0,{1,10,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ArriveMinPosE",33,0,0,{1,11,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"KillOK_Cycab",33,0,0,{1,12,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"LooseCommE",33,0,0,{1,13,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"LooseCamE",33,0,0,{1,14,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"CycabFollowingRatio_Start",1,12,0,{1,15,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"EmStopE",33,0,0,{1,16,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BadInputE",33,0,0,{1,17,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"USERSTART_CycabFollowingRatio",33,0,0,{1,18,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2_CycabStartPost",33,0,0,{1,19,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"CycabStartPost_Start",1,18,0,{1,20,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"PbMotorE",33,0,0,{1,21,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2_CycabFollowingRatio",33,0,0,{1,22,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ManualModeE",33,0,0,{1,23,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ActivateOK_Cycab",33,0,0,{1,24,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"DirOverE",33,0,0,{1,25,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ROBOT_FAIL",33,0,0,{1,26,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"SOFT_FAIL",33,0,0,{1,27,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"SENSOR_FAIL",33,0,0,{1,28,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"CPU_OVERLOAD",33,0,0,{1,29,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Activate",2,29,0,{1,31,8,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ActivateCycabFollowingRatio_Cycab",2,30,0,{1,31,33,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"CycabFollowingRatioTransite",2,31,0,{1,32,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_CycabFollowingRatio",2,32,0,{1,33,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"STARTED_CycabFollowingRatio",34,0,0,{1,34,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEnd_CycabFollowingRatio",34,0,0,{1,35,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEnd_Urgence",34,0,0,{1,36,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"STARTED_Urgence",34,0,0,{1,37,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_Urgence",34,0,0,{1,38,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ActivateCycabStartPost_Cycab",2,33,0,{1,39,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"CycabStartPostTransite",2,34,0,{1,40,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_CycabStartPost",2,35,0,{1,41,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"STARTED_CycabStartPost",34,0,0,{1,42,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEnd_CycabStartPost",34,0,0,{1,43,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"EndKill",2,36,0,{1,44,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FinBF",2,37,0,{1,45,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FinT3",2,38,0,{1,46,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"GoodEndRPr",34,0,0,{1,47,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3RPr",34,0,0,{1,48,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2_EmergencyStopE",40,0,0,{1,76,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"START_CycabFollowingRatio",40,0,0,{1,77,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_CycabFollowingRatio",40,0,0,{1,78,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_Local_CycabFollowingRatio",40,0,0,{1,79,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"START_Urgence",40,0,0,{1,80,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_Local_Urgence",40,0,0,{1,81,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"START_CycabStartPost",40,0,0,{1,82,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Prev_rt_Cycab",8,39,0,{1,83,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"StartTransite_Cycab",40,0,0,{1,84,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FinTransite_Cycab",40,0,0,{1,85,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"FirstRT_Cycab",40,0,0,{1,86,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Reparam_Cycab",8,40,0,{1,87,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_CycabStartPost",40,0,0,{1,88,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2_LooseTargetE",40,0,0,{1,89,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort_Local_CycabStartPost",40,0,0,{1,90,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_CycabStartPost",40,0,0,{1,91,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_CycabFollowingRatio",40,0,0,{1,92,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2_EmStopE",40,0,0,{1,93,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_Urgence",40,0,0,{1,94,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_Urgence",40,0,0,{1,95,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"ReadyToStart_Cycab",40,0,0,{1,96,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2",40,0,0,{1,97,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_controle_prr",40,0,0,{1,98,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_controle_prr",40,0,0,{1,99,4,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_prr",48,0,0,{1,101,6,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_prr",48,0,0,{1,102,6,0},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3",48,0,0,{1,51,6,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort",48,0,0,{1,55,8,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2",48,0,0,{1,76,8,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF",48,0,0,{1,77,9,2},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3",48,0,0,{1,51,6,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort",48,0,0,{1,55,8,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T2",48,0,0,{1,76,8,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF",48,0,0,{1,77,9,3},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"L_T2_EmStopE",40,0,0,{1,48,8,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"L_T2_EmergencyStopE",40,0,0,{1,49,2,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"L_T2_LooseTargetE",40,0,0,{1,50,2,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"dummylocalsignal",40,0,0,{1,51,3,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"Abort",48,0,0,{1,57,6,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"T3_controle_Urgence",48,0,0,{1,58,7,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"BF_controle_Urgence",48,0,0,{1,59,8,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"E_T2_EmergencyStopE",48,0,0,{1,63,6,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"E_T2_LooseTargetE",48,0,0,{1,65,8,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL},
{"E_T2_EmStopE",48,0,0,{1,67,10,4},0,(void*) NULL,0,(void*) NULL,0,(void*) NULL}};

struct __InputEntry __aa_InputTable [] = {
{"Prr_Start",20,"orcStrlPtr",0,0,0,0,_check_orcStrlPtr,aa_IS_Prr_Start,0,{1,4,7,0}},
{"InitOK_Cycab",24,0,0,-1,0,1,0,aa_IS_InitOK_Cycab,0,{1,5,4,0}},
{"CmdStopOK_Cycab",15,0,0,-1,0,2,0,aa_IS_CmdStopOK_Cycab,0,{1,6,4,0}},
{"LooseTargetE",87,0,0,-1,0,3,0,aa_IS_LooseTargetE,0,{1,7,4,0}},
{"USERSTART_CycabStartPost",20,0,0,-1,0,4,0,aa_IS_USERSTART_CycabStartPost,0,{1,8,4,0}},
{"ReadyToStop_Cycab",79,0,0,-1,0,5,0,aa_IS_ReadyToStop_Cycab,0,{1,9,4,0}},
{"EmergencyStopE",4,0,0,-1,0,6,0,aa_IS_EmergencyStopE,0,{1,10,4,0}},
{"ArriveMinPosE",72,0,0,-1,0,7,0,aa_IS_ArriveMinPosE,0,{1,11,4,0}},
{"KillOK_Cycab",16,0,0,-1,0,8,0,aa_IS_KillOK_Cycab,0,{1,12,4,0}},
{"LooseCommE",70,0,0,-1,0,9,0,aa_IS_LooseCommE,0,{1,13,4,0}},
{"LooseCamE",48,0,0,-1,0,10,0,aa_IS_LooseCamE,0,{1,14,4,0}},
{"CycabFollowingRatio_Start",34,"orcStrlPtr",0,0,0,11,_check_orcStrlPtr,aa_IS_CycabFollowingRatio_Start,0,{1,15,4,0}},
{"EmStopE",63,0,0,-1,0,12,0,aa_IS_EmStopE,0,{1,16,4,0}},
{"BadInputE",52,0,0,-1,0,13,0,aa_IS_BadInputE,0,{1,17,4,0}},
{"USERSTART_CycabFollowingRatio",23,0,0,-1,0,14,0,aa_IS_USERSTART_CycabFollowingRatio,0,{1,18,4,0}},
{"T2_CycabStartPost",43,0,0,-1,0,15,0,aa_IS_T2_CycabStartPost,0,{1,19,4,0}},
{"CycabStartPost_Start",31,"orcStrlPtr",0,0,0,16,_check_orcStrlPtr,aa_IS_CycabStartPost_Start,0,{1,20,4,0}},
{"PbMotorE",69,0,0,-1,0,17,0,aa_IS_PbMotorE,0,{1,21,4,0}},
{"T2_CycabFollowingRatio",46,0,0,-1,0,18,0,aa_IS_T2_CycabFollowingRatio,0,{1,22,4,0}},
{"ManualModeE",54,0,0,-1,0,19,0,aa_IS_ManualModeE,0,{1,23,4,0}},
{"ActivateOK_Cycab",33,0,0,-1,0,20,0,aa_IS_ActivateOK_Cycab,0,{1,24,4,0}},
{"DirOverE",61,0,0,-1,0,21,0,aa_IS_DirOverE,0,{1,25,4,0}},
{"ROBOT_FAIL",62,0,0,-1,0,22,0,aa_IS_ROBOT_FAIL,0,{1,26,4,0}},
{"SOFT_FAIL",89,0,0,-1,0,23,0,aa_IS_SOFT_FAIL,0,{1,27,4,0}},
{"SENSOR_FAIL",45,0,0,-1,0,24,0,aa_IS_SENSOR_FAIL,0,{1,28,4,0}},
{"CPU_OVERLOAD",22,0,0,-1,0,25,0,aa_IS_CPU_OVERLOAD,0,{1,29,4,0}}};

static int __aa_ExclusionEntry0[] = {23,1,2,3,5,6,7,8,9,10,12,11,13,16,15,17,18,19,20,21,22,23,24,25};

struct __ExclusionEntry __aa_ExclusionTable [] = {
{__aa_ExclusionEntry0,{1,51,4,0}}
};

struct __VariableEntry __aa_VariableTable [] = {
{"__aa_V0","V0","orcStrlPtr",0,1,0,(char*)&__aa_V0,"Prr_Start",0,0,0,{1,4,7,0}},
{"__aa_V1","V1","boolean",-2,2,0,(char*)&__aa_V1,"Prr_Start",0,0,0,{1,4,7,0}},
{"__aa_V2","V2","boolean",-2,2,0,(char*)&__aa_V2,"InitOK_Cycab",0,0,0,{1,5,4,0}},
{"__aa_V3","V3","boolean",-2,2,0,(char*)&__aa_V3,"CmdStopOK_Cycab",0,0,0,{1,6,4,0}},
{"__aa_V4","V4","boolean",-2,2,0,(char*)&__aa_V4,"LooseTargetE",0,0,0,{1,7,4,0}},
{"__aa_V5","V5","boolean",-2,2,0,(char*)&__aa_V5,"USERSTART_CycabStartPost",0,0,0,{1,8,4,0}},
{"__aa_V6","V6","boolean",-2,2,0,(char*)&__aa_V6,"ReadyToStop_Cycab",0,0,0,{1,9,4,0}},
{"__aa_V7","V7","boolean",-2,2,0,(char*)&__aa_V7,"EmergencyStopE",0,0,0,{1,10,4,0}},
{"__aa_V8","V8","boolean",-2,2,0,(char*)&__aa_V8,"ArriveMinPosE",0,0,0,{1,11,4,0}},
{"__aa_V9","V9","boolean",-2,2,0,(char*)&__aa_V9,"KillOK_Cycab",0,0,0,{1,12,4,0}},
{"__aa_V10","V10","boolean",-2,2,0,(char*)&__aa_V10,"LooseCommE",0,0,0,{1,13,4,0}},
{"__aa_V11","V11","boolean",-2,2,0,(char*)&__aa_V11,"LooseCamE",0,0,0,{1,14,4,0}},
{"__aa_V12","V12","orcStrlPtr",0,1,0,(char*)&__aa_V12,"CycabFollowingRatio_Start",0,0,0,{1,15,4,0}},
{"__aa_V13","V13","boolean",-2,2,0,(char*)&__aa_V13,"CycabFollowingRatio_Start",0,0,0,{1,15,4,0}},
{"__aa_V14","V14","boolean",-2,2,0,(char*)&__aa_V14,"EmStopE",0,0,0,{1,16,4,0}},
{"__aa_V15","V15","boolean",-2,2,0,(char*)&__aa_V15,"BadInputE",0,0,0,{1,17,4,0}},
{"__aa_V16","V16","boolean",-2,2,0,(char*)&__aa_V16,"USERSTART_CycabFollowingRatio",0,0,0,{1,18,4,0}},
{"__aa_V17","V17","boolean",-2,2,0,(char*)&__aa_V17,"T2_CycabStartPost",0,0,0,{1,19,4,0}},
{"__aa_V18","V18","orcStrlPtr",0,1,0,(char*)&__aa_V18,"CycabStartPost_Start",0,0,0,{1,20,4,0}},
{"__aa_V19","V19","boolean",-2,2,0,(char*)&__aa_V19,"CycabStartPost_Start",0,0,0,{1,20,4,0}},
{"__aa_V20","V20","boolean",-2,2,0,(char*)&__aa_V20,"PbMotorE",0,0,0,{1,21,4,0}},
{"__aa_V21","V21","boolean",-2,2,0,(char*)&__aa_V21,"T2_CycabFollowingRatio",0,0,0,{1,22,4,0}},
{"__aa_V22","V22","boolean",-2,2,0,(char*)&__aa_V22,"ManualModeE",0,0,0,{1,23,4,0}},
{"__aa_V23","V23","boolean",-2,2,0,(char*)&__aa_V23,"ActivateOK_Cycab",0,0,0,{1,24,4,0}},
{"__aa_V24","V24","boolean",-2,2,0,(char*)&__aa_V24,"DirOverE",0,0,0,{1,25,4,0}},
{"__aa_V25","V25","boolean",-2,2,0,(char*)&__aa_V25,"ROBOT_FAIL",0,0,0,{1,26,4,0}},
{"__aa_V26","V26","boolean",-2,2,0,(char*)&__aa_V26,"SOFT_FAIL",0,0,0,{1,27,4,0}},
{"__aa_V27","V27","boolean",-2,2,0,(char*)&__aa_V27,"SENSOR_FAIL",0,0,0,{1,28,4,0}},
{"__aa_V28","V28","boolean",-2,2,0,(char*)&__aa_V28,"CPU_OVERLOAD",0,0,0,{1,29,4,0}},
{"__aa_V29","V29","orcStrlPtr",0,1,0,(char*)&__aa_V29,"Activate",0,0,0,{1,31,8,0}},
{"__aa_V30","V30","orcStrlPtr",0,1,0,(char*)&__aa_V30,"ActivateCycabFollowingRatio_Cycab",0,0,0,{1,31,33,0}},
{"__aa_V31","V31","orcStrlPtr",0,1,0,(char*)&__aa_V31,"CycabFollowingRatioTransite",0,0,0,{1,32,4,0}},
{"__aa_V32","V32","orcStrlPtr",0,1,0,(char*)&__aa_V32,"Abort_CycabFollowingRatio",0,0,0,{1,33,4,0}},
{"__aa_V33","V33","orcStrlPtr",0,1,0,(char*)&__aa_V33,"ActivateCycabStartPost_Cycab",0,0,0,{1,39,4,0}},
{"__aa_V34","V34","orcStrlPtr",0,1,0,(char*)&__aa_V34,"CycabStartPostTransite",0,0,0,{1,40,4,0}},
{"__aa_V35","V35","orcStrlPtr",0,1,0,(char*)&__aa_V35,"Abort_CycabStartPost",0,0,0,{1,41,4,0}},
{"__aa_V36","V36","orcStrlPtr",0,1,0,(char*)&__aa_V36,"EndKill",0,0,0,{1,44,4,0}},
{"__aa_V37","V37","orcStrlPtr",0,1,0,(char*)&__aa_V37,"FinBF",0,0,0,{1,45,4,0}},
{"__aa_V38","V38","orcStrlPtr",0,1,0,(char*)&__aa_V38,"FinT3",0,0,0,{1,46,4,0}},
{"__aa_V39","V39","orcStrlPtr",0,1,0,(char*)&__aa_V39,"Prev_rt_Cycab",0,0,0,{1,83,4,0}},
{"__aa_V40","V40","boolean",-2,1,0,(char*)&__aa_V40,"Reparam_Cycab",0,0,0,{1,87,4,0}}
};

struct __HaltEntry __aa_HaltTable [] = {
{{1,135,1,0}},
{{1,119,1,0}},
{{1,167,2,1}},
{{1,169,2,1}},
{{1,171,2,1}},
{{1,52,9,2}},
{{1,58,4,2}},
{{1,67,4,2}},
{{1,80,5,2}},
{{1,86,4,2}},
{{1,88,4,2}},
{{1,90,4,2}},
{{1,92,4,2}},
{{1,94,4,2}},
{{1,96,4,2}},
{{1,98,4,2}},
{{1,100,4,2}},
{{1,104,4,2}},
{{1,52,9,3}},
{{1,58,4,3}},
{{1,67,4,3}},
{{1,80,5,3}},
{{1,86,4,3}},
{{1,88,4,3}},
{{1,90,4,3}},
{{1,92,4,3}},
{{1,94,4,3}},
{{1,96,4,3}},
{{1,98,4,3}},
{{1,100,4,3}},
{{1,104,4,3}},
{{1,54,1,4}},
{{1,74,11,4}},
{{1,86,11,4}},
{{1,95,5,4}},
{{1,101,3,4}},
{{1,107,1,4}},
{{1,119,6,4}},
{{1,145,3,5}},
{{1,147,6,5}},
{{1,149,8,5}},
{{1,152,6,5}}
};


static void __aa__reset_input () {
__aa_V1 = _false;
__aa_V2 = _false;
__aa_V3 = _false;
__aa_V4 = _false;
__aa_V5 = _false;
__aa_V6 = _false;
__aa_V7 = _false;
__aa_V8 = _false;
__aa_V9 = _false;
__aa_V10 = _false;
__aa_V11 = _false;
__aa_V13 = _false;
__aa_V14 = _false;
__aa_V15 = _false;
__aa_V16 = _false;
__aa_V17 = _false;
__aa_V19 = _false;
__aa_V20 = _false;
__aa_V21 = _false;
__aa_V22 = _false;
__aa_V23 = _false;
__aa_V24 = _false;
__aa_V25 = _false;
__aa_V26 = _false;
__aa_V27 = _false;
__aa_V28 = _false;
}


/* MODULE DATA FOR SIMULATION */

int aa();
int aa_reset();

static struct __ModuleEntry __aa_ModuleData = {
"Simulation interface release 5","aa",
6,0,89,26,0,0,19,28,16,0,1,41,0,42,0,0,0,
__aa_HaltList,
__aa_AwaitedList,
__aa_EmittedList,
__aa_StartedList,
__aa_KilledList,
__aa_SuspendedList,
__aa_ActiveList,
0,0,
aa_initialize,aa,aa_reset,
__aa_show_variable,__aa_set_variable,__aa_check_value,0,
__aa_InstanceTable,
0,
__aa_SignalTable,__aa_InputTable,0,
0,__aa_ExclusionTable,
__aa_VariableTable,
0,
__aa_HaltTable,
0};

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __aa_R[42] = {_true,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int aa () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[141];

__aa_ModuleData.awaited_list = __aa_AwaitedList;
__ResetModuleEntryBeforeReaction();
E[0] = (__aa_R[22]&&!(__aa_R[0]));
E[1] = (E[0]&&(__CheckVariables(__aa_CheckArray[7]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7()));
E[2] = (__aa_R[23]&&!(__aa_R[0]));
E[3] = (E[2]&&(__CheckVariables(__aa_CheckArray[13]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13()));
E[4] = (__aa_R[24]&&!(__aa_R[0]));
E[5] = (E[4]&&(__CheckVariables(__aa_CheckArray[4]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4()));
E[6] = (((((((((__aa_R[21]||__aa_R[22])||__aa_R[23])||__aa_R[24])||__aa_R[25])||__aa_R[26])||__aa_R[27])||__aa_R[28])||__aa_R[29])||__aa_R[30]);
E[7] = (__aa_R[19]&&!(__aa_R[0]));
E[8] = (__aa_R[34]&&!(__aa_R[0]));
E[9] = (__aa_R[10]&&!(__aa_R[0]));
E[10] = (E[9]&&(__CheckVariables(__aa_CheckArray[13]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13()));
E[11] = (E[10]||E[3]);
if (E[11]) {
__AppendToList(__aa_EmittedList,62);
}
E[12] = (E[8]&&E[11]);
E[13] = (__aa_R[35]&&!(__aa_R[0]));
E[14] = (__aa_R[11]&&!(__aa_R[0]));
E[15] = (E[14]&&(__CheckVariables(__aa_CheckArray[4]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4()));
E[16] = (E[15]||E[5]);
if (E[16]) {
__AppendToList(__aa_EmittedList,58);
}
E[17] = (E[13]&&E[16]);
E[18] = (__aa_R[36]&&!(__aa_R[0]));
E[19] = (__aa_R[9]&&!(__aa_R[0]));
E[20] = (E[19]&&(__CheckVariables(__aa_CheckArray[7]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7()));
E[21] = (E[20]||E[1]);
if (E[21]) {
__AppendToList(__aa_EmittedList,45);
}
E[22] = (E[18]&&E[21]);
E[23] = ((E[12]||E[17])||E[22]);
if (E[23]) {
__AppendToList(__aa_EmittedList,59);
}
E[24] = (__aa_R[19]&&((E[7]&&!(E[23]))));
E[25] = (E[24]&&(__CheckVariables(__aa_CheckArray[2]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2()));
E[26] = (__aa_R[38]&&!(__aa_R[0]));
E[27] = (__aa_R[6]&&!(__aa_R[0]));
E[28] = ((E[12]||E[17])||E[22]);
if (E[28]) {
__AppendToList(__aa_EmittedList,48);
}
E[29] = (__aa_R[6]&&((E[27]&&!(E[28]))));
E[30] = (E[29]&&(__CheckVariables(__aa_CheckArray[2]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2()));
E[31] = (E[30]||E[25]);
if (E[31]) {
__AppendToList(__aa_EmittedList,65);
}
E[32] = (E[26]&&E[31]);
E[33] = (__aa_R[41]&&!(__aa_R[0]));
E[34] = (E[33]&&(__CheckVariables(__aa_CheckArray[3]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3()));
E[35] = (E[32]||E[34]);
if (E[35]) {
__AppendToList(__aa_EmittedList,54);
}
E[36] = (__aa_R[20]&&!(__aa_R[0]));
E[37] = (__aa_R[20]&&((E[36]&&!(E[23]))));
E[38] = (((E[25]&&E[35]))||((E[37]&&E[35])));
if (E[38]) {
__AppendToList(__aa_EmittedList,35);
}
if (E[38]) {
__CheckVariables(__aa_CheckArray[83]);__aa_A83();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A83\n");
#endif
}
if (E[38]) {
__CheckVariables(__aa_CheckArray[84]);__aa_A84();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A84\n");
#endif
}
E[39] = (__aa_R[21]&&!(__aa_R[0]));
E[40] = (E[38]||((__aa_R[21]&&((E[39]&&!(E[23]))))));
E[41] = (((E[6]&&!(__aa_R[21])))||E[40]);
E[0] = (E[0]&&!((__CheckVariables(__aa_CheckArray[7]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7())));
E[0] = (E[38]||((__aa_R[22]&&E[0])));
E[42] = (((E[6]&&!(__aa_R[22])))||E[0]);
E[43] = (E[42]||E[1]);
E[2] = (E[2]&&!((__CheckVariables(__aa_CheckArray[13]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13())));
E[2] = (E[38]||((__aa_R[23]&&E[2])));
E[44] = (((E[6]&&!(__aa_R[23])))||E[2]);
E[45] = (E[44]||E[3]);
E[4] = (E[4]&&!((__CheckVariables(__aa_CheckArray[4]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4())));
E[4] = (E[38]||((__aa_R[24]&&E[4])));
E[46] = (((E[6]&&!(__aa_R[24])))||E[4]);
E[47] = (E[46]||E[5]);
E[48] = (__aa_R[25]&&!(__aa_R[0]));
E[49] = (E[48]&&!((__CheckVariables(__aa_CheckArray[22]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22())));
E[49] = (E[38]||((__aa_R[25]&&E[49])));
E[50] = (((E[6]&&!(__aa_R[25])))||E[49]);
E[51] = (__aa_R[26]&&!(__aa_R[0]));
E[52] = (E[51]&&!((__CheckVariables(__aa_CheckArray[18]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__aa_A18())));
E[52] = (E[38]||((__aa_R[26]&&E[52])));
E[53] = (((E[6]&&!(__aa_R[26])))||E[52]);
E[54] = (__aa_R[27]&&!(__aa_R[0]));
E[55] = (E[54]&&!((__CheckVariables(__aa_CheckArray[14]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14())));
E[55] = (E[38]||((__aa_R[27]&&E[55])));
E[56] = (((E[6]&&!(__aa_R[27])))||E[55]);
E[57] = (__aa_R[28]&&!(__aa_R[0]));
E[58] = (E[57]&&!((__CheckVariables(__aa_CheckArray[10]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10())));
E[58] = (E[38]||((__aa_R[28]&&E[58])));
E[59] = (((E[6]&&!(__aa_R[28])))||E[58]);
E[60] = (__aa_R[29]&&!(__aa_R[0]));
E[61] = (E[60]&&!((__CheckVariables(__aa_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11())));
E[61] = (E[38]||((__aa_R[29]&&E[61])));
E[62] = (((E[6]&&!(__aa_R[29])))||E[61]);
E[63] = (__aa_R[30]&&!(__aa_R[0]));
E[64] = (E[63]&&!((__CheckVariables(__aa_CheckArray[8]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8())));
E[64] = (E[38]||((__aa_R[30]&&E[64])));
E[65] = (((E[6]&&!(__aa_R[30])))||E[64]);
E[63] = (E[63]&&(__CheckVariables(__aa_CheckArray[8]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8()));
if (E[63]) {
__AppendToList(__aa_EmittedList,78);
}
E[66] = (E[65]||E[63]);
E[67] = (((((((((((((E[1]||E[3])||E[5]))&&E[41])&&E[43])&&E[45])&&E[47])&&E[50])&&E[53])&&E[56])&&E[59])&&E[62])&&E[66]);
E[68] = ((__CheckVariables(__aa_CheckArray[16]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 16\n"),
#endif
__aa_A16())||E[67]);
if (E[68]) {
__AppendToList(__aa_EmittedList,15);
}
E[69] = (((((((((__aa_R[8]||__aa_R[9])||__aa_R[10])||__aa_R[11])||__aa_R[12])||__aa_R[13])||__aa_R[14])||__aa_R[15])||__aa_R[16])||__aa_R[17]);
E[70] = (__aa_R[7]&&!(__aa_R[0]));
E[71] = (__aa_R[7]&&((E[70]&&!(E[28]))));
E[72] = (((E[30]&&E[35]))||((E[71]&&E[35])));
if (E[72]) {
__AppendToList(__aa_EmittedList,27);
}
if (E[72]) {
__CheckVariables(__aa_CheckArray[76]);__aa_A76();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A76\n");
#endif
}
if (E[72]) {
__CheckVariables(__aa_CheckArray[77]);__aa_A77();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A77\n");
#endif
}
E[73] = (__aa_R[8]&&!(__aa_R[0]));
E[74] = (E[72]||((__aa_R[8]&&((E[73]&&!(E[28]))))));
E[75] = (((E[69]&&!(__aa_R[8])))||E[74]);
E[19] = (E[19]&&!((__CheckVariables(__aa_CheckArray[7]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7())));
E[19] = (E[72]||((__aa_R[9]&&E[19])));
E[76] = (((E[69]&&!(__aa_R[9])))||E[19]);
E[77] = (E[76]||E[20]);
E[9] = (E[9]&&!((__CheckVariables(__aa_CheckArray[13]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13())));
E[9] = (E[72]||((__aa_R[10]&&E[9])));
E[78] = (((E[69]&&!(__aa_R[10])))||E[9]);
E[79] = (E[78]||E[10]);
E[14] = (E[14]&&!((__CheckVariables(__aa_CheckArray[4]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__aa_A4())));
E[14] = (E[72]||((__aa_R[11]&&E[14])));
E[80] = (((E[69]&&!(__aa_R[11])))||E[14]);
E[81] = (E[80]||E[15]);
E[82] = (__aa_R[12]&&!(__aa_R[0]));
E[83] = (E[82]&&!((__CheckVariables(__aa_CheckArray[22]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22())));
E[83] = (E[72]||((__aa_R[12]&&E[83])));
E[84] = (((E[69]&&!(__aa_R[12])))||E[83]);
E[85] = (__aa_R[13]&&!(__aa_R[0]));
E[86] = (E[85]&&!((__CheckVariables(__aa_CheckArray[18]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__aa_A18())));
E[86] = (E[72]||((__aa_R[13]&&E[86])));
E[87] = (((E[69]&&!(__aa_R[13])))||E[86]);
E[88] = (__aa_R[14]&&!(__aa_R[0]));
E[89] = (E[88]&&!((__CheckVariables(__aa_CheckArray[14]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14())));
E[89] = (E[72]||((__aa_R[14]&&E[89])));
E[90] = (((E[69]&&!(__aa_R[14])))||E[89]);
E[91] = (__aa_R[15]&&!(__aa_R[0]));
E[92] = (E[91]&&!((__CheckVariables(__aa_CheckArray[10]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10())));
E[92] = (E[72]||((__aa_R[15]&&E[92])));
E[93] = (((E[69]&&!(__aa_R[15])))||E[92]);
E[94] = (__aa_R[16]&&!(__aa_R[0]));
E[95] = (E[94]&&!((__CheckVariables(__aa_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11())));
E[95] = (E[72]||((__aa_R[16]&&E[95])));
E[96] = (((E[69]&&!(__aa_R[16])))||E[95]);
E[97] = (__aa_R[17]&&!(__aa_R[0]));
E[98] = (E[97]&&!((__CheckVariables(__aa_CheckArray[20]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 20\n"),
#endif
__aa_A20())));
E[98] = (E[72]||((__aa_R[17]&&E[98])));
E[99] = (((E[69]&&!(__aa_R[17])))||E[98]);
E[97] = (E[97]&&(__CheckVariables(__aa_CheckArray[20]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 20\n"),
#endif
__aa_A20()));
if (E[97]) {
__AppendToList(__aa_EmittedList,74);
}
E[100] = (E[99]||E[97]);
E[101] = (((((((((((((E[20]||E[10])||E[15]))&&E[75])&&E[77])&&E[79])&&E[81])&&E[84])&&E[87])&&E[90])&&E[93])&&E[96])&&E[100]);
E[102] = ((__CheckVariables(__aa_CheckArray[19]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__aa_A19())||E[101]);
if (E[102]) {
__AppendToList(__aa_EmittedList,18);
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[27]);__aa_A27();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A27\n");
#endif
}
if (E[72]) {
__CheckVariables(__aa_CheckArray[28]);__aa_A28();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A28\n");
#endif
}
E[73] = (E[73]&&E[28]);
if (E[73]) {
__CheckVariables(__aa_CheckArray[79]);__aa_A79();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A79\n");
#endif
}
E[97] = ((((((((((E[97]&&E[75])&&E[76])&&E[78])&&E[80])&&E[84])&&E[87])&&E[90])&&E[93])&&E[96])&&E[100]);
if (E[97]) {
__AppendToList(__aa_EmittedList,31);
__AppendToList(__aa_EmittedList,47);
}
if (E[97]) {
__CheckVariables(__aa_CheckArray[80]);__aa_A80();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A80\n");
#endif
}
if (E[101]) {
__CheckVariables(__aa_CheckArray[81]);__aa_A81();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A81\n");
#endif
}
E[70] = (E[70]&&E[28]);
if (E[70]) {
__CheckVariables(__aa_CheckArray[78]);__aa_A78();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A78\n");
#endif
}
if ((((E[73]||E[97])||E[101])||E[70])) {
__AppendToList(__aa_EmittedList,28);
}
if (((((E[73]||E[97])||E[101])||E[70]))) {
__CheckVariables(__aa_CheckArray[29]);__aa_A29();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A29\n");
#endif
}
E[28] = (E[27]&&E[28]);
if (E[28]) {
__AppendToList(__aa_EmittedList,29);
}
if (E[28]) {
__CheckVariables(__aa_CheckArray[75]);__aa_A75();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A75\n");
#endif
}
if (E[28]) {
__CheckVariables(__aa_CheckArray[30]);__aa_A30();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A30\n");
#endif
}
E[27] = (E[75]||E[73]);
E[103] = ((((((((((E[73]&&E[27])&&E[77])&&E[79])&&E[81])&&E[84])&&E[87])&&E[90])&&E[93])&&E[96])&&E[100]);
E[104] = (__aa_R[0]&&!((__CheckVariables(__aa_CheckArray[1]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1())));
if (E[104]) {
__CheckVariables(__aa_CheckArray[94]);__aa_A94();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A94\n");
#endif
}
E[105] = (__aa_R[0]&&!((__CheckVariables(__aa_CheckArray[12]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12())));
if (E[105]) {
__CheckVariables(__aa_CheckArray[95]);__aa_A95();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A95\n");
#endif
}
E[106] = (__aa_R[0]&&!((__CheckVariables(__aa_CheckArray[17]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__aa_A17())));
if (E[106]) {
__CheckVariables(__aa_CheckArray[96]);__aa_A96();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A96\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[97]);__aa_A97();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A97\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[98]);__aa_A98();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A98\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[99]);__aa_A99();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A99\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[100]);__aa_A100();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A100\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[101]);__aa_A101();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A101\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[102]);__aa_A102();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A102\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[103]);__aa_A103();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A103\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[104]);__aa_A104();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A104\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[105]);__aa_A105();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A105\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[106]);__aa_A106();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A106\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[107]);__aa_A107();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A107\n");
#endif
}
if (__aa_R[0]) {
__CheckVariables(__aa_CheckArray[108]);__aa_A108();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A108\n");
#endif
}
E[107] = (((((E[28]||((E[97]||E[101])))||E[103])||E[70]))||__aa_R[0]);
E[108] = (__aa_R[32]&&!(__aa_R[0]));
E[63] = ((((((((((E[63]&&E[41])&&E[42])&&E[44])&&E[46])&&E[50])&&E[53])&&E[56])&&E[59])&&E[62])&&E[66]);
if (E[63]) {
__AppendToList(__aa_EmittedList,39);
__AppendToList(__aa_EmittedList,60);
}
if (E[63]) {
__CheckVariables(__aa_CheckArray[87]);__aa_A87();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A87\n");
#endif
}
E[109] = (E[108]&&E[63]);
if (E[109]) {
__AppendToList(__aa_EmittedList,46);
}
if (E[109]) {
__CheckVariables(__aa_CheckArray[92]);__aa_A92();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A92\n");
#endif
}
if (E[109]) {
__CheckVariables(__aa_CheckArray[93]);__aa_A93();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A93\n");
#endif
}
E[110] = (__aa_R[5]&&!(__aa_R[0]));
E[111] = (((E[107]&&E[109]))||((E[110]&&E[109])));
if (E[111]) {
__AppendToList(__aa_EmittedList,30);
}
if (E[111]) {
__CheckVariables(__aa_CheckArray[31]);__aa_A31();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A31\n");
#endif
}
if (E[97]) {
__CheckVariables(__aa_CheckArray[32]);__aa_A32();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A32\n");
#endif
}
E[112] = (__aa_R[33]&&!(__aa_R[0]));
E[113] = (E[112]&&E[97]);
E[114] = (__aa_R[32]||__aa_R[33]);
E[115] = (E[114]||__aa_R[34]);
E[116] = (__aa_R[2]&&!(__aa_R[0]));
E[117] = (E[116]&&(__CheckVariables(__aa_CheckArray[12]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12()));
E[118] = (__aa_R[3]&&!(__aa_R[0]));
E[119] = (E[118]&&(__CheckVariables(__aa_CheckArray[17]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__aa_A17()));
E[120] = (__aa_R[4]&&!(__aa_R[0]));
E[121] = (E[120]&&(__CheckVariables(__aa_CheckArray[1]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1()));
E[122] = ((__aa_R[2]||__aa_R[3])||__aa_R[4]);
E[123] = (((E[122]&&!(__aa_R[2])))||E[117]);
E[124] = (((E[122]&&!(__aa_R[3])))||E[119]);
E[125] = (((E[122]&&!(__aa_R[4])))||E[121]);
E[121] = ((((((E[117]||E[119])||E[121]))&&E[123])&&E[124])&&E[125]);
if (E[121]) {
__AppendToList(__aa_EmittedList,49);
__AppendToList(__aa_EmittedList,55);
}
E[119] = (__aa_R[31]&&!(__aa_R[0]));
E[117] = (((__aa_R[0]&&E[121]))||((E[119]&&E[121])));
if (E[117]) {
__AppendToList(__aa_EmittedList,33);
__AppendToList(__aa_EmittedList,51);
}
if (E[117]) {
__CheckVariables(__aa_CheckArray[90]);__aa_A90();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A90\n");
#endif
}
if (E[117]) {
__CheckVariables(__aa_CheckArray[91]);__aa_A91();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A91\n");
#endif
}
E[108] = (E[108]&&!(E[63]));
E[48] = (E[48]&&(__CheckVariables(__aa_CheckArray[22]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22()));
E[51] = (E[51]&&(__CheckVariables(__aa_CheckArray[18]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__aa_A18()));
E[54] = (E[54]&&(__CheckVariables(__aa_CheckArray[14]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14()));
E[57] = (E[57]&&(__CheckVariables(__aa_CheckArray[10]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10()));
E[60] = (E[60]&&(__CheckVariables(__aa_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11()));
E[39] = (E[39]&&E[23]);
if (E[39]) {
__CheckVariables(__aa_CheckArray[86]);__aa_A86();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A86\n");
#endif
}
E[126] = (E[41]||E[39]);
E[127] = (((((((((((((((E[48]||E[51])||E[54])||E[57])||E[60]))&&E[126])&&E[43])&&E[45])&&E[47])&&((E[50]||E[48])))&&((E[53]||E[51])))&&((E[56]||E[54])))&&((E[59]||E[57])))&&((E[62]||E[60])))&&E[66]);
if (E[127]) {
__AppendToList(__aa_EmittedList,57);
}
E[128] = (E[117]||((__aa_R[32]&&((E[108]&&!(E[127]))))));
E[112] = (E[112]&&!(E[97]));
E[82] = (E[82]&&(__CheckVariables(__aa_CheckArray[22]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__aa_A22()));
E[85] = (E[85]&&(__CheckVariables(__aa_CheckArray[18]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__aa_A18()));
E[88] = (E[88]&&(__CheckVariables(__aa_CheckArray[14]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14()));
E[91] = (E[91]&&(__CheckVariables(__aa_CheckArray[10]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10()));
E[94] = (E[94]&&(__CheckVariables(__aa_CheckArray[11]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11()));
E[100] = (((((((((((((((E[82]||E[85])||E[88])||E[91])||E[94]))&&E[27])&&E[77])&&E[79])&&E[81])&&((E[84]||E[82])))&&((E[87]||E[85])))&&((E[90]||E[88])))&&((E[93]||E[91])))&&((E[96]||E[94])))&&E[100]);
if (E[100]) {
__AppendToList(__aa_EmittedList,61);
}
E[81] = (E[109]||((__aa_R[33]&&((E[112]&&!(E[100]))))));
E[114] = ((((E[115]&&!(E[114])))||E[128])||E[81]);
E[79] = (E[114]||E[113]);
E[11] = (E[117]||((__aa_R[34]&&((E[8]&&!(E[11]))))));
E[8] = (((E[115]&&!(__aa_R[34])))||E[11]);
E[77] = (E[8]||E[12]);
E[27] = ((((E[113]||E[12]))&&E[79])&&E[77]);
E[129] = (E[115]||__aa_R[35]);
E[8] = (((((E[128]||E[81])||E[11]))&&E[114])&&E[8]);
E[115] = (((E[129]&&!(E[115])))||E[8]);
E[114] = (E[115]||E[27]);
E[16] = (E[117]||((__aa_R[35]&&((E[13]&&!(E[16]))))));
E[13] = (((E[129]&&!(__aa_R[35])))||E[16]);
E[130] = (E[13]||E[17]);
E[131] = ((((E[27]||E[17]))&&E[114])&&E[130]);
E[132] = (E[129]||__aa_R[36]);
E[13] = ((((E[8]||E[16]))&&E[115])&&E[13]);
E[129] = (((E[132]&&!(E[129])))||E[13]);
E[115] = (E[129]||E[131]);
E[21] = (E[117]||((__aa_R[36]&&((E[18]&&!(E[21]))))));
E[18] = (((E[132]&&!(__aa_R[36])))||E[21]);
E[8] = (E[18]||E[22]);
E[133] = ((((E[131]||E[22]))&&E[115])&&E[8]);
if (E[133]) {
__AppendToList(__aa_EmittedList,85);
}
E[134] = (E[132]||__aa_R[37]);
E[18] = ((((E[13]||E[21]))&&E[129])&&E[18]);
E[132] = (((E[134]&&!(E[132])))||E[18]);
E[129] = (E[132]||E[133]);
E[13] = (E[117]||((__aa_R[37]&&((__aa_R[37]&&!(__aa_R[0]))))));
E[135] = (((E[134]&&!(__aa_R[37])))||E[13]);
E[136] = ((E[133]&&E[129])&&E[135]);
if (E[136]) {
__AppendToList(__aa_EmittedList,32);
__AppendToList(__aa_EmittedList,63);
}
if (E[136]) {
__CheckVariables(__aa_CheckArray[33]);__aa_A33();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A33\n");
#endif
}
if (E[117]) {
__CheckVariables(__aa_CheckArray[34]);__aa_A34();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A34\n");
#endif
}
if (!(_true)) {
__CheckVariables(__aa_CheckArray[35]);__aa_A35();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A35\n");
#endif
}
if (E[38]) {
__CheckVariables(__aa_CheckArray[36]);__aa_A36();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A36\n");
#endif
}
if (E[67]) {
__CheckVariables(__aa_CheckArray[88]);__aa_A88();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A88\n");
#endif
}
E[36] = (E[36]&&E[23]);
if (E[36]) {
__CheckVariables(__aa_CheckArray[85]);__aa_A85();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A85\n");
#endif
}
if ((((E[39]||E[63])||E[67])||E[36])) {
__AppendToList(__aa_EmittedList,36);
}
if (((((E[39]||E[63])||E[67])||E[36]))) {
__CheckVariables(__aa_CheckArray[37]);__aa_A37();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A37\n");
#endif
}
E[23] = (E[7]&&E[23]);
if (E[23]) {
__AppendToList(__aa_EmittedList,37);
}
if (E[23]) {
__CheckVariables(__aa_CheckArray[82]);__aa_A82();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A82\n");
#endif
}
if (E[23]) {
__CheckVariables(__aa_CheckArray[38]);__aa_A38();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A38\n");
#endif
}
E[66] = ((((((((((E[39]&&E[126])&&E[43])&&E[45])&&E[47])&&E[50])&&E[53])&&E[56])&&E[59])&&E[62])&&E[66]);
E[47] = (((((E[23]||((E[63]||E[67])))||E[66])||E[36]))||__aa_R[0]);
E[45] = (__aa_R[18]&&!(__aa_R[0]));
E[43] = (((E[47]&&E[117]))||((E[45]&&E[117])));
if (E[43]) {
__AppendToList(__aa_EmittedList,38);
}
if (E[43]) {
__CheckVariables(__aa_CheckArray[39]);__aa_A39();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A39\n");
#endif
}
if (E[63]) {
__CheckVariables(__aa_CheckArray[40]);__aa_A40();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A40\n");
#endif
}
E[126] = (__aa_R[40]&&!(__aa_R[0]));
E[7] = (E[126]&&E[31]);
if (E[7]) {
__CheckVariables(__aa_CheckArray[89]);__aa_A89();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A89\n");
#endif
}
E[137] = (__aa_R[1]&&!(__aa_R[0]));
E[138] = (E[137]&&E[136]);
if (E[138]) {
__AppendToList(__aa_EmittedList,70);
}
E[69] = ((((__aa_R[6]||E[69])||__aa_R[7]))||__aa_R[5]);
E[6] = ((((__aa_R[19]||E[6])||__aa_R[20]))||__aa_R[18]);
E[134] = (E[134]||__aa_R[31]);
E[139] = (((__aa_R[38]||__aa_R[39])||__aa_R[40])||__aa_R[41]);
E[140] = (((((E[122]||E[69])||E[6])||E[134])||E[139])||__aa_R[1]);
E[116] = (E[116]&&!((__CheckVariables(__aa_CheckArray[12]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12())));
E[116] = (__aa_R[0]||((__aa_R[2]&&E[116])));
E[118] = (E[118]&&!((__CheckVariables(__aa_CheckArray[17]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__aa_A17())));
E[118] = (__aa_R[0]||((__aa_R[3]&&E[118])));
E[120] = (E[120]&&!((__CheckVariables(__aa_CheckArray[1]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1())));
E[120] = (__aa_R[0]||((__aa_R[4]&&E[120])));
E[125] = ((((((E[116]||E[118])||E[120]))&&((E[123]||E[116])))&&((E[124]||E[118])))&&((E[125]||E[120])));
E[122] = (((((E[140]&&!(E[122])))||E[121]))||E[125]);
E[29] = (E[29]&&!((__CheckVariables(__aa_CheckArray[2]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2())));
E[29] = (__aa_R[6]&&E[29]);
E[71] = (((E[30]&&!(E[35])))||((__aa_R[7]&&((E[71]&&!(E[35]))))));
E[110] = (((E[107]&&!(E[109])))||((__aa_R[5]&&((E[110]&&!(E[109]))))));
E[99] = ((E[111]||(((E[29]||(((((((((((((((((((((E[74]||E[19])||E[9])||E[14])||E[83])||E[86])||E[89])||E[92])||E[95])||E[98]))&&E[75])&&E[76])&&E[78])&&E[80])&&E[84])&&E[87])&&E[90])&&E[93])&&E[96])&&E[99])))||E[71])))||E[110]);
E[69] = (((((E[140]&&!(E[69])))||E[100]))||E[99]);
E[24] = (E[24]&&!((__CheckVariables(__aa_CheckArray[2]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__aa_A2())));
E[24] = (__aa_R[19]&&E[24]);
E[35] = (((E[25]&&!(E[35])))||((__aa_R[20]&&((E[37]&&!(E[35]))))));
E[45] = (((E[47]&&!(E[117])))||((__aa_R[18]&&((E[45]&&!(E[117]))))));
E[65] = ((E[43]||(((E[24]||(((((((((((((((((((((E[40]||E[0])||E[2])||E[4])||E[49])||E[52])||E[55])||E[58])||E[61])||E[64]))&&E[41])&&E[42])&&E[44])&&E[46])&&E[50])&&E[53])&&E[56])&&E[59])&&E[62])&&E[65])))||E[35])))||E[45]);
E[6] = (((((E[140]&&!(E[6])))||E[127]))||E[65]);
E[112] = (E[112]&&E[100]);
E[108] = (E[108]&&E[127]);
E[77] = ((((E[112]||E[108]))&&(((E[79]||E[112])||E[108])))&&E[77]);
E[130] = ((E[77]&&((E[114]||E[77])))&&E[130]);
E[8] = ((E[130]&&((E[115]||E[130])))&&E[8]);
E[129] = ((E[8]&&((E[129]||E[8])))&&E[135]);
if (E[129]) {
__AppendToList(__aa_EmittedList,64);
}
E[121] = (((__aa_R[0]&&!(E[121])))||((__aa_R[31]&&((E[119]&&!(E[121]))))));
E[135] = ((((((E[18]||E[13]))&&E[132])&&E[135]))||E[121]);
E[134] = (((((E[140]&&!(E[134])))||((E[136]||E[129]))))||E[135]);
E[26] = (__aa_R[0]||((__aa_R[38]&&((E[26]&&!(E[31]))))));
E[132] = (__aa_R[39]&&!(__aa_R[0]));
E[18] = (((((((E[73]||E[97])||E[101])||E[70])||E[39])||E[63])||E[67])||E[36]);
if (E[18]) {
__AppendToList(__aa_EmittedList,53);
}
E[34] = ((E[32]||E[34])||((__aa_R[39]&&((E[132]&&!(E[18]))))));
E[31] = (((E[132]&&E[18]))||((__aa_R[40]&&((E[126]&&!(E[31]))))));
E[33] = (E[33]&&!((__CheckVariables(__aa_CheckArray[3]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__aa_A3())));
E[33] = (E[7]||((__aa_R[41]&&E[33])));
E[139] = ((((((E[140]&&!(E[139])))||E[26])||E[34])||E[31])||E[33]);
E[137] = (E[137]&&!(E[136]));
E[126] = (E[137]&&!(E[129]));
E[18] = (E[126]&&!((__CheckVariables(__aa_CheckArray[23]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 23\n"),
#endif
__aa_A23())));
E[132] = (E[18]&&!((__CheckVariables(__aa_CheckArray[24]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 24\n"),
#endif
__aa_A24())));
E[32] = (E[132]&&!((__CheckVariables(__aa_CheckArray[25]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 25\n"),
#endif
__aa_A25())));
E[119] = (E[32]&&!((__CheckVariables(__aa_CheckArray[26]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 26\n"),
#endif
__aa_A26())));
E[119] = (__aa_R[0]||((__aa_R[1]&&E[119])));
E[140] = (((E[140]&&!(__aa_R[1])))||E[119]);
E[115] = (E[140]||E[138]);
E[138] = ((((((E[138]&&E[122])&&E[69])&&E[6])&&E[134])&&E[139])&&E[115]);
if (E[138]) {
__AppendToList(__aa_EmittedList,41);
__AppendToList(__aa_EmittedList,43);
}
if (E[138]) {
__CheckVariables(__aa_CheckArray[72]);__aa_A72();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A72\n");
#endif
}
if ((E[7]||E[138])) {
__AppendToList(__aa_EmittedList,40);
}
if (((E[7]||E[138]))) {
__CheckVariables(__aa_CheckArray[41]);__aa_A41();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A41\n");
#endif
}
if (E[138]) {
__CheckVariables(__aa_CheckArray[73]);__aa_A73();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A73\n");
#endif
}
if (E[138]) {
__CheckVariables(__aa_CheckArray[42]);__aa_A42();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A42\n");
#endif
}
E[137] = (E[137]&&E[129]);
E[126] = (E[126]&&(__CheckVariables(__aa_CheckArray[23]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 23\n"),
#endif
__aa_A23()));
E[18] = (E[18]&&(__CheckVariables(__aa_CheckArray[24]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 24\n"),
#endif
__aa_A24()));
E[132] = (E[132]&&(__CheckVariables(__aa_CheckArray[25]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 25\n"),
#endif
__aa_A25()));
E[32] = (E[32]&&(__CheckVariables(__aa_CheckArray[26]),
#ifdef TRACE_ACTION
fprintf(stderr, "test 26\n"),
#endif
__aa_A26()));
E[115] = (((((((((((E[137]||E[126])||E[18])||E[132])||E[32]))&&E[122])&&E[69])&&E[6])&&E[134])&&E[139])&&((((((E[115]||E[137])||E[126])||E[18])||E[132])||E[32])));
if (E[115]) {
__AppendToList(__aa_EmittedList,42);
__AppendToList(__aa_EmittedList,44);
}
if (E[115]) {
__CheckVariables(__aa_CheckArray[74]);__aa_A74();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A74\n");
#endif
}
if (E[115]) {
__CheckVariables(__aa_CheckArray[43]);__aa_A43();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A43\n");
#endif
}
if (E[138]) {
__CheckVariables(__aa_CheckArray[44]);__aa_A44();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A44\n");
#endif
}
if (E[115]) {
__CheckVariables(__aa_CheckArray[45]);__aa_A45();
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A45\n");
#endif
}
E[114] = (E[72]||E[38]);
if (E[114]) {
__AppendToList(__aa_EmittedList,52);
}
E[32] = ((((E[137]||E[126])||E[18])||E[132])||E[32]);
if (E[32]) {
__AppendToList(__aa_EmittedList,69);
}
E[94] = ((((E[82]||E[85])||E[88])||E[91])||E[94]);
if (E[94]) {
__AppendToList(__aa_EmittedList,71);
}
E[91] = ((E[28]||E[73])||E[70]);
if (E[91]) {
__AppendToList(__aa_EmittedList,72);
}
E[15] = ((E[20]||E[10])||E[15]);
if (E[15]) {
__AppendToList(__aa_EmittedList,73);
}
E[60] = ((((E[48]||E[51])||E[54])||E[57])||E[60]);
if (E[60]) {
__AppendToList(__aa_EmittedList,75);
}
E[57] = ((E[23]||E[39])||E[36]);
if (E[57]) {
__AppendToList(__aa_EmittedList,76);
}
E[5] = ((E[1]||E[3])||E[5]);
if (E[5]) {
__AppendToList(__aa_EmittedList,77);
}
E[108] = (E[112]||E[108]);
if (E[108]) {
__AppendToList(__aa_EmittedList,84);
}
E[22] = (E[131]||E[22]);
if (E[22]) {
__AppendToList(__aa_EmittedList,86);
}
E[17] = (E[27]||E[17]);
if (E[17]) {
__AppendToList(__aa_EmittedList,87);
}
E[12] = (E[113]||E[12]);
if (E[12]) {
__AppendToList(__aa_EmittedList,88);
}
E[113] = ((E[138]||E[115]));
E[140] = ((((((((((((((((E[125]||E[99])||E[65])||E[135])||E[26])||E[34])||E[31])||E[33])||E[119]))&&E[122])&&E[69])&&E[6])&&E[134])&&E[139])&&E[140]));
E[139] = (E[115]||E[138]);
__aa_R[1] = (E[119]&&!(E[139]));
__aa_R[2] = (E[116]&&!(E[139]));
__aa_R[3] = (E[118]&&!(E[139]));
__aa_R[4] = (E[120]&&!(E[139]));
__aa_R[5] = (E[110]&&!(E[139]));
E[110] = (E[100]||E[139]);
__aa_R[6] = (((E[111]&&!(E[139])))||((E[29]&&!(E[110]))));
__aa_R[7] = (E[71]&&!(E[110]));
E[100] = (((((((((((E[110]||E[103])||E[100]))||E[101])||E[103])||E[100]))||E[97])||E[101])||E[103])||E[100]);
__aa_R[8] = (E[74]&&!(E[100]));
__aa_R[9] = (E[19]&&!(E[100]));
__aa_R[10] = (E[9]&&!(E[100]));
__aa_R[11] = (E[14]&&!(E[100]));
__aa_R[12] = (E[83]&&!(E[100]));
__aa_R[13] = (E[86]&&!(E[100]));
__aa_R[14] = (E[89]&&!(E[100]));
__aa_R[15] = (E[92]&&!(E[100]));
__aa_R[16] = (E[95]&&!(E[100]));
__aa_R[17] = (E[98]&&!(E[100]));
__aa_R[18] = (E[45]&&!(E[139]));
E[45] = (E[127]||E[139]);
__aa_R[19] = (((E[43]&&!(E[139])))||((E[24]&&!(E[45]))));
__aa_R[20] = (E[35]&&!(E[45]));
E[127] = (((((((((((E[45]||E[66])||E[127]))||E[67])||E[66])||E[127]))||E[63])||E[67])||E[66])||E[127]);
__aa_R[21] = (E[40]&&!(E[127]));
__aa_R[22] = (E[0]&&!(E[127]));
__aa_R[23] = (E[2]&&!(E[127]));
__aa_R[24] = (E[4]&&!(E[127]));
__aa_R[25] = (E[49]&&!(E[127]));
__aa_R[26] = (E[52]&&!(E[127]));
__aa_R[27] = (E[55]&&!(E[127]));
__aa_R[28] = (E[58]&&!(E[127]));
__aa_R[29] = (E[61]&&!(E[127]));
__aa_R[30] = (E[64]&&!(E[127]));
__aa_R[31] = (E[121]&&!(E[139]));
E[129] = ((((E[139]||E[129]))||E[136])||E[129]);
E[8] = ((((E[129]||E[8]))||E[133])||E[8]);
E[130] = ((((E[8]||E[130]))||E[131])||E[130]);
E[77] = ((((E[130]||E[77]))||E[27])||E[77]);
__aa_R[32] = (E[128]&&!(E[77]));
__aa_R[33] = (E[81]&&!(E[77]));
__aa_R[34] = (E[11]&&!(E[77]));
__aa_R[35] = (E[16]&&!(E[130]));
__aa_R[36] = (E[21]&&!(E[8]));
__aa_R[37] = (E[13]&&!(E[129]));
__aa_R[38] = (E[26]&&!(E[139]));
__aa_R[39] = (E[34]&&!(E[139]));
__aa_R[40] = (E[31]&&!(E[139]));
__aa_R[41] = (E[33]&&!(E[139]));
__aa_R[0] = !(_true);
if (__aa_R[1]) { __AppendToList(__aa_HaltList,1); }
if (__aa_R[2]) { __AppendToList(__aa_HaltList,2); }
if (__aa_R[3]) { __AppendToList(__aa_HaltList,3); }
if (__aa_R[4]) { __AppendToList(__aa_HaltList,4); }
if (__aa_R[5]) { __AppendToList(__aa_HaltList,5); }
if (__aa_R[6]) { __AppendToList(__aa_HaltList,6); }
if (__aa_R[7]) { __AppendToList(__aa_HaltList,7); }
if (__aa_R[8]) { __AppendToList(__aa_HaltList,8); }
if (__aa_R[9]) { __AppendToList(__aa_HaltList,9); }
if (__aa_R[10]) { __AppendToList(__aa_HaltList,10); }
if (__aa_R[11]) { __AppendToList(__aa_HaltList,11); }
if (__aa_R[12]) { __AppendToList(__aa_HaltList,12); }
if (__aa_R[13]) { __AppendToList(__aa_HaltList,13); }
if (__aa_R[14]) { __AppendToList(__aa_HaltList,14); }
if (__aa_R[15]) { __AppendToList(__aa_HaltList,15); }
if (__aa_R[16]) { __AppendToList(__aa_HaltList,16); }
if (__aa_R[17]) { __AppendToList(__aa_HaltList,17); }
if (__aa_R[18]) { __AppendToList(__aa_HaltList,18); }
if (__aa_R[19]) { __AppendToList(__aa_HaltList,19); }
if (__aa_R[20]) { __AppendToList(__aa_HaltList,20); }
if (__aa_R[21]) { __AppendToList(__aa_HaltList,21); }
if (__aa_R[22]) { __AppendToList(__aa_HaltList,22); }
if (__aa_R[23]) { __AppendToList(__aa_HaltList,23); }
if (__aa_R[24]) { __AppendToList(__aa_HaltList,24); }
if (__aa_R[25]) { __AppendToList(__aa_HaltList,25); }
if (__aa_R[26]) { __AppendToList(__aa_HaltList,26); }
if (__aa_R[27]) { __AppendToList(__aa_HaltList,27); }
if (__aa_R[28]) { __AppendToList(__aa_HaltList,28); }
if (__aa_R[29]) { __AppendToList(__aa_HaltList,29); }
if (__aa_R[30]) { __AppendToList(__aa_HaltList,30); }
if (__aa_R[31]) { __AppendToList(__aa_HaltList,31); }
if (__aa_R[32]) { __AppendToList(__aa_HaltList,32); }
if (__aa_R[33]) { __AppendToList(__aa_HaltList,33); }
if (__aa_R[34]) { __AppendToList(__aa_HaltList,34); }
if (__aa_R[35]) { __AppendToList(__aa_HaltList,35); }
if (__aa_R[36]) { __AppendToList(__aa_HaltList,36); }
if (__aa_R[37]) { __AppendToList(__aa_HaltList,37); }
if (__aa_R[38]) { __AppendToList(__aa_HaltList,38); }
if (__aa_R[39]) { __AppendToList(__aa_HaltList,39); }
if (__aa_R[40]) { __AppendToList(__aa_HaltList,40); }
if (__aa_R[41]) { __AppendToList(__aa_HaltList,41); }
if (!E[140]) { __AppendToList(__aa_HaltList,0); }
__ResetModuleEntryAfterReaction();
__aa_ModuleData.awaited_list = __aa_AllAwaitedList;
__aa__reset_input();
return E[140];
}

/* AUTOMATON RESET */

int aa_reset () {
__aa_ModuleData.awaited_list = __aa_AwaitedList;
__ResetModuleEntry();
__aa_ModuleData.awaited_list = __aa_AllAwaitedList;
__aa_ModuleData.state = 0;
__aa_R[0] = _true;
__aa_R[1] = _false;
__aa_R[2] = _false;
__aa_R[3] = _false;
__aa_R[4] = _false;
__aa_R[5] = _false;
__aa_R[6] = _false;
__aa_R[7] = _false;
__aa_R[8] = _false;
__aa_R[9] = _false;
__aa_R[10] = _false;
__aa_R[11] = _false;
__aa_R[12] = _false;
__aa_R[13] = _false;
__aa_R[14] = _false;
__aa_R[15] = _false;
__aa_R[16] = _false;
__aa_R[17] = _false;
__aa_R[18] = _false;
__aa_R[19] = _false;
__aa_R[20] = _false;
__aa_R[21] = _false;
__aa_R[22] = _false;
__aa_R[23] = _false;
__aa_R[24] = _false;
__aa_R[25] = _false;
__aa_R[26] = _false;
__aa_R[27] = _false;
__aa_R[28] = _false;
__aa_R[29] = _false;
__aa_R[30] = _false;
__aa_R[31] = _false;
__aa_R[32] = _false;
__aa_R[33] = _false;
__aa_R[34] = _false;
__aa_R[35] = _false;
__aa_R[36] = _false;
__aa_R[37] = _false;
__aa_R[38] = _false;
__aa_R[39] = _false;
__aa_R[40] = _false;
__aa_R[41] = _false;
__aa__reset_input();
return 0;
}
char* CompilationType = "Compiled Sorted Equations";

int __NumberOfModules = 1;
struct __ModuleEntry* __ModuleTable[] = {
&__aa_ModuleData
};
