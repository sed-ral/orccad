/* sscc : C CODE OF SORTED EQUATIONS Urgence - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __Urgence_GENERIC_TEST(TEST) return TEST;
typedef void (*__Urgence_APF)();
static __Urgence_APF *__Urgence_PActionArray;

#include "Urgence.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _CycabFollowingRatio_controler_DEFINED
#ifndef CycabFollowingRatio_controler
extern void CycabFollowingRatio_controler(orcStrlPtr);
#endif
#endif
#ifndef _CycabFollowingRatio_fileparameter_DEFINED
#ifndef CycabFollowingRatio_fileparameter
extern void CycabFollowingRatio_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _CycabFollowingRatio_parameter_DEFINED
#ifndef CycabFollowingRatio_parameter
extern void CycabFollowingRatio_parameter(orcStrlPtr);
#endif
#endif
#ifndef _CycabStartPost_controler_DEFINED
#ifndef CycabStartPost_controler
extern void CycabStartPost_controler(orcStrlPtr);
#endif
#endif
#ifndef _CycabStartPost_fileparameter_DEFINED
#ifndef CycabStartPost_fileparameter
extern void CycabStartPost_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _CycabStartPost_parameter_DEFINED
#ifndef CycabStartPost_parameter
extern void CycabStartPost_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __Urgence_V0;
static boolean __Urgence_V1;
static orcStrlPtr __Urgence_V2;
static boolean __Urgence_V3;
static boolean __Urgence_V4;
static boolean __Urgence_V5;
static boolean __Urgence_V6;
static orcStrlPtr __Urgence_V7;
static boolean __Urgence_V8;
static boolean __Urgence_V9;
static boolean __Urgence_V10;
static boolean __Urgence_V11;
static boolean __Urgence_V12;
static boolean __Urgence_V13;
static boolean __Urgence_V14;


/* INPUT FUNCTIONS */

void Urgence_I_START_Urgence () {
__Urgence_V0 = _true;
}
void Urgence_I_Abort_Local_Urgence () {
__Urgence_V1 = _true;
}
void Urgence_I_CycabFollowingRatio_Start (orcStrlPtr __V) {
_orcStrlPtr(&__Urgence_V2,__V);
__Urgence_V3 = _true;
}
void Urgence_I_USERSTART_CycabFollowingRatio () {
__Urgence_V4 = _true;
}
void Urgence_I_BF_CycabFollowingRatio () {
__Urgence_V5 = _true;
}
void Urgence_I_T3_CycabFollowingRatio () {
__Urgence_V6 = _true;
}
void Urgence_I_CycabStartPost_Start (orcStrlPtr __V) {
_orcStrlPtr(&__Urgence_V7,__V);
__Urgence_V8 = _true;
}
void Urgence_I_USERSTART_CycabStartPost () {
__Urgence_V9 = _true;
}
void Urgence_I_BF_CycabStartPost () {
__Urgence_V10 = _true;
}
void Urgence_I_T3_CycabStartPost () {
__Urgence_V11 = _true;
}
void Urgence_I_T2_EmergencyStopE () {
__Urgence_V12 = _true;
}
void Urgence_I_T2_LooseTargetE () {
__Urgence_V13 = _true;
}
void Urgence_I_T2_EmStopE () {
__Urgence_V14 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __Urgence_A1 \
__Urgence_V0
#define __Urgence_A2 \
__Urgence_V1
#define __Urgence_A3 \
__Urgence_V3
#define __Urgence_A4 \
__Urgence_V4
#define __Urgence_A5 \
__Urgence_V5
#define __Urgence_A6 \
__Urgence_V6
#define __Urgence_A7 \
__Urgence_V8
#define __Urgence_A8 \
__Urgence_V9
#define __Urgence_A9 \
__Urgence_V10
#define __Urgence_A10 \
__Urgence_V11
#define __Urgence_A11 \
__Urgence_V12
#define __Urgence_A12 \
__Urgence_V13
#define __Urgence_A13 \
__Urgence_V14

/* OUTPUT ACTIONS */

#define __Urgence_A14 \
Urgence_O_BF_Urgence()
#define __Urgence_A15 \
Urgence_O_STARTED_Urgence()
#define __Urgence_A16 \
Urgence_O_GoodEnd_Urgence()
#define __Urgence_A17 \
Urgence_O_Abort_Urgence()
#define __Urgence_A18 \
Urgence_O_T3_Urgence()
#define __Urgence_A19 \
Urgence_O_START_CycabFollowingRatio()
#define __Urgence_A20 \
Urgence_O_Abort_Local_CycabFollowingRatio()
#define __Urgence_A21 \
Urgence_O_START_CycabStartPost()
#define __Urgence_A22 \
Urgence_O_Abort_Local_CycabStartPost()

/* ASSIGNMENTS */

#define __Urgence_A23 \
__Urgence_V0 = _false
#define __Urgence_A24 \
__Urgence_V1 = _false
#define __Urgence_A25 \
__Urgence_V3 = _false
#define __Urgence_A26 \
__Urgence_V4 = _false
#define __Urgence_A27 \
__Urgence_V5 = _false
#define __Urgence_A28 \
__Urgence_V6 = _false
#define __Urgence_A29 \
__Urgence_V8 = _false
#define __Urgence_A30 \
__Urgence_V9 = _false
#define __Urgence_A31 \
__Urgence_V10 = _false
#define __Urgence_A32 \
__Urgence_V11 = _false
#define __Urgence_A33 \
__Urgence_V12 = _false
#define __Urgence_A34 \
__Urgence_V13 = _false
#define __Urgence_A35 \
__Urgence_V14 = _false

/* PROCEDURE CALLS */

#define __Urgence_A36 \
CycabStartPost_parameter(__Urgence_V7)
#define __Urgence_A37 \
CycabStartPost_controler(__Urgence_V7)
#define __Urgence_A38 \
CycabFollowingRatio_parameter(__Urgence_V2)
#define __Urgence_A39 \
CycabFollowingRatio_controler(__Urgence_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __Urgence_A40 \

#define __Urgence_A41 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int Urgence_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __Urgence__reset_input () {
__Urgence_V0 = _false;
__Urgence_V1 = _false;
__Urgence_V3 = _false;
__Urgence_V4 = _false;
__Urgence_V5 = _false;
__Urgence_V6 = _false;
__Urgence_V8 = _false;
__Urgence_V9 = _false;
__Urgence_V10 = _false;
__Urgence_V11 = _false;
__Urgence_V12 = _false;
__Urgence_V13 = _false;
__Urgence_V14 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __Urgence_R[8] = {_true,_false,_false,_false,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int Urgence () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[34];
E[0] = (__Urgence_R[3]&&!(__Urgence_R[0]));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Urgence_A5));
E[2] = (__Urgence_R[4]&&!(__Urgence_R[0]));
E[3] = (E[2]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__Urgence_A13));
E[4] = (__Urgence_R[2]||__Urgence_R[3]);
E[5] = (E[4]||__Urgence_R[4]);
E[6] = (__Urgence_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__Urgence_A3)));
if (E[6]) {
__Urgence_A40;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A40\n");
#endif
}
E[7] = (__Urgence_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__Urgence_A7)));
if (E[7]) {
__Urgence_A41;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A41\n");
#endif
}
E[8] = (__Urgence_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Urgence_A1));
E[9] = (__Urgence_R[1]&&!(__Urgence_R[0]));
E[10] = (E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Urgence_A1));
E[10] = (E[8]||E[10]);
if (E[10]) {
__Urgence_A36;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A36\n");
#endif
}
if (E[10]) {
__Urgence_A37;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A37\n");
#endif
}
E[8] = (__Urgence_R[2]&&!(__Urgence_R[0]));
E[11] = (E[8]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__Urgence_A9)));
E[12] = (E[11]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__Urgence_A10)));
E[12] = (E[10]||((__Urgence_R[2]&&E[12])));
E[8] = (E[8]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__Urgence_A9));
if (E[8]) {
__Urgence_A38;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A38\n");
#endif
}
if (E[8]) {
__Urgence_A39;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A39\n");
#endif
}
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Urgence_A5)));
E[13] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Urgence_A6)));
E[13] = (E[8]||((__Urgence_R[3]&&E[13])));
E[4] = ((((E[5]&&!(E[4])))||E[12])||E[13]);
E[14] = (E[4]||E[1]);
E[2] = (E[2]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__Urgence_A13)));
E[2] = (E[10]||((__Urgence_R[4]&&E[2])));
E[15] = (((E[5]&&!(__Urgence_R[4])))||E[2]);
E[16] = (E[15]||E[3]);
E[17] = ((((E[1]||E[3]))&&E[14])&&E[16]);
E[18] = (__Urgence_R[5]&&!(__Urgence_R[0]));
E[19] = (E[18]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__Urgence_A12));
E[20] = (E[5]||__Urgence_R[5]);
E[15] = (((((E[12]||E[13])||E[2]))&&E[4])&&E[15]);
E[5] = (((E[20]&&!(E[5])))||E[15]);
E[4] = (E[5]||E[17]);
E[18] = (E[18]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__Urgence_A12)));
E[18] = (E[10]||((__Urgence_R[5]&&E[18])));
E[21] = (((E[20]&&!(__Urgence_R[5])))||E[18]);
E[22] = (E[21]||E[19]);
E[23] = ((((E[17]||E[19]))&&E[4])&&E[22]);
E[24] = (__Urgence_R[6]&&!(__Urgence_R[0]));
E[25] = (E[24]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__Urgence_A11));
E[26] = (E[20]||__Urgence_R[6]);
E[21] = ((((E[15]||E[18]))&&E[5])&&E[21]);
E[20] = (((E[26]&&!(E[20])))||E[21]);
E[5] = (E[20]||E[23]);
E[24] = (E[24]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__Urgence_A11)));
E[24] = (E[10]||((__Urgence_R[6]&&E[24])));
E[15] = (((E[26]&&!(__Urgence_R[6])))||E[24]);
E[27] = (E[15]||E[25]);
E[28] = ((((E[23]||E[25]))&&E[5])&&E[27]);
E[29] = (E[26]||__Urgence_R[7]);
E[15] = ((((E[21]||E[24]))&&E[20])&&E[15]);
E[26] = (((E[29]&&!(E[26])))||E[15]);
E[20] = (E[26]||E[28]);
E[21] = (__Urgence_R[7]&&!(__Urgence_R[0]));
E[30] = (E[21]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Urgence_A2)));
E[30] = (E[10]||((__Urgence_R[7]&&E[30])));
E[31] = (((E[29]&&!(__Urgence_R[7])))||E[30]);
E[32] = ((E[28]&&E[20])&&E[31]);
if (E[32]) {
__Urgence_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A14\n");
#endif
}
if (E[10]) {
__Urgence_A15;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A15\n");
#endif
}
if (E[32]) {
__Urgence_A16;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A16\n");
#endif
}
E[21] = (E[21]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Urgence_A2));
if (E[21]) {
__Urgence_A17;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A17\n");
#endif
}
E[0] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Urgence_A6));
E[11] = (E[11]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__Urgence_A10));
E[16] = ((((E[0]||E[11]))&&(((E[14]||E[0])||E[11])))&&E[16]);
E[22] = ((E[16]&&((E[4]||E[16])))&&E[22]);
E[27] = ((E[22]&&((E[5]||E[22])))&&E[27]);
E[20] = (E[20]||E[27]);
E[5] = ((E[27]&&E[20])&&E[31]);
if (E[5]) {
__Urgence_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A18\n");
#endif
}
if (E[8]) {
__Urgence_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A19\n");
#endif
}
if (((((E[3]||E[19])||E[25])||E[21]))) {
__Urgence_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A20\n");
#endif
}
if (E[10]) {
__Urgence_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A21\n");
#endif
}
if (((((E[3]||E[19])||E[25])||E[21]))) {
__Urgence_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__Urgence_A22\n");
#endif
}
E[11] = (E[0]||E[11]);
E[0] = (E[23]||E[25]);
E[4] = (E[17]||E[19]);
E[1] = (E[1]||E[3]);
E[20] = ((E[21]&&E[20])&&((E[31]||E[21])));
E[14] = ((((E[32]||E[5]))||E[20]));
E[33] = (__Urgence_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Urgence_A1)));
E[9] = (E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Urgence_A1)));
E[9] = (E[33]||((__Urgence_R[1]&&E[9])));
E[31] = (((((((E[15]||E[30]))&&E[26])&&E[31]))||E[9]));
E[29] = (E[29]||__Urgence_R[1]);
E[20] = (((((E[20]||E[5]))||E[32])||E[5])||E[20]);
E[27] = ((((E[20]||E[27]))||E[28])||E[27]);
E[22] = ((((E[27]||E[22]))||E[23])||E[22]);
E[16] = ((((E[22]||E[16]))||E[17])||E[16]);
__Urgence_R[2] = (E[12]&&!(E[16]));
__Urgence_R[3] = (E[13]&&!(E[16]));
__Urgence_R[4] = (E[2]&&!(E[16]));
__Urgence_R[5] = (E[18]&&!(E[22]));
__Urgence_R[6] = (E[24]&&!(E[27]));
__Urgence_R[7] = (E[30]&&!(E[20]));
__Urgence_R[0] = !(_true);
__Urgence_R[1] = E[9];
__Urgence__reset_input();
return E[31];
}

/* AUTOMATON RESET */

int Urgence_reset () {
__Urgence_R[0] = _true;
__Urgence_R[1] = _false;
__Urgence_R[2] = _false;
__Urgence_R[3] = _false;
__Urgence_R[4] = _false;
__Urgence_R[5] = _false;
__Urgence_R[6] = _false;
__Urgence_R[7] = _false;
__Urgence__reset_input();
return 0;
}
