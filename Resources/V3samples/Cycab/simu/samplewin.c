/********************************  Orccad ************************************
*
* File           : Samples/Misc/Drv/samplewin.c
* Author         : Roger Pissard-Gibollet and Nicolas Turro
* Version        : 3.0 alpha
* Creation       : 12 March  1998
*
****************************** Description **********************************
*
*   Example of use of the API to simulate a robot arm with 2 d.o.f
*   without using Orccad.
*
***************************** Modifications *********************************
*
*
*****************************************************************************
* (c) Copyright 1998, INRIA, all rights reserved
*****************************************************************************/
#include "winCycab.h"
#include "Rcan.h"

#include <stdlib.h>

int LooseCommE;
#define NO_EVENT  0
#define SET_EVENT 1
int pos_avg,pos_avd,pos_arg,pos_ard;
int pavg, pavd,parg,pard,pdir;
int joy_dir,joy_tra;
int pos_dir;

void * sampleControl(void *arg)
{
  int end = FALSE, free=FALSE;
  char key;
    
    
  while (end == FALSE){
    usleep(100 MS);
    /* Read Cycab State */
    cycabGetPos(&pos_avg, &pos_avd, &pos_arg, &pos_ard);
    cycabGetDirPos(&pos_dir);
    cycabGetJoystick(&joy_dir,&joy_tra);
    /* Put control      */
    pdir = joy_dir;
    pavg = pavd = pard = parg =  100*joy_tra; 
    cycabPutVelTop(pavg,pavd,parg,pard);
    cycabPutDirPos(pdir);
    

    key = winKey();
    /* Choose the mode */
    switch(key)
      {
      case 'q':
	end = TRUE;
	break;
      case 's':
	printf("pos wheels = %d %d %d %d\n",pos_avg,pos_avd,pos_arg,pos_ard);
	printf("dir = %d\n",pos_dir);
	printf("joystick = %d %d \n",joy_dir,joy_tra);
      }
  }
  winKill();

  return (0);
}


int main()
{
  int n;
  /* thread_t tid; */
  pthread_t tid;

   /* Init du Cycab (On regarde si les noeuds sont OK) */
  if (canInit() == ERROR) {
    fprintf(stderr, "can not initialized\n");
    LooseCommE = SET_EVENT;
  } else {
    LooseCommE = NO_EVENT;
  }
  
  if (cycabPutBrake(CYCAB_OFF) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  }
  if (cycabPutMotor(CYCAB_ON) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  }
  if (cycabPutDir(CYCAB_ON) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  }
  if (cycabPutVel(CYCAB_ON) == ERROR) {
    if (LooseCommE == NO_EVENT)
      LooseCommE = SET_EVENT;
  }

  /* Launch the control     */
  if (n = pthread_create(&tid, NULL, sampleControl,NULL)) {
    fprintf( stderr,"win::Cannot create thread: %s\n",strerror(n));
    exit(1);
  }  
  /* Wait end of the control */
  pthread_join(tid, NULL);
  return(OK);
}
