/********************************  Orccad ************************************
*
* File           : $ROBOTIQUE/Cycab/orccad/simu/win.c
* Author         : Roger Pissard-Gibollet a
* Version        : 3.1
* Creation       : 21 February 2000
*
****************************** Description **********************************
*
*  Implementation of an API to simulate Cycab acces
*
***************************** Modifications *********************************
*
*
*****************************************************************************
* (c) Copyright 2000, INRIA, all rights reserved
*****************************************************************************/

#include "winCycab.h"

/* Global Window variables */
Ginfo g;

/* ---------------------------------------------------- */
/*  Internal Functions                                  */
/* ---------------------------------------------------- */

/*
Compute the robot velocities model
 */
void winModel() 
{
  
  static   double mouse[2],olddir;
  int i;

  /*
    Saturation on velocities input
    */
  for (i = 0 ; i<NBWHEELS; i++){
    if (g.uwheels[i] > VMAXWHEELS ){
      g.uwheels[i] = VMAXWHEELS;
    }
    else if (g.uwheels[i] < - VMAXWHEELS){
      g.uwheels[i] = - VMAXWHEELS;
    }
  }
  
  /*
   compute the Cycab Model
  */
  for (i = 0 ; i<NBWHEELS; i++){
      g.vwheels[i] = g.uwheels[i];
      g.pwheels[i] = g.pwheels[i] + g.vwheels[i]*DT;
  }
  /*
    Compute if Limits are reached in direction
    */
  olddir =  g.pdir;
  if (g.udir > PDIRMAX)
    g.pdir = PDIRMAX;
  else 
    if (g.udir < - PDIRMAX)
      g.pdir = - PDIRMAX;
    else
      g.pdir = g.udir;

  /* mettre 1 premier ordre sur les vitesses ? */
  g.vdir = (g.pdir- olddir)/DT;
 
  /*
    Compute the joystick.
  */
  /* Mouse position -> joystick  */
  if ( (g.mousex <= g.winX/2+JOYSIZE/2) && (g.mousex >= g.winX/2-JOYSIZE/2) &&
       (g.mousey <= JOYSIZE) ) {
    g.joystick[1]  = (600/JOYSIZE)*(g.mousex - g.winX/2);
    g.joystick[0]  = (600/JOYSIZE)*(-g.mousey + JOYSIZE/2);
  }
  
}

/*
Draw the window with the robot
 */
void winDraw()
{
  XSegment joy[3];
  winModel();
  /* double buffering */
  /* draw in the pixmap */
  XSetForeground(g.disp, g.gc, g.white);
  XFillRectangle(g.disp,g.db,g.gc, 0, 0, g.winX, g.winY);
  /* Draw the joystick  */
  XSetForeground(g.disp, g.gc, g.black);
  XDrawRectangle(g.disp,g.db,g.gc, g.winX/2-JOYSIZE/2,0, JOYSIZE, JOYSIZE); 
  /*
  XDrawRectangle(g.disp,g.db,g.gc, g.winX/2-JOYSIZE/2,0, JOYSIZE/2, JOYSIZE/2);
  XDrawRectangle(g.disp,g.db,g.gc, g.winX/2,JOYSIZE/2, JOYSIZE/2, JOYSIZE/2);
  */

  joy[0].x1 = g.mousex  - JOYCROIX/2; 
  joy[0].y1 = g.mousey;
  joy[0].x2 = joy[0].x1 + JOYCROIX;
  joy[0].y2 = joy[0].y1 ;
  joy[1].x1 = g.mousex; 
  joy[1].y1 = g.mousey - JOYCROIX/2; 
  joy[1].x2 = joy[1].x1 ;
  joy[1].y2 = joy[1].y1 + JOYCROIX ;
  joy[2].x1 = g.mousex; 
  joy[2].y1 = g.mousey; 
  joy[2].x2 = g.winX/2 ;
  joy[2].y2 = JOYSIZE/2;

  XDrawSegments(g.disp, g.db, g.gc, joy, 3);
 
  /* then copy the pixmap in the window */
  XCopyArea(g.disp, g.db, g.win, g.gc, 0, 0,g.winX, g.winY, 0, 0); 
  /* force the screen refresh */
  XFlush(g.disp);
}

/*
function launched in a thread.
 */
void * winLoopThread(void *arg)
{
  struct timeval tempo;
  char string[TMPSTRLEN];
  int numEvents;
  /* Event Loop   */
  while(g.end==FALSE) {
    usleep(DT MICROS);
    numEvents = XEventsQueued(g.disp, QueuedAfterReading);
    if(numEvents)
      XNextEvent(g.disp,&(g.event));
    switch(g.event.type){
    case KeyPress:
      XLookupString(&(g.event.xkey),string,TMPSTRLEN,NULL,NULL);
      g.key=string[0];
      break;
    case ButtonPress:
    case MotionNotify:
	g.mousex = (int) g.event.xmotion.x;
	g.mousey = (int) g.event.xmotion.y;
	if ( (g.mousex > g.winX/2+JOYSIZE/2) || (g.mousex < g.winX/2-JOYSIZE/2) ||
	     (g.mousey > JOYSIZE) ) {
	  g.mousex =  g.winX/2;
	  g.mousey = JOYSIZE/2;
	  
	}
      break;
    }
    winDraw();
  }
  return( 0 );
}

/* ---------------------------------------------------- */
/*  Internal Functions                                  */
/* ---------------------------------------------------- */


/* Function Name : winkill = destroy window
   return        : OK
 */
int winInit(int x, int y)
{
  int             n,i;
  
  /* Users Parameters */
  if ((x<0)  || (x >  XMAX) ||(y<0)  || (y >  YMAX)) {
    return(ERROR);
  }
  g.dispX = x ; g.dispY = y;
  /*  g.winX  = WXSIZE ; g.winY = WYSIZE ;*/
  g.winX  = x; g.winY =  y;

  /* Put zero to position and velocities */
  for (i = 0 ; i<NBWHEELS; i++){
    g.pwheels[i] = g.vwheels[i] = 0.0;
  }

  g.pdir=g.vdir = g.brake = g.menable = g.denable = g.venable = 0.0;
  g.mousex = g.winX/2; g.joystick[0] = 0.0;
  g.mousey = JOYSIZE/2; g.joystick[1] = 0.0;

  g.refresh   = TRUE;
  g.key       = 0;
  /* Open Display     */
  if((g.disp = XOpenDisplay(NULL)) == NULL){
    fprintf(stderr, "winInit::Cannot connect to server\n");
    return(ERROR);
  }
  /* Create Window    */
  g.screen = DefaultScreen(g.disp);
  g.win = XCreateSimpleWindow(g.disp, RootWindow(g.disp,g.screen), 
			      g.dispX, g.dispY, g.winX, g.winY, 0, 0, 0);
  g.end = FALSE;
  XStoreName(g.disp, g.win, "Robot Simulation"); 
  /* Make our graphics context */
  g.gc = XCreateGC(g.disp, g.win, 0x0, NULL); 
  g.black = (long)BlackPixel(g.disp, g.screen);
  g.white = (long)WhitePixel(g.disp, g.screen);
  XSetWindowBackground(g.disp, g.win, g.black);
  XSetForeground(g.disp, g.gc, g.white);
  /* Double buffer creation */
  g.db = XCreatePixmap(g.disp,DefaultRootWindow(g.disp), g.winX, g.winY, DefaultDepth(g.disp,g.screen));
  /* Filter Event */
  XSelectInput(g.disp, g.win, KeyPressMask| ButtonPressMask |
	       ButtonReleaseMask | ButtonMotionMask|
	       StructureNotifyMask | ExposureMask | ColormapChangeMask);
  /* Map Window   */
  XMapWindow(g.disp,g.win);
  /* Launch the X Loop */
  if ( n = pthread_create(&(g.tid),NULL,winLoopThread,NULL)) {
    fprintf( stderr,"winInit::Cannot create thread: %s\n",strerror(n));
    return(ERROR);
  }
  return(OK);
}

/* Function Name : winkill = destroy window
   return        : OK
 */
int winKill()
{
  g.end = TRUE;
  return(OK);
}

/* Function Name : winKey = initialize window
   return        : the key write on keyboard (the mouse must be on window)
 */
char winKey()
{
  return (g.key);
}

/* ---------------------------------------------------- */
/*  External Functions                                  */
/* ---------------------------------------------------- */

int canInit()
{
  int st; 
  st = winInit(300,300);
  return(st);
}

/* a?? in top coder */
int cycabGetPos(int *avg, int *avd, int *arg, int *ard)
{
  *avg = (int) g.pwheels[0];
  *avd = (int) g.pwheels[1];
  *arg = (int) g.pwheels[2];
  *ard = (int) g.pwheels[3];
  
  return(OK);
}

/* etat = 0, cycab stopped by brakes 
   etat = -1, cycab can run */
int cycabPutBrake(int etat){
  g.brake = etat;
  return(OK);
}

int cycabGetBrake(int *etat){
  *etat = g.brake;
  return(OK);
}
/* etat = 0, enable motor
   etat = -1, disable motor */
int cycabPutMotor(int etat)
{
  g.menable = etat;
  g.refresh   = TRUE;
  return(OK);
}
int cycabGetMotor(int *etat)
{
  *etat =g.menable;
  return(OK);
}
/* etat = 0, direction loop TIMER_routine is activated
   etat = -1, direction loop TIMER _routine is Disactivated */
int cycabPutDir(int etat)
{
  g.denable = etat;
  return(OK);
}
int cycabGetDir(int *etat)
{
  *etat = g.denable;
  return(OK);
}
/* centieme_degre in 1/100 deg (between -3500 and +3500) */
int cycabPutDirPos(int centieme_degre)
{
  g.udir = (double) centieme_degre;
  return(OK);
}
int cycabGetDirPos(int *centieme_degre)
{
  *centieme_degre = (int) g.pdir;
  return(OK);
}

/* etat_??? bit 0  1  2  3  4  5  6  7
                F0 F1 AU TE AL Fg Fd AR
		F0 fault on motor A
		F1 fault on motor B
		AU emergency stop activated
		TE not used anymore
		AL not used anymore
		Fg left switch limit (on direction only)
		Fd right switch limit (on direction only)
		AR emergency stop memorised
		*/
int cycabGetState(int *etat_ava, int *etat_arr, int *etat_dir)
{
  char key;

  *etat_ava = *etat_arr = *etat_dir = 0xFF;
  key = winKey();

  switch(key)
    {
    case 'q':
      *etat_ava = *etat_arr = *etat_dir = 0x7F;
      break;
    case 's':
      printf("Wheels pos= %g %g %g %g\n",
	     TOP_TO_RADIAN(g.pwheels[0]),
	     TOP_TO_RADIAN(g.pwheels[1]),
	     TOP_TO_RADIAN(g.pwheels[2]),
	     TOP_TO_RADIAN(g.pwheels[3]));
      printf("Wheels vel= %g %g %g %g\n",
	     TOP_TO_RADIAN(g.vwheels[0]),
	     TOP_TO_RADIAN(g.vwheels[1]),
	     TOP_TO_RADIAN(g.vwheels[2]),
	     TOP_TO_RADIAN(g.vwheels[3]));
      printf("Direction: pos= %g \n",(g.pdir * M_PI)/18000);
      break;
    }

  return(OK);
}

/* j_??? between -300 and +300 */
int cycabGetJoystick(int *j_dir, int *j_tra)
{
  *j_dir = (int) g.joystick[1];
  *j_tra = (int) g.joystick[0];
  return(OK);
}

/* utilities for velocity control on cycab */
/* etat = 0, direction loop TIMER_routine is activated
   etat = -1, direction loop TIMER _routine is Disactivated */
int cycabPutVel(int etat)
{
  g.venable = etat;
  return(OK);
}
int cycabGetVel(int *etat)
{
  *etat = g.venable;
  return(OK);
}

/* value are in top/sec, between -100000 and +100000 */
int cycabPutVelTop(int avg, int avd, int arg, int ard)
{
  g.uwheels[0] = (double) avg;
  g.uwheels[1] = (double) avd;
  g.uwheels[2] = (double) arg;
  g.uwheels[3] = (double) ard;
  return(OK);
}
int cycabGetVelTop(int *avg, int *avd, int *arg, int *ard)
{
  *avg = (int) g.vwheels[0];
  *avd = (int) g.vwheels[1];
  *arg = (int) g.vwheels[2];
  *ard = (int) g.vwheels[3];
  
  return(OK);
}


int _libc_sigprocmask()
{
  return(0);
}
