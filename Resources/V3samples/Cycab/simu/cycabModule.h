#ifndef cycabModule_h
#define cycabModule_h

#ifndef M_PI
#define M_PI 3.1415927
#endif

#define TOP_TO_RADIAN(t) (M_PI * ((double)t) / 20480.0)
#define RADIAN_TO_TOP(t) (((double)t) * 20480.0 / M_PI)

/* frequence timer sur le 68332 (configuration au 01/99) */
#define FREQUENCE_TIMER (9.76/1000.0)
#define FREQUENCE_VELOCITY (2.0 * FREQUENCE_TIMER)

#define BIT(a,b) ((a >> b) & 0x1)

/* CYCAB_ON (0) and CYCAB_OFF (-1) */
/* return OK if success, ERROR else */
#ifdef __cplusplus
extern "C" {
#endif

/* etat = 0, cycab stopped by brakes 
   etat = -1, cycab can run */
extern int cycabPutBrake(int etat);
extern int cycabGetBrake(int *etat);

/* etat = 0, enable motor
   etat = -1, disable motor */
extern int cycabPutMotor(int etat);
extern int cycabGetMotor(int *etat);

/* a?? in top coder */
extern int cycabGetPos(int *avg, int *avd, int *arg, int *ard);

/* etat = 0, direction loop TIMER_routine is activated
   etat = -1, direction loop TIMER _routine is Disactivated */
extern int cycabPutDir(int etat);
extern int cycabGetDir(int *etat);

/* centieme_degre in 1/100 deg (between -3500 and +3500) */
extern int cycabPutDirPos(int centieme_degre);
extern int cycabGetDirPos(int *centieme_degre);


/* etat_??? bit 0  1  2  3  4  5  6  7
                F0 F1 AU TE AL Fg Fd AR
		F0 fault on motor A
		F1 fault on motor B
		AU emergency stop activated
		TE not used anymore
		AL not used anymore
		Fg left switch limit (on direction only)
		Fd right switch limit (on direction only)
		AR emergency stop memorised
		*/
extern int cycabGetState(int *etat_ava, int *etat_arr, int *etat_dir);

/* j_??? between -300 and +300 */
extern int cycabGetJoystick(int *j_dir, int *j_tra);

/* utilities for velocity control on cycab */
/* etat = 0, direction loop TIMER_routine is activated
   etat = -1, direction loop TIMER _routine is Disactivated */
extern int cycabPutVel(int etat);
extern int cycabGetVel(int *etat);

/* value are in top/sec, between -100000 and +100000 */
extern int cycabPutVelTop(int avg, int avd, int arg, int ard);
extern int cycabGetVelTop(int *avg, int *avd, int *arg, int *ard);

#ifdef __cplusplus
}
#endif
#endif
