/********************************  Orccad ************************************
*
* File           :  $ROBOTIQUE/Cycab/orccad/simu/win.c
* Author         : Roger Pissard-Gibollet 
* Version        : 3.1
* Creation       : 21 February 2000
*
****************************** Description **********************************
*
*  Implementation of an API to simulate Cycab acces
*
***************************** Modifications *********************************
*
*
*****************************************************************************
* (c) Copyright 2000, INRIA, all rights reserved
*****************************************************************************/

#ifndef __win_h
#define __win_h
#include <stdio.h>
#include <math.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/X.h>
#include <unistd.h>
#include <string.h>
#include <malloc.h>
#include <pthread.h>
#include <assert.h>

/*
Configuration of the application 
*/
/* size of the windows              */
#define XMAX      800
#define YMAX      800
/* Graphic Robot Frame Definition   */
#define RBASEX     g.winX/2
#define RBASEY     g.winY/2
#define JOYSIZE    40
#define JOYCROIX    4
/* simulator rate (units:seconds)   */
#define DT        0.01
#define MS        *1000
#define MICROS    *1000000
/* Description of Cycab             */
#define NBWHEELS 4
#define VMAXWHEELS 100000
#define PDIRMAX    3500
#define JOYMAX     300
/*
Standard definition
*/
#define TRUE      1
#define FALSE     0
#define OK        0
#define ERROR     -1
#define TMPSTRLEN 10
#define STRLEN    80


/*
Definition of the structure which maintains
the state of the application
*/
typedef struct GINFO{
  /* Display         */
  Display  *disp;
  char     *dispname;
  /* Graphics        */
  GC       gc;
  Pixmap db;
  long black, white;
  /* Window          */
  Window   win;
  int screen;
  /* Window Geometry */
  int dispX, dispY, winX, winY;
  /* Window Event    */
  XEvent event;
  int    end;
  int    refresh;
  char   key;
  int    mousex;
  int    mousey;
  /* input control   */
  double uwheels[NBWHEELS];
  double udir; 
  /* Cycab State     */
  double pwheels[NBWHEELS];
  double vwheels[NBWHEELS];
  double pdir,vdir;
  int brake;
  int menable;
  int denable;
  int venable;
  /* joystick        */
  double joystick[2];
  /* Thread Info     */ 
  pthread_t       tid;
} Ginfo;

/*
User functions exportation
*/
#ifdef __cplusplus
extern "C" {
#endif

#include "drvCycab.h"

#ifdef __cplusplus
}
#endif

#endif
