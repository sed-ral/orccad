/********************************  Orccad ************************************
*
* File           : Samples/Misc/Drv/win.c
* Author         : Roger Pissard-Gibollet and Nicolas Turro
* Version        : 3.0 alpha
* Creation       : 12 March  1998
*
****************************** Description **********************************
*
*  Implementation of an API to simulate a robot arm with 2 d.o.f
*
***************************** Modifications *********************************
*
*
*****************************************************************************
* (c) Copyright 1998, INRIA, all rights reserved
*****************************************************************************/

#include "win.h"
#include <signal.h>
#include <errno.h>
#define SIGTIMER SIGALRM

/* Function to mask timer signal interrruption */
int TimerSigMask() {
  sigset_t allsig;

  sigemptyset(&allsig);
  sigaddset(&allsig, SIGTIMER);
  if (pthread_sigmask(SIG_UNBLOCK,&allsig,NULL) != OK) {
    printf("pthread_sigmask Failed %d \n", errno);
    return ERROR;
  }

  return OK;
}

/* Global Window variables */
Ginfo g;

/* ---------------------------------------------------- */
/*  Internal Functions                                  */
/* ---------------------------------------------------- */

/*
Compute the robot model
 */
void winModel() 
{
  static   double m11,m12,m22,mm11,mm12,mm21,mm22,Delta;
  static   double f[2];
  static   double v[2];
  static   double grav[2];
  static   double tau[2];
  static   double frot[2];
  static   double c1,c2,s1,s2,c12,s12;
  static   double mouse[2];
  int i;

  /*
    Computation of the dynamic inverse model of the 2 axis robot:
    t_dd =  M-1( pos.(tau - v - grav) - frot) 
    where :
     M-1 : inverse of Inertia Matrix
       v : Coriolis vector
     grav: Gravity term
     frot: friction forces
     tau : joint torque 
     pos : joint positions
     t_d : joint velocities
     t-dd: joint acceleration
   */
  /*
    Compute each component.
    Detail in Introduction to Robotics, Mechanics and Control;
              J. Craig, Addison-Wesley 1989 pp 202-207
    */

  /*
    Saturation on torque
    */
  for (i = 0 ; i<NBAXIS; i++){
    tau[i]=g.tau[i];
    if (g.tau[i] > TORQUEMAX ){
      tau[i]   = TORQUEMAX;
      g.tau[i] = TORQUEMAX;
    }
    else if (g.tau[i] < - TORQUEMAX){
      tau[i] = - TORQUEMAX;
      g.tau[i] = - TORQUEMAX;
    }
  }
  /* Compute cos and sin */
  c1 = cos(g.pos[0]);
  c2 = cos(g.pos[1]);
  s1 = sin(g.pos[0]);
  s2 = sin(g.pos[1]);
  c12= cos(g.pos[0]+g.pos[1]);
  s12= sin(g.pos[0]+g.pos[1]);
  /* Inertia Matrix */
  m11=L2*L2*M2+2*L1*L2*M2*c2+L1*L1*(M1+M2);
  m12=L2*L2*M2+L1*L2*M2*c2;
  m22=L2*L2*M2;
  /* Inversion of the Inertia Matrix */
  Delta = m11*m22-m12*m12;
  if (Delta!=0.0)
    Delta=1/Delta;
  else
    fprintf(stderr,"Inverse Inertia Matrix error\n");
  mm11=m22*Delta;
  mm12=-m12*Delta;
  mm21=mm12;
  mm22=m11*Delta;
  /* Coriolis force                  */
  v[0]=-M2*L1*L2*s2*g.t_d[1]*g.t_d[1]-2*M2*L1*L2*s2*g.t_d[1]*g.t_d[0];
  v[1]=M2*L1*L2*s2*g.t_d[1]*g.t_d[0];
  /* Gravity force                    */
  grav[0]=M2*L2*GRAV*c12 + (M1+M2)*L1*GRAV*c1;
  grav[1]=M2*L2*GRAV*c12;
  /* Friction  (viscosity model)      */
  frot[0]=FRIC1*g.t_d[0];
  frot[1]=FRIC2*g.t_d[1];
  /*
    Compute t_dd 
    */
  f[0]=(tau[0]-grav[0] - v[0] ) - frot[0];
  f[1]=(tau[1]-grav[1] - v[1] ) - frot[1];
  g.t_dd[0]=mm11*f[0]+mm12*f[1];
  g.t_dd[1]=mm21*f[0]+mm22*f[1];
  /*
    Integration 
    */
  g.t_d[0]=g.t_d[0]+g.t_dd[0]* DT;
  g.t_d[1]=g.t_d[1]+g.t_dd[1]* DT;

  g.pos[0]=g.pos[0]+g.t_d[0]* DT+.5*g.t_dd[0]*DT*DT;
  g.pos[1]=g.pos[1]+g.t_d[1]* DT+.5*g.t_dd[1]*DT*DT;

  /*
    Compute if Limits are reached in position
    */
  for (i = 0 ; i<NBAXIS; i++){
    g.limit[i] = FALSE;
    if (g.pos[i] > LIMITMAX ){
      g.pos[i] = LIMITMAX;
      g.limit[i] = TRUE;
    }
    else if (g.pos[i] < LIMITMIN){
      g.pos[i] = LIMITMIN;
      g.limit[i] = TRUE;
    }
  }
  /*
    Compute the sensor values.
  */
  /* Mouse position in robot frame  */
  mouse[0]  = (g.mousex - RBASEX)/WPIXMET;
  mouse[1] = - (g.mousey - RBASEY)/WPIXMET;
  /* cartesian Model p 167-169 */
  /* [ c12  -s12  c1.l1 + c12.l2 ]  */
  /* [ s12   c12  s1.l1 + s12.l2 ]  */
  /* [   0     0               1 ]  */ 
  /* Sensor = effector position - mouse position */
  /* in robot frame                              */
  mouse[0] = mouse[0]  - (c1*L1 + c12*L2);
  mouse[1] = mouse[1]  - (s1*L1 + s12*L2);
  /* Sensor in Effector Frame */
  g.sensorx = c12*mouse[0] + s12*mouse[1];
  g.sensory = -s12*mouse[0] + c12*mouse[1];
  
}

/*
Draw the window with the robot
 */
void winDraw()
{
  XSegment segs[NBAXIS+4];
  double q1,q2;

  winModel();
  /* first link        */
  segs[0].x1 = RBASEX;
  segs[0].y1 = RBASEY;
  segs[0].x2 = segs[0].x1 + L1*WPIXMET*cos(g.pos[0]);
  segs[0].y2 = segs[0].y1 - L1*WPIXMET*sin(g.pos[0]);
  /* second link       */
  segs[1].x1 = segs[0].x2;
  segs[1].y1 = segs[0].y2;
  segs[1].x2 = segs[1].x1 + L2*WPIXMET*cos(g.pos[0]+g.pos[1]);
  segs[1].y2 = segs[1].y1 - L2*WPIXMET*sin(g.pos[0]+g.pos[1]); 
  /* grip              */
  segs[2].x1 = segs[1].x2;
  segs[2].y1 = segs[1].y2;
  segs[2].x2 = segs[2].x1 + (LGRIP/2)*sin(g.pos[0]+g.pos[1]);
  segs[2].y2 = segs[2].y1 + (LGRIP/2)*cos(g.pos[0]+g.pos[1]);
  segs[3].x1 = segs[1].x2;
  segs[3].y1 = segs[1].y2;
  segs[3].x2 = segs[2].x1 - (LGRIP/2)*sin(g.pos[0]+g.pos[1]);
  segs[3].y2 = segs[2].y1 - (LGRIP/2)*cos(g.pos[0]+g.pos[1]);
  segs[4].x1 = segs[2].x2;
  segs[4].y1 = segs[2].y2;
  segs[4].x2 = segs[4].x1 + (LGRIP)*cos(g.pos[0]+g.pos[1]);
  segs[4].y2 = segs[4].y1 - (LGRIP)*sin(g.pos[0]+g.pos[1]);
  segs[5].x1 = segs[3].x2;
  segs[5].y1 = segs[3].y2;
  segs[5].x2 = segs[5].x1 + (LGRIP)*cos(g.pos[0]+g.pos[1]);
  segs[5].y2 = segs[5].y1 - (LGRIP)*sin(g.pos[0]+g.pos[1]);
  /* double buffering */
  /* draw in the pixmap */
  XSetForeground(g.disp, g.gc, g.black);
  XFillRectangle(g.disp,g.db,g.gc, 0, 0, g.winX, g.winY);
  XSetForeground(g.disp, g.gc, g.white);
  XDrawSegments(g.disp, g.db, g.gc, segs, NBAXIS+4);
  XDrawArc(g.disp, g.db,g.gc,segs[0].x1-RAXIS/2,segs[0].y1-RAXIS/2,
	   RAXIS,RAXIS,23040,23040);
  XDrawArc(g.disp, g.db,g.gc,segs[0].x2-RAXIS/2,segs[0].y2-RAXIS/2,
	   RAXIS,RAXIS,23040,23040); 
  XFillRectangle(g.disp,g.db,g.gc, g.mousex, g.mousey, DIMTARGET, DIMTARGET);
  /* then copy the pixmap in the window */
  XCopyArea(g.disp, g.db, g.win, g.gc, 0, 0,g.winX, g.winY, 0, 0); 
  /* force the screen refresh */
  XFlush(g.disp);
}

/*
function launched in a thread.
 */
void * winLoopThread(void *arg)
{
  char string[TMPSTRLEN];
  int numEvents;
  /* Event Loop   */
  while(g.end==FALSE) {
    usleep(DT MICROS);
    numEvents = XEventsQueued(g.disp, QueuedAfterReading);
    if(numEvents)
      XNextEvent(g.disp,&(g.event));
    switch(g.event.type){
    case KeyPress:
      XLookupString(&(g.event.xkey),string,TMPSTRLEN,NULL,NULL);
      g.key=string[0];
      break;
    case ButtonPress:
    case MotionNotify:
      g.mousex = (int) g.event.xmotion.x;
      g.mousey = (int) g.event.xmotion.y;
      break;
    }
    winDraw();
  }
  return( 0 );
}

/* ---------------------------------------------------- */
/*  External Functions                                  */
/* ---------------------------------------------------- */

/* Function Name : winkill = destroy window
   return        : OK
 */
int winInit(int x, int y)
{
  int             n,i;
  pthread_attr_t attributes;

  /* Users Parameters */
  if ((x<0)  || (x >  XMAX) ||(y<0)  || (y >  YMAX)) {
    return(ERROR);
  }
  g.dispX = x ; g.dispY = y;
  /*  g.winX  = WXSIZE ; g.winY = WYSIZE ;*/
  g.winX  = x; g.winY =  y;

  
  /* Put zero to position and velocities */
  for (i = 0 ; i<NBAXIS; i++){
    g.pos[i] = 0.0;
    g.tau[i] = 0.0;
    g.t_d[i] = 0.0;
    g.t_dd[i] = 0.0;
    g.mousex = RBASEX + (L1+L2+0.5)*WPIXMET; g.sensorx = L1+L2-0.5;
    g.mousey = RBASEY; g.sensory = 0.0;
  }
  g.refresh   = TRUE;
  g.key       = 0;
  /* Open Display     */
  if((g.disp = XOpenDisplay(NULL)) == NULL){
    fprintf(stderr, "winInit::Cannot connect to server\n");
    return(ERROR);
  }
  /* Create Window    */
  g.screen = DefaultScreen(g.disp);
  g.win = XCreateSimpleWindow(g.disp, RootWindow(g.disp,g.screen), 
			      g.dispX, g.dispY, g.winX, g.winY, 0, 0, 0);
  g.end = FALSE;
  XStoreName(g.disp, g.win, "Robot Simulation"); 
  /* Make our graphics context */
  g.gc = XCreateGC(g.disp, g.win, 0x0, NULL); 
  g.black = (long)BlackPixel(g.disp, g.screen);
  g.white = (long)WhitePixel(g.disp, g.screen);
  XSetWindowBackground(g.disp, g.win, g.black);
  XSetForeground(g.disp, g.gc, g.white);
  /* Double buffer creation */
  g.db = XCreatePixmap(g.disp,DefaultRootWindow(g.disp), g.winX, g.winY, DefaultDepth(g.disp,g.screen));
  /* Filter Event */
  XSelectInput(g.disp, g.win, KeyPressMask| ButtonPressMask |
	       ButtonReleaseMask | ButtonMotionMask|
	       StructureNotifyMask | ExposureMask | ColormapChangeMask);
  /* Map Window   */
  XMapWindow(g.disp,g.win);
  /* Launch the X Loop */
  TimerSigMask();

  pthread_attr_init(&attributes);
  if ( n = pthread_create(&(g.tid),&attributes,winLoopThread,NULL)) {
    fprintf( stderr,"winInit::Cannot create thread: %s\n",strerror(n));
    return(ERROR);
  }
  return(OK);
}

/* Function Name : winGetPos = gives the values of robot articulation
   parameters    : double pos[NBAXIS]  = robot articulation (radians)
   return        : OK
 */
int winGetPos(double pos[NBAXIS])
{
  int i;
  for (i = 0 ; i<NBAXIS; i++)
    pos[i] = g.pos[i];

  return(OK);
}

/* Function Name : winGetSensor = gives the position of target in effector frame
   parameters    : double spos[2] = x, y
   return        : OK
 */
int winGetSensor(double spos[2])
{
  spos[0] = g.sensorx;
  spos[1] = g.sensory;
  return(OK);
}

/* Function Name : winGetLimit = gives if limit articulation are reached
   parameters    : double limit[NBAXIS]  = limits (TRUE/FALSE)
   return        : OK
 */
int winGetLimit(int limit[NBAXIS])
{
  int i;
  for (i = 0 ; i<NBAXIS; i++)
    limit[i] = g.limit[i];

  return(OK);
}

/* Function Name : winPutTau = inputs the torque on robot
   parameters    : double tau[NBAXIS]  =  torque input (Nm)
   return        : OK
 */
int winPutTau(double tau[NBAXIS])
{
  int i;
  /* Users Parameters */
  for (i = 0 ; i<NBAXIS; i++)
    g.tau[i] = tau[i];
  
  g.refresh   = TRUE;
  return(OK);
}

/* Function Name : winkill = destroy window
   return        : OK
 */
int winKill()
{
  g.end = TRUE;
  return(OK);
}

/* Function Name : winKey = initialize window
   return        : the key write on keyboard (the mouse must be on window)
 */
char winKey()
{
  return (g.key);
}

