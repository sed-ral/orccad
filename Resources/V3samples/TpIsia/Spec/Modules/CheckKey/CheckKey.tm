// Ilv Version: 3.1
// File generated: Thu Jan 10 14:13:27 2002
// Creator class: IlvGraphOutput
Palettes 5
"default" 0 "white" "black" "default" "9x15" 0 solid solid 0 0 0
1 "white" "blue" "default" "9x15" 0 solid solid 0 0 0
3 "red" "red" "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "white" "lightsteelblue" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 130 90 170 160 
2 IlvMessageLabel 130 90 171 161 F8 0 1 16 4 "CheckKey"   [Box
0 1 1 CheckKey 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 120 120 20 20 
2 IlvMessageLabel 140 116 44 28 F8 0 25 16 4 "Key"   0
[Port
0 1 1 Key 130 130 140 116 44 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 210 240 20 20 2 
2 IlvMessageLabel 200 226 80 28 F8 0 25 16 4 "EndOK"   0
[Port
0 1 2 EndOK 220 250 200 226 80 28 8  8 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
EOF
