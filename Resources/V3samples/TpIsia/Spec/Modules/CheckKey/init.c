// Orccad Version: 3.2
// Module : CheckKey
// Initialisation File
// 
// Module Ports :
// 	input INTEGER KeyI
// 	output EVENT EndOKE
// 
// Time Methods usable in module files:
// 	 GetLocalTime(): return relative period time as double value
// 	 GetCurrentTime(): return global period time as double value 
// 
// Date of creation : Thu Jan 10 14:07:48 2002

