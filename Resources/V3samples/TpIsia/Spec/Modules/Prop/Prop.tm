// Ilv Version: 2.4
// File generated: Thu Feb 18 23:20:10 1999
// Creator class: IlvGraphOutput
Palettes 6
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 100 110 110 90 
5 IlvMessageLabel 100 110 111 91 0 "Prop" 16 [Box
0 1 1 Prop 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 120 20 20 
2 IlvMessageLabel 112 116 52 28 0 "Goal" 1 0
[Port
0 1 1 Goal 100 130 112 116 52 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 200 120 20 20 
2 IlvMessageLabel 177 116 42 28 0 "Tau" 1 0
[Port
0 1 2 Tau 210 130 177 116 42 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 160 20 20 
2 IlvMessageLabel 112 156 42 28 0 "Pos" 1 0
[Port
0 1 3 Pos 100 170 112 156 42 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
