// Orccad Version: 3.0 alpha   
// Module : Prop
// Computation File 
//
// Module Ports :
// 	input DOUBLE Goal[2]
// 	output DOUBLE Tau[2]
//
// Date of creation : Thu Feb 18 23:10:38 1999

Tau[0]= 5000*(Goal[0] - Pos[0]);
Tau[1] =5000*(Goal[1] - Pos[1]);
