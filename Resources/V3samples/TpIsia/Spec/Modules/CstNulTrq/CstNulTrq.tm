// Ilv Version: 2.4
// File generated: Wed Feb 17 17:09:11 1999
// Creator class: IlvGraphOutput
Palettes 5
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 3
3 0 { 0 0 OrcIlvModAlgo 2
3 IlvFilledRectangle 150 90 110 110 
4 IlvMessageLabel 150 90 111 111 0 "CstNulTrq" 16 [Box
0 1 1 CstNulTrq 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 250 120 20 20 
2 IlvMessageLabel 231 116 34 28 0 "tau" 1 0
[Port
0 1 1 tau 260 130 231 116 34 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
