// Orccad Version: 3.0 alpha
// Module : Traj
// Initialisation File
//
// Module Ports :
// 	output DOUBLE Pos[2]
// 	param DOUBLE Dest[2]
// 	param DOUBLE Time
//
// Date of creation : Thu Feb 18 23:24:22 1999

nbtick=Time/getSampleTime();
printf("ticks = %d\n",nbtick);
n=0;
inc[0]=(Dest[0]-theta[0])/nbtick;
inc[1]=(Dest[1]-theta[1])/nbtick;
printf("inc=%f %f\n",inc[0],inc[1]);
Pos[0]=theta[0];
Pos[1]=theta[1];
