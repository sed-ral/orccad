// Ilv Version: 2.4
// File generated: Thu Feb 18 23:42:51 1999
// Creator class: IlvGraphOutput
Palettes 6
3 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 100 90 120 90 
5 IlvMessageLabel 100 90 121 91 0 "Traj" 16 [Box
0 1 1 Traj 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 210 110 20 20 
2 IlvMessageLabel 187 106 42 28 0 "Pos" 1 0
[Port
0 1 1 Pos 220 120 187 106 42 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortPrm 2
3 IlvPolyline 11
132 82 148 98 140 90 148 82 132 98 140 90 148 90 132 90 140 90 140 82
140 98 
2 IlvMessageLabel 128 95 50 28 0 "Dest" 1 0
[Port
0 1 2 Dest 140 90 128 95 50 28 2  2 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortPrm 2
3 IlvPolyline 11
172 82 188 98 180 90 188 82 172 98 180 90 188 90 172 90 180 90 180 82
180 98 
2 IlvMessageLabel 167 95 52 28 0 "Time" 1 0
[Port
0 1 3 Time 180 90 167 95 52 28 2  2 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
2 0 { 7 0 OrcIlvPortVar 2
3 IlvFilledEllipse 90 110 20 20 
2 IlvMessageLabel 112 106 54 28 0 "theta" 1 0
[Port
0 1 4 theta 100 120 112 106 54 28 1  1 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 26
