// Orccad Version: 3.0 alpha   
// Module : Traj
// Computation File 
//
// Module Ports :
// 	output DOUBLE Pos[2]
// 	param DOUBLE Dest[2]
// 	param DOUBLE Time
//
// Date of creation : Thu Feb 18 23:24:22 1999

if (n < nbtick) 
{
  Pos[0]+=inc[0];
  Pos[1]+=inc[1];}
else 
{
  Pos[0]=Dest[0];
  Pos[1]=Dest[1];}
n++;
