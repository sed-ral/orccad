// Orccad Version: 3.0 alpha   
// Module : GravComp
// Computation File 
//
// Module Ports :
// 	input DOUBLE Grav[2]
// 	output DOUBLE Tau[2]
//
// Date of creation : Thu Feb 18 22:31:21 1999

Tau[0]=Grav[0];
Tau[1]=Grav[1];
