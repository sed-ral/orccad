// Ilv Version: 2.4
// File generated: Thu Feb 18 22:31:21 1999
// Creator class: IlvGraphOutput
Palettes 6
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 120 120 110 80 
5 IlvMessageLabel 120 120 111 81 0 "GravComp" 16 [Box
0 1 1 GravComp 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 110 130 20 20 
2 IlvMessageLabel 132 126 54 28 0 "Grav" 1 0
[Port
0 1 1 Grav 120 140 132 126 54 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 220 170 20 20 
2 IlvMessageLabel 197 166 42 28 0 "Tau" 1 0
[Port
0 1 2 Tau 230 180 197 166 42 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
