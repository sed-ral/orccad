// Ilv Version: 2.4
// File generated: Fri Feb 19 00:18:42 1999
// Creator class: IlvGraphOutput
Palettes 6
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 110 90 110 100 
5 IlvMessageLabel 110 90 111 101 0 "PropGrav" 16 [Box
0 1 1 PropGrav 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 90 20 20 
2 IlvMessageLabel 122 86 52 28 0 "Goal" 1 0
[Port
0 1 1 Goal 110 100 122 86 52 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 110 20 20 
2 IlvMessageLabel 122 106 54 28 0 "Grav" 1 0
[Port
0 1 2 Grav 110 120 122 106 54 28 1  1 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
2 0 { 5 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 160 20 20 
2 IlvMessageLabel 122 156 42 28 0 "Pos" 1 0
[Port
0 1 3 Pos 110 170 122 156 42 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
2 0 { 7 0 OrcIlvPortVar 2
3 IlvFilledEllipse 210 100 20 20 
2 IlvMessageLabel 187 96 42 28 0 "Tau" 1 0
[Port
0 1 4 Tau 220 110 187 96 42 28 4  4 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 26
