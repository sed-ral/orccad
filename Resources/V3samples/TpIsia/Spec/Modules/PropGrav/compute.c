// Orccad Version: 3.0 alpha   
// Module : PropGrav
// Computation File 
//
// Module Ports :
// 	input DOUBLE Goal[2]
// 	input DOUBLE Grav[2]
// 	input DOUBLE Pos[2]
// 	output DOUBLE Tau[2]
//
// Date of creation : Fri Feb 19 00:18:07 1999

Tau[0]=5000*(Goal[0] - Pos[0])+Grav[0];
Tau[1]=5000*(Goal[1] - Pos[1])+Grav[1];
