// Ilv Version: 2.4
// File generated: Thu Feb 18 22:21:04 1999
// Creator class: IlvGraphOutput
Palettes 6
5 "gray" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
3 "gray" "red" "default" "fixed" 0 solid solid 0 0 0
"default" 0 "gray" "black" "default" "fixed" 0 solid solid 0 0 0
2 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 "gray" "blue" "default" "fixed" 0 solid solid 0 0 0
4 "gray" "lightsteelblue" "default" "fixed" 0 solid solid 0 0 0
IlvObjects 5
3 0 { 0 0 OrcIlvModAlgo 2
4 IlvFilledRectangle 110 120 150 100 
5 IlvMessageLabel 110 120 151 101 0 "Grav" 16 [Box
0 1 1 Grav 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 100 130 20 20 
2 IlvMessageLabel 122 126 54 28 0 "theta" 1 0
[Port
0 1 1 theta 110 140 122 126 54 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 26
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 250 190 20 20 
2 IlvMessageLabel 239 186 18 28 0 "G" 1 0
[Port
0 1 2 G 260 200 239 186 18 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 26
