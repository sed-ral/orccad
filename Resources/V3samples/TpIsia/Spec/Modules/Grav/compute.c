// Orccad Version: 3.0 alpha   
// Module : Grav
// Computation File 
//
// Module Ports :
// 	input DOUBLE theta[2]
// 	output DOUBLE G[2]
//
// Date of creation : Thu Feb 18 22:21:04 1999


G[0]=M2*L2*GRAV*cos(theta[0]+theta[1])+(M1+M2)*L1*GRAV*cos(theta[0]);
G[1]=M2*L2*GRAV*cos(theta[0]+theta[1]);

