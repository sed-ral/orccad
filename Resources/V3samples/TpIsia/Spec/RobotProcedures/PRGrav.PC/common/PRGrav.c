/* sscc : C CODE OF SORTED EQUATIONS PRGrav - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __PRGrav_GENERIC_TEST(TEST) return TEST;
typedef void (*__PRGrav_APF)();
static __PRGrav_APF *__PRGrav_PActionArray;

#include "PRGrav.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _TrGrav_controler_DEFINED
#ifndef TrGrav_controler
extern void TrGrav_controler(orcStrlPtr);
#endif
#endif
#ifndef _TrGrav_fileparameter_DEFINED
#ifndef TrGrav_fileparameter
extern void TrGrav_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _TrGrav_parameter_DEFINED
#ifndef TrGrav_parameter
extern void TrGrav_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __PRGrav_V0;
static boolean __PRGrav_V1;
static orcStrlPtr __PRGrav_V2;
static boolean __PRGrav_V3;
static boolean __PRGrav_V4;
static boolean __PRGrav_V5;
static boolean __PRGrav_V6;


/* INPUT FUNCTIONS */

void PRGrav_I_START_PRGrav () {
__PRGrav_V0 = _true;
}
void PRGrav_I_Abort_Local_PRGrav () {
__PRGrav_V1 = _true;
}
void PRGrav_I_TrGrav_Start (orcStrlPtr __V) {
_orcStrlPtr(&__PRGrav_V2,__V);
__PRGrav_V3 = _true;
}
void PRGrav_I_USERSTART_TrGrav () {
__PRGrav_V4 = _true;
}
void PRGrav_I_BF_TrGrav () {
__PRGrav_V5 = _true;
}
void PRGrav_I_T3_TrGrav () {
__PRGrav_V6 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __PRGrav_A1 \
__PRGrav_V0
#define __PRGrav_A2 \
__PRGrav_V1
#define __PRGrav_A3 \
__PRGrav_V3
#define __PRGrav_A4 \
__PRGrav_V4
#define __PRGrav_A5 \
__PRGrav_V5
#define __PRGrav_A6 \
__PRGrav_V6

/* OUTPUT ACTIONS */

#define __PRGrav_A7 \
PRGrav_O_BF_PRGrav()
#define __PRGrav_A8 \
PRGrav_O_STARTED_PRGrav()
#define __PRGrav_A9 \
PRGrav_O_GoodEnd_PRGrav()
#define __PRGrav_A10 \
PRGrav_O_Abort_PRGrav()
#define __PRGrav_A11 \
PRGrav_O_T3_PRGrav()
#define __PRGrav_A12 \
PRGrav_O_START_TrGrav()
#define __PRGrav_A13 \
PRGrav_O_Abort_Local_TrGrav()

/* ASSIGNMENTS */

#define __PRGrav_A14 \
__PRGrav_V0 = _false
#define __PRGrav_A15 \
__PRGrav_V1 = _false
#define __PRGrav_A16 \
__PRGrav_V3 = _false
#define __PRGrav_A17 \
__PRGrav_V4 = _false
#define __PRGrav_A18 \
__PRGrav_V5 = _false
#define __PRGrav_A19 \
__PRGrav_V6 = _false

/* PROCEDURE CALLS */

#define __PRGrav_A20 \
TrGrav_parameter(__PRGrav_V2)
#define __PRGrav_A21 \
TrGrav_controler(__PRGrav_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __PRGrav_A22 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int PRGrav_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __PRGrav__reset_input () {
__PRGrav_V0 = _false;
__PRGrav_V1 = _false;
__PRGrav_V3 = _false;
__PRGrav_V4 = _false;
__PRGrav_V5 = _false;
__PRGrav_V6 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __PRGrav_R[4] = {_true,_false,_false,_false};

/* AUTOMATON ENGINE */

int PRGrav () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = (__PRGrav_R[2]&&!(__PRGrav_R[0]));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__PRGrav_A5));
E[2] = (__PRGrav_R[2]||__PRGrav_R[3]);
E[3] = (__PRGrav_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__PRGrav_A3)));
if (E[3]) {
__PRGrav_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRGrav_A22\n");
#endif
}
E[4] = (__PRGrav_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRGrav_A1));
E[5] = (__PRGrav_R[1]&&!(__PRGrav_R[0]));
E[6] = (E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRGrav_A1));
E[6] = (E[4]||E[6]);
if (E[6]) {
__PRGrav_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRGrav_A20\n");
#endif
}
if (E[6]) {
__PRGrav_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRGrav_A21\n");
#endif
}
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__PRGrav_A5)));
E[4] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__PRGrav_A6)));
E[4] = (E[6]||((__PRGrav_R[2]&&E[4])));
E[7] = (((E[2]&&!(__PRGrav_R[2])))||E[4]);
E[8] = (E[7]||E[1]);
E[9] = (__PRGrav_R[3]&&!(__PRGrav_R[0]));
E[10] = (E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__PRGrav_A2)));
E[10] = (E[6]||((__PRGrav_R[3]&&E[10])));
E[11] = (((E[2]&&!(__PRGrav_R[3])))||E[10]);
E[1] = ((E[1]&&E[8])&&E[11]);
if (E[1]) {
__PRGrav_A7;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRGrav_A7\n");
#endif
}
if (E[6]) {
__PRGrav_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRGrav_A8\n");
#endif
}
if (E[1]) {
__PRGrav_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRGrav_A9\n");
#endif
}
E[9] = (E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__PRGrav_A2));
if (E[9]) {
__PRGrav_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRGrav_A10\n");
#endif
}
E[0] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__PRGrav_A6));
E[8] = (E[8]||E[0]);
E[0] = ((E[0]&&E[8])&&E[11]);
if (E[0]) {
__PRGrav_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRGrav_A11\n");
#endif
}
if (E[6]) {
__PRGrav_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRGrav_A12\n");
#endif
}
if (E[9]) {
__PRGrav_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRGrav_A13\n");
#endif
}
E[8] = ((E[9]&&E[8])&&((E[11]||E[9])));
E[12] = ((((E[1]||E[0]))||E[8]));
E[13] = (__PRGrav_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRGrav_A1)));
E[5] = (E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRGrav_A1)));
E[5] = (E[13]||((__PRGrav_R[1]&&E[5])));
E[11] = (((((((E[4]||E[10]))&&E[7])&&E[11]))||E[5]));
E[2] = (E[2]||__PRGrav_R[1]);
E[8] = (((((E[8]||E[0]))||E[1])||E[0])||E[8]);
__PRGrav_R[2] = (E[4]&&!(E[8]));
__PRGrav_R[3] = (E[10]&&!(E[8]));
__PRGrav_R[0] = !(_true);
__PRGrav_R[1] = E[5];
__PRGrav__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int PRGrav_reset () {
__PRGrav_R[0] = _true;
__PRGrav_R[1] = _false;
__PRGrav_R[2] = _false;
__PRGrav_R[3] = _false;
__PRGrav__reset_input();
return 0;
}
