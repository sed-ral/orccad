/* sscc : C CODE OF SORTED EQUATIONS PRMove - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __PRMove_GENERIC_TEST(TEST) return TEST;
typedef void (*__PRMove_APF)();
static __PRMove_APF *__PRMove_PActionArray;

#include "PRMove.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _TrMove_controler_DEFINED
#ifndef TrMove_controler
extern void TrMove_controler(orcStrlPtr);
#endif
#endif
#ifndef _TrMove_fileparameter_DEFINED
#ifndef TrMove_fileparameter
extern void TrMove_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _TrMove_parameter_DEFINED
#ifndef TrMove_parameter
extern void TrMove_parameter(orcStrlPtr ,string ,string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __PRMove_V0;
static boolean __PRMove_V1;
static orcStrlPtr __PRMove_V2;
static boolean __PRMove_V3;
static boolean __PRMove_V4;
static boolean __PRMove_V5;
static boolean __PRMove_V6;


/* INPUT FUNCTIONS */

void PRMove_I_START_PRMove () {
__PRMove_V0 = _true;
}
void PRMove_I_Abort_Local_PRMove () {
__PRMove_V1 = _true;
}
void PRMove_I_TrMove_Start (orcStrlPtr __V) {
_orcStrlPtr(&__PRMove_V2,__V);
__PRMove_V3 = _true;
}
void PRMove_I_USERSTART_TrMove () {
__PRMove_V4 = _true;
}
void PRMove_I_BF_TrMove () {
__PRMove_V5 = _true;
}
void PRMove_I_T3_TrMove () {
__PRMove_V6 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __PRMove_A1 \
__PRMove_V0
#define __PRMove_A2 \
__PRMove_V1
#define __PRMove_A3 \
__PRMove_V3
#define __PRMove_A4 \
__PRMove_V4
#define __PRMove_A5 \
__PRMove_V5
#define __PRMove_A6 \
__PRMove_V6

/* OUTPUT ACTIONS */

#define __PRMove_A7 \
PRMove_O_BF_PRMove()
#define __PRMove_A8 \
PRMove_O_STARTED_PRMove()
#define __PRMove_A9 \
PRMove_O_GoodEnd_PRMove()
#define __PRMove_A10 \
PRMove_O_Abort_PRMove()
#define __PRMove_A11 \
PRMove_O_T3_PRMove()
#define __PRMove_A12 \
PRMove_O_START_TrMove()
#define __PRMove_A13 \
PRMove_O_Abort_Local_TrMove()

/* ASSIGNMENTS */

#define __PRMove_A14 \
__PRMove_V0 = _false
#define __PRMove_A15 \
__PRMove_V1 = _false
#define __PRMove_A16 \
__PRMove_V3 = _false
#define __PRMove_A17 \
__PRMove_V4 = _false
#define __PRMove_A18 \
__PRMove_V5 = _false
#define __PRMove_A19 \
__PRMove_V6 = _false

/* PROCEDURE CALLS */

#define __PRMove_A20 \
TrMove_parameter(__PRMove_V2,"0.1 0.3 ","1.0")
#define __PRMove_A21 \
TrMove_controler(__PRMove_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __PRMove_A22 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int PRMove_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __PRMove__reset_input () {
__PRMove_V0 = _false;
__PRMove_V1 = _false;
__PRMove_V3 = _false;
__PRMove_V4 = _false;
__PRMove_V5 = _false;
__PRMove_V6 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __PRMove_R[4] = {_true,_false,_false,_false};

/* AUTOMATON ENGINE */

int PRMove () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = (__PRMove_R[2]&&!(__PRMove_R[0]));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__PRMove_A5));
E[2] = (__PRMove_R[2]||__PRMove_R[3]);
E[3] = (__PRMove_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__PRMove_A3)));
if (E[3]) {
__PRMove_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRMove_A22\n");
#endif
}
E[4] = (__PRMove_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRMove_A1));
E[5] = (__PRMove_R[1]&&!(__PRMove_R[0]));
E[6] = (E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRMove_A1));
E[6] = (E[4]||E[6]);
if (E[6]) {
__PRMove_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRMove_A20\n");
#endif
}
if (E[6]) {
__PRMove_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRMove_A21\n");
#endif
}
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__PRMove_A5)));
E[4] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__PRMove_A6)));
E[4] = (E[6]||((__PRMove_R[2]&&E[4])));
E[7] = (((E[2]&&!(__PRMove_R[2])))||E[4]);
E[8] = (E[7]||E[1]);
E[9] = (__PRMove_R[3]&&!(__PRMove_R[0]));
E[10] = (E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__PRMove_A2)));
E[10] = (E[6]||((__PRMove_R[3]&&E[10])));
E[11] = (((E[2]&&!(__PRMove_R[3])))||E[10]);
E[1] = ((E[1]&&E[8])&&E[11]);
if (E[1]) {
__PRMove_A7;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRMove_A7\n");
#endif
}
if (E[6]) {
__PRMove_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRMove_A8\n");
#endif
}
if (E[1]) {
__PRMove_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRMove_A9\n");
#endif
}
E[9] = (E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__PRMove_A2));
if (E[9]) {
__PRMove_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRMove_A10\n");
#endif
}
E[0] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__PRMove_A6));
E[8] = (E[8]||E[0]);
E[0] = ((E[0]&&E[8])&&E[11]);
if (E[0]) {
__PRMove_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRMove_A11\n");
#endif
}
if (E[6]) {
__PRMove_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRMove_A12\n");
#endif
}
if (E[9]) {
__PRMove_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRMove_A13\n");
#endif
}
E[8] = ((E[9]&&E[8])&&((E[11]||E[9])));
E[12] = ((((E[1]||E[0]))||E[8]));
E[13] = (__PRMove_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRMove_A1)));
E[5] = (E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRMove_A1)));
E[5] = (E[13]||((__PRMove_R[1]&&E[5])));
E[11] = (((((((E[4]||E[10]))&&E[7])&&E[11]))||E[5]));
E[2] = (E[2]||__PRMove_R[1]);
E[8] = (((((E[8]||E[0]))||E[1])||E[0])||E[8]);
__PRMove_R[2] = (E[4]&&!(E[8]));
__PRMove_R[3] = (E[10]&&!(E[8]));
__PRMove_R[0] = !(_true);
__PRMove_R[1] = E[5];
__PRMove__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int PRMove_reset () {
__PRMove_R[0] = _true;
__PRMove_R[1] = _false;
__PRMove_R[2] = _false;
__PRMove_R[3] = _false;
__PRMove__reset_input();
return 0;
}
