/*--------------------------------------
 * definitions for simulation purposes 
 *--------------------------------------
 */
void TrGrav_parameter() {}
void TrGrav_fileparameter() {}
void TrGrav_controler() {}
void TrMove_parameter() {}
void TrMove_fileparameter() {}
void TrMove_controler() {}
void TrCompGrav_parameter() {}
void TrCompGrav_fileparameter() {}
void TrCompGrav_controler() {}
typedef void* orcStrlPtr;
#define _orcStrlPtr(x,y)(*(x)=y)
#define _check_orcStrlPtr _check_integer
#define _text_to_orcStrlPtr  _text_to_integer
#define _orcStrlPtr_to_text _integer_to_text
#define _NO_PROCEDURE_DEFINITIONS
