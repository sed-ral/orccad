/* sscc : C CODE OF SORTED EQUATIONS PRCompGrav - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __PRCompGrav_GENERIC_TEST(TEST) return TEST;
typedef void (*__PRCompGrav_APF)();
static __PRCompGrav_APF *__PRCompGrav_PActionArray;

#include "PRCompGrav.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _TrCompGrav_controler_DEFINED
#ifndef TrCompGrav_controler
extern void TrCompGrav_controler(orcStrlPtr);
#endif
#endif
#ifndef _TrCompGrav_fileparameter_DEFINED
#ifndef TrCompGrav_fileparameter
extern void TrCompGrav_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _TrCompGrav_parameter_DEFINED
#ifndef TrCompGrav_parameter
extern void TrCompGrav_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __PRCompGrav_V0;
static boolean __PRCompGrav_V1;
static orcStrlPtr __PRCompGrav_V2;
static boolean __PRCompGrav_V3;
static boolean __PRCompGrav_V4;
static boolean __PRCompGrav_V5;
static boolean __PRCompGrav_V6;


/* INPUT FUNCTIONS */

void PRCompGrav_I_START_PRCompGrav () {
__PRCompGrav_V0 = _true;
}
void PRCompGrav_I_Abort_Local_PRCompGrav () {
__PRCompGrav_V1 = _true;
}
void PRCompGrav_I_TrCompGrav_Start (orcStrlPtr __V) {
_orcStrlPtr(&__PRCompGrav_V2,__V);
__PRCompGrav_V3 = _true;
}
void PRCompGrav_I_USERSTART_TrCompGrav () {
__PRCompGrav_V4 = _true;
}
void PRCompGrav_I_BF_TrCompGrav () {
__PRCompGrav_V5 = _true;
}
void PRCompGrav_I_T3_TrCompGrav () {
__PRCompGrav_V6 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __PRCompGrav_A1 \
__PRCompGrav_V0
#define __PRCompGrav_A2 \
__PRCompGrav_V1
#define __PRCompGrav_A3 \
__PRCompGrav_V3
#define __PRCompGrav_A4 \
__PRCompGrav_V4
#define __PRCompGrav_A5 \
__PRCompGrav_V5
#define __PRCompGrav_A6 \
__PRCompGrav_V6

/* OUTPUT ACTIONS */

#define __PRCompGrav_A7 \
PRCompGrav_O_BF_PRCompGrav()
#define __PRCompGrav_A8 \
PRCompGrav_O_STARTED_PRCompGrav()
#define __PRCompGrav_A9 \
PRCompGrav_O_GoodEnd_PRCompGrav()
#define __PRCompGrav_A10 \
PRCompGrav_O_Abort_PRCompGrav()
#define __PRCompGrav_A11 \
PRCompGrav_O_T3_PRCompGrav()
#define __PRCompGrav_A12 \
PRCompGrav_O_START_TrCompGrav()
#define __PRCompGrav_A13 \
PRCompGrav_O_Abort_Local_TrCompGrav()

/* ASSIGNMENTS */

#define __PRCompGrav_A14 \
__PRCompGrav_V0 = _false
#define __PRCompGrav_A15 \
__PRCompGrav_V1 = _false
#define __PRCompGrav_A16 \
__PRCompGrav_V3 = _false
#define __PRCompGrav_A17 \
__PRCompGrav_V4 = _false
#define __PRCompGrav_A18 \
__PRCompGrav_V5 = _false
#define __PRCompGrav_A19 \
__PRCompGrav_V6 = _false

/* PROCEDURE CALLS */

#define __PRCompGrav_A20 \
TrCompGrav_parameter(__PRCompGrav_V2)
#define __PRCompGrav_A21 \
TrCompGrav_controler(__PRCompGrav_V2)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __PRCompGrav_A22 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int PRCompGrav_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __PRCompGrav__reset_input () {
__PRCompGrav_V0 = _false;
__PRCompGrav_V1 = _false;
__PRCompGrav_V3 = _false;
__PRCompGrav_V4 = _false;
__PRCompGrav_V5 = _false;
__PRCompGrav_V6 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __PRCompGrav_R[4] = {_true,_false,_false,_false};

/* AUTOMATON ENGINE */

int PRCompGrav () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[14];
E[0] = (__PRCompGrav_R[2]&&!(__PRCompGrav_R[0]));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__PRCompGrav_A5));
E[2] = (__PRCompGrav_R[2]||__PRCompGrav_R[3]);
E[3] = (__PRCompGrav_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__PRCompGrav_A3)));
if (E[3]) {
__PRCompGrav_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRCompGrav_A22\n");
#endif
}
E[4] = (__PRCompGrav_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRCompGrav_A1));
E[5] = (__PRCompGrav_R[1]&&!(__PRCompGrav_R[0]));
E[6] = (E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRCompGrav_A1));
E[6] = (E[4]||E[6]);
if (E[6]) {
__PRCompGrav_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRCompGrav_A20\n");
#endif
}
if (E[6]) {
__PRCompGrav_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRCompGrav_A21\n");
#endif
}
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__PRCompGrav_A5)));
E[4] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__PRCompGrav_A6)));
E[4] = (E[6]||((__PRCompGrav_R[2]&&E[4])));
E[7] = (((E[2]&&!(__PRCompGrav_R[2])))||E[4]);
E[8] = (E[7]||E[1]);
E[9] = (__PRCompGrav_R[3]&&!(__PRCompGrav_R[0]));
E[10] = (E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__PRCompGrav_A2)));
E[10] = (E[6]||((__PRCompGrav_R[3]&&E[10])));
E[11] = (((E[2]&&!(__PRCompGrav_R[3])))||E[10]);
E[1] = ((E[1]&&E[8])&&E[11]);
if (E[1]) {
__PRCompGrav_A7;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRCompGrav_A7\n");
#endif
}
if (E[6]) {
__PRCompGrav_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRCompGrav_A8\n");
#endif
}
if (E[1]) {
__PRCompGrav_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRCompGrav_A9\n");
#endif
}
E[9] = (E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__PRCompGrav_A2));
if (E[9]) {
__PRCompGrav_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRCompGrav_A10\n");
#endif
}
E[0] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__PRCompGrav_A6));
E[8] = (E[8]||E[0]);
E[0] = ((E[0]&&E[8])&&E[11]);
if (E[0]) {
__PRCompGrav_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRCompGrav_A11\n");
#endif
}
if (E[6]) {
__PRCompGrav_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRCompGrav_A12\n");
#endif
}
if (E[9]) {
__PRCompGrav_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__PRCompGrav_A13\n");
#endif
}
E[8] = ((E[9]&&E[8])&&((E[11]||E[9])));
E[12] = ((((E[1]||E[0]))||E[8]));
E[13] = (__PRCompGrav_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRCompGrav_A1)));
E[5] = (E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__PRCompGrav_A1)));
E[5] = (E[13]||((__PRCompGrav_R[1]&&E[5])));
E[11] = (((((((E[4]||E[10]))&&E[7])&&E[11]))||E[5]));
E[2] = (E[2]||__PRCompGrav_R[1]);
E[8] = (((((E[8]||E[0]))||E[1])||E[0])||E[8]);
__PRCompGrav_R[2] = (E[4]&&!(E[8]));
__PRCompGrav_R[3] = (E[10]&&!(E[8]));
__PRCompGrav_R[0] = !(_true);
__PRCompGrav_R[1] = E[5];
__PRCompGrav__reset_input();
return E[11];
}

/* AUTOMATON RESET */

int PRCompGrav_reset () {
__PRCompGrav_R[0] = _true;
__PRCompGrav_R[1] = _false;
__PRCompGrav_R[2] = _false;
__PRCompGrav_R[3] = _false;
__PRCompGrav__reset_input();
return 0;
}
