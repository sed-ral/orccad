/* sscc : C CODE OF SORTED EQUATIONS aa - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __aa_GENERIC_TEST(TEST) return TEST;
typedef void (*__aa_APF)();
static __aa_APF *__aa_PActionArray;

#include "aa.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _TrCompGrav_controler_DEFINED
#ifndef TrCompGrav_controler
extern void TrCompGrav_controler(orcStrlPtr);
#endif
#endif
#ifndef _TrCompGrav_fileparameter_DEFINED
#ifndef TrCompGrav_fileparameter
extern void TrCompGrav_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _TrCompGrav_parameter_DEFINED
#ifndef TrCompGrav_parameter
extern void TrCompGrav_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static orcStrlPtr __aa_V0;
static boolean __aa_V1;
static boolean __aa_V2;
static boolean __aa_V3;
static boolean __aa_V4;
static boolean __aa_V5;
static boolean __aa_V6;
static boolean __aa_V7;
static boolean __aa_V8;
static orcStrlPtr __aa_V9;
static boolean __aa_V10;
static boolean __aa_V11;
static boolean __aa_V12;
static boolean __aa_V13;
static boolean __aa_V14;
static boolean __aa_V15;
static orcStrlPtr __aa_V16;
static orcStrlPtr __aa_V17;
static orcStrlPtr __aa_V18;
static orcStrlPtr __aa_V19;
static orcStrlPtr __aa_V20;
static orcStrlPtr __aa_V21;
static orcStrlPtr __aa_V22;
static orcStrlPtr __aa_V23;
static boolean __aa_V24;


/* INPUT FUNCTIONS */

void aa_I_Prr_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V0,__V);
__aa_V1 = _true;
}
void aa_I_KillOK_WinX () {
__aa_V2 = _true;
}
void aa_I_USERSTART_TrCompGrav () {
__aa_V3 = _true;
}
void aa_I_ActivateOK_WinX () {
__aa_V4 = _true;
}
void aa_I_T2_TrCompGrav () {
__aa_V5 = _true;
}
void aa_I_ReadyToStop_WinX () {
__aa_V6 = _true;
}
void aa_I_InitOK_WinX () {
__aa_V7 = _true;
}
void aa_I_EndTRE () {
__aa_V8 = _true;
}
void aa_I_TrCompGrav_Start (orcStrlPtr __V) {
_orcStrlPtr(&__aa_V9,__V);
__aa_V10 = _true;
}
void aa_I_CmdStopOK_WinX () {
__aa_V11 = _true;
}
void aa_I_ROBOT_FAIL () {
__aa_V12 = _true;
}
void aa_I_SOFT_FAIL () {
__aa_V13 = _true;
}
void aa_I_SENSOR_FAIL () {
__aa_V14 = _true;
}
void aa_I_CPU_OVERLOAD () {
__aa_V15 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __aa_A1 \
__aa_V1
#define __aa_A2 \
__aa_V2
#define __aa_A3 \
__aa_V3
#define __aa_A4 \
__aa_V4
#define __aa_A5 \
__aa_V5
#define __aa_A6 \
__aa_V6
#define __aa_A7 \
__aa_V7
#define __aa_A8 \
__aa_V8
#define __aa_A9 \
__aa_V10
#define __aa_A10 \
__aa_V11
#define __aa_A11 \
__aa_V12
#define __aa_A12 \
__aa_V13
#define __aa_A13 \
__aa_V14
#define __aa_A14 \
__aa_V15

/* OUTPUT ACTIONS */

#define __aa_A15 \
aa_O_Activate(__aa_V16)
#define __aa_A16 \
aa_O_STARTED_PRCompGrav()
#define __aa_A17 \
aa_O_Abort_PRCompGrav()
#define __aa_A18 \
aa_O_GoodEnd_PRCompGrav()
#define __aa_A19 \
aa_O_ActivateTrCompGrav_WinX(__aa_V17)
#define __aa_A20 \
aa_O_TrCompGravTransite(__aa_V18)
#define __aa_A21 \
aa_O_Abort_TrCompGrav(__aa_V19)
#define __aa_A22 \
aa_O_STARTED_TrCompGrav()
#define __aa_A23 \
aa_O_GoodEnd_TrCompGrav()
#define __aa_A24 \
aa_O_EndKill(__aa_V20)
#define __aa_A25 \
aa_O_FinBF(__aa_V21)
#define __aa_A26 \
aa_O_FinT3(__aa_V22)
#define __aa_A27 \
aa_O_GoodEndRPr()
#define __aa_A28 \
aa_O_T3RPr()

/* ASSIGNMENTS */

#define __aa_A29 \
__aa_V1 = _false
#define __aa_A30 \
__aa_V2 = _false
#define __aa_A31 \
__aa_V3 = _false
#define __aa_A32 \
__aa_V4 = _false
#define __aa_A33 \
__aa_V5 = _false
#define __aa_A34 \
__aa_V6 = _false
#define __aa_A35 \
__aa_V7 = _false
#define __aa_A36 \
__aa_V8 = _false
#define __aa_A37 \
__aa_V10 = _false
#define __aa_A38 \
__aa_V11 = _false
#define __aa_A39 \
__aa_V12 = _false
#define __aa_A40 \
__aa_V13 = _false
#define __aa_A41 \
__aa_V14 = _false
#define __aa_A42 \
__aa_V15 = _false
#define __aa_A43 \
_orcStrlPtr(&__aa_V20,__aa_V23)
#define __aa_A44 \
_orcStrlPtr(&__aa_V21,__aa_V0)
#define __aa_A45 \
_orcStrlPtr(&__aa_V22,__aa_V0)
#define __aa_A46 \
_orcStrlPtr(&__aa_V19,__aa_V9)
#define __aa_A47 \
_orcStrlPtr(&__aa_V17,__aa_V9)
#define __aa_A48 \
_orcStrlPtr(&__aa_V23,__aa_V9)
#define __aa_A49 \
_orcStrlPtr(&__aa_V18,__aa_V9)
#define __aa_A50 \
_orcStrlPtr(&__aa_V18,__aa_V9)
#define __aa_A51 \
_orcStrlPtr(&__aa_V18,__aa_V9)
#define __aa_A52 \
_orcStrlPtr(&__aa_V18,__aa_V9)
#define __aa_A53 \
_orcStrlPtr(&__aa_V20,__aa_V23)

/* PROCEDURE CALLS */

#define __aa_A54 \
TrCompGrav_parameter(__aa_V9)
#define __aa_A55 \
TrCompGrav_controler(__aa_V9)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __aa_A56 \

#define __aa_A57 \

#define __aa_A58 \

#define __aa_A59 \

#define __aa_A60 \

#define __aa_A61 \

#define __aa_A62 \

#define __aa_A63 \

#define __aa_A64 \

#define __aa_A65 \

#define __aa_A66 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int aa_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __aa__reset_input () {
__aa_V1 = _false;
__aa_V2 = _false;
__aa_V3 = _false;
__aa_V4 = _false;
__aa_V5 = _false;
__aa_V6 = _false;
__aa_V7 = _false;
__aa_V8 = _false;
__aa_V10 = _false;
__aa_V11 = _false;
__aa_V12 = _false;
__aa_V13 = _false;
__aa_V14 = _false;
__aa_V15 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __aa_R[16] = {_true,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int aa () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[40];
if (!(_true)) {
__aa_A15;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A15\n");
#endif
}
E[0] = (__aa_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1)));
if (E[0]) {
__aa_A56;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A56\n");
#endif
}
E[1] = (__aa_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9)));
if (E[1]) {
__aa_A57;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A57\n");
#endif
}
if (__aa_R[0]) {
__aa_A58;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A58\n");
#endif
}
if (__aa_R[0]) {
__aa_A59;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A59\n");
#endif
}
if (__aa_R[0]) {
__aa_A60;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A60\n");
#endif
}
if (__aa_R[0]) {
__aa_A61;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A61\n");
#endif
}
if (__aa_R[0]) {
__aa_A62;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A62\n");
#endif
}
if (__aa_R[0]) {
__aa_A63;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A63\n");
#endif
}
if (__aa_R[0]) {
__aa_A64;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A64\n");
#endif
}
if (__aa_R[0]) {
__aa_A65;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A65\n");
#endif
}
if (__aa_R[0]) {
__aa_A66;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A66\n");
#endif
}
E[2] = (__aa_R[2]&&!(__aa_R[0]));
E[3] = (E[2]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9));
E[4] = (__aa_R[3]&&!(__aa_R[0]));
E[5] = (E[4]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1));
E[6] = (__aa_R[2]||__aa_R[3]);
E[7] = (((E[6]&&!(__aa_R[2])))||E[3]);
E[8] = (((E[6]&&!(__aa_R[3])))||E[5]);
E[5] = ((((E[3]||E[5]))&&E[7])&&E[8]);
E[3] = (__aa_R[9]&&!(__aa_R[0]));
E[9] = (((__aa_R[0]&&E[5]))||((E[3]&&E[5])));
if (E[9]) {
__aa_A16;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A16\n");
#endif
}
if (!(_true)) {
__aa_A17;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A17\n");
#endif
}
E[10] = (__aa_R[10]&&!(__aa_R[0]));
E[11] = (__aa_R[8]&&!(__aa_R[0]));
E[12] = (E[11]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8));
E[13] = (__aa_R[7]||__aa_R[8]);
E[14] = (__aa_R[5]&&((__aa_R[5]&&!(__aa_R[0]))));
E[15] = (E[14]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7));
E[16] = (__aa_R[12]&&!(__aa_R[0]));
E[17] = (E[16]&&E[15]);
E[18] = (__aa_R[15]&&!(__aa_R[0]));
E[19] = (E[18]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10));
E[20] = (E[17]||E[19]);
E[21] = (__aa_R[6]&&((__aa_R[6]&&!(__aa_R[0]))));
E[22] = (((E[15]&&E[20]))||((E[21]&&E[20])));
if (E[22]) {
__aa_A47;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A47\n");
#endif
}
if (E[22]) {
__aa_A48;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A48\n");
#endif
}
E[23] = (E[22]||((__aa_R[7]&&((__aa_R[7]&&!(__aa_R[0]))))));
E[24] = (((E[13]&&!(__aa_R[7])))||E[23]);
E[11] = (E[11]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__aa_A8)));
E[11] = (E[22]||((__aa_R[8]&&E[11])));
E[25] = (((E[13]&&!(__aa_R[8])))||E[11]);
E[12] = ((E[12]&&E[24])&&((E[25]||E[12])));
if (E[12]) {
__aa_A51;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A51\n");
#endif
}
E[26] = (E[10]&&E[12]);
E[27] = (__aa_R[10]||__aa_R[11]);
if (E[9]) {
__aa_A54;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A54\n");
#endif
}
if (E[9]) {
__aa_A55;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A55\n");
#endif
}
E[10] = (E[9]||((__aa_R[10]&&((E[10]&&!(E[12]))))));
E[28] = (((E[27]&&!(__aa_R[10])))||E[10]);
E[29] = (E[9]||((__aa_R[11]&&((__aa_R[11]&&!(__aa_R[0]))))));
E[30] = (((E[27]&&!(__aa_R[11])))||E[29]);
E[26] = ((E[26]&&((E[28]||E[26])))&&E[30]);
if (E[26]) {
__aa_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A18\n");
#endif
}
if (E[22]) {
__aa_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A19\n");
#endif
}
if (!(_true)) {
__aa_A50;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A50\n");
#endif
}
if (!(_true)) {
__aa_A49;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A49\n");
#endif
}
if (E[12]) {
__aa_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A20\n");
#endif
}
if (!(_true)) {
__aa_A46;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A46\n");
#endif
}
if (!(_true)) {
__aa_A21;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A21\n");
#endif
}
E[31] = (E[12]||__aa_R[0]);
E[32] = (__aa_R[4]&&!(__aa_R[0]));
E[33] = (((E[31]&&E[9]))||((E[32]&&E[9])));
if (E[33]) {
__aa_A22;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A22\n");
#endif
}
if (E[12]) {
__aa_A23;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A23\n");
#endif
}
E[34] = (__aa_R[14]&&!(__aa_R[0]));
E[35] = (E[34]&&E[15]);
if (E[35]) {
__aa_A53;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A53\n");
#endif
}
E[36] = (__aa_R[1]&&!(__aa_R[0]));
E[37] = (E[36]&&E[26]);
E[13] = ((((__aa_R[5]||E[13])||__aa_R[6]))||__aa_R[4]);
E[27] = (E[27]||__aa_R[9]);
E[38] = (((__aa_R[12]||__aa_R[13])||__aa_R[14])||__aa_R[15]);
E[39] = ((((E[6]||E[13])||E[27])||E[38])||__aa_R[1]);
E[2] = (E[2]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__aa_A9)));
E[2] = (__aa_R[0]||((__aa_R[2]&&E[2])));
E[4] = (E[4]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__aa_A1)));
E[4] = (__aa_R[0]||((__aa_R[3]&&E[4])));
E[8] = ((((E[2]||E[4]))&&((E[7]||E[2])))&&((E[8]||E[4])));
E[6] = (((((E[39]&&!(E[6])))||E[5]))||E[8]);
E[14] = (E[14]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__aa_A7)));
E[14] = (__aa_R[5]&&E[14]);
E[20] = (((E[15]&&!(E[20])))||((__aa_R[6]&&((E[21]&&!(E[20]))))));
E[32] = (((E[31]&&!(E[9])))||((__aa_R[4]&&((E[32]&&!(E[9]))))));
E[25] = ((E[33]||(((E[14]||(((((E[23]||E[11]))&&E[24])&&E[25])))||E[20])))||E[32]);
E[13] = (((E[39]&&!(E[13])))||E[25]);
E[5] = (((__aa_R[0]&&!(E[5])))||((__aa_R[9]&&((E[3]&&!(E[5]))))));
E[30] = ((((((E[10]||E[29]))&&E[28])&&E[30]))||E[5]);
E[27] = (((((E[39]&&!(E[27])))||E[26]))||E[30]);
E[16] = (__aa_R[0]||((__aa_R[12]&&((E[16]&&!(E[15]))))));
E[28] = (__aa_R[13]&&!(__aa_R[0]));
E[19] = ((E[17]||E[19])||((__aa_R[13]&&((E[28]&&!(E[12]))))));
E[15] = (((E[28]&&E[12]))||((__aa_R[14]&&((E[34]&&!(E[15]))))));
E[18] = (E[18]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__aa_A10)));
E[18] = (E[35]||((__aa_R[15]&&E[18])));
E[38] = ((((((E[39]&&!(E[38])))||E[16])||E[19])||E[15])||E[18]);
E[36] = (E[36]&&!(E[26]));
E[34] = (E[36]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11)));
E[28] = (E[34]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12)));
E[17] = (E[28]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13)));
E[3] = (E[17]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14)));
E[3] = (__aa_R[0]||((__aa_R[1]&&E[3])));
E[39] = (((E[39]&&!(__aa_R[1])))||E[3]);
E[24] = (E[39]||E[37]);
E[37] = (((((E[37]&&E[6])&&E[13])&&E[27])&&E[38])&&E[24]);
if (E[37]) {
__aa_A43;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A43\n");
#endif
}
if (((E[35]||E[37]))) {
__aa_A24;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A24\n");
#endif
}
if (E[37]) {
__aa_A44;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A44\n");
#endif
}
if (E[37]) {
__aa_A25;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A25\n");
#endif
}
E[36] = (E[36]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__aa_A11));
E[34] = (E[34]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__aa_A12));
E[28] = (E[28]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__aa_A13));
E[17] = (E[17]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__aa_A14));
E[24] = (((((((((E[36]||E[34])||E[28])||E[17]))&&E[6])&&E[13])&&E[27])&&E[38])&&(((((E[24]||E[36])||E[34])||E[28])||E[17])));
if (E[24]) {
__aa_A45;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A45\n");
#endif
}
if (E[24]) {
__aa_A26;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A26\n");
#endif
}
if (E[37]) {
__aa_A27;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A27\n");
#endif
}
if (E[24]) {
__aa_A28;
#ifdef TRACE_ACTION
fprintf(stderr, "__aa_A28\n");
#endif
}
E[17] = (((E[36]||E[34])||E[28])||E[17]);
E[28] = ((E[37]||E[24]));
E[39] = ((((((((((((((E[8]||E[25])||E[30])||E[16])||E[19])||E[15])||E[18])||E[3]))&&E[6])&&E[13])&&E[27])&&E[38])&&E[39]));
E[38] = (E[24]||E[37]);
__aa_R[1] = (E[3]&&!(E[38]));
__aa_R[2] = (E[2]&&!(E[38]));
__aa_R[3] = (E[4]&&!(E[38]));
__aa_R[4] = (E[32]&&!(E[38]));
__aa_R[5] = (((E[33]&&!(E[38])))||((E[14]&&!(E[38]))));
__aa_R[6] = (E[20]&&!(E[38]));
E[20] = (E[38]||E[12]);
__aa_R[7] = (E[23]&&!(E[20]));
__aa_R[8] = (E[11]&&!(E[20]));
__aa_R[9] = (E[5]&&!(E[38]));
E[5] = (E[38]||E[26]);
__aa_R[10] = (E[10]&&!(E[5]));
__aa_R[11] = (E[29]&&!(E[5]));
__aa_R[12] = (E[16]&&!(E[38]));
__aa_R[13] = (E[19]&&!(E[38]));
__aa_R[14] = (E[15]&&!(E[38]));
__aa_R[15] = (E[18]&&!(E[38]));
__aa_R[0] = !(_true);
__aa__reset_input();
return E[39];
}

/* AUTOMATON RESET */

int aa_reset () {
__aa_R[0] = _true;
__aa_R[1] = _false;
__aa_R[2] = _false;
__aa_R[3] = _false;
__aa_R[4] = _false;
__aa_R[5] = _false;
__aa_R[6] = _false;
__aa_R[7] = _false;
__aa_R[8] = _false;
__aa_R[9] = _false;
__aa_R[10] = _false;
__aa_R[11] = _false;
__aa_R[12] = _false;
__aa_R[13] = _false;
__aa_R[14] = _false;
__aa_R[15] = _false;
__aa__reset_input();
return 0;
}
