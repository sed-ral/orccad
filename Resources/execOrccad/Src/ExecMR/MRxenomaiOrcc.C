//*******************************  Orccad *************************************
//
// File           : linuxNPTLOrcc.C
// Author         : Soraya Arias
// Version        :
// Creation       : 25 June 2004
//
//***************************** Description ***********************************
//
//   This file provides an interface between all Orccad system call
//   and Linux 2.6 Kernel (including Native Posix Thread Library)
//
//**************************** Modifications **********************************
//
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************

#include "Exec/MRxenomaiOrcc.h"
#include "Exec/clockgen.h"
#include <Exec/mt.h>

ORCTIMER_ID AsyncTimer[MAXTIMERS];
struct timespec clock_resolution;
int toto;
struct timespec orctime, oldtime, inittime;
int crexec, cr;
RTIME exectime;
ORCTHR_ID MAIN_RT;
RT_TASK_INFO verifinfo, delinfo, execinfo;
RT_SEM_INFO seminfo;
int message_counter = 0, end_alarm_clock = 0, ji = 0;
long long startime, now;
long long hjitter, jinc, max, moy, drift, jitter;
int cpt_sem = 0, cpt_mutex = 0;
char nameSem[10], nameMutex[10], nameAlarm[10];
int Timer_exists = 0;
int NbHClocks;  /**< Number of async timers */

int orcTimerInit(int i, ModuleTask * mt)
{
    int err;
    //fill the AsyncTimer datafields
    AsyncTimer[i] = new orcTimer_t;
    AsyncTimer[i]->period = mt->GetSampleTime();
    AsyncTimer[i]->ns_period = (RTIME)(NSEC_PER_SEC * AsyncTimer[i]->period);
    AsyncTimer[i]->mt = mt;
    orcTimerSetTime(i,AsyncTimer[i]->period);
    sprintf(nameAlarm, "Alarm_%d", i);
    err = rt_alarm_create(&(AsyncTimer[i]->AlarmClk), nameAlarm);
    if (err != 0)printf("rt_alarm_create() error %d \n", err);
#ifdef DEBUG
    else printf("rt_alarm_create() %s OK \n", nameAlarm);
#endif

    return OK;
}

int orcTimerDelete(void)
{
  int err,i;
    extern int Timer_exists;
    RT_ALARM_INFO alarminfo;
    //#ifdef DEBUG
    printf("orcTimerDelete \n");
    //#endif
    end_alarm_clock = 1;
    for(i=0; i < NbHClocks;i++) {    
      rt_alarm_start(&(AsyncTimer[i]->AlarmClk),0,ORCFOREVER);//????
    #ifdef DEBUG
    printf("orcTimerDelete %d waits for Period_Loop \n", i);
    #endif
    orcTaskJoin(AsyncTimer[i]->AlarmHandler);
    #ifdef DEBUG
    printf("orcTimerDelete %d Period_Loop joined \n", i);
    #endif
    #ifdef DEBUG
    printf("deleting AlarmTimer %d \n",i);
    #endif
    err = rt_alarm_inquire(&(AsyncTimer[i]->AlarmClk),&alarminfo);
    if (err !=0) printf("alarm not valid %d \n", err);
    else
    {
      if ((err = rt_alarm_delete(&(AsyncTimer[i]->AlarmClk))) != 0)
	  printf("ERROR deleting alarm %d error %d\n", i, err);
    
    //#ifdef DEBUG
    else printf("deleted alarmclock [%d]  \n",i);
    //#endif
    }
    delete AsyncTimer[i];
    }
    Timer_exists = 0;
    return OK;
}

int orcTimerArm()
{
  int i, err;
      for(i = 0; i < NbHClocks;i++) {
	sprintf(nameAlarm,"AlarmHandler_%d",i);
      err = orcSpawn(&(AsyncTimer[i]->AlarmHandler), nameAlarm, PRI_CLK, NORMAL_STACK, (SOSO) orcAlarm_handler, (void *)(AsyncTimer[i]->mt) );
      if (err != OK) printf("error in asynctimer[%d] thread spawn \n", i); 
      }
    return OK;
}

int orcTimerLaunchAll(void)
{
    int err = -1;
      for(int i = 0; i < NbHClocks;i++) {
	err = rt_alarm_start(&(AsyncTimer[i]->AlarmClk),
			 rt_timer_ns2ticks(AsyncTimer[i]->ns_delay),
			 rt_timer_ns2ticks(AsyncTimer[i]->ns_period));
      if (err != 0) printf("rt_alarm_start() %d error %d \n", i, err);
      else printf("alarm %d started with period %llu ns \n", i, AsyncTimer[i]->ns_period);
       }
    Timer_exists = 1;
    return OK;
}


/**
 * Timer handler function
 *
 * @param int
 * @return
 */
void orcAlarm_handler(ModuleTask * mt)
{
  //    void *arg;
  int err,index;
  ORCTIMER_ID asynctimer;

  printf("starting orcAlarm_handler %d \n", mt->getIndexTimer());
    asynctimer = mt->getClkGen();
    while (!end_alarm_clock)
    {
#ifdef DEBUG
        printf("rt_alarm_wait WAITING \n");
#endif
        err = rt_alarm_wait(&(asynctimer->AlarmClk));
        if (err != 0) printf("rt_alarm_wait error %d err %d\n", mt->getIndexTimer(), err);
#ifdef DEBUG
        else printf("rt_alarm_wait fired \n");
#endif
		mt->MTsynchronize();
    }
    //#ifdef DEBUG
    printf("Timer loop %d finishing... \n", mt->getIndexTimer());
    //#endif
 
    return ;
}
