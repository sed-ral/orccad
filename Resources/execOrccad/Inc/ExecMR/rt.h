//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/rt.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 beta
// Creation       : 18 May 1994
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtine of
//           a robot action (a Robot Task)
//
// Class Definition:
//    RobotTask: controls an elementary robot action
//    ListRobotTask: manages a  RobotTask list
//
// Errors: 200-300
//
//**************************** Modifications **********************************
//
// 6 November 1996
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#ifndef __rt_h
#define __rt_h

#include "utils.h"
#include "global.h"
#include "mt.h"
#include "canal.h"
#include "param.h"
#include "phr.h"

class ListRobotTask;

class RobotTask: public Global
{
    // real-time horloge interrupt function
    // which control MTs (active) periodicity
    friend void clocks(ProcedureRobot *);
    // task real-time control of tms and rt state evolution
    friend void fct_control(RobotTask *);

    friend class ProcedureRobot;
    friend class ClockGen;
    friend class ListRobotTask;
    friend class ModuleTask;

protected:
    char *Name;
    int Status;
    int Order;
    RobotTask * Next;        // double pointer list for rt
    RobotTask * Previous;
    ListModuleTask *List_MT; // list of mts
    int CurState;            // state of rt : RT_STATE
    int PrecTimeOut;         // time of waiting control semaphore
    int ComputeTimeOut;      // time of end transition
    double Period;           // period of rt
     Parameters *Param;       // parameters of the RT KK
    int Parametrized;       // Param indicator (0 or 1)
    PhR *PhysRes;            // associated robot
    ORCTHR_ID CtrlId;        // RT controler real-time task
    // inentifier
    int CtrlSpawned;
public:
    ProcedureRobot *PrR;
    // Constructors,Destructors
    RobotTask(char *name, ProcedureRobot *Prr, PhR *, int);
    virtual ~RobotTask();
    //Parameters *Param;       // parameters of the RT KK
    // Delete real-time tasks
    void DeleteTasks();
    //  Insert mt mt_name in rt. f_pt is the real-time
    // funtion of the mt and it_filter is the number of period
    // at where results are not valid
    int insert(ModuleTask *);
    // gives semaphore of synchro at all its mts
    void synchronize();
    // gives semaphore of synchro at all its mts without clock test
    void synchronize_all();
    // launch fct_control
    int controler();
    // in automaton function output gives semaphore to indicates
    // that this signal output is raised
    void OutAuto(int i);
    double GetGlobalTime();
    // Physical Ressource
    PhR *GetPhR()
    {
        return PhysRes;
    }
    ProcedureRobot *GetPrr()
    {
        return PrR;
    }
    Parameters *GetPtrState()
    {
        return (GlobalState);
    };
    Parameters *GetPtrParameters()
    {
        return (Param);
    }
    ; // KK
    // Put/Get Attributes
    void PutState(int st)
    {
        CurState = st;
    };
    int GetState()
    {
        return CurState;
    };
    int GetStatus()
    {
        return Status;
    };
    char *GetName()
    {
        return Name;
    };
    virtual int SetParam()
    {
        return 1;
    };
    virtual int SetT1Exceptions()
    {
        return 1;
    };
    void PutOrder(int order)
    {
        Order = order;
    };
    int GetOrder()
    {
        return Order;
    };
    int First()
    {
        if (Order == RT_FIRST) return TRUE;
        else return FALSE;
    }
    int Last()
    {
        if (Order == RT_LAST) return TRUE;
        else return FALSE;
    }
    // in clock tick !!!
    int getPrecTimeOut()
    {
        return PrecTimeOut;
    };
    void putPrecTimeOut(int prec)
    {
        PrecTimeOut = prec;
    };
    ListModuleTask* GetRTList_MT()
    {
        return (List_MT);
    };
};

//
// Class to manage rts list
//
class ListRobotTask
{
    // real-time horloge interrupt function
    friend void Hl(ProcedureRobot *);
    friend void clocks(ProcedureRobot* );
    friend class ProcedureRobot;
    friend class ClockGen;

protected:
    RobotTask *List;  // Robot-task list
    int Number;       // Number of rt
public:
    // Destructor,Constructor
    ListRobotTask()
    {
        List = (RobotTask *)NULL; Number = 0;
    }
    ~ListRobotTask();
    // Delete real-time tasks
    void DeleteTasks();
    int insert(RobotTask *);
    int GetNumber()
    {
        return (Number);
    };
};

#endif

