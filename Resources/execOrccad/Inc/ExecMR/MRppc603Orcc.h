// *******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/linuxNPTLOrcc.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
// ***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime for Linux 2.6
//            abstract type, define
//
// **************************** Modifications **********************************
//
// Author         : Soraya Arias
// Date           : 24 June 2004
//
// This file provides an interface between all Orccad system call and
// Linux 2.6 Kernel (integrating the native posix thread library and
// message queues)
//
// BEWARE: timer resolution grain is 1ms in this version !
//
// *****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
// *****************************************************************************
#ifndef __linuxMRppc603orcc_h
#define __linuxMRppc603orcc_h

/////////////////////////
/// Linux include
/////////////////////////
#include <values.h>
#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
#include <signal.h>
#include <sys/time.h>
#include <semaphore.h>
#include <pthread.h>
#include <sched.h>
//#include <posix_time.h>
#include <sys/mman.h>


/////////////////////////
/// Define
/////////////////////////
typedef void (*FUNCPTR) (...); /**< General purpose pointer to function */
typedef void * (*SOSO) (void *); /**< Special type to be used for casting function in pthread_create() */


#define OK 0
#define ERROR -1
#define STATUS int

#define FALSE 0
#define TRUE 1

/**
 * Max number of characters for file name
 * This number is based on _POSIX_PATH_MAX value (see posix1_lim.h and see limits.h the
 * ifdef __USE_POSIX statement)
 *
 * @see <posix1_lim.h>#_POSIX_PATH_MAX
 * @see <limits.h>
 */
#define MAXFILENAME _POSIX_PATH_MAX +1


/*-------Message Queues----------*/
#ifdef _ORC_MQ_POSIX
#include <mqueue.h>
#define ORCMSGQ mqd_t     /**< Posix Message Queue Type */
#define SIZE_QUEUE  16    /**< Size of the Message Queue */
#define MSG_SIZE  sizeof(MAXINT)
#define MQPERM IPC_CREAT | IPC_EXCL | 0666 /**< Message Queue Permission */
#define MQPRIO_MIN 1      /**< Min Message Queue Priority Level */
#define MQPRIO_MAX _POSIX_MQ_PRIO_MAX /**< Max Message Queue Priority Level */
#define MQACCESS_FIFO 0   /**< Message Queue Receive Policy */



#define ORCNORMALPRI 0    /**< Message Queue Normal Priority Level */
#define ORCURGENTPRI 1    /**< Message Queue Urgent Priority Level */

extern char orcMsgQName[256];       /**< Global name for the message queue */
extern char orcMsgQRec[SIZE_QUEUE]; /**< Global name for the received message message queue */
#else
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
/**
* Message queue Id structure
*/
typedef struct msgQId
{
    int msgId; /**< Message queue id */
    key_t key; /**< Key */
    //  char msgName[256];
}
orcmsgQ_t;

/**
* Message buffer structure used within message queue
*/
typedef struct msgBuf
{
    long mtype;     /**< Message type, must be > 0 */
    int orcmsgInfo; /**< Message data */
}
orcmsgQBuf_t;

#define ORCMSGQ orcmsgQ_t * /**< Message Queue Type */
#define SIZE_QUEUE  10      /**< Size of the Message Queue */
#define SIZE_MES_MAX 4056   /**< Maximum size of message in Message Queue */
#define SIZE_MES_MIN 128    /**< Minimum size of message in Message Queue */
#define MQPERM IPC_CREAT | IPC_EXCL | 0666 /**< Message Queue Permission */
#define MQPRIO_MAX (255)        /**< Message Queue Priorities */
#define ORCNORMALPRI 1          /**< Message Queue Normal Priority Level */
#define ORCURGENTPRI MQPRIO_MAX /**< Message Queue Urgent Priority Level */
#endif
/*-------Semaphores----------*/
typedef struct sem_struct
{
    sem_t * sem;
    int maxcount;
}
orcSem_t;
//#define ORCSEM_ID sem_t * /**< Semaphore Type */
#define ORCSEM_ID orcSem_t * /**< Semaphore Type */
#define ORCFULL  1        /**< State of a Full Binary Semaphore */
#define ORCEMPTY 0        /**< State of a Empty Binary Semaphore */
#define ORCFOREVER -1     /**< Waiting forever */
#define ORCMUTEX_ID pthread_mutex_t *  /**< Mutex Type */

#define WAIT_FOREVER 0 /**< Define set to be compatible with previous OS */

/*-------Tasks----------*/
#define ORCTHR_ID pthread_t *    /**< Posix Thread Type */
#define STACKSIZE size_t         /**< Stack size Type */
#define SCHEDPOLICY SCHED_FIFO  /**< Scheduling policy (see schedbits.h, sched.h )*/

#define PRI_CLK  sched_get_priority_max(SCHEDPOLICY) /**< Clock task priority level (see sched_get_priority_max() in sched.h) */
#define PRI_MIN  sched_get_priority_min(SCHEDPOLICY)
#define PRI_CNL   (PRI_CLK - 1) /**< Canal task priority level */
#define PRI_RT    (PRI_CLK - 2) /**< Robot controller task priority level */
#define PRI_MT    (PRI_RT)      /**< Module controller task priority level */
#define PRI_PRR   (PRI_MIN)      /**< Procedure controller task priority level */
#define PRI_MT_MAX    (PRI_RT - 5) //5 priority levels for system tasks
#define PRI_MT_MIN   (PRI_MIN)
#define PRIO_PHR  (PRI_RT - 1) //a definir selon l'OS

#define SMALL_STACK  (PTHREAD_STACK_MIN)//5000           /**< Small task stack size PTHREAD_STACK_MIN not defined */
#define NORMAL_STACK (5*SMALL_STACK)  /**< Normal task stack size */
#define BIG_STACK    (15*SMALL_STACK) /**< Big task stack size */

/*-------Timers----------*/
#define NSEC_PER_SEC 1000000000 //already define in posix_timers.h
#define ORC_CLOCK CLOCK_MONOTONIC /**<Linux Realtime Wall Clock */
//#define ORC_CLOCK CLOCK_MONOTONIC_HR /**<High Resolution Wall Clock */
//#define TIME_UNIT 1000000 /**< Time unit = microsecond */
#define TIME_UNIT 1000000000 /**< Time unit = nanosecond */
//#define TIME_RES  1000000    /**< Linux Timer Resolution : 1 ms = 1000000 ns*/
#define TIME_RES  1  /**< High Res. Timer Resolution = 1 ns with 2.6.x-hrt kernel */
#define MAXTIMERS 100 /**< Max. number of timers */
#define MINPERIOD 0.0001  /**< Minimum period in seconds */
class ModuleTask;

typedef struct _orc_timer_t
{
  timer_t timerId; /**< posix timer >*/
  struct itimerspec timerInterval; /**< to handle timer period */
  double period; /**< periodic delay */
  ModuleTask * mt; /**< module task triggered by this timer */
}
orcTimer_t;

#define ORCTIMER_ID orcTimer_t * /**< Timer Type */
#define ORCSIGTIMER SIGRTMIN      /**< Signal the timer is linked with */

extern ORCTHR_ID pthrTimerHandlerId; /**< timer handler thread */
extern sigset_t allsigset, oldsigset;
extern long startime, now;
extern long hjitter, jinc, max, moy, drift, jitter;
extern int Timer_exists, ji;
extern double  ns_period;
extern ORCTIMER_ID AsyncTimer[MAXTIMERS];
extern int nMsgSize;
extern struct timespec orctime;
extern struct timespec thrtime;
extern int st1, sval;
extern int NbHClocks;
extern void * dummy;
//extern unsigned long flags;
/*-------Message Queues----------*/
#ifdef _ORC_MQ_POSIX
/**
 * Function to create a message queue
 *
 * @param
 * @return ORCMSGQ pointer if posix message queue created, NULL otherwise and errno is set
 * @see ORCMSGQ
 * @see <mqueue.h>#mq_open
 * @see <mqueue.h>#mq_unlink
 */
inline int orcMsgQCreate(ORCMSGQ *orcMsgQ)
{
    ORCMSGQ err;
    struct mq_attr mqAttr;
    char sTmp[256];

    sprintf(sTmp, "%d", MAXINT);
    nMsgSize = sizeof(sTmp) - 1;

#ifdef _DEBUG
    fprintf(stderr, "orcMsgQCreate:: nMsgSize = %d\n", nMsgSize);
    fprintf(stderr, "orcMsgQCreate:: sizeof(MAXINT = %d \n", sizeof(MAXINT));
#endif
    mqAttr.mq_maxmsg = SIZE_QUEUE;
    mqAttr.mq_msgsize = nMsgSize;
    mqAttr.mq_flags = 0; // 0 = blocking open on the queue

    // Create the MQNAME identifier MQNAME =  getpid()
    sprintf(orcMsgQName, "/%ld", getpid());

    err = mq_open(orcMsgQName, O_RDWR | O_CREAT | O_EXCL, 0x1B0, &mqAttr);
    if (err == (ORCMSGQ)ERROR)
    {
        if (errno == EEXIST)
        {
            printf("msq already exists \n");
            mq_unlink(orcMsgQName);
            err = mq_open(orcMsgQName, O_RDWR | O_CREAT, 0x1B0, &mqAttr);
            if (err == (ORCMSGQ)ERROR)
            {
                *orcMsgQ = -1;
                return ERROR;
            }
        }
        else
        {
            *orcMsgQ = -1;
            return ERROR;
        }
    }
    *orcMsgQ = err;
    return OK;
}

/**
 * Function to get the number of messages pending in a posix message queue
 *
 * @param posix message queue
 * @return long number of messages
 * @see <mqueue.h>#mq_getattr
 * @see <mqueue.h>#mq_attr
 */
inline long orcMsgQNumMsgs(ORCMSGQ queue)
{
    struct mq_attr mqstat;
    mq_getattr(queue, &mqstat);
    return mqstat.mq_curmsgs;
}

/**
 * Function to send a message on a posix message queue
 * Sending the message is done according to a priority level such as :
 *
 * @param posix message queue
 * @param msg message to be sent
 * @param prio message priority level
 * @return mq_send returned value
 * @see <mqueue.h>#mq_send
 */
inline int orcMsgQSend(ORCMSGQ queue, int msg, int prio)
{

#ifdef _DEBUG
    fprintf(stderr, "orcMsgQSend msg = %d \n", msg);
#endif
    sprintf(orcMsgQRec, "%d", msg);

    if (prio == ORCNORMALPRI)
    {
        return mq_send(queue, orcMsgQRec, sizeof(orcMsgQRec), MQPRIO_MIN);
    }
    else
        return mq_send(queue, orcMsgQRec, sizeof(orcMsgQRec), MQPRIO_MAX);
}

/**
 * Function to receive messages on a posix message queue
 *
 * @param posix message queue
 * @param msg pointer to the received message
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <mqueue.h>#mq_receive
 */
inline int orcMsgQReceive(ORCMSGQ queue, int *nMsgVal)
{
#ifdef _DEBUG
    fprintf(stderr, "orcMsgQReceive:: SIZE_QUEUE = %d \n", SIZE_QUEUE);
#endif

    if (mq_receive(queue, orcMsgQRec, nMsgSize, MQACCESS_FIFO) == ERROR)
    {
        fprintf(stderr, "mq_receive Error = %d \n", errno);
        return ERROR;
    }
    *nMsgVal = atoi(orcMsgQRec);

#ifdef _DEBUG
    fprintf(stderr, "orcMsgQReceive:: msg = %d \n", *nMsgVal);
#endif

    return OK;
}

/**
 * Function to close and erase a posix message queue
 *
 * @param queue pointer to the message queue
 * @return mq_close returned status or mq_unlink returned status
 * @see  <mqueue.h>#mq_close
 * @see  <mqueue.h>#mq_unlink
 */
inline int orcMsgQClose(ORCMSGQ queue)
{
    int err1, err2;
    err1 = mq_close(queue);
    err2 = mq_unlink(orcMsgQName);

    return (err1 ? err1 : err2);
}
#else
/**
* Function to create a message queue
*
* @param ORCMSGQ pointer if message queue created, NULL otherwise and errno is set
* @return OK if successfull, ERROR otherwise
* @see ORCMSGQ
* @see <sys/msg.h>#msgget
* @see <sys/msg.h>#msgctl
*
*/
inline int orcMsgQCreate(ORCMSGQ *orcMsgQ)
{
    //  ORCMSGQ orcMsgQ;
    key_t key = IPC_PRIVATE;
    int err;

    err = msgget(key, MQPERM);
    if ((err == ERROR) && (errno == EEXIST))
    {
        printf("orcMsgQCreate: msgQ already exists \n");
        err = msgget(key, 0);
        msgctl(err, IPC_RMID, 0);
        err = msgget(key, IPC_CREAT | IPC_EXCL | MQPERM);
    }
    else if ((err == ERROR) && (errno != EEXIST))
    {
        printf("orcMsgQCreate: Error = %d \n", errno);
        return ERROR;
    }

    *orcMsgQ = (ORCMSGQ) malloc(sizeof(orcmsgQ_t));
    (*orcMsgQ)->msgId = err;
    (*orcMsgQ)->key = key;
    printf("orcMsgQ Created \n");
    return err;
}

/**
* Function to get the number of messages pending in a message queue
*
* @param queue pointer to the message queue
* @return long number of messages if no error occurs otherwise ERROR and errno is set
* @see <sys/msg.h>#msgctl
*/
inline long orcMsgQNumMsgs(ORCMSGQ queue)
{
    struct msqid_ds tmpBuf;

    if (msgctl(queue->msgId, IPC_STAT, &tmpBuf) == ERROR)
    {
        printf("orcMsgQNumMsgs: Error = %d \n", errno);
        return ERROR;
    }
#ifdef _DEBUG
    printf("orcMsgQNumMsgs:: nb of msgs = %d \n", tmpBuf.msg_qnum );
#endif
    return (long)tmpBuf.msg_qnum;
}

/**
* Function to send a message on a message queue
* Sending the message is done according to a priority level such as :
*
* @param queue pointer to the message queue
* @param msg message to be sent
* @param prio priority level
* @return OK if successfull, ERROR otherwise and errno is set
* @see <sys/msg.h>#msgsnd
*/
inline int orcMsgQSend(ORCMSGQ queue, int msg, int prio)
{
    orcmsgQBuf_t tmpBuf;
    int nMsgSize;

    nMsgSize = sizeof(orcmsgQBuf_t) - sizeof(long);
#ifdef _DEBUG
    printf("orcMsgQSend: msgId = %d \n", queue->msgId);
    printf("orcMsgQSend: nMsgSize = %d \n", nMsgSize);
#endif
    tmpBuf.orcmsgInfo = msg;
    tmpBuf.mtype = MQPRIO_MAX - prio;

    /* 0 : If message queue is full -> message is not written to the queue */
    if (msgsnd(queue->msgId, &tmpBuf, nMsgSize, 0) == ERROR)
    {
        printf("orcMsgQSend: Error = %d \n", errno);
        return ERROR;
    }
    return OK;
}

/**
* Function to receive messages on a message queue
*
* @param queue pointer to the message queue
* @param msg pointer to the message received
* @return OK if successfull, ERROR otherwise and errno is set
* @see <sys/msg.h>#msgrcv
*/
inline int orcMsgQReceive(ORCMSGQ queue, int *msg)
{
    orcmsgQBuf_t tmpBuf;
    int nMsgSize;

    nMsgSize = sizeof(orcmsgQBuf_t) - sizeof(long);

    /* 0 : the calling process blocks until a message arrives in queue */
    /* mtype = -MQPRIO_MAX: type of message to retrieve from the queue
    if mtype < 0 : first message on
    the queue with the lowest type less than  or  equal
    to the absolute value of msgtyp will be read
    */
    if (msgrcv(queue->msgId, &tmpBuf, nMsgSize, -MQPRIO_MAX, 0) == ERROR)
    {
        printf("orcMsgQReceive: Error = %d \n", errno);
        return ERROR;
    }
#ifdef _DEBUG
    printf("orcMsgQReceive: msg received = %d \n", tmpBuf.orcmsgInfo);
#endif
    *msg = tmpBuf.orcmsgInfo;

    return OK;
}

/**
* Function to close and erase a message queue
*
* @param queue pointer to the message queue
* @return OK if successfull, ERROR otherwise and errno is set
* @see <sys/msg.h>#msgctl
*/
inline int orcMsgQClose(ORCMSGQ queue)
{

    if (msgctl(queue->msgId, IPC_RMID, 0) == ERROR)
    {
        printf("orcMsgQClose: Error = %d \n", errno);
        return ERROR;
    }
    printf("orcMsgQ closed\n");
    free(queue);

    return OK;
}
#endif

/*-------Semaphores----------*/
/**
 * Function to create a semaphore
 *
 * @param initState semaphore initial creation state, eg ORCFULL, ORCEMPTY
 * @return ORCSEM_ID semaphore pointer created if successfull, NULL otherwise
 * @see ORCFULL
 * @see ORCEMPTY
 * @see <semaphore.h>#sem_init
 */
inline ORCSEM_ID orcSemCreate(int initState, int maxcount)
{
    ORCSEM_ID newSem;
    newSem = new orcSem_t;
    newSem->sem = new sem_t;
    newSem->maxcount = 0;
    int status;

    if (initState > maxcount)
    {
        initState = maxcount;
        printf("waring, trying to initialize semaphore above maxcount \n");
    }

    status = sem_init(newSem->sem, 0, initState);
    newSem->maxcount = maxcount;

    if (status == OK)
    {
#ifdef _DEBUG
        printf("orcSemCreate %p\n", newSem);
#endif
        return newSem;
    }
    else
    {
#ifdef _DEBUG
        printf("SemCreate returns NULL\n");
#endif
        return NULL;
    }
}

inline ORCSEM_ID orcSemCreate_Fugace()
{
    printf("fugace semaphore cannot be implemented with this OS \n");
    return NULL;
}

inline ORCSEM_ID orcSemCreate_count(int value, int maxcount)
{
    return orcSemCreate(value,maxcount);
}

/**
 * Function to delete a given semaphore
 *
 * @param sem semaphore to be deleted
 * @return sem_destroy() returned value
 * @see <semaphore.h>#sem_destroy
 */
inline int orcSemDelete(ORCSEM_ID sem)
{
#ifdef _DEBUG
    printf("SemDelete %p\n", sem);
#endif
    return (sem_destroy(sem->sem));
}

/**
 * Function to give a semaphore
 *
 * @param sem semaphore
 * @return sem_post) returned value
 * @see <semaphore.h>#sem_post
 */


inline int orcSemGive(ORCSEM_ID sem)
{
    //int res = OK;
#ifdef _DEBUG
    printf("SemGive %p\n", sem);
#endif

    /*     save_flags(flags); */
    /*     cli(); */
    sem_getvalue(sem->sem, &sval);

    /*     if(sem->maxcount > 0){ */
    if (sval < sem->maxcount)
        return (sem_post(sem->sem));
    /* else printf("trying to overload sem \n"); */
    /*     } */
    /*     else //if(sem->maxcount == 0) */
    /*       { */
    /* 	sem_post(sem->sem); */
    /* 	sem_getvalue(sem->sem, &sval); */
    /* 	if (sval > 0) { */
    /* 	  sem_wait(sem->sem); */
    /* 	  printf("skipping \n"); */
    /* 	} */
    /*       } */

    /*     restore_flags(flags); */
    return OK;
}

/**
 * Function to take a semaphore
 * TODO : no timeout in Posix semaphores
 *
 * @param sem semaphore
 * @param timeout value
 * @return sem_wait() returned value
 * @see <semaphore.h>#sem_wait
 */
inline int orcSemTake(ORCSEM_ID sem, int timeout)
{

    int dummy = timeout; // set to avoid compilation warnings
#ifdef _DEBUG
    printf("SemTake %p\n", sem);
#endif

    /* for debug the signal mask purpose*/
    while (sem_wait(sem->sem) < 0)
    {
        perror("semwait error");
        printf("interrupt on sem %p \n", sem);
    }
    //return (sem_wait(sem->sem));
}

/*-------Mutex----------*/
/**
 * Function to create a mutex in unlocked state
 *
 * @param initState semaphore initial creation state, eg ORCFULL, ORCEMPTY
 * @return ORCMUTEX_ID mutex pointer created if successfull, NULL otherwise
 * @see <pthread.h>#pthread_mutex_init
 */
inline ORCMUTEX_ID orcMutexCreate()
{
    ORCMUTEX_ID newMutex;
    newMutex = new pthread_mutex_t;
    int status;

    status = pthread_mutex_init(newMutex, NULL);

    if (status == OK)
    {
#ifdef _DEBUG
        printf("orcMutexCreate %p\n", newSem);
#endif
        return newMutex;
    }
    else
    {
#ifdef _DEBUG
        printf("orcMutexCreate returns NULL\n");
#endif
        return NULL;
    }
}
/**
 * Function to delete a given mutex
 *
 * @param mutex to be deleted
 * @return pthread_mutex_destroy returned value
 * @see <pthread.h>#pthread_mutex_destroy
 */
inline int orcMutexDelete(ORCMUTEX_ID mutex)
{
#ifdef _DEBUG
    printf("MutexDelete %p\n", mutex);
#endif
    return (pthread_mutex_destroy(mutex));
}
/**
 * Function to lock a mutex
 *
 * @param mutex
 * @return pthread_mutex_lock() returned value
 * @see <pthread.h>#pthread_mutex_lock
 */
inline int orcMutexLock(ORCMUTEX_ID mutex)
{
#ifdef _DEBUG
    printf("MutexLock %p\n", mutex);
#endif
    return (pthread_mutex_lock(mutex));
}
/**
 * Function to unlock a mutex
 *
 * @param mutex
 * @return pthread_mutex_unlock() returned value
 * @see <pthread.h>#pthread_mutex_lock
 */
inline int orcMutexUnlock(ORCMUTEX_ID mutex)
{
#ifdef _DEBUG
    printf("MutexUnlock %p\n", mutex);
#endif
    return (pthread_mutex_unlock(mutex));
}


/*-------Tasks----------*/
/**
 * Function for suspending current process
 * TODO : no taskDelay in Posix (sleep uses seconds, delay ?, nanosleep ?)
 *
 * @param delay
 * @return OK
 */
inline int orcTaskDelay(int delay)
{
    int dummy = delay; // set to avoid compilation warnings

    return OK;
}

/**
 * Function to check if a task is still valid or not
 *
 * @param Id task identifier
 * @return OK if task is valid, ERROR otherwise
 * @see ORCTHR_ID
 * @see pthread_kill
 */
inline int orcTaskIdVerify(ORCTHR_ID Id)
{
    if (Id != NULL)
    {
        /* if (pthread_kill(*Id, 0) != OK) */
        /*         { */
        /*             printf(" pthread_kill ERROR on %p \n", Id); */
        /*             return ERROR; */
        /*         } */
        /*         else */
        return OK;
    }
    printf("orcTaskIdVerify ERROR on %p \n", Id);
    return ERROR;
}

/**
 * Function to kill a task (pthread)
 *
 * @param Id task identifier
 * @return ERROR or pthread_cancel returned value
 * @see ORCTHR_ID
 * @see <pthread.h>#pthread_kill
 * @see <pthread.h>#pthread_cancel
 */
inline int orcTaskDelete(ORCTHR_ID Id)
{
    int status;
    // Check if thread is ok
    if ( Id != NULL)
    {
        if (pthread_kill(*Id, 0) == OK)
        {
            // Send SIGTERM signal <- BAD!!
            // return (pthread_kill(*Id,SIGTERM));
            printf("orcTaskDelete %p\n", Id);
            status = pthread_cancel(*Id);
            if (status == OK) return status;
        }
    }
    printf("orcTaskDelete ERROR %p\n", Id);
    return ERROR;
}

/// Function to set relative linux task priority
inline int orcScalePriority(int os_priority)
{
    if ((PRI_MT_MIN + os_priority < PRI_MT_MAX) && os_priority > 0)
        return ( PRI_MT_MIN + os_priority );
    printf("ERROR orcScalePriority, priority=%d\n", os_priority);
    return PRI_MT_MAX;
}

/**
 * Function to spawn a task
 *
 * @param tid task identifier
 * @param name name descripting the task
 * @param prio task priority level
 * @param stackSize stack size to be allocated for the task
 * @param funcptr routine to be spawn by the task
 * @param arg argument needed for the routine to be spawn
 * @return pthread_create returned value
 * @see ORCTHR_ID
 * @see STACKSIZE
 * @see FUNCPTR
 * @see <pthread.h>#pthread_create
 */
inline int orcSpawn(ORCTHR_ID *tid, const char *name, int prio, STACKSIZE stackSize, SOSO funcptr, void * arg)
{

    int status;
    int prioget, pol;
    struct sched_param schedattriget, schedattributes;
    pthread_attr_t attributes;

    *tid = new pthread_t;

    // To avoid compilation warnings
    char *dummy = (char *)malloc((strlen(name) * sizeof(char)) + 1);
    strcpy(dummy, name);

    // Allocation and initialization of the attribute structure
    pthread_attr_init(&attributes);
    // setting CPU affinity
    /* cpu_set_t mask; */
    /*     CPU_ZERO (&mask); */
    /*     CPU_SET (0,&mask); */
    /*     pthread_attr_setaffinity_np(&attributes, sizeof(mask), &mask); */
    // Thread creation
    status = pthread_create(*tid, &attributes, (SOSO)funcptr, arg);
#ifdef _DEBUG
    if (status != 0)
        fprintf(stderr, "pthread_Create %s error = %d \n", name, status);
#endif
    schedattributes.sched_priority = prio;
    pthread_setschedparam(**tid, SCHED_FIFO, &schedattributes);
    pthread_getschedparam(**tid, &pol, &schedattriget);
    prioget = schedattriget.sched_priority;
    printf("thr create %s pol %d prio %d StackSize %d Id %p \n", name, pol, prioget, stackSize, *tid);
    return status;
}

inline int orcSpawn1(ORCTHR_ID *tid, const char *name, int prio, STACKSIZE stackSize, SOSO funcptr, void * arg)
{

    int status;
    int prioget, pol;
    struct sched_param schedattriget, schedattributes;
    pthread_attr_t attributes;

    *tid = new pthread_t;

    // To avoid compilation warnings
    char *dummy = (char *)malloc((strlen(name) * sizeof(char)) + 1);
    strcpy(dummy, name);

    // Allocation and initialization of the attribute structure
    pthread_attr_init(&attributes);
    // setting CPU affinity
    /* cpu_set_t mask; */
    /*     CPU_ZERO (&mask); */
    /*     CPU_SET (1,&mask); */
    /*     pthread_attr_setaffinity_np(&attributes, sizeof(mask), &mask); */
    // Thread creation
    status = pthread_create(*tid, &attributes, (SOSO)funcptr, arg);
#ifdef _DEBUG
    if (status != 0)
        fprintf(stderr, "pthread_Create %s error = %d \n", name, status);
#endif
    schedattributes.sched_priority = prio;
    pthread_setschedparam(**tid, SCHED_FIFO, &schedattributes);
    pthread_getschedparam(**tid, &pol, &schedattriget);
    prioget = schedattriget.sched_priority;
    printf("thr create %s pol %d prio %d StackSize %d Id %p \n", name, pol, prioget, stackSize, *tid);
    return status;
}

/**
 * Function to lock a task
 * This function does nothing : defined for interoperability between OS needs
 *
 * @param
 * @return OK
 */
inline int orcTaskLock()
{
    return OK;
}

/**
 * Function to unlock a task
 * This function does nothing : defined for interoperability between OS needs
 *
 * @param
 * @return OK
 */
inline int orcTaskUnlock()
{
    return OK;
}

/*-------Timers----------*/
/**
 * Timer handler function
 *
 * @param int
 * @return
 */
void *orcTimer_handler(void *);

/**
 * Function to unmask SIGINT and mask all signals including timer signal interruption
 *
 * @param
 * @return OK if successful, ERROR otherwise and errno is set
 * @see ORCSIGTIMER
 * @see <signal.h>#sigprocmask
 * @see <signal.h>#sigaddset
 */
inline int orcTimerSigMask()
{
    sigset_t set;

    sigfillset(&set);
    if (pthread_sigmask(SIG_BLOCK, &set, NULL) != OK)
    {
        printf("pthread_sigmask Failed %d \n", errno);
        return ERROR;
    }
    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    if (pthread_sigmask(SIG_UNBLOCK, &set, NULL) != OK)
    {
        printf("pthread_sigmask Failed %d \n", errno);
        return ERROR;
    }
    return OK;
}
/**
 * Function to un-mask timer signal and mask all others
 *
 * @param
 * @return OK if successful, ERROR otherwise and errno is set
 * @see ORCSIGTIMER
 * @see <signal.h>#sigprocmask
 * @see <signal.h>#sigaddset
 */
inline int orcTimerSigUnMask()
{
    sigset_t set;

    sigfillset(&set);
    if (pthread_sigmask(SIG_BLOCK, &set, NULL) != OK)
    {
        printf("pthread_sigmask Failed %d \n", errno);
        return ERROR;
    }
    sigemptyset(&set);
    sigaddset(&set, ORCSIGTIMER);
    if (pthread_sigmask(SIG_UNBLOCK, &set, NULL) != OK)
    {
        printf("pthread_sigmask Failed %d \n", errno);
        return ERROR;
    }
    return OK;
}

/**
 * Function to set the timer settings
 * This function sets in particular the period for the timer[i]
 *
 * @param index of the timer
 * @param period in seconds value to be set for timer
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see timespec structure
 */
inline int orcTimerSetTime(int i, double period)
{
    double sec, nsec;
    int status;
    sigset_t oldsig;
#ifdef _DEBUG
    pthread_sigmask(SIG_SETMASK, NULL, &oldsig);
    if ((status = sigismember(&oldsig, ORCSIGTIMER)) == 0) printf("ORCSIGTIMER NOT BLOCKED in begin SetTime\n");
    else if (status == 1) printf("ORCSIGTIMER is BLOCKED in begin SetTime\n");
#endif
    //check period validity
    if(period < MINPERIOD) period = MINPERIOD;
    //Convert the period in nanosecond
    double ns_period = (double) NSEC_PER_SEC * period;
    // Set the timespec struct -> delay in second, delay in microsecond
    // get the part in second
    sec = floor(ns_period / (double)NSEC_PER_SEC);
    // get the remainder part in nanosecond
    nsec = rint(fmod(ns_period, (double)NSEC_PER_SEC));

     AsyncTimer[i]->timerInterval.it_interval.tv_sec = (int)(sec);
    // Handle period < TIME_RES
    if (ns_period < (double) TIME_RES)
    {
      AsyncTimer[i]->timerInterval.it_interval.tv_nsec  = TIME_RES;
    }
    else
      AsyncTimer[i]->timerInterval.it_interval.tv_nsec = (int)(floor(nsec / (double)TIME_RES)) * TIME_RES;

    // Setting the ns_period for the timer interval
    AsyncTimer[i]->timerInterval.it_value.tv_sec = 0; /** initial expiration */
    AsyncTimer[i]->timerInterval.it_value.tv_nsec = TIME_RES; /**tv_nsec must not be 0!!! */


#ifdef _DEBUG
    fprintf(stderr, "orcTimerSetTime:: AsyncTimer[i]->timerInterval.it_value.tv_sec = %d, AsyncTimer[i]->timerInterval.it_value.tv_nsec = %d \n", AsyncTimer[i]->timerInterval.it_value.tv_sec, AsyncTimer[i]->timerInterval.it_value.tv_nsec);
#endif

    return OK;
}

/**
 * Function to reset the timer settings
 * This function sets in the period and restarting delay for timer[i]
 *
 * @param index of the timer
 * @param period in seconds value to be set for timer
 * @param restarting delay
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see timespec structure
 */

inline int orcTimerResetTime(int i, double period, long long ns_delay)
{
    double sec, nsec;
    //check period validity
    if(period < MINPERIOD) period = MINPERIOD;
    //Convert the period in nanosecond
    double ns_period = (double) NSEC_PER_SEC * period;
    // Set the timespec struct -> delay in second, delay in microsecond
    // get the part in second
    sec = floor(ns_period / (double)NSEC_PER_SEC);
    // get the remainder part in nanosecond
    nsec = rint(fmod(ns_period, (double)NSEC_PER_SEC));

     AsyncTimer[i]->timerInterval.it_interval.tv_sec = (int)(sec);
    // Handle period < TIME_RES
    if (ns_period < (double) TIME_RES)
    {
      AsyncTimer[i]->timerInterval.it_interval.tv_nsec  = TIME_RES;
    }
    else
      AsyncTimer[i]->timerInterval.it_interval.tv_nsec = (int)(floor(nsec / (double)TIME_RES)) * TIME_RES;


    // get the part in second
    sec = ns_delay / NSEC_PER_SEC;
    // get the remainder part in nanosecond
    nsec = ns_delay %  NSEC_PER_SEC;

     AsyncTimer[i]->timerInterval.it_value.tv_sec = (int)(sec);
    // Handle delay < TIME_RES
    if (ns_delay < (double) TIME_RES)
    {
      AsyncTimer[i]->timerInterval.it_value.tv_nsec  = TIME_RES; /**tv_nsec must not be 0!!! */
    }
    else
      AsyncTimer[i]->timerInterval.it_value.tv_nsec = (int)(floor(nsec / (double)TIME_RES)) * TIME_RES;

#ifdef _DEBUG
    fprintf(stderr, "orcTimerSetTime:: AsyncTimer[i]->timerInterval.it_value.tv_sec = %d, AsyncTimer[i]->timerInterval.it_value.tv_nsec = %d \n", AsyncTimer[i]->timerInterval.it_value.tv_sec, AsyncTimer[i]->timerInterval.it_value.tv_nsec);
#endif

    return OK;
}

/**
 * Function to create a periodic timer
 * This function sets a periodic timer,
 * the corresponding signal  and the timer handler
 * and the orcTimer structure is affected
 *
 * @param period value to be set for timer
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see ORCTIMER_ID
 * @see orcTimerSetTime
 * @see orcTimer
 * @see <timer.h>#timer_create
 */

extern int orcTimerInit(int, ModuleTask *);

/**
 * Function to delete/erase the timer
 * This function disabled the periodic timer, the associated signal
 * unmask this signal within the task and erase the AsyncTimer structure
 *
 * @param
 * @return OK if successfull, ERROR otherwise
 * @see orcTimer
 * @see <timer.h>#timer_delete
 * @see <signal.h>#sigemptyset
 * @see <signal.h>#sigprocmask
 */
extern int orcTimerDelete(void);

/**
 * Function to set the periodic timer handler
 *
 * @param func timer handler function pointer
 * @param arg argument for the timer handler function
 * @return OK
 */
extern int orcTimerArm(void);
/**
 * Function to launch the periodic timers
 *
 * @param
 * @return OK if successfull, ERROR otherwise
 * @see ORCSIGTIMER
 * @see <signal.h>#sigismember
 * @see <pthread.h>#pthread_sigmask
 */
extern int orcTimerLaunchAll(void);
/**
 * Function to restart ith periodic timer[i]
 *
 * @param
 * @return OK if successfull, ERROR otherwise
 * @see ORCSIGTIMER
 * @see <signal.h>#sigismember
 * @see <pthread.h>#pthread_sigmask
 */
inline int orcTimerReset(int i)
{
    /* rearming the ith timer */

    if (timer_settime(AsyncTimer[i]->timerId, 0, &(AsyncTimer[i]->timerInterval), NULL) < 0)
    {
      printf("AsyncTimer[%d]Reset:: failed errno = %d \n", i, errno);
        return ERROR;
    }
      printf("AsyncTimer[%d]Reset \n", i);
    return OK;
}

/* -------- MISC -------- */
/**
 * Function to check a given path
 * TODO : MAXFILENAME is set to 80 in util.h, it should be PATH_MAX+1
 *
 * @param path pathname to retrieve
 * @return OK if successfull, ERROR otherwise
 * @see MAXFILENAME
 */
inline int orcGetPath(char *path)
{
    if (getcwd(path, MAXFILENAME) != OK)
        return ERROR;
    return OK;
}

/**
 * Function to set a directory path
 *
 * @param path name pf the directory path
 * @return chdir() returned value
 * @see chdir
 */
inline int orcSetPath(const char *path)
{
    return chdir(path);
}

/**
 * Function to spy events
 * Function does nothing but defined for interoperability need between OS
 *
 * @param eventid event identifier to spy
 * @param buffer
 * @param bufsize
 * @return OK
 */
inline int orcSpyEvent(int eventid, char* buffer, int bufsize)
{
    return OK;
};


/* Functions to have very accurate time measurements */
// (for feedback scheduling. Not all are correctly implemented yet in linux)

/**
 * Function to get Cpu time in nanoseconds
 * Behaviour unknown on a SMP...
 * 
 * @return long long time in nanoseconds from boot
 */
inline long long orcGetCpuTime(void)
{
    clock_gettime(ORC_CLOCK, &orctime);
    return (long long)(&orctime)->tv_sec *NSEC_PER_SEC + (&orctime)->tv_nsec;
}

/**
 * Function to get the execution time of a task in nanoseconds
 * It sems that the baheviour of CLOCK_THREAD_CPUTIME_ID is not 
 * the one expected from reading Posix specs
 *
 * @param task to be probed
 * @return long long exectimetime in nanoseconds
 */
inline long long orcGetExecTime(ORCTHR_ID task)
{
    st1 = clock_gettime(CLOCK_THREAD_CPUTIME_ID, &thrtime);
    if (st1 == 0) return(thrtime.tv_sec*NSEC_PER_SEC + thrtime.tv_nsec);
    else
    {
        printf("error %d on orcGetExecTime \n", st1);
        return(long long)0;
    }
}

/**
 * Function to get the start time of a task 
 * 
 *
 * @param task to be probed
 * @return 0, not yet implemented
 */inline long long orcGetStartTime(ORCTHR_ID task)
{
    return (long long)0;
}

/**
 * Function to set the system in real-time mode
 * Needs to execute as super-user
 *
 * 
 * @return OK on success
 * @see sched_setscheduler
 */
inline int orcMakeRealTime(void)
{
    struct sched_param mysched;
    int cr;
    //mlockall(MCL_CURRENT | MCL_FUTURE);
    pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    mysched.sched_priority = PRI_PRR;
    if ((cr = sched_setscheduler( 0, SCHED_FIFO, &mysched )) == -1 )
    {
        printf("ERROR IN SETTING THE POSIX SCHEDULER\n");
        exit(1);
    }
    printf("POSIX scheduler set in RT FIFO mode\n");
    return cr;
}

/**
 * Function to wait for the termination of a task
 *
 * @param task to wait for
 * @return OK on success
 * @see pthread_join
 */
inline int orcTaskJoin(ORCTHR_ID task)
{
    int cr;
    cr = pthread_join(*task, NULL);
    if (cr != 0)
    {
        printf("pthread_join error %d on task %p \n", cr, task);
        return cr;
    }
    else return OK;
}

#define timerdiff(a,b) ((long)((a)->tv_sec - (b)->tv_sec)*NSEC_PER_SEC + (long)((a)->tv_nsec - (b)->tv_nsec))


#endif

