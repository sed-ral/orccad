//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/module.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 beta
// Creation       : 6 November 1996
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtine of
//           a Module.
//
// Class Definition:
//    ModuleAlgo:  Module-Algo code
//    ModulePr  :  Module-Pr code
//    ModuleAtr :  Module-Atr code
//
// Errors:
//
//**************************** Modifications **********************************
//
//
// SOSO#time 27-06-00 Add temporal routine with respect to relative
//                    computation period
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#ifndef __module_h
#define __module_h

#include "osOrcc.h"
#include "global.h"

class ModuleTask;

class ModuleAlgo: public Global
{
protected:
    // Name
    char * Name;
    // Clock index activated by ClockGen
    //int IndexClk;
    double Period;
    // algorithm sample time
    double Sample_time;
    // algorithm current time
    double Current_time;
    // algorithm init algorithm time
    double Local_reftime; // SOSO#time 27-06-00
    // Module Task ptr
    ModuleTask *Mt_ptr;
    // Parameters rt
    Parameters *Param;
public:
    // Constructors/Destrutors
    ModuleAlgo(const char *, ModuleTask *, double);
    // Destructors
    virtual ~ModuleAlgo();
    void plugParam();
    //access to algorithm sample time
    double getSampleTime();
    void setSampleTime(double);
    //access to algorithm current time
    double getCurrentTime();
    void putCurrentTime(double);
    //access to algorithm local time
    double getLocalRefTime(); // SOSO#time 27-06-00
    double getLocalTime();    // SOSO#time 27-06-00

    int getIndexClk()
    {
      return OK;//IndexClk;
    };

    // Methods redefined by inheritance
    void init()
    {
        printf("ModuleAlgo::init()\n");
    }
    void param()
    {
        printf("ModuleAlgo::param()\n");
    }
    void reparam()
    {
        printf("ModuleAlgo::reparam()\n");
    }
    void compute()
    {
        printf("ModuleAlgo::compute()\n");
    }
    void end()
    {
        printf("ModuleAlgo::end()\n");
    }
};

class ModulePr
{
protected:
    // Name
    char * Name;
public:
    // Constructors/Destrutors
    ModulePr(char *);
    // Destructors
    virtual ~ModulePr();
    // Parameters rt
    Parameters *Param;
    // Methods redefined by inheritance
    virtual void init()
    {
        printf("ModulePr::init()\n");
    }
    virtual void param()
    {
        printf("ModulePr::param()\n");
    }
    virtual void end()
    {
        printf("ModulePr::end()\n");
    }
};

class ModuleBeh
{
protected:
    // Name
    char * Name;
public:
    // Constructors/Destrutors
    ModuleBeh(char *);
    // Destructors
    virtual ~ModuleBeh();
};

#endif
