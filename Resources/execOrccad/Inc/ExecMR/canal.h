//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/canal.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 beta
// Creation       : 18 May 1994
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtine of
//           communication on Vxworks between Esterel
//           (Synchronous) and asynchronous tasks.
//
// Class Definition:
//   Canal: interfaces Asynchronous signal with a synchronous
//          automaton specified in Esterel
//
//**************************** Modifications **********************************
//
// 6 November 1996 integration on Orccad Exec
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#ifndef __canal_h
#define __canal_h

#include        "osOrcc.h"
#include        "global.h"
#include        "eventdbg.h"


class ProcedureRobot;

class Canal: public Global
{
    // Real-time function
    friend void func_manage(Canal *canal);

protected:
    int Status;        // indicate the correct constraction
    // of the object
    ORCMSGQ EventQueue;    // Queue to recept messages
    FUNCPTR EventManage;   // ptr of function task to analyse
    // messages receive, call inputs and
    // transite automaton
    ORCTHR_ID EventManageId;
    FUNCPTR TabInputFunc[MAXINPUTSIG]; // tab of input function automaton
    // with tab index = event number
    FUNCPTR AutomatonTransite;
    int NbDefinedEvent;            // number of events

    //OT 18.07.02
    /**
    * @brief Semaphores to synchronize the sending-receiving of messages in the message queue.
    * 
    * The mechanism works as follow:
    * @arg ReceiveEvent() is waiting for semaphore canal->TabSem[mbx_msg];
    * @arg As soon as SendEvent() has sent a message, it signals canal->TabSem[mbx_msg] ant waits canal->Tab * Sem[end_msg_action];
    * @arg After the automaton transition, canal->TabSem[end_msg_action] is signaled.
    * Remark: this mechanism doesn't work with a binary semaphore, and two real-time tasks of different priorities. so the second semaphore becomes a count semaphore
    * @author Olivier Testa
    */
    ORCSEM_ID TabSem[2];
public:
    // Constructors, Destructors
    Canal(ProcedureRobot *prr, FUNCPTR autotransite, FUNCPTR *tabinputfunc,
          const int nbsignals);
    ~Canal();
    // Send signal to EventQueue
    void SendEvent(SIGNAL Event, const char *label, double time);
    // Send Urgent signal to  EventQueue
    void SendUEvent(SIGNAL Event, const char *label, double time);
    // return first event of  EventQueue else return FAIL
    int ReceiveEvent();
    // return number of events received in  EventQueue
    int GetNumberEvent()
    {
        return (orcMsgQNumMsgs(EventQueue));
    };
    // Stop function task EventManage
    void StopManage();
    int GetStatus()
    {
        return Status;
    };
    //Transite and Reset Automaton
    void StartAutomaton();
};


#endif
