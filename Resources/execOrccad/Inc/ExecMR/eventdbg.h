//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/evendbg.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime
//            debug which can display system errors, logical events,
//
//
// Class Definition:
//  EventDebug: display logical events, system errors and mecanisms
//
//**************************** Modifications **********************************
//
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#ifndef __eventdbg_h
#define __eventdbg_h

#include "osOrcc.h"
#include "utils.h"

class EventDebug
{

protected:
    // Mode of debug
    int Mode;
    // Tables for matching
    char TabError[MAXTABERROR][MAXCHARNAME];
    int MaxTabError;
    char TabInEvent[MAXTABINEVENT][MAXCHARNAME];
    int MaxTabInEvent;
    char TabOutEvent[MAXTABOUTEVENT][MAXCHARNAME];
    int MaxTabOutEvent;
    char TabSem[MAXTABSEM][MAXCHARNAME];
    int MaxTabSem;
    // Tables for saving
    int HistType[MAXTABHIST];
    int HistNumber[MAXTABHIST];
    char HistLabel[MAXTABHIST][MAXCHARNAME];
    double HistTime[MAXTABHIST];
    int Histindex;
    int Histbuffer;

public:
    EventDebug(int);

    void open();
    void close();
    void displayError(int, double);
    void displayEvent(int, const char*, DBG_EVT_TYPE, double);
    void putTabInEvent(char eventname[MAXTABINEVENT][MAXCHARNAME], int);
    void displaySem(int, DBG_EVT_TYPE, double);
};

#endif
