/**
 * @file ${ORCCARD_SRC}/Inc/Exec/singleReaderACM.h
 * @brief Asynchronous communication mechanism for a single-reader-single-writer design
 * @see 'Four-slot fully asynchronous communication mechanism', H.R Simpson, IEE PROCEEDINGS, vol.137, Pt.E, No.1, January 1990.
 * @author James Lain� - james.laine@caramail.com
 * @date 15/08/2001
 * @author updated RPG 
 */

#ifndef __singleReaderACM_h
#define __singleReaderACM_h

#ifdef _DEBUG
//#include <iostream>
using namespace std;
#endif

/**
 * @class SingleReaderACM
 * @brief Asynchronous communication mechanism for a single-reader-single-writer design
 *
 * This Asynchronous communication mechanism enables a 'loosely synchronous'
 * (compared to mutual exclusion with semaphores)
 * , potentially fully asynchronous.
 * The aim of the mechanism implement in ORCCARD is to ensure data protection and to not introduce new synchronizations in MA controllers, sources of deadlocks.
 *
 * @author James Lain� - james.laine@caramail.com
 * @date 15/08/2001
 * @version 1.0
 * @since ORCCADv3.2
 * @test define sr_DEBUG to have a single reader ACM manipulation trace
 * @todo When compiler will allow the use of C++ keyword export on a template class (I used g++ --version=egcs-2.91.66), use it in generated code and compile this file with ORCCAD runtime
 * @see 'Four-slot fully asynchronous communication mechanism', H.R Simpson, IEE PROCEEDINGS, vol.137, Pt.E, No.1, January 1990.
 */


template <class DataType> class SingleReaderACM
{
protected:

    /**
     * @brief four-slot array, arranged as two pairs of slots, to store and access data value
     *
     * _data[k][i][j] is a type(int, float, double) represents by enum OrcVariableType( ${ORCCARD_SRC}/Inc/Kernel/Ityp.h).
     *
     * i represents the pair index :
     * @arg not potentially in reading if writing
     * @arg not potentially in writing if reading
     *
     * j represents the slot index in pair i which :
     * @arg NOT containing last written data what can be in reading if writing
     * @arg containing last written data if reading
     *
     * k represents the kieme index data vector.
     *
     */
    DataType (*_data)[2][2];
    /// _slot[i] : element of this array indicates the index of the slot which contains the latest data in pair i
    mybool _slot[2];
    /// indicates the pair about to be, being, or last read
    mybool _latest;
    /// indicates the pair last written
    mybool _reading;
    /// dimension
    int  _dimVar;
public:
    /**
     *  @brief preset _slot[i], _latest and _reading to zero
     */
    SingleReaderACM(int val = 1): _latest(0), _reading(0), _dimVar(val)

    {
        _slot[0]=_slot[1]=0;
        _data = new DataType[_dimVar][2][2];

    };
    /**
     *  @brief nothing to do!
     */
    ~SingleReaderACM()
    {}
    ;

    /**
    *  @brief the writing four-slots method
    *  @param <class DataType>* item : a pointer refering to the data to be written
    *  @return void
    * @see 'Four-slot fully asynchronous communication mechanism', H.R Simpson, IEE PROCEEDINGS, vol.137, Pt.E, No.1, January 1990. for algorith details
    */
    void write(DataType item[] )
    {
        mybool pair, index;
        int indexV;

        pair = !_reading;
        index = !_slot[pair];

        for (indexV=0;indexV<_dimVar;indexV++)
            _data[indexV][pair][index] = item[indexV];

        _slot[pair] = index;
        _latest = pair;

        /*       #ifdef _DEBUG */
        /*       cout << "[sr] Writes " << item[0] << " in pair " << pair  */
        /*  	   << ",slot " << index << endl;  */
        /*       #endif */
    };

    /**
    *  @brief the reading four-slots method
    *  @param none
    *  @return the latest written data
    *  @see 'Four-slot fully asynchronous communication mechanism', H.R Simpson, IEE PROCEEDINGS, vol.137, Pt.E, No.1, January 1990. for algorith details
    */

    int read(DataType item[])
    {
        mybool pair, index;
        int indexV;

        pair = _latest;
        _reading = pair;
        index = _slot[pair];

        for (indexV=0;indexV<_dimVar;indexV++)
            item[indexV]=_data[indexV][pair][index];


        /*       #ifdef _DEBUG */
        /*       cout << "[sr] Reads " << _data[0][pair][index] << " in pair " << pair  */
        /*  	   << ",slot " << index << endl;  */
        /*       #endif */

        return _dimVar;
    };
};

#endif





