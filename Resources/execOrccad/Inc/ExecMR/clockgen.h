//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/clock.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime
//           a clock generator
//
// Class Definition:
//   ClockGen  = object which generate clocks
//
//
//**************************** Modifications **********************************
//
// 6 November 1996
//
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#ifndef __clockgen_h
#define __clockgen_h

#include "osOrcc.h"
#include "utils.h"

class ModuleTask;
class RobotTask;
class ProcedureRobot;




class ClockGen
{
    // real-time horloge interrupt function
    // which control MTs (active) periodicity
    // friend void clockit(ClockGen* );
    //friend void timerArm(void* );
    //friend void clockit(void* );
    //friend void clocks(ProcedureRobot* );
    friend class RobotTask;
protected:
    int clockStatus;
    //ORCTHR_ID clockitId;            // id of the base clock connected to IT
    ORCTHR_ID clocksId;             // id of the Harmonic Clocks real-time task
    /*     ORCSEM_ID BaseClock;            // base clock semaphore */
    /*     double BasePeriod;              // period of the base clock */
    double HarmonicClock[MAXHARMONIC]; // table of periods
    int ActiveClock[MAXHARMONIC];   // Active a clock (= 1)
    //ORCTIMER_ID AsyncTimer[MAXHARMONIC];           // tab of timers
    //int NbHClocks;                  // Number of async clocks
    double Time;                    // Global time of the prr
    double TZERO;                   //starting time in nanosecs
public:
    // Constructor
    ClockGen(double *, int);
    // Destructor
    ~ClockGen();
    // Time functions
    int GetNumberOfClks()
    {
        return NbHClocks;
    }
    void ResetClks();
    double GetHarmonicPeriod(int); // Get a harmonic Clock period
    double GetTime()
    {
        return (Time = ((double) orcGetCpuTime() - TZERO)/NSEC_PER_SEC);
    }
    void Stop();
    void Init(ProcedureRobot*);
    int IsClkActive(int);
    //Feedback Scheduling
/*     void SetHarmonicPeriod(double, int); */
/*     double GetHarmonic(int); */
/*     double SetHarmonic(double, int); */
};

#endif
