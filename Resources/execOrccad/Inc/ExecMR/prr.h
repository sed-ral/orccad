//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/prr.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 beta
// Creation       : 18 May 1994
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtine of
//           a robot application (a Procedure Robot)
//
// Class Definition:
//   ProcedureRobot  = object which controls RTs
//
//
//**************************** Modifications **********************************
//
// 6 November 1996 : Integration in Orccad Exec
//
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#ifndef __prr_h
#define __prr_h


#include "utils.h"
#include "canal.h"
#include "rt.h"
#include "clockgen.h"
#include "datadbg.h"
#include "eventdbg.h"

class ProcedureRobot
{
    friend void prr();
    friend void clocks(ProcedureRobot* );
    friend class ClockGen;

protected:

    char *Name;
    int PrrStatus;
    ORCTHR_ID PrrId;
    ORCSEM_ID SemWaitEnd;
    Parameters *GlobState;          // Table of symbol for manage data
    // state
    ListRobotTask *ListRT;          // RTs list used in PrR
    Canal *AutomatonInterf;         // Manage Interface Asynchrone/
    // Synchrone
    ClockGen *Clock;               // Clock Generator
    DataDebug *DataStorage;         // Store Datas coming from ports
    EventDebug *EventStorage;       // Store Events and Errors
    int Number;                     // Number of RTs
    char OrccadPath[MAXFILENAME];   // Orccad user Path
    double *State;                  // Global State - RPG Modif 24/02/2000

public:
    // Constructor/Destructor
    ProcedureRobot();
    ProcedureRobot(char *name, int nbparams,
                   FUNCPTR autotransite, FUNCPTR autoreset,
                   FUNCPTR *tabinputfunc,
                   char tabevent[MAXTABINEVENT][MAXCHARNAME], const int nbsignals, double *tabclks, int nbclks, int mode);
    ~ProcedureRobot();
    void SetUp(char *name, int nbparams,
               FUNCPTR autotransite, FUNCPTR autoreset,
               FUNCPTR *tabinputfunc,
               char tabevent[MAXTABINEVENT][MAXCHARNAME], const int nbsignals,
               double *tabclks, int nbclks, int mode);
    char* GetName()
    {
        return Name;
    }
    // Delete real-time tasks
    void DeleteTasks();
    // insert a RT in PrR
    int insertRT(RobotTask *RT, FUNCPTR func);
    // get the first RT of the list
    RobotTask* GetFirstRT();
    void StartAutomaton();
    // installation of horloge interrupt with period
    void initHl();
    // Pointers passed to RTs
    Canal* GetCanal()
    {
        return AutomatonInterf;
    }
    DataDebug* GetDataDebug()
    {
        return DataStorage ;
    }
    EventDebug* GetEventDebug()
    {
        return EventStorage;
    }
    Parameters* GetParameters()
    {
        return GlobState;
    }
    ClockGen* GetClock()
    {
        return Clock;
    }
    void EmitEnd();
    void EmitUrgentEnd();
    void WaitEnd();
    void Reset();
    void Abort();
    void Destroy_All();
    void SetTaskId(ORCTHR_ID id);
    int GetStatus()
    {
        return PrrStatus;
    }
    char* GetPath();
    void SetPath(char*);
};



#endif

