// *******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/linuxNPTLOrcc.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
// ***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime for Linux 2.6
//            abstract type, define
//
// **************************** Modifications **********************************
//
// Author         : Soraya Arias
// Date           : 24 June 2004
//
// This file provides an interface between all Orccad system call and
// Linux 2.6 Kernel (integrating the native posix thread library and
// message queues)
//
// BEWARE: timer resolution grain is 1ms in this version !
//
// *****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
// *****************************************************************************
#ifndef __linuxMRxenomaiorcc_h
#define __linuxMRxenomaiorcc_h

/////////////////////////
/// Linux include
/////////////////////////
#include <features.h>
#include <values.h>
#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <math.h>
#include <errno.h>
#include <signal.h>
#include <fcntl.h>
#include <semaphore.h>
#include <pthread.h>
#include <sched.h>
#include <getopt.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>
#include <sys/io.h> /* pour glibc */
#include <sys/time.h>
#include <sys/mman.h>
#include <bits/siginfo.h>
/////////////////////////
/// Xenomai Native include
/////////////////////////
#include <native/types.h>
#include <native/task.h>
#include <native/queue.h>
//#include <native/intr.h>
#include <native/timer.h>
#include <native/sem.h>
#include <native/mutex.h>
#include <native/alarm.h>
#include <native/heap.h>
#include <native/queue.h>
#include <native/syscall.h>
/////////////////////////
/// Define
/////////////////////////
typedef void (*FUNCPTR) (...);  /**< General purpose pointer to function */
typedef void  (*SOSO) (void *); /**< Special type to be used for casting function in rt_task_create() */


#define OK 0
#define ERROR -1
#define STATUS int

#define FALSE 0
#define TRUE 1

/**
 * Max number of characters for file name
 * This number is based on _POSIX_PATH_MAX value (see posix1_lim.h and see limits.h the
 * ifdef __USE_POSIX statement)
 *
 * @see <posix1_lim.h>#_POSIX_PATH_MAX
 * @see <limits.h>
 */
#define MAXFILENAME _POSIX_PATH_MAX +1


/*-------Message Queues----------*/
/**
 * Message queue Id structure
 */
typedef struct msgQId
{
    int msgId; /**< Message queue id */
    key_t key; /**< Key */
    //  char msgName[256];
}
orcmsgQ_t;

/**
 * Message buffer structure used within message queue
 */
typedef struct msgBuf
{
    long mtype;     /**< Message type, must be > 0 */
    int orcmsgInfo; /**< Message data */
}
orcmsgQBuf_t;

#define ORCMSGQ RT_QUEUE *  /**< Native Xenomai Message Queue Type */
#define SIZE_QUEUE  10       /**< Size of the Message Queue */
#define SIZE_MES_MAX 4056    /**< Maximum size of a message in Message Queue */
#define SIZE_MES_MIN 128     /**< Minimum size of a message in Message Queue */
#define MQPERM IPC_CREAT | IPC_EXCL | 0666 /**< Message Queue Permission */
#define MQPRIO_MAX (255)     /**< Max Message Queue Priorities Level */
#define ORCNORMALPRI 1       /**< Message Queue Normal Priority Level */
#define ORCURGENTPRI MQPRIO_MAX /**< Message Queue Urgent Priority Level */

/*-------Semaphores----------*/
//#define ORCSEM_ID RT_SEM * /**< Native Xenomai Semaphore Type */
typedef struct sem_struct
{
    RT_SEM * sem;
    int maxcount;
}
orcSem_t;
#define ORCSEM_ID orcSem_t * /**< Semaphore Type */
#define ORCFULL  1        /**< State of a Full Binary Semaphore */
#define ORCEMPTY 0        /**< State of a Empty Binary Semaphore */
#define ORCFOREVER  TM_INFINITE    /**< Waiting forever */

#define WAIT_FOREVER TM_INFINITE /**< Define set to be compatible with previous OS */
#define ORCMUTEX_ID RT_MUTEX *  /**< Native Xenomai Mutex Type */
/*-------Tasks----------*/
#define ORCTHR_ID RT_TASK *    /**< Native Xenomai Thread Type */
#define STACKSIZE int         /**< Stack size Type */
#define SCHED_OTHER             0
#define SCHED_FIFO              1
#define SCHED_RR                2
#define SCHEDPOLICY SCHED_FIFO  /**< Scheduling policy (see schedbits.h, sched.h )*/

#define PRI_CLK  99
#define PRI_MIN  1

#define PRI_CNL   PRI_CLK - 1 /**< Canal task priority level */
#define PRI_RT    PRI_CLK - 2 /**< Robot controller task priority level */
#define PRI_MT    (PRI_MIN + 1)  /**< Module controller task priority level */
#define PRI_PRR   PRI_MIN      /**< Procedure controller task priority level */
#define PRI_MT_MAX    (PRI_RT - 5) //5 priority levels for system tasks
#define PRI_MT_MIN   PRI_MIN
#define PRIO_PHR  (PRI_RT - 1) //a definir selon l'OS

#define SMALL_STACK  1000           /**< Small task stack size PTHREAD_STACK_MIN not defined */
#define NORMAL_STACK 5*SMALL_STACK  /**< Normal task stack size */
#define BIG_STACK    15*SMALL_STACK /**< Big task stack size */

/*-------Timers----------*/
/**
 * Timer structure definition
 */
class ModuleTask; //timers are linked to some Mt...

typedef struct timer
{
  RT_ALARM AlarmClk;  /**< Alarm based timer */
  ORCTHR_ID AlarmHandler; /**< thread to handle the alarm */
  double period;  /**< period in seconds */
  RTIME ns_delay; /**< starting delay in nanoseconds*/
  RTIME ns_period; /**< Periodic delay in nanoseconds*/
  ModuleTask * mt; /**< module task triggered by this timer */
} orcTimer_t;

#define ORCTIMER_ID orcTimer_t * /**< Orccad Timer Type */
#define ORC_CLOCK CLOCK_MONOTONIC /**<Linux monotonic Wall Clock */
#define TIME_UNIT 1000000 /**< Time unit = microsecond */
/* #define TIME_RES  10000  */  /**< Linux Timer Resolution = 10 ms with 2.4.x kernel*/
//#define TIME_RES  1000   /**< Linux Timer Resolution = 1 ms with 2.6.x kernel */
#define TIME_RES  1   /**< High Res. Timer Resolution = 1 us with 2.x.x-hrt kernel */
#define NSEC_PER_SEC 1000000000 //already define in posix_timers.h
#define MAXTIMERS 100 /**< Max. number of timers */
#define MINPERIOD 0.0001  /**< Minimum period in seconds */

extern struct timespec clock_resolution;
extern int toto;
extern struct timespec orctime, oldtime, inittime;
extern int crexec, cr;
extern RTIME exectime;
extern ORCTHR_ID MAIN_RT;
extern RT_TASK_INFO verifinfo, delinfo, execinfo;
extern RT_SEM_INFO seminfo;
extern ORCTIMER_ID AsyncTimer[MAXTIMERS];
/*-------static variables----------*/
extern int message_counter , end_base_clock , ji ;
extern long long startime, now;
extern long long hjitter, jinc, max, moy, drift, jitter;
extern int cpt_sem, cpt_mutex ;
extern char nameSem[10], nameMutex[10], nameAlarm[10];
extern int Timer_exists;
extern int NbHClocks;
/*-------Message Queues----------*/
/**
 * Function to create a message queue
 *
 * @param
 * @return ORCMSGQ pointer if message queue created, NULL otherwise and errno is set
 * @see ORCMSGQ
 * @see <sys/msg.h>#msgget
 * @see <sys/msg.h>#msgctl
 */
inline int orcMsgQCreate(ORCMSGQ *orcMsgQ)
{
    size_t size;
    //ORCMSGQ OrcQueue;
    //RT_QUEUE *orcMsgQ;
    message_counter = 0;
    size = SIZE_MES_MAX;
    int err;

    *orcMsgQ = new RT_QUEUE;
    /*     if(rt_queue_bind(*orcMsgQ, "OrcRTQ", TM_NONBLOCK) == 0) */
    /*       rt_queue_delete(*orcMsgQ); */
    if ((err = rt_queue_create(*orcMsgQ, "OrcRTQ", SIZE_MES_MAX * SIZE_QUEUE, SIZE_QUEUE, Q_FIFO | Q_SHARED)) != 0)
    {
        printf("orcMsgQCreate ERROR %d \n", err);
    }
    else printf("orcMsgQCreate OK\n");
    return err;
}

/**
 * Function to get the number of messages pending in a message queue
 *
 * @param queue pointer to the message queue
 * @return long number of messages if no error occurs otherwise ERROR and errno is set
 * @see <sys/msg.h>#msgctl
 */
inline long orcMsgQNumMsgs(ORCMSGQ queue)
{
    return (long)message_counter;
}
/**
 * Function to send a message on a message queue
 * Sending the message is done according to a priority level such as :
 *
 * @param queue pointer to the message queue
 * @param msg message to be sent
 * @param prio message priority level
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <sys/msg.h>#msgsnd
 */
inline int orcMsgQSend(ORCMSGQ OrcQueue, int msg, int prio)
{
    int i;
    void * tmpbuf;
    size_t orcEventSize;
    orcEventSize = (size_t)sizeof(int);
    if ((tmpbuf = rt_queue_alloc(OrcQueue, orcEventSize)) == 0)
    {
        printf("ERROR orcMsgQSend tmpbuf not created \n");
        return ERROR;
    }
    memcpy(tmpbuf, &msg, orcEventSize);
    if ((i = rt_queue_send(OrcQueue, tmpbuf, orcEventSize, Q_NORMAL)) < 0)
    {
        printf("ERROR orcMsgQSend %d \n", i);
        return ERROR;
    }
    message_counter++;
    return OK;
}
/**
 * Function to receive messages on a message queue
 *
 * @param queue pointer to the message queue
 * @param msg pointer to the received message
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <sys/msg.h>#msgrcv
 *
 */
inline int orcMsgQReceive(ORCMSGQ OrcQueue, int *msg)
{
    ssize_t i;
    void *message;

    if ((i = rt_queue_receive(OrcQueue, &message, ORCFOREVER )) < 0)
    {
        printf("ERREUR orcMsgQReceive %d \n", i);
        return ERROR;
    }
    else
    {
        memcpy(msg, message, i);
        rt_queue_free(OrcQueue, message);
        message_counter--;
        return OK;
    }
}

/**
 * Function to close and erase a message queue
 *
 * @param queue pointer to the message queue
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see <sys/msg.h>#msgctl
 */
inline int orcMsgQClose(ORCMSGQ OrcQueue)

{
    int i;
    if (OrcQueue == 0)
        return ERROR;
    if ((i = rt_queue_delete(OrcQueue)) != 0)
    {
        printf("ERREUR orcMsgQClose %d \n", i);
        return ERROR;
    }

    else
    {
        printf("orcMsgQ Closed \n");
        delete(OrcQueue);
        return OK;
    }
}


/*-------Semaphores----------*/
/**
 * Function to create a semaphore
 *
 * @param initState semaphore initial creation state, eg ORCFULL, ORCEMPTY
 * @return ORCSEM_ID semaphore pointer created if successfull, NULL otherwise
 * @see ORCFULL
 * @see ORCEMPTY
 * @see <semaphore.h>#sem_init
 */
inline ORCSEM_ID orcSemCreate(int initState, int maxcount)
{
    ORCSEM_ID newSem;
    newSem = new orcSem_t;
    newSem->sem = new RT_SEM;
    newSem->maxcount = 0;
    int status;
    sprintf(nameSem, "%d", cpt_sem);

    /*     if(rt_sem_bind(newSem, nameSem, TM_NONBLOCK) == 0) */
    /*       rt_sem_delete(newSem); */

    if (initState > maxcount)
    {
        initState = maxcount;
        printf("waring, trying to initialize semaphore above maxcount \n");
    }

    status = rt_sem_create(newSem->sem, nameSem, initState, S_FIFO);
    newSem->maxcount = maxcount;

    if (status == OK)
    {
#ifdef DEBUG
        printf("orcSemCreate %p name %s\n", newSem, nameSem);
#endif
        cpt_sem++;
        return newSem;
    }
    else
    {
#ifdef DEBUG
        printf("orcSemCreate ERROR %d\n", status);
#endif
        return NULL;
    }
}
inline ORCSEM_ID orcSemCreate_Fugace(void)
{
    ORCSEM_ID newSem;
    newSem = new orcSem_t;
    newSem->sem = new RT_SEM;
    newSem->maxcount = 0;
    int status;
    sprintf(nameSem, "%d", cpt_sem);

    /*     if(rt_sem_bind(newSem, nameSem, TM_NONBLOCK) == 0) */
    /*       rt_sem_delete(newSem); */

    status = rt_sem_create(newSem->sem, nameSem, 0, S_PULSE);

    if (status == OK)
    {
#ifdef DEBUG
        printf("orcSemCreate %p name %s\n", newSem, nameSem);
#endif
        cpt_sem++;
        return newSem;
    }
    else
    {
#ifdef DEBUG
        printf("orcSemCreate ERROR %d\n", status);
#endif
        return NULL;
    }
}

inline ORCSEM_ID orcSemCreate_count(int initState, int maxcount)
{
    return orcSemCreate(initState,maxcount);
}

/**
 * Function to delete a given semaphore
 *
 * @param sem semaphore to be deleted
 * @return sem_destroy() returned value
 * @see <semaphore.h>#sem_destroy
 */
inline int orcSemDelete(ORCSEM_ID sem)
{
    int cr;
#ifdef DEBUG
    RT_SEM_INFO seminfo;
#endif
    if (sem == 0)
    {
        printf("ERROR orcSemDelete %p \n", sem);
        return ERROR;
    }
#ifdef DEBUG
    rt_sem_inquire(sem->sem, &seminfo);
#endif
    cr = rt_sem_delete(sem->sem);
    if (cr == 0)
    {
        delete sem;
#ifdef DEBUG
        printf("orcSem Deleted %p %s\n", sem,seminfo.name);
#endif
        return OK;
    }
    printf("ERREUR %d during orcSemDelete %p \n", cr, sem);
    return ERROR;
}

/**
 * Function to give a semaphore
 *
 * @param sem semaphore
 * @return sem_post) returned value
 * @see <semaphore.h>#sem_post
 */
inline int orcSemGive(ORCSEM_ID sem)
{
    //int res = OK;
    //  spl_t s;
#ifdef DEBUG
    printf("SemGive %p\n", sem);
#endif
    //xnlock_get_irqsave(&nklock, s);
    rt_sem_inquire(sem->sem, &seminfo);

    if (seminfo.count < sem->maxcount)
        return rt_sem_v(sem->sem);
    else printf("trying to overload semaphore \n");
    //xnlock_put_irqrestore(&nklock, s);
    return OK;
    //return (rt_sem_v(sem));
}

/**
 * Function to take a semaphore
 * TODO : no timeout in Posix semaphores
 *
 * @param sem semaphore
 * @param timeout value
 * @return sem_wait() returned value
 * @see <semaphore.h>#sem_wait
 */
inline int orcSemTake(ORCSEM_ID sem, int timeout)
{
#ifdef DEBUG
    printf("SemTake %p\n", sem);
#endif
    return (rt_sem_p(sem->sem, timeout));
}

/*-------Mutex----------*/
/**
 * Function to create a mutex in unlocked state
 *
 * @param initState semaphore initial creation state, eg ORCFULL, ORCEMPTY
 * @return ORCMUTEX_ID mutex pointer created if successfull, NULL otherwise
 * @see <pthread.h>#pthread_mutex_init
 */
inline ORCMUTEX_ID orcMutexCreate()
{
    ORCMUTEX_ID newMutex;
    newMutex = new RT_MUTEX;
    int status;
    sprintf(nameMutex, "%d", cpt_mutex);

    /*     if(rt_mutex_bind(newMutex, nameMutex, TM_NONBLOCK) == 0) */
    /*       rt_mutex_delete(newMutex); */

    status = rt_mutex_create(newMutex, nameMutex);
    if (status == OK)
    {
#ifdef DEBUG
        printf("orcMutexCreate_count %p\n", newMutex);
#endif
        cpt_mutex++;
        return newMutex;
    }
    else
    {
#ifdef DEBUG
        printf("orcMutexCreate_count ERROR %d \n", status);
#endif
        return NULL;
    }
}
/**
 * Function to delete a given mutex
 *
 * @param mutex to be deleted
 * @return pthread_mutex_destroy returned value
 * @see <pthread.h>#pthread_mutex_destroy
 */
inline int orcMutexDelete(ORCMUTEX_ID mutex)
{
#ifdef _DEBUG
    printf("MutexDelete %p\n", mutex);
#endif
    return (rt_mutex_delete(mutex));
}
/**
 * Function to lock a mutex
 *
 * @param mutex
 * @return pthread_mutex_lock() returned value
 * @see <pthread.h>#pthread_mutex_lock
 */
inline int orcMutexLock(ORCMUTEX_ID mutex)
{
#ifdef _DEBUG
    printf("MutexLock %p\n", mutex);
#endif
    return (rt_mutex_acquire(mutex,ORCFOREVER));
}
/**
 * Function to unlock a mutex
 *
 * @param mutex
 * @return pthread_mutex_unlock() returned value
 * @see <pthread.h>#pthread_mutex_lock
 */
inline int orcMutexUnlock(ORCMUTEX_ID mutex)
{
#ifdef _DEBUG
    printf("MutexUnlock %p\n", mutex);
#endif
    return (rt_mutex_release(mutex));
}

/*-------Tasks----------*/
/**
 * Function for suspending current process
 * TODO : no taskDelay in Posix (sleep uses seconds, delay ?, nanosleep ?)
 *
 * @param delay
 * @return OK
 */
inline int orcTaskDelay(int delay)
{
    int cr;
    if ((cr = rt_task_sleep(rt_timer_ns2ticks(delay))) == 0)return OK;
    else
    {
        printf("rt_task_sleep ERROR %d \n", cr);
        return ERROR;
    }
}

/// Function to set relative linux task priority
inline int orcScalePriority(int os_priority)
{
    if ((PRI_MT_MIN + os_priority < PRI_MT_MAX) && os_priority > 0)
        return ( PRI_MT_MIN + os_priority );
    printf("ERROR orcScalePriority, priority=%d\n", os_priority);
    return PRI_MT_MAX;
}
/**
 * Function to check if a task is still valid or not
 *
 * @param Id task identifier
 * @return OK if task is valid, ERROR otherwise
 * @see ORCTHR_ID
 * @see rt_task_inquire
 */
inline int orcTaskIdVerify(ORCTHR_ID Id)
{
    int cr;
    //RT_TASK_INFO verifinfo;
    cr = rt_task_inquire(Id,&verifinfo);
    if (cr == 0)
    {
        return OK;
    }
    printf("orcTaskIdVerify %p ERROR %d\n", Id, cr);
    return ERROR;
}

/**
 * Function to kill a task
 *
 * @param Id task identifier
 * @return OK on success and ERROR otherwise
 * @see ORCTHR_ID
 * @see rt_task_inquire
 * @see rt_task_delete
 */
inline int orcTaskDelete(ORCTHR_ID Id)
{
    int result;

    if (Id == 0)
    {
        printf("ERROR orcTaskDelete, Id == 0\n");
        return ERROR;
    }

    result = rt_task_inquire(Id,&delinfo);
    if (result == 0)
    {
#ifdef DEBUG
        printf("DELETING TASK %s \n", delinfo.name);
#endif

        result = rt_task_delete(Id);
        delete Id;
        if (result != 0)
        {
            printf("ERROR rt_task_delete  %p error %d\n",Id,result);
            return ERROR;
        }
	//#ifdef DEBUG
        printf("TASK %s DELETED \n", delinfo.name);
	//#endif
        return OK;
    }
    else
    {
        printf("orcTaskDelete %p rt_task_inquire ERROR %d \n", Id, result);
        return ERROR;
    }
}


/**
 * Function to spawn a rt_task on CPU(i)
 *
 * @param tid task identifier
 * @param name name descripting the task
 * @param prio task priority level 
 * @param stackSize stack size to be allocated for the task
 * @param ptr to routine to be spawn by the task (cast to SOSO)
 * @param arg argument needed for the routine to be spawn
 * @return OK if success and ERROR otherwise
 * @see ORCTHR_ID
 * @see STACKSIZE
 * @see SOSO
 * @see rt_task_spawn
 */
inline int orcSpawn(ORCTHR_ID *tid, const char *name, int prio, STACKSIZE stackSize, SOSO funcptr, void * arg)
{
    int err;

    *tid = new RT_TASK;

    if (rt_task_bind(*tid, name, TM_NONBLOCK) == 0)
        rt_task_delete(*tid);

    if ((err = rt_task_spawn(*tid, name, stackSize, prio, T_FPU | T_JOINABLE, (SOSO) funcptr, arg)) != 0)
    {
        printf("ERROR %d rt_task_spawn %s \n", err, name);
        return ERROR;
    }
    printf("RT_TASK SPAWNED %s prio %d StackSize %d Id %p \n", name, prio, stackSize, *tid);

    return OK;
}

inline int orcSpawn0(ORCTHR_ID *tid, const char *name, int prio, STACKSIZE stackSize, SOSO funcptr, void * arg)
{
    int err;

    *tid = new RT_TASK;

    if (rt_task_bind(*tid, name, TM_NONBLOCK) == 0)
        rt_task_delete(*tid);

    if ((err = rt_task_spawn(*tid, name, stackSize, prio, T_FPU | T_JOINABLE | T_CPU(0), (SOSO) funcptr, arg)) != 0)
    {
        printf("ERROR %d rt_task_spawn %s \n", err, name);
        return ERROR;
    }
    printf("RT_TASK SPAWNED %s prio %d StackSize %d Id %p \n", name, prio, stackSize, *tid);

    return OK;
}

inline int orcSpawn1(ORCTHR_ID *tid, const char *name, int prio, STACKSIZE stackSize, SOSO funcptr, void * arg)
{
    int err;

    *tid = new RT_TASK;

    if (rt_task_bind(*tid, name, TM_NONBLOCK) == 0)
        rt_task_delete(*tid);

    if ((err = rt_task_spawn(*tid, name, stackSize, prio, T_FPU | T_JOINABLE | T_CPU(1), (SOSO) funcptr, arg)) != 0)
    {
        printf("ERROR %d rt_task_spawn %s \n", err, name);
        return ERROR;
    }
    printf("RT_TASK SPAWNED %s prio %d StackSize %d Id %p \n", name, prio, stackSize, *tid);

    return OK;
}

/**
 * Function to lock a task
 * This function does nothing : defined for interoperability between OS needs
 *
 * @param
 * @return OK
 */
inline int orcTaskLock()
{
    return OK;
}

/**
 * Function to unlock a task
 * This function does nothing : defined for interoperability between OS needs
 *
 * @param
 * @return OK
 */
inline int orcTaskUnlock()
{
    return OK;
}

inline int orcTaskSuspend(ORCTHR_ID task)
{
    return rt_task_suspend(task);
}

/**
 * Function to wait for the termination of a task
 *
 * @param task to wait for
 * @return OK on success
 * @see rt_task_join
 */
inline int orcTaskJoin(ORCTHR_ID task)
{
    int cr;
#ifdef DEBUG
    printf("waiting for orcTaskJoin %p \n", task);
#endif
    cr = rt_task_join(task);
    if (cr != 0)
    {
        printf("rt_task_join error %d on task %p \n", cr, task);
        return cr;
    }
#ifdef DEBUG
    printf("rt_task_join returns OK \n");
#endif
    return OK;
}


/*-------Timers----------*/
/**
 * Timer handler function
 *
 * @param int
 * @return
 */
void orcAlarm_handler(ModuleTask *);


/**
 * Function to mask all signals but SIGINT and SIGXCPU 
 *
 * @return OK if successful, ERROR otherwise and errno is set
 * @see <signal.h>#sigprocmask
 * @see <signal.h>#sigaddset
 */
inline int orcTimerSigMask()
{
    sigset_t set ;
    sigfillset(&set);
    if (pthread_sigmask(SIG_BLOCK, &set, NULL) != OK)
    {
        printf("pthread_sigmask Failed %d \n", errno);
        return ERROR;
    }
    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    sigaddset(&set, SIGXCPU);
    if (pthread_sigmask(SIG_UNBLOCK, &set, NULL) != OK)
    {
        printf("pthread_sigmask Failed %d \n", errno);
        return ERROR;
    }

    return OK;
}

/**
 * Function to un-mask timer signal and mask all others
 *
 * does nothing, compatibility with Posix systems if any call
 * @return OK 
 */
inline int orcTimerSigUnMask()
{
    return OK;
}

/**
 * Function to set the timer settings
 * This function sets in particular the period for the timer
 *
 * @param index of the timer
 * @param period value to be set for timer
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see TIME_UNIT
 * @see TIME_RES
 */
inline int orcTimerSetTime(int i, double period)
{
    /* init time measure */
    if (clock_gettime(ORC_CLOCK, &inittime) == -1)
    {
        printf("clock_gettime error \n");
        exit(1);
    }
    else printf("init-time %ld sec %ld ns\n", inittime.tv_sec, inittime.tv_nsec);
    //check period validity
    if(period < MINPERIOD) period = MINPERIOD;

    AsyncTimer[i]->ns_period = (RTIME)(period * NSEC_PER_SEC);
    AsyncTimer[i]->ns_delay = (RTIME)0.0;

    /*     if (ioperm(0x378, 3, 1)) */
    /*     { */
    /*         // error */
    /*         printf("IOPERM error\n"); */
    /*     } */

    return OK;
}
/**
 * Function to reset the timer settings
 * This function sets in the period and restarting delay for timer[i]
 *
 * @param index of the timer
 * @param period in seconds value to be set for timer
 * @param restarting delay
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see timespec structure
 */

inline int orcTimerResetTime(int i, double period, long long ns_delay)
{
    //check period validity
    if(period < MINPERIOD) period = MINPERIOD;

    AsyncTimer[i]->ns_period = (RTIME)(period * NSEC_PER_SEC);
    AsyncTimer[i]->ns_delay = (RTIME)(ns_delay);

    return OK;
}

/**
 * Function to restart ith periodic timer[i]
 *
 * @param
 * @return OK if successfull, ERROR otherwise
 * @see ORCSIGTIMER
 * @see <signal.h>#sigismember
 * @see <pthread.h>#pthread_sigmask
 */
inline int orcTimerReset(int i)
{
    /* rearming the ith timer */

  if (rt_alarm_start(&(AsyncTimer[i]->AlarmClk),
			 rt_timer_ns2ticks(AsyncTimer[i]->ns_delay),
			 rt_timer_ns2ticks(AsyncTimer[i]->ns_period)) != 0)
    {
      printf("AsyncTimer[%d]Reset:: failed errno = %d \n", i, errno);
        return ERROR;
    }
  //printf("AsyncTimer[%d]Reset \n", i);
    return OK;
}

/**
 * Function to create a periodic timer
 * This function sets a periodic timer,
 * the corresponding signal  and the timer handler
 * and the orcTimer structure is affected
 *
 * @param period value to be set for timer
 * @return OK if successfull, ERROR otherwise and errno is set
 * @see ORCTIMER_ID
 * @see orcTimerSetTime
 * @see orcTimer
 * @see <timer.h>#timer_create
 */

extern int orcTimerInit(int, ModuleTask *);


/**
 * Function to delete/erase the timers
 * This function disabled the periodic timer and erase the AsyncTimer structure
 *
 * @param
 * @return OK if successfull, ERROR otherwise
 * @see orcTimer
 * @see <timer.h>#timer_delete
 * @see <signal.h>#sigemptyset
 * @see <signal.h>#sigprocmask
 */
extern int orcTimerDelete(void);

/**
 * Function to set the periodic timer handler
 *
 * @param func timer handler function pointer
 * @param arg argument for the timer handler function
 * @return OK
 */

extern int orcTimerArm(void);

/**
 * Function to launch the periodic timer
 *
 * @param
 * @return OK if successfull, ERROR otherwise
 * @see ORCSIGTIMER
 * @see <signal.h>#sigismember
 * @see <pthread.h>#pthread_sigmask
 */
extern int orcTimerLaunchAll(void);

/* -------- MISC -------- */
/**
 * Function to check a given path
 * TODO : MAXFILENAME is set to 80 in util.h, it should be PATH_MAX+1
 *
 * @param path pathname to retrieve
 * @return OK if successfull, ERROR otherwise
 * @see MAXFILENAME
 */
inline int orcGetPath(char *path)
{
    if (getcwd(path, MAXFILENAME) != OK)
        return ERROR;
    return OK;
}

/**
 * Function to set a directory path
 *
 * @param path name pf the directory path
 * @return chdir() returned value
 * @see chdir
 */
inline int orcSetPath(const char *path)
{
    return chdir(path);
}

/**
 * Function to spy events
 * Function does nothing but defined for interoperability need between OS
 *
 * @param eventid event identifier to spy
 * @param buffer
 * @param bufsize
 * @return OK
 */
inline int orcSpyEvent(int eventid, char* buffer, int bufsize)
{
    return OK;
};
/* Functions to have very accurate time measurements */
// (for feedback scheduling. Not implemented yet in linux)

///Function that returns the Cpu time in nanoseconds
inline long long orcGetCpuTime(void)
{
    return (long long)rt_timer_ticks2ns(rt_timer_read());
}

///Function that returns the task exec time in nanoseconds
inline long long orcGetExecTime(ORCTHR_ID task)
{
    //RT_TASK_INFO execinfo;
    crexec = rt_task_inquire(task,&execinfo);
    if (crexec != 0)
    {
        printf(" orcGetExecTime error %d \n", crexec);
        return (long long)0;
    }
    else return (long long)execinfo.exectime;
}

///Function that returns the task start time in nanoseconds
inline long long orcGetStartTime(ORCTHR_ID task)
{
    return (long long)0;
}

///Function that makes the main thread real-time compliant
inline int orcMakeRealTime(void)
{
    int prio_main = PRI_MIN;
    mlockall(MCL_CURRENT | MCL_FUTURE);
    if ((cr = rt_task_shadow(MAIN_RT, "MAIN_PRR", prio_main, T_FPU | T_JOINABLE)) != 0)
    {
        printf("rt_task_shadow error %d \n", cr);
    }
    else
    {
        printf("rt_task_shadow done priority %d \n", prio_main);
    }
    return cr;
}


#define timerdiff(a,b) ((long)((a)->tv_sec - (b)->tv_sec)*NSEC_PER_SEC + (long)((a)->tv_nsec - (b)->tv_nsec))


#endif
