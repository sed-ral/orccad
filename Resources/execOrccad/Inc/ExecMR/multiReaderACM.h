/**
 * @file ${ORCCARD_SRC}/Inc/Exec/multiReaderACM.h
 * @brief Asynchronous communication mechanism for a multi-reader-single-writer design
 * @see 'Multireader and multiwriter asynchronous communication mechanisms', H.R Simpson, IEE Proc.-Comput. Digit. Tech, Vol.144, No. 4, July 1997.
 * @author James Lain� - james.laine@caramail.com
 * @date 15/08/2001
 */


#ifndef __multiReaderACM_h
#define __multiReaderACM_h

#define mr_DEBUG

#include <iostream.h>

/**
 * @class MultiReaderACM
 * @brief Asynchronous communication mechanism for a multi-reader-single-writer design
 *
 * This Asynchronous communication mechanism enables a 'loosely synchronous'
 * (compared to mutual exclusion with semaphores)
 * , potentially fully asynchronous.
 * The aim of the mechanism implement in ORCCARD is to ensure data protection and to not introduce new synchronizations in MA controllers, sources of deadlocks.
 *
 * This class is a improved implementation of multi-reader-single-writer ACM [Simpson97].
 * Data storage needs 2(nbReaders+1) slots that represents 2(nbReaders+1)*sizeof(<DataType>) Ko.
 *
 * @test define mr_DEBUG to have a multireader ACM manipulation trace
 * @todo When compiler will allow the use of C++ keyword export on a template class (I used g++ --version=egcs-2.91.66), use it in generated code and compile this file with ORCCAD runtime
 * @see class SingleReader and 'Multireader and multiwriter asynchronous communication mechanisms', H.R Simpson, IEE Proc.-Comput. Digit. Tech, Vol.144, No. 4, July 1997.
 */


template <class DataType> class MultiReaderACM
{
protected:
    /// number of readers sharing MultiReaderACM instance
    int _readersNumber;

    /**
      * @brief (2*readersNumber+2)-slots array, arranged as two group of slots, to store and access data value
      *
      * _data[i][j] is a type(int, float, double) represents by enum OrcVariableType( ${ORCCARD_SRC}/Inc/Kernel/Ityp.h).
      *
      * i (0..1)represents the index of the slots group:
      * @arg not potentially in readind if writing
      * @arg not potentially in writing if reading
      *
      * j (0..readersNumber-1) represents the slot index in slots group i which :
      * @arg NOT containing last written data what can be in reading if writing
      * @arg containing last written data if reading
      *
      * Data storage represents 2(nbReaders+1)*sizeof(<DataType>) Ko. 
      */
    DataType** _data;

    /** @brief the reading strategie array
      *
      * Let us consider :
      * @arg i the index of the module what wants to read (0..readersNumber-1)
      * @arg j the index of the last slots group written (0..1)
      *
      * So, _reading[i][j] is the index of the last written data in slots group j
      */
    int** _reading;

    /** @brief the writing strategie array
      *
      * _writing[i] is, if i is the index of the last slots group written, the index of :
      * @arg the slot to be written if writing
      * @arg the slot to be read if reading
      */
    int* _writing;

    /// indicates the last slots group written
    mybool _slotsGroup;


public:
    /**
     *  @brief allocates ans presets attributes
     *  @param int readersNumber : the number of readers sharing MultiReaderACM instance
     */
    MultiReaderACM(int readersNumber)
    {

        _readersNumber = readersNumber;

        _data = new DataType*[2];
        _reading = new int*[_readersNumber];
        _writing = new int[2];

        int i;

        for ( i=0; i<2; i++)
            _data[i] = new DataType[_readersNumber+1];

        for ( i=0; i<_readersNumber; i++)
            _reading[i] = new int[2];

        _slotsGroup = 0;
    };

    /**
     *  @brief desallocates _data, _reading and _writing
     */
    ~MultiReaderACM()
    {

        int i;

        for ( i=0; i<2; i++)
            delete[] _data[i];

        for ( i=0; i<_readersNumber; i++)
            delete[] _reading[i];

        delete[] _data;
        delete[] _reading;
        delete[] _writing;
    };


    /**
    *  @brief emulates the NOT function for the choice of a not in reading slot
    *
    *  in each slots group, there is _readersNumber+1 slots.
    *  So, in the worst case, there is still one slot not in reading.
    *  For the slots group in reading, this function examine each inex of slots potentially in reading in order find (and return) the index of a free slot for writing.  
    *  @param none
    *  @return the index of the worst case not in reading slot in slots group to be written or -1 if it has failled
    */
    int findFreeSlot(void)
    {

        mybool slotFound;

        for (int freeSlot=0; freeSlot<_readersNumber+1; freeSlot++)
        {
            slotFound = true;

            for (int readerIndex=0; readerIndex<_readersNumber; readerIndex++)
            {
                if ( _reading[readerIndex][_slotsGroup] == freeSlot )
                {
                    // slot potentially in reading -> next slot index in group
                    slotFound = false;
                    break;
                }
            }

            if ( slotFound )
                return freeSlot;
        }
        return -1;
    };


    /**
     *  @brief the multi-reader-single-writer-ACM write method
     *  @param <class DataType>* item : a pointer refering to the data to be written
     *  @return void
     *  @see 'Multireader and multiwriter asynchronous communication mechanisms', H.R Simpson, IEE Proc.-Comput. Digit. Tech, Vol.144, No. 4, July 1997.
     *  @warning I'm not agree with Simpson's software form (fig. 2). write method do not offer the latest data but the previous one. Refer you to algebraic form
     */
    void write(DataType* item )
    {

        _data[_slotsGroup][_writing[_slotsGroup]] = *item;

#ifdef mr_DEBUG
        cout << "[mr] Writes " << *item << " in group " << _slotsGroup
        << ",slot " << _writing[_slotsGroup] << endl;
#endif

        _slotsGroup = !_slotsGroup;
        _writing[_slotsGroup] = findFreeSlot();
    };


    /**
     *  @brief the multi-reader-single-writer-ACM read method
     *  @param int readerIndex : the index of the reader module (link OrcModuleAlgo::findReaderIndex(OrcRobotTask*))
     *  @return the latest written data
     *  @see 'Multireader and multiwriter asynchronous communication mechanisms', H.R Simpson, IEE Proc.-Comput. Digit. Tech, Vol.144, No. 4, July 1997. 
     */
    DataType read(int readerIndex)
    {

        mybool slotsGroup;

        _reading[readerIndex] = _writing;
        slotsGroup =! _slotsGroup;

#ifdef mr_DEBUG
        cout << "[mr#" << readerIndex << "] Reads " << _data[slotsGroup][_reading[readerIndex][slotsGroup]]
        << " in group " << slotsGroup  << ",slot " << _reading[readerIndex][slotsGroup] << endl;
#endif

        return _data[slotsGroup][_reading[readerIndex][slotsGroup]];
    };

};


#endif





