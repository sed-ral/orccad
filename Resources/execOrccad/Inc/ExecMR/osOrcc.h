//*******************************  Orccad *************************************
//
// File           : $ORCCAD_SRC/Inc/Exec/osOrcc.h
// Author         : Roger Pissard-Gibollet and Konstantin Kapellos
// Version        : 3.0 alpha
// Creation       : 6 November 1996
//
//***************************** Description ***********************************
//
//           Header for Orccad Exec Runtime
//            abstract type, define
//
//**************************** Modifications **********************************
//
// SOSO#6 09-03-00 changement de DATA_INT en DATA_INTEGER
// SOSO#step 10-04-00 ajout d'une erreur pour le mode d'execution
// SOSO#Linux 05-00 Add the Linux .C file and a generic line to be customed
//                  when a new Execution Kernel Librairy is implemented and added
// SOSO#maxdim 24-11-00 Modify Max Values relative to vector/matrix dimension
// SOSO#Nptl 24-06-04 Add Linux NPTL file
//
//*****************************************************************************
// (c) Copyright 1996, INRIA, all rights reserved
//*****************************************************************************
#ifndef __osOrcc_h
#define __osOrcc_h


#ifdef ORCSOLARIS
#include "solarisOrcc.h"
#endif

#ifdef ORCVXWORKS
#include "vxworksOrcc.h"
#endif

// SOSO#Linux vvvvv
#ifdef ORCLINUX
#include "linuxOrcc.h"
#endif

// To be customed if necessary when a new target executable system is used
// to run orccad Applications
#ifdef ORCEXECSYST
#include "myExecSystOrcc.h"
#endif
// SOSO#Linux ^^^^^

// SOSO#Nptl vvvv
#ifdef ORCLINUXNPTL
#include "linuxNPTLOrcc.h"
#endif
// SOSO#Nptl ^^^

// DS#Nptl vvvv
#ifdef ORCLINUXMRNPTL
#include "MRnptlOrcc.h"
#endif

// DS#Xenomai vvvv
#ifdef ORCLINUXMRXENO
#include "MRxenomaiOrcc.h"
#endif

// DS#Nptl vvvv
#ifdef ORCLINUXMRPPC
#include "MRppc603Orcc.h"
#endif

#endif
