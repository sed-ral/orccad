//----------------------------------------------------------
// Module jobs
//
// File : module_jobs.h
// Date of creation : 2012/05/22 16:27:07
//----------------------------------------------------------
#ifndef __orc_Mod_jobs_h
#define __orc_Mod_jobs_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"
#include "Exec/paramType.h"
/*PROTECTED REGION ID(h_include_ArmXjMove_orc_Mod_jobs) ENABLED START*/
#define MODE_SUSPEND 0
#define MODE_ACTIVE  1
//__Add include file directive here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/

class orc_RT_ArmXjMove;
class orc_Mod_jobs: public ModuleAlgo { 
protected: 
  // Internal Variables 
/*PROTECTED REGION ID(h_intern_ArmXjMove_orc_Mod_jobs) ENABLED START*/
int mode;
  //__Add local variable declaration here and delete ENABLED to regenerate it
/*PROTECTED REGION END*/
	orc_RT_ArmXjMove *pRT;
  int swtrajVar;
public:
  orc_Mod_jobs(ModuleTask *mt, double period) :
    ModuleAlgo((char *)"orc_Mod_jobs", mt, period){};
  ~orc_Mod_jobs(){};

  // Output Data Ports declaration
  
  // Output Event Ports
  int swtraj;
  int redbut;
  int outbound;
  
  //T1 Parameters declaration
  // Parameter Ports declaration			
  // Value sent by T1
  const int getT1Var() {return swtrajVar;};
  // Methods of computation
  
 /*PROTECTED REGION ID(h_methods_ArmXjMove_orc_Mod_jobs) ENABLED START*/

  //__Add custom methods here and delete ENABLED to regenerate it
 /*PROTECTED REGION END*/

  void init(const int limit[2], const int  &key); 
  void param(); 
  void reparam(); 
  void compute(const int limit[2], const int  &key); 
  void end();
};
#endif
// End of class orc_Mod_jobs
