//----------------------------------------------------------
// Module command
//
// File : module_command.h
// Date of creation : 2012/05/22 16:27:07
//----------------------------------------------------------
#ifndef __orc_Mod_command_h
#define __orc_Mod_command_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"
#include "Exec/paramType.h"
/*PROTECTED REGION ID(h_include_ArmXjMove_orc_Mod_command) ENABLED START*/


// Robot parameters
#define ARM_L1   1.0
#define ARM_L2   1.0
#define ARM_M1   10.0
#define ARM_M2   5.0
#define ARM_GRAV 9.81
// Command gain
#define KX  20.0
#define KY  20.0

//__Add include file directive here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/

class orc_RT_ArmXjMove;
class orc_Mod_command: public ModuleAlgo { 
protected: 
  // Internal Variables 
/*PROTECTED REGION ID(h_intern_ArmXjMove_orc_Mod_command) ENABLED START*/
int index;
  //__Add local variable declaration here and delete ENABLED to regenerate it
/*PROTECTED REGION END*/
	orc_RT_ArmXjMove *pRT;
public:
  orc_Mod_command(ModuleTask *mt, double period) :
    ModuleAlgo((char *)"orc_Mod_command", mt, period){};
  ~orc_Mod_command(){};

  // Output Data Ports declaration
  double torque[2];

  // Output Event Ports
  
  //T1 Parameters declaration
  // Parameter Ports declaration			
  // Value sent by T1
  // Methods of computation
  
 /*PROTECTED REGION ID(h_methods_ArmXjMove_orc_Mod_command) ENABLED START*/

  //__Add custom methods here and delete ENABLED to regenerate it
 /*PROTECTED REGION END*/

  void init(const double pose[2], const double pos[2]); 
  void param(); 
  void reparam(); 
  void compute(const double pose[2], const double pos[2]); 
  void end();
};
#endif
// End of class orc_Mod_command
