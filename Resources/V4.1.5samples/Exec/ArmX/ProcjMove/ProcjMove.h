/**
 * Execution code for Robot Procedure ProcjMove 
 * File automatically generated
 *
 * File : ProcjMove.h
 * Date of creation : 2012/05/22 16:27:08
 */
#ifndef __orc_ProcjMove_h
#define __orc_ProcjMove_h

#include "Exec/utils.h"
#include "Exec/prr.h"

#include "interface_ProcjMove.h"
#include "rts_ArmXjMove.h"
#include "PhR_WinX.h"

#define NBCLKS 1 // to be improved !
#define BASECLK  1.0 // to be improved !

class orc_ProcjMove : public ProcedureRobot
{
protected :
  // Index Tab for clocks
  double *TabClks;
  // Physical Resources
  orc_PhR_WinX *orcWinX;

public :
    // Robot Tasks
    orc_RT_ArmXjMove *ArmXjMove;
    orc_RT_ArmXjMove *GetRTArmXjMovePtr() { return ArmXjMove; };

    orc_ProcjMove();
    ~orc_ProcjMove();
    void Launch();
};
#endif
// End class orc_ProcjMove
