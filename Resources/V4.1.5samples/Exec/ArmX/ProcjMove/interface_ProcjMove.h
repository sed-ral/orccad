/**
 * Interface component to cope with events from or to STRL program
 *
 * File generated automatically
 *
 * File : interface_ProcjMove.h
 * Date of creation : 2012/05/22 16:27:08
 */
#ifndef __interface_h
#define __interface_h

#include "Exec/canal.h"

enum INPUT_SIGNALS_USER {
  ArmXjMove_Start = COMPUTETIMEOUT + 1,
  USERSTART_ArmXjMove,
  swtraj,
  badtraj,
  redbut,
  outbound,
  errtrack,
  endtraj,
  InitOK_WinX,
  ActivateOK_WinX,
  KillOK_WinX,
  CmdStopOK_WinX,
  NB_SIGNALS
};

extern "C" { int aa_ProcjMove(); }
extern "C" { int aa_ProcjMove_reset (); }
extern "C" { int prr_reset (ProcedureRobot *); }
extern "C" {
  extern FUNCPTR TABSIG[NB_SIGNALS];
  FUNCPTR aa_ProcjMove_I_ArmXjMove_Start();
  FUNCPTR aa_ProcjMove_I_USERSTART_ArmXjMove();
  FUNCPTR aa_ProcjMove_I_swtraj();
  FUNCPTR aa_ProcjMove_I_badtraj();
  FUNCPTR aa_ProcjMove_I_redbut();
  FUNCPTR aa_ProcjMove_I_outbound();
  FUNCPTR aa_ProcjMove_I_errtrack();
  FUNCPTR aa_ProcjMove_I_endtraj();
  FUNCPTR aa_ProcjMove_I_InitOK_WinX();
  FUNCPTR aa_ProcjMove_I_ActivateOK_WinX();
  FUNCPTR aa_ProcjMove_I_KillOK_WinX();
  FUNCPTR aa_ProcjMove_I_CmdStopOK_WinX();
  FUNCPTR aa_ProcjMove_I_Prr_Start(void*);
  FUNCPTR aa_ProcjMove_I_ROBOT_FAIL();
  FUNCPTR aa_ProcjMove_I_SOFT_FAIL();
  FUNCPTR aa_ProcjMove_I_SENSOR_FAIL();
  FUNCPTR aa_ProcjMove_I_CPU_OVERLOAD();
  int aa_ProcjMove_O_Abort_ArmXjMove(void *);
  int aa_ProcjMove_O_ActivateArmXjMove_WinX(void *);
  int aa_ProcjMove_O_ArmXjMoveTransite(void *);
  int aa_ProcjMove_O_Treatswtraj_ArmXjMove(void*);	
  int aa_ProcjMove_O_Activate(void*);
  int aa_ProcjMove_O_Transite(void*);
  int aa_ProcjMove_O_EndParam(void*);
  int aa_ProcjMove_O_EndKill(void*);
  int aa_ProcjMove_O_EndObs(void*);
  int aa_ProcjMove_O_FinBF(void*);
  int aa_ProcjMove_O_FinT3(void*);
  int aa_ProcjMove_O_GoodEndRPr();
  int aa_ProcjMove_O_T3RPr();
  int f_default();
} // end extern

extern char TABEVENT[NB_SIGNALS][MAXCHARNAME];
#endif
