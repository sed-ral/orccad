/**
 * Module execution handling according to the 
 * specified temporal constraints : CTArmjMove
 *
 * Orccad Version: ORCCADVERSION
 * File : mt_CTArmjMove.C
 * Date of creation : 2012/05/22 16:27:07
 */
#include "Exec/rt.h"
#include "Exec/clockgen.h"
#include "Exec/datadbg.h"
#include "rts_ArmXjMove.h"
#include "interface_ProcjMove.h"
#include "mt_CTArmjMove.h"

void orc_Mt_CTArmjMove::InitTrace()
{

}

void orc_Mt_CTArmjMove::Param()
{
}

void orc_Mt_CTArmjMove::ReParam()
{
}

int orc_Mt_CTArmjMove::SetT1Exceptions()
{
  
  
   // Give T1 info for swtraj
   jtraj_11.typtrajParam.isT1 = pRT->Getswtraj().isT1;
   jtraj_11.typtrajParam.varT1 = pRT->Getswtraj().varT1;
   // Reset T1
   pRT->aut_output_swtraj = 0;
   pRT->Putswtraj(pRT->aut_output_swtraj);



  return OK;
}
void fct_CTArmjMove(orc_Mt_CTArmjMove *mt)
{
  
  orc_RT_ArmXjMove *pRT = (orc_RT_ArmXjMove *)(mt->RT);
  
  int Current_Iteration;
  int status_rt, access_drv, do_reinit = FALSE;
  
  //Init all index
  int _mi_jtraj_11 = mt->jtraj_11.getIndexClk();
  int _mi_jobs_10 = mt->jobs_10.getIndexClk();
  int _mi_error_12 = mt->error_12.getIndexClk();
  int _mi_command_9 = mt->command_9.getIndexClk();
  while (!mt->CondKill) {
    if (mt->SemaphoreGive(Create_ok) == ERROR) 
      return;
    if (mt->SemaphoreTake(Init, WAIT_FOREVER) == ERROR)
      return;
    access_drv = (mt->RT->GetPhR())->GetState();
    
    // Begin Param
    mt->Param();
    mt->EndReparam();
    // End Param
    
    // Begin Init
    if (access_drv == PHR_ACCESS) {
      mt->WinX->GET_pos();
      mt->WinX->GET_limit();
      mt->WinX->GET_key();
      mt->WinX->GET_pos();
      mt->WinX->GET_pos();
    }
    
    mt->jtraj_11.init(mt->WinX->pos);
    
    mt->jobs_10.init(mt->WinX->limit, mt->WinX->key);
    
    mt->error_12.init(mt->jtraj_11.pos, mt->WinX->pos);
    
    mt->command_9.init(mt->error_12.errpos, mt->WinX->pos);
    
    // End Init
    // Write Port event
     
    
    if (mt->jtraj_11.badtraj == SET_EVENT) {
      (mt->EventInterface)->SendEvent(badtraj,"ArmXjMove",mt->RT->GetGlobalTime());
      mt->jtraj_11.badtraj = TAKE_EVENT;
    }
     
    
     
    
    if (mt->jobs_10.redbut == SET_EVENT) {
      (mt->EventInterface)->SendEvent(redbut,"ArmXjMove",mt->RT->GetGlobalTime());
      mt->jobs_10.redbut = TAKE_EVENT;
    }
    
    if (mt->jobs_10.outbound == SET_EVENT) {
      (mt->EventInterface)->SendEvent(outbound,"ArmXjMove",mt->RT->GetGlobalTime());
      mt->jobs_10.outbound = TAKE_EVENT;
    }
     
     
    
    if (mt->error_12.errtrack == SET_EVENT) {
      (mt->EventInterface)->SendEvent(errtrack,"ArmXjMove",mt->RT->GetGlobalTime());
      mt->error_12.errtrack = TAKE_EVENT;
    }
     
     
     
    
    //End write Port Event
      
    if (mt->SemaphoreGive(Init_ok) == ERROR)
      return;
    if (mt->SemaphoreTake(Compute,WAIT_FOREVER) == ERROR)
      return;
    Current_Iteration = 0;
    
           
    
    for (;;) {
  	  if (mt->SemaphoreTake(Synchro, WAIT_FOREVER) == ERROR) return;
      if (mt->CondEnd) break;
      status_rt =  mt->RT->GetState();
      // Take Unlock Parameters
      if (mt->IsReparam()) {
        mt->ReParam();
        mt->EndReparam();
      }
      // End re-Parameters
      if (mt->Filter_Iteration == Current_Iteration)
        mt->SemaphoreGive(Filter_ok);
      Current_Iteration++;
      access_drv = (mt->RT->GetPhR())->GetState();
      //Begin computation
      if ((access_drv == PHR_ACCESS) &&
          ((status_rt == ACTIVE) || (status_rt == TRANSITE)) &&
          (do_reinit == FALSE)) {
        mt->WinX->reinit();
        do_reinit = TRUE;
      }
      //T1 Exceptions Handling 
      mt->SetT1Exceptions();
      
      if (access_drv == PHR_ACCESS) {
        mt->WinX->GET_pos();
        mt->WinX->GET_limit();
        mt->WinX->GET_key();
        mt->WinX->GET_pos();
        mt->WinX->GET_pos();
      }
    
    //begin jtraj_11
    
    if ((mt->Clock)->IsClkActive(_mi_jtraj_11) == 0) {
      mt->jtraj_11.compute(mt->WinX->pos);
    }
    
    
    
    if (mt->jtraj_11.badtraj == SET_EVENT) {
      (mt->EventInterface)->SendEvent(badtraj,"ArmXjMove",mt->RT->GetGlobalTime());
      mt->jtraj_11.badtraj = TAKE_EVENT;
    }
    
    //end jtraj_11
    
    //begin jobs_10
    
    if ((mt->Clock)->IsClkActive(_mi_jobs_10) == 0) {
      mt->jobs_10.compute(mt->WinX->limit,mt->WinX->key);
    }
    
    if (mt->jobs_10.swtraj == SET_EVENT) {
      (mt->EventInterface)->SendEvent(swtraj,"ArmXjMove",mt->RT->GetGlobalTime());
      mt->jobs_10.swtraj = TAKE_EVENT;
     }
    
    
    
    if (mt->jobs_10.redbut == SET_EVENT) {
      (mt->EventInterface)->SendEvent(redbut,"ArmXjMove",mt->RT->GetGlobalTime());
      mt->jobs_10.redbut = TAKE_EVENT;
    }
    
    if (mt->jobs_10.outbound == SET_EVENT) {
      (mt->EventInterface)->SendEvent(outbound,"ArmXjMove",mt->RT->GetGlobalTime());
      mt->jobs_10.outbound = TAKE_EVENT;
    }
    
    //end jobs_10
    
    //begin error_12
    
    if ((mt->Clock)->IsClkActive(_mi_error_12) == 0) {
      mt->error_12.compute(mt->jtraj_11.pos,mt->WinX->pos);
    }
    
    
    
    if (mt->error_12.errtrack == SET_EVENT) {
      (mt->EventInterface)->SendEvent(errtrack,"ArmXjMove",mt->RT->GetGlobalTime());
      mt->error_12.errtrack = TAKE_EVENT;
    }
    
    //end error_12
    
    //begin command_9
    
    if ((mt->Clock)->IsClkActive(_mi_command_9) == 0) {
      mt->command_9.compute(mt->error_12.errpos,mt->WinX->pos);
    }
    
    
    
    //end command_9
    
    
    status_rt =  mt->RT->GetState();
    if ((access_drv == PHR_ACCESS) &&
        ((status_rt == ACTIVE) || (status_rt == TRANSITE))) {
      mt->WinX->PUT_torque(mt->command_9.torque);
    }
    mt->MTEndCompute();
    
    }
    // Begin end
    mt->jtraj_11.end();
    
    if (mt->jtraj_11.endtraj == SET_EVENT) {
      (mt->EventInterface)->SendEvent(endtraj,"ArmXjMove",mt->RT->GetGlobalTime());
      mt->jtraj_11.endtraj = TAKE_EVENT;
    }
    mt->jobs_10.end();
    mt->error_12.end();
    mt->command_9.end();
    // End end
    if (mt->SemaphoreGive(End_ok) == ERROR) return;
    if (mt->SemaphoreTake(Continue,WAIT_FOREVER) == ERROR) return;
    }
  if (mt->SemaphoreTake(Kill,WAIT_FOREVER) == ERROR) return;
  mt->CondKill = mt->CondEnd = mt->RequestParam = FALSE;

  if (mt->SemaphoreGive(Kill_ok) == ERROR) return;
}
