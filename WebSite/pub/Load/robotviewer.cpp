/**************************************************************************\
 *
 *  Copyright (C) 2000 by INRIA.  All rights reserved.
 *
 *  Alain Girault - Pierre-Brice Wieber
 *
 *  Compile this file with:
 *  gcc robotviewer.cpp -o robotviewer -lSoQt -lqt -I/usr/local/qt/include
 *  
 *
 *  Usage:
 *  robotviewer [-d description_path] [-w width] [-h height] [-n number_of_images] [-p output_file_format] position_file camera_file
 *
 *  Where "pos" is the file containing the 21 values for the position of
 *  the robot, "cam" is the file giving the coordinates of the camera
 *  pointing to the pelvis and "dir" is the eventual directory containing
 *  all the segment files (format inventor 2.1).
 *
\**************************************************************************/


#include <stdlib.h>
#include <stdio.h>
#include <stream.h>
#include <string.h>
#include <Inventor/SoDB.h>
#include <Inventor/SoInput.h>
#include <Inventor/nodes/SoGroup.h>
#include <Inventor/nodes/SoDirectionalLight.h>
#include <Inventor/nodes/SoPerspectiveCamera.h>
#include <Inventor/nodes/SoSeparator.h>
#include <Inventor/nodes/SoRotationXYZ.h>
#include <Inventor/nodes/SoTranslation.h>
#include <Inventor/fields/SoSFVec3f.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/SbViewportRegion.h>
#include <Inventor/SoOffscreenRenderer.h>


// *************************************************************************


SoNode *
get_segment_from_file(char * file_name)
{
  if (!file_name) {
    cerr << "No file name provided.\n";
    return NULL;
  }

  SoInput in;
  if (!in.openFile(file_name)) {
    cerr << "Couldn't open file '" << file_name << "'.\n";
    return NULL;
  }

  SoNode * root = SoDB::readAll(&in);
  return root;
} // get_segment_from_file()


// *************************************************************************


static const float pi = 3.1415926;
SoRotationXYZ * articulation[29];
SoTranslation * pelvistranslation;

int
make_robot_position (SoGroup * root, float * pos, char * description_path)
{
  /* Denavit-Hartenberg type definition of the translations and rotations used to build the robot */
  float r[29] = {0, 0, 0, 0, 0, 0, 0.110, 0, 0, 0.410, 0.410, 0, 0.083, 0.083, 0, 0.410, 0.410, 0, 0, 0.220, 0, 0, 0.410, 0.410, 0, 0, 0.110, 0, 0};
  SoTranslation * transX[29];
  for (int i = 0 ; i < 29 ; i++) {
    transX[i] = new SoTranslation;
    transX[i]->translation.setValue(r[i], 0, 0);
  }

  float alpha[29] = {-pi/2, pi/2, pi/2, pi/2, pi/2, pi/2, 0, pi/2, pi/2, 0, 0, -pi/2, pi/2, pi/2, -pi/2, 0, 0, pi/2, pi/2, 0, -pi/2, -pi/2, 0, 0, -pi/2, 0, -pi/2, pi/2, -pi/2};
  SoRotationXYZ * rotX[29];
  for (int i = 0 ; i < 29 ; i++) {
    rotX[i] = new SoRotationXYZ;
    rotX[i]->axis = SoRotationXYZ::X;
    rotX[i]->angle = alpha[i];
  }

  float lambda[29] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0.170, 0, -0.170, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.120, 0, 0.128, 0, 0};
  SoTranslation * transZ[29];
  for (int i = 0 ; i < 29 ; i++) {
    transZ[i] = new SoTranslation;
    transZ[i]->translation.setValue(0, 0, lambda[i]);
  }

  float theta[29] = {0, pi/2, pi/2, pi/2, pi/2, pi/2, 0, pi/2, -pi/2, 0, 0, 0, pi, 0, 0, 0, pi/2, -pi/2, 0, 0, pi/2, pi/2, 0, 0, 0, 0, 0, pi/2, 0};
  float signe[29] = {0, 0, 0, 1, 1, 1, -1, -1, -1, -1, -1, -1, 0, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, 1, 0, 1, 1, 1};
  int indice[29] = {0, 0, 0, 20, 21, 19, 9, 10, 4, 3, 2, 1, 0, 1, 2, 3, 4, 10, 9, 11, 12, 8, 7, 6, 5, 0, 13, 14, 15};
  SoRotationXYZ ** rotZ = articulation;
  for (int i = 0 ; i < 29 ; i++) {
    rotZ[i] = new SoRotationXYZ;
    rotZ[i]->axis = SoRotationXYZ::Z;
    rotZ[i]->angle = theta[i] + signe[i] * pos[indice[i]-1];
  }

  /* Load the segments from files */
  char * segment_name[17];
  SoNode * segment_node[17];
  for (int i = 0 ; i < 17 ; i++) {
    segment_name[i] = new char[strlen(description_path) + 10];
    sprintf(segment_name[i], "%s/s%d.iv", description_path, i + 1);
    segment_node[i] = get_segment_from_file(segment_name[i]);
    if (segment_node[i] == NULL) {
      cerr << "Couldn't build segment " << i << ".\n";
      exit(-1);
    }
  }

  /* Add the ground */
  root->addChild(segment_node[13]);

  /* Translate and rotate to the tip of the right foot */
  pelvistranslation = new SoTranslation;
  pelvistranslation->translation.setValue(pos[15], 0.903+pos[16], pos[17]);
  root->addChild(pelvistranslation);
  for (int i = 0 ; i < 12 ; i++) {
    root->addChild(transX[i]);
    root->addChild(rotX[i]);
    root->addChild(transZ[i]);
    root->addChild(rotZ[i]);
  }

  /* Add the right leg */
  for (int i = 12 ; i < 19 ; i++) {
    root->addChild(transX[i]);
    root->addChild(rotX[i]);
    root->addChild(transZ[i]);
    root->addChild(rotZ[i]);
    root->addChild(segment_node[i-12]);
  }

  /* Add the left leg */
  SoSeparator * leftleg = new SoSeparator;
  root->addChild(leftleg);
  for (int i = 19 ; i < 25 ; i++) {
    leftleg->addChild(transX[i]);
    leftleg->addChild(rotX[i]);
    leftleg->addChild(transZ[i]);
    leftleg->addChild(rotZ[i]);
    leftleg->addChild(segment_node[i-12]);
  }

  /* Add the trunk */
  for (int i = 26 ; i < 29 ; i++) {
    root->addChild(transX[i]);
    root->addChild(rotX[i]);
    root->addChild(transZ[i]);
    root->addChild(rotZ[i]);
    root->addChild(segment_node[i-12]);
  }
  return TRUE;
} // make_robot_position()

int
change_robot_position (float * pos)
{
  pelvistranslation->translation.setValue(pos[15], 0.903+pos[16], pos[17]);

  float theta[29] = {0, pi/2, pi/2, pi/2, pi/2, pi/2, 0, pi/2, -pi/2, 0, 0, 0, pi, 0, 0, 0, pi/2, -pi/2, 0, 0, pi/2, pi/2, 0, 0, 0, 0, 0, pi/2, 0};
  float signe[29] = {0, 0, 0, 1, 1, 1, -1, -1, -1, -1, -1, -1, 0, 1, 1, 1, 1, 1, 1, 1, 1, -1, -1, -1, 1, 0, 1, 1, 1};
  int indice[29] = {0, 0, 0, 20, 21, 19, 9, 10, 4, 3, 2, 1, 0, 1, 2, 3, 4, 10, 9, 11, 12, 8, 7, 6, 5, 0, 13, 14, 15};
  SoRotationXYZ ** rotZ = articulation;
  for (int i = 0 ; i < 29 ; i++) {
    rotZ[i]->angle = theta[i] + signe[i] * pos[indice[i]-1];
  }

  return TRUE;
} // change_robot_position()


// *************************************************************************


float *
get_vector_from_file(char * file_name, int size)
{
  filebuf f_i;
  istream f_in(&f_i);
  if (! f_i.open(file_name, ios::in)) {
    cerr << "Couldn't open file '" << file_name << "'.\n";
    return NULL;
  }

  float * vect = new float[size];
  for (int i = 0 ; i < size ; i++)
    f_in >> vect[i];
  if (f_in.eof()) {
    cerr << "The file '" << file_name << "' must contain "
	 << size << " values.\n";
    f_i.close();
    return NULL;    
  }
  f_i.close();

  return vect;
} // get_vector_from_file


// *************************************************************************

const char program_usage[] = " [-d description_path] [-w width] [-h height] [-n number_of_images] [-p output_file_format] position_file camera_file";
char description_path[255];
char output_file_format[255];
char position_file[255];
char camera_file[255];
int width = 600;
int height = 600;
int number_of_images = 1;
int output_flag = FALSE;

// *************************************************************************

int
verify_usage(int argc, 
	     char ** argv)
{
  /* The program name is always the first argument */
  char * prog_name = strrchr(argv[0], '/');
  if (!prog_name)
    prog_name = argv[0];

  /* Check that there are at least two arguments */
  if (argc < 3) {
    cerr << "Error: not enough arguments\n"
	 << "Usage: " << prog_name << program_usage << "\n";
    return FALSE;
  }

  /* Default description path */
  strcpy(description_path, "/home/ravel/wieber/BIP2000/Visualisation/Description");

  /* Scan the list of arguments for options */
  int last_argument = 0;
  for (int i = 1 ; i < argc-2 ; i++) {
    if (!strcmp(argv[i], "-d")) {
      i++;
      strcpy(description_path, argv[i]);
      last_argument = i;
    }
    else if (!strcmp(argv[i], "-w")) {
      i++;
      width = atoi(argv[i]);
      if (width <= 0) {
	cerr << "Error: width must be a positive integer: " << argv[i] << "\n"
	     << "Usage: " << prog_name << program_usage << "\n";
	return FALSE;
      }
      if (width > 1600) {
	cout << "Warning: maximal width is 1600\n";
	width = 1600;
      }
      last_argument = i;
    }
    else if (!strcmp(argv[i], "-h")) {
      i++;
      height = atoi(argv[i]);
      if (height <= 0) {
	cerr << "Error: height must be a positive integer: " << argv[i] << "\n"
	     << "Usage: " << prog_name << program_usage << "\n";
	return FALSE;
      }
      if (height > 1200) {
	cout << "Warning: maximal height is 1200\n";
	height = 1200;
      }
      last_argument = i;
    }
    else if (!strcmp(argv[i], "-n")) {
      i++;
      number_of_images = atoi(argv[i]);
      if (number_of_images <= 0) {
	cerr << "Error: number_of_images must be a positive integer: " << argv[i] << "\n"
	     << "Usage: " << prog_name << program_usage << "\n";
	return FALSE;
      }
      last_argument = i;
    }
    else if (!strcmp(argv[i], "-p")) {
      i++;
      strcpy(output_file_format, argv[i]);
      output_flag = TRUE;
      last_argument = i;
    }
    else {
      cerr << "Error: unknown option: " << argv[i] << "\n"
	   << "Usage: " << prog_name << program_usage << "\n";
      return FALSE;
    }
  }

  /* The last two arguments must be the position and the camera files */
  if (last_argument >= argc-2) {
    cerr << "Error: not enough arguments\n"
	 << "Usage: " << prog_name << program_usage << "\n";
    return FALSE;
  }
  strcpy(position_file, argv[argc-2]);
  strcpy(camera_file, argv[argc-1]);

  return TRUE;
} // verify_usage()


// *************************************************************************


int
main(int argc,
     char ** argv)
{
  /* Check and retrieve the arguments */
  if (!verify_usage(argc, argv))
    exit(-1);

  /* Read the file containing the position of the robot */
  float * pos = get_vector_from_file(position_file, 21*number_of_images);
  if (pos == NULL) {
    cerr << "Couldn't read 21 * " << number_of_images << " values from file '" << position_file << "'.\n";
    exit(-1);
  }

  /* Read the file containing the position of the camera */
  float * cam = get_vector_from_file(camera_file, 3);
  if (cam == NULL) {
    cerr << "Couldn't read 3 values from file '" << camera_file << "'.\n";
    exit(-1);
  }

  /* Initialise QT */
  QWidget * window = SoQt::init(argv[0]);
  if (window == NULL)
    exit(-1);

  /* Create the root */
  SoGroup * root = new SoGroup;
  root->ref();

  /* Compute the target of the camera */
  float min[3], max[3], target[3];
  for(int j = 0 ; j < 3 ; j++)
    min[j] = max[j] = pos[15+j];
  for(int i = 1 ; i < number_of_images ; i++) {
    for(int j = 0 ; j < 3 ; j++) {
      if (min[j] > pos[21*i+15+j])
        min[j] = pos[21*i+15+j];
      if (max[j] < pos[21*i+15+j])
        max[j] = pos[21*i+15+j];
    }
  }
  for(int j = 0 ; j < 3 ; j++)
    target[j] = (max[j]+min[j])/2;
  target[1] += 0.903;

  /* Create the camera */
  SoPerspectiveCamera * camera = new SoPerspectiveCamera;
  camera->position.setValue(cam[0], cam[1], cam[2]);
  camera->pointAt(SbVec3f(target[0], target[1], target[2]));
  camera->focalDistance = SbVec3f(target[0]-cam[0], target[1]-cam[1], target[2]-cam[2]).length();
  camera->nearDistance = 0.01;
  root->addChild(camera);

  if (output_flag == TRUE) {
    /* Create the light */
    SoDirectionalLight * light = new SoDirectionalLight;
    light->direction.setValue(target[0]-cam[0], target[1]-cam[1], target[2]-cam[2]);
    root->addChild(light);
  }

  /* Build the robot */
  if (!make_robot_position(root, pos, description_path)) {
    cerr << "Couldn't build the robot.\n";
    exit(-1);
  }

  if (output_flag == TRUE) {
    char * output_file_name = new char[strlen(output_file_format) + 10];
    SoOffscreenRenderer * offscreen = new SoOffscreenRenderer(SbViewportRegion(width, height));

    for(int i = 0 ; i < number_of_images ; i++) {
      /* Draw the view offscreen */
      change_robot_position(pos+21*i);
      offscreen->render(root);

      /* Write the view in the file */
      sprintf(output_file_name, output_file_format, i + 1);
      offscreen->writeToFile(SbString(output_file_name), SbName("jpg"));
    }
  }
  else {
    /* Draw the view in the window */
    SoQtExaminerViewer * examinerviewer = new SoQtExaminerViewer(window);
    examinerviewer->setSceneGraph(root);
    examinerviewer->show();
    SoQt::setWidgetSize(window, SbVec2s(width, height));
    SoQt::show(window);
    SoQt::mainLoop();
  }

  exit(0);
} // main()


// *************************************************************************
