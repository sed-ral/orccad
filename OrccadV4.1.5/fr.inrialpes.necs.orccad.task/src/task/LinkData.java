/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import port.PortData;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link task.LinkData#getSource <em>Source</em>}</li>
 *   <li>{@link task.LinkData#getTarget <em>Target</em>}</li>
 *   <li>{@link task.LinkData#getLinkType <em>Link Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see task.TaskPackage#getLinkData()
 * @model
 * @generated
 */
public interface LinkData extends LinkGeneric {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(PortData)
	 * @see task.TaskPackage#getLinkData_Source()
	 * @model required="true"
	 * @generated
	 */
	PortData getSource();

	/**
	 * Sets the value of the '{@link task.LinkData#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(PortData value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(PortData)
	 * @see task.TaskPackage#getLinkData_Target()
	 * @model required="true"
	 * @generated
	 */
	PortData getTarget();

	/**
	 * Sets the value of the '{@link task.LinkData#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(PortData value);

	/**
	 * Returns the value of the '<em><b>Link Type</b></em>' attribute.
	 * The literals are from the enumeration {@link task.LinkDataType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link Type</em>' attribute.
	 * @see task.LinkDataType
	 * @see #setLinkType(LinkDataType)
	 * @see task.TaskPackage#getLinkData_LinkType()
	 * @model
	 * @generated
	 */
	LinkDataType getLinkType();

	/**
	 * Sets the value of the '{@link task.LinkData#getLinkType <em>Link Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Link Type</em>' attribute.
	 * @see task.LinkDataType
	 * @see #getLinkType()
	 * @generated
	 */
	void setLinkType(LinkDataType value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkCoherentVariable(DiagnosticChain dal3, Map<Object, Object> dal4);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkLinkData(DiagnosticChain dal3, Map<Object, Object> dal4);

} // LinkData
