/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task;

import constraint.TemporalConstraint;

import java.util.Map;

import module.ModuleAlgorithmic;
import module.ModulePhysicalRobot;
import module.ModulePhysicalSensor;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Robot Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link task.RobotTask#getTemporalConstraints <em>Temporal Constraints</em>}</li>
 *   <li>{@link task.RobotTask#getModuleAlgorithmics <em>Module Algorithmics</em>}</li>
 *   <li>{@link task.RobotTask#getModulePhysicalRobot <em>Module Physical Robot</em>}</li>
 *   <li>{@link task.RobotTask#getModulePhysicalSensors <em>Module Physical Sensors</em>}</li>
 *   <li>{@link task.RobotTask#getLinkDatas <em>Link Datas</em>}</li>
 *   <li>{@link task.RobotTask#getLinkDrivers <em>Link Drivers</em>}</li>
 *   <li>{@link task.RobotTask#getLinkParams <em>Link Params</em>}</li>
 *   <li>{@link task.RobotTask#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see task.TaskPackage#getRobotTask()
 * @model
 * @generated
 */
public interface RobotTask extends EObject {
	/**
	 * Returns the value of the '<em><b>Temporal Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link constraint.TemporalConstraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Temporal Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temporal Constraints</em>' containment reference list.
	 * @see task.TaskPackage#getRobotTask_TemporalConstraints()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<TemporalConstraint> getTemporalConstraints();

	/**
	 * Returns the value of the '<em><b>Module Algorithmics</b></em>' containment reference list.
	 * The list contents are of type {@link module.ModuleAlgorithmic}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Module Algorithmics</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Module Algorithmics</em>' containment reference list.
	 * @see task.TaskPackage#getRobotTask_ModuleAlgorithmics()
	 * @model containment="true"
	 * @generated
	 */
	EList<ModuleAlgorithmic> getModuleAlgorithmics();

	/**
	 * Returns the value of the '<em><b>Module Physical Robot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Module Physical Robot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Module Physical Robot</em>' containment reference.
	 * @see #setModulePhysicalRobot(ModulePhysicalRobot)
	 * @see task.TaskPackage#getRobotTask_ModulePhysicalRobot()
	 * @model containment="true" required="true"
	 * @generated
	 */
	ModulePhysicalRobot getModulePhysicalRobot();

	/**
	 * Sets the value of the '{@link task.RobotTask#getModulePhysicalRobot <em>Module Physical Robot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Module Physical Robot</em>' containment reference.
	 * @see #getModulePhysicalRobot()
	 * @generated
	 */
	void setModulePhysicalRobot(ModulePhysicalRobot value);

	/**
	 * Returns the value of the '<em><b>Module Physical Sensors</b></em>' containment reference list.
	 * The list contents are of type {@link module.ModulePhysicalSensor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Module Physical Sensors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Module Physical Sensors</em>' containment reference list.
	 * @see task.TaskPackage#getRobotTask_ModulePhysicalSensors()
	 * @model containment="true"
	 * @generated
	 */
	EList<ModulePhysicalSensor> getModulePhysicalSensors();

	/**
	 * Returns the value of the '<em><b>Link Datas</b></em>' containment reference list.
	 * The list contents are of type {@link task.LinkData}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link Datas</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link Datas</em>' containment reference list.
	 * @see task.TaskPackage#getRobotTask_LinkDatas()
	 * @model containment="true"
	 * @generated
	 */
	EList<LinkData> getLinkDatas();

	/**
	 * Returns the value of the '<em><b>Link Drivers</b></em>' containment reference list.
	 * The list contents are of type {@link task.LinkDriver}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link Drivers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link Drivers</em>' containment reference list.
	 * @see task.TaskPackage#getRobotTask_LinkDrivers()
	 * @model containment="true"
	 * @generated
	 */
	EList<LinkDriver> getLinkDrivers();

	/**
	 * Returns the value of the '<em><b>Link Params</b></em>' containment reference list.
	 * The list contents are of type {@link task.LinkParam}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link Params</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link Params</em>' containment reference list.
	 * @see task.TaskPackage#getRobotTask_LinkParams()
	 * @model containment="true"
	 * @generated
	 */
	EList<LinkParam> getLinkParams();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see task.TaskPackage#getRobotTask_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link task.RobotTask#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkLoop(DiagnosticChain rt3, Map<Object, Object> rt4);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkRobotTask(DiagnosticChain rt3, Map<Object, Object> rt4);

} // RobotTask
