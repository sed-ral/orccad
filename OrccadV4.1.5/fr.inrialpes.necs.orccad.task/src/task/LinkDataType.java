/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Link Data Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see task.TaskPackage#getLinkDataType()
 * @model
 * @generated
 */
public enum LinkDataType implements Enumerator {
	/**
	 * The '<em><b>FUNC</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FUNC_VALUE
	 * @generated
	 * @ordered
	 */
	FUNC(0, "FUNC", "FUNC"),

	/**
	 * The '<em><b>ASYN ASYN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASYN_ASYN_VALUE
	 * @generated
	 * @ordered
	 */
	ASYN_ASYN(1, "ASYN_ASYN", "ASYN_ASYN"),

	/**
	 * The '<em><b>ASYN SYN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ASYN_SYN_VALUE
	 * @generated
	 * @ordered
	 */
	ASYN_SYN(2, "ASYN_SYN", "ASYN_SYN"),

	/**
	 * The '<em><b>SYN SYN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SYN_SYN_VALUE
	 * @generated
	 * @ordered
	 */
	SYN_SYN(3, "SYN_SYN", "SYN_SYN"),

	/**
	 * The '<em><b>SYN ASYN</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SYN_ASYN_VALUE
	 * @generated
	 * @ordered
	 */
	SYN_ASYN(4, "SYN_ASYN", "SYN_ASYN");

	/**
	 * The '<em><b>FUNC</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>FUNC</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FUNC
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int FUNC_VALUE = 0;

	/**
	 * The '<em><b>ASYN ASYN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ASYN ASYN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASYN_ASYN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ASYN_ASYN_VALUE = 1;

	/**
	 * The '<em><b>ASYN SYN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>ASYN SYN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ASYN_SYN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int ASYN_SYN_VALUE = 2;

	/**
	 * The '<em><b>SYN SYN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SYN SYN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SYN_SYN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SYN_SYN_VALUE = 3;

	/**
	 * The '<em><b>SYN ASYN</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SYN ASYN</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SYN_ASYN
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SYN_ASYN_VALUE = 4;

	/**
	 * An array of all the '<em><b>Link Data Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final LinkDataType[] VALUES_ARRAY =
		new LinkDataType[] {
			FUNC,
			ASYN_ASYN,
			ASYN_SYN,
			SYN_SYN,
			SYN_ASYN,
		};

	/**
	 * A public read-only list of all the '<em><b>Link Data Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<LinkDataType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Link Data Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LinkDataType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LinkDataType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Link Data Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LinkDataType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LinkDataType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Link Data Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static LinkDataType get(int value) {
		switch (value) {
			case FUNC_VALUE: return FUNC;
			case ASYN_ASYN_VALUE: return ASYN_ASYN;
			case ASYN_SYN_VALUE: return ASYN_SYN;
			case SYN_SYN_VALUE: return SYN_SYN;
			case SYN_ASYN_VALUE: return SYN_ASYN;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private LinkDataType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //LinkDataType
