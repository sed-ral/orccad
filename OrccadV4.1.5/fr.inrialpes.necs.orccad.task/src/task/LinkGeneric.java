/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link Generic</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see task.TaskPackage#getLinkGeneric()
 * @model
 * @generated
 */
public interface LinkGeneric extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkLoop(DiagnosticChain dal7, Map<Object, Object> dal8);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void checkSourceAndTargetDirections(DiagnosticChain pg1, Map<Object, Object> pg2);

} // LinkGeneric
