/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task.impl;

import constraint.TemporalConstraint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.sound.sampled.Port;

import module.ModuleAlgorithmic;
import module.ModulePhysicalRobot;
import module.ModulePhysicalSensor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

import port.EventCondition;
import port.ExceptionT1;
import port.ExceptionT2;
import port.ExceptionT3;
import port.PortData;
import port.PortDriver;
import port.PortGeneric;
import port.PortIOType;
import port.PortParam;
import port.PortParamIOType;

import task.LinkData;
import task.LinkDataType;
import task.LinkDriver;
import task.LinkGeneric;
import task.LinkParam;
import task.RobotTask;
import task.TaskPackage;

import task.util.TaskValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Robot Task</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link task.impl.RobotTaskImpl#getTemporalConstraints <em>Temporal Constraints</em>}</li>
 *   <li>{@link task.impl.RobotTaskImpl#getModuleAlgorithmics <em>Module Algorithmics</em>}</li>
 *   <li>{@link task.impl.RobotTaskImpl#getModulePhysicalRobot <em>Module Physical Robot</em>}</li>
 *   <li>{@link task.impl.RobotTaskImpl#getModulePhysicalSensors <em>Module Physical Sensors</em>}</li>
 *   <li>{@link task.impl.RobotTaskImpl#getLinkDatas <em>Link Datas</em>}</li>
 *   <li>{@link task.impl.RobotTaskImpl#getLinkDrivers <em>Link Drivers</em>}</li>
 *   <li>{@link task.impl.RobotTaskImpl#getLinkParams <em>Link Params</em>}</li>
 *   <li>{@link task.impl.RobotTaskImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RobotTaskImpl extends EObjectImpl implements RobotTask {
	/**
	 * The cached value of the '{@link #getTemporalConstraints() <em>Temporal Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemporalConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<TemporalConstraint> temporalConstraints;

	/**
	 * The cached value of the '{@link #getModuleAlgorithmics() <em>Module Algorithmics</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModuleAlgorithmics()
	 * @generated
	 * @ordered
	 */
	protected EList<ModuleAlgorithmic> moduleAlgorithmics;

	/**
	 * The cached value of the '{@link #getModulePhysicalRobot() <em>Module Physical Robot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModulePhysicalRobot()
	 * @generated
	 * @ordered
	 */
	protected ModulePhysicalRobot modulePhysicalRobot;

	/**
	 * The cached value of the '{@link #getModulePhysicalSensors() <em>Module Physical Sensors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModulePhysicalSensors()
	 * @generated
	 * @ordered
	 */
	protected EList<ModulePhysicalSensor> modulePhysicalSensors;

	/**
	 * The cached value of the '{@link #getLinkDatas() <em>Link Datas</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkDatas()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkData> linkDatas;

	/**
	 * The cached value of the '{@link #getLinkDrivers() <em>Link Drivers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkDrivers()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkDriver> linkDrivers;

	/**
	 * The cached value of the '{@link #getLinkParams() <em>Link Params</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkParams()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkParam> linkParams;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RobotTaskImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TaskPackage.Literals.ROBOT_TASK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TemporalConstraint> getTemporalConstraints() {
		if (temporalConstraints == null) {
			temporalConstraints = new EObjectContainmentEList<TemporalConstraint>(TemporalConstraint.class, this, TaskPackage.ROBOT_TASK__TEMPORAL_CONSTRAINTS);
		}
		return temporalConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModuleAlgorithmic> getModuleAlgorithmics() {
		if (moduleAlgorithmics == null) {
			moduleAlgorithmics = new EObjectContainmentEList<ModuleAlgorithmic>(ModuleAlgorithmic.class, this, TaskPackage.ROBOT_TASK__MODULE_ALGORITHMICS);
		}
		return moduleAlgorithmics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModulePhysicalRobot getModulePhysicalRobot() {
		return modulePhysicalRobot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModulePhysicalRobot(ModulePhysicalRobot newModulePhysicalRobot, NotificationChain msgs) {
		ModulePhysicalRobot oldModulePhysicalRobot = modulePhysicalRobot;
		modulePhysicalRobot = newModulePhysicalRobot;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_ROBOT, oldModulePhysicalRobot, newModulePhysicalRobot);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModulePhysicalRobot(ModulePhysicalRobot newModulePhysicalRobot) {
		if (newModulePhysicalRobot != modulePhysicalRobot) {
			NotificationChain msgs = null;
			if (modulePhysicalRobot != null)
				msgs = ((InternalEObject)modulePhysicalRobot).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_ROBOT, null, msgs);
			if (newModulePhysicalRobot != null)
				msgs = ((InternalEObject)newModulePhysicalRobot).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_ROBOT, null, msgs);
			msgs = basicSetModulePhysicalRobot(newModulePhysicalRobot, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_ROBOT, newModulePhysicalRobot, newModulePhysicalRobot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModulePhysicalSensor> getModulePhysicalSensors() {
		if (modulePhysicalSensors == null) {
			modulePhysicalSensors = new EObjectContainmentEList<ModulePhysicalSensor>(ModulePhysicalSensor.class, this, TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_SENSORS);
		}
		return modulePhysicalSensors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkData> getLinkDatas() {
		if (linkDatas == null) {
			linkDatas = new EObjectContainmentEList<LinkData>(LinkData.class, this, TaskPackage.ROBOT_TASK__LINK_DATAS);
		}
		return linkDatas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkDriver> getLinkDrivers() {
		if (linkDrivers == null) {
			linkDrivers = new EObjectContainmentEList<LinkDriver>(LinkDriver.class, this, TaskPackage.ROBOT_TASK__LINK_DRIVERS);
		}
		return linkDrivers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkParam> getLinkParams() {
		if (linkParams == null) {
			linkParams = new EObjectContainmentEList<LinkParam>(LinkParam.class, this, TaskPackage.ROBOT_TASK__LINK_PARAMS);
		}
		return linkParams;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TaskPackage.ROBOT_TASK__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkLoop(DiagnosticChain rt3, Map<Object, Object> rt4) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		
		boolean allOK = true;
		
		/**
		 * Tous les ports d'entree ne doivent recevoir qu'un lien
		 * 
		 */
		
		System.out.println("\n****************CheckLoop****************" );
		
		
		
		List<LinkGeneric> lLink = new ArrayList<LinkGeneric>();		
		//liste lien data
		lLink.addAll(this.getLinkDatas());System.out.println("linkData OK");
		//liste lien driver
		lLink.addAll(this.getLinkDrivers());System.out.println("linkDriver OK");
		//liste lien param
		lLink.addAll(this.getLinkParams());System.out.println("linkParam OK");
		
		List<PortGeneric> lInput = new ArrayList<PortGeneric>(); // liste de tous les ports d'entrée INPUT ou INPUT_OUTPUT
		for(Iterator<LinkGeneric> it_l = lLink.iterator(); it_l.hasNext();){
			LinkGeneric lg = it_l.next();
			
			if(lg instanceof LinkData && ((LinkData)lg).getTarget().getDirection()!= PortIOType.OUTPUT){
				lInput.add(((LinkData)lg).getTarget());
			}
			
			if(lg instanceof LinkDriver && ((LinkDriver)lg).getTarget().getDirection()!= PortIOType.OUTPUT){
				lInput.add(((LinkDriver)lg).getTarget());
			}
	
			if(lg instanceof LinkParam && ((LinkParam)lg).getTarget().getDirection()!= PortParamIOType.OUTPUT ){
				lInput.add(((LinkParam)lg).getTarget());
			}
					
		}
		System.out.println("lInPut OK");
		List<PortGeneric> lInput_tmp = new ArrayList<PortGeneric>();
		int nbr = 0;
		for(Iterator<PortGeneric> it_pg = lInput.iterator(); it_pg.hasNext();){
			System.out.println("lInPut OK 1");PortGeneric pg = it_pg.next();
			System.out.println("lInPut OK 2");nbr=0;
			
			System.out.println("lInPut OK 3");lInput_tmp.addAll(lInput);
			System.out.println("lInPut OK 4");lInput_tmp.remove(pg);
			
			System.out.println("lInPut OK 5");if(lInput_tmp.contains(pg)) System.out.println("EnDouble" + pg.getPortName());
			
			lInput_tmp.clear();

		}

		System.out.println("****************EndCheckLoop****************\n" );
		
		if (false) {
			if (rt3 != null) {
				rt3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.ROBOT_TASK__CHECK_LOOP,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkLoop", EObjectValidator.getObjectLabel(this, rt4) }),
						 new Object [] { this }));
			}
			allOK = false;
		}
		
		return allOK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkRobotTask(DiagnosticChain rt3, Map<Object, Object> rt4) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		boolean allOK = true;
		
		if (this.getName()==null || this.getName().length()<1) {
			if (rt3 != null) {
				rt3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.ROBOT_TASK__CHECK_ROBOT_TASK,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkRobotTask", EObjectValidator.getObjectLabel(this, rt4) + " - Robot Task Name must be filled." }),
						 new Object [] { this }));
			}
			allOK = false;
		}
		
		// Il existe au moins un lien driver entre data outpu et driver input
		if (this.linkDrivers.isEmpty()) {
			if (rt3 != null) {
				rt3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.ROBOT_TASK__CHECK_ROBOT_TASK,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkRobotTask", EObjectValidator.getObjectLabel(this, rt4) + " - At least one LinkDriver from ModuleAlgo to ModulePhysical." }),
						 new Object [] { this }));
			}
			allOK = false;
		}
		else {
			// Check linkdriver
			Iterator<LinkDriver> itLink = this.linkDrivers.iterator();
			for (; itLink.hasNext(); ) {
				LinkDriver item = itLink.next();
				if (item.getSource() instanceof PortData) {
					return allOK;
				}
			}
			// No LinkDriver Data -> Driver found
			if (rt3 != null) {
				rt3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.ROBOT_TASK__CHECK_ROBOT_TASK,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkRobotTask", EObjectValidator.getObjectLabel(this, rt4) + " - Missing at least one LinkDriver from ModuleAlgo to ModulePhysical." }),
						 new Object [] { this }));
			}
			allOK = false;
		}
		return allOK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TaskPackage.ROBOT_TASK__TEMPORAL_CONSTRAINTS:
				return ((InternalEList<?>)getTemporalConstraints()).basicRemove(otherEnd, msgs);
			case TaskPackage.ROBOT_TASK__MODULE_ALGORITHMICS:
				return ((InternalEList<?>)getModuleAlgorithmics()).basicRemove(otherEnd, msgs);
			case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_ROBOT:
				return basicSetModulePhysicalRobot(null, msgs);
			case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_SENSORS:
				return ((InternalEList<?>)getModulePhysicalSensors()).basicRemove(otherEnd, msgs);
			case TaskPackage.ROBOT_TASK__LINK_DATAS:
				return ((InternalEList<?>)getLinkDatas()).basicRemove(otherEnd, msgs);
			case TaskPackage.ROBOT_TASK__LINK_DRIVERS:
				return ((InternalEList<?>)getLinkDrivers()).basicRemove(otherEnd, msgs);
			case TaskPackage.ROBOT_TASK__LINK_PARAMS:
				return ((InternalEList<?>)getLinkParams()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TaskPackage.ROBOT_TASK__TEMPORAL_CONSTRAINTS:
				return getTemporalConstraints();
			case TaskPackage.ROBOT_TASK__MODULE_ALGORITHMICS:
				return getModuleAlgorithmics();
			case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_ROBOT:
				return getModulePhysicalRobot();
			case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_SENSORS:
				return getModulePhysicalSensors();
			case TaskPackage.ROBOT_TASK__LINK_DATAS:
				return getLinkDatas();
			case TaskPackage.ROBOT_TASK__LINK_DRIVERS:
				return getLinkDrivers();
			case TaskPackage.ROBOT_TASK__LINK_PARAMS:
				return getLinkParams();
			case TaskPackage.ROBOT_TASK__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TaskPackage.ROBOT_TASK__TEMPORAL_CONSTRAINTS:
				getTemporalConstraints().clear();
				getTemporalConstraints().addAll((Collection<? extends TemporalConstraint>)newValue);
				return;
			case TaskPackage.ROBOT_TASK__MODULE_ALGORITHMICS:
				getModuleAlgorithmics().clear();
				getModuleAlgorithmics().addAll((Collection<? extends ModuleAlgorithmic>)newValue);
				return;
			case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_ROBOT:
				setModulePhysicalRobot((ModulePhysicalRobot)newValue);
				return;
			case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_SENSORS:
				getModulePhysicalSensors().clear();
				getModulePhysicalSensors().addAll((Collection<? extends ModulePhysicalSensor>)newValue);
				return;
			case TaskPackage.ROBOT_TASK__LINK_DATAS:
				getLinkDatas().clear();
				getLinkDatas().addAll((Collection<? extends LinkData>)newValue);
				return;
			case TaskPackage.ROBOT_TASK__LINK_DRIVERS:
				getLinkDrivers().clear();
				getLinkDrivers().addAll((Collection<? extends LinkDriver>)newValue);
				return;
			case TaskPackage.ROBOT_TASK__LINK_PARAMS:
				getLinkParams().clear();
				getLinkParams().addAll((Collection<? extends LinkParam>)newValue);
				return;
			case TaskPackage.ROBOT_TASK__NAME:
				//modified by boudinf
				//to be improved to avoid name as C++ keywords
				if(((String)newValue).matches("[a-zA-Z_0-9]*") ){ 
					setName((String)newValue);
					return;
				}//end modification

		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TaskPackage.ROBOT_TASK__TEMPORAL_CONSTRAINTS:
				getTemporalConstraints().clear();
				return;
			case TaskPackage.ROBOT_TASK__MODULE_ALGORITHMICS:
				getModuleAlgorithmics().clear();
				return;
			case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_ROBOT:
				setModulePhysicalRobot((ModulePhysicalRobot)null);
				return;
			case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_SENSORS:
				getModulePhysicalSensors().clear();
				return;
			case TaskPackage.ROBOT_TASK__LINK_DATAS:
				getLinkDatas().clear();
				return;
			case TaskPackage.ROBOT_TASK__LINK_DRIVERS:
				getLinkDrivers().clear();
				return;
			case TaskPackage.ROBOT_TASK__LINK_PARAMS:
				getLinkParams().clear();
				return;
			case TaskPackage.ROBOT_TASK__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TaskPackage.ROBOT_TASK__TEMPORAL_CONSTRAINTS:
				return temporalConstraints != null && !temporalConstraints.isEmpty();
			case TaskPackage.ROBOT_TASK__MODULE_ALGORITHMICS:
				return moduleAlgorithmics != null && !moduleAlgorithmics.isEmpty();
			case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_ROBOT:
				return modulePhysicalRobot != null;
			case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_SENSORS:
				return modulePhysicalSensors != null && !modulePhysicalSensors.isEmpty();
			case TaskPackage.ROBOT_TASK__LINK_DATAS:
				return linkDatas != null && !linkDatas.isEmpty();
			case TaskPackage.ROBOT_TASK__LINK_DRIVERS:
				return linkDrivers != null && !linkDrivers.isEmpty();
			case TaskPackage.ROBOT_TASK__LINK_PARAMS:
				return linkParams != null && !linkParams.isEmpty();
			case TaskPackage.ROBOT_TASK__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //RobotTaskImpl
