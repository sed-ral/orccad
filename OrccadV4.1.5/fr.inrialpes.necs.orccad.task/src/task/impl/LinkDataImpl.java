/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task.impl;

import java.util.Map;

import module.ModuleAlgorithmic;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

import port.PortData;
import port.PortIOType;

import task.LinkData;
import task.LinkDataType;
import task.TaskPackage;

import task.util.TaskValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Link Data</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link task.impl.LinkDataImpl#getSource <em>Source</em>}</li>
 *   <li>{@link task.impl.LinkDataImpl#getTarget <em>Target</em>}</li>
 *   <li>{@link task.impl.LinkDataImpl#getLinkType <em>Link Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LinkDataImpl extends LinkGenericImpl implements LinkData {
	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected PortData source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected PortData target;

	/**
	 * The default value of the '{@link #getLinkType() <em>Link Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkType()
	 * @generated
	 * @ordered
	 */
	protected static final LinkDataType LINK_TYPE_EDEFAULT = LinkDataType.FUNC;

	/**
	 * The cached value of the '{@link #getLinkType() <em>Link Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkType()
	 * @generated
	 * @ordered
	 */
	protected LinkDataType linkType = LINK_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkDataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TaskPackage.Literals.LINK_DATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortData getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject)source;
			source = (PortData)eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TaskPackage.LINK_DATA__SOURCE, oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortData basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(PortData newSource) {
		PortData oldSource = source;
		source = newSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TaskPackage.LINK_DATA__SOURCE, oldSource, source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortData getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (PortData)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TaskPackage.LINK_DATA__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortData basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(PortData newTarget) {
		PortData oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TaskPackage.LINK_DATA__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkDataType getLinkType() {
		return linkType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLinkType(LinkDataType newLinkType) {
		LinkDataType oldLinkType = linkType;
		linkType = newLinkType == null ? LINK_TYPE_EDEFAULT : newLinkType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TaskPackage.LINK_DATA__LINK_TYPE, oldLinkType, linkType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkCoherentVariable(DiagnosticChain dal3, Map<Object, Object> dal4) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		boolean allOK = true;

		// check the variable have the same number of column
		if (this.source != null && this.target != null && 
				this.source.getCol()!=this.target.getCol()) {
			if (dal3 != null) {
				dal3.add
				(new BasicDiagnostic
						(Diagnostic.ERROR,
								TaskValidator.DIAGNOSTIC_SOURCE,
								TaskValidator.LINK_DATA__CHECK_COHERENT_VARIABLE,

								EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new 
										Object[] { "checkCoherentVariable", 
										EObjectValidator.getObjectLabel(this, dal4) + "toto2010"}),
										new Object [] { this }));
			}
			allOK = false;
		}

		// check the variable have the same number of row
		if (this.source != null && this.target != null && 
				this.source.getRow()!=this.target.getRow()) {
			if (dal3 != null) {
				dal3.add
				(new BasicDiagnostic
						(Diagnostic.ERROR,
								TaskValidator.DIAGNOSTIC_SOURCE,
								TaskValidator.LINK_DATA__CHECK_COHERENT_VARIABLE,

								EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new 
										Object[] { "checkCoherentVariable", 
										EObjectValidator.getObjectLabel(this, dal4) }),
										new Object [] { this }));
			}
			allOK = false;
		}

		// check the variables have the same type
		if (this.source != null && this.target != null && 
				!this.source.getVarType().equals(this.target.getVarType())) {
			if (dal3 != null) {
				dal3.add
				(new BasicDiagnostic
						(Diagnostic.ERROR,
								TaskValidator.DIAGNOSTIC_SOURCE,
								TaskValidator.LINK_DATA__CHECK_COHERENT_VARIABLE,

								EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new 
										Object[] { "checkCoherentVariable", 
										EObjectValidator.getObjectLabel(this, dal4) }),
										new Object [] { this }));
			}
			allOK = false;
		}


		return allOK;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkLinkData(DiagnosticChain dal3, Map<Object, Object> dal4) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		
		boolean allOK = true;
		
		// source == null ou target == null
		if (this.source == null || this.target==null) {
			if (dal3 != null) {
				dal3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.LINK_DATA__CHECK_LINK_DATA,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkLinkData", EObjectValidator.getObjectLabel(this, dal4) }),
						 new Object [] { this }));
			}
			allOK = false;
		}
		
	
		
		// if target is an output or source is an input
		if (this.target.getDirection()!= PortIOType.INPUT || this.source.getDirection()!= PortIOType.OUTPUT) {
			if (dal3 != null) {
				dal3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.LINK_DATA__CHECK_LINK_DATA,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkLinkData", EObjectValidator.getObjectLabel(this, dal4) }),
						 new Object [] { this }));
			}
			allOK =  false;
		}		
		
		
		// check in DataLinkType = FUNC, source and target Module have the same Temporal constraint
		if (this.linkType== LinkDataType.FUNC && !((ModuleAlgorithmic)this.target.getContainer()).getTemporalConstraints().equals(((ModuleAlgorithmic)this.source.getContainer()).getTemporalConstraints())) {
			if (dal3 != null) {
				dal3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.LINK_DATA__CHECK_LINK_DATA,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkLinkData", EObjectValidator.getObjectLabel(this, dal4) }),
						 new Object [] { this}));
			}
			allOK =  false;
		}
		
		
		
		
		
		return allOK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TaskPackage.LINK_DATA__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case TaskPackage.LINK_DATA__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
			case TaskPackage.LINK_DATA__LINK_TYPE:
				return getLinkType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TaskPackage.LINK_DATA__SOURCE:
				setSource((PortData)newValue);
				return;
			case TaskPackage.LINK_DATA__TARGET:
				setTarget((PortData)newValue);
				return;
			case TaskPackage.LINK_DATA__LINK_TYPE:
				setLinkType((LinkDataType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TaskPackage.LINK_DATA__SOURCE:
				setSource((PortData)null);
				return;
			case TaskPackage.LINK_DATA__TARGET:
				setTarget((PortData)null);
				return;
			case TaskPackage.LINK_DATA__LINK_TYPE:
				setLinkType(LINK_TYPE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TaskPackage.LINK_DATA__SOURCE:
				return source != null;
			case TaskPackage.LINK_DATA__TARGET:
				return target != null;
			case TaskPackage.LINK_DATA__LINK_TYPE:
				return linkType != LINK_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (linkType: ");
		result.append(linkType);
		result.append(')');
		return result.toString();
	}

} //LinkDataImpl
