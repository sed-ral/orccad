/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import module.ModuleGeneric;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;

import port.PortData;
import port.PortDataDriver;
import port.PortDriver;
import port.PortIOType;

import task.LinkData;
import task.LinkDriver;
import task.LinkGeneric;
import task.LinkParam;
import task.RobotTask;
import task.TaskPackage;

import task.util.TaskValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Link Driver</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link task.impl.LinkDriverImpl#getSource <em>Source</em>}</li>
 *   <li>{@link task.impl.LinkDriverImpl#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class LinkDriverImpl extends LinkGenericImpl implements LinkDriver {
	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected PortDataDriver source;

	/**
	 * The cached value of the '{@link #getTarget() <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTarget()
	 * @generated
	 * @ordered
	 */
	protected PortDataDriver target;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkDriverImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TaskPackage.Literals.LINK_DRIVER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDataDriver getSource() {
		if (source != null && source.eIsProxy()) {
			InternalEObject oldSource = (InternalEObject)source;
			source = (PortDataDriver)eResolveProxy(oldSource);
			if (source != oldSource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TaskPackage.LINK_DRIVER__SOURCE, oldSource, source));
			}
		}
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDataDriver basicGetSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(PortDataDriver newSource) {
		PortDataDriver oldSource = source;
		source = newSource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TaskPackage.LINK_DRIVER__SOURCE, oldSource, source));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDataDriver getTarget() {
		if (target != null && target.eIsProxy()) {
			InternalEObject oldTarget = (InternalEObject)target;
			target = (PortDataDriver)eResolveProxy(oldTarget);
			if (target != oldTarget) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TaskPackage.LINK_DRIVER__TARGET, oldTarget, target));
			}
		}
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDataDriver basicGetTarget() {
		return target;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTarget(PortDataDriver newTarget) {
		PortDataDriver oldTarget = target;
		target = newTarget;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TaskPackage.LINK_DRIVER__TARGET, oldTarget, target));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkCoherentVariable(DiagnosticChain drl3, Map<Object, Object> drl4) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		boolean allOK = true;

		// check the variable have the same number of column
		if (this.source != null && this.target != null && 
				this.source.getCol()!=this.target.getCol()) {
			if (drl3 != null) {
				drl3.add
				(new BasicDiagnostic
						(Diagnostic.ERROR,
								TaskValidator.DIAGNOSTIC_SOURCE,
								TaskValidator.LINK_DATA__CHECK_COHERENT_VARIABLE,

								EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new 
										Object[] { "checkCoherentVariable", 
										EObjectValidator.getObjectLabel(this, drl4) + "toto2010"}),
										new Object [] { this }));
			}
			allOK = false;
		}

		// check the variable have the same number of row
		if (this.source != null && this.target != null && 
				this.source.getRow()!=this.target.getRow()) {
			if (drl3 != null) {
				drl3.add
				(new BasicDiagnostic
						(Diagnostic.ERROR,
								TaskValidator.DIAGNOSTIC_SOURCE,
								TaskValidator.LINK_DATA__CHECK_COHERENT_VARIABLE,

								EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new 
										Object[] { "checkCoherentVariable", 
										EObjectValidator.getObjectLabel(this, drl4) }),
										new Object [] { this }));
			}
			allOK = false;
		}

		// check the variables have the same type
		if (this.source != null && this.target != null && 
				!this.source.getVarType().equals(this.target.getVarType())) {
			if (drl3 != null) {
				drl3.add
				(new BasicDiagnostic
						(Diagnostic.ERROR,
								TaskValidator.DIAGNOSTIC_SOURCE,
								TaskValidator.LINK_DATA__CHECK_COHERENT_VARIABLE,

								EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new 
										Object[] { "checkCoherentVariable", 
										EObjectValidator.getObjectLabel(this, drl4) }),
										new Object [] { this }));
			}
			allOK = false;
		}


		return allOK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkLinkDriver(DiagnosticChain drl3, Map<Object, Object> drl4) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		boolean allOK = true;
		
		// source == null ou target == null
		if (this.source == null || this.target==null) {
			if (drl3 != null) {
				drl3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.LINK_DATA__CHECK_LINK_DATA,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkLinkData", EObjectValidator.getObjectLabel(this, drl4) }),
						 new Object [] { this }));
			}
			allOK = false;
		}
		
	
		
		// if target is an output or source is an input
		if (this.target.getDirection()!= PortIOType.INPUT || this.source.getDirection()!= PortIOType.OUTPUT) {
			if (drl3 != null) {
				drl3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.LINK_DATA__CHECK_LINK_DATA,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkLinkData", EObjectValidator.getObjectLabel(this, drl4) }),
						 new Object [] { this }));
			}
			allOK =  false;
		}	
		return allOK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TaskPackage.LINK_DRIVER__SOURCE:
				if (resolve) return getSource();
				return basicGetSource();
			case TaskPackage.LINK_DRIVER__TARGET:
				if (resolve) return getTarget();
				return basicGetTarget();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TaskPackage.LINK_DRIVER__SOURCE:
				boolean ok2create =true;
				/*// pas de source == target
				if(this.target == newValue) ok2create=false;
				// pas de boucle sur le module
				if(this.target!=null && this.source!=null){
					ModuleGeneric moduleSource = (ModuleGeneric)this.source.eContainer();
					ModuleGeneric moduleTarget = (ModuleGeneric)this.target.eContainer();
					if(moduleSource.equals(moduleTarget))
						ok2create=false;
				}
				// source et target appartiennent a la meme tache robot
				RobotTask robotTaskSource = (RobotTask)this.source.eContainer().eContainer();
				RobotTask robotTaskTarget = (RobotTask)this.target.eContainer().eContainer();
				if(robotTaskSource != robotTaskTarget) ok2create=false;
				*/
				if(ok2create)setSource((PortDataDriver)newValue);
				//else System.out.println("Pas bieeeennnn !!!!");
				return;
			case TaskPackage.LINK_DRIVER__TARGET:
				System.out.println("****** SET LINK DRIVER TARGET ******");
				
				if(this.eContainer()!=null){ // pour le chargement
				
				RobotTask parent = (RobotTask)(this.eContainer());
				
				// test if the target port is in the same RobotTask
				
				RobotTask rt1 = (RobotTask) ((PortDataDriver)newValue).eContainer().eContainer();
				
				if(rt1!=parent){
					System.out.println("WARNING - You can only select an INPUT port from the same RobotTask.\n" + parent.getName() + "  " + rt1.getName());
					
					
					
					return;											
				}
				
				
				
				// test if the target Port is an INPUT port
				if(		newValue instanceof PortDriver && ((PortDriver)newValue).getDirection()!= PortIOType.INPUT ||
						newValue instanceof PortData   && ((PortData)newValue).getDirection()!= PortIOType.INPUT){
					System.out.println("WARNING - The target port must be an INPUT port.");
					return;						
				}
				
				// test to know is the target port has already a link connect to it
				boolean targetIsBusy = false;
				
				List<LinkGeneric> lg = new ArrayList<LinkGeneric>();
				lg.addAll(parent.getLinkDrivers());
				
				for(Iterator<LinkGeneric> iLinkGeneric = lg.iterator(); iLinkGeneric.hasNext(); ){
					LinkGeneric currentLinkGeneric = iLinkGeneric.next();
					
					// if it is a DataLink
					if(currentLinkGeneric instanceof LinkDriver && ((LinkDriver)currentLinkGeneric).getTarget() !=null && ((LinkDriver)currentLinkGeneric).getTarget().equals(newValue))
						targetIsBusy = true;
					// if it is a DriverLink
					if(currentLinkGeneric instanceof LinkData && ((LinkData)currentLinkGeneric).getTarget() !=null && ((LinkData)currentLinkGeneric).getTarget().equals(newValue))
						targetIsBusy = true;		
				
				}
					
				// if targetIsBusy is false we can continue else ERROR message
				if(targetIsBusy){
					System.out.println("WARNING - an INPUT can have only input link to him.");
					return;
				}
				
				
				
				}	
				
			/*	
				if(this.getSource()==null && this.getTarget()==null)setTarget((PortDataDriver)newValue);
				
				
				if((newValue instanceof PortData && this.getSource()!=null && this.getSource() instanceof PortDriver)||(newValue instanceof PortDriver && this.getSource()!=null && this.getSource() instanceof PortData)){
					
				RobotTask robotTask = (RobotTask)this.eContainer();
				List<LinkGeneric> lg = new ArrayList<LinkGeneric>();  
					lg.addAll(robotTask.getLinkDatas());
					lg.addAll(robotTask.getLinkDrivers());
					lg.addAll(robotTask.getLinkParams());
	
				boolean exist = false;
				for(Iterator<LinkGeneric> iLink = lg.iterator(); iLink.hasNext();){
					LinkGeneric lCurrent = iLink.next();
					
					if(lCurrent instanceof LinkData   && ((LinkData)  lCurrent).getTarget()!=null && ((LinkData)  lCurrent).getTarget().equals(newValue)) exist =true; 
					if(lCurrent instanceof LinkDriver && ((LinkDriver)lCurrent).getTarget()!=null && ((LinkDriver)lCurrent).getTarget().equals(newValue)) exist =true;
						
					
						
					
					//if(lCurrent instanceof LinkParam  && ((LinkParam)  lCurrent).getTarget()!=null && ((LinkParam) lCurrent).getTarget().equals(newValue)) exist =true;
				}
				if(exist){
					System.out.println("WARNING - not allows to have multiple link to an input port.");
				}else {*/
					setTarget((PortDataDriver)newValue);
				/*}
				
		}else {
			
			System.out.println("WARNING - Source and target must be different type.");
		}*/
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TaskPackage.LINK_DRIVER__SOURCE:
				setSource((PortDataDriver)null);
				return;
			case TaskPackage.LINK_DRIVER__TARGET:
				setTarget((PortDataDriver)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TaskPackage.LINK_DRIVER__SOURCE:
				return source != null;
			case TaskPackage.LINK_DRIVER__TARGET:
				return target != null;
		}
		return super.eIsSet(featureID);
	}

} //LinkDriverImpl
