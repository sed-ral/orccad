/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see task.TaskFactory
 * @model kind="package"
 * @generated
 */
public interface TaskPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "task";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://task";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "task";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TaskPackage eINSTANCE = task.impl.TaskPackageImpl.init();

	/**
	 * The meta object id for the '{@link task.impl.RobotTaskImpl <em>Robot Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see task.impl.RobotTaskImpl
	 * @see task.impl.TaskPackageImpl#getRobotTask()
	 * @generated
	 */
	int ROBOT_TASK = 0;

	/**
	 * The feature id for the '<em><b>Temporal Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK__TEMPORAL_CONSTRAINTS = 0;

	/**
	 * The feature id for the '<em><b>Module Algorithmics</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK__MODULE_ALGORITHMICS = 1;

	/**
	 * The feature id for the '<em><b>Module Physical Robot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK__MODULE_PHYSICAL_ROBOT = 2;

	/**
	 * The feature id for the '<em><b>Module Physical Sensors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK__MODULE_PHYSICAL_SENSORS = 3;

	/**
	 * The feature id for the '<em><b>Link Datas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK__LINK_DATAS = 4;

	/**
	 * The feature id for the '<em><b>Link Drivers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK__LINK_DRIVERS = 5;

	/**
	 * The feature id for the '<em><b>Link Params</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK__LINK_PARAMS = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK__NAME = 7;

	/**
	 * The number of structural features of the '<em>Robot Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_FEATURE_COUNT = 8;

	/**
	 * The meta object id for the '{@link task.impl.LinkGenericImpl <em>Link Generic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see task.impl.LinkGenericImpl
	 * @see task.impl.TaskPackageImpl#getLinkGeneric()
	 * @generated
	 */
	int LINK_GENERIC = 1;

	/**
	 * The number of structural features of the '<em>Link Generic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_GENERIC_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link task.impl.LinkDataImpl <em>Link Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see task.impl.LinkDataImpl
	 * @see task.impl.TaskPackageImpl#getLinkData()
	 * @generated
	 */
	int LINK_DATA = 2;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_DATA__SOURCE = LINK_GENERIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_DATA__TARGET = LINK_GENERIC_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Link Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_DATA__LINK_TYPE = LINK_GENERIC_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Link Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_DATA_FEATURE_COUNT = LINK_GENERIC_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link task.impl.LinkDriverImpl <em>Link Driver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see task.impl.LinkDriverImpl
	 * @see task.impl.TaskPackageImpl#getLinkDriver()
	 * @generated
	 */
	int LINK_DRIVER = 3;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_DRIVER__SOURCE = LINK_GENERIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_DRIVER__TARGET = LINK_GENERIC_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Link Driver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_DRIVER_FEATURE_COUNT = LINK_GENERIC_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link task.impl.LinkParamImpl <em>Link Param</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see task.impl.LinkParamImpl
	 * @see task.impl.TaskPackageImpl#getLinkParam()
	 * @generated
	 */
	int LINK_PARAM = 4;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_PARAM__SOURCE = LINK_GENERIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_PARAM__TARGET = LINK_GENERIC_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Link Param</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_PARAM_FEATURE_COUNT = LINK_GENERIC_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link task.LinkDataType <em>Link Data Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see task.LinkDataType
	 * @see task.impl.TaskPackageImpl#getLinkDataType()
	 * @generated
	 */
	int LINK_DATA_TYPE = 5;


	/**
	 * Returns the meta object for class '{@link task.RobotTask <em>Robot Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Robot Task</em>'.
	 * @see task.RobotTask
	 * @generated
	 */
	EClass getRobotTask();

	/**
	 * Returns the meta object for the containment reference list '{@link task.RobotTask#getTemporalConstraints <em>Temporal Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Temporal Constraints</em>'.
	 * @see task.RobotTask#getTemporalConstraints()
	 * @see #getRobotTask()
	 * @generated
	 */
	EReference getRobotTask_TemporalConstraints();

	/**
	 * Returns the meta object for the containment reference list '{@link task.RobotTask#getModuleAlgorithmics <em>Module Algorithmics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Module Algorithmics</em>'.
	 * @see task.RobotTask#getModuleAlgorithmics()
	 * @see #getRobotTask()
	 * @generated
	 */
	EReference getRobotTask_ModuleAlgorithmics();

	/**
	 * Returns the meta object for the containment reference '{@link task.RobotTask#getModulePhysicalRobot <em>Module Physical Robot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Module Physical Robot</em>'.
	 * @see task.RobotTask#getModulePhysicalRobot()
	 * @see #getRobotTask()
	 * @generated
	 */
	EReference getRobotTask_ModulePhysicalRobot();

	/**
	 * Returns the meta object for the containment reference list '{@link task.RobotTask#getModulePhysicalSensors <em>Module Physical Sensors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Module Physical Sensors</em>'.
	 * @see task.RobotTask#getModulePhysicalSensors()
	 * @see #getRobotTask()
	 * @generated
	 */
	EReference getRobotTask_ModulePhysicalSensors();

	/**
	 * Returns the meta object for the containment reference list '{@link task.RobotTask#getLinkDatas <em>Link Datas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Link Datas</em>'.
	 * @see task.RobotTask#getLinkDatas()
	 * @see #getRobotTask()
	 * @generated
	 */
	EReference getRobotTask_LinkDatas();

	/**
	 * Returns the meta object for the containment reference list '{@link task.RobotTask#getLinkDrivers <em>Link Drivers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Link Drivers</em>'.
	 * @see task.RobotTask#getLinkDrivers()
	 * @see #getRobotTask()
	 * @generated
	 */
	EReference getRobotTask_LinkDrivers();

	/**
	 * Returns the meta object for the containment reference list '{@link task.RobotTask#getLinkParams <em>Link Params</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Link Params</em>'.
	 * @see task.RobotTask#getLinkParams()
	 * @see #getRobotTask()
	 * @generated
	 */
	EReference getRobotTask_LinkParams();

	/**
	 * Returns the meta object for the attribute '{@link task.RobotTask#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see task.RobotTask#getName()
	 * @see #getRobotTask()
	 * @generated
	 */
	EAttribute getRobotTask_Name();

	/**
	 * Returns the meta object for class '{@link task.LinkGeneric <em>Link Generic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Link Generic</em>'.
	 * @see task.LinkGeneric
	 * @generated
	 */
	EClass getLinkGeneric();

	/**
	 * Returns the meta object for class '{@link task.LinkData <em>Link Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Link Data</em>'.
	 * @see task.LinkData
	 * @generated
	 */
	EClass getLinkData();

	/**
	 * Returns the meta object for the reference '{@link task.LinkData#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see task.LinkData#getSource()
	 * @see #getLinkData()
	 * @generated
	 */
	EReference getLinkData_Source();

	/**
	 * Returns the meta object for the reference '{@link task.LinkData#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see task.LinkData#getTarget()
	 * @see #getLinkData()
	 * @generated
	 */
	EReference getLinkData_Target();

	/**
	 * Returns the meta object for the attribute '{@link task.LinkData#getLinkType <em>Link Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Link Type</em>'.
	 * @see task.LinkData#getLinkType()
	 * @see #getLinkData()
	 * @generated
	 */
	EAttribute getLinkData_LinkType();

	/**
	 * Returns the meta object for class '{@link task.LinkDriver <em>Link Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Link Driver</em>'.
	 * @see task.LinkDriver
	 * @generated
	 */
	EClass getLinkDriver();

	/**
	 * Returns the meta object for the reference '{@link task.LinkDriver#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see task.LinkDriver#getSource()
	 * @see #getLinkDriver()
	 * @generated
	 */
	EReference getLinkDriver_Source();

	/**
	 * Returns the meta object for the reference '{@link task.LinkDriver#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see task.LinkDriver#getTarget()
	 * @see #getLinkDriver()
	 * @generated
	 */
	EReference getLinkDriver_Target();

	/**
	 * Returns the meta object for class '{@link task.LinkParam <em>Link Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Link Param</em>'.
	 * @see task.LinkParam
	 * @generated
	 */
	EClass getLinkParam();

	/**
	 * Returns the meta object for the reference '{@link task.LinkParam#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see task.LinkParam#getSource()
	 * @see #getLinkParam()
	 * @generated
	 */
	EReference getLinkParam_Source();

	/**
	 * Returns the meta object for the reference '{@link task.LinkParam#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see task.LinkParam#getTarget()
	 * @see #getLinkParam()
	 * @generated
	 */
	EReference getLinkParam_Target();

	/**
	 * Returns the meta object for enum '{@link task.LinkDataType <em>Link Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Link Data Type</em>'.
	 * @see task.LinkDataType
	 * @generated
	 */
	EEnum getLinkDataType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TaskFactory getTaskFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link task.impl.RobotTaskImpl <em>Robot Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see task.impl.RobotTaskImpl
		 * @see task.impl.TaskPackageImpl#getRobotTask()
		 * @generated
		 */
		EClass ROBOT_TASK = eINSTANCE.getRobotTask();

		/**
		 * The meta object literal for the '<em><b>Temporal Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROBOT_TASK__TEMPORAL_CONSTRAINTS = eINSTANCE.getRobotTask_TemporalConstraints();

		/**
		 * The meta object literal for the '<em><b>Module Algorithmics</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROBOT_TASK__MODULE_ALGORITHMICS = eINSTANCE.getRobotTask_ModuleAlgorithmics();

		/**
		 * The meta object literal for the '<em><b>Module Physical Robot</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROBOT_TASK__MODULE_PHYSICAL_ROBOT = eINSTANCE.getRobotTask_ModulePhysicalRobot();

		/**
		 * The meta object literal for the '<em><b>Module Physical Sensors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROBOT_TASK__MODULE_PHYSICAL_SENSORS = eINSTANCE.getRobotTask_ModulePhysicalSensors();

		/**
		 * The meta object literal for the '<em><b>Link Datas</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROBOT_TASK__LINK_DATAS = eINSTANCE.getRobotTask_LinkDatas();

		/**
		 * The meta object literal for the '<em><b>Link Drivers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROBOT_TASK__LINK_DRIVERS = eINSTANCE.getRobotTask_LinkDrivers();

		/**
		 * The meta object literal for the '<em><b>Link Params</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROBOT_TASK__LINK_PARAMS = eINSTANCE.getRobotTask_LinkParams();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROBOT_TASK__NAME = eINSTANCE.getRobotTask_Name();

		/**
		 * The meta object literal for the '{@link task.impl.LinkGenericImpl <em>Link Generic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see task.impl.LinkGenericImpl
		 * @see task.impl.TaskPackageImpl#getLinkGeneric()
		 * @generated
		 */
		EClass LINK_GENERIC = eINSTANCE.getLinkGeneric();

		/**
		 * The meta object literal for the '{@link task.impl.LinkDataImpl <em>Link Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see task.impl.LinkDataImpl
		 * @see task.impl.TaskPackageImpl#getLinkData()
		 * @generated
		 */
		EClass LINK_DATA = eINSTANCE.getLinkData();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_DATA__SOURCE = eINSTANCE.getLinkData_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_DATA__TARGET = eINSTANCE.getLinkData_Target();

		/**
		 * The meta object literal for the '<em><b>Link Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LINK_DATA__LINK_TYPE = eINSTANCE.getLinkData_LinkType();

		/**
		 * The meta object literal for the '{@link task.impl.LinkDriverImpl <em>Link Driver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see task.impl.LinkDriverImpl
		 * @see task.impl.TaskPackageImpl#getLinkDriver()
		 * @generated
		 */
		EClass LINK_DRIVER = eINSTANCE.getLinkDriver();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_DRIVER__SOURCE = eINSTANCE.getLinkDriver_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_DRIVER__TARGET = eINSTANCE.getLinkDriver_Target();

		/**
		 * The meta object literal for the '{@link task.impl.LinkParamImpl <em>Link Param</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see task.impl.LinkParamImpl
		 * @see task.impl.TaskPackageImpl#getLinkParam()
		 * @generated
		 */
		EClass LINK_PARAM = eINSTANCE.getLinkParam();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_PARAM__SOURCE = eINSTANCE.getLinkParam_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_PARAM__TARGET = eINSTANCE.getLinkParam_Target();

		/**
		 * The meta object literal for the '{@link task.LinkDataType <em>Link Data Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see task.LinkDataType
		 * @see task.impl.TaskPackageImpl#getLinkDataType()
		 * @generated
		 */
		EEnum LINK_DATA_TYPE = eINSTANCE.getLinkDataType();

	}

} //TaskPackage
