/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import port.PortDataDriver;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link Driver</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link task.LinkDriver#getSource <em>Source</em>}</li>
 *   <li>{@link task.LinkDriver#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see task.TaskPackage#getLinkDriver()
 * @model
 * @generated
 */
public interface LinkDriver extends LinkGeneric {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(PortDataDriver)
	 * @see task.TaskPackage#getLinkDriver_Source()
	 * @model required="true"
	 * @generated
	 */
	PortDataDriver getSource();

	/**
	 * Sets the value of the '{@link task.LinkDriver#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(PortDataDriver value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(PortDataDriver)
	 * @see task.TaskPackage#getLinkDriver_Target()
	 * @model required="true"
	 * @generated
	 */
	PortDataDriver getTarget();

	/**
	 * Sets the value of the '{@link task.LinkDriver#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(PortDataDriver value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkCoherentVariable(DiagnosticChain drl3, Map<Object, Object> drl4);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkLinkDriver(DiagnosticChain drl3, Map<Object, Object> drl4);

} // LinkDriver
