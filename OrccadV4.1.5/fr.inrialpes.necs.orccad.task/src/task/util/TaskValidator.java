/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task.util;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import task.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see task.TaskPackage
 * @generated
 */
public class TaskValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final TaskValidator INSTANCE = new TaskValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "task";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Loop' of 'Robot Task'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ROBOT_TASK__CHECK_LOOP = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Robot Task' of 'Robot Task'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ROBOT_TASK__CHECK_ROBOT_TASK = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Loop' of 'Link Generic'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LINK_GENERIC__CHECK_LOOP = 3;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Coherent Variable' of 'Link Data'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LINK_DATA__CHECK_COHERENT_VARIABLE = 4;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Link Data' of 'Link Data'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LINK_DATA__CHECK_LINK_DATA = 5;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Coherent Variable' of 'Link Driver'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LINK_DRIVER__CHECK_COHERENT_VARIABLE = 6;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Link Driver' of 'Link Driver'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LINK_DRIVER__CHECK_LINK_DRIVER = 7;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Loop On Module' of 'Link Param'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LINK_PARAM__CHECK_LOOP_ON_MODULE = 8;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Link Param' of 'Link Param'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LINK_PARAM__CHECK_LINK_PARAM = 9;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 9;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return TaskPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case TaskPackage.ROBOT_TASK:
				return validateRobotTask((RobotTask)value, diagnostics, context);
			case TaskPackage.LINK_GENERIC:
				return validateLinkGeneric((LinkGeneric)value, diagnostics, context);
			case TaskPackage.LINK_DATA:
				return validateLinkData((LinkData)value, diagnostics, context);
			case TaskPackage.LINK_DRIVER:
				return validateLinkDriver((LinkDriver)value, diagnostics, context);
			case TaskPackage.LINK_PARAM:
				return validateLinkParam((LinkParam)value, diagnostics, context);
			case TaskPackage.LINK_DATA_TYPE:
				return validateLinkDataType((LinkDataType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRobotTask(RobotTask robotTask, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(robotTask, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(robotTask, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(robotTask, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(robotTask, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(robotTask, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(robotTask, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(robotTask, diagnostics, context);
		if (result || diagnostics != null) result &= validateRobotTask_checkLoop(robotTask, diagnostics, context);
		if (result || diagnostics != null) result &= validateRobotTask_checkRobotTask(robotTask, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkLoop constraint of '<em>Robot Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRobotTask_checkLoop(RobotTask robotTask, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return robotTask.checkLoop(diagnostics, context);
	}

	/**
	 * Validates the checkRobotTask constraint of '<em>Robot Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRobotTask_checkRobotTask(RobotTask robotTask, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return robotTask.checkRobotTask(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkGeneric(LinkGeneric linkGeneric, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(linkGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(linkGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(linkGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(linkGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(linkGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(linkGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(linkGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinkGeneric_checkLoop(linkGeneric, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkLoop constraint of '<em>Link Generic</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkGeneric_checkLoop(LinkGeneric linkGeneric, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linkGeneric.checkLoop(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkData(LinkData linkData, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(linkData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(linkData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(linkData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(linkData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(linkData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(linkData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(linkData, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinkGeneric_checkLoop(linkData, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinkData_checkCoherentVariable(linkData, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinkData_checkLinkData(linkData, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkCoherentVariable constraint of '<em>Link Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkData_checkCoherentVariable(LinkData linkData, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linkData.checkCoherentVariable(diagnostics, context);
	}

	/**
	 * Validates the checkLinkData constraint of '<em>Link Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkData_checkLinkData(LinkData linkData, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linkData.checkLinkData(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkDriver(LinkDriver linkDriver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(linkDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(linkDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(linkDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(linkDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(linkDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(linkDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(linkDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinkGeneric_checkLoop(linkDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinkDriver_checkCoherentVariable(linkDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinkDriver_checkLinkDriver(linkDriver, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkCoherentVariable constraint of '<em>Link Driver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkDriver_checkCoherentVariable(LinkDriver linkDriver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linkDriver.checkCoherentVariable(diagnostics, context);
	}

	/**
	 * Validates the checkLinkDriver constraint of '<em>Link Driver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkDriver_checkLinkDriver(LinkDriver linkDriver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linkDriver.checkLinkDriver(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkParam(LinkParam linkParam, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(linkParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(linkParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(linkParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(linkParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(linkParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(linkParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(linkParam, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinkGeneric_checkLoop(linkParam, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinkParam_checkLoopOnModule(linkParam, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinkParam_checkLinkParam(linkParam, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkLoopOnModule constraint of '<em>Link Param</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkParam_checkLoopOnModule(LinkParam linkParam, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linkParam.checkLoopOnModule(diagnostics, context);
	}

	/**
	 * Validates the checkLinkParam constraint of '<em>Link Param</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkParam_checkLinkParam(LinkParam linkParam, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linkParam.checkLinkParam(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkDataType(LinkDataType linkDataType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //TaskValidator
