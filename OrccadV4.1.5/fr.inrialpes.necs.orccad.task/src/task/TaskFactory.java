/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see task.TaskPackage
 * @generated
 */
public interface TaskFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TaskFactory eINSTANCE = task.impl.TaskFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Robot Task</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Robot Task</em>'.
	 * @generated
	 */
	RobotTask createRobotTask();

	/**
	 * Returns a new object of class '<em>Link Generic</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Link Generic</em>'.
	 * @generated
	 */
	LinkGeneric createLinkGeneric();

	/**
	 * Returns a new object of class '<em>Link Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Link Data</em>'.
	 * @generated
	 */
	LinkData createLinkData();

	/**
	 * Returns a new object of class '<em>Link Driver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Link Driver</em>'.
	 * @generated
	 */
	LinkDriver createLinkDriver();

	/**
	 * Returns a new object of class '<em>Link Param</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Link Param</em>'.
	 * @generated
	 */
	LinkParam createLinkParam();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	TaskPackage getTaskPackage();

} //TaskFactory
