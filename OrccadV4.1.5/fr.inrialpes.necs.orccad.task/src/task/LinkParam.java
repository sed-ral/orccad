/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;

import port.ExceptionT1;
import port.PortParam;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link Param</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link task.LinkParam#getSource <em>Source</em>}</li>
 *   <li>{@link task.LinkParam#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see task.TaskPackage#getLinkParam()
 * @model
 * @generated
 */
public interface LinkParam extends LinkGeneric {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(ExceptionT1)
	 * @see task.TaskPackage#getLinkParam_Source()
	 * @model required="true"
	 * @generated
	 */
	ExceptionT1 getSource();

	/**
	 * Sets the value of the '{@link task.LinkParam#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(ExceptionT1 value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(PortParam)
	 * @see task.TaskPackage#getLinkParam_Target()
	 * @model required="true"
	 * @generated
	 */
	PortParam getTarget();

	/**
	 * Sets the value of the '{@link task.LinkParam#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(PortParam value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkLoopOnModule(DiagnosticChain pl1, Map<Object, Object> pl2);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkLinkParam(DiagnosticChain pl1, Map<Object, Object> pl2);

} // LinkParam
