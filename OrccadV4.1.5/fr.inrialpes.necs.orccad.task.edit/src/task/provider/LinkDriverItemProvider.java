/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task.provider;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import module.ModuleAlgorithmic;
import module.ModulePhysical;
import module.ModulePhysicalSensor;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;

import port.PortData;
import port.PortDataDriver;
import port.PortDriver;
import port.PortIOType;
import port.provider.PortDataItemProvider;
import port.provider.PortDriverItemProvider;

import task.LinkDriver;
import task.RobotTask;
import task.TaskPackage;

/**
 * This is the item provider adapter for a {@link task.LinkDriver} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LinkDriverItemProvider
	extends LinkGenericItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkDriverItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSourcePropertyDescriptor(object);
			addTargetPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourcePropertyDescriptorORI(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinkDriver_source_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinkDriver_source_feature", "_UI_LinkDriver_type"),
				 TaskPackage.Literals.LINK_DRIVER__SOURCE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addSourcePropertyDescriptor(Object object) {

		itemPropertyDescriptors.add
		(new ItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_LinkDriver_source_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_LinkDriver_source_feature", "_UI_LinkDriver_type"),
						TaskPackage.Literals.LINK_DRIVER__SOURCE,
						true, // isSettable
						false, // multiline
						false, // sortChoices
						null,
						null,
						null) {
			@Override
			public Collection<?> getChoiceOfValues(Object object)
			{
				LinkDriver ldriver = (LinkDriver)object;
				RobotTask rt = (RobotTask)(ldriver.eContainer());
				List<PortDataDriver> result = new ArrayList<PortDataDriver>();
				if (rt == null) {
					System.out.println(	"getChoiceOfValues : RT of object == null \n");
					return result;
				}
				// Output Data Ports
				for (Iterator<ModuleAlgorithmic> itAlgo = rt.getModuleAlgorithmics().iterator(); itAlgo.hasNext(); ) {
					ModuleAlgorithmic obj = itAlgo.next();
					for (Iterator<PortData> itData = obj.getPortDatas().iterator(); itData.hasNext(); ) {
						PortDataDriver pData = itData.next();
						if (pData.getDirection() == PortIOType.OUTPUT) {
							result.add(pData);
						}
					}
				}
				// Output Driver Ports
				// from ModulePhysical
				for (Iterator<PortDriver> itDriver = rt.getModulePhysicalRobot().getPortDrivers().iterator(); itDriver.hasNext(); ) {
						PortDataDriver pData = itDriver.next();
						if (pData.getDirection() == PortIOType.OUTPUT) {
							result.add(pData);
						}
				}
				// from ModuleSensors
				for (Iterator<ModulePhysicalSensor> itSens = rt.getModulePhysicalSensors().iterator(); itSens.hasNext(); ) {
					ModulePhysicalSensor sens = itSens.next();
					for (Iterator<PortDriver> itDriver = sens.getPortDrivers().iterator(); itDriver.hasNext(); ) {
						PortDataDriver pData = itDriver.next();
						if (pData.getDirection() == PortIOType.OUTPUT) {
							result.add(pData);
						}
					}
				}
				return result;
			}
		});

	}

	/**
	 * This adds a property descriptor for the Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addTargetPropertyDescriptor(Object object) {
		
		itemPropertyDescriptors.add
		(new ItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_LinkDriver_target_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_LinkDriver_target_feature", "_UI_LinkDriver_type"),
						TaskPackage.Literals.LINK_DRIVER__TARGET,
						true, // isSettable
						false, // multiline
						false, // sortChoices
						null,
						null,
						null) {
			@Override
			public Collection<?> getChoiceOfValues(Object object)
			{
				LinkDriver ldriver = (LinkDriver)object;
				RobotTask rt = (RobotTask)(ldriver.eContainer());
				List<PortDataDriver> result = new ArrayList<PortDataDriver>();
				if (rt == null) {
					System.out.println(	"getChoiceOfValues : RT of object == null \n");
					return result;
				}
				// Input Data Ports
				for (Iterator<ModuleAlgorithmic> itAlgo = rt.getModuleAlgorithmics().iterator(); itAlgo.hasNext(); ) {
					ModuleAlgorithmic obj = itAlgo.next();
					for (Iterator<PortData> itData = obj.getPortDatas().iterator(); itData.hasNext(); ) {
						PortDataDriver pData = itData.next();
						if (pData.getDirection() == PortIOType.INPUT) {
							result.add(pData);
						}
					}
				}
				// Input Driver Ports
				// from ModulePhysical
				for (Iterator<PortDriver> itDriver = rt.getModulePhysicalRobot().getPortDrivers().iterator(); itDriver.hasNext(); ) {
						PortDataDriver pData = itDriver.next();
						if (pData.getDirection() == PortIOType.INPUT) {
							result.add(pData);
						}
				}
				// from ModuleSensors
				for (Iterator<ModulePhysicalSensor> itSens = rt.getModulePhysicalSensors().iterator(); itSens.hasNext(); ) {
					ModulePhysicalSensor sens = itSens.next();
					for (Iterator<PortDriver> itDriver = sens.getPortDrivers().iterator(); itDriver.hasNext(); ) {
						PortDataDriver pData = itDriver.next();
						if (pData.getDirection() == PortIOType.INPUT) {
							result.add(pData);
						}
					}
				}
				return result;
			}
		});
	/*
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinkDriver_target_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinkDriver_target_feature", "_UI_LinkDriver_type"),
				 TaskPackage.Literals.LINK_DRIVER__TARGET,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	*/
	}

	/**
	 * This returns LinkDriver.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LinkDriver"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		String label =null;
		/**if ((((LinkDriver)object).getSource() != null) && (((LinkDriver)object).getTarget() != null)) {
			String srcContainerName = (((LinkDriver)object).getSource() instanceof PortData)?((PortData)((LinkDriver)object).getSource()).getContainer().getName():((PortDriver)((LinkDriver)object).getSource()).getContainer().getName();
			String desContainerName = (((LinkDriver)object).getTarget() instanceof PortData)?((PortData)((LinkDriver)object).getTarget()).getContainer().getName():((PortDriver)((LinkDriver)object).getTarget()).getContainer().getName();

			label = srcContainerName+ "_" + ((LinkDriver)object).getSource().getPortName()+"->"+desContainerName+"_"+((LinkDriver)object).getTarget().getPortName();
		}**/
		
		if(((LinkDriver)object).getSource()!=null && ((LinkDriver)object).getSource() instanceof PortData && ((LinkDriver)object).getTarget()!=null && ((LinkDriver)object).getTarget() instanceof PortDriver){
			 PortData pData = (PortData)((LinkDriver)object).getSource();
			 PortDriver pDriver = (PortDriver)((LinkDriver)object).getTarget();
			 PortDataItemProvider pdip = new PortDataItemProvider(null);
			 PortDriverItemProvider pdrip = new PortDriverItemProvider(null);
 
			 label = ": " + pdip.getText(pData) + " -> " + pdrip.getText(pDriver);
			
		}
		
		if(((LinkDriver)object).getSource()!=null && ((LinkDriver)object).getSource() instanceof PortDriver && ((LinkDriver)object).getTarget()!=null && ((LinkDriver)object).getTarget() instanceof PortData){
			 PortData pData = (PortData)((LinkDriver)object).getTarget();
			 PortDriver pDriver = (PortDriver)((LinkDriver)object).getSource();
			 PortDataItemProvider pdip = new PortDataItemProvider(null);
			 PortDriverItemProvider pdrip = new PortDriverItemProvider(null);

			 label = ": " + pdrip.getText(pDriver) + " -> " + pdip.getText(pData);
			
		}		
		
		return label == null || label.length() == 0 ?
			getString("_UI_LinkDriver_type") :
			getString("_UI_LinkDriver_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
