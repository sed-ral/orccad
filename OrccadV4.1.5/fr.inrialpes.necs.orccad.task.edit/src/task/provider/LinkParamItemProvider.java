/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task.provider;


import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import port.ExceptionT1;
import port.PortData;
import port.PortDriver;
import port.PortParam;
import port.provider.ExceptionT1ItemProvider;
import port.provider.PortDataItemProvider;
import port.provider.PortDriverItemProvider;
import port.provider.PortParamItemProvider;

import task.LinkDriver;
import task.LinkParam;
import task.TaskPackage;

/**
 * This is the item provider adapter for a {@link task.LinkParam} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LinkParamItemProvider
	extends LinkGenericItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkParamItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSourcePropertyDescriptor(object);
			addTargetPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourcePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinkParam_source_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinkParam_source_feature", "_UI_LinkParam_type"),
				 TaskPackage.Literals.LINK_PARAM__SOURCE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinkParam_target_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinkParam_target_feature", "_UI_LinkParam_type"),
				 TaskPackage.Literals.LINK_PARAM__TARGET,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns LinkParam.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LinkParam"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		String label = getString("_UI_LinkParam_type");
/*		if ((((LinkParam)object).getSource() != null) && (((LinkParam)object).getTarget() != null))
			label = ((LinkParam)object).getSource().getContainer().getName()+"_"+ ((LinkParam)object).getSource().getPortName()+"->"+((LinkParam)object).getTarget().getContainer().getName()+"_"+((LinkParam)object).getTarget().getPortName();
*/
		
		if(((LinkParam)object).getSource()!=null && ((LinkParam)object).getTarget()!=null ){
			 ExceptionT1 exc = (ExceptionT1)((LinkParam)object).getSource();
			 PortParam pParam = (PortParam)((LinkParam)object).getTarget();
			 PortParamItemProvider ppip = new PortParamItemProvider(null);
			 ExceptionT1ItemProvider et1ip = new ExceptionT1ItemProvider(null);

			 label = ": " + et1ip.getText(exc) + " -> " + ppip.getText(pParam);
			
		}
		
		
		return label == null || label.length() == 0 ?
			getString("_UI_LinkParam_type") :
			getString("_UI_LinkParam_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
