/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package task.provider;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import module.ModuleAlgorithmic;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

import port.PortData;
import port.PortIOType;
import port.provider.PortDataItemProvider;
import task.LinkData;
import task.RobotTask;
import task.TaskPackage;

/**
 * This is the item provider adapter for a {@link task.LinkData} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class LinkDataItemProvider
	extends LinkGenericItemProvider
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkDataItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSourcePropertyDescriptor(object);
			addTargetPropertyDescriptor(object);
			addLinkTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourcePropertyDescriptorORI(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinkData_source_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinkData_source_feature", "_UI_LinkData_type"),
				 TaskPackage.Literals.LINK_DATA__SOURCE,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addSourcePropertyDescriptor(Object object) {
		
		itemPropertyDescriptors.add
		(new ItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_LinkData_source_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_LinkData_source_feature", "_UI_LinkData_type"),
						TaskPackage.Literals.LINK_DATA__SOURCE,
						true, // isSettable
						false, // multiline
						false, // sortChoices
						null,
						null,
						null) {
			@Override
			public Collection<?> getChoiceOfValues(Object object)
			{
				LinkData ldata = (LinkData)object;
				RobotTask ddd = (RobotTask)(ldata.eContainer());
				List<PortData> result = new ArrayList<PortData>();
				if (ddd == null) {
					System.out.println(	"getChoiceOfValues : RT of object == null \n");
					return result;
				}
				
				for (Iterator<ModuleAlgorithmic> itAlgo = ddd.getModuleAlgorithmics().iterator(); itAlgo.hasNext(); ) {
					ModuleAlgorithmic obj = itAlgo.next();
					for (Iterator<PortData> itData = obj.getPortDatas().iterator(); itData.hasNext(); ) {
						PortData pData = itData.next();
						if (pData.getDirection() == PortIOType.OUTPUT) {
							result.add(pData);
						}
					}					
				}
				return result;
			}
		});

	}

	/**
	 * This adds a property descriptor for the Target feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addTargetPropertyDescriptor(Object object) {
		
		itemPropertyDescriptors.add
		(new ItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_LinkData_target_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_LinkData_target_feature", "_UI_LinkData_type"),
						TaskPackage.Literals.LINK_DATA__TARGET,
						true, // isSettable
						false, // multiline
						false, // sortChoices
						null,
						null,
						null) {
			@Override
			public Collection<?> getChoiceOfValues(Object object)
			{
				LinkData ldata = (LinkData)object;
				RobotTask rt = (RobotTask)(ldata.eContainer());
				List<PortData> result = new ArrayList<PortData>();
				if (rt == null) {
					System.out.println(	"getChoiceOfValues : RT of object == null \n");
					return result;
				}
				
				for (Iterator<ModuleAlgorithmic> itAlgo = rt.getModuleAlgorithmics().iterator(); itAlgo.hasNext(); ) {
					ModuleAlgorithmic obj = itAlgo.next();
					for (Iterator<PortData> itData = obj.getPortDatas().iterator(); itData.hasNext(); ) {
						PortData pData = itData.next();
						if (pData.getDirection() == PortIOType.INPUT) {
							boolean isAlreadyUsed = false;
							/*
							for (Iterator<LinkData> itLink = rt.getLinkDatas().iterator(); itLink.hasNext();) {
								LinkData ld = itLink.next();
								if (ld.getTarget().equals(pData)) {
									System.out.println("deja un lien data sur cet port data");
									isAlreadyUsed = true;
									break;
								}				
							}
							for (Iterator<LinkDriver> it = rt.getLinkDrivers().iterator(); it
									.hasNext();) {
								LinkDriver ld = it.next();
								if (ld.getTarget().equals(target)) {
									System.out.println("deja un lien driver sur cet port data");
									isAlreadyUsed = true;
									break;
								}
							}
							*/
							if (!isAlreadyUsed)
								result.add(pData);
						}
					}					
				}
				return result;
			}
		});

		/*
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinkData_target_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinkData_target_feature", "_UI_LinkData_type"),
				 TaskPackage.Literals.LINK_DATA__TARGET,
				 true,
				 false,
				 true,
				 null,
				 null,
				 null));
		*/
	}

	/**
	 * This adds a property descriptor for the Link Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLinkTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_LinkData_linkType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_LinkData_linkType_feature", "_UI_LinkData_type"),
				 TaskPackage.Literals.LINK_DATA__LINK_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This returns LinkData.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/LinkData"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		String label = getString("_UI_LinkData_type");
		/**if ((((LinkData)object).getSource() != null) && (((LinkData)object).getTarget() != null))
			label = ((LinkData)object).getSource().getContainer().getName()+"_"+ ((LinkData)object).getSource().getPortName()+"->"+((LinkData)object).getTarget().getContainer().getName()+"_"+((LinkData)object).getTarget().getPortName();
		**/
		
		 PortDataItemProvider pdip = new PortDataItemProvider(null);

		 if(((LinkData)object).getSource() != null && ((LinkData)object).getTarget() != null)
			 	 label = ": " + pdip.getText((PortData)((LinkData)object).getSource()) + " -> " + pdip.getText((PortData)((LinkData)object).getTarget());
		
		return label == null || label.length() == 0 ?
			getString("_UI_LinkData_type") :
			getString("_UI_LinkData_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(LinkData.class)) {
			case TaskPackage.LINK_DATA__LINK_TYPE:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
