/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Exception</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see port.PortPackage#getEventException()
 * @model
 * @generated
 */
public interface EventException extends PortEvent {
} // EventException
