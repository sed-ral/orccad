/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;

import module.ModuleGeneric;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Condition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link port.EventCondition#getEventType <em>Event Type</em>}</li>
 *   <li>{@link port.EventCondition#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see port.PortPackage#getEventCondition()
 * @model
 * @generated
 */
public interface EventCondition extends PortEvent {
	/**
	 * Returns the value of the '<em><b>Event Type</b></em>' attribute.
	 * The literals are from the enumeration {@link port.EventConditionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Type</em>' attribute.
	 * @see port.EventConditionType
	 * @see #setEventType(EventConditionType)
	 * @see port.PortPackage#getEventCondition_EventType()
	 * @model
	 * @generated
	 */
	EventConditionType getEventType();

	/**
	 * Sets the value of the '{@link port.EventCondition#getEventType <em>Event Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Type</em>' attribute.
	 * @see port.EventConditionType
	 * @see #getEventType()
	 * @generated
	 */
	void setEventType(EventConditionType value);

	/**
	 * Returns the value of the '<em><b>Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link module.ModuleGeneric#getEventConditions <em>Event Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' container reference.
	 * @see #setContainer(ModuleGeneric)
	 * @see port.PortPackage#getEventCondition_Container()
	 * @see module.ModuleGeneric#getEventConditions
	 * @model opposite="eventConditions" transient="false"
	 * @generated
	 */
	ModuleGeneric getContainer();

	/**
	 * Sets the value of the '{@link port.EventCondition#getContainer <em>Container</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' container reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(ModuleGeneric value);

} // EventCondition
