/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Event Exception Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see port.PortPackage#getEventExceptionType()
 * @model
 * @generated
 */
public enum EventExceptionType implements Enumerator {
	/**
	 * The '<em><b>EXCEPTION T1</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXCEPTION_T1_VALUE
	 * @generated
	 * @ordered
	 */
	EXCEPTION_T1(1, "EXCEPTION_T1", "EXCEPTION_T1"),

	/**
	 * The '<em><b>EXCEPTION T2</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXCEPTION_T2_VALUE
	 * @generated
	 * @ordered
	 */
	EXCEPTION_T2(2, "EXCEPTION_T2", "EXCEPTION_T2"),

	/**
	 * The '<em><b>EXCEPTION T3</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EXCEPTION_T3_VALUE
	 * @generated
	 * @ordered
	 */
	EXCEPTION_T3(3, "EXCEPTION_T3", "EXCEPTION_T3");

	/**
	 * The '<em><b>EXCEPTION T1</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EXCEPTION T1</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EXCEPTION_T1
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EXCEPTION_T1_VALUE = 1;

	/**
	 * The '<em><b>EXCEPTION T2</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EXCEPTION T2</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EXCEPTION_T2
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EXCEPTION_T2_VALUE = 2;

	/**
	 * The '<em><b>EXCEPTION T3</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EXCEPTION T3</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EXCEPTION_T3
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int EXCEPTION_T3_VALUE = 3;

	/**
	 * An array of all the '<em><b>Event Exception Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final EventExceptionType[] VALUES_ARRAY =
		new EventExceptionType[] {
			EXCEPTION_T1,
			EXCEPTION_T2,
			EXCEPTION_T3,
		};

	/**
	 * A public read-only list of all the '<em><b>Event Exception Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<EventExceptionType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Event Exception Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EventExceptionType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EventExceptionType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Event Exception Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EventExceptionType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			EventExceptionType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Event Exception Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EventExceptionType get(int value) {
		switch (value) {
			case EXCEPTION_T1_VALUE: return EXCEPTION_T1;
			case EXCEPTION_T2_VALUE: return EXCEPTION_T2;
			case EXCEPTION_T3_VALUE: return EXCEPTION_T3;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EventExceptionType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //EventExceptionType
