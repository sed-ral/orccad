/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port.impl;

import module.ModuleGeneric;
import module.ModulePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import port.ExceptionT1;
import port.PortPackage;
import port.PortVarType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Exception T1</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link port.impl.ExceptionT1Impl#getRow <em>Row</em>}</li>
 *   <li>{@link port.impl.ExceptionT1Impl#getCol <em>Col</em>}</li>
 *   <li>{@link port.impl.ExceptionT1Impl#getVarName <em>Var Name</em>}</li>
 *   <li>{@link port.impl.ExceptionT1Impl#getVarType <em>Var Type</em>}</li>
 *   <li>{@link port.impl.ExceptionT1Impl#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ExceptionT1Impl extends EventExceptionImpl implements ExceptionT1 {
	/**
	 * The default value of the '{@link #getRow() <em>Row</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRow()
	 * @generated
	 * @ordered
	 */
	protected static final int ROW_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getRow() <em>Row</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRow()
	 * @generated
	 * @ordered
	 */
	protected int row = ROW_EDEFAULT;

	/**
	 * The default value of the '{@link #getCol() <em>Col</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCol()
	 * @generated
	 * @ordered
	 */
	protected static final int COL_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getCol() <em>Col</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCol()
	 * @generated
	 * @ordered
	 */
	protected int col = COL_EDEFAULT;

	/**
	 * The default value of the '{@link #getVarName() <em>Var Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarName()
	 * @generated
	 * @ordered
	 */
	protected static final String VAR_NAME_EDEFAULT = "";

	/**
	 * The cached value of the '{@link #getVarName() <em>Var Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarName()
	 * @generated
	 * @ordered
	 */
	protected String varName = VAR_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVarType() <em>Var Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarType()
	 * @generated
	 * @ordered
	 */
	protected static final PortVarType VAR_TYPE_EDEFAULT = PortVarType.INTEGER;

	/**
	 * The cached value of the '{@link #getVarType() <em>Var Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarType()
	 * @generated
	 * @ordered
	 */
	protected PortVarType varType = VAR_TYPE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExceptionT1Impl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortPackage.Literals.EXCEPTION_T1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRow() {
		return row;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRow(int newRow) {
		int oldRow = row;
		row = newRow;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.EXCEPTION_T1__ROW, oldRow, row));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCol() {
		return col;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCol(int newCol) {
		int oldCol = col;
		col = newCol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.EXCEPTION_T1__COL, oldCol, col));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVarName() {
		return varName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVarName(String newVarName) {
		String oldVarName = varName;
		varName = newVarName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.EXCEPTION_T1__VAR_NAME, oldVarName, varName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortVarType getVarType() {
		return varType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVarType(PortVarType newVarType) {
		PortVarType oldVarType = varType;
		varType = newVarType == null ? VAR_TYPE_EDEFAULT : newVarType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.EXCEPTION_T1__VAR_TYPE, oldVarType, varType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModuleGeneric getContainer() {
		if (eContainerFeatureID() != PortPackage.EXCEPTION_T1__CONTAINER) return null;
		return (ModuleGeneric)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainer(ModuleGeneric newContainer, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContainer, PortPackage.EXCEPTION_T1__CONTAINER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainer(ModuleGeneric newContainer) {
		if (newContainer != eInternalContainer() || (eContainerFeatureID() != PortPackage.EXCEPTION_T1__CONTAINER && newContainer != null)) {
			if (EcoreUtil.isAncestor(this, newContainer))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainer != null)
				msgs = ((InternalEObject)newContainer).eInverseAdd(this, ModulePackage.MODULE_GENERIC__EVENT_T1S, ModuleGeneric.class, msgs);
			msgs = basicSetContainer(newContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.EXCEPTION_T1__CONTAINER, newContainer, newContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.EXCEPTION_T1__CONTAINER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContainer((ModuleGeneric)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.EXCEPTION_T1__CONTAINER:
				return basicSetContainer(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case PortPackage.EXCEPTION_T1__CONTAINER:
				return eInternalContainer().eInverseRemove(this, ModulePackage.MODULE_GENERIC__EVENT_T1S, ModuleGeneric.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PortPackage.EXCEPTION_T1__ROW:
				return getRow();
			case PortPackage.EXCEPTION_T1__COL:
				return getCol();
			case PortPackage.EXCEPTION_T1__VAR_NAME:
				return getVarName();
			case PortPackage.EXCEPTION_T1__VAR_TYPE:
				return getVarType();
			case PortPackage.EXCEPTION_T1__CONTAINER:
				return getContainer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PortPackage.EXCEPTION_T1__ROW:
				if(((Integer)newValue) > 0  ) // added by boudinf
					setRow((Integer)newValue);
				return;
			case PortPackage.EXCEPTION_T1__COL:
				if(((Integer)newValue) > 0  ) // added by boudinf
					setCol((Integer)newValue);
				return;
			case PortPackage.EXCEPTION_T1__VAR_NAME:
				if(((String)newValue).matches("[a-zA-Z_0-9]*") ) // added by boudinf
					setVarName((String)newValue);
				return;
			case PortPackage.EXCEPTION_T1__VAR_TYPE:
				setVarType((PortVarType)newValue);
				return;
			case PortPackage.EXCEPTION_T1__CONTAINER:
				setContainer((ModuleGeneric)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PortPackage.EXCEPTION_T1__ROW:
				setRow(ROW_EDEFAULT);
				return;
			case PortPackage.EXCEPTION_T1__COL:
				setCol(COL_EDEFAULT);
				return;
			case PortPackage.EXCEPTION_T1__VAR_NAME:
				setVarName(VAR_NAME_EDEFAULT);
				return;
			case PortPackage.EXCEPTION_T1__VAR_TYPE:
				setVarType(VAR_TYPE_EDEFAULT);
				return;
			case PortPackage.EXCEPTION_T1__CONTAINER:
				setContainer((ModuleGeneric)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PortPackage.EXCEPTION_T1__ROW:
				return row != ROW_EDEFAULT;
			case PortPackage.EXCEPTION_T1__COL:
				return col != COL_EDEFAULT;
			case PortPackage.EXCEPTION_T1__VAR_NAME:
				return VAR_NAME_EDEFAULT == null ? varName != null : !VAR_NAME_EDEFAULT.equals(varName);
			case PortPackage.EXCEPTION_T1__VAR_TYPE:
				return varType != VAR_TYPE_EDEFAULT;
			case PortPackage.EXCEPTION_T1__CONTAINER:
				return getContainer() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (row: ");
		result.append(row);
		result.append(", col: ");
		result.append(col);
		result.append(", varName: ");
		result.append(varName);
		result.append(", varType: ");
		result.append(varType);
		result.append(')');
		return result.toString();
	}

} //ExceptionT1Impl
