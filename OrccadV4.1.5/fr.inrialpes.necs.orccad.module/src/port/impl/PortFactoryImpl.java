/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import port.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PortFactoryImpl extends EFactoryImpl implements PortFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PortFactory init() {
		try {
			PortFactory thePortFactory = (PortFactory)EPackage.Registry.INSTANCE.getEFactory("http://port"); 
			if (thePortFactory != null) {
				return thePortFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new PortFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case PortPackage.PORT_GENERIC: return createPortGeneric();
			case PortPackage.PORT_DATA_DRIVER: return createPortDataDriver();
			case PortPackage.PORT_DATA: return createPortData();
			case PortPackage.PORT_DRIVER: return createPortDriver();
			case PortPackage.PORT_PARAM: return createPortParam();
			case PortPackage.PORT_EVENT: return createPortEvent();
			case PortPackage.EVENT_EXCEPTION: return createEventException();
			case PortPackage.EVENT_CONDITION: return createEventCondition();
			case PortPackage.EXCEPTION_T1: return createExceptionT1();
			case PortPackage.EXCEPTION_T2: return createExceptionT2();
			case PortPackage.EXCEPTION_T3: return createExceptionT3();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case PortPackage.EVENT_EXCEPTION_TYPE:
				return createEventExceptionTypeFromString(eDataType, initialValue);
			case PortPackage.EVENT_CONDITION_TYPE:
				return createEventConditionTypeFromString(eDataType, initialValue);
			case PortPackage.PORT_PARAM_LEVEL_TYPE:
				return createPortParamLevelTypeFromString(eDataType, initialValue);
			case PortPackage.PORT_IO_TYPE:
				return createPortIOTypeFromString(eDataType, initialValue);
			case PortPackage.PORT_PARAM_IO_TYPE:
				return createPortParamIOTypeFromString(eDataType, initialValue);
			case PortPackage.PORT_VAR_TYPE:
				return createPortVarTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case PortPackage.EVENT_EXCEPTION_TYPE:
				return convertEventExceptionTypeToString(eDataType, instanceValue);
			case PortPackage.EVENT_CONDITION_TYPE:
				return convertEventConditionTypeToString(eDataType, instanceValue);
			case PortPackage.PORT_PARAM_LEVEL_TYPE:
				return convertPortParamLevelTypeToString(eDataType, instanceValue);
			case PortPackage.PORT_IO_TYPE:
				return convertPortIOTypeToString(eDataType, instanceValue);
			case PortPackage.PORT_PARAM_IO_TYPE:
				return convertPortParamIOTypeToString(eDataType, instanceValue);
			case PortPackage.PORT_VAR_TYPE:
				return convertPortVarTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortGeneric createPortGeneric() {
		PortGenericImpl portGeneric = new PortGenericImpl();
		return portGeneric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDataDriver createPortDataDriver() {
		PortDataDriverImpl portDataDriver = new PortDataDriverImpl();
		return portDataDriver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortData createPortData() {
		PortDataImpl portData = new PortDataImpl();
		return portData;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortDriver createPortDriver() {
		PortDriverImpl portDriver = new PortDriverImpl();
		return portDriver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortParam createPortParam() {
		PortParamImpl portParam = new PortParamImpl();
		return portParam;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortEvent createPortEvent() {
		PortEventImpl portEvent = new PortEventImpl();
		return portEvent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventException createEventException() {
		EventExceptionImpl eventException = new EventExceptionImpl();
		return eventException;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventCondition createEventCondition() {
		EventConditionImpl eventCondition = new EventConditionImpl();
		return eventCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionT1 createExceptionT1() {
		ExceptionT1Impl exceptionT1 = new ExceptionT1Impl();
		return exceptionT1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionT2 createExceptionT2() {
		ExceptionT2Impl exceptionT2 = new ExceptionT2Impl();
		return exceptionT2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExceptionT3 createExceptionT3() {
		ExceptionT3Impl exceptionT3 = new ExceptionT3Impl();
		return exceptionT3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventExceptionType createEventExceptionTypeFromString(EDataType eDataType, String initialValue) {
		EventExceptionType result = EventExceptionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEventExceptionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventConditionType createEventConditionTypeFromString(EDataType eDataType, String initialValue) {
		EventConditionType result = EventConditionType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEventConditionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortParamLevelType createPortParamLevelTypeFromString(EDataType eDataType, String initialValue) {
		PortParamLevelType result = PortParamLevelType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPortParamLevelTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortIOType createPortIOTypeFromString(EDataType eDataType, String initialValue) {
		PortIOType result = PortIOType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPortIOTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortParamIOType createPortParamIOTypeFromString(EDataType eDataType, String initialValue) {
		PortParamIOType result = PortParamIOType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPortParamIOTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortVarType createPortVarTypeFromString(EDataType eDataType, String initialValue) {
		PortVarType result = PortVarType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPortVarTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortPackage getPortPackage() {
		return (PortPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static PortPackage getPackage() {
		return PortPackage.eINSTANCE;
	}

} //PortFactoryImpl
