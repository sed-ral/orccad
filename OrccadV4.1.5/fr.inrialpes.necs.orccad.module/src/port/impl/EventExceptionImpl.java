/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port.impl;

import org.eclipse.emf.ecore.EClass;

import port.EventException;
import port.PortPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Exception</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class EventExceptionImpl extends PortEventImpl implements EventException {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventExceptionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortPackage.Literals.EVENT_EXCEPTION;
	}

} //EventExceptionImpl
