/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port.impl;

import java.util.Map;

import module.ModuleGeneric;
import module.ModulePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.EcoreUtil;

import port.PortPackage;
import port.PortParam;
import port.PortParamIOType;
import port.PortParamLevelType;
import port.PortVarType;

import port.util.PortValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Param</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link port.impl.PortParamImpl#getRow <em>Row</em>}</li>
 *   <li>{@link port.impl.PortParamImpl#getCol <em>Col</em>}</li>
 *   <li>{@link port.impl.PortParamImpl#getVarName <em>Var Name</em>}</li>
 *   <li>{@link port.impl.PortParamImpl#getVarType <em>Var Type</em>}</li>
 *   <li>{@link port.impl.PortParamImpl#getVarDefaultValue <em>Var Default Value</em>}</li>
 *   <li>{@link port.impl.PortParamImpl#getLevelType <em>Level Type</em>}</li>
 *   <li>{@link port.impl.PortParamImpl#getDirection <em>Direction</em>}</li>
 *   <li>{@link port.impl.PortParamImpl#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class PortParamImpl extends PortGenericImpl implements PortParam {
	/**
	 * The default value of the '{@link #getRow() <em>Row</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRow()
	 * @generated
	 * @ordered
	 */
	protected static final int ROW_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getRow() <em>Row</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRow()
	 * @generated
	 * @ordered
	 */
	protected int row = ROW_EDEFAULT;

	/**
	 * The default value of the '{@link #getCol() <em>Col</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCol()
	 * @generated
	 * @ordered
	 */
	protected static final int COL_EDEFAULT = 1;

	/**
	 * The cached value of the '{@link #getCol() <em>Col</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCol()
	 * @generated
	 * @ordered
	 */
	protected int col = COL_EDEFAULT;

	/**
	 * The default value of the '{@link #getVarName() <em>Var Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarName()
	 * @generated
	 * @ordered
	 */
	protected static final String VAR_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVarName() <em>Var Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarName()
	 * @generated
	 * @ordered
	 */
	protected String varName = VAR_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getVarType() <em>Var Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarType()
	 * @generated
	 * @ordered
	 */
	protected static final PortVarType VAR_TYPE_EDEFAULT = PortVarType.INTEGER;

	/**
	 * The cached value of the '{@link #getVarType() <em>Var Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarType()
	 * @generated
	 * @ordered
	 */
	protected PortVarType varType = VAR_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getVarDefaultValue() <em>Var Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarDefaultValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VAR_DEFAULT_VALUE_EDEFAULT = "0";

	/**
	 * The cached value of the '{@link #getVarDefaultValue() <em>Var Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVarDefaultValue()
	 * @generated
	 * @ordered
	 */
	protected String varDefaultValue = VAR_DEFAULT_VALUE_EDEFAULT;

	/**
	 * The default value of the '{@link #getLevelType() <em>Level Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevelType()
	 * @generated
	 * @ordered
	 */
	protected static final PortParamLevelType LEVEL_TYPE_EDEFAULT = PortParamLevelType.MODULE;

	/**
	 * The cached value of the '{@link #getLevelType() <em>Level Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevelType()
	 * @generated
	 * @ordered
	 */
	protected PortParamLevelType levelType = LEVEL_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDirection() <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected static final PortParamIOType DIRECTION_EDEFAULT = PortParamIOType.INPUT_OUTPUT;

	/**
	 * The cached value of the '{@link #getDirection() <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDirection()
	 * @generated
	 * @ordered
	 */
	protected PortParamIOType direction = DIRECTION_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortParamImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PortPackage.Literals.PORT_PARAM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRow() {
		return row;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRow(int newRow) {
		int oldRow = row;
		row = newRow;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.PORT_PARAM__ROW, oldRow, row));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCol() {
		return col;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCol(int newCol) {
		int oldCol = col;
		col = newCol;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.PORT_PARAM__COL, oldCol, col));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVarName() {
		return varName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVarName(String newVarName) {
		String oldVarName = varName;
		varName = newVarName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.PORT_PARAM__VAR_NAME, oldVarName, varName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortVarType getVarType() {
		return varType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVarType(PortVarType newVarType) {
		PortVarType oldVarType = varType;
		varType = newVarType == null ? VAR_TYPE_EDEFAULT : newVarType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.PORT_PARAM__VAR_TYPE, oldVarType, varType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVarDefaultValue() {
		return varDefaultValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVarDefaultValue(String newVarDefaultValue) {
		String oldVarDefaultValue = varDefaultValue;
		varDefaultValue = newVarDefaultValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.PORT_PARAM__VAR_DEFAULT_VALUE, oldVarDefaultValue, varDefaultValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortParamLevelType getLevelType() {
		return levelType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevelType(PortParamLevelType newLevelType) {
		PortParamLevelType oldLevelType = levelType;
		levelType = newLevelType == null ? LEVEL_TYPE_EDEFAULT : newLevelType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.PORT_PARAM__LEVEL_TYPE, oldLevelType, levelType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortParamIOType getDirection() {
		return direction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDirection(PortParamIOType newDirection) {
		PortParamIOType oldDirection = direction;
		direction = newDirection == null ? DIRECTION_EDEFAULT : newDirection;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.PORT_PARAM__DIRECTION, oldDirection, direction));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModuleGeneric getContainer() {
		if (eContainerFeatureID() != PortPackage.PORT_PARAM__CONTAINER) return null;
		return (ModuleGeneric)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainer(ModuleGeneric newContainer, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newContainer, PortPackage.PORT_PARAM__CONTAINER, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainer(ModuleGeneric newContainer) {
		if (newContainer != eInternalContainer() || (eContainerFeatureID() != PortPackage.PORT_PARAM__CONTAINER && newContainer != null)) {
			if (EcoreUtil.isAncestor(this, newContainer))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainer != null)
				msgs = ((InternalEObject)newContainer).eInverseAdd(this, ModulePackage.MODULE_GENERIC__PORT_PARAMS, ModuleGeneric.class, msgs);
			msgs = basicSetContainer(newContainer, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PortPackage.PORT_PARAM__CONTAINER, newContainer, newContainer));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean checkVarDefaultValue(DiagnosticChain pg1, Map<Object, Object> pg2) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (pg1 != null) {
				pg1.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 PortValidator.DIAGNOSTIC_SOURCE,
						 PortValidator.PORT_PARAM__CHECK_VAR_DEFAULT_VALUE,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkVarDefaultValue", EObjectValidator.getObjectLabel(this, pg2) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.PORT_PARAM__CONTAINER:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetContainer((ModuleGeneric)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PortPackage.PORT_PARAM__CONTAINER:
				return basicSetContainer(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case PortPackage.PORT_PARAM__CONTAINER:
				return eInternalContainer().eInverseRemove(this, ModulePackage.MODULE_GENERIC__PORT_PARAMS, ModuleGeneric.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PortPackage.PORT_PARAM__ROW:
				return getRow();
			case PortPackage.PORT_PARAM__COL:
				return getCol();
			case PortPackage.PORT_PARAM__VAR_NAME:
				return getVarName();
			case PortPackage.PORT_PARAM__VAR_TYPE:
				return getVarType();
			case PortPackage.PORT_PARAM__VAR_DEFAULT_VALUE:
				return getVarDefaultValue();
			case PortPackage.PORT_PARAM__LEVEL_TYPE:
				return getLevelType();
			case PortPackage.PORT_PARAM__DIRECTION:
				return getDirection();
			case PortPackage.PORT_PARAM__CONTAINER:
				return getContainer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PortPackage.PORT_PARAM__ROW:
				if((Integer)newValue>0) // added by boudinf
					setRow((Integer)newValue);
				return;
			case PortPackage.PORT_PARAM__COL:
				if((Integer)newValue>0) // added by boudinf
					setCol((Integer)newValue);
				return;
			case PortPackage.PORT_PARAM__VAR_NAME:
				if(((String)newValue).matches("[a-zA-Z_0-9]*"))// added by boudinf
				setVarName((String)newValue);
				return;
			case PortPackage.PORT_PARAM__VAR_TYPE:
				setVarType((PortVarType)newValue);
				return;
			case PortPackage.PORT_PARAM__VAR_DEFAULT_VALUE:
				setVarDefaultValue((String)newValue);
				return;
			case PortPackage.PORT_PARAM__LEVEL_TYPE:
				setLevelType((PortParamLevelType)newValue);
				return;
			case PortPackage.PORT_PARAM__DIRECTION:
				setDirection((PortParamIOType)newValue);
				return;
			case PortPackage.PORT_PARAM__CONTAINER:
				setContainer((ModuleGeneric)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PortPackage.PORT_PARAM__ROW:
				setRow(ROW_EDEFAULT);
				return;
			case PortPackage.PORT_PARAM__COL:
				setCol(COL_EDEFAULT);
				return;
			case PortPackage.PORT_PARAM__VAR_NAME:
				setVarName(VAR_NAME_EDEFAULT);
				return;
			case PortPackage.PORT_PARAM__VAR_TYPE:
				setVarType(VAR_TYPE_EDEFAULT);
				return;
			case PortPackage.PORT_PARAM__VAR_DEFAULT_VALUE:
				setVarDefaultValue(VAR_DEFAULT_VALUE_EDEFAULT);
				return;
			case PortPackage.PORT_PARAM__LEVEL_TYPE:
				setLevelType(LEVEL_TYPE_EDEFAULT);
				return;
			case PortPackage.PORT_PARAM__DIRECTION:
				setDirection(DIRECTION_EDEFAULT);
				return;
			case PortPackage.PORT_PARAM__CONTAINER:
				setContainer((ModuleGeneric)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PortPackage.PORT_PARAM__ROW:
				return row != ROW_EDEFAULT;
			case PortPackage.PORT_PARAM__COL:
				return col != COL_EDEFAULT;
			case PortPackage.PORT_PARAM__VAR_NAME:
				return VAR_NAME_EDEFAULT == null ? varName != null : !VAR_NAME_EDEFAULT.equals(varName);
			case PortPackage.PORT_PARAM__VAR_TYPE:
				return varType != VAR_TYPE_EDEFAULT;
			case PortPackage.PORT_PARAM__VAR_DEFAULT_VALUE:
				return VAR_DEFAULT_VALUE_EDEFAULT == null ? varDefaultValue != null : !VAR_DEFAULT_VALUE_EDEFAULT.equals(varDefaultValue);
			case PortPackage.PORT_PARAM__LEVEL_TYPE:
				return levelType != LEVEL_TYPE_EDEFAULT;
			case PortPackage.PORT_PARAM__DIRECTION:
				return direction != DIRECTION_EDEFAULT;
			case PortPackage.PORT_PARAM__CONTAINER:
				return getContainer() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (row: ");
		result.append(row);
		result.append(", col: ");
		result.append(col);
		result.append(", varName: ");
		result.append(varName);
		result.append(", varType: ");
		result.append(varType);
		result.append(", varDefaultValue: ");
		result.append(varDefaultValue);
		result.append(", levelType: ");
		result.append(levelType);
		result.append(", direction: ");
		result.append(direction);
		result.append(')');
		return result.toString();
	}

} //PortParamImpl
