/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port.impl;

import constraint.ConstraintPackage;

import constraint.impl.ConstraintPackageImpl;

import module.ModulePackage;

import module.impl.ModulePackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import port.EventCondition;
import port.EventConditionType;
import port.EventException;
import port.EventExceptionType;
import port.ExceptionT1;
import port.ExceptionT2;
import port.ExceptionT3;
import port.PortData;
import port.PortDataDriver;
import port.PortDriver;
import port.PortEvent;
import port.PortFactory;
import port.PortGeneric;
import port.PortIOType;
import port.PortPackage;
import port.PortParam;
import port.PortParamIOType;
import port.PortParamLevelType;
import port.PortVarType;

import port.util.PortValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PortPackageImpl extends EPackageImpl implements PortPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portGenericEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portDataDriverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portDataEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portDriverEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portParamEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass portEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventExceptionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventConditionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exceptionT1EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exceptionT2EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass exceptionT3EClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum eventExceptionTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum eventConditionTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum portParamLevelTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum portIOTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum portParamIOTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum portVarTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see port.PortPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private PortPackageImpl() {
		super(eNS_URI, PortFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link PortPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static PortPackage init() {
		if (isInited) return (PortPackage)EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI);

		// Obtain or create and register package
		PortPackageImpl thePortPackage = (PortPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof PortPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new PortPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		ModulePackageImpl theModulePackage = (ModulePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ModulePackage.eNS_URI) instanceof ModulePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ModulePackage.eNS_URI) : ModulePackage.eINSTANCE);
		ConstraintPackageImpl theConstraintPackage = (ConstraintPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) instanceof ConstraintPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) : ConstraintPackage.eINSTANCE);

		// Create package meta-data objects
		thePortPackage.createPackageContents();
		theModulePackage.createPackageContents();
		theConstraintPackage.createPackageContents();

		// Initialize created meta-data
		thePortPackage.initializePackageContents();
		theModulePackage.initializePackageContents();
		theConstraintPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(thePortPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return PortValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		thePortPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(PortPackage.eNS_URI, thePortPackage);
		return thePortPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortGeneric() {
		return portGenericEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortGeneric_Comment() {
		return (EAttribute)portGenericEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortGeneric_PortName() {
		return (EAttribute)portGenericEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortDataDriver() {
		return portDataDriverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortDataDriver_Row() {
		return (EAttribute)portDataDriverEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortDataDriver_Col() {
		return (EAttribute)portDataDriverEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortDataDriver_VarName() {
		return (EAttribute)portDataDriverEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortDataDriver_VarType() {
		return (EAttribute)portDataDriverEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortDataDriver_Direction() {
		return (EAttribute)portDataDriverEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortData() {
		return portDataEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortData_Container() {
		return (EReference)portDataEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortDriver() {
		return portDriverEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortDriver_Container() {
		return (EReference)portDriverEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortParam() {
		return portParamEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortParam_Row() {
		return (EAttribute)portParamEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortParam_Col() {
		return (EAttribute)portParamEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortParam_VarName() {
		return (EAttribute)portParamEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortParam_VarType() {
		return (EAttribute)portParamEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortParam_VarDefaultValue() {
		return (EAttribute)portParamEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortParam_LevelType() {
		return (EAttribute)portParamEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortParam_Direction() {
		return (EAttribute)portParamEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPortParam_Container() {
		return (EReference)portParamEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPortEvent() {
		return portEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortEvent_EventName() {
		return (EAttribute)portEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPortEvent_Direction() {
		return (EAttribute)portEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventException() {
		return eventExceptionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventCondition() {
		return eventConditionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventCondition_EventType() {
		return (EAttribute)eventConditionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventCondition_Container() {
		return (EReference)eventConditionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExceptionT1() {
		return exceptionT1EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExceptionT1_Row() {
		return (EAttribute)exceptionT1EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExceptionT1_Col() {
		return (EAttribute)exceptionT1EClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExceptionT1_VarName() {
		return (EAttribute)exceptionT1EClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExceptionT1_VarType() {
		return (EAttribute)exceptionT1EClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExceptionT1_Container() {
		return (EReference)exceptionT1EClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExceptionT2() {
		return exceptionT2EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExceptionT2_Container() {
		return (EReference)exceptionT2EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExceptionT3() {
		return exceptionT3EClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExceptionT3_Container() {
		return (EReference)exceptionT3EClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEventExceptionType() {
		return eventExceptionTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getEventConditionType() {
		return eventConditionTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPortParamLevelType() {
		return portParamLevelTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPortIOType() {
		return portIOTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPortParamIOType() {
		return portParamIOTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPortVarType() {
		return portVarTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortFactory getPortFactory() {
		return (PortFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		portGenericEClass = createEClass(PORT_GENERIC);
		createEAttribute(portGenericEClass, PORT_GENERIC__COMMENT);
		createEAttribute(portGenericEClass, PORT_GENERIC__PORT_NAME);

		portDataDriverEClass = createEClass(PORT_DATA_DRIVER);
		createEAttribute(portDataDriverEClass, PORT_DATA_DRIVER__ROW);
		createEAttribute(portDataDriverEClass, PORT_DATA_DRIVER__COL);
		createEAttribute(portDataDriverEClass, PORT_DATA_DRIVER__VAR_NAME);
		createEAttribute(portDataDriverEClass, PORT_DATA_DRIVER__VAR_TYPE);
		createEAttribute(portDataDriverEClass, PORT_DATA_DRIVER__DIRECTION);

		portDataEClass = createEClass(PORT_DATA);
		createEReference(portDataEClass, PORT_DATA__CONTAINER);

		portDriverEClass = createEClass(PORT_DRIVER);
		createEReference(portDriverEClass, PORT_DRIVER__CONTAINER);

		portParamEClass = createEClass(PORT_PARAM);
		createEAttribute(portParamEClass, PORT_PARAM__ROW);
		createEAttribute(portParamEClass, PORT_PARAM__COL);
		createEAttribute(portParamEClass, PORT_PARAM__VAR_NAME);
		createEAttribute(portParamEClass, PORT_PARAM__VAR_TYPE);
		createEAttribute(portParamEClass, PORT_PARAM__VAR_DEFAULT_VALUE);
		createEAttribute(portParamEClass, PORT_PARAM__LEVEL_TYPE);
		createEAttribute(portParamEClass, PORT_PARAM__DIRECTION);
		createEReference(portParamEClass, PORT_PARAM__CONTAINER);

		portEventEClass = createEClass(PORT_EVENT);
		createEAttribute(portEventEClass, PORT_EVENT__EVENT_NAME);
		createEAttribute(portEventEClass, PORT_EVENT__DIRECTION);

		eventExceptionEClass = createEClass(EVENT_EXCEPTION);

		eventConditionEClass = createEClass(EVENT_CONDITION);
		createEAttribute(eventConditionEClass, EVENT_CONDITION__EVENT_TYPE);
		createEReference(eventConditionEClass, EVENT_CONDITION__CONTAINER);

		exceptionT1EClass = createEClass(EXCEPTION_T1);
		createEAttribute(exceptionT1EClass, EXCEPTION_T1__ROW);
		createEAttribute(exceptionT1EClass, EXCEPTION_T1__COL);
		createEAttribute(exceptionT1EClass, EXCEPTION_T1__VAR_NAME);
		createEAttribute(exceptionT1EClass, EXCEPTION_T1__VAR_TYPE);
		createEReference(exceptionT1EClass, EXCEPTION_T1__CONTAINER);

		exceptionT2EClass = createEClass(EXCEPTION_T2);
		createEReference(exceptionT2EClass, EXCEPTION_T2__CONTAINER);

		exceptionT3EClass = createEClass(EXCEPTION_T3);
		createEReference(exceptionT3EClass, EXCEPTION_T3__CONTAINER);

		// Create enums
		eventExceptionTypeEEnum = createEEnum(EVENT_EXCEPTION_TYPE);
		eventConditionTypeEEnum = createEEnum(EVENT_CONDITION_TYPE);
		portParamLevelTypeEEnum = createEEnum(PORT_PARAM_LEVEL_TYPE);
		portIOTypeEEnum = createEEnum(PORT_IO_TYPE);
		portParamIOTypeEEnum = createEEnum(PORT_PARAM_IO_TYPE);
		portVarTypeEEnum = createEEnum(PORT_VAR_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ModulePackage theModulePackage = (ModulePackage)EPackage.Registry.INSTANCE.getEPackage(ModulePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		portDataDriverEClass.getESuperTypes().add(this.getPortGeneric());
		portDataEClass.getESuperTypes().add(this.getPortDataDriver());
		portDriverEClass.getESuperTypes().add(this.getPortDataDriver());
		portParamEClass.getESuperTypes().add(this.getPortGeneric());
		portEventEClass.getESuperTypes().add(this.getPortGeneric());
		eventExceptionEClass.getESuperTypes().add(this.getPortEvent());
		eventConditionEClass.getESuperTypes().add(this.getPortEvent());
		exceptionT1EClass.getESuperTypes().add(this.getEventException());
		exceptionT2EClass.getESuperTypes().add(this.getEventException());
		exceptionT3EClass.getESuperTypes().add(this.getEventException());

		// Initialize classes and features; add operations and parameters
		initEClass(portGenericEClass, PortGeneric.class, "PortGeneric", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPortGeneric_Comment(), ecorePackage.getEString(), "comment", null, 0, 1, PortGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortGeneric_PortName(), ecorePackage.getEString(), "portName", null, 0, 1, PortGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = addEOperation(portGenericEClass, ecorePackage.getEBoolean(), "checkPortName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "pg1", 0, 1, IS_UNIQUE, IS_ORDERED);
		EGenericType g1 = createEGenericType(ecorePackage.getEMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "pg2", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(portDataDriverEClass, PortDataDriver.class, "PortDataDriver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPortDataDriver_Row(), ecorePackage.getEInt(), "row", "1", 0, 1, PortDataDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortDataDriver_Col(), ecorePackage.getEInt(), "col", "1", 0, 1, PortDataDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortDataDriver_VarName(), ecorePackage.getEString(), "varName", "", 0, 1, PortDataDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortDataDriver_VarType(), this.getPortVarType(), "varType", "INTEGER", 0, 1, PortDataDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortDataDriver_Direction(), this.getPortIOType(), "direction", "OUTPUT", 0, 1, PortDataDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portDataEClass, PortData.class, "PortData", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPortData_Container(), theModulePackage.getModuleAlgorithmic(), theModulePackage.getModuleAlgorithmic_PortDatas(), "container", null, 1, 1, PortData.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portDriverEClass, PortDriver.class, "PortDriver", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPortDriver_Container(), theModulePackage.getModulePhysical(), theModulePackage.getModulePhysical_PortDrivers(), "container", null, 0, 1, PortDriver.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(portParamEClass, PortParam.class, "PortParam", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPortParam_Row(), ecorePackage.getEInt(), "row", "1", 0, 1, PortParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortParam_Col(), ecorePackage.getEInt(), "col", "1", 0, 1, PortParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortParam_VarName(), ecorePackage.getEString(), "varName", null, 0, 1, PortParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortParam_VarType(), this.getPortVarType(), "varType", "INTEGER", 0, 1, PortParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortParam_VarDefaultValue(), ecorePackage.getEString(), "varDefaultValue", "0", 1, 1, PortParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortParam_LevelType(), this.getPortParamLevelType(), "levelType", null, 0, 1, PortParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortParam_Direction(), this.getPortParamIOType(), "direction", "INPUT_OUTPUT", 0, 1, PortParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPortParam_Container(), theModulePackage.getModuleGeneric(), theModulePackage.getModuleGeneric_PortParams(), "container", null, 0, 1, PortParam.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(portParamEClass, ecorePackage.getEBoolean(), "checkVarDefaultValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDiagnosticChain(), "pg1", 0, 1, IS_UNIQUE, IS_ORDERED);
		g1 = createEGenericType(ecorePackage.getEMap());
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEJavaObject());
		g1.getETypeArguments().add(g2);
		addEParameter(op, g1, "pg2", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(portEventEClass, PortEvent.class, "PortEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPortEvent_EventName(), ecorePackage.getEString(), "eventName", null, 0, 1, PortEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPortEvent_Direction(), this.getPortIOType(), "direction", "OUTPUT", 0, 1, PortEvent.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventExceptionEClass, EventException.class, "EventException", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(eventConditionEClass, EventCondition.class, "EventCondition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEventCondition_EventType(), this.getEventConditionType(), "eventType", null, 0, 1, EventCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventCondition_Container(), theModulePackage.getModuleGeneric(), theModulePackage.getModuleGeneric_EventConditions(), "container", null, 0, 1, EventCondition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(exceptionT1EClass, ExceptionT1.class, "ExceptionT1", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExceptionT1_Row(), ecorePackage.getEInt(), "row", "1", 0, 1, ExceptionT1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExceptionT1_Col(), ecorePackage.getEInt(), "col", "1", 0, 1, ExceptionT1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExceptionT1_VarName(), ecorePackage.getEString(), "varName", "", 0, 1, ExceptionT1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExceptionT1_VarType(), this.getPortVarType(), "varType", "INTEGER", 0, 1, ExceptionT1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExceptionT1_Container(), theModulePackage.getModuleGeneric(), theModulePackage.getModuleGeneric_EventT1s(), "container", null, 0, 1, ExceptionT1.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(exceptionT2EClass, ExceptionT2.class, "ExceptionT2", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExceptionT2_Container(), theModulePackage.getModuleGeneric(), theModulePackage.getModuleGeneric_EventT2s(), "container", null, 0, 1, ExceptionT2.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(exceptionT3EClass, ExceptionT3.class, "ExceptionT3", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getExceptionT3_Container(), theModulePackage.getModuleGeneric(), theModulePackage.getModuleGeneric_EventT3s(), "container", null, 0, 1, ExceptionT3.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(eventExceptionTypeEEnum, EventExceptionType.class, "EventExceptionType");
		addEEnumLiteral(eventExceptionTypeEEnum, EventExceptionType.EXCEPTION_T1);
		addEEnumLiteral(eventExceptionTypeEEnum, EventExceptionType.EXCEPTION_T2);
		addEEnumLiteral(eventExceptionTypeEEnum, EventExceptionType.EXCEPTION_T3);

		initEEnum(eventConditionTypeEEnum, EventConditionType.class, "EventConditionType");
		addEEnumLiteral(eventConditionTypeEEnum, EventConditionType.PRE);
		addEEnumLiteral(eventConditionTypeEEnum, EventConditionType.POST);
		addEEnumLiteral(eventConditionTypeEEnum, EventConditionType.SYNCHRO);
		addEEnumLiteral(eventConditionTypeEEnum, EventConditionType.EXTERN);

		initEEnum(portParamLevelTypeEEnum, PortParamLevelType.class, "PortParamLevelType");
		addEEnumLiteral(portParamLevelTypeEEnum, PortParamLevelType.MODULE);
		addEEnumLiteral(portParamLevelTypeEEnum, PortParamLevelType.ROBOT_TASK);

		initEEnum(portIOTypeEEnum, PortIOType.class, "PortIOType");
		addEEnumLiteral(portIOTypeEEnum, PortIOType.INPUT);
		addEEnumLiteral(portIOTypeEEnum, PortIOType.OUTPUT);

		initEEnum(portParamIOTypeEEnum, PortParamIOType.class, "PortParamIOType");
		addEEnumLiteral(portParamIOTypeEEnum, PortParamIOType.INPUT);
		addEEnumLiteral(portParamIOTypeEEnum, PortParamIOType.OUTPUT);
		addEEnumLiteral(portParamIOTypeEEnum, PortParamIOType.INPUT_OUTPUT);

		initEEnum(portVarTypeEEnum, PortVarType.class, "PortVarType");
		addEEnumLiteral(portVarTypeEEnum, PortVarType.BOOLEAN);
		addEEnumLiteral(portVarTypeEEnum, PortVarType.INTEGER);
		addEEnumLiteral(portVarTypeEEnum, PortVarType.FLOAT);
		addEEnumLiteral(portVarTypeEEnum, PortVarType.DOUBLE);
		addEEnumLiteral(portVarTypeEEnum, PortVarType.USER_DEFINED);

		// Create resource
		createResource(eNS_URI);
	}

} //PortPackageImpl
