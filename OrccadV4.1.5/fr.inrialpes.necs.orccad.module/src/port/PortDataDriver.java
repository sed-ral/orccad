/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Driver</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link port.PortDataDriver#getRow <em>Row</em>}</li>
 *   <li>{@link port.PortDataDriver#getCol <em>Col</em>}</li>
 *   <li>{@link port.PortDataDriver#getVarName <em>Var Name</em>}</li>
 *   <li>{@link port.PortDataDriver#getVarType <em>Var Type</em>}</li>
 *   <li>{@link port.PortDataDriver#getDirection <em>Direction</em>}</li>
 * </ul>
 * </p>
 *
 * @see port.PortPackage#getPortDataDriver()
 * @model
 * @generated
 */
public interface PortDataDriver extends PortGeneric {
	/**
	 * Returns the value of the '<em><b>Row</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Row</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Row</em>' attribute.
	 * @see #setRow(int)
	 * @see port.PortPackage#getPortDataDriver_Row()
	 * @model default="1"
	 * @generated
	 */
	int getRow();

	/**
	 * Sets the value of the '{@link port.PortDataDriver#getRow <em>Row</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Row</em>' attribute.
	 * @see #getRow()
	 * @generated
	 */
	void setRow(int value);

	/**
	 * Returns the value of the '<em><b>Col</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Col</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Col</em>' attribute.
	 * @see #setCol(int)
	 * @see port.PortPackage#getPortDataDriver_Col()
	 * @model default="1"
	 * @generated
	 */
	int getCol();

	/**
	 * Sets the value of the '{@link port.PortDataDriver#getCol <em>Col</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Col</em>' attribute.
	 * @see #getCol()
	 * @generated
	 */
	void setCol(int value);

	/**
	 * Returns the value of the '<em><b>Var Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Name</em>' attribute.
	 * @see #setVarName(String)
	 * @see port.PortPackage#getPortDataDriver_VarName()
	 * @model default=""
	 * @generated
	 */
	String getVarName();

	/**
	 * Sets the value of the '{@link port.PortDataDriver#getVarName <em>Var Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Name</em>' attribute.
	 * @see #getVarName()
	 * @generated
	 */
	void setVarName(String value);

	/**
	 * Returns the value of the '<em><b>Var Type</b></em>' attribute.
	 * The default value is <code>"INTEGER"</code>.
	 * The literals are from the enumeration {@link port.PortVarType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Type</em>' attribute.
	 * @see port.PortVarType
	 * @see #setVarType(PortVarType)
	 * @see port.PortPackage#getPortDataDriver_VarType()
	 * @model default="INTEGER"
	 * @generated
	 */
	PortVarType getVarType();

	/**
	 * Sets the value of the '{@link port.PortDataDriver#getVarType <em>Var Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Type</em>' attribute.
	 * @see port.PortVarType
	 * @see #getVarType()
	 * @generated
	 */
	void setVarType(PortVarType value);

	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The default value is <code>"OUTPUT"</code>.
	 * The literals are from the enumeration {@link port.PortIOType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see port.PortIOType
	 * @see #setDirection(PortIOType)
	 * @see port.PortPackage#getPortDataDriver_Direction()
	 * @model default="OUTPUT"
	 * @generated
	 */
	PortIOType getDirection();

	/**
	 * Sets the value of the '{@link port.PortDataDriver#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see port.PortIOType
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(PortIOType value);

} // PortDataDriver
