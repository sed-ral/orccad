/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see port.PortPackage
 * @generated
 */
public interface PortFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PortFactory eINSTANCE = port.impl.PortFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Generic</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic</em>'.
	 * @generated
	 */
	PortGeneric createPortGeneric();

	/**
	 * Returns a new object of class '<em>Data Driver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Driver</em>'.
	 * @generated
	 */
	PortDataDriver createPortDataDriver();

	/**
	 * Returns a new object of class '<em>Data</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data</em>'.
	 * @generated
	 */
	PortData createPortData();

	/**
	 * Returns a new object of class '<em>Driver</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Driver</em>'.
	 * @generated
	 */
	PortDriver createPortDriver();

	/**
	 * Returns a new object of class '<em>Param</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Param</em>'.
	 * @generated
	 */
	PortParam createPortParam();

	/**
	 * Returns a new object of class '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event</em>'.
	 * @generated
	 */
	PortEvent createPortEvent();

	/**
	 * Returns a new object of class '<em>Event Exception</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Exception</em>'.
	 * @generated
	 */
	EventException createEventException();

	/**
	 * Returns a new object of class '<em>Event Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Condition</em>'.
	 * @generated
	 */
	EventCondition createEventCondition();

	/**
	 * Returns a new object of class '<em>Exception T1</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exception T1</em>'.
	 * @generated
	 */
	ExceptionT1 createExceptionT1();

	/**
	 * Returns a new object of class '<em>Exception T2</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exception T2</em>'.
	 * @generated
	 */
	ExceptionT2 createExceptionT2();

	/**
	 * Returns a new object of class '<em>Exception T3</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Exception T3</em>'.
	 * @generated
	 */
	ExceptionT3 createExceptionT3();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	PortPackage getPortPackage();

} //PortFactory
