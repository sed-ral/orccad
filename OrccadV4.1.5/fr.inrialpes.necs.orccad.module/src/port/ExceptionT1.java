/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;

import module.ModuleGeneric;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exception T1</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link port.ExceptionT1#getRow <em>Row</em>}</li>
 *   <li>{@link port.ExceptionT1#getCol <em>Col</em>}</li>
 *   <li>{@link port.ExceptionT1#getVarName <em>Var Name</em>}</li>
 *   <li>{@link port.ExceptionT1#getVarType <em>Var Type</em>}</li>
 *   <li>{@link port.ExceptionT1#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see port.PortPackage#getExceptionT1()
 * @model
 * @generated
 */
public interface ExceptionT1 extends EventException {
	/**
	 * Returns the value of the '<em><b>Row</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Row</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Row</em>' attribute.
	 * @see #setRow(int)
	 * @see port.PortPackage#getExceptionT1_Row()
	 * @model default="1"
	 * @generated
	 */
	int getRow();

	/**
	 * Sets the value of the '{@link port.ExceptionT1#getRow <em>Row</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Row</em>' attribute.
	 * @see #getRow()
	 * @generated
	 */
	void setRow(int value);

	/**
	 * Returns the value of the '<em><b>Col</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Col</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Col</em>' attribute.
	 * @see #setCol(int)
	 * @see port.PortPackage#getExceptionT1_Col()
	 * @model default="1"
	 * @generated
	 */
	int getCol();

	/**
	 * Sets the value of the '{@link port.ExceptionT1#getCol <em>Col</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Col</em>' attribute.
	 * @see #getCol()
	 * @generated
	 */
	void setCol(int value);

	/**
	 * Returns the value of the '<em><b>Var Name</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Name</em>' attribute.
	 * @see #setVarName(String)
	 * @see port.PortPackage#getExceptionT1_VarName()
	 * @model default=""
	 * @generated
	 */
	String getVarName();

	/**
	 * Sets the value of the '{@link port.ExceptionT1#getVarName <em>Var Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Name</em>' attribute.
	 * @see #getVarName()
	 * @generated
	 */
	void setVarName(String value);

	/**
	 * Returns the value of the '<em><b>Var Type</b></em>' attribute.
	 * The default value is <code>"INTEGER"</code>.
	 * The literals are from the enumeration {@link port.PortVarType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Type</em>' attribute.
	 * @see port.PortVarType
	 * @see #setVarType(PortVarType)
	 * @see port.PortPackage#getExceptionT1_VarType()
	 * @model default="INTEGER"
	 * @generated
	 */
	PortVarType getVarType();

	/**
	 * Sets the value of the '{@link port.ExceptionT1#getVarType <em>Var Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Type</em>' attribute.
	 * @see port.PortVarType
	 * @see #getVarType()
	 * @generated
	 */
	void setVarType(PortVarType value);

	/**
	 * Returns the value of the '<em><b>Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link module.ModuleGeneric#getEventT1s <em>Event T1s</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' container reference.
	 * @see #setContainer(ModuleGeneric)
	 * @see port.PortPackage#getExceptionT1_Container()
	 * @see module.ModuleGeneric#getEventT1s
	 * @model opposite="eventT1s" transient="false"
	 * @generated
	 */
	ModuleGeneric getContainer();

	/**
	 * Sets the value of the '{@link port.ExceptionT1#getContainer <em>Container</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' container reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(ModuleGeneric value);

} // ExceptionT1
