/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;

import module.ModuleGeneric;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Exception T3</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link port.ExceptionT3#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see port.PortPackage#getExceptionT3()
 * @model
 * @generated
 */
public interface ExceptionT3 extends EventException {
	/**
	 * Returns the value of the '<em><b>Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link module.ModuleGeneric#getEventT3s <em>Event T3s</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' container reference.
	 * @see #setContainer(ModuleGeneric)
	 * @see port.PortPackage#getExceptionT3_Container()
	 * @see module.ModuleGeneric#getEventT3s
	 * @model opposite="eventT3s" transient="false"
	 * @generated
	 */
	ModuleGeneric getContainer();

	/**
	 * Sets the value of the '{@link port.ExceptionT3#getContainer <em>Container</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' container reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(ModuleGeneric value);

} // ExceptionT3
