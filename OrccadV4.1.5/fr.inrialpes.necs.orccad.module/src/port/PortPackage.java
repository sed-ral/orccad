/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see port.PortFactory
 * @model kind="package"
 * @generated
 */
public interface PortPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "port";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://port";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "port";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PortPackage eINSTANCE = port.impl.PortPackageImpl.init();

	/**
	 * The meta object id for the '{@link port.impl.PortGenericImpl <em>Generic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.impl.PortGenericImpl
	 * @see port.impl.PortPackageImpl#getPortGeneric()
	 * @generated
	 */
	int PORT_GENERIC = 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_GENERIC__COMMENT = 0;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_GENERIC__PORT_NAME = 1;

	/**
	 * The number of structural features of the '<em>Generic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_GENERIC_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link port.impl.PortDataDriverImpl <em>Data Driver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.impl.PortDataDriverImpl
	 * @see port.impl.PortPackageImpl#getPortDataDriver()
	 * @generated
	 */
	int PORT_DATA_DRIVER = 1;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA_DRIVER__COMMENT = PORT_GENERIC__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA_DRIVER__PORT_NAME = PORT_GENERIC__PORT_NAME;

	/**
	 * The feature id for the '<em><b>Row</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA_DRIVER__ROW = PORT_GENERIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Col</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA_DRIVER__COL = PORT_GENERIC_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Var Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA_DRIVER__VAR_NAME = PORT_GENERIC_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Var Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA_DRIVER__VAR_TYPE = PORT_GENERIC_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA_DRIVER__DIRECTION = PORT_GENERIC_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Data Driver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA_DRIVER_FEATURE_COUNT = PORT_GENERIC_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link port.impl.PortDataImpl <em>Data</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.impl.PortDataImpl
	 * @see port.impl.PortPackageImpl#getPortData()
	 * @generated
	 */
	int PORT_DATA = 2;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA__COMMENT = PORT_DATA_DRIVER__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA__PORT_NAME = PORT_DATA_DRIVER__PORT_NAME;

	/**
	 * The feature id for the '<em><b>Row</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA__ROW = PORT_DATA_DRIVER__ROW;

	/**
	 * The feature id for the '<em><b>Col</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA__COL = PORT_DATA_DRIVER__COL;

	/**
	 * The feature id for the '<em><b>Var Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA__VAR_NAME = PORT_DATA_DRIVER__VAR_NAME;

	/**
	 * The feature id for the '<em><b>Var Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA__VAR_TYPE = PORT_DATA_DRIVER__VAR_TYPE;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA__DIRECTION = PORT_DATA_DRIVER__DIRECTION;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA__CONTAINER = PORT_DATA_DRIVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Data</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DATA_FEATURE_COUNT = PORT_DATA_DRIVER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link port.impl.PortDriverImpl <em>Driver</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.impl.PortDriverImpl
	 * @see port.impl.PortPackageImpl#getPortDriver()
	 * @generated
	 */
	int PORT_DRIVER = 3;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DRIVER__COMMENT = PORT_DATA_DRIVER__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DRIVER__PORT_NAME = PORT_DATA_DRIVER__PORT_NAME;

	/**
	 * The feature id for the '<em><b>Row</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DRIVER__ROW = PORT_DATA_DRIVER__ROW;

	/**
	 * The feature id for the '<em><b>Col</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DRIVER__COL = PORT_DATA_DRIVER__COL;

	/**
	 * The feature id for the '<em><b>Var Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DRIVER__VAR_NAME = PORT_DATA_DRIVER__VAR_NAME;

	/**
	 * The feature id for the '<em><b>Var Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DRIVER__VAR_TYPE = PORT_DATA_DRIVER__VAR_TYPE;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DRIVER__DIRECTION = PORT_DATA_DRIVER__DIRECTION;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DRIVER__CONTAINER = PORT_DATA_DRIVER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Driver</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_DRIVER_FEATURE_COUNT = PORT_DATA_DRIVER_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link port.impl.PortParamImpl <em>Param</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.impl.PortParamImpl
	 * @see port.impl.PortPackageImpl#getPortParam()
	 * @generated
	 */
	int PORT_PARAM = 4;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PARAM__COMMENT = PORT_GENERIC__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PARAM__PORT_NAME = PORT_GENERIC__PORT_NAME;

	/**
	 * The feature id for the '<em><b>Row</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PARAM__ROW = PORT_GENERIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Col</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PARAM__COL = PORT_GENERIC_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Var Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PARAM__VAR_NAME = PORT_GENERIC_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Var Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PARAM__VAR_TYPE = PORT_GENERIC_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Var Default Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PARAM__VAR_DEFAULT_VALUE = PORT_GENERIC_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Level Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PARAM__LEVEL_TYPE = PORT_GENERIC_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PARAM__DIRECTION = PORT_GENERIC_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PARAM__CONTAINER = PORT_GENERIC_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Param</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_PARAM_FEATURE_COUNT = PORT_GENERIC_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link port.impl.PortEventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.impl.PortEventImpl
	 * @see port.impl.PortPackageImpl#getPortEvent()
	 * @generated
	 */
	int PORT_EVENT = 5;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_EVENT__COMMENT = PORT_GENERIC__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_EVENT__PORT_NAME = PORT_GENERIC__PORT_NAME;

	/**
	 * The feature id for the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_EVENT__EVENT_NAME = PORT_GENERIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_EVENT__DIRECTION = PORT_GENERIC_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PORT_EVENT_FEATURE_COUNT = PORT_GENERIC_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link port.impl.EventExceptionImpl <em>Event Exception</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.impl.EventExceptionImpl
	 * @see port.impl.PortPackageImpl#getEventException()
	 * @generated
	 */
	int EVENT_EXCEPTION = 6;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_EXCEPTION__COMMENT = PORT_EVENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_EXCEPTION__PORT_NAME = PORT_EVENT__PORT_NAME;

	/**
	 * The feature id for the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_EXCEPTION__EVENT_NAME = PORT_EVENT__EVENT_NAME;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_EXCEPTION__DIRECTION = PORT_EVENT__DIRECTION;

	/**
	 * The number of structural features of the '<em>Event Exception</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_EXCEPTION_FEATURE_COUNT = PORT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link port.impl.EventConditionImpl <em>Event Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.impl.EventConditionImpl
	 * @see port.impl.PortPackageImpl#getEventCondition()
	 * @generated
	 */
	int EVENT_CONDITION = 7;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONDITION__COMMENT = PORT_EVENT__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONDITION__PORT_NAME = PORT_EVENT__PORT_NAME;

	/**
	 * The feature id for the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONDITION__EVENT_NAME = PORT_EVENT__EVENT_NAME;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONDITION__DIRECTION = PORT_EVENT__DIRECTION;

	/**
	 * The feature id for the '<em><b>Event Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONDITION__EVENT_TYPE = PORT_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONDITION__CONTAINER = PORT_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Event Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONDITION_FEATURE_COUNT = PORT_EVENT_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link port.impl.ExceptionT1Impl <em>Exception T1</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.impl.ExceptionT1Impl
	 * @see port.impl.PortPackageImpl#getExceptionT1()
	 * @generated
	 */
	int EXCEPTION_T1 = 8;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T1__COMMENT = EVENT_EXCEPTION__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T1__PORT_NAME = EVENT_EXCEPTION__PORT_NAME;

	/**
	 * The feature id for the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T1__EVENT_NAME = EVENT_EXCEPTION__EVENT_NAME;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T1__DIRECTION = EVENT_EXCEPTION__DIRECTION;

	/**
	 * The feature id for the '<em><b>Row</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T1__ROW = EVENT_EXCEPTION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Col</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T1__COL = EVENT_EXCEPTION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Var Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T1__VAR_NAME = EVENT_EXCEPTION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Var Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T1__VAR_TYPE = EVENT_EXCEPTION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T1__CONTAINER = EVENT_EXCEPTION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Exception T1</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T1_FEATURE_COUNT = EVENT_EXCEPTION_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link port.impl.ExceptionT2Impl <em>Exception T2</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.impl.ExceptionT2Impl
	 * @see port.impl.PortPackageImpl#getExceptionT2()
	 * @generated
	 */
	int EXCEPTION_T2 = 9;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T2__COMMENT = EVENT_EXCEPTION__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T2__PORT_NAME = EVENT_EXCEPTION__PORT_NAME;

	/**
	 * The feature id for the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T2__EVENT_NAME = EVENT_EXCEPTION__EVENT_NAME;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T2__DIRECTION = EVENT_EXCEPTION__DIRECTION;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T2__CONTAINER = EVENT_EXCEPTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Exception T2</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T2_FEATURE_COUNT = EVENT_EXCEPTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link port.impl.ExceptionT3Impl <em>Exception T3</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.impl.ExceptionT3Impl
	 * @see port.impl.PortPackageImpl#getExceptionT3()
	 * @generated
	 */
	int EXCEPTION_T3 = 10;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T3__COMMENT = EVENT_EXCEPTION__COMMENT;

	/**
	 * The feature id for the '<em><b>Port Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T3__PORT_NAME = EVENT_EXCEPTION__PORT_NAME;

	/**
	 * The feature id for the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T3__EVENT_NAME = EVENT_EXCEPTION__EVENT_NAME;

	/**
	 * The feature id for the '<em><b>Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T3__DIRECTION = EVENT_EXCEPTION__DIRECTION;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T3__CONTAINER = EVENT_EXCEPTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Exception T3</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXCEPTION_T3_FEATURE_COUNT = EVENT_EXCEPTION_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link port.EventExceptionType <em>Event Exception Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.EventExceptionType
	 * @see port.impl.PortPackageImpl#getEventExceptionType()
	 * @generated
	 */
	int EVENT_EXCEPTION_TYPE = 11;

	/**
	 * The meta object id for the '{@link port.EventConditionType <em>Event Condition Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.EventConditionType
	 * @see port.impl.PortPackageImpl#getEventConditionType()
	 * @generated
	 */
	int EVENT_CONDITION_TYPE = 12;

	/**
	 * The meta object id for the '{@link port.PortParamLevelType <em>Param Level Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.PortParamLevelType
	 * @see port.impl.PortPackageImpl#getPortParamLevelType()
	 * @generated
	 */
	int PORT_PARAM_LEVEL_TYPE = 13;

	/**
	 * The meta object id for the '{@link port.PortIOType <em>IO Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.PortIOType
	 * @see port.impl.PortPackageImpl#getPortIOType()
	 * @generated
	 */
	int PORT_IO_TYPE = 14;

	/**
	 * The meta object id for the '{@link port.PortParamIOType <em>Param IO Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.PortParamIOType
	 * @see port.impl.PortPackageImpl#getPortParamIOType()
	 * @generated
	 */
	int PORT_PARAM_IO_TYPE = 15;

	/**
	 * The meta object id for the '{@link port.PortVarType <em>Var Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see port.PortVarType
	 * @see port.impl.PortPackageImpl#getPortVarType()
	 * @generated
	 */
	int PORT_VAR_TYPE = 16;


	/**
	 * Returns the meta object for class '{@link port.PortGeneric <em>Generic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generic</em>'.
	 * @see port.PortGeneric
	 * @generated
	 */
	EClass getPortGeneric();

	/**
	 * Returns the meta object for the attribute '{@link port.PortGeneric#getComment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comment</em>'.
	 * @see port.PortGeneric#getComment()
	 * @see #getPortGeneric()
	 * @generated
	 */
	EAttribute getPortGeneric_Comment();

	/**
	 * Returns the meta object for the attribute '{@link port.PortGeneric#getPortName <em>Port Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port Name</em>'.
	 * @see port.PortGeneric#getPortName()
	 * @see #getPortGeneric()
	 * @generated
	 */
	EAttribute getPortGeneric_PortName();

	/**
	 * Returns the meta object for class '{@link port.PortDataDriver <em>Data Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Driver</em>'.
	 * @see port.PortDataDriver
	 * @generated
	 */
	EClass getPortDataDriver();

	/**
	 * Returns the meta object for the attribute '{@link port.PortDataDriver#getRow <em>Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Row</em>'.
	 * @see port.PortDataDriver#getRow()
	 * @see #getPortDataDriver()
	 * @generated
	 */
	EAttribute getPortDataDriver_Row();

	/**
	 * Returns the meta object for the attribute '{@link port.PortDataDriver#getCol <em>Col</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Col</em>'.
	 * @see port.PortDataDriver#getCol()
	 * @see #getPortDataDriver()
	 * @generated
	 */
	EAttribute getPortDataDriver_Col();

	/**
	 * Returns the meta object for the attribute '{@link port.PortDataDriver#getVarName <em>Var Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Name</em>'.
	 * @see port.PortDataDriver#getVarName()
	 * @see #getPortDataDriver()
	 * @generated
	 */
	EAttribute getPortDataDriver_VarName();

	/**
	 * Returns the meta object for the attribute '{@link port.PortDataDriver#getVarType <em>Var Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Type</em>'.
	 * @see port.PortDataDriver#getVarType()
	 * @see #getPortDataDriver()
	 * @generated
	 */
	EAttribute getPortDataDriver_VarType();

	/**
	 * Returns the meta object for the attribute '{@link port.PortDataDriver#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see port.PortDataDriver#getDirection()
	 * @see #getPortDataDriver()
	 * @generated
	 */
	EAttribute getPortDataDriver_Direction();

	/**
	 * Returns the meta object for class '{@link port.PortData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data</em>'.
	 * @see port.PortData
	 * @generated
	 */
	EClass getPortData();

	/**
	 * Returns the meta object for the container reference '{@link port.PortData#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see port.PortData#getContainer()
	 * @see #getPortData()
	 * @generated
	 */
	EReference getPortData_Container();

	/**
	 * Returns the meta object for class '{@link port.PortDriver <em>Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Driver</em>'.
	 * @see port.PortDriver
	 * @generated
	 */
	EClass getPortDriver();

	/**
	 * Returns the meta object for the container reference '{@link port.PortDriver#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see port.PortDriver#getContainer()
	 * @see #getPortDriver()
	 * @generated
	 */
	EReference getPortDriver_Container();

	/**
	 * Returns the meta object for class '{@link port.PortParam <em>Param</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Param</em>'.
	 * @see port.PortParam
	 * @generated
	 */
	EClass getPortParam();

	/**
	 * Returns the meta object for the attribute '{@link port.PortParam#getRow <em>Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Row</em>'.
	 * @see port.PortParam#getRow()
	 * @see #getPortParam()
	 * @generated
	 */
	EAttribute getPortParam_Row();

	/**
	 * Returns the meta object for the attribute '{@link port.PortParam#getCol <em>Col</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Col</em>'.
	 * @see port.PortParam#getCol()
	 * @see #getPortParam()
	 * @generated
	 */
	EAttribute getPortParam_Col();

	/**
	 * Returns the meta object for the attribute '{@link port.PortParam#getVarName <em>Var Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Name</em>'.
	 * @see port.PortParam#getVarName()
	 * @see #getPortParam()
	 * @generated
	 */
	EAttribute getPortParam_VarName();

	/**
	 * Returns the meta object for the attribute '{@link port.PortParam#getVarType <em>Var Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Type</em>'.
	 * @see port.PortParam#getVarType()
	 * @see #getPortParam()
	 * @generated
	 */
	EAttribute getPortParam_VarType();

	/**
	 * Returns the meta object for the attribute '{@link port.PortParam#getVarDefaultValue <em>Var Default Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Default Value</em>'.
	 * @see port.PortParam#getVarDefaultValue()
	 * @see #getPortParam()
	 * @generated
	 */
	EAttribute getPortParam_VarDefaultValue();

	/**
	 * Returns the meta object for the attribute '{@link port.PortParam#getLevelType <em>Level Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level Type</em>'.
	 * @see port.PortParam#getLevelType()
	 * @see #getPortParam()
	 * @generated
	 */
	EAttribute getPortParam_LevelType();

	/**
	 * Returns the meta object for the attribute '{@link port.PortParam#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see port.PortParam#getDirection()
	 * @see #getPortParam()
	 * @generated
	 */
	EAttribute getPortParam_Direction();

	/**
	 * Returns the meta object for the container reference '{@link port.PortParam#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see port.PortParam#getContainer()
	 * @see #getPortParam()
	 * @generated
	 */
	EReference getPortParam_Container();

	/**
	 * Returns the meta object for class '{@link port.PortEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see port.PortEvent
	 * @generated
	 */
	EClass getPortEvent();

	/**
	 * Returns the meta object for the attribute '{@link port.PortEvent#getEventName <em>Event Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Name</em>'.
	 * @see port.PortEvent#getEventName()
	 * @see #getPortEvent()
	 * @generated
	 */
	EAttribute getPortEvent_EventName();

	/**
	 * Returns the meta object for the attribute '{@link port.PortEvent#getDirection <em>Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Direction</em>'.
	 * @see port.PortEvent#getDirection()
	 * @see #getPortEvent()
	 * @generated
	 */
	EAttribute getPortEvent_Direction();

	/**
	 * Returns the meta object for class '{@link port.EventException <em>Event Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Exception</em>'.
	 * @see port.EventException
	 * @generated
	 */
	EClass getEventException();

	/**
	 * Returns the meta object for class '{@link port.EventCondition <em>Event Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Condition</em>'.
	 * @see port.EventCondition
	 * @generated
	 */
	EClass getEventCondition();

	/**
	 * Returns the meta object for the attribute '{@link port.EventCondition#getEventType <em>Event Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Event Type</em>'.
	 * @see port.EventCondition#getEventType()
	 * @see #getEventCondition()
	 * @generated
	 */
	EAttribute getEventCondition_EventType();

	/**
	 * Returns the meta object for the container reference '{@link port.EventCondition#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see port.EventCondition#getContainer()
	 * @see #getEventCondition()
	 * @generated
	 */
	EReference getEventCondition_Container();

	/**
	 * Returns the meta object for class '{@link port.ExceptionT1 <em>Exception T1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exception T1</em>'.
	 * @see port.ExceptionT1
	 * @generated
	 */
	EClass getExceptionT1();

	/**
	 * Returns the meta object for the attribute '{@link port.ExceptionT1#getRow <em>Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Row</em>'.
	 * @see port.ExceptionT1#getRow()
	 * @see #getExceptionT1()
	 * @generated
	 */
	EAttribute getExceptionT1_Row();

	/**
	 * Returns the meta object for the attribute '{@link port.ExceptionT1#getCol <em>Col</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Col</em>'.
	 * @see port.ExceptionT1#getCol()
	 * @see #getExceptionT1()
	 * @generated
	 */
	EAttribute getExceptionT1_Col();

	/**
	 * Returns the meta object for the attribute '{@link port.ExceptionT1#getVarName <em>Var Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Name</em>'.
	 * @see port.ExceptionT1#getVarName()
	 * @see #getExceptionT1()
	 * @generated
	 */
	EAttribute getExceptionT1_VarName();

	/**
	 * Returns the meta object for the attribute '{@link port.ExceptionT1#getVarType <em>Var Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Var Type</em>'.
	 * @see port.ExceptionT1#getVarType()
	 * @see #getExceptionT1()
	 * @generated
	 */
	EAttribute getExceptionT1_VarType();

	/**
	 * Returns the meta object for the container reference '{@link port.ExceptionT1#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see port.ExceptionT1#getContainer()
	 * @see #getExceptionT1()
	 * @generated
	 */
	EReference getExceptionT1_Container();

	/**
	 * Returns the meta object for class '{@link port.ExceptionT2 <em>Exception T2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exception T2</em>'.
	 * @see port.ExceptionT2
	 * @generated
	 */
	EClass getExceptionT2();

	/**
	 * Returns the meta object for the container reference '{@link port.ExceptionT2#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see port.ExceptionT2#getContainer()
	 * @see #getExceptionT2()
	 * @generated
	 */
	EReference getExceptionT2_Container();

	/**
	 * Returns the meta object for class '{@link port.ExceptionT3 <em>Exception T3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Exception T3</em>'.
	 * @see port.ExceptionT3
	 * @generated
	 */
	EClass getExceptionT3();

	/**
	 * Returns the meta object for the container reference '{@link port.ExceptionT3#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see port.ExceptionT3#getContainer()
	 * @see #getExceptionT3()
	 * @generated
	 */
	EReference getExceptionT3_Container();

	/**
	 * Returns the meta object for enum '{@link port.EventExceptionType <em>Event Exception Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Event Exception Type</em>'.
	 * @see port.EventExceptionType
	 * @generated
	 */
	EEnum getEventExceptionType();

	/**
	 * Returns the meta object for enum '{@link port.EventConditionType <em>Event Condition Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Event Condition Type</em>'.
	 * @see port.EventConditionType
	 * @generated
	 */
	EEnum getEventConditionType();

	/**
	 * Returns the meta object for enum '{@link port.PortParamLevelType <em>Param Level Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Param Level Type</em>'.
	 * @see port.PortParamLevelType
	 * @generated
	 */
	EEnum getPortParamLevelType();

	/**
	 * Returns the meta object for enum '{@link port.PortIOType <em>IO Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>IO Type</em>'.
	 * @see port.PortIOType
	 * @generated
	 */
	EEnum getPortIOType();

	/**
	 * Returns the meta object for enum '{@link port.PortParamIOType <em>Param IO Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Param IO Type</em>'.
	 * @see port.PortParamIOType
	 * @generated
	 */
	EEnum getPortParamIOType();

	/**
	 * Returns the meta object for enum '{@link port.PortVarType <em>Var Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Var Type</em>'.
	 * @see port.PortVarType
	 * @generated
	 */
	EEnum getPortVarType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PortFactory getPortFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link port.impl.PortGenericImpl <em>Generic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.impl.PortGenericImpl
		 * @see port.impl.PortPackageImpl#getPortGeneric()
		 * @generated
		 */
		EClass PORT_GENERIC = eINSTANCE.getPortGeneric();

		/**
		 * The meta object literal for the '<em><b>Comment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_GENERIC__COMMENT = eINSTANCE.getPortGeneric_Comment();

		/**
		 * The meta object literal for the '<em><b>Port Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_GENERIC__PORT_NAME = eINSTANCE.getPortGeneric_PortName();

		/**
		 * The meta object literal for the '{@link port.impl.PortDataDriverImpl <em>Data Driver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.impl.PortDataDriverImpl
		 * @see port.impl.PortPackageImpl#getPortDataDriver()
		 * @generated
		 */
		EClass PORT_DATA_DRIVER = eINSTANCE.getPortDataDriver();

		/**
		 * The meta object literal for the '<em><b>Row</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_DATA_DRIVER__ROW = eINSTANCE.getPortDataDriver_Row();

		/**
		 * The meta object literal for the '<em><b>Col</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_DATA_DRIVER__COL = eINSTANCE.getPortDataDriver_Col();

		/**
		 * The meta object literal for the '<em><b>Var Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_DATA_DRIVER__VAR_NAME = eINSTANCE.getPortDataDriver_VarName();

		/**
		 * The meta object literal for the '<em><b>Var Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_DATA_DRIVER__VAR_TYPE = eINSTANCE.getPortDataDriver_VarType();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_DATA_DRIVER__DIRECTION = eINSTANCE.getPortDataDriver_Direction();

		/**
		 * The meta object literal for the '{@link port.impl.PortDataImpl <em>Data</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.impl.PortDataImpl
		 * @see port.impl.PortPackageImpl#getPortData()
		 * @generated
		 */
		EClass PORT_DATA = eINSTANCE.getPortData();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_DATA__CONTAINER = eINSTANCE.getPortData_Container();

		/**
		 * The meta object literal for the '{@link port.impl.PortDriverImpl <em>Driver</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.impl.PortDriverImpl
		 * @see port.impl.PortPackageImpl#getPortDriver()
		 * @generated
		 */
		EClass PORT_DRIVER = eINSTANCE.getPortDriver();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_DRIVER__CONTAINER = eINSTANCE.getPortDriver_Container();

		/**
		 * The meta object literal for the '{@link port.impl.PortParamImpl <em>Param</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.impl.PortParamImpl
		 * @see port.impl.PortPackageImpl#getPortParam()
		 * @generated
		 */
		EClass PORT_PARAM = eINSTANCE.getPortParam();

		/**
		 * The meta object literal for the '<em><b>Row</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_PARAM__ROW = eINSTANCE.getPortParam_Row();

		/**
		 * The meta object literal for the '<em><b>Col</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_PARAM__COL = eINSTANCE.getPortParam_Col();

		/**
		 * The meta object literal for the '<em><b>Var Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_PARAM__VAR_NAME = eINSTANCE.getPortParam_VarName();

		/**
		 * The meta object literal for the '<em><b>Var Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_PARAM__VAR_TYPE = eINSTANCE.getPortParam_VarType();

		/**
		 * The meta object literal for the '<em><b>Var Default Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_PARAM__VAR_DEFAULT_VALUE = eINSTANCE.getPortParam_VarDefaultValue();

		/**
		 * The meta object literal for the '<em><b>Level Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_PARAM__LEVEL_TYPE = eINSTANCE.getPortParam_LevelType();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_PARAM__DIRECTION = eINSTANCE.getPortParam_Direction();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PORT_PARAM__CONTAINER = eINSTANCE.getPortParam_Container();

		/**
		 * The meta object literal for the '{@link port.impl.PortEventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.impl.PortEventImpl
		 * @see port.impl.PortPackageImpl#getPortEvent()
		 * @generated
		 */
		EClass PORT_EVENT = eINSTANCE.getPortEvent();

		/**
		 * The meta object literal for the '<em><b>Event Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_EVENT__EVENT_NAME = eINSTANCE.getPortEvent_EventName();

		/**
		 * The meta object literal for the '<em><b>Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PORT_EVENT__DIRECTION = eINSTANCE.getPortEvent_Direction();

		/**
		 * The meta object literal for the '{@link port.impl.EventExceptionImpl <em>Event Exception</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.impl.EventExceptionImpl
		 * @see port.impl.PortPackageImpl#getEventException()
		 * @generated
		 */
		EClass EVENT_EXCEPTION = eINSTANCE.getEventException();

		/**
		 * The meta object literal for the '{@link port.impl.EventConditionImpl <em>Event Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.impl.EventConditionImpl
		 * @see port.impl.PortPackageImpl#getEventCondition()
		 * @generated
		 */
		EClass EVENT_CONDITION = eINSTANCE.getEventCondition();

		/**
		 * The meta object literal for the '<em><b>Event Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_CONDITION__EVENT_TYPE = eINSTANCE.getEventCondition_EventType();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_CONDITION__CONTAINER = eINSTANCE.getEventCondition_Container();

		/**
		 * The meta object literal for the '{@link port.impl.ExceptionT1Impl <em>Exception T1</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.impl.ExceptionT1Impl
		 * @see port.impl.PortPackageImpl#getExceptionT1()
		 * @generated
		 */
		EClass EXCEPTION_T1 = eINSTANCE.getExceptionT1();

		/**
		 * The meta object literal for the '<em><b>Row</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXCEPTION_T1__ROW = eINSTANCE.getExceptionT1_Row();

		/**
		 * The meta object literal for the '<em><b>Col</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXCEPTION_T1__COL = eINSTANCE.getExceptionT1_Col();

		/**
		 * The meta object literal for the '<em><b>Var Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXCEPTION_T1__VAR_NAME = eINSTANCE.getExceptionT1_VarName();

		/**
		 * The meta object literal for the '<em><b>Var Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXCEPTION_T1__VAR_TYPE = eINSTANCE.getExceptionT1_VarType();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXCEPTION_T1__CONTAINER = eINSTANCE.getExceptionT1_Container();

		/**
		 * The meta object literal for the '{@link port.impl.ExceptionT2Impl <em>Exception T2</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.impl.ExceptionT2Impl
		 * @see port.impl.PortPackageImpl#getExceptionT2()
		 * @generated
		 */
		EClass EXCEPTION_T2 = eINSTANCE.getExceptionT2();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXCEPTION_T2__CONTAINER = eINSTANCE.getExceptionT2_Container();

		/**
		 * The meta object literal for the '{@link port.impl.ExceptionT3Impl <em>Exception T3</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.impl.ExceptionT3Impl
		 * @see port.impl.PortPackageImpl#getExceptionT3()
		 * @generated
		 */
		EClass EXCEPTION_T3 = eINSTANCE.getExceptionT3();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXCEPTION_T3__CONTAINER = eINSTANCE.getExceptionT3_Container();

		/**
		 * The meta object literal for the '{@link port.EventExceptionType <em>Event Exception Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.EventExceptionType
		 * @see port.impl.PortPackageImpl#getEventExceptionType()
		 * @generated
		 */
		EEnum EVENT_EXCEPTION_TYPE = eINSTANCE.getEventExceptionType();

		/**
		 * The meta object literal for the '{@link port.EventConditionType <em>Event Condition Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.EventConditionType
		 * @see port.impl.PortPackageImpl#getEventConditionType()
		 * @generated
		 */
		EEnum EVENT_CONDITION_TYPE = eINSTANCE.getEventConditionType();

		/**
		 * The meta object literal for the '{@link port.PortParamLevelType <em>Param Level Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.PortParamLevelType
		 * @see port.impl.PortPackageImpl#getPortParamLevelType()
		 * @generated
		 */
		EEnum PORT_PARAM_LEVEL_TYPE = eINSTANCE.getPortParamLevelType();

		/**
		 * The meta object literal for the '{@link port.PortIOType <em>IO Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.PortIOType
		 * @see port.impl.PortPackageImpl#getPortIOType()
		 * @generated
		 */
		EEnum PORT_IO_TYPE = eINSTANCE.getPortIOType();

		/**
		 * The meta object literal for the '{@link port.PortParamIOType <em>Param IO Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.PortParamIOType
		 * @see port.impl.PortPackageImpl#getPortParamIOType()
		 * @generated
		 */
		EEnum PORT_PARAM_IO_TYPE = eINSTANCE.getPortParamIOType();

		/**
		 * The meta object literal for the '{@link port.PortVarType <em>Var Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see port.PortVarType
		 * @see port.impl.PortPackageImpl#getPortVarType()
		 * @generated
		 */
		EEnum PORT_VAR_TYPE = eINSTANCE.getPortVarType();

	}

} //PortPackage
