/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import port.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see port.PortPackage
 * @generated
 */
public class PortAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static PortPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = PortPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PortSwitch<Adapter> modelSwitch =
		new PortSwitch<Adapter>() {
			@Override
			public Adapter casePortGeneric(PortGeneric object) {
				return createPortGenericAdapter();
			}
			@Override
			public Adapter casePortDataDriver(PortDataDriver object) {
				return createPortDataDriverAdapter();
			}
			@Override
			public Adapter casePortData(PortData object) {
				return createPortDataAdapter();
			}
			@Override
			public Adapter casePortDriver(PortDriver object) {
				return createPortDriverAdapter();
			}
			@Override
			public Adapter casePortParam(PortParam object) {
				return createPortParamAdapter();
			}
			@Override
			public Adapter casePortEvent(PortEvent object) {
				return createPortEventAdapter();
			}
			@Override
			public Adapter caseEventException(EventException object) {
				return createEventExceptionAdapter();
			}
			@Override
			public Adapter caseEventCondition(EventCondition object) {
				return createEventConditionAdapter();
			}
			@Override
			public Adapter caseExceptionT1(ExceptionT1 object) {
				return createExceptionT1Adapter();
			}
			@Override
			public Adapter caseExceptionT2(ExceptionT2 object) {
				return createExceptionT2Adapter();
			}
			@Override
			public Adapter caseExceptionT3(ExceptionT3 object) {
				return createExceptionT3Adapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link port.PortGeneric <em>Generic</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see port.PortGeneric
	 * @generated
	 */
	public Adapter createPortGenericAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link port.PortDataDriver <em>Data Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see port.PortDataDriver
	 * @generated
	 */
	public Adapter createPortDataDriverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link port.PortData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see port.PortData
	 * @generated
	 */
	public Adapter createPortDataAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link port.PortDriver <em>Driver</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see port.PortDriver
	 * @generated
	 */
	public Adapter createPortDriverAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link port.PortParam <em>Param</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see port.PortParam
	 * @generated
	 */
	public Adapter createPortParamAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link port.PortEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see port.PortEvent
	 * @generated
	 */
	public Adapter createPortEventAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link port.EventException <em>Event Exception</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see port.EventException
	 * @generated
	 */
	public Adapter createEventExceptionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link port.EventCondition <em>Event Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see port.EventCondition
	 * @generated
	 */
	public Adapter createEventConditionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link port.ExceptionT1 <em>Exception T1</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see port.ExceptionT1
	 * @generated
	 */
	public Adapter createExceptionT1Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link port.ExceptionT2 <em>Exception T2</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see port.ExceptionT2
	 * @generated
	 */
	public Adapter createExceptionT2Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link port.ExceptionT3 <em>Exception T3</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see port.ExceptionT3
	 * @generated
	 */
	public Adapter createExceptionT3Adapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //PortAdapterFactory
