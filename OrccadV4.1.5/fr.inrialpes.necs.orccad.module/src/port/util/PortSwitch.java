/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port.util;

import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

import port.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see port.PortPackage
 * @generated
 */
public class PortSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static PortPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortSwitch() {
		if (modelPackage == null) {
			modelPackage = PortPackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case PortPackage.PORT_GENERIC: {
				PortGeneric portGeneric = (PortGeneric)theEObject;
				T result = casePortGeneric(portGeneric);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortPackage.PORT_DATA_DRIVER: {
				PortDataDriver portDataDriver = (PortDataDriver)theEObject;
				T result = casePortDataDriver(portDataDriver);
				if (result == null) result = casePortGeneric(portDataDriver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortPackage.PORT_DATA: {
				PortData portData = (PortData)theEObject;
				T result = casePortData(portData);
				if (result == null) result = casePortDataDriver(portData);
				if (result == null) result = casePortGeneric(portData);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortPackage.PORT_DRIVER: {
				PortDriver portDriver = (PortDriver)theEObject;
				T result = casePortDriver(portDriver);
				if (result == null) result = casePortDataDriver(portDriver);
				if (result == null) result = casePortGeneric(portDriver);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortPackage.PORT_PARAM: {
				PortParam portParam = (PortParam)theEObject;
				T result = casePortParam(portParam);
				if (result == null) result = casePortGeneric(portParam);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortPackage.PORT_EVENT: {
				PortEvent portEvent = (PortEvent)theEObject;
				T result = casePortEvent(portEvent);
				if (result == null) result = casePortGeneric(portEvent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortPackage.EVENT_EXCEPTION: {
				EventException eventException = (EventException)theEObject;
				T result = caseEventException(eventException);
				if (result == null) result = casePortEvent(eventException);
				if (result == null) result = casePortGeneric(eventException);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortPackage.EVENT_CONDITION: {
				EventCondition eventCondition = (EventCondition)theEObject;
				T result = caseEventCondition(eventCondition);
				if (result == null) result = casePortEvent(eventCondition);
				if (result == null) result = casePortGeneric(eventCondition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortPackage.EXCEPTION_T1: {
				ExceptionT1 exceptionT1 = (ExceptionT1)theEObject;
				T result = caseExceptionT1(exceptionT1);
				if (result == null) result = caseEventException(exceptionT1);
				if (result == null) result = casePortEvent(exceptionT1);
				if (result == null) result = casePortGeneric(exceptionT1);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortPackage.EXCEPTION_T2: {
				ExceptionT2 exceptionT2 = (ExceptionT2)theEObject;
				T result = caseExceptionT2(exceptionT2);
				if (result == null) result = caseEventException(exceptionT2);
				if (result == null) result = casePortEvent(exceptionT2);
				if (result == null) result = casePortGeneric(exceptionT2);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case PortPackage.EXCEPTION_T3: {
				ExceptionT3 exceptionT3 = (ExceptionT3)theEObject;
				T result = caseExceptionT3(exceptionT3);
				if (result == null) result = caseEventException(exceptionT3);
				if (result == null) result = casePortEvent(exceptionT3);
				if (result == null) result = casePortGeneric(exceptionT3);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortGeneric(PortGeneric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Driver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Driver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortDataDriver(PortDataDriver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortData(PortData object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Driver</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Driver</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortDriver(PortDriver object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Param</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Param</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortParam(PortParam object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePortEvent(PortEvent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Exception</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Exception</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventException(EventException object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Condition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Condition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventCondition(EventCondition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exception T1</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exception T1</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExceptionT1(ExceptionT1 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exception T2</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exception T2</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExceptionT2(ExceptionT2 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Exception T3</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Exception T3</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExceptionT3(ExceptionT3 object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //PortSwitch
