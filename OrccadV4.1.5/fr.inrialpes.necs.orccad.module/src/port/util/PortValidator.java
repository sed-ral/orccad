/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port.util;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import port.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see port.PortPackage
 * @generated
 */
public class PortValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final PortValidator INSTANCE = new PortValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "port";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Port Name' of 'Generic'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PORT_GENERIC__CHECK_PORT_NAME = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Var Default Value' of 'Param'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PORT_PARAM__CHECK_VAR_DEFAULT_VALUE = 2;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 2;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PortValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return PortPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case PortPackage.PORT_GENERIC:
				return validatePortGeneric((PortGeneric)value, diagnostics, context);
			case PortPackage.PORT_DATA_DRIVER:
				return validatePortDataDriver((PortDataDriver)value, diagnostics, context);
			case PortPackage.PORT_DATA:
				return validatePortData((PortData)value, diagnostics, context);
			case PortPackage.PORT_DRIVER:
				return validatePortDriver((PortDriver)value, diagnostics, context);
			case PortPackage.PORT_PARAM:
				return validatePortParam((PortParam)value, diagnostics, context);
			case PortPackage.PORT_EVENT:
				return validatePortEvent((PortEvent)value, diagnostics, context);
			case PortPackage.EVENT_EXCEPTION:
				return validateEventException((EventException)value, diagnostics, context);
			case PortPackage.EVENT_CONDITION:
				return validateEventCondition((EventCondition)value, diagnostics, context);
			case PortPackage.EXCEPTION_T1:
				return validateExceptionT1((ExceptionT1)value, diagnostics, context);
			case PortPackage.EXCEPTION_T2:
				return validateExceptionT2((ExceptionT2)value, diagnostics, context);
			case PortPackage.EXCEPTION_T3:
				return validateExceptionT3((ExceptionT3)value, diagnostics, context);
			case PortPackage.EVENT_EXCEPTION_TYPE:
				return validateEventExceptionType((EventExceptionType)value, diagnostics, context);
			case PortPackage.EVENT_CONDITION_TYPE:
				return validateEventConditionType((EventConditionType)value, diagnostics, context);
			case PortPackage.PORT_PARAM_LEVEL_TYPE:
				return validatePortParamLevelType((PortParamLevelType)value, diagnostics, context);
			case PortPackage.PORT_IO_TYPE:
				return validatePortIOType((PortIOType)value, diagnostics, context);
			case PortPackage.PORT_PARAM_IO_TYPE:
				return validatePortParamIOType((PortParamIOType)value, diagnostics, context);
			case PortPackage.PORT_VAR_TYPE:
				return validatePortVarType((PortVarType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortGeneric(PortGeneric portGeneric, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(portGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(portGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(portGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(portGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(portGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(portGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(portGeneric, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortGeneric_checkPortName(portGeneric, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkPortName constraint of '<em>Generic</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortGeneric_checkPortName(PortGeneric portGeneric, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return portGeneric.checkPortName(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortDataDriver(PortDataDriver portDataDriver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(portDataDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(portDataDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(portDataDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(portDataDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(portDataDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(portDataDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(portDataDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortGeneric_checkPortName(portDataDriver, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortData(PortData portData, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(portData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(portData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(portData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(portData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(portData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(portData, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(portData, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortGeneric_checkPortName(portData, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortDriver(PortDriver portDriver, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(portDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(portDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(portDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(portDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(portDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(portDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(portDriver, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortGeneric_checkPortName(portDriver, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortParam(PortParam portParam, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(portParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(portParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(portParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(portParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(portParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(portParam, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(portParam, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortGeneric_checkPortName(portParam, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortParam_checkVarDefaultValue(portParam, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkVarDefaultValue constraint of '<em>Param</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortParam_checkVarDefaultValue(PortParam portParam, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return portParam.checkVarDefaultValue(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortEvent(PortEvent portEvent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(portEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(portEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(portEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(portEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(portEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(portEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(portEvent, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortGeneric_checkPortName(portEvent, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEventException(EventException eventException, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(eventException, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(eventException, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(eventException, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(eventException, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(eventException, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(eventException, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(eventException, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortGeneric_checkPortName(eventException, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEventCondition(EventCondition eventCondition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(eventCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(eventCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(eventCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(eventCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(eventCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(eventCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(eventCondition, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortGeneric_checkPortName(eventCondition, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExceptionT1(ExceptionT1 exceptionT1, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(exceptionT1, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(exceptionT1, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(exceptionT1, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(exceptionT1, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(exceptionT1, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(exceptionT1, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(exceptionT1, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortGeneric_checkPortName(exceptionT1, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExceptionT2(ExceptionT2 exceptionT2, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(exceptionT2, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(exceptionT2, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(exceptionT2, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(exceptionT2, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(exceptionT2, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(exceptionT2, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(exceptionT2, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortGeneric_checkPortName(exceptionT2, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExceptionT3(ExceptionT3 exceptionT3, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(exceptionT3, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(exceptionT3, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(exceptionT3, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(exceptionT3, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(exceptionT3, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(exceptionT3, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(exceptionT3, diagnostics, context);
		if (result || diagnostics != null) result &= validatePortGeneric_checkPortName(exceptionT3, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEventExceptionType(EventExceptionType eventExceptionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEventConditionType(EventConditionType eventConditionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortParamLevelType(PortParamLevelType portParamLevelType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortIOType(PortIOType portIOType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortParamIOType(PortParamIOType portParamIOType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePortVarType(PortVarType portVarType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //PortValidator
