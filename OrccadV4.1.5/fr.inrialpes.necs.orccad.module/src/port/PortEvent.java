/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link port.PortEvent#getEventName <em>Event Name</em>}</li>
 *   <li>{@link port.PortEvent#getDirection <em>Direction</em>}</li>
 * </ul>
 * </p>
 *
 * @see port.PortPackage#getPortEvent()
 * @model
 * @generated
 */
public interface PortEvent extends PortGeneric {
	/**
	 * Returns the value of the '<em><b>Event Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Name</em>' attribute.
	 * @see #setEventName(String)
	 * @see port.PortPackage#getPortEvent_EventName()
	 * @model
	 * @generated
	 */
	String getEventName();

	/**
	 * Sets the value of the '{@link port.PortEvent#getEventName <em>Event Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Name</em>' attribute.
	 * @see #getEventName()
	 * @generated
	 */
	void setEventName(String value);

	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The default value is <code>"OUTPUT"</code>.
	 * The literals are from the enumeration {@link port.PortIOType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see port.PortIOType
	 * @see port.PortPackage#getPortEvent_Direction()
	 * @model default="OUTPUT" changeable="false"
	 * @generated
	 */
	PortIOType getDirection();

} // PortEvent
