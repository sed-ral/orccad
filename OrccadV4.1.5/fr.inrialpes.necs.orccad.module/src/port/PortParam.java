/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;

import java.util.Map;

import module.ModuleGeneric;

import org.eclipse.emf.common.util.DiagnosticChain;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Param</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link port.PortParam#getRow <em>Row</em>}</li>
 *   <li>{@link port.PortParam#getCol <em>Col</em>}</li>
 *   <li>{@link port.PortParam#getVarName <em>Var Name</em>}</li>
 *   <li>{@link port.PortParam#getVarType <em>Var Type</em>}</li>
 *   <li>{@link port.PortParam#getVarDefaultValue <em>Var Default Value</em>}</li>
 *   <li>{@link port.PortParam#getLevelType <em>Level Type</em>}</li>
 *   <li>{@link port.PortParam#getDirection <em>Direction</em>}</li>
 *   <li>{@link port.PortParam#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see port.PortPackage#getPortParam()
 * @model
 * @generated
 */
public interface PortParam extends PortGeneric {
	/**
	 * Returns the value of the '<em><b>Row</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Row</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Row</em>' attribute.
	 * @see #setRow(int)
	 * @see port.PortPackage#getPortParam_Row()
	 * @model default="1"
	 * @generated
	 */
	int getRow();

	/**
	 * Sets the value of the '{@link port.PortParam#getRow <em>Row</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Row</em>' attribute.
	 * @see #getRow()
	 * @generated
	 */
	void setRow(int value);

	/**
	 * Returns the value of the '<em><b>Col</b></em>' attribute.
	 * The default value is <code>"1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Col</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Col</em>' attribute.
	 * @see #setCol(int)
	 * @see port.PortPackage#getPortParam_Col()
	 * @model default="1"
	 * @generated
	 */
	int getCol();

	/**
	 * Sets the value of the '{@link port.PortParam#getCol <em>Col</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Col</em>' attribute.
	 * @see #getCol()
	 * @generated
	 */
	void setCol(int value);

	/**
	 * Returns the value of the '<em><b>Var Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Name</em>' attribute.
	 * @see #setVarName(String)
	 * @see port.PortPackage#getPortParam_VarName()
	 * @model
	 * @generated
	 */
	String getVarName();

	/**
	 * Sets the value of the '{@link port.PortParam#getVarName <em>Var Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Name</em>' attribute.
	 * @see #getVarName()
	 * @generated
	 */
	void setVarName(String value);

	/**
	 * Returns the value of the '<em><b>Var Type</b></em>' attribute.
	 * The default value is <code>"INTEGER"</code>.
	 * The literals are from the enumeration {@link port.PortVarType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Type</em>' attribute.
	 * @see port.PortVarType
	 * @see #setVarType(PortVarType)
	 * @see port.PortPackage#getPortParam_VarType()
	 * @model default="INTEGER"
	 * @generated
	 */
	PortVarType getVarType();

	/**
	 * Sets the value of the '{@link port.PortParam#getVarType <em>Var Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Type</em>' attribute.
	 * @see port.PortVarType
	 * @see #getVarType()
	 * @generated
	 */
	void setVarType(PortVarType value);

	/**
	 * Returns the value of the '<em><b>Var Default Value</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Var Default Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Var Default Value</em>' attribute.
	 * @see #setVarDefaultValue(String)
	 * @see port.PortPackage#getPortParam_VarDefaultValue()
	 * @model default="0" required="true"
	 * @generated
	 */
	String getVarDefaultValue();

	/**
	 * Sets the value of the '{@link port.PortParam#getVarDefaultValue <em>Var Default Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Var Default Value</em>' attribute.
	 * @see #getVarDefaultValue()
	 * @generated
	 */
	void setVarDefaultValue(String value);

	/**
	 * Returns the value of the '<em><b>Level Type</b></em>' attribute.
	 * The literals are from the enumeration {@link port.PortParamLevelType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level Type</em>' attribute.
	 * @see port.PortParamLevelType
	 * @see #setLevelType(PortParamLevelType)
	 * @see port.PortPackage#getPortParam_LevelType()
	 * @model
	 * @generated
	 */
	PortParamLevelType getLevelType();

	/**
	 * Sets the value of the '{@link port.PortParam#getLevelType <em>Level Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level Type</em>' attribute.
	 * @see port.PortParamLevelType
	 * @see #getLevelType()
	 * @generated
	 */
	void setLevelType(PortParamLevelType value);

	/**
	 * Returns the value of the '<em><b>Direction</b></em>' attribute.
	 * The default value is <code>"INPUT_OUTPUT"</code>.
	 * The literals are from the enumeration {@link port.PortParamIOType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Direction</em>' attribute.
	 * @see port.PortParamIOType
	 * @see #setDirection(PortParamIOType)
	 * @see port.PortPackage#getPortParam_Direction()
	 * @model default="INPUT_OUTPUT"
	 * @generated
	 */
	PortParamIOType getDirection();

	/**
	 * Sets the value of the '{@link port.PortParam#getDirection <em>Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Direction</em>' attribute.
	 * @see port.PortParamIOType
	 * @see #getDirection()
	 * @generated
	 */
	void setDirection(PortParamIOType value);

	/**
	 * Returns the value of the '<em><b>Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link module.ModuleGeneric#getPortParams <em>Port Params</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' container reference.
	 * @see #setContainer(ModuleGeneric)
	 * @see port.PortPackage#getPortParam_Container()
	 * @see module.ModuleGeneric#getPortParams
	 * @model opposite="portParams" transient="false"
	 * @generated
	 */
	ModuleGeneric getContainer();

	/**
	 * Sets the value of the '{@link port.PortParam#getContainer <em>Container</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' container reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(ModuleGeneric value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkVarDefaultValue(DiagnosticChain pg1, Map<Object, Object> pg2);

} // PortParam
