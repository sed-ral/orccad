/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;

import module.ModulePhysical;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Driver</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link port.PortDriver#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see port.PortPackage#getPortDriver()
 * @model
 * @generated
 */
public interface PortDriver extends PortDataDriver {
	/**
	 * Returns the value of the '<em><b>Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link module.ModulePhysical#getPortDrivers <em>Port Drivers</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' container reference.
	 * @see #setContainer(ModulePhysical)
	 * @see port.PortPackage#getPortDriver_Container()
	 * @see module.ModulePhysical#getPortDrivers
	 * @model opposite="portDrivers" transient="false"
	 * @generated
	 */
	ModulePhysical getContainer();

	/**
	 * Sets the value of the '{@link port.PortDriver#getContainer <em>Container</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' container reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(ModulePhysical value);

} // PortDriver
