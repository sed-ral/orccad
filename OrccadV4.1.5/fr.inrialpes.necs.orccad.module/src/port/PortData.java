/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package port;

import module.ModuleAlgorithmic;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link port.PortData#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see port.PortPackage#getPortData()
 * @model
 * @generated
 */
public interface PortData extends PortDataDriver {
	/**
	 * Returns the value of the '<em><b>Container</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link module.ModuleAlgorithmic#getPortDatas <em>Port Datas</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' container reference.
	 * @see port.PortPackage#getPortData_Container()
	 * @see module.ModuleAlgorithmic#getPortDatas
	 * @model opposite="portDatas" required="true" transient="false" changeable="false"
	 * @generated
	 */
	ModuleAlgorithmic getContainer();

} // PortData
