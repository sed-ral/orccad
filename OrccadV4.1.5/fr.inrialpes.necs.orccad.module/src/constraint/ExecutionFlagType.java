/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package constraint;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Execution Flag Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see constraint.ConstraintPackage#getExecutionFlagType()
 * @model
 * @generated
 */
public enum ExecutionFlagType implements Enumerator {
	/**
	 * The '<em><b>STRICT CONSTRAINT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STRICT_CONSTRAINT_VALUE
	 * @generated
	 * @ordered
	 */
	STRICT_CONSTRAINT(3, "STRICT_CONSTRAINT", "STRICT_CONSTRAINT"),

	/**
	 * The '<em><b>SOFT CONSTRAINT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOFT_CONSTRAINT_VALUE
	 * @generated
	 * @ordered
	 */
	SOFT_CONSTRAINT(2, "SOFT_CONSTRAINT", "SOFT_CONSTRAINT"),

	/**
	 * The '<em><b>SKIP CONSTRAINT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SKIP_CONSTRAINT_VALUE
	 * @generated
	 * @ordered
	 */
	SKIP_CONSTRAINT(1, "SKIP_CONSTRAINT", "SKIP_CONSTRAINT"),

	/**
	 * The '<em><b>OTHER</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OTHER_VALUE
	 * @generated
	 * @ordered
	 */
	OTHER(0, "OTHER", "OTHER");

	/**
	 * The '<em><b>STRICT CONSTRAINT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>STRICT CONSTRAINT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STRICT_CONSTRAINT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int STRICT_CONSTRAINT_VALUE = 3;

	/**
	 * The '<em><b>SOFT CONSTRAINT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SOFT CONSTRAINT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SOFT_CONSTRAINT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SOFT_CONSTRAINT_VALUE = 2;

	/**
	 * The '<em><b>SKIP CONSTRAINT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SKIP CONSTRAINT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SKIP_CONSTRAINT
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SKIP_CONSTRAINT_VALUE = 1;

	/**
	 * The '<em><b>OTHER</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>OTHER</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OTHER
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int OTHER_VALUE = 0;

	/**
	 * An array of all the '<em><b>Execution Flag Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ExecutionFlagType[] VALUES_ARRAY =
		new ExecutionFlagType[] {
			STRICT_CONSTRAINT,
			SOFT_CONSTRAINT,
			SKIP_CONSTRAINT,
			OTHER,
		};

	/**
	 * A public read-only list of all the '<em><b>Execution Flag Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ExecutionFlagType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Execution Flag Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ExecutionFlagType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ExecutionFlagType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Execution Flag Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ExecutionFlagType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ExecutionFlagType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Execution Flag Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ExecutionFlagType get(int value) {
		switch (value) {
			case STRICT_CONSTRAINT_VALUE: return STRICT_CONSTRAINT;
			case SOFT_CONSTRAINT_VALUE: return SOFT_CONSTRAINT;
			case SKIP_CONSTRAINT_VALUE: return SKIP_CONSTRAINT;
			case OTHER_VALUE: return OTHER;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ExecutionFlagType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //ExecutionFlagType
