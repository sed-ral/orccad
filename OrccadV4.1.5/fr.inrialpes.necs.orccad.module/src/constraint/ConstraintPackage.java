/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package constraint;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see constraint.ConstraintFactory
 * @model kind="package"
 * @generated
 */
public interface ConstraintPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "constraint";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://constraint";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "constraint";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConstraintPackage eINSTANCE = constraint.impl.ConstraintPackageImpl.init();

	/**
	 * The meta object id for the '{@link constraint.impl.TemporalConstraintImpl <em>Temporal Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see constraint.impl.TemporalConstraintImpl
	 * @see constraint.impl.ConstraintPackageImpl#getTemporalConstraint()
	 * @generated
	 */
	int TEMPORAL_CONSTRAINT = 0;

	/**
	 * The feature id for the '<em><b>Cpu Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORAL_CONSTRAINT__CPU_NAME = 0;

	/**
	 * The feature id for the '<em><b>Execution Flag</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORAL_CONSTRAINT__EXECUTION_FLAG = 1;

	/**
	 * The feature id for the '<em><b>Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORAL_CONSTRAINT__EXECUTION_TIME = 2;

	/**
	 * The feature id for the '<em><b>Execution Time Units</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORAL_CONSTRAINT__EXECUTION_TIME_UNITS = 3;

	/**
	 * The feature id for the '<em><b>Period Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORAL_CONSTRAINT__PERIOD_TIME = 4;

	/**
	 * The feature id for the '<em><b>Period Time Units</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORAL_CONSTRAINT__PERIOD_TIME_UNITS = 5;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORAL_CONSTRAINT__PRIORITY = 6;

	/**
	 * The feature id for the '<em><b>Synchronisation Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORAL_CONSTRAINT__SYNCHRONISATION_TYPE = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORAL_CONSTRAINT__NAME = 8;

	/**
	 * The feature id for the '<em><b>Container</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORAL_CONSTRAINT__CONTAINER = 9;

	/**
	 * The number of structural features of the '<em>Temporal Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TEMPORAL_CONSTRAINT_FEATURE_COUNT = 10;

	/**
	 * The meta object id for the '{@link constraint.ExecutionFlagType <em>Execution Flag Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see constraint.ExecutionFlagType
	 * @see constraint.impl.ConstraintPackageImpl#getExecutionFlagType()
	 * @generated
	 */
	int EXECUTION_FLAG_TYPE = 1;

	/**
	 * The meta object id for the '{@link constraint.TimeUnit <em>Time Unit</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see constraint.TimeUnit
	 * @see constraint.impl.ConstraintPackageImpl#getTimeUnit()
	 * @generated
	 */
	int TIME_UNIT = 2;

	/**
	 * The meta object id for the '{@link constraint.SynchronisationType <em>Synchronisation Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see constraint.SynchronisationType
	 * @see constraint.impl.ConstraintPackageImpl#getSynchronisationType()
	 * @generated
	 */
	int SYNCHRONISATION_TYPE = 3;


	/**
	 * Returns the meta object for class '{@link constraint.TemporalConstraint <em>Temporal Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Temporal Constraint</em>'.
	 * @see constraint.TemporalConstraint
	 * @generated
	 */
	EClass getTemporalConstraint();

	/**
	 * Returns the meta object for the attribute '{@link constraint.TemporalConstraint#getCpuName <em>Cpu Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cpu Name</em>'.
	 * @see constraint.TemporalConstraint#getCpuName()
	 * @see #getTemporalConstraint()
	 * @generated
	 */
	EAttribute getTemporalConstraint_CpuName();

	/**
	 * Returns the meta object for the attribute '{@link constraint.TemporalConstraint#getExecutionFlag <em>Execution Flag</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Flag</em>'.
	 * @see constraint.TemporalConstraint#getExecutionFlag()
	 * @see #getTemporalConstraint()
	 * @generated
	 */
	EAttribute getTemporalConstraint_ExecutionFlag();

	/**
	 * Returns the meta object for the attribute '{@link constraint.TemporalConstraint#getExecutionTime <em>Execution Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Time</em>'.
	 * @see constraint.TemporalConstraint#getExecutionTime()
	 * @see #getTemporalConstraint()
	 * @generated
	 */
	EAttribute getTemporalConstraint_ExecutionTime();

	/**
	 * Returns the meta object for the attribute '{@link constraint.TemporalConstraint#getExecutionTimeUnits <em>Execution Time Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Time Units</em>'.
	 * @see constraint.TemporalConstraint#getExecutionTimeUnits()
	 * @see #getTemporalConstraint()
	 * @generated
	 */
	EAttribute getTemporalConstraint_ExecutionTimeUnits();

	/**
	 * Returns the meta object for the attribute '{@link constraint.TemporalConstraint#getPeriodTime <em>Period Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Period Time</em>'.
	 * @see constraint.TemporalConstraint#getPeriodTime()
	 * @see #getTemporalConstraint()
	 * @generated
	 */
	EAttribute getTemporalConstraint_PeriodTime();

	/**
	 * Returns the meta object for the attribute '{@link constraint.TemporalConstraint#getPeriodTimeUnits <em>Period Time Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Period Time Units</em>'.
	 * @see constraint.TemporalConstraint#getPeriodTimeUnits()
	 * @see #getTemporalConstraint()
	 * @generated
	 */
	EAttribute getTemporalConstraint_PeriodTimeUnits();

	/**
	 * Returns the meta object for the attribute '{@link constraint.TemporalConstraint#getPriority <em>Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Priority</em>'.
	 * @see constraint.TemporalConstraint#getPriority()
	 * @see #getTemporalConstraint()
	 * @generated
	 */
	EAttribute getTemporalConstraint_Priority();

	/**
	 * Returns the meta object for the attribute '{@link constraint.TemporalConstraint#getSynchronisationType <em>Synchronisation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Synchronisation Type</em>'.
	 * @see constraint.TemporalConstraint#getSynchronisationType()
	 * @see #getTemporalConstraint()
	 * @generated
	 */
	EAttribute getTemporalConstraint_SynchronisationType();

	/**
	 * Returns the meta object for the attribute '{@link constraint.TemporalConstraint#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see constraint.TemporalConstraint#getName()
	 * @see #getTemporalConstraint()
	 * @generated
	 */
	EAttribute getTemporalConstraint_Name();

	/**
	 * Returns the meta object for the reference list '{@link constraint.TemporalConstraint#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Container</em>'.
	 * @see constraint.TemporalConstraint#getContainer()
	 * @see #getTemporalConstraint()
	 * @generated
	 */
	EReference getTemporalConstraint_Container();

	/**
	 * Returns the meta object for enum '{@link constraint.ExecutionFlagType <em>Execution Flag Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Execution Flag Type</em>'.
	 * @see constraint.ExecutionFlagType
	 * @generated
	 */
	EEnum getExecutionFlagType();

	/**
	 * Returns the meta object for enum '{@link constraint.TimeUnit <em>Time Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Time Unit</em>'.
	 * @see constraint.TimeUnit
	 * @generated
	 */
	EEnum getTimeUnit();

	/**
	 * Returns the meta object for enum '{@link constraint.SynchronisationType <em>Synchronisation Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Synchronisation Type</em>'.
	 * @see constraint.SynchronisationType
	 * @generated
	 */
	EEnum getSynchronisationType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConstraintFactory getConstraintFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link constraint.impl.TemporalConstraintImpl <em>Temporal Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see constraint.impl.TemporalConstraintImpl
		 * @see constraint.impl.ConstraintPackageImpl#getTemporalConstraint()
		 * @generated
		 */
		EClass TEMPORAL_CONSTRAINT = eINSTANCE.getTemporalConstraint();

		/**
		 * The meta object literal for the '<em><b>Cpu Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPORAL_CONSTRAINT__CPU_NAME = eINSTANCE.getTemporalConstraint_CpuName();

		/**
		 * The meta object literal for the '<em><b>Execution Flag</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPORAL_CONSTRAINT__EXECUTION_FLAG = eINSTANCE.getTemporalConstraint_ExecutionFlag();

		/**
		 * The meta object literal for the '<em><b>Execution Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPORAL_CONSTRAINT__EXECUTION_TIME = eINSTANCE.getTemporalConstraint_ExecutionTime();

		/**
		 * The meta object literal for the '<em><b>Execution Time Units</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPORAL_CONSTRAINT__EXECUTION_TIME_UNITS = eINSTANCE.getTemporalConstraint_ExecutionTimeUnits();

		/**
		 * The meta object literal for the '<em><b>Period Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPORAL_CONSTRAINT__PERIOD_TIME = eINSTANCE.getTemporalConstraint_PeriodTime();

		/**
		 * The meta object literal for the '<em><b>Period Time Units</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPORAL_CONSTRAINT__PERIOD_TIME_UNITS = eINSTANCE.getTemporalConstraint_PeriodTimeUnits();

		/**
		 * The meta object literal for the '<em><b>Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPORAL_CONSTRAINT__PRIORITY = eINSTANCE.getTemporalConstraint_Priority();

		/**
		 * The meta object literal for the '<em><b>Synchronisation Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPORAL_CONSTRAINT__SYNCHRONISATION_TYPE = eINSTANCE.getTemporalConstraint_SynchronisationType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TEMPORAL_CONSTRAINT__NAME = eINSTANCE.getTemporalConstraint_Name();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TEMPORAL_CONSTRAINT__CONTAINER = eINSTANCE.getTemporalConstraint_Container();

		/**
		 * The meta object literal for the '{@link constraint.ExecutionFlagType <em>Execution Flag Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see constraint.ExecutionFlagType
		 * @see constraint.impl.ConstraintPackageImpl#getExecutionFlagType()
		 * @generated
		 */
		EEnum EXECUTION_FLAG_TYPE = eINSTANCE.getExecutionFlagType();

		/**
		 * The meta object literal for the '{@link constraint.TimeUnit <em>Time Unit</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see constraint.TimeUnit
		 * @see constraint.impl.ConstraintPackageImpl#getTimeUnit()
		 * @generated
		 */
		EEnum TIME_UNIT = eINSTANCE.getTimeUnit();

		/**
		 * The meta object literal for the '{@link constraint.SynchronisationType <em>Synchronisation Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see constraint.SynchronisationType
		 * @see constraint.impl.ConstraintPackageImpl#getSynchronisationType()
		 * @generated
		 */
		EEnum SYNCHRONISATION_TYPE = eINSTANCE.getSynchronisationType();

	}

} //ConstraintPackage
