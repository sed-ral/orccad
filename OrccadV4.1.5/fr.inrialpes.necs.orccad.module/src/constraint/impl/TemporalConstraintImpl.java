/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package constraint.impl;

import constraint.ConstraintPackage;
import constraint.ExecutionFlagType;
import constraint.SynchronisationType;
import constraint.TemporalConstraint;
import constraint.TimeUnit;

import module.ModuleAlgorithmic;
import module.ModulePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Temporal Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link constraint.impl.TemporalConstraintImpl#getCpuName <em>Cpu Name</em>}</li>
 *   <li>{@link constraint.impl.TemporalConstraintImpl#getExecutionFlag <em>Execution Flag</em>}</li>
 *   <li>{@link constraint.impl.TemporalConstraintImpl#getExecutionTime <em>Execution Time</em>}</li>
 *   <li>{@link constraint.impl.TemporalConstraintImpl#getExecutionTimeUnits <em>Execution Time Units</em>}</li>
 *   <li>{@link constraint.impl.TemporalConstraintImpl#getPeriodTime <em>Period Time</em>}</li>
 *   <li>{@link constraint.impl.TemporalConstraintImpl#getPeriodTimeUnits <em>Period Time Units</em>}</li>
 *   <li>{@link constraint.impl.TemporalConstraintImpl#getPriority <em>Priority</em>}</li>
 *   <li>{@link constraint.impl.TemporalConstraintImpl#getSynchronisationType <em>Synchronisation Type</em>}</li>
 *   <li>{@link constraint.impl.TemporalConstraintImpl#getName <em>Name</em>}</li>
 *   <li>{@link constraint.impl.TemporalConstraintImpl#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TemporalConstraintImpl extends EObjectImpl implements TemporalConstraint {
	/**
	 * The default value of the '{@link #getCpuName() <em>Cpu Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCpuName()
	 * @generated
	 * @ordered
	 */
	protected static final String CPU_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCpuName() <em>Cpu Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCpuName()
	 * @generated
	 * @ordered
	 */
	protected String cpuName = CPU_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getExecutionFlag() <em>Execution Flag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionFlag()
	 * @generated
	 * @ordered
	 */
	protected static final ExecutionFlagType EXECUTION_FLAG_EDEFAULT = ExecutionFlagType.OTHER;

	/**
	 * The cached value of the '{@link #getExecutionFlag() <em>Execution Flag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionFlag()
	 * @generated
	 * @ordered
	 */
	protected ExecutionFlagType executionFlag = EXECUTION_FLAG_EDEFAULT;

	/**
	 * The default value of the '{@link #getExecutionTime() <em>Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionTime()
	 * @generated
	 * @ordered
	 */
	protected static final double EXECUTION_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getExecutionTime() <em>Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionTime()
	 * @generated
	 * @ordered
	 */
	protected double executionTime = EXECUTION_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getExecutionTimeUnits() <em>Execution Time Units</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionTimeUnits()
	 * @generated
	 * @ordered
	 */
	protected static final TimeUnit EXECUTION_TIME_UNITS_EDEFAULT = TimeUnit.S;

	/**
	 * The cached value of the '{@link #getExecutionTimeUnits() <em>Execution Time Units</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecutionTimeUnits()
	 * @generated
	 * @ordered
	 */
	protected TimeUnit executionTimeUnits = EXECUTION_TIME_UNITS_EDEFAULT;

	/**
	 * The default value of the '{@link #getPeriodTime() <em>Period Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriodTime()
	 * @generated
	 * @ordered
	 */
	protected static final double PERIOD_TIME_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPeriodTime() <em>Period Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriodTime()
	 * @generated
	 * @ordered
	 */
	protected double periodTime = PERIOD_TIME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPeriodTimeUnits() <em>Period Time Units</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriodTimeUnits()
	 * @generated
	 * @ordered
	 */
	protected static final TimeUnit PERIOD_TIME_UNITS_EDEFAULT = TimeUnit.S;

	/**
	 * The cached value of the '{@link #getPeriodTimeUnits() <em>Period Time Units</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriodTimeUnits()
	 * @generated
	 * @ordered
	 */
	protected TimeUnit periodTimeUnits = PERIOD_TIME_UNITS_EDEFAULT;

	/**
	 * The default value of the '{@link #getPriority() <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected static final int PRIORITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getPriority() <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPriority()
	 * @generated
	 * @ordered
	 */
	protected int priority = PRIORITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getSynchronisationType() <em>Synchronisation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSynchronisationType()
	 * @generated
	 * @ordered
	 */
	protected static final SynchronisationType SYNCHRONISATION_TYPE_EDEFAULT = SynchronisationType.CLK;

	/**
	 * The cached value of the '{@link #getSynchronisationType() <em>Synchronisation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSynchronisationType()
	 * @generated
	 * @ordered
	 */
	protected SynchronisationType synchronisationType = SYNCHRONISATION_TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getContainer() <em>Container</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainer()
	 * @generated
	 * @ordered
	 */
	protected EList<ModuleAlgorithmic> container;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TemporalConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintPackage.Literals.TEMPORAL_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getCpuName() {
		return cpuName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCpuName(String newCpuName) {
		String oldCpuName = cpuName;
		cpuName = newCpuName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintPackage.TEMPORAL_CONSTRAINT__CPU_NAME, oldCpuName, cpuName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionFlagType getExecutionFlag() {
		return executionFlag;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionFlag(ExecutionFlagType newExecutionFlag) {
		ExecutionFlagType oldExecutionFlag = executionFlag;
		executionFlag = newExecutionFlag == null ? EXECUTION_FLAG_EDEFAULT : newExecutionFlag;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_FLAG, oldExecutionFlag, executionFlag));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getExecutionTime() {
		return executionTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionTime(double newExecutionTime) {
		double oldExecutionTime = executionTime;
		executionTime = newExecutionTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME, oldExecutionTime, executionTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeUnit getExecutionTimeUnits() {
		return executionTimeUnits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionTimeUnits(TimeUnit newExecutionTimeUnits) {
		TimeUnit oldExecutionTimeUnits = executionTimeUnits;
		executionTimeUnits = newExecutionTimeUnits == null ? EXECUTION_TIME_UNITS_EDEFAULT : newExecutionTimeUnits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME_UNITS, oldExecutionTimeUnits, executionTimeUnits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPeriodTime() {
		return periodTime;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPeriodTime(double newPeriodTime) {
		double oldPeriodTime = periodTime;
		periodTime = newPeriodTime;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME, oldPeriodTime, periodTime));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TimeUnit getPeriodTimeUnits() {
		return periodTimeUnits;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPeriodTimeUnits(TimeUnit newPeriodTimeUnits) {
		TimeUnit oldPeriodTimeUnits = periodTimeUnits;
		periodTimeUnits = newPeriodTimeUnits == null ? PERIOD_TIME_UNITS_EDEFAULT : newPeriodTimeUnits;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME_UNITS, oldPeriodTimeUnits, periodTimeUnits));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPriority() {
		return priority;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPriority(int newPriority) {
		int oldPriority = priority;
		priority = newPriority;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintPackage.TEMPORAL_CONSTRAINT__PRIORITY, oldPriority, priority));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SynchronisationType getSynchronisationType() {
		return synchronisationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSynchronisationType(SynchronisationType newSynchronisationType) {
		SynchronisationType oldSynchronisationType = synchronisationType;
		synchronisationType = newSynchronisationType == null ? SYNCHRONISATION_TYPE_EDEFAULT : newSynchronisationType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintPackage.TEMPORAL_CONSTRAINT__SYNCHRONISATION_TYPE, oldSynchronisationType, synchronisationType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ConstraintPackage.TEMPORAL_CONSTRAINT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModuleAlgorithmic> getContainer() {
		if (container == null) {
			container = new EObjectWithInverseResolvingEList<ModuleAlgorithmic>(ModuleAlgorithmic.class, this, ConstraintPackage.TEMPORAL_CONSTRAINT__CONTAINER, ModulePackage.MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS);
		}
		return container;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConstraintPackage.TEMPORAL_CONSTRAINT__CONTAINER:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getContainer()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ConstraintPackage.TEMPORAL_CONSTRAINT__CONTAINER:
				return ((InternalEList<?>)getContainer()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ConstraintPackage.TEMPORAL_CONSTRAINT__CPU_NAME:
				return getCpuName();
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_FLAG:
				return getExecutionFlag();
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME:
				return getExecutionTime();
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME_UNITS:
				return getExecutionTimeUnits();
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME:
				return getPeriodTime();
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME_UNITS:
				return getPeriodTimeUnits();
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PRIORITY:
				return getPriority();
			case ConstraintPackage.TEMPORAL_CONSTRAINT__SYNCHRONISATION_TYPE:
				return getSynchronisationType();
			case ConstraintPackage.TEMPORAL_CONSTRAINT__NAME:
				return getName();
			case ConstraintPackage.TEMPORAL_CONSTRAINT__CONTAINER:
				return getContainer();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ConstraintPackage.TEMPORAL_CONSTRAINT__CPU_NAME:
				setCpuName((String)newValue);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_FLAG:
				setExecutionFlag((ExecutionFlagType)newValue);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME:
				setExecutionTime((Double)newValue);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME_UNITS:
				setExecutionTimeUnits((TimeUnit)newValue);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME:
				//modified by boudinf
				if((Double)newValue > 0.0) 
					setPeriodTime((Double)newValue);
				else 
					System.out.println("Error - The Periode must be positive !");
					//end modification
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME_UNITS:
				setPeriodTimeUnits((TimeUnit)newValue);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PRIORITY:
				setPriority((Integer)newValue);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__SYNCHRONISATION_TYPE:
				setSynchronisationType((SynchronisationType)newValue);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__NAME:
				if(((String)newValue).matches("[a-zA-Z_0-9]*") ) 
					setName((String)newValue);
				return;
				

		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ConstraintPackage.TEMPORAL_CONSTRAINT__CPU_NAME:
				setCpuName(CPU_NAME_EDEFAULT);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_FLAG:
				setExecutionFlag(EXECUTION_FLAG_EDEFAULT);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME:
				setExecutionTime(EXECUTION_TIME_EDEFAULT);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME_UNITS:
				setExecutionTimeUnits(EXECUTION_TIME_UNITS_EDEFAULT);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME:
				setPeriodTime(PERIOD_TIME_EDEFAULT);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME_UNITS:
				setPeriodTimeUnits(PERIOD_TIME_UNITS_EDEFAULT);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PRIORITY:
				setPriority(PRIORITY_EDEFAULT);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__SYNCHRONISATION_TYPE:
				setSynchronisationType(SYNCHRONISATION_TYPE_EDEFAULT);
				return;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ConstraintPackage.TEMPORAL_CONSTRAINT__CPU_NAME:
				return CPU_NAME_EDEFAULT == null ? cpuName != null : !CPU_NAME_EDEFAULT.equals(cpuName);
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_FLAG:
				return executionFlag != EXECUTION_FLAG_EDEFAULT;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME:
				return executionTime != EXECUTION_TIME_EDEFAULT;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME_UNITS:
				return executionTimeUnits != EXECUTION_TIME_UNITS_EDEFAULT;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME:
				return periodTime != PERIOD_TIME_EDEFAULT;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME_UNITS:
				return periodTimeUnits != PERIOD_TIME_UNITS_EDEFAULT;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PRIORITY:
				return priority != PRIORITY_EDEFAULT;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__SYNCHRONISATION_TYPE:
				return synchronisationType != SYNCHRONISATION_TYPE_EDEFAULT;
			case ConstraintPackage.TEMPORAL_CONSTRAINT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ConstraintPackage.TEMPORAL_CONSTRAINT__CONTAINER:
				return container != null && !container.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (cpuName: ");
		result.append(cpuName);
		result.append(", executionFlag: ");
		result.append(executionFlag);
		result.append(", executionTime: ");
		result.append(executionTime);
		result.append(", executionTimeUnits: ");
		result.append(executionTimeUnits);
		result.append(", periodTime: ");
		result.append(periodTime);
		result.append(", periodTimeUnits: ");
		result.append(periodTimeUnits);
		result.append(", priority: ");
		result.append(priority);
		result.append(", synchronisationType: ");
		result.append(synchronisationType);
		result.append(", name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //TemporalConstraintImpl
