/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package constraint.impl;

import constraint.ConstraintFactory;
import constraint.ConstraintPackage;
import constraint.ExecutionFlagType;
import constraint.SynchronisationType;
import constraint.TemporalConstraint;
import constraint.TimeUnit;

import module.ModulePackage;

import module.impl.ModulePackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import port.PortPackage;

import port.impl.PortPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ConstraintPackageImpl extends EPackageImpl implements ConstraintPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass temporalConstraintEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum executionFlagTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum timeUnitEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum synchronisationTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see constraint.ConstraintPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ConstraintPackageImpl() {
		super(eNS_URI, ConstraintFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ConstraintPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ConstraintPackage init() {
		if (isInited) return (ConstraintPackage)EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI);

		// Obtain or create and register package
		ConstraintPackageImpl theConstraintPackage = (ConstraintPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ConstraintPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ConstraintPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		ModulePackageImpl theModulePackage = (ModulePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ModulePackage.eNS_URI) instanceof ModulePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ModulePackage.eNS_URI) : ModulePackage.eINSTANCE);
		PortPackageImpl thePortPackage = (PortPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) instanceof PortPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) : PortPackage.eINSTANCE);

		// Create package meta-data objects
		theConstraintPackage.createPackageContents();
		theModulePackage.createPackageContents();
		thePortPackage.createPackageContents();

		// Initialize created meta-data
		theConstraintPackage.initializePackageContents();
		theModulePackage.initializePackageContents();
		thePortPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theConstraintPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ConstraintPackage.eNS_URI, theConstraintPackage);
		return theConstraintPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTemporalConstraint() {
		return temporalConstraintEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTemporalConstraint_CpuName() {
		return (EAttribute)temporalConstraintEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTemporalConstraint_ExecutionFlag() {
		return (EAttribute)temporalConstraintEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTemporalConstraint_ExecutionTime() {
		return (EAttribute)temporalConstraintEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTemporalConstraint_ExecutionTimeUnits() {
		return (EAttribute)temporalConstraintEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTemporalConstraint_PeriodTime() {
		return (EAttribute)temporalConstraintEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTemporalConstraint_PeriodTimeUnits() {
		return (EAttribute)temporalConstraintEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTemporalConstraint_Priority() {
		return (EAttribute)temporalConstraintEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTemporalConstraint_SynchronisationType() {
		return (EAttribute)temporalConstraintEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTemporalConstraint_Name() {
		return (EAttribute)temporalConstraintEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTemporalConstraint_Container() {
		return (EReference)temporalConstraintEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getExecutionFlagType() {
		return executionFlagTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTimeUnit() {
		return timeUnitEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSynchronisationType() {
		return synchronisationTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstraintFactory getConstraintFactory() {
		return (ConstraintFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		temporalConstraintEClass = createEClass(TEMPORAL_CONSTRAINT);
		createEAttribute(temporalConstraintEClass, TEMPORAL_CONSTRAINT__CPU_NAME);
		createEAttribute(temporalConstraintEClass, TEMPORAL_CONSTRAINT__EXECUTION_FLAG);
		createEAttribute(temporalConstraintEClass, TEMPORAL_CONSTRAINT__EXECUTION_TIME);
		createEAttribute(temporalConstraintEClass, TEMPORAL_CONSTRAINT__EXECUTION_TIME_UNITS);
		createEAttribute(temporalConstraintEClass, TEMPORAL_CONSTRAINT__PERIOD_TIME);
		createEAttribute(temporalConstraintEClass, TEMPORAL_CONSTRAINT__PERIOD_TIME_UNITS);
		createEAttribute(temporalConstraintEClass, TEMPORAL_CONSTRAINT__PRIORITY);
		createEAttribute(temporalConstraintEClass, TEMPORAL_CONSTRAINT__SYNCHRONISATION_TYPE);
		createEAttribute(temporalConstraintEClass, TEMPORAL_CONSTRAINT__NAME);
		createEReference(temporalConstraintEClass, TEMPORAL_CONSTRAINT__CONTAINER);

		// Create enums
		executionFlagTypeEEnum = createEEnum(EXECUTION_FLAG_TYPE);
		timeUnitEEnum = createEEnum(TIME_UNIT);
		synchronisationTypeEEnum = createEEnum(SYNCHRONISATION_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ModulePackage theModulePackage = (ModulePackage)EPackage.Registry.INSTANCE.getEPackage(ModulePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes and features; add operations and parameters
		initEClass(temporalConstraintEClass, TemporalConstraint.class, "TemporalConstraint", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTemporalConstraint_CpuName(), ecorePackage.getEString(), "cpuName", null, 0, 1, TemporalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTemporalConstraint_ExecutionFlag(), this.getExecutionFlagType(), "executionFlag", "OTHER", 0, 1, TemporalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTemporalConstraint_ExecutionTime(), ecorePackage.getEDouble(), "executionTime", "0.0", 0, 1, TemporalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTemporalConstraint_ExecutionTimeUnits(), this.getTimeUnit(), "executionTimeUnits", "s", 0, 1, TemporalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTemporalConstraint_PeriodTime(), ecorePackage.getEDouble(), "periodTime", "0.0", 0, 1, TemporalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTemporalConstraint_PeriodTimeUnits(), this.getTimeUnit(), "periodTimeUnits", "s", 0, 1, TemporalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTemporalConstraint_Priority(), ecorePackage.getEInt(), "priority", "0", 0, 1, TemporalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTemporalConstraint_SynchronisationType(), this.getSynchronisationType(), "synchronisationType", "CLK", 0, 1, TemporalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTemporalConstraint_Name(), ecorePackage.getEString(), "name", null, 1, 1, TemporalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTemporalConstraint_Container(), theModulePackage.getModuleAlgorithmic(), theModulePackage.getModuleAlgorithmic_TemporalConstraints(), "container", null, 1, -1, TemporalConstraint.class, !IS_TRANSIENT, !IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(executionFlagTypeEEnum, ExecutionFlagType.class, "ExecutionFlagType");
		addEEnumLiteral(executionFlagTypeEEnum, ExecutionFlagType.STRICT_CONSTRAINT);
		addEEnumLiteral(executionFlagTypeEEnum, ExecutionFlagType.SOFT_CONSTRAINT);
		addEEnumLiteral(executionFlagTypeEEnum, ExecutionFlagType.SKIP_CONSTRAINT);
		addEEnumLiteral(executionFlagTypeEEnum, ExecutionFlagType.OTHER);

		initEEnum(timeUnitEEnum, TimeUnit.class, "TimeUnit");
		addEEnumLiteral(timeUnitEEnum, TimeUnit.S);
		addEEnumLiteral(timeUnitEEnum, TimeUnit.MS);
		addEEnumLiteral(timeUnitEEnum, TimeUnit.US);
		addEEnumLiteral(timeUnitEEnum, TimeUnit.NS);

		initEEnum(synchronisationTypeEEnum, SynchronisationType.class, "SynchronisationType");
		addEEnumLiteral(synchronisationTypeEEnum, SynchronisationType.CLK);
		addEEnumLiteral(synchronisationTypeEEnum, SynchronisationType.LINK);
		addEEnumLiteral(synchronisationTypeEEnum, SynchronisationType.LINK_AND_CLK);
		addEEnumLiteral(synchronisationTypeEEnum, SynchronisationType.EXTERN);

		// Create resource
		createResource(eNS_URI);
	}

} //ConstraintPackageImpl
