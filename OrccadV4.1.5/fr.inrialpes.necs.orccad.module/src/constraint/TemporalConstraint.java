/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package constraint;

import module.ModuleAlgorithmic;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Temporal Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link constraint.TemporalConstraint#getCpuName <em>Cpu Name</em>}</li>
 *   <li>{@link constraint.TemporalConstraint#getExecutionFlag <em>Execution Flag</em>}</li>
 *   <li>{@link constraint.TemporalConstraint#getExecutionTime <em>Execution Time</em>}</li>
 *   <li>{@link constraint.TemporalConstraint#getExecutionTimeUnits <em>Execution Time Units</em>}</li>
 *   <li>{@link constraint.TemporalConstraint#getPeriodTime <em>Period Time</em>}</li>
 *   <li>{@link constraint.TemporalConstraint#getPeriodTimeUnits <em>Period Time Units</em>}</li>
 *   <li>{@link constraint.TemporalConstraint#getPriority <em>Priority</em>}</li>
 *   <li>{@link constraint.TemporalConstraint#getSynchronisationType <em>Synchronisation Type</em>}</li>
 *   <li>{@link constraint.TemporalConstraint#getName <em>Name</em>}</li>
 *   <li>{@link constraint.TemporalConstraint#getContainer <em>Container</em>}</li>
 * </ul>
 * </p>
 *
 * @see constraint.ConstraintPackage#getTemporalConstraint()
 * @model
 * @generated
 */
public interface TemporalConstraint extends EObject {
	/**
	 * Returns the value of the '<em><b>Cpu Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cpu Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cpu Name</em>' attribute.
	 * @see #setCpuName(String)
	 * @see constraint.ConstraintPackage#getTemporalConstraint_CpuName()
	 * @model
	 * @generated
	 */
	String getCpuName();

	/**
	 * Sets the value of the '{@link constraint.TemporalConstraint#getCpuName <em>Cpu Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cpu Name</em>' attribute.
	 * @see #getCpuName()
	 * @generated
	 */
	void setCpuName(String value);

	/**
	 * Returns the value of the '<em><b>Execution Flag</b></em>' attribute.
	 * The default value is <code>"OTHER"</code>.
	 * The literals are from the enumeration {@link constraint.ExecutionFlagType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Flag</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Flag</em>' attribute.
	 * @see constraint.ExecutionFlagType
	 * @see #setExecutionFlag(ExecutionFlagType)
	 * @see constraint.ConstraintPackage#getTemporalConstraint_ExecutionFlag()
	 * @model default="OTHER"
	 * @generated
	 */
	ExecutionFlagType getExecutionFlag();

	/**
	 * Sets the value of the '{@link constraint.TemporalConstraint#getExecutionFlag <em>Execution Flag</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Flag</em>' attribute.
	 * @see constraint.ExecutionFlagType
	 * @see #getExecutionFlag()
	 * @generated
	 */
	void setExecutionFlag(ExecutionFlagType value);

	/**
	 * Returns the value of the '<em><b>Execution Time</b></em>' attribute.
	 * The default value is <code>"0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Time</em>' attribute.
	 * @see #setExecutionTime(double)
	 * @see constraint.ConstraintPackage#getTemporalConstraint_ExecutionTime()
	 * @model default="0.0"
	 * @generated
	 */
	double getExecutionTime();

	/**
	 * Sets the value of the '{@link constraint.TemporalConstraint#getExecutionTime <em>Execution Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Time</em>' attribute.
	 * @see #getExecutionTime()
	 * @generated
	 */
	void setExecutionTime(double value);

	/**
	 * Returns the value of the '<em><b>Execution Time Units</b></em>' attribute.
	 * The default value is <code>"s"</code>.
	 * The literals are from the enumeration {@link constraint.TimeUnit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Time Units</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Time Units</em>' attribute.
	 * @see constraint.TimeUnit
	 * @see #setExecutionTimeUnits(TimeUnit)
	 * @see constraint.ConstraintPackage#getTemporalConstraint_ExecutionTimeUnits()
	 * @model default="s"
	 * @generated
	 */
	TimeUnit getExecutionTimeUnits();

	/**
	 * Sets the value of the '{@link constraint.TemporalConstraint#getExecutionTimeUnits <em>Execution Time Units</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Time Units</em>' attribute.
	 * @see constraint.TimeUnit
	 * @see #getExecutionTimeUnits()
	 * @generated
	 */
	void setExecutionTimeUnits(TimeUnit value);

	/**
	 * Returns the value of the '<em><b>Period Time</b></em>' attribute.
	 * The default value is <code>"0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Period Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Period Time</em>' attribute.
	 * @see #setPeriodTime(double)
	 * @see constraint.ConstraintPackage#getTemporalConstraint_PeriodTime()
	 * @model default="0.0"
	 * @generated
	 */
	double getPeriodTime();

	/**
	 * Sets the value of the '{@link constraint.TemporalConstraint#getPeriodTime <em>Period Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Period Time</em>' attribute.
	 * @see #getPeriodTime()
	 * @generated
	 */
	void setPeriodTime(double value);

	/**
	 * Returns the value of the '<em><b>Period Time Units</b></em>' attribute.
	 * The default value is <code>"s"</code>.
	 * The literals are from the enumeration {@link constraint.TimeUnit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Period Time Units</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Period Time Units</em>' attribute.
	 * @see constraint.TimeUnit
	 * @see #setPeriodTimeUnits(TimeUnit)
	 * @see constraint.ConstraintPackage#getTemporalConstraint_PeriodTimeUnits()
	 * @model default="s"
	 * @generated
	 */
	TimeUnit getPeriodTimeUnits();

	/**
	 * Sets the value of the '{@link constraint.TemporalConstraint#getPeriodTimeUnits <em>Period Time Units</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Period Time Units</em>' attribute.
	 * @see constraint.TimeUnit
	 * @see #getPeriodTimeUnits()
	 * @generated
	 */
	void setPeriodTimeUnits(TimeUnit value);

	/**
	 * Returns the value of the '<em><b>Priority</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority</em>' attribute.
	 * @see #setPriority(int)
	 * @see constraint.ConstraintPackage#getTemporalConstraint_Priority()
	 * @model default="0"
	 * @generated
	 */
	int getPriority();

	/**
	 * Sets the value of the '{@link constraint.TemporalConstraint#getPriority <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority</em>' attribute.
	 * @see #getPriority()
	 * @generated
	 */
	void setPriority(int value);

	/**
	 * Returns the value of the '<em><b>Synchronisation Type</b></em>' attribute.
	 * The default value is <code>"CLK"</code>.
	 * The literals are from the enumeration {@link constraint.SynchronisationType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Synchronisation Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Synchronisation Type</em>' attribute.
	 * @see constraint.SynchronisationType
	 * @see #setSynchronisationType(SynchronisationType)
	 * @see constraint.ConstraintPackage#getTemporalConstraint_SynchronisationType()
	 * @model default="CLK"
	 * @generated
	 */
	SynchronisationType getSynchronisationType();

	/**
	 * Sets the value of the '{@link constraint.TemporalConstraint#getSynchronisationType <em>Synchronisation Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Synchronisation Type</em>' attribute.
	 * @see constraint.SynchronisationType
	 * @see #getSynchronisationType()
	 * @generated
	 */
	void setSynchronisationType(SynchronisationType value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see constraint.ConstraintPackage#getTemporalConstraint_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link constraint.TemporalConstraint#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Container</b></em>' reference list.
	 * The list contents are of type {@link module.ModuleAlgorithmic}.
	 * It is bidirectional and its opposite is '{@link module.ModuleAlgorithmic#getTemporalConstraints <em>Temporal Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' reference list.
	 * @see constraint.ConstraintPackage#getTemporalConstraint_Container()
	 * @see module.ModuleAlgorithmic#getTemporalConstraints
	 * @model opposite="temporalConstraints" required="true" changeable="false"
	 * @generated
	 */
	EList<ModuleAlgorithmic> getContainer();

} // TemporalConstraint
