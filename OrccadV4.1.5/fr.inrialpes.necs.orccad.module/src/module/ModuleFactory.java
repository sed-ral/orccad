/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see module.ModulePackage
 * @generated
 */
public interface ModuleFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ModuleFactory eINSTANCE = module.impl.ModuleFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Generic</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Generic</em>'.
	 * @generated
	 */
	ModuleGeneric createModuleGeneric();

	/**
	 * Returns a new object of class '<em>Algorithmic</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Algorithmic</em>'.
	 * @generated
	 */
	ModuleAlgorithmic createModuleAlgorithmic();

	/**
	 * Returns a new object of class '<em>Physical</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical</em>'.
	 * @generated
	 */
	ModulePhysical createModulePhysical();

	/**
	 * Returns a new object of class '<em>Physical Robot</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Robot</em>'.
	 * @generated
	 */
	ModulePhysicalRobot createModulePhysicalRobot();

	/**
	 * Returns a new object of class '<em>Physical Sensor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Physical Sensor</em>'.
	 * @generated
	 */
	ModulePhysicalSensor createModulePhysicalSensor();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ModulePackage getModulePackage();

} //ModuleFactory
