/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module.util;

import module.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see module.ModulePackage
 * @generated
 */
public class ModuleAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ModulePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModuleAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ModulePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModuleSwitch<Adapter> modelSwitch =
		new ModuleSwitch<Adapter>() {
			@Override
			public Adapter caseModuleGeneric(ModuleGeneric object) {
				return createModuleGenericAdapter();
			}
			@Override
			public Adapter caseModuleAlgorithmic(ModuleAlgorithmic object) {
				return createModuleAlgorithmicAdapter();
			}
			@Override
			public Adapter caseModulePhysical(ModulePhysical object) {
				return createModulePhysicalAdapter();
			}
			@Override
			public Adapter caseModulePhysicalRobot(ModulePhysicalRobot object) {
				return createModulePhysicalRobotAdapter();
			}
			@Override
			public Adapter caseModulePhysicalSensor(ModulePhysicalSensor object) {
				return createModulePhysicalSensorAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link module.ModuleGeneric <em>Generic</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see module.ModuleGeneric
	 * @generated
	 */
	public Adapter createModuleGenericAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link module.ModuleAlgorithmic <em>Algorithmic</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see module.ModuleAlgorithmic
	 * @generated
	 */
	public Adapter createModuleAlgorithmicAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link module.ModulePhysical <em>Physical</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see module.ModulePhysical
	 * @generated
	 */
	public Adapter createModulePhysicalAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link module.ModulePhysicalRobot <em>Physical Robot</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see module.ModulePhysicalRobot
	 * @generated
	 */
	public Adapter createModulePhysicalRobotAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link module.ModulePhysicalSensor <em>Physical Sensor</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see module.ModulePhysicalSensor
	 * @generated
	 */
	public Adapter createModulePhysicalSensorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ModuleAdapterFactory
