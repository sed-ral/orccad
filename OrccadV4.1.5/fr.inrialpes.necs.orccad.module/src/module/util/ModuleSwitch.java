/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module.util;

import java.util.List;

import module.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see module.ModulePackage
 * @generated
 */
public class ModuleSwitch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ModulePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModuleSwitch() {
		if (modelPackage == null) {
			modelPackage = ModulePackage.eINSTANCE;
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	public T doSwitch(EObject theEObject) {
		return doSwitch(theEObject.eClass(), theEObject);
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(EClass theEClass, EObject theEObject) {
		if (theEClass.eContainer() == modelPackage) {
			return doSwitch(theEClass.getClassifierID(), theEObject);
		}
		else {
			List<EClass> eSuperTypes = theEClass.getESuperTypes();
			return
				eSuperTypes.isEmpty() ?
					defaultCase(theEObject) :
					doSwitch(eSuperTypes.get(0), theEObject);
		}
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ModulePackage.MODULE_GENERIC: {
				ModuleGeneric moduleGeneric = (ModuleGeneric)theEObject;
				T result = caseModuleGeneric(moduleGeneric);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModulePackage.MODULE_ALGORITHMIC: {
				ModuleAlgorithmic moduleAlgorithmic = (ModuleAlgorithmic)theEObject;
				T result = caseModuleAlgorithmic(moduleAlgorithmic);
				if (result == null) result = caseModuleGeneric(moduleAlgorithmic);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModulePackage.MODULE_PHYSICAL: {
				ModulePhysical modulePhysical = (ModulePhysical)theEObject;
				T result = caseModulePhysical(modulePhysical);
				if (result == null) result = caseModuleGeneric(modulePhysical);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModulePackage.MODULE_PHYSICAL_ROBOT: {
				ModulePhysicalRobot modulePhysicalRobot = (ModulePhysicalRobot)theEObject;
				T result = caseModulePhysicalRobot(modulePhysicalRobot);
				if (result == null) result = caseModulePhysical(modulePhysicalRobot);
				if (result == null) result = caseModuleGeneric(modulePhysicalRobot);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ModulePackage.MODULE_PHYSICAL_SENSOR: {
				ModulePhysicalSensor modulePhysicalSensor = (ModulePhysicalSensor)theEObject;
				T result = caseModulePhysicalSensor(modulePhysicalSensor);
				if (result == null) result = caseModulePhysical(modulePhysicalSensor);
				if (result == null) result = caseModuleGeneric(modulePhysicalSensor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Generic</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Generic</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModuleGeneric(ModuleGeneric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Algorithmic</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Algorithmic</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModuleAlgorithmic(ModuleAlgorithmic object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModulePhysical(ModulePhysical object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Robot</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Robot</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModulePhysicalRobot(ModulePhysicalRobot object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Physical Sensor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Physical Sensor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModulePhysicalSensor(ModulePhysicalSensor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	public T defaultCase(EObject object) {
		return null;
	}

} //ModuleSwitch
