/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module.impl;

import constraint.ConstraintPackage;
import constraint.TemporalConstraint;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import module.ModuleAlgorithmic;
import module.ModuleGeneric;
import module.ModulePackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import port.PortData;
import port.PortIOType;
import port.PortPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Algorithmic</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link module.impl.ModuleAlgorithmicImpl#getFilter <em>Filter</em>}</li>
 *   <li>{@link module.impl.ModuleAlgorithmicImpl#getPortDatas <em>Port Datas</em>}</li>
 *   <li>{@link module.impl.ModuleAlgorithmicImpl#getTemporalConstraints <em>Temporal Constraints</em>}</li>
 *   <li>{@link module.impl.ModuleAlgorithmicImpl#getDependencies <em>Dependencies</em>}</li>
 *   <li>{@link module.impl.ModuleAlgorithmicImpl#getLevel <em>Level</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ModuleAlgorithmicImpl extends ModuleGenericImpl implements ModuleAlgorithmic {
	/**
	 * The default value of the '{@link #getFilter() <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilter()
	 * @generated
	 * @ordered
	 */
	protected static final int FILTER_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getFilter() <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFilter()
	 * @generated
	 * @ordered
	 */
	protected int filter = FILTER_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPortDatas() <em>Port Datas</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortDatas()
	 * @generated
	 * @ordered
	 */
	protected EList<PortData> portDatas;

	/**
	 * The cached value of the '{@link #getTemporalConstraints() <em>Temporal Constraints</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemporalConstraints()
	 * @generated
	 * @ordered
	 */
	protected TemporalConstraint temporalConstraints;

	/**
	 * The cached value of the '{@link #getDependencies() <em>Dependencies</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDependencies()
	 * @generated
	 * @ordered
	 */
	protected EList<ModuleGeneric> dependencies;

	/**
	 * The default value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected static final int LEVEL_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getLevel() <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLevel()
	 * @generated
	 * @ordered
	 */
	protected int level = LEVEL_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModuleAlgorithmicImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModulePackage.Literals.MODULE_ALGORITHMIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getFilter() {
		return filter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFilter(int newFilter) {
		int oldFilter = filter;
		filter = newFilter;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModulePackage.MODULE_ALGORITHMIC__FILTER, oldFilter, filter));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortData> getPortDatas() {
		if (portDatas == null) {
			portDatas = new EObjectContainmentWithInverseEList<PortData>(PortData.class, this, ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS, PortPackage.PORT_DATA__CONTAINER);
		}
		return portDatas;
	}
	
	//added by boudinf
	@SuppressWarnings("null")
	@Override
	public EList<PortData> getPortDatasINPUT() {
		
		
		EList<PortData> pdList = getPortDatas();
		pdList.removeAll(getPortDatas());
		
		for(Iterator<PortData> ip = this.getPortDatas().iterator(); ip.hasNext();){
			PortData pda = ip.next();
			if(pda.getDirection().equals(PortIOType.INPUT)) pdList.add(pda);
		}
		
		return (EList<PortData>) pdList;
	}
	//end boudinf
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemporalConstraint getTemporalConstraints() {
		if (temporalConstraints != null && temporalConstraints.eIsProxy()) {
			InternalEObject oldTemporalConstraints = (InternalEObject)temporalConstraints;
			temporalConstraints = (TemporalConstraint)eResolveProxy(oldTemporalConstraints);
			if (temporalConstraints != oldTemporalConstraints) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ModulePackage.MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS, oldTemporalConstraints, temporalConstraints));
			}
		}
		return temporalConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemporalConstraint basicGetTemporalConstraints() {
		return temporalConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTemporalConstraints(TemporalConstraint newTemporalConstraints, NotificationChain msgs) {
		TemporalConstraint oldTemporalConstraints = temporalConstraints;
		temporalConstraints = newTemporalConstraints;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ModulePackage.MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS, oldTemporalConstraints, newTemporalConstraints);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTemporalConstraints(TemporalConstraint newTemporalConstraints) {
		if (newTemporalConstraints != temporalConstraints) {
			NotificationChain msgs = null;
			if (temporalConstraints != null)
				msgs = ((InternalEObject)temporalConstraints).eInverseRemove(this, ConstraintPackage.TEMPORAL_CONSTRAINT__CONTAINER, TemporalConstraint.class, msgs);
			if (newTemporalConstraints != null)
				msgs = ((InternalEObject)newTemporalConstraints).eInverseAdd(this, ConstraintPackage.TEMPORAL_CONSTRAINT__CONTAINER, TemporalConstraint.class, msgs);
			msgs = basicSetTemporalConstraints(newTemporalConstraints, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModulePackage.MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS, newTemporalConstraints, newTemporalConstraints));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModuleGeneric> getDependencies() {
		if (dependencies == null) {
			dependencies = new EObjectResolvingEList<ModuleGeneric>(ModuleGeneric.class, this, ModulePackage.MODULE_ALGORITHMIC__DEPENDENCIES);
		}
		return dependencies;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLevel() {
		return level;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLevel(int newLevel) {
		int oldLevel = level;
		level = newLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModulePackage.MODULE_ALGORITHMIC__LEVEL, oldLevel, level));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPortDatas()).basicAdd(otherEnd, msgs);
			//added by boudinf
			case ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS_INPUT:
				System.out.println("NotificationChain eInverseAdd");
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPortDatasINPUT()).basicAdd(otherEnd, msgs);	
			//end boudinf
			case ModulePackage.MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS:
				if (temporalConstraints != null)
					msgs = ((InternalEObject)temporalConstraints).eInverseRemove(this, ConstraintPackage.TEMPORAL_CONSTRAINT__CONTAINER, TemporalConstraint.class, msgs);
				return basicSetTemporalConstraints((TemporalConstraint)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS:
				return ((InternalEList<?>)getPortDatas()).basicRemove(otherEnd, msgs);
			case ModulePackage.MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS:
				return basicSetTemporalConstraints(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModulePackage.MODULE_ALGORITHMIC__FILTER:
				return getFilter();
			case ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS:
				System.out.println("eGet");
				return getPortDatas();
				//added by boudinf
			case ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS_INPUT:
				return getPortDatasINPUT();				
				//enf boudinf
			case ModulePackage.MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS:
				if (resolve) return getTemporalConstraints();
				return basicGetTemporalConstraints();
			case ModulePackage.MODULE_ALGORITHMIC__DEPENDENCIES:
				return getDependencies();
			case ModulePackage.MODULE_ALGORITHMIC__LEVEL:
				return getLevel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModulePackage.MODULE_ALGORITHMIC__FILTER:
				setFilter((Integer)newValue);
				return;
			case ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS:
				getPortDatas().clear();
				getPortDatas().addAll((Collection<? extends PortData>)newValue);
				return;
			//added by boudinf
			case ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS_INPUT:
				System.out.println("eSet");
				getPortDatasINPUT().clear();
				getPortDatasINPUT().addAll((Collection<? extends PortData>)newValue);
				return;
				
			//end boudinf
			
			case ModulePackage.MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS:
				setTemporalConstraints((TemporalConstraint)newValue);
				return;
			case ModulePackage.MODULE_ALGORITHMIC__DEPENDENCIES:
				getDependencies().clear();
				getDependencies().addAll((Collection<? extends ModuleGeneric>)newValue);
				return;
			case ModulePackage.MODULE_ALGORITHMIC__LEVEL:
				setLevel((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModulePackage.MODULE_ALGORITHMIC__FILTER:
				setFilter(FILTER_EDEFAULT);
				return;
			case ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS:
				getPortDatas().clear();
				return;
				//added by boudinf
			case ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS_INPUT:
				System.out.println("eUnset");
				getPortDatas().clear();
				return;			
				//end boudinf	
			case ModulePackage.MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS:
				setTemporalConstraints((TemporalConstraint)null);
				return;
			case ModulePackage.MODULE_ALGORITHMIC__DEPENDENCIES:
				getDependencies().clear();
				return;
			case ModulePackage.MODULE_ALGORITHMIC__LEVEL:
				setLevel(LEVEL_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModulePackage.MODULE_ALGORITHMIC__FILTER:
				return filter != FILTER_EDEFAULT;
			case ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS:
				return portDatas != null && !portDatas.isEmpty();
				//added by boudinf
			case ModulePackage.MODULE_ALGORITHMIC__PORT_DATAS_INPUT:
				System.out.println("eIsSet");
				return portDatas != null && !portDatas.isEmpty();				
				// end boudinf
			case ModulePackage.MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS:
				return temporalConstraints != null;
			case ModulePackage.MODULE_ALGORITHMIC__DEPENDENCIES:
				return dependencies != null && !dependencies.isEmpty();
			case ModulePackage.MODULE_ALGORITHMIC__LEVEL:
				return level != LEVEL_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (filter: ");
		result.append(filter);
		result.append(", level: ");
		result.append(level);
		result.append(')');
		return result.toString();
	}



} //ModuleAlgorithmicImpl
