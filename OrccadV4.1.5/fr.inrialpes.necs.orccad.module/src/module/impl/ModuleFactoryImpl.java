/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module.impl;

import module.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ModuleFactoryImpl extends EFactoryImpl implements ModuleFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ModuleFactory init() {
		try {
			ModuleFactory theModuleFactory = (ModuleFactory)EPackage.Registry.INSTANCE.getEFactory("http://module"); 
			if (theModuleFactory != null) {
				return theModuleFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ModuleFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModuleFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ModulePackage.MODULE_GENERIC: return createModuleGeneric();
			case ModulePackage.MODULE_ALGORITHMIC: return createModuleAlgorithmic();
			case ModulePackage.MODULE_PHYSICAL: return createModulePhysical();
			case ModulePackage.MODULE_PHYSICAL_ROBOT: return createModulePhysicalRobot();
			case ModulePackage.MODULE_PHYSICAL_SENSOR: return createModulePhysicalSensor();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case ModulePackage.STATUS_TYPE:
				return createStatusTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case ModulePackage.STATUS_TYPE:
				return convertStatusTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ModuleGeneric createModuleGeneric() {
		ModuleGenericImpl moduleGeneric = new ModuleGenericImpl();
		generateID(moduleGeneric);
		return moduleGeneric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ModuleAlgorithmic createModuleAlgorithmic() {
		ModuleAlgorithmicImpl moduleAlgorithmic = new ModuleAlgorithmicImpl();
		generateID(moduleAlgorithmic);
		return moduleAlgorithmic;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModulePhysical createModulePhysical() {
		ModulePhysicalImpl modulePhysical = new ModulePhysicalImpl();
		return modulePhysical;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ModulePhysicalRobot createModulePhysicalRobot() {
		ModulePhysicalRobotImpl modulePhysicalRobot = new ModulePhysicalRobotImpl();
		generateID(modulePhysicalRobot);
		return modulePhysicalRobot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public ModulePhysicalSensor createModulePhysicalSensor() {
		ModulePhysicalSensorImpl modulePhysicalSensor = new ModulePhysicalSensorImpl();
		generateID(modulePhysicalSensor);
		return modulePhysicalSensor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatusType createStatusTypeFromString(EDataType eDataType, String initialValue) {
		StatusType result = StatusType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertStatusTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModulePackage getModulePackage() {
		return (ModulePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ModulePackage getPackage() {
		return ModulePackage.eINSTANCE;
	}
	
	
	
	private static long generate_id_counter = 0;

	protected void generateID(EObject obj) {
	  if (obj instanceof ModuleGeneric) {
		  ModuleGeneric ge = (ModuleGeneric) obj;     
	    generate_id_counter++;      
	    ge.setId(Long.toHexString(generate_id_counter) );
	  }
	}


} //ModuleFactoryImpl
