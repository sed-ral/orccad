/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module.impl;

import java.util.Collection;
import java.util.Iterator;

import module.ModulePackage;
import module.ModulePhysical;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import port.PortDriver;
import port.PortPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link module.impl.ModulePhysicalImpl#getPortDrivers <em>Port Drivers</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ModulePhysicalImpl extends ModuleGenericImpl implements ModulePhysical {
	/**
	 * The cached value of the '{@link #getPortDrivers() <em>Port Drivers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortDrivers()
	 * @generated
	 * @ordered
	 */
	protected EList<PortDriver> portDrivers;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModulePhysicalImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModulePackage.Literals.MODULE_PHYSICAL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortDriver> getPortDrivers() {
		if (portDrivers == null) {
			portDrivers = new EObjectContainmentWithInverseEList<PortDriver>(PortDriver.class, this, ModulePackage.MODULE_PHYSICAL__PORT_DRIVERS, PortPackage.PORT_DRIVER__CONTAINER);
		}
		return portDrivers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModulePackage.MODULE_PHYSICAL__PORT_DRIVERS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPortDrivers()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModulePackage.MODULE_PHYSICAL__PORT_DRIVERS:
				return ((InternalEList<?>)getPortDrivers()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		

		
		
		switch (featureID) {
			case ModulePackage.MODULE_PHYSICAL__PORT_DRIVERS:
				//added by boudinf
				System.out.println(	"featureID  " + featureID +
									"\nresolve  " + resolve +
									"\ncoreType " +	coreType						
									);
				// end boudinf
				
				
				for(Iterator<PortDriver> itpd = getPortDrivers().iterator(); itpd.hasNext();){
					PortDriver pdriver = itpd.next();
					
					System.out.println(pdriver.toString() );
				}
				
				return getPortDrivers();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModulePackage.MODULE_PHYSICAL__PORT_DRIVERS:
				getPortDrivers().clear();
				getPortDrivers().addAll((Collection<? extends PortDriver>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModulePackage.MODULE_PHYSICAL__PORT_DRIVERS:
				getPortDrivers().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModulePackage.MODULE_PHYSICAL__PORT_DRIVERS:
				return portDrivers != null && !portDrivers.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //ModulePhysicalImpl
