/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module.impl;

import module.ModulePackage;
import module.ModulePhysicalRobot;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Physical Robot</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ModulePhysicalRobotImpl extends ModulePhysicalImpl implements ModulePhysicalRobot {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModulePhysicalRobotImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModulePackage.Literals.MODULE_PHYSICAL_ROBOT;
	}

} //ModulePhysicalRobotImpl
