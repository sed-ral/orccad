/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module.impl;

import java.util.Collection;

import module.ModuleGeneric;
import module.ModulePackage;
import module.StatusType;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import port.EventCondition;
import port.ExceptionT1;
import port.ExceptionT2;
import port.ExceptionT3;
import port.PortPackage;
import port.PortParam;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Generic</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link module.impl.ModuleGenericImpl#getAuthor <em>Author</em>}</li>
 *   <li>{@link module.impl.ModuleGenericImpl#getComment <em>Comment</em>}</li>
 *   <li>{@link module.impl.ModuleGenericImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link module.impl.ModuleGenericImpl#getName <em>Name</em>}</li>
 *   <li>{@link module.impl.ModuleGenericImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link module.impl.ModuleGenericImpl#getId <em>Id</em>}</li>
 *   <li>{@link module.impl.ModuleGenericImpl#getSourceFileName <em>Source File Name</em>}</li>
 *   <li>{@link module.impl.ModuleGenericImpl#getPortParams <em>Port Params</em>}</li>
 *   <li>{@link module.impl.ModuleGenericImpl#getEventConditions <em>Event Conditions</em>}</li>
 *   <li>{@link module.impl.ModuleGenericImpl#getEventT1s <em>Event T1s</em>}</li>
 *   <li>{@link module.impl.ModuleGenericImpl#getEventT2s <em>Event T2s</em>}</li>
 *   <li>{@link module.impl.ModuleGenericImpl#getEventT3s <em>Event T3s</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ModuleGenericImpl extends EObjectImpl implements ModuleGeneric {
	/**
	 * The default value of the '{@link #getAuthor() <em>Author</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuthor()
	 * @generated
	 * @ordered
	 */
	protected static final String AUTHOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAuthor() <em>Author</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAuthor()
	 * @generated
	 * @ordered
	 */
	protected String author = AUTHOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected static final String COMMENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getComment() <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComment()
	 * @generated
	 * @ordered
	 */
	protected String comment = COMMENT_EDEFAULT;

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final String VERSION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected String version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final StatusType STATUS_EDEFAULT = StatusType.VALID;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected StatusType status = STATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getSourceFileName() <em>Source File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceFileName()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_FILE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSourceFileName() <em>Source File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceFileName()
	 * @generated
	 * @ordered
	 */
	protected String sourceFileName = SOURCE_FILE_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPortParams() <em>Port Params</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPortParams()
	 * @generated
	 * @ordered
	 */
	protected EList<PortParam> portParams;

	/**
	 * The cached value of the '{@link #getEventConditions() <em>Event Conditions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventConditions()
	 * @generated
	 * @ordered
	 */
	protected EList<EventCondition> eventConditions;

	/**
	 * The cached value of the '{@link #getEventT1s() <em>Event T1s</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventT1s()
	 * @generated
	 * @ordered
	 */
	protected EList<ExceptionT1> eventT1s;

	/**
	 * The cached value of the '{@link #getEventT2s() <em>Event T2s</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventT2s()
	 * @generated
	 * @ordered
	 */
	protected EList<ExceptionT2> eventT2s;

	/**
	 * The cached value of the '{@link #getEventT3s() <em>Event T3s</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEventT3s()
	 * @generated
	 * @ordered
	 */
	protected EList<ExceptionT3> eventT3s;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModuleGenericImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ModulePackage.Literals.MODULE_GENERIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAuthor() {
		return author;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAuthor(String newAuthor) {
		String oldAuthor = author;
		author = newAuthor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModulePackage.MODULE_GENERIC__AUTHOR, oldAuthor, author));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComment(String newComment) {
		String oldComment = comment;
		comment = newComment;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModulePackage.MODULE_GENERIC__COMMENT, oldComment, comment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		String oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModulePackage.MODULE_GENERIC__VERSION, oldVersion, version));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModulePackage.MODULE_GENERIC__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatusType getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(StatusType newStatus) {
		StatusType oldStatus = status;
		status = newStatus == null ? STATUS_EDEFAULT : newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModulePackage.MODULE_GENERIC__STATUS, oldStatus, status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModulePackage.MODULE_GENERIC__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourceFileName() {
		return sourceFileName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceFileName(String newSourceFileName) {
		String oldSourceFileName = sourceFileName;
		sourceFileName = newSourceFileName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ModulePackage.MODULE_GENERIC__SOURCE_FILE_NAME, oldSourceFileName, sourceFileName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<PortParam> getPortParams() {
		if (portParams == null) {
			portParams = new EObjectContainmentWithInverseEList<PortParam>(PortParam.class, this, ModulePackage.MODULE_GENERIC__PORT_PARAMS, PortPackage.PORT_PARAM__CONTAINER);
		}
		return portParams;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EventCondition> getEventConditions() {
		if (eventConditions == null) {
			eventConditions = new EObjectContainmentWithInverseEList<EventCondition>(EventCondition.class, this, ModulePackage.MODULE_GENERIC__EVENT_CONDITIONS, PortPackage.EVENT_CONDITION__CONTAINER);
		}
		return eventConditions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExceptionT1> getEventT1s() {
		if (eventT1s == null) {
			eventT1s = new EObjectContainmentWithInverseEList<ExceptionT1>(ExceptionT1.class, this, ModulePackage.MODULE_GENERIC__EVENT_T1S, PortPackage.EXCEPTION_T1__CONTAINER);
		}
		return eventT1s;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExceptionT2> getEventT2s() {
		if (eventT2s == null) {
			eventT2s = new EObjectContainmentWithInverseEList<ExceptionT2>(ExceptionT2.class, this, ModulePackage.MODULE_GENERIC__EVENT_T2S, PortPackage.EXCEPTION_T2__CONTAINER);
		}
		return eventT2s;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExceptionT3> getEventT3s() {
		if (eventT3s == null) {
			eventT3s = new EObjectContainmentWithInverseEList<ExceptionT3>(ExceptionT3.class, this, ModulePackage.MODULE_GENERIC__EVENT_T3S, PortPackage.EXCEPTION_T3__CONTAINER);
		}
		return eventT3s;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModulePackage.MODULE_GENERIC__PORT_PARAMS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPortParams()).basicAdd(otherEnd, msgs);
			case ModulePackage.MODULE_GENERIC__EVENT_CONDITIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEventConditions()).basicAdd(otherEnd, msgs);
			case ModulePackage.MODULE_GENERIC__EVENT_T1S:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEventT1s()).basicAdd(otherEnd, msgs);
			case ModulePackage.MODULE_GENERIC__EVENT_T2S:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEventT2s()).basicAdd(otherEnd, msgs);
			case ModulePackage.MODULE_GENERIC__EVENT_T3S:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getEventT3s()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ModulePackage.MODULE_GENERIC__PORT_PARAMS:
				return ((InternalEList<?>)getPortParams()).basicRemove(otherEnd, msgs);
			case ModulePackage.MODULE_GENERIC__EVENT_CONDITIONS:
				return ((InternalEList<?>)getEventConditions()).basicRemove(otherEnd, msgs);
			case ModulePackage.MODULE_GENERIC__EVENT_T1S:
				return ((InternalEList<?>)getEventT1s()).basicRemove(otherEnd, msgs);
			case ModulePackage.MODULE_GENERIC__EVENT_T2S:
				return ((InternalEList<?>)getEventT2s()).basicRemove(otherEnd, msgs);
			case ModulePackage.MODULE_GENERIC__EVENT_T3S:
				return ((InternalEList<?>)getEventT3s()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ModulePackage.MODULE_GENERIC__AUTHOR:
				return getAuthor();
			case ModulePackage.MODULE_GENERIC__COMMENT:
				return getComment();
			case ModulePackage.MODULE_GENERIC__VERSION:
				return getVersion();
			case ModulePackage.MODULE_GENERIC__NAME:
				return getName();
			case ModulePackage.MODULE_GENERIC__STATUS:
				return getStatus();
			case ModulePackage.MODULE_GENERIC__ID:
				return getId();
			case ModulePackage.MODULE_GENERIC__SOURCE_FILE_NAME:
				return getSourceFileName();
			case ModulePackage.MODULE_GENERIC__PORT_PARAMS:
				return getPortParams();
			case ModulePackage.MODULE_GENERIC__EVENT_CONDITIONS:
				return getEventConditions();
			case ModulePackage.MODULE_GENERIC__EVENT_T1S:
				return getEventT1s();
			case ModulePackage.MODULE_GENERIC__EVENT_T2S:
				return getEventT2s();
			case ModulePackage.MODULE_GENERIC__EVENT_T3S:
				return getEventT3s();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ModulePackage.MODULE_GENERIC__AUTHOR:
				setAuthor((String)newValue);
				return;
			case ModulePackage.MODULE_GENERIC__COMMENT:
				setComment((String)newValue);
				return;
			case ModulePackage.MODULE_GENERIC__VERSION:
				setVersion((String)newValue);
				return;
			case ModulePackage.MODULE_GENERIC__NAME:
				if(((String)newValue).matches("[a-zA-Z_0-9]*") )// added by boudinf
					setName((String)newValue);
				return;
				
			case ModulePackage.MODULE_GENERIC__STATUS:
				setStatus((StatusType)newValue);
				return;
			case ModulePackage.MODULE_GENERIC__ID:
				setId((String)newValue);
				return;
			case ModulePackage.MODULE_GENERIC__SOURCE_FILE_NAME:
				setSourceFileName((String)newValue);
				return;
			case ModulePackage.MODULE_GENERIC__PORT_PARAMS:
				getPortParams().clear();
				getPortParams().addAll((Collection<? extends PortParam>)newValue);
				return;
			case ModulePackage.MODULE_GENERIC__EVENT_CONDITIONS:
				getEventConditions().clear();
				getEventConditions().addAll((Collection<? extends EventCondition>)newValue);
				return;
			case ModulePackage.MODULE_GENERIC__EVENT_T1S:
				getEventT1s().clear();
				getEventT1s().addAll((Collection<? extends ExceptionT1>)newValue);
				return;
			case ModulePackage.MODULE_GENERIC__EVENT_T2S:
				getEventT2s().clear();
				getEventT2s().addAll((Collection<? extends ExceptionT2>)newValue);
				return;
			case ModulePackage.MODULE_GENERIC__EVENT_T3S:
				getEventT3s().clear();
				getEventT3s().addAll((Collection<? extends ExceptionT3>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ModulePackage.MODULE_GENERIC__AUTHOR:
				setAuthor(AUTHOR_EDEFAULT);
				return;
			case ModulePackage.MODULE_GENERIC__COMMENT:
				setComment(COMMENT_EDEFAULT);
				return;
			case ModulePackage.MODULE_GENERIC__VERSION:
				setVersion(VERSION_EDEFAULT);
				return;
			case ModulePackage.MODULE_GENERIC__NAME:
				setName(NAME_EDEFAULT);
				return;
			case ModulePackage.MODULE_GENERIC__STATUS:
				setStatus(STATUS_EDEFAULT);
				return;
			case ModulePackage.MODULE_GENERIC__ID:
				setId(ID_EDEFAULT);
				return;
			case ModulePackage.MODULE_GENERIC__SOURCE_FILE_NAME:
				setSourceFileName(SOURCE_FILE_NAME_EDEFAULT);
				return;
			case ModulePackage.MODULE_GENERIC__PORT_PARAMS:
				getPortParams().clear();
				return;
			case ModulePackage.MODULE_GENERIC__EVENT_CONDITIONS:
				getEventConditions().clear();
				return;
			case ModulePackage.MODULE_GENERIC__EVENT_T1S:
				getEventT1s().clear();
				return;
			case ModulePackage.MODULE_GENERIC__EVENT_T2S:
				getEventT2s().clear();
				return;
			case ModulePackage.MODULE_GENERIC__EVENT_T3S:
				getEventT3s().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ModulePackage.MODULE_GENERIC__AUTHOR:
				return AUTHOR_EDEFAULT == null ? author != null : !AUTHOR_EDEFAULT.equals(author);
			case ModulePackage.MODULE_GENERIC__COMMENT:
				return COMMENT_EDEFAULT == null ? comment != null : !COMMENT_EDEFAULT.equals(comment);
			case ModulePackage.MODULE_GENERIC__VERSION:
				return VERSION_EDEFAULT == null ? version != null : !VERSION_EDEFAULT.equals(version);
			case ModulePackage.MODULE_GENERIC__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case ModulePackage.MODULE_GENERIC__STATUS:
				return status != STATUS_EDEFAULT;
			case ModulePackage.MODULE_GENERIC__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case ModulePackage.MODULE_GENERIC__SOURCE_FILE_NAME:
				return SOURCE_FILE_NAME_EDEFAULT == null ? sourceFileName != null : !SOURCE_FILE_NAME_EDEFAULT.equals(sourceFileName);
			case ModulePackage.MODULE_GENERIC__PORT_PARAMS:
				return portParams != null && !portParams.isEmpty();
			case ModulePackage.MODULE_GENERIC__EVENT_CONDITIONS:
				return eventConditions != null && !eventConditions.isEmpty();
			case ModulePackage.MODULE_GENERIC__EVENT_T1S:
				return eventT1s != null && !eventT1s.isEmpty();
			case ModulePackage.MODULE_GENERIC__EVENT_T2S:
				return eventT2s != null && !eventT2s.isEmpty();
			case ModulePackage.MODULE_GENERIC__EVENT_T3S:
				return eventT3s != null && !eventT3s.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (author: ");
		result.append(author);
		result.append(", comment: ");
		result.append(comment);
		result.append(", version: ");
		result.append(version);
		result.append(", name: ");
		result.append(name);
		result.append(", status: ");
		result.append(status);
		result.append(", id: ");
		result.append(id);
		result.append(", sourceFileName: ");
		result.append(sourceFileName);
		result.append(')');
		return result.toString();
	}

} //ModuleGenericImpl
