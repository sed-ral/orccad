/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module.impl;

import constraint.ConstraintPackage;

import constraint.impl.ConstraintPackageImpl;

import module.ModuleAlgorithmic;
import module.ModuleFactory;
import module.ModuleGeneric;
import module.ModulePackage;
import module.ModulePhysical;
import module.ModulePhysicalRobot;
import module.ModulePhysicalSensor;
import module.StatusType;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import port.PortPackage;

import port.impl.PortPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ModulePackageImpl extends EPackageImpl implements ModulePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moduleGenericEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass moduleAlgorithmicEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modulePhysicalEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modulePhysicalRobotEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass modulePhysicalSensorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum statusTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see module.ModulePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ModulePackageImpl() {
		super(eNS_URI, ModuleFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ModulePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ModulePackage init() {
		if (isInited) return (ModulePackage)EPackage.Registry.INSTANCE.getEPackage(ModulePackage.eNS_URI);

		// Obtain or create and register package
		ModulePackageImpl theModulePackage = (ModulePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ModulePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ModulePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		PortPackageImpl thePortPackage = (PortPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) instanceof PortPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI) : PortPackage.eINSTANCE);
		ConstraintPackageImpl theConstraintPackage = (ConstraintPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) instanceof ConstraintPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) : ConstraintPackage.eINSTANCE);

		// Create package meta-data objects
		theModulePackage.createPackageContents();
		thePortPackage.createPackageContents();
		theConstraintPackage.createPackageContents();

		// Initialize created meta-data
		theModulePackage.initializePackageContents();
		thePortPackage.initializePackageContents();
		theConstraintPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theModulePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ModulePackage.eNS_URI, theModulePackage);
		return theModulePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModuleGeneric() {
		return moduleGenericEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModuleGeneric_Author() {
		return (EAttribute)moduleGenericEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModuleGeneric_Comment() {
		return (EAttribute)moduleGenericEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModuleGeneric_Version() {
		return (EAttribute)moduleGenericEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModuleGeneric_Name() {
		return (EAttribute)moduleGenericEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModuleGeneric_Status() {
		return (EAttribute)moduleGenericEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModuleGeneric_Id() {
		return (EAttribute)moduleGenericEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModuleGeneric_SourceFileName() {
		return (EAttribute)moduleGenericEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModuleGeneric_PortParams() {
		return (EReference)moduleGenericEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModuleGeneric_EventConditions() {
		return (EReference)moduleGenericEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModuleGeneric_EventT1s() {
		return (EReference)moduleGenericEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModuleGeneric_EventT2s() {
		return (EReference)moduleGenericEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModuleGeneric_EventT3s() {
		return (EReference)moduleGenericEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModuleAlgorithmic() {
		return moduleAlgorithmicEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModuleAlgorithmic_Filter() {
		return (EAttribute)moduleAlgorithmicEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModuleAlgorithmic_PortDatas() {
		return (EReference)moduleAlgorithmicEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModuleAlgorithmic_TemporalConstraints() {
		return (EReference)moduleAlgorithmicEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModuleAlgorithmic_Dependencies() {
		return (EReference)moduleAlgorithmicEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModuleAlgorithmic_Level() {
		return (EAttribute)moduleAlgorithmicEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModulePhysical() {
		return modulePhysicalEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getModulePhysical_PortDrivers() {
		return (EReference)modulePhysicalEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModulePhysicalRobot() {
		return modulePhysicalRobotEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getModulePhysicalSensor() {
		return modulePhysicalSensorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getModulePhysicalSensor_Shared() {
		return (EAttribute)modulePhysicalSensorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStatusType() {
		return statusTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModuleFactory getModuleFactory() {
		return (ModuleFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		moduleGenericEClass = createEClass(MODULE_GENERIC);
		createEAttribute(moduleGenericEClass, MODULE_GENERIC__AUTHOR);
		createEAttribute(moduleGenericEClass, MODULE_GENERIC__COMMENT);
		createEAttribute(moduleGenericEClass, MODULE_GENERIC__VERSION);
		createEAttribute(moduleGenericEClass, MODULE_GENERIC__NAME);
		createEAttribute(moduleGenericEClass, MODULE_GENERIC__STATUS);
		createEAttribute(moduleGenericEClass, MODULE_GENERIC__ID);
		createEAttribute(moduleGenericEClass, MODULE_GENERIC__SOURCE_FILE_NAME);
		createEReference(moduleGenericEClass, MODULE_GENERIC__PORT_PARAMS);
		createEReference(moduleGenericEClass, MODULE_GENERIC__EVENT_CONDITIONS);
		createEReference(moduleGenericEClass, MODULE_GENERIC__EVENT_T1S);
		createEReference(moduleGenericEClass, MODULE_GENERIC__EVENT_T2S);
		createEReference(moduleGenericEClass, MODULE_GENERIC__EVENT_T3S);

		moduleAlgorithmicEClass = createEClass(MODULE_ALGORITHMIC);
		createEAttribute(moduleAlgorithmicEClass, MODULE_ALGORITHMIC__FILTER);
		createEReference(moduleAlgorithmicEClass, MODULE_ALGORITHMIC__PORT_DATAS);
		//added by boudinf
		System.out.println("create EReference in ModulePackageImpl - ModuleAlgo");
		//createEReference(moduleAlgorithmicEClass, MODULE_ALGORITHMIC__PORT_DATAS_INPUT);
		//boudinf
		createEReference(moduleAlgorithmicEClass, MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS);
		createEReference(moduleAlgorithmicEClass, MODULE_ALGORITHMIC__DEPENDENCIES);
		createEAttribute(moduleAlgorithmicEClass, MODULE_ALGORITHMIC__LEVEL);

		modulePhysicalEClass = createEClass(MODULE_PHYSICAL);
		createEReference(modulePhysicalEClass, MODULE_PHYSICAL__PORT_DRIVERS);

		modulePhysicalRobotEClass = createEClass(MODULE_PHYSICAL_ROBOT);

		modulePhysicalSensorEClass = createEClass(MODULE_PHYSICAL_SENSOR);
		createEAttribute(modulePhysicalSensorEClass, MODULE_PHYSICAL_SENSOR__SHARED);

		// Create enums
		statusTypeEEnum = createEEnum(STATUS_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		PortPackage thePortPackage = (PortPackage)EPackage.Registry.INSTANCE.getEPackage(PortPackage.eNS_URI);
		ConstraintPackage theConstraintPackage = (ConstraintPackage)EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		moduleAlgorithmicEClass.getESuperTypes().add(this.getModuleGeneric());
		modulePhysicalEClass.getESuperTypes().add(this.getModuleGeneric());
		modulePhysicalRobotEClass.getESuperTypes().add(this.getModulePhysical());
		modulePhysicalSensorEClass.getESuperTypes().add(this.getModulePhysical());

		// Initialize classes and features; add operations and parameters
		initEClass(moduleGenericEClass, ModuleGeneric.class, "ModuleGeneric", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModuleGeneric_Author(), ecorePackage.getEString(), "author", null, 0, 1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModuleGeneric_Comment(), ecorePackage.getEString(), "comment", null, 0, 1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModuleGeneric_Version(), ecorePackage.getEString(), "version", null, 0, 1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModuleGeneric_Name(), ecorePackage.getEString(), "name", null, 1, 1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModuleGeneric_Status(), this.getStatusType(), "status", null, 0, 1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModuleGeneric_Id(), ecorePackage.getEString(), "id", null, 0, 1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModuleGeneric_SourceFileName(), ecorePackage.getEString(), "sourceFileName", null, 0, 1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModuleGeneric_PortParams(), thePortPackage.getPortParam(), thePortPackage.getPortParam_Container(), "portParams", null, 0, -1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModuleGeneric_EventConditions(), thePortPackage.getEventCondition(), thePortPackage.getEventCondition_Container(), "eventConditions", null, 0, -1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModuleGeneric_EventT1s(), thePortPackage.getExceptionT1(), thePortPackage.getExceptionT1_Container(), "eventT1s", null, 0, -1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModuleGeneric_EventT2s(), thePortPackage.getExceptionT2(), thePortPackage.getExceptionT2_Container(), "eventT2s", null, 0, -1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModuleGeneric_EventT3s(), thePortPackage.getExceptionT3(), thePortPackage.getExceptionT3_Container(), "eventT3s", null, 0, -1, ModuleGeneric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(moduleAlgorithmicEClass, ModuleAlgorithmic.class, "ModuleAlgorithmic", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModuleAlgorithmic_Filter(), ecorePackage.getEInt(), "filter", "0", 0, 1, ModuleAlgorithmic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModuleAlgorithmic_PortDatas(), thePortPackage.getPortData(), thePortPackage.getPortData_Container(), "portDatas", null, 0, -1, ModuleAlgorithmic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModuleAlgorithmic_TemporalConstraints(), theConstraintPackage.getTemporalConstraint(), theConstraintPackage.getTemporalConstraint_Container(), "temporalConstraints", null, 1, 1, ModuleAlgorithmic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getModuleAlgorithmic_Dependencies(), this.getModuleGeneric(), null, "dependencies", null, 0, -1, ModuleAlgorithmic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getModuleAlgorithmic_Level(), ecorePackage.getEInt(), "level", "0", 1, 1, ModuleAlgorithmic.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(modulePhysicalEClass, ModulePhysical.class, "ModulePhysical", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getModulePhysical_PortDrivers(), thePortPackage.getPortDriver(), thePortPackage.getPortDriver_Container(), "portDrivers", null, 0, -1, ModulePhysical.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(modulePhysicalRobotEClass, ModulePhysicalRobot.class, "ModulePhysicalRobot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(modulePhysicalSensorEClass, ModulePhysicalSensor.class, "ModulePhysicalSensor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getModulePhysicalSensor_Shared(), ecorePackage.getEBoolean(), "shared", null, 0, 1, ModulePhysicalSensor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(statusTypeEEnum, StatusType.class, "StatusType");
		addEEnumLiteral(statusTypeEEnum, StatusType.VALID);
		addEEnumLiteral(statusTypeEEnum, StatusType.NOT_VALID);
		addEEnumLiteral(statusTypeEEnum, StatusType.STARTED);
		addEEnumLiteral(statusTypeEEnum, StatusType.TERMINATED);
		addEEnumLiteral(statusTypeEEnum, StatusType.MODIFIED);

		// Create resource
		createResource(eNS_URI);
	}

	@Override
	public EReference getModuleAlgorithmic_PortDatas_INPUT() {
		
		EStructuralFeature esf = moduleAlgorithmicEClass.getEStructuralFeatures().get(1);
		
		System.out.println("blablabla " + esf.toString());
		
		EReference er = (EReference)moduleAlgorithmicEClass.getEStructuralFeatures().get(1);
		return er;
	}

} //ModulePackageImpl
