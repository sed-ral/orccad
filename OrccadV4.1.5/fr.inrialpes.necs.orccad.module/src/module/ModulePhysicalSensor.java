/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Sensor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link module.ModulePhysicalSensor#isShared <em>Shared</em>}</li>
 * </ul>
 * </p>
 *
 * @see module.ModulePackage#getModulePhysicalSensor()
 * @model
 * @generated
 */
public interface ModulePhysicalSensor extends ModulePhysical {
	/**
	 * Returns the value of the '<em><b>Shared</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Shared</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Shared</em>' attribute.
	 * @see #setShared(boolean)
	 * @see module.ModulePackage#getModulePhysicalSensor_Shared()
	 * @model
	 * @generated
	 */
	boolean isShared();

	/**
	 * Sets the value of the '{@link module.ModulePhysicalSensor#isShared <em>Shared</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Shared</em>' attribute.
	 * @see #isShared()
	 * @generated
	 */
	void setShared(boolean value);

} // ModulePhysicalSensor
