/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see module.ModuleFactory
 * @model kind="package"
 * @generated
 */
public interface ModulePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "module";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://module";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "module";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ModulePackage eINSTANCE = module.impl.ModulePackageImpl.init();

	/**
	 * The meta object id for the '{@link module.impl.ModuleGenericImpl <em>Generic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see module.impl.ModuleGenericImpl
	 * @see module.impl.ModulePackageImpl#getModuleGeneric()
	 * @generated
	 */
	int MODULE_GENERIC = 0;

	/**
	 * The feature id for the '<em><b>Author</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__AUTHOR = 0;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__COMMENT = 1;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__VERSION = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__NAME = 3;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__STATUS = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__ID = 5;

	/**
	 * The feature id for the '<em><b>Source File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__SOURCE_FILE_NAME = 6;

	/**
	 * The feature id for the '<em><b>Port Params</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__PORT_PARAMS = 7;

	/**
	 * The feature id for the '<em><b>Event Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__EVENT_CONDITIONS = 8;

	/**
	 * The feature id for the '<em><b>Event T1s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__EVENT_T1S = 9;

	/**
	 * The feature id for the '<em><b>Event T2s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__EVENT_T2S = 10;

	/**
	 * The feature id for the '<em><b>Event T3s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC__EVENT_T3S = 11;

	/**
	 * The number of structural features of the '<em>Generic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_GENERIC_FEATURE_COUNT = 12;

	/**
	 * The meta object id for the '{@link module.impl.ModuleAlgorithmicImpl <em>Algorithmic</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see module.impl.ModuleAlgorithmicImpl
	 * @see module.impl.ModulePackageImpl#getModuleAlgorithmic()
	 * @generated
	 */
	int MODULE_ALGORITHMIC = 1;

	/**
	 * The feature id for the '<em><b>Author</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__AUTHOR = MODULE_GENERIC__AUTHOR;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__COMMENT = MODULE_GENERIC__COMMENT;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__VERSION = MODULE_GENERIC__VERSION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__NAME = MODULE_GENERIC__NAME;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__STATUS = MODULE_GENERIC__STATUS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__ID = MODULE_GENERIC__ID;

	/**
	 * The feature id for the '<em><b>Source File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__SOURCE_FILE_NAME = MODULE_GENERIC__SOURCE_FILE_NAME;

	/**
	 * The feature id for the '<em><b>Port Params</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__PORT_PARAMS = MODULE_GENERIC__PORT_PARAMS;

	
	
	
	/**
	 * The feature id for the '<em><b>Event Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__EVENT_CONDITIONS = MODULE_GENERIC__EVENT_CONDITIONS;

	/**
	 * The feature id for the '<em><b>Event T1s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__EVENT_T1S = MODULE_GENERIC__EVENT_T1S;

	/**
	 * The feature id for the '<em><b>Event T2s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__EVENT_T2S = MODULE_GENERIC__EVENT_T2S;

	/**
	 * The feature id for the '<em><b>Event T3s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__EVENT_T3S = MODULE_GENERIC__EVENT_T3S;

	/**
	 * The feature id for the '<em><b>Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__FILTER = MODULE_GENERIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Port Datas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__PORT_DATAS = MODULE_GENERIC_FEATURE_COUNT + 1;

	
	// added by boudinf
	int MODULE_ALGORITHMIC__PORT_DATAS_INPUT = MODULE_GENERIC_FEATURE_COUNT + 6;
	//boudinf
	
	/**
	 * The feature id for the '<em><b>Temporal Constraints</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS = MODULE_GENERIC_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Dependencies</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__DEPENDENCIES = MODULE_GENERIC_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC__LEVEL = MODULE_GENERIC_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Algorithmic</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_ALGORITHMIC_FEATURE_COUNT = MODULE_GENERIC_FEATURE_COUNT + 5;

	/**
	 * The meta object id for the '{@link module.impl.ModulePhysicalImpl <em>Physical</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see module.impl.ModulePhysicalImpl
	 * @see module.impl.ModulePackageImpl#getModulePhysical()
	 * @generated
	 */
	int MODULE_PHYSICAL = 2;

	/**
	 * The feature id for the '<em><b>Author</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__AUTHOR = MODULE_GENERIC__AUTHOR;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__COMMENT = MODULE_GENERIC__COMMENT;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__VERSION = MODULE_GENERIC__VERSION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__NAME = MODULE_GENERIC__NAME;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__STATUS = MODULE_GENERIC__STATUS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__ID = MODULE_GENERIC__ID;

	/**
	 * The feature id for the '<em><b>Source File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__SOURCE_FILE_NAME = MODULE_GENERIC__SOURCE_FILE_NAME;

	/**
	 * The feature id for the '<em><b>Port Params</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__PORT_PARAMS = MODULE_GENERIC__PORT_PARAMS;

	/**
	 * The feature id for the '<em><b>Event Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__EVENT_CONDITIONS = MODULE_GENERIC__EVENT_CONDITIONS;

	/**
	 * The feature id for the '<em><b>Event T1s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__EVENT_T1S = MODULE_GENERIC__EVENT_T1S;

	/**
	 * The feature id for the '<em><b>Event T2s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__EVENT_T2S = MODULE_GENERIC__EVENT_T2S;

	/**
	 * The feature id for the '<em><b>Event T3s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__EVENT_T3S = MODULE_GENERIC__EVENT_T3S;

	/**
	 * The feature id for the '<em><b>Port Drivers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL__PORT_DRIVERS = MODULE_GENERIC_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Physical</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_FEATURE_COUNT = MODULE_GENERIC_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link module.impl.ModulePhysicalRobotImpl <em>Physical Robot</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see module.impl.ModulePhysicalRobotImpl
	 * @see module.impl.ModulePackageImpl#getModulePhysicalRobot()
	 * @generated
	 */
	int MODULE_PHYSICAL_ROBOT = 3;

	/**
	 * The feature id for the '<em><b>Author</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__AUTHOR = MODULE_PHYSICAL__AUTHOR;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__COMMENT = MODULE_PHYSICAL__COMMENT;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__VERSION = MODULE_PHYSICAL__VERSION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__NAME = MODULE_PHYSICAL__NAME;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__STATUS = MODULE_PHYSICAL__STATUS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__ID = MODULE_PHYSICAL__ID;

	/**
	 * The feature id for the '<em><b>Source File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__SOURCE_FILE_NAME = MODULE_PHYSICAL__SOURCE_FILE_NAME;

	/**
	 * The feature id for the '<em><b>Port Params</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__PORT_PARAMS = MODULE_PHYSICAL__PORT_PARAMS;

	/**
	 * The feature id for the '<em><b>Event Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__EVENT_CONDITIONS = MODULE_PHYSICAL__EVENT_CONDITIONS;

	/**
	 * The feature id for the '<em><b>Event T1s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__EVENT_T1S = MODULE_PHYSICAL__EVENT_T1S;

	/**
	 * The feature id for the '<em><b>Event T2s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__EVENT_T2S = MODULE_PHYSICAL__EVENT_T2S;

	/**
	 * The feature id for the '<em><b>Event T3s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__EVENT_T3S = MODULE_PHYSICAL__EVENT_T3S;

	/**
	 * The feature id for the '<em><b>Port Drivers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT__PORT_DRIVERS = MODULE_PHYSICAL__PORT_DRIVERS;

	/**
	 * The number of structural features of the '<em>Physical Robot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_ROBOT_FEATURE_COUNT = MODULE_PHYSICAL_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link module.impl.ModulePhysicalSensorImpl <em>Physical Sensor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see module.impl.ModulePhysicalSensorImpl
	 * @see module.impl.ModulePackageImpl#getModulePhysicalSensor()
	 * @generated
	 */
	int MODULE_PHYSICAL_SENSOR = 4;

	/**
	 * The feature id for the '<em><b>Author</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__AUTHOR = MODULE_PHYSICAL__AUTHOR;

	/**
	 * The feature id for the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__COMMENT = MODULE_PHYSICAL__COMMENT;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__VERSION = MODULE_PHYSICAL__VERSION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__NAME = MODULE_PHYSICAL__NAME;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__STATUS = MODULE_PHYSICAL__STATUS;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__ID = MODULE_PHYSICAL__ID;

	/**
	 * The feature id for the '<em><b>Source File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__SOURCE_FILE_NAME = MODULE_PHYSICAL__SOURCE_FILE_NAME;

	/**
	 * The feature id for the '<em><b>Port Params</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__PORT_PARAMS = MODULE_PHYSICAL__PORT_PARAMS;

	/**
	 * The feature id for the '<em><b>Event Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__EVENT_CONDITIONS = MODULE_PHYSICAL__EVENT_CONDITIONS;

	/**
	 * The feature id for the '<em><b>Event T1s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__EVENT_T1S = MODULE_PHYSICAL__EVENT_T1S;

	/**
	 * The feature id for the '<em><b>Event T2s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__EVENT_T2S = MODULE_PHYSICAL__EVENT_T2S;

	/**
	 * The feature id for the '<em><b>Event T3s</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__EVENT_T3S = MODULE_PHYSICAL__EVENT_T3S;

	/**
	 * The feature id for the '<em><b>Port Drivers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__PORT_DRIVERS = MODULE_PHYSICAL__PORT_DRIVERS;

	/**
	 * The feature id for the '<em><b>Shared</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR__SHARED = MODULE_PHYSICAL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Physical Sensor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODULE_PHYSICAL_SENSOR_FEATURE_COUNT = MODULE_PHYSICAL_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link module.StatusType <em>Status Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see module.StatusType
	 * @see module.impl.ModulePackageImpl#getStatusType()
	 * @generated
	 */
	int STATUS_TYPE = 5;




	/**
	 * Returns the meta object for class '{@link module.ModuleGeneric <em>Generic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Generic</em>'.
	 * @see module.ModuleGeneric
	 * @generated
	 */
	EClass getModuleGeneric();

	/**
	 * Returns the meta object for the attribute '{@link module.ModuleGeneric#getAuthor <em>Author</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Author</em>'.
	 * @see module.ModuleGeneric#getAuthor()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EAttribute getModuleGeneric_Author();

	/**
	 * Returns the meta object for the attribute '{@link module.ModuleGeneric#getComment <em>Comment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comment</em>'.
	 * @see module.ModuleGeneric#getComment()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EAttribute getModuleGeneric_Comment();

	/**
	 * Returns the meta object for the attribute '{@link module.ModuleGeneric#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see module.ModuleGeneric#getVersion()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EAttribute getModuleGeneric_Version();

	/**
	 * Returns the meta object for the attribute '{@link module.ModuleGeneric#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see module.ModuleGeneric#getName()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EAttribute getModuleGeneric_Name();

	/**
	 * Returns the meta object for the attribute '{@link module.ModuleGeneric#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see module.ModuleGeneric#getStatus()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EAttribute getModuleGeneric_Status();

	/**
	 * Returns the meta object for the attribute '{@link module.ModuleGeneric#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see module.ModuleGeneric#getId()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EAttribute getModuleGeneric_Id();

	/**
	 * Returns the meta object for the attribute '{@link module.ModuleGeneric#getSourceFileName <em>Source File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source File Name</em>'.
	 * @see module.ModuleGeneric#getSourceFileName()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EAttribute getModuleGeneric_SourceFileName();

	/**
	 * Returns the meta object for the containment reference list '{@link module.ModuleGeneric#getPortParams <em>Port Params</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port Params</em>'.
	 * @see module.ModuleGeneric#getPortParams()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EReference getModuleGeneric_PortParams();

	/**
	 * Returns the meta object for the containment reference list '{@link module.ModuleGeneric#getEventConditions <em>Event Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event Conditions</em>'.
	 * @see module.ModuleGeneric#getEventConditions()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EReference getModuleGeneric_EventConditions();

	/**
	 * Returns the meta object for the containment reference list '{@link module.ModuleGeneric#getEventT1s <em>Event T1s</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event T1s</em>'.
	 * @see module.ModuleGeneric#getEventT1s()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EReference getModuleGeneric_EventT1s();

	/**
	 * Returns the meta object for the containment reference list '{@link module.ModuleGeneric#getEventT2s <em>Event T2s</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event T2s</em>'.
	 * @see module.ModuleGeneric#getEventT2s()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EReference getModuleGeneric_EventT2s();

	/**
	 * Returns the meta object for the containment reference list '{@link module.ModuleGeneric#getEventT3s <em>Event T3s</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event T3s</em>'.
	 * @see module.ModuleGeneric#getEventT3s()
	 * @see #getModuleGeneric()
	 * @generated
	 */
	EReference getModuleGeneric_EventT3s();

	/**
	 * Returns the meta object for class '{@link module.ModuleAlgorithmic <em>Algorithmic</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Algorithmic</em>'.
	 * @see module.ModuleAlgorithmic
	 * @generated
	 */
	EClass getModuleAlgorithmic();

	/**
	 * Returns the meta object for the attribute '{@link module.ModuleAlgorithmic#getFilter <em>Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Filter</em>'.
	 * @see module.ModuleAlgorithmic#getFilter()
	 * @see #getModuleAlgorithmic()
	 * @generated
	 */
	EAttribute getModuleAlgorithmic_Filter();

	/**
	 * Returns the meta object for the containment reference list '{@link module.ModuleAlgorithmic#getPortDatas <em>Port Datas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port Datas</em>'.
	 * @see module.ModuleAlgorithmic#getPortDatas()
	 * @see #getModuleAlgorithmic()
	 * @generated
	 */
	EReference getModuleAlgorithmic_PortDatas();

	
	/**
	 * Returns the meta object for the containment reference list '{@link module.ModuleAlgorithmic#getPortDatas <em>Port Datas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port Datas</em>'.
	 * @see module.ModuleAlgorithmic#getPortDatas()
	 * @see #getModuleAlgorithmic()
	 * @generated NOT
	 */
	EReference getModuleAlgorithmic_PortDatas_INPUT();

	
	/**
	 * Returns the meta object for the reference '{@link module.ModuleAlgorithmic#getTemporalConstraints <em>Temporal Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Temporal Constraints</em>'.
	 * @see module.ModuleAlgorithmic#getTemporalConstraints()
	 * @see #getModuleAlgorithmic()
	 * @generated
	 */
	EReference getModuleAlgorithmic_TemporalConstraints();

	/**
	 * Returns the meta object for the reference list '{@link module.ModuleAlgorithmic#getDependencies <em>Dependencies</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Dependencies</em>'.
	 * @see module.ModuleAlgorithmic#getDependencies()
	 * @see #getModuleAlgorithmic()
	 * @generated
	 */
	EReference getModuleAlgorithmic_Dependencies();

	/**
	 * Returns the meta object for the attribute '{@link module.ModuleAlgorithmic#getLevel <em>Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Level</em>'.
	 * @see module.ModuleAlgorithmic#getLevel()
	 * @see #getModuleAlgorithmic()
	 * @generated
	 */
	EAttribute getModuleAlgorithmic_Level();

	/**
	 * Returns the meta object for class '{@link module.ModulePhysical <em>Physical</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical</em>'.
	 * @see module.ModulePhysical
	 * @generated
	 */
	EClass getModulePhysical();

	/**
	 * Returns the meta object for the containment reference list '{@link module.ModulePhysical#getPortDrivers <em>Port Drivers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Port Drivers</em>'.
	 * @see module.ModulePhysical#getPortDrivers()
	 * @see #getModulePhysical()
	 * @generated
	 */
	EReference getModulePhysical_PortDrivers();

	/**
	 * Returns the meta object for class '{@link module.ModulePhysicalRobot <em>Physical Robot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Robot</em>'.
	 * @see module.ModulePhysicalRobot
	 * @generated
	 */
	EClass getModulePhysicalRobot();

	/**
	 * Returns the meta object for class '{@link module.ModulePhysicalSensor <em>Physical Sensor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Physical Sensor</em>'.
	 * @see module.ModulePhysicalSensor
	 * @generated
	 */
	EClass getModulePhysicalSensor();

	/**
	 * Returns the meta object for the attribute '{@link module.ModulePhysicalSensor#isShared <em>Shared</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Shared</em>'.
	 * @see module.ModulePhysicalSensor#isShared()
	 * @see #getModulePhysicalSensor()
	 * @generated
	 */
	EAttribute getModulePhysicalSensor_Shared();

	/**
	 * Returns the meta object for enum '{@link module.StatusType <em>Status Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status Type</em>'.
	 * @see module.StatusType
	 * @generated
	 */
	EEnum getStatusType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ModuleFactory getModuleFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link module.impl.ModuleGenericImpl <em>Generic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see module.impl.ModuleGenericImpl
		 * @see module.impl.ModulePackageImpl#getModuleGeneric()
		 * @generated
		 */
		EClass MODULE_GENERIC = eINSTANCE.getModuleGeneric();

		/**
		 * The meta object literal for the '<em><b>Author</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE_GENERIC__AUTHOR = eINSTANCE.getModuleGeneric_Author();

		/**
		 * The meta object literal for the '<em><b>Comment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE_GENERIC__COMMENT = eINSTANCE.getModuleGeneric_Comment();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE_GENERIC__VERSION = eINSTANCE.getModuleGeneric_Version();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE_GENERIC__NAME = eINSTANCE.getModuleGeneric_Name();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE_GENERIC__STATUS = eINSTANCE.getModuleGeneric_Status();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE_GENERIC__ID = eINSTANCE.getModuleGeneric_Id();

		/**
		 * The meta object literal for the '<em><b>Source File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE_GENERIC__SOURCE_FILE_NAME = eINSTANCE.getModuleGeneric_SourceFileName();

		/**
		 * The meta object literal for the '<em><b>Port Params</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE_GENERIC__PORT_PARAMS = eINSTANCE.getModuleGeneric_PortParams();

		/**
		 * The meta object literal for the '<em><b>Event Conditions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE_GENERIC__EVENT_CONDITIONS = eINSTANCE.getModuleGeneric_EventConditions();

		/**
		 * The meta object literal for the '<em><b>Event T1s</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE_GENERIC__EVENT_T1S = eINSTANCE.getModuleGeneric_EventT1s();

		/**
		 * The meta object literal for the '<em><b>Event T2s</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE_GENERIC__EVENT_T2S = eINSTANCE.getModuleGeneric_EventT2s();

		/**
		 * The meta object literal for the '<em><b>Event T3s</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE_GENERIC__EVENT_T3S = eINSTANCE.getModuleGeneric_EventT3s();

		/**
		 * The meta object literal for the '{@link module.impl.ModuleAlgorithmicImpl <em>Algorithmic</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see module.impl.ModuleAlgorithmicImpl
		 * @see module.impl.ModulePackageImpl#getModuleAlgorithmic()
		 * @generated
		 */
		EClass MODULE_ALGORITHMIC = eINSTANCE.getModuleAlgorithmic();

		/**
		 * The meta object literal for the '<em><b>Filter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE_ALGORITHMIC__FILTER = eINSTANCE.getModuleAlgorithmic_Filter();

		/**
		 * The meta object literal for the '<em><b>Port Datas</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE_ALGORITHMIC__PORT_DATAS = eINSTANCE.getModuleAlgorithmic_PortDatas();

		/**
		 * The meta object literal for the '<em><b>Temporal Constraints</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE_ALGORITHMIC__PORT_DATAS_INPUT = eINSTANCE.getModuleAlgorithmic_PortDatas_INPUT();

		/**
		 * The meta object literal for the '<em><b>Temporal Constraints</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE_ALGORITHMIC__TEMPORAL_CONSTRAINTS = eINSTANCE.getModuleAlgorithmic_TemporalConstraints();

		/**
		 * The meta object literal for the '<em><b>Dependencies</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE_ALGORITHMIC__DEPENDENCIES = eINSTANCE.getModuleAlgorithmic_Dependencies();

		/**
		 * The meta object literal for the '<em><b>Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE_ALGORITHMIC__LEVEL = eINSTANCE.getModuleAlgorithmic_Level();

		/**
		 * The meta object literal for the '{@link module.impl.ModulePhysicalImpl <em>Physical</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see module.impl.ModulePhysicalImpl
		 * @see module.impl.ModulePackageImpl#getModulePhysical()
		 * @generated
		 */
		EClass MODULE_PHYSICAL = eINSTANCE.getModulePhysical();

		/**
		 * The meta object literal for the '<em><b>Port Drivers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MODULE_PHYSICAL__PORT_DRIVERS = eINSTANCE.getModulePhysical_PortDrivers();

		/**
		 * The meta object literal for the '{@link module.impl.ModulePhysicalRobotImpl <em>Physical Robot</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see module.impl.ModulePhysicalRobotImpl
		 * @see module.impl.ModulePackageImpl#getModulePhysicalRobot()
		 * @generated
		 */
		EClass MODULE_PHYSICAL_ROBOT = eINSTANCE.getModulePhysicalRobot();

		/**
		 * The meta object literal for the '{@link module.impl.ModulePhysicalSensorImpl <em>Physical Sensor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see module.impl.ModulePhysicalSensorImpl
		 * @see module.impl.ModulePackageImpl#getModulePhysicalSensor()
		 * @generated
		 */
		EClass MODULE_PHYSICAL_SENSOR = eINSTANCE.getModulePhysicalSensor();

		/**
		 * The meta object literal for the '<em><b>Shared</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODULE_PHYSICAL_SENSOR__SHARED = eINSTANCE.getModulePhysicalSensor_Shared();

		/**
		 * The meta object literal for the '{@link module.StatusType <em>Status Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see module.StatusType
		 * @see module.impl.ModulePackageImpl#getStatusType()
		 * @generated
		 */
		EEnum STATUS_TYPE = eINSTANCE.getStatusType();

	}

} //ModulePackage
