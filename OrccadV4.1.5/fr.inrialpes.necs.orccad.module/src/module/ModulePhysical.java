/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module;

import org.eclipse.emf.common.util.EList;

import port.PortDriver;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link module.ModulePhysical#getPortDrivers <em>Port Drivers</em>}</li>
 * </ul>
 * </p>
 *
 * @see module.ModulePackage#getModulePhysical()
 * @model
 * @generated
 */
public interface ModulePhysical extends ModuleGeneric {
	/**
	 * Returns the value of the '<em><b>Port Drivers</b></em>' containment reference list.
	 * The list contents are of type {@link port.PortDriver}.
	 * It is bidirectional and its opposite is '{@link port.PortDriver#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Drivers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Drivers</em>' containment reference list.
	 * @see module.ModulePackage#getModulePhysical_PortDrivers()
	 * @see port.PortDriver#getContainer
	 * @model opposite="container" containment="true"
	 * @generated
	 */
	EList<PortDriver> getPortDrivers();

} // ModulePhysical
