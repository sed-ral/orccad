/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Physical Robot</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see module.ModulePackage#getModulePhysicalRobot()
 * @model
 * @generated
 */
public interface ModulePhysicalRobot extends ModulePhysical {
} // ModulePhysicalRobot
