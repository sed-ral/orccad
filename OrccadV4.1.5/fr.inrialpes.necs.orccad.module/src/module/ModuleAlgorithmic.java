/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module;

import constraint.TemporalConstraint;

import org.eclipse.emf.common.util.EList;

import port.PortData;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Algorithmic</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link module.ModuleAlgorithmic#getFilter <em>Filter</em>}</li>
 *   <li>{@link module.ModuleAlgorithmic#getPortDatas <em>Port Datas</em>}</li>
 *   <li>{@link module.ModuleAlgorithmic#getTemporalConstraints <em>Temporal Constraints</em>}</li>
 *   <li>{@link module.ModuleAlgorithmic#getDependencies <em>Dependencies</em>}</li>
 *   <li>{@link module.ModuleAlgorithmic#getLevel <em>Level</em>}</li>
 * </ul>
 * </p>
 *
 * @see module.ModulePackage#getModuleAlgorithmic()
 * @model
 * @generated
 */
public interface ModuleAlgorithmic extends ModuleGeneric {
	/**
	 * Returns the value of the '<em><b>Filter</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Filter</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Filter</em>' attribute.
	 * @see #setFilter(int)
	 * @see module.ModulePackage#getModuleAlgorithmic_Filter()
	 * @model default="0"
	 * @generated
	 */
	int getFilter();

	/**
	 * Sets the value of the '{@link module.ModuleAlgorithmic#getFilter <em>Filter</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Filter</em>' attribute.
	 * @see #getFilter()
	 * @generated
	 */
	void setFilter(int value);

	/**
	 * Returns the value of the '<em><b>Port Datas</b></em>' containment reference list.
	 * The list contents are of type {@link port.PortData}.
	 * It is bidirectional and its opposite is '{@link port.PortData#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Datas</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Datas</em>' containment reference list.
	 * @see module.ModulePackage#getModuleAlgorithmic_PortDatas()
	 * @see port.PortData#getContainer
	 * @model opposite="container" containment="true"
	 * @generated
	 */
	EList<PortData> getPortDatas();


	/**
	 * Returns the value of the '<em><b>Port Datas</b></em>' containment reference list.
	 * The list contents are of type {@link port.PortData}.
	 * It is bidirectional and its opposite is '{@link port.PortData#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Datas</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Datas</em>' containment reference list.
	 * @see module.ModulePackage#getModuleAlgorithmic_PortDatas()
	 * @see port.PortData#getContainer
	 * @model opposite="container" containment="true"
	 * @generated NOT
	 */
	EList<PortData> getPortDatasINPUT();

	/**
	 * Returns the value of the '<em><b>Temporal Constraints</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link constraint.TemporalConstraint#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Temporal Constraints</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Temporal Constraints</em>' reference.
	 * @see #setTemporalConstraints(TemporalConstraint)
	 * @see module.ModulePackage#getModuleAlgorithmic_TemporalConstraints()
	 * @see constraint.TemporalConstraint#getContainer
	 * @model opposite="container" required="true"
	 * @generated
	 */
	TemporalConstraint getTemporalConstraints();

	/**
	 * Sets the value of the '{@link module.ModuleAlgorithmic#getTemporalConstraints <em>Temporal Constraints</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Temporal Constraints</em>' reference.
	 * @see #getTemporalConstraints()
	 * @generated
	 */
	void setTemporalConstraints(TemporalConstraint value);

	/**
	 * Returns the value of the '<em><b>Dependencies</b></em>' reference list.
	 * The list contents are of type {@link module.ModuleGeneric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dependencies</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dependencies</em>' reference list.
	 * @see module.ModulePackage#getModuleAlgorithmic_Dependencies()
	 * @model
	 * @generated
	 */
	EList<ModuleGeneric> getDependencies();

	/**
	 * Returns the value of the '<em><b>Level</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Level</em>' attribute.
	 * @see #setLevel(int)
	 * @see module.ModulePackage#getModuleAlgorithmic_Level()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getLevel();

	/**
	 * Sets the value of the '{@link module.ModuleAlgorithmic#getLevel <em>Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Level</em>' attribute.
	 * @see #getLevel()
	 * @generated
	 */
	void setLevel(int value);

} // ModuleAlgorithmic
