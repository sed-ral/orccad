/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import port.EventCondition;
import port.ExceptionT1;
import port.ExceptionT2;
import port.ExceptionT3;
import port.PortParam;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Generic</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link module.ModuleGeneric#getAuthor <em>Author</em>}</li>
 *   <li>{@link module.ModuleGeneric#getComment <em>Comment</em>}</li>
 *   <li>{@link module.ModuleGeneric#getVersion <em>Version</em>}</li>
 *   <li>{@link module.ModuleGeneric#getName <em>Name</em>}</li>
 *   <li>{@link module.ModuleGeneric#getStatus <em>Status</em>}</li>
 *   <li>{@link module.ModuleGeneric#getId <em>Id</em>}</li>
 *   <li>{@link module.ModuleGeneric#getSourceFileName <em>Source File Name</em>}</li>
 *   <li>{@link module.ModuleGeneric#getPortParams <em>Port Params</em>}</li>
 *   <li>{@link module.ModuleGeneric#getEventConditions <em>Event Conditions</em>}</li>
 *   <li>{@link module.ModuleGeneric#getEventT1s <em>Event T1s</em>}</li>
 *   <li>{@link module.ModuleGeneric#getEventT2s <em>Event T2s</em>}</li>
 *   <li>{@link module.ModuleGeneric#getEventT3s <em>Event T3s</em>}</li>
 * </ul>
 * </p>
 *
 * @see module.ModulePackage#getModuleGeneric()
 * @model
 * @generated
 */
public interface ModuleGeneric extends EObject {
	/**
	 * Returns the value of the '<em><b>Author</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Author</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Author</em>' attribute.
	 * @see #setAuthor(String)
	 * @see module.ModulePackage#getModuleGeneric_Author()
	 * @model
	 * @generated
	 */
	String getAuthor();

	/**
	 * Sets the value of the '{@link module.ModuleGeneric#getAuthor <em>Author</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Author</em>' attribute.
	 * @see #getAuthor()
	 * @generated
	 */
	void setAuthor(String value);

	/**
	 * Returns the value of the '<em><b>Comment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comment</em>' attribute.
	 * @see #setComment(String)
	 * @see module.ModulePackage#getModuleGeneric_Comment()
	 * @model
	 * @generated
	 */
	String getComment();

	/**
	 * Sets the value of the '{@link module.ModuleGeneric#getComment <em>Comment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comment</em>' attribute.
	 * @see #getComment()
	 * @generated
	 */
	void setComment(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see module.ModulePackage#getModuleGeneric_Version()
	 * @model
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link module.ModuleGeneric#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see module.ModulePackage#getModuleGeneric_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link module.ModuleGeneric#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link module.StatusType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see module.StatusType
	 * @see #setStatus(StatusType)
	 * @see module.ModulePackage#getModuleGeneric_Status()
	 * @model
	 * @generated
	 */
	StatusType getStatus();

	/**
	 * Sets the value of the '{@link module.ModuleGeneric#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see module.StatusType
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(StatusType value);

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see module.ModulePackage#getModuleGeneric_Id()
	 * @model id="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link module.ModuleGeneric#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Source File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source File Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source File Name</em>' attribute.
	 * @see #setSourceFileName(String)
	 * @see module.ModulePackage#getModuleGeneric_SourceFileName()
	 * @model
	 * @generated
	 */
	String getSourceFileName();

	/**
	 * Sets the value of the '{@link module.ModuleGeneric#getSourceFileName <em>Source File Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source File Name</em>' attribute.
	 * @see #getSourceFileName()
	 * @generated
	 */
	void setSourceFileName(String value);

	/**
	 * Returns the value of the '<em><b>Port Params</b></em>' containment reference list.
	 * The list contents are of type {@link port.PortParam}.
	 * It is bidirectional and its opposite is '{@link port.PortParam#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Params</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Params</em>' containment reference list.
	 * @see module.ModulePackage#getModuleGeneric_PortParams()
	 * @see port.PortParam#getContainer
	 * @model opposite="container" containment="true"
	 * @generated
	 */
	EList<PortParam> getPortParams();

	/**
	 * Returns the value of the '<em><b>Event Conditions</b></em>' containment reference list.
	 * The list contents are of type {@link port.EventCondition}.
	 * It is bidirectional and its opposite is '{@link port.EventCondition#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Conditions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Conditions</em>' containment reference list.
	 * @see module.ModulePackage#getModuleGeneric_EventConditions()
	 * @see port.EventCondition#getContainer
	 * @model opposite="container" containment="true"
	 * @generated
	 */
	EList<EventCondition> getEventConditions();

	/**
	 * Returns the value of the '<em><b>Event T1s</b></em>' containment reference list.
	 * The list contents are of type {@link port.ExceptionT1}.
	 * It is bidirectional and its opposite is '{@link port.ExceptionT1#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event T1s</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event T1s</em>' containment reference list.
	 * @see module.ModulePackage#getModuleGeneric_EventT1s()
	 * @see port.ExceptionT1#getContainer
	 * @model opposite="container" containment="true"
	 * @generated
	 */
	EList<ExceptionT1> getEventT1s();

	/**
	 * Returns the value of the '<em><b>Event T2s</b></em>' containment reference list.
	 * The list contents are of type {@link port.ExceptionT2}.
	 * It is bidirectional and its opposite is '{@link port.ExceptionT2#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event T2s</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event T2s</em>' containment reference list.
	 * @see module.ModulePackage#getModuleGeneric_EventT2s()
	 * @see port.ExceptionT2#getContainer
	 * @model opposite="container" containment="true"
	 * @generated
	 */
	EList<ExceptionT2> getEventT2s();

	/**
	 * Returns the value of the '<em><b>Event T3s</b></em>' containment reference list.
	 * The list contents are of type {@link port.ExceptionT3}.
	 * It is bidirectional and its opposite is '{@link port.ExceptionT3#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event T3s</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event T3s</em>' containment reference list.
	 * @see module.ModulePackage#getModuleGeneric_EventT3s()
	 * @see port.ExceptionT3#getContainer
	 * @model opposite="container" containment="true"
	 * @generated
	 */
	EList<ExceptionT3> getEventT3s();

} // ModuleGeneric
