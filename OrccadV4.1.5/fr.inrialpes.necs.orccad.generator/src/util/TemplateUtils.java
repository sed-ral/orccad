package util;

//import java.util.ArrayList;
//import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import module.*;

import java.util.*;

import constraint.TemporalConstraint;

import port.PortData;
import task.RobotTask;
import procedure.Entity;
import procedure.LinkEntities;

public class TemplateUtils {

	public static List<Entity> sortLinkSequence(List entSeqList) {
		int seqListSize = entSeqList.size();
		if (seqListSize < 2)
			return null;
		// Get First Sequence entity
		Iterator<Entity> itEntSeqList = ((List<Entity>) entSeqList).iterator();
		Entity entSeqFirst = ((Entity[]) (entSeqList.toArray()))[0];
		while (itEntSeqList.hasNext()) {
			entSeqFirst = itEntSeqList.next();
			if ((((LinkEntities) entSeqFirst.getTargetLinks()) == null)
					&& (((LinkEntities) entSeqFirst.getSourceLinks()) != null)) {
				break;
			}
		}
		List<Entity> sortSeqList = new ArrayList<Entity>(seqListSize);
		sortSeqList.add(entSeqFirst);
		Entity entity = entSeqFirst;
		while (entity.getSourceLinks() != null) {
			Entity nextEnt = entity.getSourceLinks().getTarget();
			sortSeqList.add(nextEnt);
			entity = nextEnt;
		}
		/* System.out.println("Mod = " + mod.getName() + " detected in list "); */
		return sortSeqList;
	}

	public static int randomValue() {
		Random randomGenerator = new Random();
		return randomGenerator.nextInt(1000);
	}

	public static String timestamp() {
		// return String.valueOf( System.currentTimeMillis() );
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date).toString();

	}

	public static int getPeriodTimeNSVal(TemporalConstraint TC) {
		int timeUnit = 1;
		switch (TC.getPeriodTimeUnits()) {
			case MS: timeUnit = 1000000;
				break;
			case NS: timeUnit = 1;
				break;
			case S: timeUnit = 1000000000;
				break;
			case US: timeUnit = 1000;
				break;
		}
		return (int)(TC.getPeriodTime()*timeUnit);
	}
	
	public static int getBaseClock(Set<Object> periodTimeList) {
		Object firstVal = periodTimeList.toArray()[0];
		int currentGCD = (Integer)firstVal;
		int periodTimeNext = 0;
		Iterator<Object> itPeriodTimeList = (Iterator<Object>) periodTimeList.iterator();
		while (itPeriodTimeList.hasNext()) {
			periodTimeNext = (Integer)(itPeriodTimeList.next());
			currentGCD = computeGCD(currentGCD, periodTimeNext);
			if (currentGCD == 1)
	            break;
	    }
		
		return currentGCD;
	}

	public static int computeGCD(int a, int b) {
		int r;
		
		while (b > 0) {
			r = a % b; 
			a = b; 
			b = r; 
		} 	
		return a;
	}
	
	public static int setModuleLevel2() {
		return 10;
	}

	// for (int i = 0; i < modDependencyList.size(); i++) {

	public static int setModuleDependency(RobotTask RT) {
		System.out.println("\t\tsetModuleDependency is called");

		if (!RT.getLinkDatas().isEmpty()) {
			for (int i = 0; i < RT.getLinkDatas().size(); i++) {
				// Module Dependency List construction
				PortData port = RT.getLinkDatas().get(i).getTarget();
				ModuleAlgorithmic modA = (ModuleAlgorithmic) port.eContainer();
				System.out.println("ModA NAme = " + modA.getName() + " Port "
						+ port.getPortName());

				modA.getDependencies().add(
						(ModuleAlgorithmic) RT.getLinkDatas().get(i)
								.getSource().eContainer());
			}
		}
		if (!RT.getLinkDrivers().isEmpty()) {
			/*
			 * for (int i = 0; i < RT.getRThasDriverLinksDriverToData().size();
			 * i++) { // Module Dependency List construction PortData port =
			 * RT.getRThasDriverLinksDriverToData().get(i).getTarget();
			 * ModuleAlgorithmic modA = (ModuleAlgorithmic)port.eContainer();
			 * System.out.println("ModA NAme = " + modA.getName() + " Port" +
			 * port.getVarName());
			 * modA.getDependencies().add((ModulePhysical)RT.
			 * getRThasDriverLinksDriverToData
			 * ().get(i).getSource().eContainer()); }
			 */
		}

		for (int i = 0; i < RT.getModuleAlgorithmics().size(); i++) {
			System.out.println("ModA name = "
					+ RT.getModuleAlgorithmics().get(i).getName()
					+ " Dependency.size = "
					+ RT.getModuleAlgorithmics().get(i).getDependencies()
							.size());
			for (int j = 0; j < RT.getModuleAlgorithmics().get(i)
					.getDependencies().size(); j++) {
				System.out.println("ModDepend name = "
						+ RT.getModuleAlgorithmics().get(i).getDependencies()
								.get(j).getName());
			}
		}
		return 0;
	}

	public static int setModuleLevel(List modDependencyList) {
		Boolean[] isModLevelModified = new Boolean[modDependencyList.size()];
		int isEnd = modDependencyList.size();
		int curLevel = 0;

		// Init hash Table
		for (int i = 0; i < modDependencyList.size(); i++) {
			isModLevelModified[i] = true;
		}
		while (isEnd > 0) {
			Iterator<Object> itObjList = (Iterator<Object>) modDependencyList
					.iterator();
			// Get a List composed by {ModAlgo, Dependency List}
			int cnt = 0;
			while (itObjList.hasNext()) {
				List<Object> modDepList = (List<Object>) itObjList.next();
				Iterator<Object> itObj = modDepList.iterator();
				// Retrieve ModAlgo
				ModuleAlgorithmic curModA = (ModuleAlgorithmic) itObj.next();
				System.out.println("CurModA = " + curModA.getName());

				// Retrieve Dependency List for ModAlgo
				List<ModuleGeneric> listDepMod = (List<ModuleGeneric>) itObj
						.next();
				if (isModLevelModified[cnt]) {
					// Retrieve module levels from dependency list
					List<Integer> listI = new ArrayList<Integer>();
					Iterator<ModuleGeneric> itDepList = listDepMod.iterator();
					while (itDepList.hasNext()) {
						ModuleGeneric mod = (ModuleGeneric) itDepList.next();
						if (mod instanceof ModuleAlgorithmic) {

							ModuleAlgorithmic modA = (ModuleAlgorithmic) mod;
							System.out.println("ModA  = " + mod.getName()
									+ " detected in list ");

							listI.add(modA.getLevel());
						} else {
							System.out.println("Mod = " + mod.getName()
									+ " detected in list ");
						}
					}
					// Compute dependency level
					curLevel = 1;
					if (listI.size() != 0)
						curLevel += Collections.max(listI);
					// Update level
					if (curLevel != curModA.getLevel()) {
						curModA.setLevel(curLevel);
						isModLevelModified[cnt] = true;
					} else {
						isModLevelModified[cnt] = false;
						isEnd -= 1;
					}
				}
				cnt++;
			}
		}
		return 0;
	}
}
