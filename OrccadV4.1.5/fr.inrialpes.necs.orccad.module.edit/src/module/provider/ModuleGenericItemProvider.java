/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package module.provider;


import java.util.Collection;
import java.util.List;

import module.ModuleGeneric;
import module.ModulePackage;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import port.PortFactory;

/**
 * This is the item provider adapter for a {@link module.ModuleGeneric} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ModuleGenericItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModuleGenericItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAuthorPropertyDescriptor(object);
			addCommentPropertyDescriptor(object);
			addVersionPropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addStatusPropertyDescriptor(object);
			addSourceFileNamePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Author feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAuthorPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ModuleGeneric_author_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ModuleGeneric_author_feature", "_UI_ModuleGeneric_type"),
				 ModulePackage.Literals.MODULE_GENERIC__AUTHOR,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Comment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCommentPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ModuleGeneric_comment_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ModuleGeneric_comment_feature", "_UI_ModuleGeneric_type"),
				 ModulePackage.Literals.MODULE_GENERIC__COMMENT,
				 true,
				 true,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Version feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVersionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ModuleGeneric_version_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ModuleGeneric_version_feature", "_UI_ModuleGeneric_type"),
				 ModulePackage.Literals.MODULE_GENERIC__VERSION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ModuleGeneric_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ModuleGeneric_name_feature", "_UI_ModuleGeneric_type"),
				 ModulePackage.Literals.MODULE_GENERIC__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Status feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStatusPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ModuleGeneric_status_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ModuleGeneric_status_feature", "_UI_ModuleGeneric_type"),
				 ModulePackage.Literals.MODULE_GENERIC__STATUS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Source File Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSourceFileNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_ModuleGeneric_sourceFileName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_ModuleGeneric_sourceFileName_feature", "_UI_ModuleGeneric_type"),
				 ModulePackage.Literals.MODULE_GENERIC__SOURCE_FILE_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ModulePackage.Literals.MODULE_GENERIC__PORT_PARAMS);
			childrenFeatures.add(ModulePackage.Literals.MODULE_GENERIC__EVENT_CONDITIONS);
			childrenFeatures.add(ModulePackage.Literals.MODULE_GENERIC__EVENT_T1S);
			childrenFeatures.add(ModulePackage.Literals.MODULE_GENERIC__EVENT_T2S);
			childrenFeatures.add(ModulePackage.Literals.MODULE_GENERIC__EVENT_T3S);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns ModuleGeneric.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/ModuleGeneric"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((ModuleGeneric)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_ModuleGeneric_type") :
			getString("_UI_ModuleGeneric_type") + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ModuleGeneric.class)) {
			case ModulePackage.MODULE_GENERIC__AUTHOR:
			case ModulePackage.MODULE_GENERIC__COMMENT:
			case ModulePackage.MODULE_GENERIC__VERSION:
			case ModulePackage.MODULE_GENERIC__NAME:
			case ModulePackage.MODULE_GENERIC__STATUS:
			case ModulePackage.MODULE_GENERIC__ID:
			case ModulePackage.MODULE_GENERIC__SOURCE_FILE_NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case ModulePackage.MODULE_GENERIC__PORT_PARAMS:
			case ModulePackage.MODULE_GENERIC__EVENT_CONDITIONS:
			case ModulePackage.MODULE_GENERIC__EVENT_T1S:
			case ModulePackage.MODULE_GENERIC__EVENT_T2S:
			case ModulePackage.MODULE_GENERIC__EVENT_T3S:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(ModulePackage.Literals.MODULE_GENERIC__PORT_PARAMS,
				 PortFactory.eINSTANCE.createPortParam()));

		newChildDescriptors.add
			(createChildParameter
				(ModulePackage.Literals.MODULE_GENERIC__EVENT_CONDITIONS,
				 PortFactory.eINSTANCE.createEventCondition()));

		newChildDescriptors.add
			(createChildParameter
				(ModulePackage.Literals.MODULE_GENERIC__EVENT_T1S,
				 PortFactory.eINSTANCE.createExceptionT1()));

		newChildDescriptors.add
			(createChildParameter
				(ModulePackage.Literals.MODULE_GENERIC__EVENT_T2S,
				 PortFactory.eINSTANCE.createExceptionT2()));

		newChildDescriptors.add
			(createChildParameter
				(ModulePackage.Literals.MODULE_GENERIC__EVENT_T3S,
				 PortFactory.eINSTANCE.createExceptionT3()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ModuleEditPlugin.INSTANCE;
	}

}
