/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package constraint.provider;


import constraint.ConstraintPackage;
import constraint.SynchronisationType;
import constraint.TemporalConstraint;

import java.util.Collection;
import java.util.List;

import module.provider.ModuleEditPlugin;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link constraint.TemporalConstraint} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TemporalConstraintItemProvider
	extends ItemProviderAdapter
	implements
		IEditingDomainItemProvider,
		IStructuredItemContentProvider,
		ITreeItemContentProvider,
		IItemLabelProvider,
		IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TemporalConstraintItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCpuNamePropertyDescriptor(object);
			addExecutionFlagPropertyDescriptor(object);
			addExecutionTimePropertyDescriptor(object);
			addExecutionTimeUnitsPropertyDescriptor(object);
			addPeriodTimePropertyDescriptor(object);
			addPeriodTimeUnitsPropertyDescriptor(object);
			addPriorityPropertyDescriptor(object);
			addSynchronisationTypePropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addContainerPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Cpu Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCpuNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TemporalConstraint_cpuName_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TemporalConstraint_cpuName_feature", "_UI_TemporalConstraint_type"),
				 ConstraintPackage.Literals.TEMPORAL_CONSTRAINT__CPU_NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Execution Flag feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExecutionFlagPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TemporalConstraint_executionFlag_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TemporalConstraint_executionFlag_feature", "_UI_TemporalConstraint_type"),
				 ConstraintPackage.Literals.TEMPORAL_CONSTRAINT__EXECUTION_FLAG,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Execution Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExecutionTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TemporalConstraint_executionTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TemporalConstraint_executionTime_feature", "_UI_TemporalConstraint_type"),
				 ConstraintPackage.Literals.TEMPORAL_CONSTRAINT__EXECUTION_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Execution Time Units feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExecutionTimeUnitsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TemporalConstraint_executionTimeUnits_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TemporalConstraint_executionTimeUnits_feature", "_UI_TemporalConstraint_type"),
				 ConstraintPackage.Literals.TEMPORAL_CONSTRAINT__EXECUTION_TIME_UNITS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Period Time feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPeriodTimePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TemporalConstraint_periodTime_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TemporalConstraint_periodTime_feature", "_UI_TemporalConstraint_type"),
				 ConstraintPackage.Literals.TEMPORAL_CONSTRAINT__PERIOD_TIME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.REAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Period Time Units feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPeriodTimeUnitsPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TemporalConstraint_periodTimeUnits_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TemporalConstraint_periodTimeUnits_feature", "_UI_TemporalConstraint_type"),
				 ConstraintPackage.Literals.TEMPORAL_CONSTRAINT__PERIOD_TIME_UNITS,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Priority feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPriorityPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TemporalConstraint_priority_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TemporalConstraint_priority_feature", "_UI_TemporalConstraint_type"),
				 ConstraintPackage.Literals.TEMPORAL_CONSTRAINT__PRIORITY,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.INTEGRAL_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Synchronisation Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSynchronisationTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TemporalConstraint_synchronisationType_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TemporalConstraint_synchronisationType_feature", "_UI_TemporalConstraint_type"),
				 ConstraintPackage.Literals.TEMPORAL_CONSTRAINT__SYNCHRONISATION_TYPE,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TemporalConstraint_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TemporalConstraint_name_feature", "_UI_TemporalConstraint_type"),
				 ConstraintPackage.Literals.TEMPORAL_CONSTRAINT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Container feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainerPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_TemporalConstraint_container_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_TemporalConstraint_container_feature", "_UI_TemporalConstraint_type"),
				 ConstraintPackage.Literals.TEMPORAL_CONSTRAINT__CONTAINER,
				 false,
				 false,
				 false,
				 null,
				 null,
				 null));
	}

	/**
	 * This returns TemporalConstraint.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		
		if(((TemporalConstraint)object).getSynchronisationType().equals(SynchronisationType.CLK))
			return overlayImage(object, getResourceLocator().getImage("full/obj16/TemporalConstraintCLK"));
		
		if(((TemporalConstraint)object).getSynchronisationType().equals(SynchronisationType.EXTERN))
			return overlayImage(object, getResourceLocator().getImage("full/obj16/TemporalConstraintEXTERN"));
		
		if(((TemporalConstraint)object).getSynchronisationType().equals(SynchronisationType.LINK))
			return overlayImage(object, getResourceLocator().getImage("full/obj16/TemporalConstraintLINK"));
		
		
		if(((TemporalConstraint)object).getSynchronisationType().equals(SynchronisationType.LINK_AND_CLK))
			return overlayImage(object, getResourceLocator().getImage("full/obj16/TemporalConstraintLINK_AND_CLK"));
		
		return overlayImage(object, getResourceLocator().getImage("full/obj16/TemporalConstraint"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		String label = ((TemporalConstraint)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_TemporalConstraint_type") :
			getString("_UI_TemporalConstraint_type") + " " + label + " ( " + ((TemporalConstraint)object).getSynchronisationType().toString() + " , T= " + ((TemporalConstraint)object).getPeriodTime()+ " " +((TemporalConstraint)object).getPeriodTimeUnits().toString() + " )";
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TemporalConstraint.class)) {
			case ConstraintPackage.TEMPORAL_CONSTRAINT__CPU_NAME:
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_FLAG:
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME:
			case ConstraintPackage.TEMPORAL_CONSTRAINT__EXECUTION_TIME_UNITS:
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME:
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PERIOD_TIME_UNITS:
			case ConstraintPackage.TEMPORAL_CONSTRAINT__PRIORITY:
			case ConstraintPackage.TEMPORAL_CONSTRAINT__SYNCHRONISATION_TYPE:
			case ConstraintPackage.TEMPORAL_CONSTRAINT__NAME:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return ModuleEditPlugin.INSTANCE;
	}

}
