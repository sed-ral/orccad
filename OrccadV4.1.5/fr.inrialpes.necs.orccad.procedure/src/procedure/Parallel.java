/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Parallel</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link procedure.Parallel#getParaEntities <em>Para Entities</em>}</li>
 * </ul>
 * </p>
 *
 * @see procedure.ProcedurePackage#getParallel()
 * @model
 * @generated
 */
public interface Parallel extends Entity {
	/**
	 * Returns the value of the '<em><b>Para Entities</b></em>' containment reference list.
	 * The list contents are of type {@link procedure.Entity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Para Entities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Para Entities</em>' containment reference list.
	 * @see procedure.ProcedurePackage#getParallel_ParaEntities()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Entity> getParaEntities();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkParallel(DiagnosticChain c1, Map<Object, Object> c2);

} // Parallel
