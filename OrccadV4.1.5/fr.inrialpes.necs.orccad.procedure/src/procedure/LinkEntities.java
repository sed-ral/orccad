/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Link Entities</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link procedure.LinkEntities#getSource <em>Source</em>}</li>
 *   <li>{@link procedure.LinkEntities#getTarget <em>Target</em>}</li>
 * </ul>
 * </p>
 *
 * @see procedure.ProcedurePackage#getLinkEntities()
 * @model
 * @generated
 */
public interface LinkEntities extends EObject {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link procedure.Entity#getSourceLinks <em>Source Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' reference.
	 * @see #setSource(Entity)
	 * @see procedure.ProcedurePackage#getLinkEntities_Source()
	 * @see procedure.Entity#getSourceLinks
	 * @model opposite="sourceLinks" required="true"
	 * @generated
	 */
	Entity getSource();

	/**
	 * Sets the value of the '{@link procedure.LinkEntities#getSource <em>Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' reference.
	 * @see #getSource()
	 * @generated
	 */
	void setSource(Entity value);

	/**
	 * Returns the value of the '<em><b>Target</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link procedure.Entity#getTargetLinks <em>Target Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target</em>' reference.
	 * @see #setTarget(Entity)
	 * @see procedure.ProcedurePackage#getLinkEntities_Target()
	 * @see procedure.Entity#getTargetLinks
	 * @model opposite="targetLinks" required="true"
	 * @generated
	 */
	Entity getTarget();

	/**
	 * Sets the value of the '{@link procedure.LinkEntities#getTarget <em>Target</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target</em>' reference.
	 * @see #getTarget()
	 * @generated
	 */
	void setTarget(Entity value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkLinkEntities(DiagnosticChain c1, Map<Object, Object> c2);

} // LinkEntities
