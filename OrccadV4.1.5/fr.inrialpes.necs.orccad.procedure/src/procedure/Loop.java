/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Loop</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link procedure.Loop#getCondition <em>Condition</em>}</li>
 *   <li>{@link procedure.Loop#getLoopEntity <em>Loop Entity</em>}</li>
 * </ul>
 * </p>
 *
 * @see procedure.ProcedurePackage#getLoop()
 * @model
 * @generated
 */
public interface Loop extends Entity {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' attribute.
	 * @see #setCondition(String)
	 * @see procedure.ProcedurePackage#getLoop_Condition()
	 * @model required="true"
	 * @generated
	 */
	String getCondition();

	/**
	 * Sets the value of the '{@link procedure.Loop#getCondition <em>Condition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' attribute.
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(String value);

	/**
	 * Returns the value of the '<em><b>Loop Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Loop Entity</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Loop Entity</em>' containment reference.
	 * @see #setLoopEntity(Entity)
	 * @see procedure.ProcedurePackage#getLoop_LoopEntity()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Entity getLoopEntity();

	/**
	 * Sets the value of the '{@link procedure.Loop#getLoopEntity <em>Loop Entity</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loop Entity</em>' containment reference.
	 * @see #getLoopEntity()
	 * @generated
	 */
	void setLoopEntity(Entity value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkLoop(DiagnosticChain c1, Map<Object, Object> c2);

} // Loop
