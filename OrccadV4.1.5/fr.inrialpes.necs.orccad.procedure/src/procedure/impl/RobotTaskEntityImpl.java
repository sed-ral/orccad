/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure.impl;

import constraint.TemporalConstraint;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

import module.ModuleAlgorithmic;
import module.ModulePhysicalRobot;
import module.ModulePhysicalSensor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

import port.PortData;
import procedure.ProcedurePackage;
import procedure.RobotTaskEntity;

import procedure.util.ProcedureValidator;
import task.LinkData;
import task.LinkDriver;
import task.LinkParam;
import task.RobotTask;
import task.TaskPackage;

import task.util.TaskValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Robot Task Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link procedure.impl.RobotTaskEntityImpl#getTemporalConstraints <em>Temporal Constraints</em>}</li>
 *   <li>{@link procedure.impl.RobotTaskEntityImpl#getModuleAlgorithmics <em>Module Algorithmics</em>}</li>
 *   <li>{@link procedure.impl.RobotTaskEntityImpl#getModulePhysicalRobot <em>Module Physical Robot</em>}</li>
 *   <li>{@link procedure.impl.RobotTaskEntityImpl#getModulePhysicalSensors <em>Module Physical Sensors</em>}</li>
 *   <li>{@link procedure.impl.RobotTaskEntityImpl#getLinkDatas <em>Link Datas</em>}</li>
 *   <li>{@link procedure.impl.RobotTaskEntityImpl#getLinkDrivers <em>Link Drivers</em>}</li>
 *   <li>{@link procedure.impl.RobotTaskEntityImpl#getLinkParams <em>Link Params</em>}</li>
 *   <li>{@link procedure.impl.RobotTaskEntityImpl#getName <em>Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RobotTaskEntityImpl extends EntityImpl implements RobotTaskEntity {
	/**
	 * The cached value of the '{@link #getTemporalConstraints() <em>Temporal Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTemporalConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<TemporalConstraint> temporalConstraints;

	/**
	 * The cached value of the '{@link #getModuleAlgorithmics() <em>Module Algorithmics</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModuleAlgorithmics()
	 * @generated
	 * @ordered
	 */
	protected EList<ModuleAlgorithmic> moduleAlgorithmics;

	/**
	 * The cached value of the '{@link #getModulePhysicalRobot() <em>Module Physical Robot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModulePhysicalRobot()
	 * @generated
	 * @ordered
	 */
	protected ModulePhysicalRobot modulePhysicalRobot;

	/**
	 * The cached value of the '{@link #getModulePhysicalSensors() <em>Module Physical Sensors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getModulePhysicalSensors()
	 * @generated
	 * @ordered
	 */
	protected EList<ModulePhysicalSensor> modulePhysicalSensors;

	/**
	 * The cached value of the '{@link #getLinkDatas() <em>Link Datas</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkDatas()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkData> linkDatas;

	/**
	 * The cached value of the '{@link #getLinkDrivers() <em>Link Drivers</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkDrivers()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkDriver> linkDrivers;

	/**
	 * The cached value of the '{@link #getLinkParams() <em>Link Params</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkParams()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkParam> linkParams;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RobotTaskEntityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProcedurePackage.Literals.ROBOT_TASK_ENTITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TemporalConstraint> getTemporalConstraints() {
		if (temporalConstraints == null) {
			temporalConstraints = new EObjectContainmentEList<TemporalConstraint>(TemporalConstraint.class, this, ProcedurePackage.ROBOT_TASK_ENTITY__TEMPORAL_CONSTRAINTS);
		}
		return temporalConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModuleAlgorithmic> getModuleAlgorithmics() {
		if (moduleAlgorithmics == null) {
			moduleAlgorithmics = new EObjectContainmentEList<ModuleAlgorithmic>(ModuleAlgorithmic.class, this, ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_ALGORITHMICS);
		}
		return moduleAlgorithmics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ModulePhysicalRobot getModulePhysicalRobot() {
		return modulePhysicalRobot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetModulePhysicalRobot(ModulePhysicalRobot newModulePhysicalRobot, NotificationChain msgs) {
		ModulePhysicalRobot oldModulePhysicalRobot = modulePhysicalRobot;
		modulePhysicalRobot = newModulePhysicalRobot;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT, oldModulePhysicalRobot, newModulePhysicalRobot);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setModulePhysicalRobot(ModulePhysicalRobot newModulePhysicalRobot) {
		if (newModulePhysicalRobot != modulePhysicalRobot) {
			NotificationChain msgs = null;
			if (modulePhysicalRobot != null)
				msgs = ((InternalEObject)modulePhysicalRobot).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT, null, msgs);
			if (newModulePhysicalRobot != null)
				msgs = ((InternalEObject)newModulePhysicalRobot).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT, null, msgs);
			msgs = basicSetModulePhysicalRobot(newModulePhysicalRobot, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT, newModulePhysicalRobot, newModulePhysicalRobot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ModulePhysicalSensor> getModulePhysicalSensors() {
		if (modulePhysicalSensors == null) {
			modulePhysicalSensors = new EObjectContainmentEList<ModulePhysicalSensor>(ModulePhysicalSensor.class, this, ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_SENSORS);
		}
		return modulePhysicalSensors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkData> getLinkDatas() {
		if (linkDatas == null) {
			linkDatas = new EObjectContainmentEList<LinkData>(LinkData.class, this, ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DATAS);
		}
		return linkDatas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkDriver> getLinkDrivers() {
		if (linkDrivers == null) {
			linkDrivers = new EObjectContainmentEList<LinkDriver>(LinkDriver.class, this, ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DRIVERS);
		}
		return linkDrivers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkParam> getLinkParams() {
		if (linkParams == null) {
			linkParams = new EObjectContainmentEList<LinkParam>(LinkParam.class, this, ProcedurePackage.ROBOT_TASK_ENTITY__LINK_PARAMS);
		}
		return linkParams;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ProcedurePackage.ROBOT_TASK_ENTITY__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean checkLoop(DiagnosticChain rt3, Map<Object, Object> rt4) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (rt3 != null) {
				rt3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.ROBOT_TASK__CHECK_LOOP,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkLoop", EObjectValidator.getObjectLabel(this, rt4) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	
	public boolean checkRobotTask(DiagnosticChain rt3, Map<Object, Object> rt4) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		
		boolean allOK = true;
		System.out.println("SOSOSOSOS Start CHECK RT");

		if (this.getName()==null || this.getName().length()<1) {
			if (rt3 != null) {
				rt3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.ROBOT_TASK__CHECK_ROBOT_TASK,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkRobotTask", EObjectValidator.getObjectLabel(this, rt4) + " - Robot Task Name must be filled." }),
						 new Object [] { this }));
			}
			allOK = false;
		}
		System.out.println("SOSOSOSOS RT Name CHECK OK");

		// Il existe au moins un lien driver entre data outpu et driver input
		if (this.linkDrivers.isEmpty()) {
			if (rt3 != null) {
				rt3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.ROBOT_TASK__CHECK_ROBOT_TASK,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkRobotTask", EObjectValidator.getObjectLabel(this, rt4) + " - At least one LinkDriver from ModuleAlgo to ModulePhysical." }),
						 new Object [] { this }));
			}
			allOK = false;
			System.out.println("SOSOSOSOS Error LinkDriver empty");
		}
		else {
			System.out.println("SOSOSOSOS Check LinkDriver");

			// Check linkdriver
			Iterator<LinkDriver> itLink = this.linkDrivers.iterator();
			for (; itLink.hasNext(); ) {
				LinkDriver item = itLink.next();
				try {
					System.out.println("SOSOSOSOS Check LinkSource Type");

					if (item.getSource() instanceof PortData) {
						return allOK;
					}
				} catch (Exception e)
				{
					System.out.println("SOSOSOSOS ERROR RT " + e.toString());
				}
			}
			// No LinkDriver Data -> Driver found
			if (rt3 != null) {
				rt3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 TaskValidator.DIAGNOSTIC_SOURCE,
						 TaskValidator.ROBOT_TASK__CHECK_ROBOT_TASK,
						 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkRobotTask", EObjectValidator.getObjectLabel(this, rt4) + " - Missing at least one LinkDriver from ModuleAlgo to ModulePhysical." }),
						 new Object [] { this }));
			}
			System.out.println("SOSOSOSOS No LinkDriver from ModAlgo to Physical Ressource");

			allOK = false;
		}
		return allOK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ProcedurePackage.ROBOT_TASK_ENTITY__TEMPORAL_CONSTRAINTS:
				return ((InternalEList<?>)getTemporalConstraints()).basicRemove(otherEnd, msgs);
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_ALGORITHMICS:
				return ((InternalEList<?>)getModuleAlgorithmics()).basicRemove(otherEnd, msgs);
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT:
				return basicSetModulePhysicalRobot(null, msgs);
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_SENSORS:
				return ((InternalEList<?>)getModulePhysicalSensors()).basicRemove(otherEnd, msgs);
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DATAS:
				return ((InternalEList<?>)getLinkDatas()).basicRemove(otherEnd, msgs);
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DRIVERS:
				return ((InternalEList<?>)getLinkDrivers()).basicRemove(otherEnd, msgs);
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_PARAMS:
				return ((InternalEList<?>)getLinkParams()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ProcedurePackage.ROBOT_TASK_ENTITY__TEMPORAL_CONSTRAINTS:
				return getTemporalConstraints();
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_ALGORITHMICS:
				return getModuleAlgorithmics();
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT:
				return getModulePhysicalRobot();
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_SENSORS:
				return getModulePhysicalSensors();
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DATAS:
				return getLinkDatas();
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DRIVERS:
				return getLinkDrivers();
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_PARAMS:
				return getLinkParams();
			case ProcedurePackage.ROBOT_TASK_ENTITY__NAME:
				return getName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ProcedurePackage.ROBOT_TASK_ENTITY__TEMPORAL_CONSTRAINTS:
				getTemporalConstraints().clear();
				getTemporalConstraints().addAll((Collection<? extends TemporalConstraint>)newValue);
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_ALGORITHMICS:
				getModuleAlgorithmics().clear();
				getModuleAlgorithmics().addAll((Collection<? extends ModuleAlgorithmic>)newValue);
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT:
				setModulePhysicalRobot((ModulePhysicalRobot)newValue);
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_SENSORS:
				getModulePhysicalSensors().clear();
				getModulePhysicalSensors().addAll((Collection<? extends ModulePhysicalSensor>)newValue);
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DATAS:
				getLinkDatas().clear();
				getLinkDatas().addAll((Collection<? extends LinkData>)newValue);
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DRIVERS:
				getLinkDrivers().clear();
				getLinkDrivers().addAll((Collection<? extends LinkDriver>)newValue);
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_PARAMS:
				getLinkParams().clear();
				getLinkParams().addAll((Collection<? extends LinkParam>)newValue);
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__NAME:
				setName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ProcedurePackage.ROBOT_TASK_ENTITY__TEMPORAL_CONSTRAINTS:
				getTemporalConstraints().clear();
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_ALGORITHMICS:
				getModuleAlgorithmics().clear();
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT:
				setModulePhysicalRobot((ModulePhysicalRobot)null);
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_SENSORS:
				getModulePhysicalSensors().clear();
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DATAS:
				getLinkDatas().clear();
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DRIVERS:
				getLinkDrivers().clear();
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_PARAMS:
				getLinkParams().clear();
				return;
			case ProcedurePackage.ROBOT_TASK_ENTITY__NAME:
				setName(NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ProcedurePackage.ROBOT_TASK_ENTITY__TEMPORAL_CONSTRAINTS:
				return temporalConstraints != null && !temporalConstraints.isEmpty();
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_ALGORITHMICS:
				return moduleAlgorithmics != null && !moduleAlgorithmics.isEmpty();
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT:
				return modulePhysicalRobot != null;
			case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_SENSORS:
				return modulePhysicalSensors != null && !modulePhysicalSensors.isEmpty();
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DATAS:
				return linkDatas != null && !linkDatas.isEmpty();
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DRIVERS:
				return linkDrivers != null && !linkDrivers.isEmpty();
			case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_PARAMS:
				return linkParams != null && !linkParams.isEmpty();
			case ProcedurePackage.ROBOT_TASK_ENTITY__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == RobotTask.class) {
			switch (derivedFeatureID) {
				case ProcedurePackage.ROBOT_TASK_ENTITY__TEMPORAL_CONSTRAINTS: return TaskPackage.ROBOT_TASK__TEMPORAL_CONSTRAINTS;
				case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_ALGORITHMICS: return TaskPackage.ROBOT_TASK__MODULE_ALGORITHMICS;
				case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT: return TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_ROBOT;
				case ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_SENSORS: return TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_SENSORS;
				case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DATAS: return TaskPackage.ROBOT_TASK__LINK_DATAS;
				case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DRIVERS: return TaskPackage.ROBOT_TASK__LINK_DRIVERS;
				case ProcedurePackage.ROBOT_TASK_ENTITY__LINK_PARAMS: return TaskPackage.ROBOT_TASK__LINK_PARAMS;
				case ProcedurePackage.ROBOT_TASK_ENTITY__NAME: return TaskPackage.ROBOT_TASK__NAME;
				default: return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == RobotTask.class) {
			switch (baseFeatureID) {
				case TaskPackage.ROBOT_TASK__TEMPORAL_CONSTRAINTS: return ProcedurePackage.ROBOT_TASK_ENTITY__TEMPORAL_CONSTRAINTS;
				case TaskPackage.ROBOT_TASK__MODULE_ALGORITHMICS: return ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_ALGORITHMICS;
				case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_ROBOT: return ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT;
				case TaskPackage.ROBOT_TASK__MODULE_PHYSICAL_SENSORS: return ProcedurePackage.ROBOT_TASK_ENTITY__MODULE_PHYSICAL_SENSORS;
				case TaskPackage.ROBOT_TASK__LINK_DATAS: return ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DATAS;
				case TaskPackage.ROBOT_TASK__LINK_DRIVERS: return ProcedurePackage.ROBOT_TASK_ENTITY__LINK_DRIVERS;
				case TaskPackage.ROBOT_TASK__LINK_PARAMS: return ProcedurePackage.ROBOT_TASK_ENTITY__LINK_PARAMS;
				case TaskPackage.ROBOT_TASK__NAME: return ProcedurePackage.ROBOT_TASK_ENTITY__NAME;
				default: return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //RobotTaskEntityImpl
