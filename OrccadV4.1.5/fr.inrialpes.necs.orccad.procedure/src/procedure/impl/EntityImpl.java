/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import procedure.Entity;
import procedure.LinkEntities;
import procedure.ProcedurePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link procedure.impl.EntityImpl#getSourceLinks <em>Source Links</em>}</li>
 *   <li>{@link procedure.impl.EntityImpl#getTargetLinks <em>Target Links</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class EntityImpl extends EObjectImpl implements Entity {
	/**
	 * The cached value of the '{@link #getSourceLinks() <em>Source Links</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSourceLinks()
	 * @generated
	 * @ordered
	 */
	protected LinkEntities sourceLinks;

	/**
	 * The cached value of the '{@link #getTargetLinks() <em>Target Links</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetLinks()
	 * @generated
	 * @ordered
	 */
	protected LinkEntities targetLinks;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EntityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProcedurePackage.Literals.ENTITY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkEntities getSourceLinks() {
		if (sourceLinks != null && sourceLinks.eIsProxy()) {
			InternalEObject oldSourceLinks = (InternalEObject)sourceLinks;
			sourceLinks = (LinkEntities)eResolveProxy(oldSourceLinks);
			if (sourceLinks != oldSourceLinks) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ProcedurePackage.ENTITY__SOURCE_LINKS, oldSourceLinks, sourceLinks));
			}
		}
		return sourceLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkEntities basicGetSourceLinks() {
		return sourceLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSourceLinks(LinkEntities newSourceLinks, NotificationChain msgs) {
		LinkEntities oldSourceLinks = sourceLinks;
		sourceLinks = newSourceLinks;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ProcedurePackage.ENTITY__SOURCE_LINKS, oldSourceLinks, newSourceLinks);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceLinks(LinkEntities newSourceLinks) {
		if (newSourceLinks != sourceLinks) {
			NotificationChain msgs = null;
			if (sourceLinks != null)
				msgs = ((InternalEObject)sourceLinks).eInverseRemove(this, ProcedurePackage.LINK_ENTITIES__SOURCE, LinkEntities.class, msgs);
			if (newSourceLinks != null)
				msgs = ((InternalEObject)newSourceLinks).eInverseAdd(this, ProcedurePackage.LINK_ENTITIES__SOURCE, LinkEntities.class, msgs);
			msgs = basicSetSourceLinks(newSourceLinks, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ProcedurePackage.ENTITY__SOURCE_LINKS, newSourceLinks, newSourceLinks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkEntities getTargetLinks() {
		if (targetLinks != null && targetLinks.eIsProxy()) {
			InternalEObject oldTargetLinks = (InternalEObject)targetLinks;
			targetLinks = (LinkEntities)eResolveProxy(oldTargetLinks);
			if (targetLinks != oldTargetLinks) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ProcedurePackage.ENTITY__TARGET_LINKS, oldTargetLinks, targetLinks));
			}
		}
		return targetLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkEntities basicGetTargetLinks() {
		return targetLinks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTargetLinks(LinkEntities newTargetLinks, NotificationChain msgs) {
		LinkEntities oldTargetLinks = targetLinks;
		targetLinks = newTargetLinks;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, ProcedurePackage.ENTITY__TARGET_LINKS, oldTargetLinks, newTargetLinks);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetLinks(LinkEntities newTargetLinks) {
		if (newTargetLinks != targetLinks) {
			NotificationChain msgs = null;
			if (targetLinks != null)
				msgs = ((InternalEObject)targetLinks).eInverseRemove(this, ProcedurePackage.LINK_ENTITIES__TARGET, LinkEntities.class, msgs);
			if (newTargetLinks != null)
				msgs = ((InternalEObject)newTargetLinks).eInverseAdd(this, ProcedurePackage.LINK_ENTITIES__TARGET, LinkEntities.class, msgs);
			msgs = basicSetTargetLinks(newTargetLinks, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ProcedurePackage.ENTITY__TARGET_LINKS, newTargetLinks, newTargetLinks));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ProcedurePackage.ENTITY__SOURCE_LINKS:
				if (sourceLinks != null)
					msgs = ((InternalEObject)sourceLinks).eInverseRemove(this, ProcedurePackage.LINK_ENTITIES__SOURCE, LinkEntities.class, msgs);
				return basicSetSourceLinks((LinkEntities)otherEnd, msgs);
			case ProcedurePackage.ENTITY__TARGET_LINKS:
				if (targetLinks != null)
					msgs = ((InternalEObject)targetLinks).eInverseRemove(this, ProcedurePackage.LINK_ENTITIES__TARGET, LinkEntities.class, msgs);
				return basicSetTargetLinks((LinkEntities)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ProcedurePackage.ENTITY__SOURCE_LINKS:
				return basicSetSourceLinks(null, msgs);
			case ProcedurePackage.ENTITY__TARGET_LINKS:
				return basicSetTargetLinks(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ProcedurePackage.ENTITY__SOURCE_LINKS:
				if (resolve) return getSourceLinks();
				return basicGetSourceLinks();
			case ProcedurePackage.ENTITY__TARGET_LINKS:
				if (resolve) return getTargetLinks();
				return basicGetTargetLinks();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ProcedurePackage.ENTITY__SOURCE_LINKS:
				setSourceLinks((LinkEntities)newValue);
				return;
			case ProcedurePackage.ENTITY__TARGET_LINKS:
				setTargetLinks((LinkEntities)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ProcedurePackage.ENTITY__SOURCE_LINKS:
				setSourceLinks((LinkEntities)null);
				return;
			case ProcedurePackage.ENTITY__TARGET_LINKS:
				setTargetLinks((LinkEntities)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ProcedurePackage.ENTITY__SOURCE_LINKS:
				return sourceLinks != null;
			case ProcedurePackage.ENTITY__TARGET_LINKS:
				return targetLinks != null;
		}
		return super.eIsSet(featureID);
	}

} //EntityImpl
