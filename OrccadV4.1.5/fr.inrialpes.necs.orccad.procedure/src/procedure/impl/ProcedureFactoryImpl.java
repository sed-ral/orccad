/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import procedure.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProcedureFactoryImpl extends EFactoryImpl implements ProcedureFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ProcedureFactory init() {
		try {
			ProcedureFactory theProcedureFactory = (ProcedureFactory)EPackage.Registry.INSTANCE.getEFactory("http://procedure"); 
			if (theProcedureFactory != null) {
				return theProcedureFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ProcedureFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ProcedurePackage.SEQUENCE: return createSequence();
			case ProcedurePackage.PARALLEL: return createParallel();
			case ProcedurePackage.ROBOT_PROCEDURE: return createRobotProcedure();
			case ProcedurePackage.ROBOT_TASK_ENTITY: return createRobotTaskEntity();
			case ProcedurePackage.LINK_ENTITIES: return createLinkEntities();
			case ProcedurePackage.LOOP: return createLoop();
			case ProcedurePackage.CONDITION: return createCondition();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sequence createSequence() {
		SequenceImpl sequence = new SequenceImpl();
		return sequence;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Parallel createParallel() {
		ParallelImpl parallel = new ParallelImpl();
		return parallel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RobotProcedure createRobotProcedure() {
		RobotProcedureImpl robotProcedure = new RobotProcedureImpl();
		return robotProcedure;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RobotTaskEntity createRobotTaskEntity() {
		RobotTaskEntityImpl robotTaskEntity = new RobotTaskEntityImpl();
		return robotTaskEntity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkEntities createLinkEntities() {
		LinkEntitiesImpl linkEntities = new LinkEntitiesImpl();
		return linkEntities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Loop createLoop() {
		LoopImpl loop = new LoopImpl();
		return loop;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Condition createCondition() {
		ConditionImpl condition = new ConditionImpl();
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedurePackage getProcedurePackage() {
		return (ProcedurePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ProcedurePackage getPackage() {
		return ProcedurePackage.eINSTANCE;
	}

} //ProcedureFactoryImpl
