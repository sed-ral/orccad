/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure.impl;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import java.util.Map;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

import procedure.Entity;
import procedure.LinkEntities;
import procedure.ProcedurePackage;
import procedure.Sequence;
import procedure.util.ProcedureValidator;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sequence</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link procedure.impl.SequenceImpl#getSeqEntities <em>Seq Entities</em>}</li>
 *   <li>{@link procedure.impl.SequenceImpl#getLinkEntities <em>Link Entities</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SequenceImpl extends EntityImpl implements Sequence {
	/**
	 * The cached value of the '{@link #getSeqEntities() <em>Seq Entities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeqEntities()
	 * @generated
	 * @ordered
	 */
	protected EList<Entity> seqEntities;

	/**
	 * The cached value of the '{@link #getLinkEntities() <em>Link Entities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLinkEntities()
	 * @generated
	 * @ordered
	 */
	protected EList<LinkEntities> linkEntities;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SequenceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ProcedurePackage.Literals.SEQUENCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Entity> getSeqEntities() {
		if (seqEntities == null) {
			seqEntities = new EObjectContainmentEList<Entity>(Entity.class, this, ProcedurePackage.SEQUENCE__SEQ_ENTITIES);
		}
		return seqEntities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<LinkEntities> getLinkEntities() {
		if (linkEntities == null) {
			linkEntities = new EObjectContainmentEList<LinkEntities>(LinkEntities.class, this, ProcedurePackage.SEQUENCE__LINK_ENTITIES);
		}
		return linkEntities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkSequence(DiagnosticChain c1, Map<Object, Object> c2) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		
		// check if every linkEntity links entities included in the sequence
		
		boolean allOK = true;
		
		List<LinkEntities> lle = this.getLinkEntities();
		
		for(Iterator<LinkEntities> it = lle.iterator(); it.hasNext();){
			
			LinkEntities le_tmp = it.next();
			
			//source check
			if (!le_tmp.getSource().eContainer().equals(this) ) {
				if (c1 != null) {
					c1.add
						(new BasicDiagnostic
							(Diagnostic.ERROR,
							 ProcedureValidator.DIAGNOSTIC_SOURCE,
							 ProcedureValidator.SEQUENCE__CHECK_SEQUENCE,
							 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkSequence", EObjectValidator.getObjectLabel(this, c2) + " - the source must be in the same sequence."}),
							 new Object [] { le_tmp }));
				}
				allOK = false;
			}
			
			//target check
			if (!le_tmp.getTarget().eContainer().equals(this) ) {
				if (c1 != null) {
					c1.add
						(new BasicDiagnostic
							(Diagnostic.ERROR,
							 ProcedureValidator.DIAGNOSTIC_SOURCE,
							 ProcedureValidator.SEQUENCE__CHECK_SEQUENCE,
							 EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "checkSequence", EObjectValidator.getObjectLabel(this, c2) + " - the target must be in the same sequence."}),
							 new Object [] { le_tmp }));
				}
				allOK = false;
			}
			
		}
		
		
		return allOK;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case ProcedurePackage.SEQUENCE__SEQ_ENTITIES:
				return ((InternalEList<?>)getSeqEntities()).basicRemove(otherEnd, msgs);
			case ProcedurePackage.SEQUENCE__LINK_ENTITIES:
				return ((InternalEList<?>)getLinkEntities()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case ProcedurePackage.SEQUENCE__SEQ_ENTITIES:
				return getSeqEntities();
			case ProcedurePackage.SEQUENCE__LINK_ENTITIES:
				return getLinkEntities();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case ProcedurePackage.SEQUENCE__SEQ_ENTITIES:
				getSeqEntities().clear();
				getSeqEntities().addAll((Collection<? extends Entity>)newValue);
				return;
			case ProcedurePackage.SEQUENCE__LINK_ENTITIES:
				getLinkEntities().clear();
				getLinkEntities().addAll((Collection<? extends LinkEntities>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case ProcedurePackage.SEQUENCE__SEQ_ENTITIES:
				getSeqEntities().clear();
				return;
			case ProcedurePackage.SEQUENCE__LINK_ENTITIES:
				getLinkEntities().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case ProcedurePackage.SEQUENCE__SEQ_ENTITIES:
				return seqEntities != null && !seqEntities.isEmpty();
			case ProcedurePackage.SEQUENCE__LINK_ENTITIES:
				return linkEntities != null && !linkEntities.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SequenceImpl
