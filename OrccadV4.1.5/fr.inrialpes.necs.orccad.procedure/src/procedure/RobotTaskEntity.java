/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure;

import task.RobotTask;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Robot Task Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see procedure.ProcedurePackage#getRobotTaskEntity()
 * @model
 * @generated
 */
public interface RobotTaskEntity extends Entity, RobotTask {
} // RobotTaskEntity
