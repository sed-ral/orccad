/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see procedure.ProcedureFactory
 * @model kind="package"
 * @generated
 */
public interface ProcedurePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "procedure";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://procedure";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "procedure";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ProcedurePackage eINSTANCE = procedure.impl.ProcedurePackageImpl.init();

	/**
	 * The meta object id for the '{@link procedure.impl.EntityImpl <em>Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see procedure.impl.EntityImpl
	 * @see procedure.impl.ProcedurePackageImpl#getEntity()
	 * @generated
	 */
	int ENTITY = 0;

	/**
	 * The feature id for the '<em><b>Source Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__SOURCE_LINKS = 0;

	/**
	 * The feature id for the '<em><b>Target Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__TARGET_LINKS = 1;

	/**
	 * The number of structural features of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link procedure.impl.SequenceImpl <em>Sequence</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see procedure.impl.SequenceImpl
	 * @see procedure.impl.ProcedurePackageImpl#getSequence()
	 * @generated
	 */
	int SEQUENCE = 1;

	/**
	 * The feature id for the '<em><b>Source Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__SOURCE_LINKS = ENTITY__SOURCE_LINKS;

	/**
	 * The feature id for the '<em><b>Target Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__TARGET_LINKS = ENTITY__TARGET_LINKS;

	/**
	 * The feature id for the '<em><b>Seq Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__SEQ_ENTITIES = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Link Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE__LINK_ENTITIES = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sequence</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SEQUENCE_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link procedure.impl.ParallelImpl <em>Parallel</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see procedure.impl.ParallelImpl
	 * @see procedure.impl.ProcedurePackageImpl#getParallel()
	 * @generated
	 */
	int PARALLEL = 2;

	/**
	 * The feature id for the '<em><b>Source Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL__SOURCE_LINKS = ENTITY__SOURCE_LINKS;

	/**
	 * The feature id for the '<em><b>Target Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL__TARGET_LINKS = ENTITY__TARGET_LINKS;

	/**
	 * The feature id for the '<em><b>Para Entities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL__PARA_ENTITIES = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Parallel</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PARALLEL_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link procedure.impl.RobotProcedureImpl <em>Robot Procedure</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see procedure.impl.RobotProcedureImpl
	 * @see procedure.impl.ProcedurePackageImpl#getRobotProcedure()
	 * @generated
	 */
	int ROBOT_PROCEDURE = 3;

	/**
	 * The feature id for the '<em><b>Source Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_PROCEDURE__SOURCE_LINKS = ENTITY__SOURCE_LINKS;

	/**
	 * The feature id for the '<em><b>Target Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_PROCEDURE__TARGET_LINKS = ENTITY__TARGET_LINKS;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_PROCEDURE__NAME = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_PROCEDURE__ENTITY = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Conditions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_PROCEDURE__CONDITIONS = ENTITY_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Robot Procedure</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_PROCEDURE_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 3;

	/**
	 * The meta object id for the '{@link procedure.impl.RobotTaskEntityImpl <em>Robot Task Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see procedure.impl.RobotTaskEntityImpl
	 * @see procedure.impl.ProcedurePackageImpl#getRobotTaskEntity()
	 * @generated
	 */
	int ROBOT_TASK_ENTITY = 4;

	/**
	 * The feature id for the '<em><b>Source Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_ENTITY__SOURCE_LINKS = ENTITY__SOURCE_LINKS;

	/**
	 * The feature id for the '<em><b>Target Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_ENTITY__TARGET_LINKS = ENTITY__TARGET_LINKS;

	/**
	 * The feature id for the '<em><b>Temporal Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_ENTITY__TEMPORAL_CONSTRAINTS = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Module Algorithmics</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_ENTITY__MODULE_ALGORITHMICS = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Module Physical Robot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_ENTITY__MODULE_PHYSICAL_ROBOT = ENTITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Module Physical Sensors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_ENTITY__MODULE_PHYSICAL_SENSORS = ENTITY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Link Datas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_ENTITY__LINK_DATAS = ENTITY_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Link Drivers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_ENTITY__LINK_DRIVERS = ENTITY_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Link Params</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_ENTITY__LINK_PARAMS = ENTITY_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_ENTITY__NAME = ENTITY_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Robot Task Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROBOT_TASK_ENTITY_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 8;

	/**
	 * The meta object id for the '{@link procedure.impl.LinkEntitiesImpl <em>Link Entities</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see procedure.impl.LinkEntitiesImpl
	 * @see procedure.impl.ProcedurePackageImpl#getLinkEntities()
	 * @generated
	 */
	int LINK_ENTITIES = 5;

	/**
	 * The feature id for the '<em><b>Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_ENTITIES__SOURCE = 0;

	/**
	 * The feature id for the '<em><b>Target</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_ENTITIES__TARGET = 1;

	/**
	 * The number of structural features of the '<em>Link Entities</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LINK_ENTITIES_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link procedure.impl.LoopImpl <em>Loop</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see procedure.impl.LoopImpl
	 * @see procedure.impl.ProcedurePackageImpl#getLoop()
	 * @generated
	 */
	int LOOP = 6;

	/**
	 * The feature id for the '<em><b>Source Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP__SOURCE_LINKS = ENTITY__SOURCE_LINKS;

	/**
	 * The feature id for the '<em><b>Target Links</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP__TARGET_LINKS = ENTITY__TARGET_LINKS;

	/**
	 * The feature id for the '<em><b>Condition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP__CONDITION = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Loop Entity</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP__LOOP_ENTITY = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Loop</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOOP_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 2;


	/**
	 * The meta object id for the '{@link procedure.impl.ConditionImpl <em>Condition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see procedure.impl.ConditionImpl
	 * @see procedure.impl.ProcedurePackageImpl#getCondition()
	 * @generated
	 */
	int CONDITION = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__NAME = 0;

	/**
	 * The feature id for the '<em><b>Container</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION__CONTAINER = 1;

	/**
	 * The number of structural features of the '<em>Condition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONDITION_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link procedure.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity</em>'.
	 * @see procedure.Entity
	 * @generated
	 */
	EClass getEntity();

	/**
	 * Returns the meta object for the reference '{@link procedure.Entity#getSourceLinks <em>Source Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source Links</em>'.
	 * @see procedure.Entity#getSourceLinks()
	 * @see #getEntity()
	 * @generated
	 */
	EReference getEntity_SourceLinks();

	/**
	 * Returns the meta object for the reference '{@link procedure.Entity#getTargetLinks <em>Target Links</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target Links</em>'.
	 * @see procedure.Entity#getTargetLinks()
	 * @see #getEntity()
	 * @generated
	 */
	EReference getEntity_TargetLinks();

	/**
	 * Returns the meta object for class '{@link procedure.Sequence <em>Sequence</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sequence</em>'.
	 * @see procedure.Sequence
	 * @generated
	 */
	EClass getSequence();

	/**
	 * Returns the meta object for the containment reference list '{@link procedure.Sequence#getSeqEntities <em>Seq Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Seq Entities</em>'.
	 * @see procedure.Sequence#getSeqEntities()
	 * @see #getSequence()
	 * @generated
	 */
	EReference getSequence_SeqEntities();

	/**
	 * Returns the meta object for the containment reference list '{@link procedure.Sequence#getLinkEntities <em>Link Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Link Entities</em>'.
	 * @see procedure.Sequence#getLinkEntities()
	 * @see #getSequence()
	 * @generated
	 */
	EReference getSequence_LinkEntities();

	/**
	 * Returns the meta object for class '{@link procedure.Parallel <em>Parallel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Parallel</em>'.
	 * @see procedure.Parallel
	 * @generated
	 */
	EClass getParallel();

	/**
	 * Returns the meta object for the containment reference list '{@link procedure.Parallel#getParaEntities <em>Para Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Para Entities</em>'.
	 * @see procedure.Parallel#getParaEntities()
	 * @see #getParallel()
	 * @generated
	 */
	EReference getParallel_ParaEntities();

	/**
	 * Returns the meta object for class '{@link procedure.RobotProcedure <em>Robot Procedure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Robot Procedure</em>'.
	 * @see procedure.RobotProcedure
	 * @generated
	 */
	EClass getRobotProcedure();

	/**
	 * Returns the meta object for the attribute '{@link procedure.RobotProcedure#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see procedure.RobotProcedure#getName()
	 * @see #getRobotProcedure()
	 * @generated
	 */
	EAttribute getRobotProcedure_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link procedure.RobotProcedure#getConditions <em>Conditions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Conditions</em>'.
	 * @see procedure.RobotProcedure#getConditions()
	 * @see #getRobotProcedure()
	 * @generated
	 */
	EReference getRobotProcedure_Conditions();

	/**
	 * Returns the meta object for the containment reference '{@link procedure.RobotProcedure#getEntity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Entity</em>'.
	 * @see procedure.RobotProcedure#getEntity()
	 * @see #getRobotProcedure()
	 * @generated
	 */
	EReference getRobotProcedure_Entity();

	/**
	 * Returns the meta object for class '{@link procedure.RobotTaskEntity <em>Robot Task Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Robot Task Entity</em>'.
	 * @see procedure.RobotTaskEntity
	 * @generated
	 */
	EClass getRobotTaskEntity();

	/**
	 * Returns the meta object for class '{@link procedure.LinkEntities <em>Link Entities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Link Entities</em>'.
	 * @see procedure.LinkEntities
	 * @generated
	 */
	EClass getLinkEntities();

	/**
	 * Returns the meta object for the reference '{@link procedure.LinkEntities#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source</em>'.
	 * @see procedure.LinkEntities#getSource()
	 * @see #getLinkEntities()
	 * @generated
	 */
	EReference getLinkEntities_Source();

	/**
	 * Returns the meta object for the reference '{@link procedure.LinkEntities#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target</em>'.
	 * @see procedure.LinkEntities#getTarget()
	 * @see #getLinkEntities()
	 * @generated
	 */
	EReference getLinkEntities_Target();

	/**
	 * Returns the meta object for class '{@link procedure.Loop <em>Loop</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Loop</em>'.
	 * @see procedure.Loop
	 * @generated
	 */
	EClass getLoop();

	/**
	 * Returns the meta object for the attribute '{@link procedure.Loop#getCondition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Condition</em>'.
	 * @see procedure.Loop#getCondition()
	 * @see #getLoop()
	 * @generated
	 */
	EAttribute getLoop_Condition();

	/**
	 * Returns the meta object for the containment reference '{@link procedure.Loop#getLoopEntity <em>Loop Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loop Entity</em>'.
	 * @see procedure.Loop#getLoopEntity()
	 * @see #getLoop()
	 * @generated
	 */
	EReference getLoop_LoopEntity();

	/**
	 * Returns the meta object for class '{@link procedure.Condition <em>Condition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Condition</em>'.
	 * @see procedure.Condition
	 * @generated
	 */
	EClass getCondition();

	/**
	 * Returns the meta object for the attribute '{@link procedure.Condition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see procedure.Condition#getName()
	 * @see #getCondition()
	 * @generated
	 */
	EAttribute getCondition_Name();

	/**
	 * Returns the meta object for the container reference '{@link procedure.Condition#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Container</em>'.
	 * @see procedure.Condition#getContainer()
	 * @see #getCondition()
	 * @generated
	 */
	EReference getCondition_Container();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ProcedureFactory getProcedureFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link procedure.impl.EntityImpl <em>Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see procedure.impl.EntityImpl
		 * @see procedure.impl.ProcedurePackageImpl#getEntity()
		 * @generated
		 */
		EClass ENTITY = eINSTANCE.getEntity();

		/**
		 * The meta object literal for the '<em><b>Source Links</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY__SOURCE_LINKS = eINSTANCE.getEntity_SourceLinks();

		/**
		 * The meta object literal for the '<em><b>Target Links</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENTITY__TARGET_LINKS = eINSTANCE.getEntity_TargetLinks();

		/**
		 * The meta object literal for the '{@link procedure.impl.SequenceImpl <em>Sequence</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see procedure.impl.SequenceImpl
		 * @see procedure.impl.ProcedurePackageImpl#getSequence()
		 * @generated
		 */
		EClass SEQUENCE = eINSTANCE.getSequence();

		/**
		 * The meta object literal for the '<em><b>Seq Entities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE__SEQ_ENTITIES = eINSTANCE.getSequence_SeqEntities();

		/**
		 * The meta object literal for the '<em><b>Link Entities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SEQUENCE__LINK_ENTITIES = eINSTANCE.getSequence_LinkEntities();

		/**
		 * The meta object literal for the '{@link procedure.impl.ParallelImpl <em>Parallel</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see procedure.impl.ParallelImpl
		 * @see procedure.impl.ProcedurePackageImpl#getParallel()
		 * @generated
		 */
		EClass PARALLEL = eINSTANCE.getParallel();

		/**
		 * The meta object literal for the '<em><b>Para Entities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PARALLEL__PARA_ENTITIES = eINSTANCE.getParallel_ParaEntities();

		/**
		 * The meta object literal for the '{@link procedure.impl.RobotProcedureImpl <em>Robot Procedure</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see procedure.impl.RobotProcedureImpl
		 * @see procedure.impl.ProcedurePackageImpl#getRobotProcedure()
		 * @generated
		 */
		EClass ROBOT_PROCEDURE = eINSTANCE.getRobotProcedure();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROBOT_PROCEDURE__NAME = eINSTANCE.getRobotProcedure_Name();

		/**
		 * The meta object literal for the '<em><b>Conditions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROBOT_PROCEDURE__CONDITIONS = eINSTANCE.getRobotProcedure_Conditions();

		/**
		 * The meta object literal for the '<em><b>Entity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROBOT_PROCEDURE__ENTITY = eINSTANCE.getRobotProcedure_Entity();

		/**
		 * The meta object literal for the '{@link procedure.impl.RobotTaskEntityImpl <em>Robot Task Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see procedure.impl.RobotTaskEntityImpl
		 * @see procedure.impl.ProcedurePackageImpl#getRobotTaskEntity()
		 * @generated
		 */
		EClass ROBOT_TASK_ENTITY = eINSTANCE.getRobotTaskEntity();

		/**
		 * The meta object literal for the '{@link procedure.impl.LinkEntitiesImpl <em>Link Entities</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see procedure.impl.LinkEntitiesImpl
		 * @see procedure.impl.ProcedurePackageImpl#getLinkEntities()
		 * @generated
		 */
		EClass LINK_ENTITIES = eINSTANCE.getLinkEntities();

		/**
		 * The meta object literal for the '<em><b>Source</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_ENTITIES__SOURCE = eINSTANCE.getLinkEntities_Source();

		/**
		 * The meta object literal for the '<em><b>Target</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LINK_ENTITIES__TARGET = eINSTANCE.getLinkEntities_Target();

		/**
		 * The meta object literal for the '{@link procedure.impl.LoopImpl <em>Loop</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see procedure.impl.LoopImpl
		 * @see procedure.impl.ProcedurePackageImpl#getLoop()
		 * @generated
		 */
		EClass LOOP = eINSTANCE.getLoop();

		/**
		 * The meta object literal for the '<em><b>Condition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOOP__CONDITION = eINSTANCE.getLoop_Condition();

		/**
		 * The meta object literal for the '<em><b>Loop Entity</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOOP__LOOP_ENTITY = eINSTANCE.getLoop_LoopEntity();

		/**
		 * The meta object literal for the '{@link procedure.impl.ConditionImpl <em>Condition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see procedure.impl.ConditionImpl
		 * @see procedure.impl.ProcedurePackageImpl#getCondition()
		 * @generated
		 */
		EClass CONDITION = eINSTANCE.getCondition();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CONDITION__NAME = eINSTANCE.getCondition_Name();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONDITION__CONTAINER = eINSTANCE.getCondition_Container();

	}

} //ProcedurePackage
