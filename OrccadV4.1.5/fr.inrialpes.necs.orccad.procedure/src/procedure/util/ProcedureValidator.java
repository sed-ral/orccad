/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure.util;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import procedure.*;

import task.util.TaskValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see procedure.ProcedurePackage
 * @generated
 */
public class ProcedureValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final ProcedureValidator INSTANCE = new ProcedureValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "procedure";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Sequence' of 'Sequence'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SEQUENCE__CHECK_SEQUENCE = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Parallel' of 'Parallel'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int PARALLEL__CHECK_PARALLEL = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Robot Procedure' of 'Robot Procedure'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int ROBOT_PROCEDURE__CHECK_ROBOT_PROCEDURE = 3;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Link Entities' of 'Link Entities'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LINK_ENTITIES__CHECK_LINK_ENTITIES = 4;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Loop' of 'Loop'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int LOOP__CHECK_LOOP = 5;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Check Condition' of 'Condition'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int CONDITION__CHECK_CONDITION = 6;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 6;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TaskValidator taskValidator;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureValidator() {
		super();
		taskValidator = TaskValidator.INSTANCE;
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return ProcedurePackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case ProcedurePackage.ENTITY:
				return validateEntity((Entity)value, diagnostics, context);
			case ProcedurePackage.SEQUENCE:
				return validateSequence((Sequence)value, diagnostics, context);
			case ProcedurePackage.PARALLEL:
				return validateParallel((Parallel)value, diagnostics, context);
			case ProcedurePackage.ROBOT_PROCEDURE:
				return validateRobotProcedure((RobotProcedure)value, diagnostics, context);
			case ProcedurePackage.ROBOT_TASK_ENTITY:
				return validateRobotTaskEntity((RobotTaskEntity)value, diagnostics, context);
			case ProcedurePackage.LINK_ENTITIES:
				return validateLinkEntities((LinkEntities)value, diagnostics, context);
			case ProcedurePackage.LOOP:
				return validateLoop((Loop)value, diagnostics, context);
			case ProcedurePackage.CONDITION:
				return validateCondition((Condition)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEntity(Entity entity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(entity, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSequence(Sequence sequence, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(sequence, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(sequence, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(sequence, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(sequence, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(sequence, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(sequence, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(sequence, diagnostics, context);
		if (result || diagnostics != null) result &= validateSequence_checkSequence(sequence, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkSequence constraint of '<em>Sequence</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSequence_checkSequence(Sequence sequence, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return sequence.checkSequence(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateParallel(Parallel parallel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(parallel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(parallel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(parallel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(parallel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(parallel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(parallel, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(parallel, diagnostics, context);
		if (result || diagnostics != null) result &= validateParallel_checkParallel(parallel, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkParallel constraint of '<em>Parallel</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateParallel_checkParallel(Parallel parallel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return parallel.checkParallel(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRobotProcedure(RobotProcedure robotProcedure, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(robotProcedure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(robotProcedure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(robotProcedure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(robotProcedure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(robotProcedure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(robotProcedure, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(robotProcedure, diagnostics, context);
		if (result || diagnostics != null) result &= validateRobotProcedure_checkRobotProcedure(robotProcedure, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkRobotProcedure constraint of '<em>Robot Procedure</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRobotProcedure_checkRobotProcedure(RobotProcedure robotProcedure, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return robotProcedure.checkRobotProcedure(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRobotTaskEntity(RobotTaskEntity robotTaskEntity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(robotTaskEntity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(robotTaskEntity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(robotTaskEntity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(robotTaskEntity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(robotTaskEntity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(robotTaskEntity, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(robotTaskEntity, diagnostics, context);
		if (result || diagnostics != null) result &= taskValidator.validateRobotTask_checkLoop(robotTaskEntity, diagnostics, context);
		if (result || diagnostics != null) result &= taskValidator.validateRobotTask_checkRobotTask(robotTaskEntity, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkEntities(LinkEntities linkEntities, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(linkEntities, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(linkEntities, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(linkEntities, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(linkEntities, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(linkEntities, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(linkEntities, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(linkEntities, diagnostics, context);
		if (result || diagnostics != null) result &= validateLinkEntities_checkLinkEntities(linkEntities, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkLinkEntities constraint of '<em>Link Entities</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLinkEntities_checkLinkEntities(LinkEntities linkEntities, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return linkEntities.checkLinkEntities(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLoop(Loop loop, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(loop, diagnostics, context);
		if (result || diagnostics != null) result &= validateLoop_checkLoop(loop, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkLoop constraint of '<em>Loop</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLoop_checkLoop(Loop loop, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return loop.checkLoop(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCondition(Condition condition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(condition, diagnostics, context);
		if (result || diagnostics != null) result &= validateCondition_checkCondition(condition, diagnostics, context);
		return result;
	}

	/**
	 * Validates the checkCondition constraint of '<em>Condition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCondition_checkCondition(Condition condition, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return condition.checkCondition(diagnostics, context);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //ProcedureValidator
