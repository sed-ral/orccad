/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sequence</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link procedure.Sequence#getSeqEntities <em>Seq Entities</em>}</li>
 *   <li>{@link procedure.Sequence#getLinkEntities <em>Link Entities</em>}</li>
 * </ul>
 * </p>
 *
 * @see procedure.ProcedurePackage#getSequence()
 * @model
 * @generated
 */
public interface Sequence extends Entity {
	/**
	 * Returns the value of the '<em><b>Seq Entities</b></em>' containment reference list.
	 * The list contents are of type {@link procedure.Entity}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Seq Entities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Seq Entities</em>' containment reference list.
	 * @see procedure.ProcedurePackage#getSequence_SeqEntities()
	 * @model containment="true" lower="2"
	 * @generated
	 */
	EList<Entity> getSeqEntities();

	/**
	 * Returns the value of the '<em><b>Link Entities</b></em>' containment reference list.
	 * The list contents are of type {@link procedure.LinkEntities}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Link Entities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Link Entities</em>' containment reference list.
	 * @see procedure.ProcedurePackage#getSequence_LinkEntities()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<LinkEntities> getLinkEntities();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean checkSequence(DiagnosticChain c1, Map<Object, Object> c2);

} // Sequence
