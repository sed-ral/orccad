/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link procedure.Entity#getSourceLinks <em>Source Links</em>}</li>
 *   <li>{@link procedure.Entity#getTargetLinks <em>Target Links</em>}</li>
 * </ul>
 * </p>
 *
 * @see procedure.ProcedurePackage#getEntity()
 * @model abstract="true"
 * @generated
 */
public interface Entity extends EObject {
	/**
	 * Returns the value of the '<em><b>Source Links</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link procedure.LinkEntities#getSource <em>Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Links</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Links</em>' reference.
	 * @see #setSourceLinks(LinkEntities)
	 * @see procedure.ProcedurePackage#getEntity_SourceLinks()
	 * @see procedure.LinkEntities#getSource
	 * @model opposite="source"
	 * @generated
	 */
	LinkEntities getSourceLinks();

	/**
	 * Sets the value of the '{@link procedure.Entity#getSourceLinks <em>Source Links</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Links</em>' reference.
	 * @see #getSourceLinks()
	 * @generated
	 */
	void setSourceLinks(LinkEntities value);

	/**
	 * Returns the value of the '<em><b>Target Links</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link procedure.LinkEntities#getTarget <em>Target</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target Links</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target Links</em>' reference.
	 * @see #setTargetLinks(LinkEntities)
	 * @see procedure.ProcedurePackage#getEntity_TargetLinks()
	 * @see procedure.LinkEntities#getTarget
	 * @model opposite="target"
	 * @generated
	 */
	LinkEntities getTargetLinks();

	/**
	 * Sets the value of the '{@link procedure.Entity#getTargetLinks <em>Target Links</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target Links</em>' reference.
	 * @see #getTargetLinks()
	 * @generated
	 */
	void setTargetLinks(LinkEntities value);

} // Entity
