/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure.tests;

import junit.textui.TestRunner;

import procedure.ProcedureFactory;
import procedure.RobotProcedure;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Robot Procedure</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class RobotProcedureTest extends EntityTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(RobotProcedureTest.class);
	}

	/**
	 * Constructs a new Robot Procedure test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RobotProcedureTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Robot Procedure test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected RobotProcedure getFixture() {
		return (RobotProcedure)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ProcedureFactory.eINSTANCE.createRobotProcedure());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //RobotProcedureTest
