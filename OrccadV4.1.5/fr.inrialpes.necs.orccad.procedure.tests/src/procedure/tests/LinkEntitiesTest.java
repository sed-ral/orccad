/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import procedure.LinkEntities;
import procedure.ProcedureFactory;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Link Entities</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class LinkEntitiesTest extends TestCase {

	/**
	 * The fixture for this Link Entities test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkEntities fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(LinkEntitiesTest.class);
	}

	/**
	 * Constructs a new Link Entities test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LinkEntitiesTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Link Entities test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(LinkEntities fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Link Entities test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LinkEntities getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ProcedureFactory.eINSTANCE.createLinkEntities());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //LinkEntitiesTest
