/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure.tests;

import junit.textui.TestRunner;

import procedure.ProcedureFactory;
import procedure.RobotTaskEntity;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Robot Task Entity</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link task.RobotTask#checkLoop(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Check Loop</em>}</li>
 *   <li>{@link task.RobotTask#checkRobotTask(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Check Robot Task</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class RobotTaskEntityTest extends EntityTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(RobotTaskEntityTest.class);
	}

	/**
	 * Constructs a new Robot Task Entity test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RobotTaskEntityTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Robot Task Entity test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected RobotTaskEntity getFixture() {
		return (RobotTaskEntity)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(ProcedureFactory.eINSTANCE.createRobotTaskEntity());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link task.RobotTask#checkLoop(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Check Loop</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see task.RobotTask#checkLoop(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testCheckLoop__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link task.RobotTask#checkRobotTask(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Check Robot Task</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see task.RobotTask#checkRobotTask(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testCheckRobotTask__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //RobotTaskEntityTest
