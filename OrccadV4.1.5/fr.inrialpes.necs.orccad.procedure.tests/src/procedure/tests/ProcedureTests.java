/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>procedure</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProcedureTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new ProcedureTests("procedure Tests");
		suite.addTestSuite(RobotTaskEntityTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureTests(String name) {
		super(name);
	}

} //ProcedureTests
