/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package procedure.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Procedure</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class ProcedureAllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new ProcedureAllTests("Procedure Tests");
		suite.addTest(ProcedureTests.suite());
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProcedureAllTests(String name) {
		super(name);
	}

} //ProcedureAllTests
