package custom.command;


import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

public class DuplicateAnythingCommand extends DuplicateEObjectsCommand {



	public DuplicateAnythingCommand(TransactionalEditingDomain editingDomain,
			String label, List eObjectsToBeDuplicated,
			Map allDuplicatedObjectsMap, EObject targetContainer) {
		super(editingDomain, label, eObjectsToBeDuplicated, allDuplicatedObjectsMap,
				targetContainer);
		// TODO Auto-generated constructor stub
	}

	public DuplicateAnythingCommand(TransactionalEditingDomain editingDomain,
			String label, List eObjectsToBeDuplicated,
			Map allDuplicatedObjectsMap) {
		super(editingDomain, label, eObjectsToBeDuplicated, allDuplicatedObjectsMap);
		// TODO Auto-generated constructor stub
	}

	public DuplicateAnythingCommand(TransactionalEditingDomain editingDomain,
			String label, List eObjectsToBeDuplicated) {
		super(editingDomain, label, eObjectsToBeDuplicated);
		// TODO Auto-generated constructor stub
	}

	public DuplicateAnythingCommand(TransactionalEditingDomain editingDomain,
			DuplicateElementsRequest req) {
		super(editingDomain, req.getLabel(), req.getElementsToBeDuplicated(),
				req.getAllDuplicatedElementsMap());
	}
}
