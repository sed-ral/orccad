package task.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

import port.ExceptionT1;
import port.ExceptionT2;
import port.ExceptionT3;
import port.PortData;
import port.PortDriver;
import port.PortParam;
import task.LinkData;
import task.RobotTask;
import task.diagram.edit.parts.EventCondition2EditPart;
import task.diagram.edit.parts.EventCondition3EditPart;

import task.diagram.edit.parts.EventConditionEditPart;
import task.diagram.edit.parts.EventConditionEventName2EditPart;
import task.diagram.edit.parts.EventConditionEventName3EditPart;

import task.diagram.edit.parts.EventConditionEventNameEditPart;
import task.diagram.edit.parts.ExceptionT12EditPart;

import task.diagram.edit.parts.ExceptionT1EditPart;
import task.diagram.edit.parts.ExceptionT22EditPart;

import task.diagram.edit.parts.ExceptionT2EditPart;
import task.diagram.edit.parts.ExceptionT32EditPart;

import task.diagram.edit.parts.ExceptionT3EditPart;

import task.diagram.edit.parts.LinkDataEditPart;
import task.diagram.edit.parts.LinkDriverEditPart;
import task.diagram.edit.parts.LinkParamEditPart;

import task.diagram.edit.parts.ModuleAlgorithmicEditPart;

import task.diagram.edit.parts.ModuleAlgorithmicNameEditPart;
import task.diagram.edit.parts.ModulePhysicalRobotEditPart;
import task.diagram.edit.parts.ModulePhysicalRobotNameEditPart;
import task.diagram.edit.parts.ModulePhysicalSensorEditPart;
import task.diagram.edit.parts.ModulePhysicalSensorNameEditPart;

import task.diagram.edit.parts.PortDataEditPart;
import task.diagram.edit.parts.PortDriver2EditPart;
import task.diagram.edit.parts.PortDriverEditPart;
import task.diagram.edit.parts.PortParam2EditPart;
import task.diagram.edit.parts.PortParam3EditPart;

import task.diagram.edit.parts.PortParamEditPart;
import task.diagram.edit.parts.RobotTaskEditPart;
import task.diagram.edit.parts.TemporalConstraintEditPart;
import task.diagram.edit.parts.TemporalConstraintNameEditPart;
import task.diagram.part.TaskDiagramEditorPlugin;
import task.diagram.part.TaskVisualIDRegistry;
import task.diagram.providers.TaskElementTypes;
import task.diagram.providers.TaskParserProvider;

/**
 * @generated
 */
public class TaskNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		TaskDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put(
						"Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		TaskDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put(
						"Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof TaskNavigatorItem
				&& !isOwnView(((TaskNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof TaskNavigatorGroup) {
			TaskNavigatorGroup group = (TaskNavigatorGroup) element;
			return TaskDiagramEditorPlugin.getInstance().getBundledImage(
					group.getIcon());
		}

		if (element instanceof TaskNavigatorItem) {
			TaskNavigatorItem navigatorItem = (TaskNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (TaskVisualIDRegistry.getVisualID(view)) {
		case RobotTaskEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://task?RobotTask", TaskElementTypes.RobotTask_1000); //$NON-NLS-1$
		case ModuleAlgorithmicEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://module?ModuleAlgorithmic", TaskElementTypes.ModuleAlgorithmic_2001); //$NON-NLS-1$
		case ModulePhysicalRobotEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://module?ModulePhysicalRobot", TaskElementTypes.ModulePhysicalRobot_2002); //$NON-NLS-1$
		case ModulePhysicalSensorEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://module?ModulePhysicalSensor", TaskElementTypes.ModulePhysicalSensor_2003); //$NON-NLS-1$
		case TemporalConstraintEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://constraint?TemporalConstraint", TaskElementTypes.TemporalConstraint_2004); //$NON-NLS-1$
		case PortDataEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?PortData", TaskElementTypes.PortData_3001); //$NON-NLS-1$
		case PortParamEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?PortParam", TaskElementTypes.PortParam_3002); //$NON-NLS-1$
		case ExceptionT3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?ExceptionT3", TaskElementTypes.ExceptionT3_3007); //$NON-NLS-1$
		case ExceptionT1EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?ExceptionT1", TaskElementTypes.ExceptionT1_3008); //$NON-NLS-1$
		case ExceptionT2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?ExceptionT2", TaskElementTypes.ExceptionT2_3009); //$NON-NLS-1$
		case EventConditionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?EventCondition", TaskElementTypes.EventCondition_3018); //$NON-NLS-1$
		case PortParam2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?PortParam", TaskElementTypes.PortParam_3003); //$NON-NLS-1$
		case PortDriverEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?PortDriver", TaskElementTypes.PortDriver_3004); //$NON-NLS-1$
		case ExceptionT32EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?ExceptionT3", TaskElementTypes.ExceptionT3_3010); //$NON-NLS-1$
		case ExceptionT12EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?ExceptionT1", TaskElementTypes.ExceptionT1_3011); //$NON-NLS-1$
		case ExceptionT22EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?ExceptionT2", TaskElementTypes.ExceptionT2_3012); //$NON-NLS-1$
		case EventCondition2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?EventCondition", TaskElementTypes.EventCondition_3019); //$NON-NLS-1$
		case PortParam3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?PortParam", TaskElementTypes.PortParam_3005); //$NON-NLS-1$
		case PortDriver2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?PortDriver", TaskElementTypes.PortDriver_3006); //$NON-NLS-1$
		case EventCondition3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://port?EventCondition", TaskElementTypes.EventCondition_3020); //$NON-NLS-1$
		case LinkDriverEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://task?LinkDriver", TaskElementTypes.LinkDriver_4001); //$NON-NLS-1$
		case LinkParamEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://task?LinkParam", TaskElementTypes.LinkParam_4002); //$NON-NLS-1$
		case LinkDataEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://task?LinkData", TaskElementTypes.LinkData_4003); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = TaskDiagramEditorPlugin.getInstance()
				.getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& TaskElementTypes.isKnownElementType(elementType)) {
			image = TaskElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof TaskNavigatorGroup) {
			TaskNavigatorGroup group = (TaskNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof TaskNavigatorItem) {
			TaskNavigatorItem navigatorItem = (TaskNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (TaskVisualIDRegistry.getVisualID(view)) {
		case RobotTaskEditPart.VISUAL_ID:
			return getRobotTask_1000Text(view);
		case ModuleAlgorithmicEditPart.VISUAL_ID:
			return getModuleAlgorithmic_2001Text(view);
		case ModulePhysicalRobotEditPart.VISUAL_ID:
			return getModulePhysicalRobot_2002Text(view);
		case ModulePhysicalSensorEditPart.VISUAL_ID:
			return getModulePhysicalSensor_2003Text(view);
		case TemporalConstraintEditPart.VISUAL_ID:
			return getTemporalConstraint_2004Text(view);
		case PortDataEditPart.VISUAL_ID:
			return getPortData_3001Text(view);
		case PortParamEditPart.VISUAL_ID:
			return getPortParam_3002Text(view);
		case ExceptionT3EditPart.VISUAL_ID:
			return getExceptionT3_3007Text(view);
		case ExceptionT1EditPart.VISUAL_ID:
			return getExceptionT1_3008Text(view);
		case ExceptionT2EditPart.VISUAL_ID:
			return getExceptionT2_3009Text(view);
		case EventConditionEditPart.VISUAL_ID:
			return getEventCondition_3018Text(view);
		case PortParam2EditPart.VISUAL_ID:
			return getPortParam_3003Text(view);
		case PortDriverEditPart.VISUAL_ID:
			return getPortDriver_3004Text(view);
		case ExceptionT32EditPart.VISUAL_ID:
			return getExceptionT3_3010Text(view);
		case ExceptionT12EditPart.VISUAL_ID:
			return getExceptionT1_3011Text(view);
		case ExceptionT22EditPart.VISUAL_ID:
			return getExceptionT2_3012Text(view);
		case EventCondition2EditPart.VISUAL_ID:
			return getEventCondition_3019Text(view);
		case PortParam3EditPart.VISUAL_ID:
			return getPortParam_3005Text(view);
		case PortDriver2EditPart.VISUAL_ID:
			return getPortDriver_3006Text(view);
		case EventCondition3EditPart.VISUAL_ID:
			return getEventCondition_3020Text(view);
		case LinkDriverEditPart.VISUAL_ID:
			return getLinkDriver_4001Text(view);
		case LinkParamEditPart.VISUAL_ID:
			return getLinkParam_4002Text(view);
		case LinkDataEditPart.VISUAL_ID:
			return getLinkData_4003Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getRobotTask_1000Text(View view) {
		RobotTask domainModelElement = (RobotTask) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getModuleAlgorithmic_2001Text(View view) {
		IParser parser = TaskParserProvider.getParser(
				TaskElementTypes.ModuleAlgorithmic_2001,
				view.getElement() != null ? view.getElement() : view,
				TaskVisualIDRegistry
						.getType(ModuleAlgorithmicNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getModulePhysicalRobot_2002Text(View view) {
		IParser parser = TaskParserProvider.getParser(
				TaskElementTypes.ModulePhysicalRobot_2002,
				view.getElement() != null ? view.getElement() : view,
				TaskVisualIDRegistry
						.getType(ModulePhysicalRobotNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getModulePhysicalSensor_2003Text(View view) {
		IParser parser = TaskParserProvider.getParser(
				TaskElementTypes.ModulePhysicalSensor_2003,
				view.getElement() != null ? view.getElement() : view,
				TaskVisualIDRegistry
						.getType(ModulePhysicalSensorNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getTemporalConstraint_2004Text(View view) {
		IParser parser = TaskParserProvider.getParser(
				TaskElementTypes.TemporalConstraint_2004,
				view.getElement() != null ? view.getElement() : view,
				TaskVisualIDRegistry
						.getType(TemporalConstraintNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPortData_3001Text(View view) {
		PortData domainModelElement = (PortData) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPortParam_3002Text(View view) {
		PortParam domainModelElement = (PortParam) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getExceptionT3_3007Text(View view) {
		ExceptionT3 domainModelElement = (ExceptionT3) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3007); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getExceptionT1_3008Text(View view) {
		ExceptionT1 domainModelElement = (ExceptionT1) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3008); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getExceptionT2_3009Text(View view) {
		ExceptionT2 domainModelElement = (ExceptionT2) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getEventCondition_3018Text(View view) {
		IParser parser = TaskParserProvider.getParser(
				TaskElementTypes.EventCondition_3018,
				view.getElement() != null ? view.getElement() : view,
				TaskVisualIDRegistry
						.getType(EventConditionEventNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5006); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPortParam_3003Text(View view) {
		PortParam domainModelElement = (PortParam) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPortDriver_3004Text(View view) {
		PortDriver domainModelElement = (PortDriver) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3004); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getEventCondition_3020Text(View view) {
		IParser parser = TaskParserProvider.getParser(
				TaskElementTypes.EventCondition_3020,
				view.getElement() != null ? view.getElement() : view,
				TaskVisualIDRegistry
						.getType(EventConditionEventName3EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5008); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getExceptionT3_3010Text(View view) {
		ExceptionT3 domainModelElement = (ExceptionT3) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3010); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getExceptionT1_3011Text(View view) {
		ExceptionT1 domainModelElement = (ExceptionT1) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3011); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getExceptionT2_3012Text(View view) {
		ExceptionT2 domainModelElement = (ExceptionT2) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3012); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getEventCondition_3019Text(View view) {
		IParser parser = TaskParserProvider.getParser(
				TaskElementTypes.EventCondition_3019,
				view.getElement() != null ? view.getElement() : view,
				TaskVisualIDRegistry
						.getType(EventConditionEventName2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5007); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPortParam_3005Text(View view) {
		PortParam domainModelElement = (PortParam) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getPortDriver_3006Text(View view) {
		PortDriver domainModelElement = (PortDriver) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getPortName();
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 3006); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getLinkDriver_4001Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getLinkParam_4002Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getLinkData_4003Text(View view) {
		LinkData domainModelElement = (LinkData) view.getElement();
		if (domainModelElement != null) {
			return String.valueOf(domainModelElement.getLinkType());
		} else {
			TaskDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 4003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return RobotTaskEditPart.MODEL_ID.equals(TaskVisualIDRegistry
				.getModelID(view));
	}

}
