package task.diagram.preferences;

import org.eclipse.core.internal.preferences.IPreferencesConstants;
import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.gmf.runtime.diagram.ui.preferences.IPreferenceConstants;
import org.eclipse.gmf.runtime.notation.DiagramLinkStyle;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.Routing;
import org.eclipse.gmf.runtime.notation.RoutingStyle;
import org.eclipse.jface.preference.IPreferenceStore;

import task.diagram.part.TaskDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramPreferenceInitializer extends AbstractPreferenceInitializer {

	/**
	 * @generated NOT
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = getPreferenceStore();
		DiagramGeneralPreferencePage.initDefaults(store);
		DiagramAppearancePreferencePage.initDefaults(store);
		DiagramConnectionsPreferencePage.initDefaults(store);
		DiagramPrintingPreferencePage.initDefaults(store);
		DiagramRulersAndGridPreferencePage.initDefaults(store);

		// added by boudinf
		store.setDefault(IPreferenceConstants.PREF_LINE_STYLE,
				Routing.RECTILINEAR);
		store.setValue(IPreferenceConstants.PREF_LINE_STYLE,
				Routing.RECTILINEAR);

		store.setDefault(IPreferenceConstants.PREF_SHOW_GRID, true);
		store.setValue(IPreferenceConstants.PREF_SHOW_GRID, true);

		store.setDefault(IPreferenceConstants.PREF_SNAP_TO_GRID, true);
		store.setValue(IPreferenceConstants.PREF_SNAP_TO_GRID, true);

		//end boudinf

	}

	/**
	 * @generated
	 */
	protected IPreferenceStore getPreferenceStore() {
		return TaskDiagramEditorPlugin.getInstance().getPreferenceStore();
	}
}
