package task.diagram.edit.parts;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.ConnectionRouter;
import org.eclipse.draw2d.ManhattanConnectionRouter;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.workspace.AbstractEMFOperation;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.Routing;
import org.eclipse.gmf.runtime.notation.RoutingStyle;
import org.eclipse.gmf.runtime.notation.View;

import org.eclipse.swt.graphics.Color;

import port.PortData;
import port.PortIOType;
import task.LinkData;
import task.LinkDataType;
import task.diagram.edit.policies.LinkDataItemSemanticEditPolicy;

/**
 * @generated
 */
public class LinkDataEditPart extends ConnectionNodeEditPart implements
		ITreeBranchEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 4003;

	/**
	 * @generated 
	 */
	public LinkDataEditPart(View view) {
		super(view);
	}

	// added by boudinf
	protected void handleNotificationEvent(Notification notification) {
		if (notification.getNotifier() instanceof LinkData) {
			System.out.println("handleNotificationEvent");
			try {

				LinkData mObj = (LinkData) ((View) LinkDataEditPart.this
						.getModel()).getElement();

				switch (mObj.getLinkType()) {

				case ASYN_ASYN:
					// point point tiret point point tiret ...
					getPrimaryShape().setLineStyle(5);
					break;

				case ASYN_SYN:
					// ligne tirets
					getPrimaryShape().setLineStyle(2);
					break;

				case SYN_ASYN:
					// ligne de points
					getPrimaryShape().setLineStyle(3);
					break;

				case SYN_SYN:
					//tiret point tiret point ...
					getPrimaryShape().setLineStyle(4);
					break;

				case FUNC:
					// ligne pleine
					getPrimaryShape().setLineStyle(1);
					break;

				}//switch

			} catch (Exception e) {
				System.out.println(e);
			}
		}
		super.handleNotificationEvent(notification);
	}

	// end boudinf

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new LinkDataItemSemanticEditPolicy());
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated NOT
	 */
	protected Connection createConnectionFigure() {
		LinkDataFigure fig = new LinkDataFigure();

		try {

			LinkData mObj = (LinkData) ((View) this.getModel()).getElement();

			switch (mObj.getLinkType()) {

			case ASYN_ASYN:
				// point point tiret point point tiret ...
				fig.setLineStyle(5);
				break;

			case ASYN_SYN:
				// ligne tirets
				fig.setLineStyle(2);
				break;

			case SYN_ASYN:
				// ligne de points
				fig.setLineStyle(3);
				break;

			case SYN_SYN:
				//tiret point tiret point ...
				fig.setLineStyle(4);
				break;

			case FUNC:
				// ligne pleine
				fig.setLineStyle(1);
				break;

			}//switch

		} catch (Exception e) {
			System.out.println(e);
		}//try

		fig.setRoutingStyles(true, true);

		return fig;
	}

	/**
	 * @generated
	 */
	public LinkDataFigure getPrimaryShape() {
		return (LinkDataFigure) getFigure();
	}

	/**
	 * @generated
	 */
	public class LinkDataFigure extends PolylineConnectionEx {

		/**
		 * @generated 
		 */
		public LinkDataFigure() {
			this.setLineWidth(2);
			this.setForegroundColor(THIS_FORE);

			setTargetDecoration(createTargetDecoration());
		}

		/*	public LinkDataFigure() {
				this.setLineWidth(2);
				this.setForegroundColor(THIS_FORE);
				this.setRoutingStyles(false, true);
				this.setSmoothness(SMOOTH_MORE);
				setTargetDecoration(createTargetDecoration());
			}*/
		/**
		 * @generated
		 */
		private RotatableDecoration createTargetDecoration() {
			PolylineDecoration df = new PolylineDecoration();
			df.setLineWidth(1);
			PointList pl = new PointList();
			pl.addPoint(getMapMode().DPtoLP(-1), getMapMode().DPtoLP(1));
			pl.addPoint(getMapMode().DPtoLP(0), getMapMode().DPtoLP(0));
			pl.addPoint(getMapMode().DPtoLP(-1), getMapMode().DPtoLP(-1));
			df.setTemplate(pl);
			df.setScale(getMapMode().DPtoLP(7), getMapMode().DPtoLP(3));
			return df;
		}

	}

	/**
	 * @generated
	 */
	static final Color THIS_FORE = new Color(null, 255, 0, 0);

}
