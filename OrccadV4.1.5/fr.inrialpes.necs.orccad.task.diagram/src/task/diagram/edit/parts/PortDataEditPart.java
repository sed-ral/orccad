package task.diagram.edit.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Ellipse;
import org.eclipse.draw2d.FlowLayout;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.FlowLayoutEditPolicy;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.AbstractBorderItemEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.l10n.DiagramColorRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.FillStyle;
import org.eclipse.gmf.runtime.notation.NotationPackage;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import port.PortData;
import port.PortIOType;

import task.diagram.edit.policies.PortDataItemSemanticEditPolicy;
import task.diagram.providers.TaskElementTypes;

/**
 * @generated
 */
public class PortDataEditPart extends AbstractBorderItemEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 3001;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public PortDataEditPart(View view) {
		super(view);
	}

	// added by boudinf
	protected void handleNotificationEvent(Notification notification) {
		if (notification.getNotifier() instanceof PortData) {
			System.out.println("handleNotificationEvent");
			try {

				PortData mObj = (PortData) ((View) PortDataEditPart.this
						.getModel()).getElement();

				if (((PortData) mObj).getDirection().equals(PortIOType.INPUT))
					getPrimaryShape().setBackgroundColor(ColorConstants.blue);
				else
					getPrimaryShape().setBackgroundColor(ColorConstants.red);

			} catch (Exception e) {
				System.out.println(e);
			}
		}
		super.handleNotificationEvent(notification);
	}

	// end boudinf

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE,
				getPrimaryDragEditPolicy());
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new PortDataItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		LayoutEditPolicy lep = new LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		return primaryShape = new Ellipse();
	}

	/**
	 * @generated
	 */
	public Ellipse getPrimaryShape() {
		return (Ellipse) primaryShape;
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(20, 20);

		//FIXME: workaround for #154536
		result.getBounds().setSize(result.getPreferredSize());
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated NOT
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {

			PortData mObj = (PortData) ((View) PortDataEditPart.this.getModel())
					.getElement();
			if (((PortData) mObj).getDirection().equals(PortIOType.INPUT))
				primaryShape.setBackgroundColor(ColorConstants.blue);
			else
				primaryShape.setBackgroundColor(ColorConstants.red);

			//primaryShape.setBackgroundColor(ColorConstants.red);
		}
	}

	/*@Override
	   protected void refreshBackgroundColor() {
	        FillStyle style = (FillStyle)getPrimaryView().getStyle(NotationPackage.Literals.FILL_STYLE);
	        if ( style != null ) {
	            setBackgroundColor(DiagramColorRegistry.getInstance().getColor(new Integer(style.getFillColor())));
	        }
	    }*/

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/getMARelTypesOnSource() {
		List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/types = new ArrayList/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/();
		types.add(TaskElementTypes.LinkDriver_4001);
		types.add(TaskElementTypes.LinkData_4003);
		return types;
	}

	/**
	 * @generated
	 */
	public List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/getMARelTypesOnSourceAndTarget(
			IGraphicalEditPart targetEditPart) {
		List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/types = new ArrayList/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/();
		if (targetEditPart instanceof task.diagram.edit.parts.PortDataEditPart) {
			types.add(TaskElementTypes.LinkDriver_4001);
		}
		if (targetEditPart instanceof PortDriverEditPart) {
			types.add(TaskElementTypes.LinkDriver_4001);
		}
		if (targetEditPart instanceof PortDriver2EditPart) {
			types.add(TaskElementTypes.LinkDriver_4001);
		}
		if (targetEditPart instanceof task.diagram.edit.parts.PortDataEditPart) {
			types.add(TaskElementTypes.LinkData_4003);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/getMATypesForTarget(
			IElementType relationshipType) {
		List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/types = new ArrayList/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/();
		if (relationshipType == TaskElementTypes.LinkDriver_4001) {
			types.add(TaskElementTypes.PortData_3001);
		}
		if (relationshipType == TaskElementTypes.LinkDriver_4001) {
			types.add(TaskElementTypes.PortDriver_3004);
		}
		if (relationshipType == TaskElementTypes.LinkDriver_4001) {
			types.add(TaskElementTypes.PortDriver_3006);
		}
		if (relationshipType == TaskElementTypes.LinkData_4003) {
			types.add(TaskElementTypes.PortData_3001);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/getMARelTypesOnTarget() {
		List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/types = new ArrayList/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/();
		types.add(TaskElementTypes.LinkDriver_4001);
		types.add(TaskElementTypes.LinkData_4003);
		return types;
	}

	/**
	 * @generated
	 */
	public List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/getMATypesForSource(
			IElementType relationshipType) {
		List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/types = new ArrayList/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/();
		if (relationshipType == TaskElementTypes.LinkDriver_4001) {
			types.add(TaskElementTypes.PortData_3001);
		}
		if (relationshipType == TaskElementTypes.LinkDriver_4001) {
			types.add(TaskElementTypes.PortDriver_3004);
		}
		if (relationshipType == TaskElementTypes.LinkDriver_4001) {
			types.add(TaskElementTypes.PortDriver_3006);
		}
		if (relationshipType == TaskElementTypes.LinkData_4003) {
			types.add(TaskElementTypes.PortData_3001);
		}
		return types;
	}

}
