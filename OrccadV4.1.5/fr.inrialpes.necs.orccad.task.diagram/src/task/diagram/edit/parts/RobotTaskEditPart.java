package task.diagram.edit.parts;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import module.ModuleAlgorithmic;
import module.ModulePhysicalRobot;
import module.ModulePhysicalSensor;
import module.impl.ModuleAlgorithmicImpl;
import module.impl.ModuleFactoryImpl;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.AbstractEMFOperation;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.UnexecutableCommand;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.core.util.StringStatics;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DiagramDragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.ArrangeRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.RequestConstants;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.emf.type.core.commands.CreateElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import port.PortData;

import constraint.TemporalConstraint;

import task.LinkData;
import task.LinkDriver;
import task.LinkParam;
import task.RobotTask;
import task.TaskPackage;
import task.diagram.edit.commands.TaskCreateShortcutDecorationsCommand;

import task.diagram.edit.policies.RobotTaskCanonicalEditPolicy;
import task.diagram.edit.policies.RobotTaskItemSemanticEditPolicy;
import task.diagram.providers.TaskElementTypes;
import task.impl.RobotTaskImpl;

/**
 * @generated
 */
public class RobotTaskEditPart extends DiagramEditPart {

	/**
	 * @generated
	 */
	public final static String MODEL_ID = "Task"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 1000;

	/**
	 * @generated
	 */
	public RobotTaskEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new RobotTaskItemSemanticEditPolicy());
		installEditPolicy(EditPolicyRoles.CANONICAL_ROLE,
				new RobotTaskCanonicalEditPolicy());
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.POPUPBAR_ROLE);
	}

	private class TaskDiagramDnDEditPolicy extends DiagramDragDropEditPolicy {

		public Command getDropObjectsCommand(DropObjectsRequest request) {

			System.out.println("getDropObjectsCommand");

			List<?> objects = request.getObjects();

			System.out.println(objects.toString());

			if (objects.size() != 1) {
				System.out.println("objects.size() != 1");
				return UnexecutableCommand.INSTANCE;
			}

			Object element = objects.get(0);

			if (element == null || getHostView().getElement().equals(element)) {
				if (element == null)
					System.out
							.println("element == null || getHostView().getElement().equals(element)");
				else
					System.out.println(element.toString());
				return UnexecutableCommand.INSTANCE;
			}

			System.out.println("----> element = " + element.toString()
					+ "\n----> HostView = "
					+ getHostView().getElement().toString());

			try {

				if (getHostView().getElement() instanceof RobotTaskEditPart
						&& element instanceof ModuleAlgorithmicImpl) {
					RobotTask rt = (RobotTask) getHostView().getElement();

					rt.getModuleAlgorithmics().add((ModuleAlgorithmic) element);

				}
			} catch (Exception e) {
				System.out.println("Ta mere en string ! ! ! " + e);
			}

			ModuleFactoryImpl mfi = new ModuleFactoryImpl();
			String s = mfi.convertToString((EDataType) TaskPackage.eINSTANCE
					.getRobotTask_ModuleAlgorithmics().getEType(), element);

			System.out.println("Ta mere en string ! ! ! " + s);

			//TODO: check element in not already in view: Already handled
			//TODO: enable several notational element for the same semantic element
			List<CreateViewRequest.ViewDescriptor> descriptors = new ArrayList<CreateViewRequest.ViewDescriptor>();
			CreateViewRequest.ViewDescriptor viewDescriptor = new CreateViewRequest.ViewDescriptor(
					new EObjectAdapter((EObject) element), Node.class,
					getSemanticHintForEObject((EObject) element),
					getDiagramPreferencesHint());

			viewDescriptor.setPersisted(true);
			descriptors.add(viewDescriptor);
			Command res = null;
			if (!descriptors.isEmpty()) {
				res = createViewsAndRestoreRelatedLinks(request, descriptors);
			}
			if (res == null)
				return UnexecutableCommand.INSTANCE;
			res.setLabel("Create a new Modal Diagram Element");
			return res;
		}

		void AddMA(RobotTaskImpl rt, ModuleAlgorithmicImpl ma) {

		}

		private String getSemanticHintForEObject(EObject element) {

			System.out.println("getSemanticHintForEObject");

			if (element instanceof ModuleAlgorithmic) {
				return ((IHintedType) TaskElementTypes
						.getElementType(ModuleAlgorithmicEditPart.VISUAL_ID))
						.getSemanticHint();
			}

			if (element instanceof RobotTask) {
				return ((IHintedType) TaskElementTypes
						.getElementType(RobotTaskEditPart.VISUAL_ID))
						.getSemanticHint();
			}

			if (element instanceof ModulePhysicalRobot) {
				return ((IHintedType) TaskElementTypes
						.getElementType(ModulePhysicalRobotEditPart.VISUAL_ID))
						.getSemanticHint();
			}

			if (element instanceof ModulePhysicalSensor) {
				return ((IHintedType) TaskElementTypes
						.getElementType(ModulePhysicalSensorEditPart.VISUAL_ID))
						.getSemanticHint();
			}

			/*if (element instanceof MethodologyRule) {
				return ((IHintedType) TaskElementTypes
						.getElementType(fr.mopcom.modal.intentionDiagram.edit.parts.MethodologyRuleEditPart.VISUAL_ID))
						.getSemanticHint();
			}

			if (element instanceof ToolDefinition) {
				return ((IHintedType) TaskElementTypes.getElementType(fr.mopcom.modal.intentionDiagram.edit.parts.ToolDefinitionEditPart.VISUAL_ID)).getSemanticHint();
			}*/

			return null;
		}

		private TransactionalEditingDomain getEditingDomain() {

			System.out.println("getEditingDomain");

			if (getHost() instanceof IGraphicalEditPart) {
				return ((IGraphicalEditPart) getHost()).getEditingDomain();
			}
			return null;
		}

		private View getHostView() {
			return (View) (getHost().getModel());
		}

		private PreferencesHint getDiagramPreferencesHint() {
			return ((IGraphicalEditPart) getHost()).getDiagramPreferencesHint();
		}

		private Command createViewsAndRestoreRelatedLinks(
				DropObjectsRequest dropRequest,
				List<CreateViewRequest.ViewDescriptor> viewDescriptors) {

			System.out.println("createViewsAndRestoreRelatedLinks");

			CreateViewRequest createViewRequest = new CreateViewRequest(
					viewDescriptors);
			createViewRequest.setLocation(dropRequest.getLocation());
			Command createCommand = getHost().getCommand(createViewRequest);

			if (createCommand != null) {
				List result = (List) createViewRequest.getNewObject();
				dropRequest.setResult(result);

				createCommand.chain(new ICommandProxy(
						new RestoreRelatedLinksCommand(
								(DiagramEditPart) getHost(), result)));

				ArrangeRequest arrangeRequest = new ArrangeRequest(
						RequestConstants.REQ_ARRANGE_DEFERRED);
				arrangeRequest.setViewAdaptersToArrange(result);
				createCommand.chain(getHost().getCommand(arrangeRequest));
			}
			return createCommand;
		}
	}

	private class RestoreRelatedLinksCommand extends
			AbstractTransactionalCommand {
		private DiagramEditPart diagramEditPart;
		private List<?> adapters;

		public RestoreRelatedLinksCommand(DiagramEditPart diagramEditPart,
				List<?> selection) {
			super(diagramEditPart.getEditingDomain(),
					"Related Link Restoration", null);
			this.diagramEditPart = diagramEditPart;
			this.adapters = selection;
		}

		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
				IAdaptable info) throws ExecutionException {
			for (Object object : adapters) {
				if (object instanceof IAdaptable) {
					View view = (View) ((IAdaptable) object)
							.getAdapter(View.class);
					if (view != null)
						refreshRelatedLinks(view);
				} else if (object instanceof View)
					refreshRelatedLinks((View) object);
			}
			return CommandResult.newOKCommandResult();
		}

		private void refreshRelatedLinks(View view) {
			EObject model = view.getElement();
			List<EObject> possibleRelatedElements = new ArrayList<EObject>();
			for (Object child : diagramEditPart.getChildren()) {
				if (child instanceof ShapeNodeEditPart)
					possibleRelatedElements.add(((ShapeNodeEditPart) child)
							.resolveSemanticElement());
			}
			createRelatedLinks(view.getElement(), possibleRelatedElements);
		}

		private void createRelatedLinks(EObject element,
				List<EObject> possibleRelatedElements) {

			System.out.println("createRelatedLinks");
			//create missing edges between notational elements
			Resource r = element.eResource();

			if (element instanceof ModuleAlgorithmic)
				processRole((ModuleAlgorithmic) element, r);

			if (element instanceof RobotTask)
				processIntention((RobotTask) element, r);

			/*			if (element instanceof ModulePhysicalSensor)
			 processMethodologyRule((ModulePhysicalSensor) element, r);

			 if (element instanceof TemporalConstraint)
			 processToolDefinition((TemporalConstraint) element, r);*/

		}

		private void processRole(ModuleAlgorithmic element, Resource r) {
			System.out.println("processRole ModuleAlgorithmic");
			//process PortData
			List<PortData> listPortData1 = new ArrayList<PortData>();
			TreeIterator<EObject> itPortData = EcoreUtil.getAllContents(r,
					false);
			while (itPortData.hasNext()) {
				EObject o = itPortData.next();
				if (o instanceof PortData)
					listPortData1.add((PortData) o);
			}

			for (Object child : diagramEditPart.getChildren()) {
				if (child instanceof ConnectionNodeEditPart) {
					EObject o = ((ConnectionNodeEditPart) child)
							.resolveSemanticElement();
					if (o instanceof PortData)
						listPortData1.remove(o);
				}
			}

			/*			List<PortData> listPortData2 = new ArrayList<PortData>();
			 for (PortData e : listPortData1)
			 if (e.getSource() == element || e.getTarget() == element)
			 listPortData2.add(e);
			 listPortData1.clear();
			 listPortData1.addAll(listPortData2);
			 for (IntentionAssociation link : listPortData1)
			 createViewForIntentionAssociation(link);*/

		}

		private void processIntention(RobotTask element, Resource r) {
			System.out.println("processIntention RobotTask");
			//process IntentionAssociation links
			List<LinkData> listIntentionAssociation1 = new ArrayList<LinkData>();
			TreeIterator<EObject> itIntentionAssociation = EcoreUtil
					.getAllContents(r, false);
			while (itIntentionAssociation.hasNext()) {
				EObject o = itIntentionAssociation.next();
				if (o instanceof LinkData)
					listIntentionAssociation1.add((LinkData) o);
			}
			for (Object child : diagramEditPart.getChildren()) {
				if (child instanceof ConnectionNodeEditPart) {
					EObject o = ((ConnectionNodeEditPart) child)
							.resolveSemanticElement();
					if (o instanceof LinkData)
						listIntentionAssociation1.remove(o);
				}
			}

			List<LinkData> listIntentionAssociation2 = new ArrayList<LinkData>();
			for (LinkData e : listIntentionAssociation1)
				if (e.getSource() == element || e.getTarget() == element)
					listIntentionAssociation2.add(e);
			listIntentionAssociation1.clear();
			listIntentionAssociation1.addAll(listIntentionAssociation2);
			for (LinkData link : listIntentionAssociation1)
				createViewForIntentionAssociation(link);

			//process IntentionUsage links
			List<LinkDriver> listIntentionUsage1 = new ArrayList<LinkDriver>();
			TreeIterator<EObject> itIntentionUsage = EcoreUtil.getAllContents(
					r, false);
			while (itIntentionUsage.hasNext()) {
				EObject o = itIntentionUsage.next();
				if (o instanceof LinkDriver)
					listIntentionUsage1.add((LinkDriver) o);
			}
			for (Object child : diagramEditPart.getChildren()) {
				if (child instanceof ConnectionNodeEditPart) {
					EObject o = ((ConnectionNodeEditPart) child)
							.resolveSemanticElement();
					if (o instanceof LinkDriver)
						listIntentionUsage1.remove(o);
				}
			}

			List<LinkDriver> listIntentionUsage2 = new ArrayList<LinkDriver>();
			for (LinkDriver e : listIntentionUsage1)
				if (e.getSource() == element || e.getTarget() == element)
					listIntentionUsage2.add(e);
			listIntentionUsage1.clear();
			listIntentionUsage1.addAll(listIntentionUsage2);
			for (LinkDriver link : listIntentionUsage1)
				createViewForIntentionUsage(link);

			//process SatisfactionLink links
			List<LinkParam> listSatisfactionLink1 = new ArrayList<LinkParam>();
			TreeIterator<EObject> itSatisfactionLink = EcoreUtil
					.getAllContents(r, false);
			while (itSatisfactionLink.hasNext()) {
				EObject o = itSatisfactionLink.next();
				if (o instanceof LinkParam)
					listSatisfactionLink1.add((LinkParam) o);
			}
			for (Object child : diagramEditPart.getChildren()) {
				if (child instanceof ConnectionNodeEditPart) {
					EObject o = ((ConnectionNodeEditPart) child)
							.resolveSemanticElement();
					if (o instanceof LinkParam)
						listSatisfactionLink1.remove(o);
				}
			}

			List<LinkParam> listSatisfactionLink2 = new ArrayList<LinkParam>();
			for (LinkParam e : listSatisfactionLink1)
				if (e.getSource() == element || e.getTarget() == element)
					listSatisfactionLink2.add(e);
			listSatisfactionLink1.clear();
			listSatisfactionLink1.addAll(listSatisfactionLink2);
			for (LinkParam link : listSatisfactionLink1)
				createViewForSatisfactionLink(link);

		}

		/*private void processMethodologyRule(MethodologyRule element, Resource r) {

			//process IntentionUsage links
			List<IntentionUsage> listIntentionUsage1 = new ArrayList<IntentionUsage>();
			TreeIterator<EObject> itIntentionUsage = EcoreUtil.getAllContents(
					r, false);
			while (itIntentionUsage.hasNext()) {
				EObject o = itIntentionUsage.next();
				if (o instanceof IntentionUsage)
					listIntentionUsage1.add((IntentionUsage) o);
			}
			for (Object child : diagramEditPart.getChildren()) {
				if (child instanceof ConnectionNodeEditPart) {
					EObject o = ((ConnectionNodeEditPart) child)
					.resolveSemanticElement();
					if (o instanceof IntentionUsage)
						listIntentionUsage1.remove(o);
				}
			}

			List<IntentionUsage> listIntentionUsage2 = new ArrayList<IntentionUsage>();
			for (IntentionUsage e : listIntentionUsage1)
				if (e.getSource() == element || e.getTarget() == element)
					listIntentionUsage2.add(e);
			listIntentionUsage1.clear();
			listIntentionUsage1.addAll(listIntentionUsage2);
			for (IntentionUsage link : listIntentionUsage1)
				createViewForIntentionUsage(link);

		}

		private void processToolDefinition(ToolDefinition element, Resource r) {

			//process IntentionUsage links
			List<IntentionUsage> listIntentionUsage1 = new ArrayList<IntentionUsage>();
			TreeIterator<EObject> itIntentionUsage = EcoreUtil.getAllContents(
					r, false);
			while (itIntentionUsage.hasNext()) {
				EObject o = itIntentionUsage.next();
				if (o instanceof IntentionUsage)
					listIntentionUsage1.add((IntentionUsage) o);
			}
			for (Object child : diagramEditPart.getChildren()) {
				if (child instanceof ConnectionNodeEditPart) {
					EObject o = ((ConnectionNodeEditPart) child)
					.resolveSemanticElement();
					if (o instanceof IntentionUsage)
						listIntentionUsage1.remove(o);
				}
			}

			List<IntentionUsage> listIntentionUsage2 = new ArrayList<IntentionUsage>();
			for (IntentionUsage e : listIntentionUsage1)
				if (e.getSource() == element || e.getTarget() == element)
					listIntentionUsage2.add(e);
			listIntentionUsage1.clear();
			listIntentionUsage1.addAll(listIntentionUsage2);
			for (IntentionUsage link : listIntentionUsage1)
				createViewForIntentionUsage(link);

		}*/

		public void createViewForIntentionAssociation(LinkData link) {

			System.out.println("createViewForIntentionAssociation LinkData ");

			//retrieve source and target edit parts
			EditPart sourceEditPart = retrieveEditPartForEObject((EObject) link
					.getSource());
			EditPart targetEditPart = retrieveEditPartForEObject((EObject) link
					.getTarget());
			//continue if none of them are empty
			if (sourceEditPart == null || targetEditPart == null)
				return;
			//Create a connection view request using connection view descriptor
			String semanticHint = ((IHintedType) TaskElementTypes
					.getElementType(LinkDataEditPart.VISUAL_ID))
					.getSemanticHint();
			CreateConnectionViewRequest.ConnectionViewDescriptor descriptor = new CreateConnectionViewRequest.ConnectionViewDescriptor(
					new DiagramObjectAdapter(link), semanticHint,
					ViewUtil.APPEND, true, diagramEditPart
							.getDiagramPreferencesHint());
			CreateConnectionViewRequest request = new CreateConnectionViewRequest(
					descriptor);
			request.setType(RequestConstants.REQ_CONNECTION_START);
			request.setSourceEditPart(sourceEditPart);
			sourceEditPart.getCommand(request);
			request.setType(RequestConstants.REQ_CONNECTION_END);
			request.setTargetEditPart(targetEditPart);
			//retrieve corresponding command and execute it
			final Command cmd = targetEditPart.getCommand(request);
			if (cmd != null && cmd.canExecute()) {
				AbstractEMFOperation op = new AbstractEMFOperation(
						diagramEditPart.getEditingDomain(),
						StringStatics.BLANK, null) {

					@Override
					protected IStatus doExecute(IProgressMonitor monitor,
							IAdaptable info) throws ExecutionException {
						cmd.execute();
						return Status.OK_STATUS;
					}
				};
				try {
					op.execute(new NullProgressMonitor(), null);
				} catch (ExecutionException ex) {
					ex.printStackTrace();
				}
			}
		}

		public void createViewForIntentionUsage(LinkDriver link) {
			System.out.println("createViewForIntentionUsage LinkDriver ");
			//retrieve source and target edit parts
			EditPart sourceEditPart = retrieveEditPartForEObject((EObject) link
					.getSource());
			EditPart targetEditPart = retrieveEditPartForEObject((EObject) link
					.getTarget());
			//continue if none of them are empty
			if (sourceEditPart == null || targetEditPart == null)
				return;
			//Create a connection view request using connection view descriptor
			String semanticHint = ((IHintedType) TaskElementTypes
					.getElementType(LinkDriverEditPart.VISUAL_ID))
					.getSemanticHint();
			CreateConnectionViewRequest.ConnectionViewDescriptor descriptor = new CreateConnectionViewRequest.ConnectionViewDescriptor(
					new DiagramObjectAdapter(link), semanticHint,
					ViewUtil.APPEND, true, diagramEditPart
							.getDiagramPreferencesHint());
			CreateConnectionViewRequest request = new CreateConnectionViewRequest(
					descriptor);
			request.setType(RequestConstants.REQ_CONNECTION_START);
			request.setSourceEditPart(sourceEditPart);
			sourceEditPart.getCommand(request);
			request.setType(RequestConstants.REQ_CONNECTION_END);
			request.setTargetEditPart(targetEditPart);
			//retrieve corresponding command and execute it
			final Command cmd = targetEditPart.getCommand(request);
			if (cmd != null && cmd.canExecute()) {
				AbstractEMFOperation op = new AbstractEMFOperation(
						diagramEditPart.getEditingDomain(),
						StringStatics.BLANK, null) {

					@Override
					protected IStatus doExecute(IProgressMonitor monitor,
							IAdaptable info) throws ExecutionException {
						cmd.execute();
						return Status.OK_STATUS;
					}
				};
				try {
					op.execute(new NullProgressMonitor(), null);
				} catch (ExecutionException ex) {
					ex.printStackTrace();
				}
			}
		}

		public void createViewForSatisfactionLink(LinkParam link) {
			//retrieve source and target edit parts
			EditPart sourceEditPart = retrieveEditPartForEObject((EObject) link
					.getSource());
			EditPart targetEditPart = retrieveEditPartForEObject((EObject) link
					.getTarget());
			//continue if none of them are empty
			if (sourceEditPart == null || targetEditPart == null)
				return;
			//Create a connection view request using connection view descriptor
			String semanticHint = ((IHintedType) TaskElementTypes
					.getElementType(LinkParamEditPart.VISUAL_ID))
					.getSemanticHint();
			CreateConnectionViewRequest.ConnectionViewDescriptor descriptor = new CreateConnectionViewRequest.ConnectionViewDescriptor(
					new DiagramObjectAdapter(link), semanticHint,
					ViewUtil.APPEND, true, diagramEditPart
							.getDiagramPreferencesHint());
			CreateConnectionViewRequest request = new CreateConnectionViewRequest(
					descriptor);
			request.setType(RequestConstants.REQ_CONNECTION_START);
			request.setSourceEditPart(sourceEditPart);
			sourceEditPart.getCommand(request);
			request.setType(RequestConstants.REQ_CONNECTION_END);
			request.setTargetEditPart(targetEditPart);
			//retrieve corresponding command and execute it
			final Command cmd = targetEditPart.getCommand(request);
			if (cmd != null && cmd.canExecute()) {
				AbstractEMFOperation op = new AbstractEMFOperation(
						diagramEditPart.getEditingDomain(),
						StringStatics.BLANK, null) {

					@Override
					protected IStatus doExecute(IProgressMonitor monitor,
							IAdaptable info) throws ExecutionException {
						cmd.execute();
						return Status.OK_STATUS;
					}
				};
				try {
					op.execute(new NullProgressMonitor(), null);
				} catch (ExecutionException ex) {
					ex.printStackTrace();
				}
			}
		}

		private EditPart retrieveEditPartForEObject(EObject element) {
			for (Object child : diagramEditPart.getChildren()) {
				if (child instanceof ShapeNodeEditPart) {
					if (((ShapeNodeEditPart) child).resolveSemanticElement() == element)
						return (EditPart) child;
				}
			}
			return null;
		}

	}

	private class DiagramObjectAdapter implements IAdaptable {
		private EObject modelElement;
		private IElementType elementType;

		public DiagramObjectAdapter(EObject modelElement) {
			this.modelElement = modelElement;
			processElementType();
		}

		private void processElementType() {

			if (modelElement instanceof LinkData)
				elementType = TaskElementTypes
						.getElementType(LinkDataEditPart.VISUAL_ID);

			if (modelElement instanceof LinkDriver)
				elementType = TaskElementTypes
						.getElementType(LinkDriverEditPart.VISUAL_ID);

			if (modelElement instanceof LinkParam)
				elementType = TaskElementTypes
						.getElementType(LinkParamEditPart.VISUAL_ID);

		}

		@Override
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter))
				return elementType;
			return null;
		}

	}

	/**
	 * @author jmwright
	 *
	 */
	public class CreateChildContentsCommand extends
			AbstractTransactionalCommand {

		private GraphicalEditPart childElement;

		public CreateChildContentsCommand(GraphicalEditPart root,
				TransactionalEditingDomain editingDomain, View parentView) {
			super(editingDomain,
					"Create automatic children", getWorkspaceFiles(parentView)); //$NON-NLS-1$
			childElement = root;
		}

		/**
		 * Create the child contents using the root's editing domain and view
		 *
		 * @param root
		 */
		public CreateChildContentsCommand(GraphicalEditPart root) {
			this(root, root.getEditingDomain(), (View) root.getModel());
		}

		protected IProgressMonitor monitor;
		protected IAdaptable info;

		/**
		 * A helper method to help us execute lots of steps easily
		 *
		 * @param command the command to execute
		 * @throws ExecutionException
		 * @see doExecuteWithResult()
		 */
		protected void doExecute(ICommand command) throws ExecutionException {
			OperationHistoryFactory.getOperationHistory().execute(command,
					monitor, info);
		}

		protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
				IAdaptable info) throws ExecutionException {
			this.monitor = monitor;
			this.info = info;

			Assert.isTrue(childElement.getModel() instanceof View);

			// get all the nodes in the model tree
			final View rootView = (View) childElement.getModel();
			//LinkComponent rootObject = (LinkComponent) rootView.getElement();

			// create an element Event
			//CreateElementCommand cc = new CreateElementCommand(new CreateElementRequest(rootObject, Uml3ElementTypes.Event_1003));
			//doExecute(cc);		// exceptions will pass through

			//Event eo = (Event) cc.getNewElement();

			// another version: SetValueCommand sv = new SetValueCommand(new SetRequest(eo, eo.eClass().getEStructuralFeature("name"), "test"));
			//SetValueCommand sv = new SetValueCommand(new SetRequest(eo, ModelPackage.eINSTANCE.getAction_Name(), "test"));
			//doExecute(sv);

			return CommandResult.newOKCommandResult();
		}

	}

}
