package task.diagram.edit.commands;

import java.util.Iterator;

import module.ModuleAlgorithmic;
import module.ModulePhysical;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;

import port.PortData;
import port.PortDataDriver;
import port.PortDriver;
import port.PortIOType;
import task.LinkData;
import task.LinkDriver;
import task.RobotTask;
import task.TaskFactory;
import task.diagram.edit.policies.TaskBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class LinkDriverCreateCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final EObject source;

	/**
	 * @generated
	 */
	private final EObject target;

	/**
	 * @generated
	 */
	private final RobotTask container;

	/**
	 * @generated
	 */
	public LinkDriverCreateCommand(CreateRelationshipRequest request,
			EObject source, EObject target) {
		super(request.getLabel(), null, request);
		this.source = source;
		this.target = target;
		container = deduceContainer(source, target);
	}

	/**
	 * @generated NOT
	 */
	public boolean canExecute() {

		//added by boudinf
		if (target instanceof PortData
				&& ((PortData) target).getDirection().equals(PortIOType.INPUT)) {

			PortData targetPortData = (PortData) target;
			ModuleAlgorithmic ma = targetPortData.getContainer();
			RobotTask rt = (RobotTask) ma.eContainer();

			for (Iterator<LinkData> it = rt.getLinkDatas().iterator(); it
					.hasNext();) {
				LinkData ld = it.next();
				if (ld.getTarget().equals(target))
					return false;
			}

			for (Iterator<LinkDriver> it = rt.getLinkDrivers().iterator(); it
					.hasNext();) {
				LinkDriver ld = it.next();
				if (ld.getTarget().equals(target))
					return false;
			}

		}

		if (target instanceof PortDriver
				&& ((PortDriver) target).getDirection()
						.equals(PortIOType.INPUT)) {

			PortDriver targetPortDriver = (PortDriver) target;
			ModulePhysical ma = targetPortDriver.getContainer();
			RobotTask rt = (RobotTask) ma.eContainer();

			for (Iterator<LinkDriver> it = rt.getLinkDrivers().iterator(); it
					.hasNext();) {
				LinkDriver ld = it.next();
				if (ld.getTarget().equals(target))
					return false;
			}

		}

		//end boudinf

		if (source == null && target == null) {
			return false;
		}
		if (source != null && false == source instanceof PortDataDriver) {
			return false;
		}
		if (target != null && false == target instanceof PortDataDriver) {
			return false;
		}
		if (getSource() == null) {
			return true; // link creation is in progress; source is not defined yet
		}
		// target may be null here but it's possible to check constraint
		if (getContainer() == null) {
			return false;
		}
		return TaskBaseItemSemanticEditPolicy.LinkConstraints
				.canCreateLinkDriver_4001(getContainer(), getSource(),
						getTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in create link command"); //$NON-NLS-1$
		}

		LinkDriver newElement = TaskFactory.eINSTANCE.createLinkDriver();
		getContainer().getLinkDrivers().add(newElement);
		newElement.setSource(getSource());
		newElement.setTarget(getTarget());
		doConfigure(newElement, monitor, info);
		((CreateElementRequest) getRequest()).setNewElement(newElement);
		return CommandResult.newOKCommandResult(newElement);

	}

	/**
	 * @generated
	 */
	protected void doConfigure(LinkDriver newElement, IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		IElementType elementType = ((CreateElementRequest) getRequest())
				.getElementType();
		ConfigureRequest configureRequest = new ConfigureRequest(
				getEditingDomain(), newElement, elementType);
		configureRequest.setClientContext(((CreateElementRequest) getRequest())
				.getClientContext());
		configureRequest.addParameters(getRequest().getParameters());
		configureRequest.setParameter(CreateRelationshipRequest.SOURCE,
				getSource());
		configureRequest.setParameter(CreateRelationshipRequest.TARGET,
				getTarget());
		ICommand configureCommand = elementType
				.getEditCommand(configureRequest);
		if (configureCommand != null && configureCommand.canExecute()) {
			configureCommand.execute(monitor, info);
		}
	}

	/**
	 * @generated
	 */
	protected void setElementToEdit(EObject element) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @generated
	 */
	protected PortDataDriver getSource() {
		return (PortDataDriver) source;
	}

	/**
	 * @generated
	 */
	protected PortDataDriver getTarget() {
		return (PortDataDriver) target;
	}

	/**
	 * @generated
	 */
	public RobotTask getContainer() {
		return container;
	}

	/**
	 * Default approach is to traverse ancestors of the source to find instance of container.
	 * Modify with appropriate logic.
	 * @generated
	 */
	private static RobotTask deduceContainer(EObject source, EObject target) {
		// Find container element for the new link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null; element = element
				.eContainer()) {
			if (element instanceof RobotTask) {
				return (RobotTask) element;
			}
		}
		return null;
	}

}
