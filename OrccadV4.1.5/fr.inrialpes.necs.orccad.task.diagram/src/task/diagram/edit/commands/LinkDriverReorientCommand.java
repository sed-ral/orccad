package task.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import port.PortDataDriver;
import task.LinkDriver;
import task.RobotTask;
import task.diagram.edit.policies.TaskBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class LinkDriverReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public LinkDriverReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof LinkDriver) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof PortDataDriver && newEnd instanceof PortDataDriver)) {
			return false;
		}
		PortDataDriver target = getLink().getTarget();
		if (!(getLink().eContainer() instanceof RobotTask)) {
			return false;
		}
		RobotTask container = (RobotTask) getLink().eContainer();
		return TaskBaseItemSemanticEditPolicy.LinkConstraints
				.canExistLinkDriver_4001(container, getNewSource(), target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof PortDataDriver && newEnd instanceof PortDataDriver)) {
			return false;
		}
		PortDataDriver source = getLink().getSource();
		if (!(getLink().eContainer() instanceof RobotTask)) {
			return false;
		}
		RobotTask container = (RobotTask) getLink().eContainer();
		return TaskBaseItemSemanticEditPolicy.LinkConstraints
				.canExistLinkDriver_4001(container, source, getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setSource(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTarget(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected LinkDriver getLink() {
		return (LinkDriver) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected PortDataDriver getOldSource() {
		return (PortDataDriver) oldEnd;
	}

	/**
	 * @generated
	 */
	protected PortDataDriver getNewSource() {
		return (PortDataDriver) newEnd;
	}

	/**
	 * @generated
	 */
	protected PortDataDriver getOldTarget() {
		return (PortDataDriver) oldEnd;
	}

	/**
	 * @generated
	 */
	protected PortDataDriver getNewTarget() {
		return (PortDataDriver) newEnd;
	}
}
