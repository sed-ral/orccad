package task.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

import task.diagram.edit.commands.ModuleAlgorithmicCreateCommand;
import task.diagram.edit.commands.ModulePhysicalRobotCreateCommand;
import task.diagram.edit.commands.ModulePhysicalSensorCreateCommand;
import task.diagram.edit.commands.TemporalConstraintCreateCommand;
import task.diagram.providers.TaskElementTypes;

/**
 * @generated
 */
public class RobotTaskItemSemanticEditPolicy extends
		TaskBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RobotTaskItemSemanticEditPolicy() {
		super(TaskElementTypes.RobotTask_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (TaskElementTypes.ModuleAlgorithmic_2001 == req.getElementType()) {
			return getGEFWrapper(new ModuleAlgorithmicCreateCommand(req));
		}
		if (TaskElementTypes.ModulePhysicalRobot_2002 == req.getElementType()) {
			return getGEFWrapper(new ModulePhysicalRobotCreateCommand(req));
		}
		if (TaskElementTypes.ModulePhysicalSensor_2003 == req.getElementType()) {
			return getGEFWrapper(new ModulePhysicalSensorCreateCommand(req));
		}
		if (TaskElementTypes.TemporalConstraint_2004 == req.getElementType()) {
			return getGEFWrapper(new TemporalConstraintCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
