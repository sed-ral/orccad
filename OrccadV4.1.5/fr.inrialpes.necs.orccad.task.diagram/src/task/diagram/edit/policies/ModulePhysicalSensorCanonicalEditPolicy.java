package task.diagram.edit.policies;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import module.ModulePackage;

import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.notation.View;

import task.diagram.edit.parts.EventCondition3EditPart;

import task.diagram.edit.parts.PortDriver2EditPart;
import task.diagram.edit.parts.PortParam3EditPart;

import task.diagram.part.TaskDiagramUpdater;
import task.diagram.part.TaskNodeDescriptor;
import task.diagram.part.TaskVisualIDRegistry;

/**
 * @generated
 */
public class ModulePhysicalSensorCanonicalEditPolicy extends
		CanonicalEditPolicy {

	/**
	 * @generated
	 */
	Set myFeaturesToSynchronize;

	/**
	 * @generated
	 */
	protected List getSemanticChildrenList() {
		View viewObject = (View) getHost().getModel();
		List result = new LinkedList();
		for (Iterator it = TaskDiagramUpdater
				.getModulePhysicalSensor_2003SemanticChildren(viewObject)
				.iterator(); it.hasNext();) {
			result.add(((TaskNodeDescriptor) it.next()).getModelElement());
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected boolean isOrphaned(Collection semanticChildren, final View view) {
		int visualID = TaskVisualIDRegistry.getVisualID(view);
		switch (visualID) {
		case PortParam3EditPart.VISUAL_ID:
		case PortDriver2EditPart.VISUAL_ID:
		case EventCondition3EditPart.VISUAL_ID:
			if (!semanticChildren.contains(view.getElement())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected String getDefaultFactoryHint() {
		return null;
	}

	/**
	 * @generated
	 */
	protected Set getFeaturesToSynchronize() {
		if (myFeaturesToSynchronize == null) {
			myFeaturesToSynchronize = new HashSet();
			myFeaturesToSynchronize.add(ModulePackage.eINSTANCE
					.getModuleGeneric_PortParams());
			myFeaturesToSynchronize.add(ModulePackage.eINSTANCE
					.getModulePhysical_PortDrivers());
			myFeaturesToSynchronize.add(ModulePackage.eINSTANCE
					.getModuleGeneric_EventConditions());
		}
		return myFeaturesToSynchronize;
	}

}
