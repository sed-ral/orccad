package task.diagram.part;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteDrawer;
import org.eclipse.gef.palette.PaletteEntry;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.PaletteStack;
import org.eclipse.gef.palette.PanningSelectionToolEntry;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;

import task.diagram.providers.TaskElementTypes;

/**
 * @generated
 */
public class TaskPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		cleanStandardTools(paletteRoot);
		paletteRoot.add(createLink1Group());
		paletteRoot.add(createPort2Group());
		paletteRoot.add(createModule3Group());
		paletteRoot.add(createAutres4Group());
		paletteRoot.add(createEvent5Group());
		paletteRoot.add(createTemporalConstraint6Group());
	}

	/**
	 * Workaround for https://bugs.eclipse.org/bugs/show_bug.cgi?id=159289
	 * @generated
	 */
	private void cleanStandardTools(PaletteRoot paletteRoot) {
		for (Iterator it = paletteRoot.getChildren().iterator(); it.hasNext();) {
			PaletteEntry entry = (PaletteEntry) it.next();
			if (!"standardGroup".equals(entry.getId())) { //$NON-NLS-1$
				continue;
			}
			for (Iterator it2 = ((PaletteContainer) entry).getChildren()
					.iterator(); it2.hasNext();) {
				PaletteEntry entry2 = (PaletteEntry) it2.next();
				if ("zoomTool".equals(entry2.getId())) { //$NON-NLS-1$
					it2.remove();
				} else if ("noteStack".equals(entry2.getId())) { //$NON-NLS-1$
					it2.remove();
				} else if ("selectionTool".equals(entry2.getId())) { //$NON-NLS-1$
					it2.remove();
				}
				if (paletteRoot.getDefaultEntry() == entry2) {
					paletteRoot.setDefaultEntry(null);
				}
			}
		}
	}

	/**
	 * Creates "Link" palette tool group
	 * @generated
	 */
	private PaletteContainer createLink1Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Link1Group_title);
		paletteContainer.setId("createLink1Group"); //$NON-NLS-1$
		paletteContainer.setDescription(Messages.Link1Group_desc);
		paletteContainer.add(createLinkDriver1CreationTool());
		paletteContainer.add(createLinkParam2CreationTool());
		paletteContainer.add(createLinkData3CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Port" palette tool group
	 * @generated
	 */
	private PaletteContainer createPort2Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Port2Group_title);
		paletteContainer.setId("createPort2Group"); //$NON-NLS-1$
		paletteContainer.add(createPortData1CreationTool());
		paletteContainer.add(createPortDriver2CreationTool());
		paletteContainer.add(createPortParam3CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Module" palette tool group
	 * @generated
	 */
	private PaletteContainer createModule3Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Module3Group_title);
		paletteContainer.setId("createModule3Group"); //$NON-NLS-1$
		paletteContainer.setDescription(Messages.Module3Group_desc);
		paletteContainer.add(createModuleAlgo1CreationTool());
		paletteContainer.add(createPhysicalRobot2CreationTool());
		paletteContainer.add(createPhysicalSensor3CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Autres" palette tool group
	 * @generated
	 */
	private PaletteContainer createAutres4Group() {
		PaletteGroup paletteContainer = new PaletteGroup(
				Messages.Autres4Group_title);
		paletteContainer.setId("createAutres4Group"); //$NON-NLS-1$
		paletteContainer.add(createZoominTool());
		return paletteContainer;
	}

	/**
	 * Creates "Event" palette tool group
	 * @generated
	 */
	private PaletteContainer createEvent5Group() {
		PaletteGroup paletteContainer = new PaletteGroup(
				Messages.Event5Group_title);
		paletteContainer.setId("createEvent5Group"); //$NON-NLS-1$
		paletteContainer.setDescription(Messages.Event5Group_desc);
		paletteContainer.add(createException1Group());
		paletteContainer.add(createCondition2Group());
		return paletteContainer;
	}

	/**
	 * Creates "Temporal Constraint" palette tool group
	 * @generated
	 */
	private PaletteContainer createTemporalConstraint6Group() {
		PaletteGroup paletteContainer = new PaletteGroup(
				Messages.TemporalConstraint6Group_title);
		paletteContainer.setId("createTemporalConstraint6Group"); //$NON-NLS-1$
		paletteContainer.add(createTemporalConstraint1CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Exception" palette tool group
	 * @generated
	 */
	private PaletteContainer createException1Group() {
		PaletteDrawer paletteContainer = new PaletteDrawer(
				Messages.Exception1Group_title);
		paletteContainer.setId("createException1Group"); //$NON-NLS-1$
		paletteContainer.add(createT11CreationTool());
		paletteContainer.add(createT22CreationTool());
		paletteContainer.add(createT33CreationTool());
		return paletteContainer;
	}

	/**
	 * Creates "Condition" palette tool group
	 * @generated
	 */
	private PaletteContainer createCondition2Group() {
		PaletteGroup paletteContainer = new PaletteGroup(
				Messages.Condition2Group_title);
		paletteContainer.setId("createCondition2Group"); //$NON-NLS-1$
		paletteContainer.setDescription(Messages.Condition2Group_desc);
		paletteContainer.add(createCondition1CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createLinkDriver1CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(TaskElementTypes.LinkDriver_4001);
		LinkToolEntry entry = new LinkToolEntry(
				Messages.LinkDriver1CreationTool_title,
				Messages.LinkDriver1CreationTool_desc, types);
		entry.setId("createLinkDriver1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.LinkDriver_4001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createLinkParam2CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(TaskElementTypes.LinkParam_4002);
		LinkToolEntry entry = new LinkToolEntry(
				Messages.LinkParam2CreationTool_title,
				Messages.LinkParam2CreationTool_desc, types);
		entry.setId("createLinkParam2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.LinkParam_4002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createLinkData3CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(TaskElementTypes.LinkData_4003);
		LinkToolEntry entry = new LinkToolEntry(
				Messages.LinkData3CreationTool_title,
				Messages.LinkData3CreationTool_desc, types);
		entry.setId("createLinkData3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.LinkData_4003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createPortData1CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(TaskElementTypes.PortData_3001);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.PortData1CreationTool_title, null, types);
		entry.setId("createPortData1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.PortData_3001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createPortDriver2CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(2);
		types.add(TaskElementTypes.PortDriver_3004);
		types.add(TaskElementTypes.PortDriver_3006);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.PortDriver2CreationTool_title, null, types);
		entry.setId("createPortDriver2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.PortDriver_3004));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createPortParam3CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(3);
		types.add(TaskElementTypes.PortParam_3002);
		types.add(TaskElementTypes.PortParam_3003);
		types.add(TaskElementTypes.PortParam_3005);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.PortParam3CreationTool_title, null, types);
		entry.setId("createPortParam3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.PortParam_3002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createModuleAlgo1CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(TaskElementTypes.ModuleAlgorithmic_2001);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.ModuleAlgo1CreationTool_title, null, types);
		entry.setId("createModuleAlgo1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.ModuleAlgorithmic_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createPhysicalRobot2CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(TaskElementTypes.ModulePhysicalRobot_2002);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.PhysicalRobot2CreationTool_title,
				Messages.PhysicalRobot2CreationTool_desc, types);
		entry.setId("createPhysicalRobot2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.ModulePhysicalRobot_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createPhysicalSensor3CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(TaskElementTypes.ModulePhysicalSensor_2003);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.PhysicalSensor3CreationTool_title,
				Messages.PhysicalSensor3CreationTool_desc, types);
		entry.setId("createPhysicalSensor3CreationTool"); //$NON-NLS-1$
		entry
				.setSmallIcon(TaskElementTypes
						.getImageDescriptor(TaskElementTypes.ModulePhysicalSensor_2003));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createZoominTool() {
		PanningSelectionToolEntry entry = new PanningSelectionToolEntry();
		entry.setId("createZoominTool"); //$NON-NLS-1$
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createT11CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(2);
		types.add(TaskElementTypes.ExceptionT1_3008);
		types.add(TaskElementTypes.ExceptionT2_3012);
		NodeToolEntry entry = new NodeToolEntry(Messages.T11CreationTool_title,
				null, types);
		entry.setId("createT11CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.ExceptionT1_3008));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createT22CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(2);
		types.add(TaskElementTypes.ExceptionT2_3009);
		types.add(TaskElementTypes.ExceptionT1_3011);
		NodeToolEntry entry = new NodeToolEntry(Messages.T22CreationTool_title,
				null, types);
		entry.setId("createT22CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.ExceptionT2_3009));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createT33CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(2);
		types.add(TaskElementTypes.ExceptionT3_3007);
		types.add(TaskElementTypes.ExceptionT3_3010);
		NodeToolEntry entry = new NodeToolEntry(Messages.T33CreationTool_title,
				null, types);
		entry.setId("createT33CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.ExceptionT3_3007));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCondition1CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(3);
		types.add(TaskElementTypes.EventCondition_3018);
		types.add(TaskElementTypes.EventCondition_3019);
		types.add(TaskElementTypes.EventCondition_3020);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Condition1CreationTool_title, null, types);
		entry.setId("createCondition1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.EventCondition_3018));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createTemporalConstraint1CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(TaskElementTypes.TemporalConstraint_2004);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.TemporalConstraint1CreationTool_title,
				Messages.TemporalConstraint1CreationTool_desc, types);
		entry.setId("createTemporalConstraint1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(TaskElementTypes
				.getImageDescriptor(TaskElementTypes.TemporalConstraint_2004));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
