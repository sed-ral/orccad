package task.diagram.part;

import module.ModulePackage;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;

import port.PortPackage;
import task.LinkData;
import task.LinkDriver;
import task.RobotTask;
import task.TaskPackage;
import task.diagram.edit.parts.EventCondition2EditPart;
import task.diagram.edit.parts.EventCondition3EditPart;

import task.diagram.edit.parts.EventConditionEditPart;
import task.diagram.edit.parts.EventConditionEventName2EditPart;
import task.diagram.edit.parts.EventConditionEventName3EditPart;

import task.diagram.edit.parts.EventConditionEventNameEditPart;
import task.diagram.edit.parts.ExceptionT12EditPart;

import task.diagram.edit.parts.ExceptionT1EditPart;
import task.diagram.edit.parts.ExceptionT22EditPart;

import task.diagram.edit.parts.ExceptionT2EditPart;
import task.diagram.edit.parts.ExceptionT32EditPart;

import task.diagram.edit.parts.ExceptionT3EditPart;

import task.diagram.edit.parts.LinkDataEditPart;
import task.diagram.edit.parts.LinkDriverEditPart;
import task.diagram.edit.parts.LinkParamEditPart;

import task.diagram.edit.parts.ModuleAlgorithmicEditPart;

import task.diagram.edit.parts.ModuleAlgorithmicNameEditPart;
import task.diagram.edit.parts.ModulePhysicalRobotEditPart;
import task.diagram.edit.parts.ModulePhysicalRobotNameEditPart;
import task.diagram.edit.parts.ModulePhysicalSensorEditPart;
import task.diagram.edit.parts.ModulePhysicalSensorNameEditPart;

import task.diagram.edit.parts.PortDataEditPart;
import task.diagram.edit.parts.PortDriver2EditPart;
import task.diagram.edit.parts.PortDriverEditPart;
import task.diagram.edit.parts.PortParam2EditPart;
import task.diagram.edit.parts.PortParam3EditPart;

import task.diagram.edit.parts.PortParamEditPart;
import task.diagram.edit.parts.RobotTaskEditPart;
import task.diagram.edit.parts.TemporalConstraintEditPart;
import task.diagram.edit.parts.TemporalConstraintNameEditPart;
import task.diagram.expressions.TaskAbstractExpression;
import task.diagram.expressions.TaskOCLFactory;
import constraint.ConstraintPackage;
import constraint.TemporalConstraint;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class TaskVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "fr.inrialpes.necs.orccad.task.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	private static TaskAbstractExpression TemporalConstraint_2004_Constraint;

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (RobotTaskEditPart.MODEL_ID.equals(view.getType())) {
				return RobotTaskEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return task.diagram.part.TaskVisualIDRegistry.getVisualID(view
				.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				TaskDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return String.valueOf(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (TaskPackage.eINSTANCE.getRobotTask().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((RobotTask) domainElement)) {
			return RobotTaskEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = task.diagram.part.TaskVisualIDRegistry
				.getModelID(containerView);
		if (!RobotTaskEditPart.MODEL_ID.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (RobotTaskEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = task.diagram.part.TaskVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = RobotTaskEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case ModuleAlgorithmicEditPart.VISUAL_ID:
			if (PortPackage.eINSTANCE.getPortData().isSuperTypeOf(
					domainElement.eClass())) {
				return PortDataEditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getPortParam().isSuperTypeOf(
					domainElement.eClass())) {
				return PortParamEditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getExceptionT3().isSuperTypeOf(
					domainElement.eClass())) {
				return ExceptionT3EditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getExceptionT1().isSuperTypeOf(
					domainElement.eClass())) {
				return ExceptionT1EditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getExceptionT2().isSuperTypeOf(
					domainElement.eClass())) {
				return ExceptionT2EditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getEventCondition().isSuperTypeOf(
					domainElement.eClass())) {
				return EventConditionEditPart.VISUAL_ID;
			}
			break;
		case ModulePhysicalRobotEditPart.VISUAL_ID:
			if (PortPackage.eINSTANCE.getPortParam().isSuperTypeOf(
					domainElement.eClass())) {
				return PortParam2EditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getPortDriver().isSuperTypeOf(
					domainElement.eClass())) {
				return PortDriverEditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getExceptionT3().isSuperTypeOf(
					domainElement.eClass())) {
				return ExceptionT32EditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getExceptionT1().isSuperTypeOf(
					domainElement.eClass())) {
				return ExceptionT12EditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getExceptionT2().isSuperTypeOf(
					domainElement.eClass())) {
				return ExceptionT22EditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getEventCondition().isSuperTypeOf(
					domainElement.eClass())) {
				return EventCondition2EditPart.VISUAL_ID;
			}
			break;
		case ModulePhysicalSensorEditPart.VISUAL_ID:
			if (PortPackage.eINSTANCE.getPortParam().isSuperTypeOf(
					domainElement.eClass())) {
				return PortParam3EditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getPortDriver().isSuperTypeOf(
					domainElement.eClass())) {
				return PortDriver2EditPart.VISUAL_ID;
			}
			if (PortPackage.eINSTANCE.getEventCondition().isSuperTypeOf(
					domainElement.eClass())) {
				return EventCondition3EditPart.VISUAL_ID;
			}
			break;
		case RobotTaskEditPart.VISUAL_ID:
			if (ModulePackage.eINSTANCE.getModuleAlgorithmic().isSuperTypeOf(
					domainElement.eClass())) {
				return ModuleAlgorithmicEditPart.VISUAL_ID;
			}
			if (ModulePackage.eINSTANCE.getModulePhysicalRobot().isSuperTypeOf(
					domainElement.eClass())) {
				return ModulePhysicalRobotEditPart.VISUAL_ID;
			}
			if (ModulePackage.eINSTANCE.getModulePhysicalSensor()
					.isSuperTypeOf(domainElement.eClass())) {
				return ModulePhysicalSensorEditPart.VISUAL_ID;
			}
			if (ConstraintPackage.eINSTANCE.getTemporalConstraint()
					.isSuperTypeOf(domainElement.eClass())
					&& isTemporalConstraint_2004((TemporalConstraint) domainElement)) {
				return TemporalConstraintEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = task.diagram.part.TaskVisualIDRegistry
				.getModelID(containerView);
		if (!RobotTaskEditPart.MODEL_ID.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (RobotTaskEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = task.diagram.part.TaskVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = RobotTaskEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case ModuleAlgorithmicEditPart.VISUAL_ID:
			if (ModuleAlgorithmicNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (PortDataEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (PortParamEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ExceptionT3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ExceptionT1EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ExceptionT2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (EventConditionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ModulePhysicalRobotEditPart.VISUAL_ID:
			if (ModulePhysicalRobotNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (PortParam2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (PortDriverEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ExceptionT32EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ExceptionT12EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ExceptionT22EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (EventCondition2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ModulePhysicalSensorEditPart.VISUAL_ID:
			if (ModulePhysicalSensorNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (PortParam3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (PortDriver2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (EventCondition3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case TemporalConstraintEditPart.VISUAL_ID:
			if (TemporalConstraintNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case EventConditionEditPart.VISUAL_ID:
			if (EventConditionEventNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case EventCondition2EditPart.VISUAL_ID:
			if (EventConditionEventName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case EventCondition3EditPart.VISUAL_ID:
			if (EventConditionEventName3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotTaskEditPart.VISUAL_ID:
			if (ModuleAlgorithmicEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ModulePhysicalRobotEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ModulePhysicalSensorEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (TemporalConstraintEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (TaskPackage.eINSTANCE.getLinkDriver().isSuperTypeOf(
				domainElement.eClass())) {
			return LinkDriverEditPart.VISUAL_ID;
		}
		if (TaskPackage.eINSTANCE.getLinkParam().isSuperTypeOf(
				domainElement.eClass())) {
			return LinkParamEditPart.VISUAL_ID;
		}
		if (TaskPackage.eINSTANCE.getLinkData().isSuperTypeOf(
				domainElement.eClass())) {
			return LinkDataEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(RobotTask element) {
		return true;
	}

	/**
	 * @generated
	 */
	private static boolean isTemporalConstraint_2004(
			TemporalConstraint domainElement) {
		if (TemporalConstraint_2004_Constraint == null) { // lazy initialization
			TemporalConstraint_2004_Constraint = TaskOCLFactory
					.getExpression(
							"self.periodTime > 0", ConstraintPackage.eINSTANCE.getTemporalConstraint()); //$NON-NLS-1$
		}
		Object result = TemporalConstraint_2004_Constraint
				.evaluate(domainElement);
		return result instanceof Boolean && ((Boolean) result).booleanValue();
	}

}
