package task.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String TaskCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String TaskCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TaskCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TaskCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TaskCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TaskCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String TaskCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String TaskCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String TaskDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String TaskDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String TaskDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String TaskDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String TaskDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String TaskDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String TaskDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String TaskDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String TaskDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String TaskDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String TaskDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String TaskDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String TaskDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String TaskNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String TaskNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String TaskNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String TaskNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String TaskNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String TaskNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String TaskNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String TaskNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String TaskNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String TaskNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String TaskNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String TaskDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String TaskDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String TaskDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String TaskDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String TaskDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String TaskElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Link1Group_title;

	/**
	 * @generated
	 */
	public static String Link1Group_desc;

	/**
	 * @generated
	 */
	public static String Port2Group_title;

	/**
	 * @generated
	 */
	public static String Module3Group_title;

	/**
	 * @generated
	 */
	public static String Module3Group_desc;

	/**
	 * @generated
	 */
	public static String Autres4Group_title;

	/**
	 * @generated
	 */
	public static String Event5Group_title;

	/**
	 * @generated
	 */
	public static String Event5Group_desc;

	/**
	 * @generated
	 */
	public static String TemporalConstraint6Group_title;

	/**
	 * @generated
	 */
	public static String Exception1Group_title;

	/**
	 * @generated
	 */
	public static String Condition2Group_title;

	/**
	 * @generated
	 */
	public static String Condition2Group_desc;

	/**
	 * @generated
	 */
	public static String LinkDriver1CreationTool_title;

	/**
	 * @generated
	 */
	public static String LinkDriver1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String LinkParam2CreationTool_title;

	/**
	 * @generated
	 */
	public static String LinkParam2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String LinkData3CreationTool_title;

	/**
	 * @generated
	 */
	public static String LinkData3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String PortData1CreationTool_title;

	/**
	 * @generated
	 */
	public static String PortDriver2CreationTool_title;

	/**
	 * @generated
	 */
	public static String PortParam3CreationTool_title;

	/**
	 * @generated
	 */
	public static String ModuleAlgo1CreationTool_title;

	/**
	 * @generated
	 */
	public static String PhysicalRobot2CreationTool_title;

	/**
	 * @generated
	 */
	public static String PhysicalRobot2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String PhysicalSensor3CreationTool_title;

	/**
	 * @generated
	 */
	public static String PhysicalSensor3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String ZoominTool_title;

	/**
	 * @generated
	 */
	public static String T11CreationTool_title;

	/**
	 * @generated
	 */
	public static String T22CreationTool_title;

	/**
	 * @generated
	 */
	public static String T33CreationTool_title;

	/**
	 * @generated
	 */
	public static String Condition1CreationTool_title;

	/**
	 * @generated
	 */
	public static String TemporalConstraint1CreationTool_title;

	/**
	 * @generated
	 */
	public static String TemporalConstraint1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotTask_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_PortData_3001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_PortData_3001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_PortParam_3002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ExceptionT1_3008_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_PortParam_3003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_PortDriver_3004_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_PortDriver_3004_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_ExceptionT1_3011_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_PortParam_3005_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_PortDriver_3006_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_PortDriver_3006_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LinkDriver_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LinkDriver_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LinkParam_4002_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LinkParam_4002_source;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LinkData_4003_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LinkData_4003_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnexpectedValueType;

	/**
	 * @generated
	 */
	public static String AbstractParser_WrongStringConversion;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnknownLiteral;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String TaskModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String TaskModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
