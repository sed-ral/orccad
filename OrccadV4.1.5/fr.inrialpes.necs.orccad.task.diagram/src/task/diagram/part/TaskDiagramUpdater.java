package task.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import module.ModuleAlgorithmic;
import module.ModulePhysicalRobot;
import module.ModulePhysicalSensor;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;

import port.EventCondition;
import port.ExceptionT1;
import port.ExceptionT2;
import port.ExceptionT3;
import port.PortData;
import port.PortDataDriver;
import port.PortDriver;
import port.PortParam;
import task.LinkData;
import task.LinkDriver;
import task.LinkParam;
import task.RobotTask;
import task.TaskPackage;
import task.diagram.edit.parts.EventCondition2EditPart;
import task.diagram.edit.parts.EventCondition3EditPart;

import task.diagram.edit.parts.EventConditionEditPart;
import task.diagram.edit.parts.ExceptionT12EditPart;

import task.diagram.edit.parts.ExceptionT1EditPart;
import task.diagram.edit.parts.ExceptionT22EditPart;

import task.diagram.edit.parts.ExceptionT2EditPart;
import task.diagram.edit.parts.ExceptionT32EditPart;

import task.diagram.edit.parts.ExceptionT3EditPart;

import task.diagram.edit.parts.LinkDataEditPart;
import task.diagram.edit.parts.LinkDriverEditPart;
import task.diagram.edit.parts.LinkParamEditPart;

import task.diagram.edit.parts.ModuleAlgorithmicEditPart;
import task.diagram.edit.parts.ModulePhysicalRobotEditPart;
import task.diagram.edit.parts.ModulePhysicalSensorEditPart;

import task.diagram.edit.parts.PortDataEditPart;
import task.diagram.edit.parts.PortDriver2EditPart;
import task.diagram.edit.parts.PortDriverEditPart;
import task.diagram.edit.parts.PortParam2EditPart;
import task.diagram.edit.parts.PortParam3EditPart;

import task.diagram.edit.parts.PortParamEditPart;
import task.diagram.edit.parts.RobotTaskEditPart;
import task.diagram.edit.parts.TemporalConstraintEditPart;
import task.diagram.providers.TaskElementTypes;
import constraint.TemporalConstraint;

/**
 * @generated
 */
public class TaskDiagramUpdater {

	/**
	 * @generated
	 */
	public static List getSemanticChildren(View view) {
		switch (TaskVisualIDRegistry.getVisualID(view)) {
		case ModuleAlgorithmicEditPart.VISUAL_ID:
			return getModuleAlgorithmic_2001SemanticChildren(view);
		case ModulePhysicalRobotEditPart.VISUAL_ID:
			return getModulePhysicalRobot_2002SemanticChildren(view);
		case ModulePhysicalSensorEditPart.VISUAL_ID:
			return getModulePhysicalSensor_2003SemanticChildren(view);
		case RobotTaskEditPart.VISUAL_ID:
			return getRobotTask_1000SemanticChildren(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModuleAlgorithmic_2001SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		ModuleAlgorithmic modelElement = (ModuleAlgorithmic) view.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getPortDatas().iterator(); it.hasNext();) {
			PortData childElement = (PortData) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == PortDataEditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getPortParams().iterator(); it
				.hasNext();) {
			PortParam childElement = (PortParam) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == PortParamEditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getEventT3s().iterator(); it.hasNext();) {
			ExceptionT3 childElement = (ExceptionT3) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ExceptionT3EditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getEventT1s().iterator(); it.hasNext();) {
			ExceptionT1 childElement = (ExceptionT1) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ExceptionT1EditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getEventT2s().iterator(); it.hasNext();) {
			ExceptionT2 childElement = (ExceptionT2) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ExceptionT2EditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getEventConditions().iterator(); it
				.hasNext();) {
			EventCondition childElement = (EventCondition) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == EventConditionEditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getModulePhysicalRobot_2002SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		ModulePhysicalRobot modelElement = (ModulePhysicalRobot) view
				.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getPortParams().iterator(); it
				.hasNext();) {
			PortParam childElement = (PortParam) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == PortParam2EditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getPortDrivers().iterator(); it
				.hasNext();) {
			PortDriver childElement = (PortDriver) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == PortDriverEditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getEventT3s().iterator(); it.hasNext();) {
			ExceptionT3 childElement = (ExceptionT3) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ExceptionT32EditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getEventT1s().iterator(); it.hasNext();) {
			ExceptionT1 childElement = (ExceptionT1) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ExceptionT12EditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getEventT2s().iterator(); it.hasNext();) {
			ExceptionT2 childElement = (ExceptionT2) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ExceptionT22EditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getEventConditions().iterator(); it
				.hasNext();) {
			EventCondition childElement = (EventCondition) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == EventCondition2EditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getModulePhysicalSensor_2003SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		ModulePhysicalSensor modelElement = (ModulePhysicalSensor) view
				.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getPortParams().iterator(); it
				.hasNext();) {
			PortParam childElement = (PortParam) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == PortParam3EditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getPortDrivers().iterator(); it
				.hasNext();) {
			PortDriver childElement = (PortDriver) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == PortDriver2EditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getEventConditions().iterator(); it
				.hasNext();) {
			EventCondition childElement = (EventCondition) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == EventCondition3EditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotTask_1000SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		RobotTask modelElement = (RobotTask) view.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getModuleAlgorithmics().iterator(); it
				.hasNext();) {
			ModuleAlgorithmic childElement = (ModuleAlgorithmic) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ModuleAlgorithmicEditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		{
			ModulePhysicalRobot childElement = modelElement
					.getModulePhysicalRobot();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ModulePhysicalRobotEditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
			}
		}
		for (Iterator it = modelElement.getModulePhysicalSensors().iterator(); it
				.hasNext();) {
			ModulePhysicalSensor childElement = (ModulePhysicalSensor) it
					.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ModulePhysicalSensorEditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		for (Iterator it = modelElement.getTemporalConstraints().iterator(); it
				.hasNext();) {
			TemporalConstraint childElement = (TemporalConstraint) it.next();
			int visualID = TaskVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == TemporalConstraintEditPart.VISUAL_ID) {
				result.add(new TaskNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getContainedLinks(View view) {
		switch (TaskVisualIDRegistry.getVisualID(view)) {
		case RobotTaskEditPart.VISUAL_ID:
			return getRobotTask_1000ContainedLinks(view);
		case ModuleAlgorithmicEditPart.VISUAL_ID:
			return getModuleAlgorithmic_2001ContainedLinks(view);
		case ModulePhysicalRobotEditPart.VISUAL_ID:
			return getModulePhysicalRobot_2002ContainedLinks(view);
		case ModulePhysicalSensorEditPart.VISUAL_ID:
			return getModulePhysicalSensor_2003ContainedLinks(view);
		case TemporalConstraintEditPart.VISUAL_ID:
			return getTemporalConstraint_2004ContainedLinks(view);
		case PortDataEditPart.VISUAL_ID:
			return getPortData_3001ContainedLinks(view);
		case PortParamEditPart.VISUAL_ID:
			return getPortParam_3002ContainedLinks(view);
		case ExceptionT3EditPart.VISUAL_ID:
			return getExceptionT3_3007ContainedLinks(view);
		case ExceptionT1EditPart.VISUAL_ID:
			return getExceptionT1_3008ContainedLinks(view);
		case ExceptionT2EditPart.VISUAL_ID:
			return getExceptionT2_3009ContainedLinks(view);
		case EventConditionEditPart.VISUAL_ID:
			return getEventCondition_3018ContainedLinks(view);
		case PortParam2EditPart.VISUAL_ID:
			return getPortParam_3003ContainedLinks(view);
		case PortDriverEditPart.VISUAL_ID:
			return getPortDriver_3004ContainedLinks(view);
		case ExceptionT32EditPart.VISUAL_ID:
			return getExceptionT3_3010ContainedLinks(view);
		case ExceptionT12EditPart.VISUAL_ID:
			return getExceptionT1_3011ContainedLinks(view);
		case ExceptionT22EditPart.VISUAL_ID:
			return getExceptionT2_3012ContainedLinks(view);
		case EventCondition2EditPart.VISUAL_ID:
			return getEventCondition_3019ContainedLinks(view);
		case PortParam3EditPart.VISUAL_ID:
			return getPortParam_3005ContainedLinks(view);
		case PortDriver2EditPart.VISUAL_ID:
			return getPortDriver_3006ContainedLinks(view);
		case EventCondition3EditPart.VISUAL_ID:
			return getEventCondition_3020ContainedLinks(view);
		case LinkDriverEditPart.VISUAL_ID:
			return getLinkDriver_4001ContainedLinks(view);
		case LinkParamEditPart.VISUAL_ID:
			return getLinkParam_4002ContainedLinks(view);
		case LinkDataEditPart.VISUAL_ID:
			return getLinkData_4003ContainedLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getIncomingLinks(View view) {
		switch (TaskVisualIDRegistry.getVisualID(view)) {
		case ModuleAlgorithmicEditPart.VISUAL_ID:
			return getModuleAlgorithmic_2001IncomingLinks(view);
		case ModulePhysicalRobotEditPart.VISUAL_ID:
			return getModulePhysicalRobot_2002IncomingLinks(view);
		case ModulePhysicalSensorEditPart.VISUAL_ID:
			return getModulePhysicalSensor_2003IncomingLinks(view);
		case TemporalConstraintEditPart.VISUAL_ID:
			return getTemporalConstraint_2004IncomingLinks(view);
		case PortDataEditPart.VISUAL_ID:
			return getPortData_3001IncomingLinks(view);
		case PortParamEditPart.VISUAL_ID:
			return getPortParam_3002IncomingLinks(view);
		case ExceptionT3EditPart.VISUAL_ID:
			return getExceptionT3_3007IncomingLinks(view);
		case ExceptionT1EditPart.VISUAL_ID:
			return getExceptionT1_3008IncomingLinks(view);
		case ExceptionT2EditPart.VISUAL_ID:
			return getExceptionT2_3009IncomingLinks(view);
		case EventConditionEditPart.VISUAL_ID:
			return getEventCondition_3018IncomingLinks(view);
		case PortParam2EditPart.VISUAL_ID:
			return getPortParam_3003IncomingLinks(view);
		case PortDriverEditPart.VISUAL_ID:
			return getPortDriver_3004IncomingLinks(view);
		case ExceptionT32EditPart.VISUAL_ID:
			return getExceptionT3_3010IncomingLinks(view);
		case ExceptionT12EditPart.VISUAL_ID:
			return getExceptionT1_3011IncomingLinks(view);
		case ExceptionT22EditPart.VISUAL_ID:
			return getExceptionT2_3012IncomingLinks(view);
		case EventCondition2EditPart.VISUAL_ID:
			return getEventCondition_3019IncomingLinks(view);
		case PortParam3EditPart.VISUAL_ID:
			return getPortParam_3005IncomingLinks(view);
		case PortDriver2EditPart.VISUAL_ID:
			return getPortDriver_3006IncomingLinks(view);
		case EventCondition3EditPart.VISUAL_ID:
			return getEventCondition_3020IncomingLinks(view);
		case LinkDriverEditPart.VISUAL_ID:
			return getLinkDriver_4001IncomingLinks(view);
		case LinkParamEditPart.VISUAL_ID:
			return getLinkParam_4002IncomingLinks(view);
		case LinkDataEditPart.VISUAL_ID:
			return getLinkData_4003IncomingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getOutgoingLinks(View view) {
		switch (TaskVisualIDRegistry.getVisualID(view)) {
		case ModuleAlgorithmicEditPart.VISUAL_ID:
			return getModuleAlgorithmic_2001OutgoingLinks(view);
		case ModulePhysicalRobotEditPart.VISUAL_ID:
			return getModulePhysicalRobot_2002OutgoingLinks(view);
		case ModulePhysicalSensorEditPart.VISUAL_ID:
			return getModulePhysicalSensor_2003OutgoingLinks(view);
		case TemporalConstraintEditPart.VISUAL_ID:
			return getTemporalConstraint_2004OutgoingLinks(view);
		case PortDataEditPart.VISUAL_ID:
			return getPortData_3001OutgoingLinks(view);
		case PortParamEditPart.VISUAL_ID:
			return getPortParam_3002OutgoingLinks(view);
		case ExceptionT3EditPart.VISUAL_ID:
			return getExceptionT3_3007OutgoingLinks(view);
		case ExceptionT1EditPart.VISUAL_ID:
			return getExceptionT1_3008OutgoingLinks(view);
		case ExceptionT2EditPart.VISUAL_ID:
			return getExceptionT2_3009OutgoingLinks(view);
		case EventConditionEditPart.VISUAL_ID:
			return getEventCondition_3018OutgoingLinks(view);
		case PortParam2EditPart.VISUAL_ID:
			return getPortParam_3003OutgoingLinks(view);
		case PortDriverEditPart.VISUAL_ID:
			return getPortDriver_3004OutgoingLinks(view);
		case ExceptionT32EditPart.VISUAL_ID:
			return getExceptionT3_3010OutgoingLinks(view);
		case ExceptionT12EditPart.VISUAL_ID:
			return getExceptionT1_3011OutgoingLinks(view);
		case ExceptionT22EditPart.VISUAL_ID:
			return getExceptionT2_3012OutgoingLinks(view);
		case EventCondition2EditPart.VISUAL_ID:
			return getEventCondition_3019OutgoingLinks(view);
		case PortParam3EditPart.VISUAL_ID:
			return getPortParam_3005OutgoingLinks(view);
		case PortDriver2EditPart.VISUAL_ID:
			return getPortDriver_3006OutgoingLinks(view);
		case EventCondition3EditPart.VISUAL_ID:
			return getEventCondition_3020OutgoingLinks(view);
		case LinkDriverEditPart.VISUAL_ID:
			return getLinkDriver_4001OutgoingLinks(view);
		case LinkParamEditPart.VISUAL_ID:
			return getLinkParam_4002OutgoingLinks(view);
		case LinkDataEditPart.VISUAL_ID:
			return getLinkData_4003OutgoingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotTask_1000ContainedLinks(View view) {
		RobotTask modelElement = (RobotTask) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getContainedTypeModelFacetLinks_LinkDriver_4001(modelElement));
		result
				.addAll(getContainedTypeModelFacetLinks_LinkParam_4002(modelElement));
		result
				.addAll(getContainedTypeModelFacetLinks_LinkData_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getModuleAlgorithmic_2001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModulePhysicalRobot_2002ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModulePhysicalSensor_2003ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getTemporalConstraint_2004ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortData_3001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortParam_3002ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT3_3007ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT1_3008ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT2_3009ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getEventCondition_3018ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortParam_3003ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortDriver_3004ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getEventCondition_3020ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT3_3010ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT1_3011ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT2_3012ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getEventCondition_3019ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortParam_3005ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortDriver_3006ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLinkDriver_4001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLinkParam_4002ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLinkData_4003ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModuleAlgorithmic_2001IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModulePhysicalRobot_2002IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModulePhysicalSensor_2003IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getTemporalConstraint_2004IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortData_3001IncomingLinks(View view) {
		PortData modelElement = (PortData) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkDriver_4001(
				modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_LinkData_4003(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getPortParam_3002IncomingLinks(View view) {
		PortParam modelElement = (PortParam) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkParam_4002(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT3_3007IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT1_3008IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT2_3009IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getEventCondition_3018IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortParam_3003IncomingLinks(View view) {
		PortParam modelElement = (PortParam) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkParam_4002(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getPortDriver_3004IncomingLinks(View view) {
		PortDriver modelElement = (PortDriver) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkDriver_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getEventCondition_3020IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT3_3010IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT1_3011IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT2_3012IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getEventCondition_3019IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortParam_3005IncomingLinks(View view) {
		PortParam modelElement = (PortParam) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkParam_4002(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getPortDriver_3006IncomingLinks(View view) {
		PortDriver modelElement = (PortDriver) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkDriver_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLinkDriver_4001IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLinkParam_4002IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLinkData_4003IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModuleAlgorithmic_2001OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModulePhysicalRobot_2002OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getModulePhysicalSensor_2003OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getTemporalConstraint_2004OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortData_3001OutgoingLinks(View view) {
		PortData modelElement = (PortData) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkDriver_4001(modelElement));
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkData_4003(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getPortParam_3002OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT3_3007OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT1_3008OutgoingLinks(View view) {
		ExceptionT1 modelElement = (ExceptionT1) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkParam_4002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT2_3009OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getEventCondition_3018OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortParam_3003OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortDriver_3004OutgoingLinks(View view) {
		PortDriver modelElement = (PortDriver) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkDriver_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getEventCondition_3020OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT3_3010OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT1_3011OutgoingLinks(View view) {
		ExceptionT1 modelElement = (ExceptionT1) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkParam_4002(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getExceptionT2_3012OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getEventCondition_3019OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortParam_3005OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getPortDriver_3006OutgoingLinks(View view) {
		PortDriver modelElement = (PortDriver) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkDriver_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLinkDriver_4001OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLinkParam_4002OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLinkData_4003OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	private static Collection getContainedTypeModelFacetLinks_LinkDriver_4001(
			RobotTask container) {
		Collection result = new LinkedList();
		for (Iterator links = container.getLinkDrivers().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof LinkDriver) {
				continue;
			}
			LinkDriver link = (LinkDriver) linkObject;
			if (LinkDriverEditPart.VISUAL_ID != TaskVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			PortDataDriver dst = link.getTarget();
			PortDataDriver src = link.getSource();
			result.add(new TaskLinkDescriptor(src, dst, link,
					TaskElementTypes.LinkDriver_4001,
					LinkDriverEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getContainedTypeModelFacetLinks_LinkParam_4002(
			RobotTask container) {
		Collection result = new LinkedList();
		for (Iterator links = container.getLinkParams().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof LinkParam) {
				continue;
			}
			LinkParam link = (LinkParam) linkObject;
			if (LinkParamEditPart.VISUAL_ID != TaskVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			PortParam dst = link.getTarget();
			ExceptionT1 src = link.getSource();
			result.add(new TaskLinkDescriptor(src, dst, link,
					TaskElementTypes.LinkParam_4002,
					LinkParamEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getContainedTypeModelFacetLinks_LinkData_4003(
			RobotTask container) {
		Collection result = new LinkedList();
		for (Iterator links = container.getLinkDatas().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof LinkData) {
				continue;
			}
			LinkData link = (LinkData) linkObject;
			if (LinkDataEditPart.VISUAL_ID != TaskVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			PortData dst = link.getTarget();
			PortData src = link.getSource();
			result
					.add(new TaskLinkDescriptor(src, dst, link,
							TaskElementTypes.LinkData_4003,
							LinkDataEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getIncomingTypeModelFacetLinks_LinkDriver_4001(
			PortDataDriver target, Map crossReferences) {
		Collection result = new LinkedList();
		Collection settings = (Collection) crossReferences.get(target);
		for (Iterator it = settings.iterator(); it.hasNext();) {
			EStructuralFeature.Setting setting = (EStructuralFeature.Setting) it
					.next();
			if (setting.getEStructuralFeature() != TaskPackage.eINSTANCE
					.getLinkDriver_Target()
					|| false == setting.getEObject() instanceof LinkDriver) {
				continue;
			}
			LinkDriver link = (LinkDriver) setting.getEObject();
			if (LinkDriverEditPart.VISUAL_ID != TaskVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			PortDataDriver src = link.getSource();
			result.add(new TaskLinkDescriptor(src, target, link,
					TaskElementTypes.LinkDriver_4001,
					LinkDriverEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getIncomingTypeModelFacetLinks_LinkParam_4002(
			PortParam target, Map crossReferences) {
		Collection result = new LinkedList();
		Collection settings = (Collection) crossReferences.get(target);
		for (Iterator it = settings.iterator(); it.hasNext();) {
			EStructuralFeature.Setting setting = (EStructuralFeature.Setting) it
					.next();
			if (setting.getEStructuralFeature() != TaskPackage.eINSTANCE
					.getLinkParam_Target()
					|| false == setting.getEObject() instanceof LinkParam) {
				continue;
			}
			LinkParam link = (LinkParam) setting.getEObject();
			if (LinkParamEditPart.VISUAL_ID != TaskVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			ExceptionT1 src = link.getSource();
			result.add(new TaskLinkDescriptor(src, target, link,
					TaskElementTypes.LinkParam_4002,
					LinkParamEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getIncomingTypeModelFacetLinks_LinkData_4003(
			PortData target, Map crossReferences) {
		Collection result = new LinkedList();
		Collection settings = (Collection) crossReferences.get(target);
		for (Iterator it = settings.iterator(); it.hasNext();) {
			EStructuralFeature.Setting setting = (EStructuralFeature.Setting) it
					.next();
			if (setting.getEStructuralFeature() != TaskPackage.eINSTANCE
					.getLinkData_Target()
					|| false == setting.getEObject() instanceof LinkData) {
				continue;
			}
			LinkData link = (LinkData) setting.getEObject();
			if (LinkDataEditPart.VISUAL_ID != TaskVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			PortData src = link.getSource();
			result
					.add(new TaskLinkDescriptor(src, target, link,
							TaskElementTypes.LinkData_4003,
							LinkDataEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getOutgoingTypeModelFacetLinks_LinkDriver_4001(
			PortDataDriver source) {
		RobotTask container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof RobotTask) {
				container = (RobotTask) element;
			}
		}
		if (container == null) {
			return Collections.EMPTY_LIST;
		}
		Collection result = new LinkedList();
		for (Iterator links = container.getLinkDrivers().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof LinkDriver) {
				continue;
			}
			LinkDriver link = (LinkDriver) linkObject;
			if (LinkDriverEditPart.VISUAL_ID != TaskVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			PortDataDriver dst = link.getTarget();
			PortDataDriver src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new TaskLinkDescriptor(src, dst, link,
					TaskElementTypes.LinkDriver_4001,
					LinkDriverEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getOutgoingTypeModelFacetLinks_LinkParam_4002(
			ExceptionT1 source) {
		RobotTask container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof RobotTask) {
				container = (RobotTask) element;
			}
		}
		if (container == null) {
			return Collections.EMPTY_LIST;
		}
		Collection result = new LinkedList();
		for (Iterator links = container.getLinkParams().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof LinkParam) {
				continue;
			}
			LinkParam link = (LinkParam) linkObject;
			if (LinkParamEditPart.VISUAL_ID != TaskVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			PortParam dst = link.getTarget();
			ExceptionT1 src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new TaskLinkDescriptor(src, dst, link,
					TaskElementTypes.LinkParam_4002,
					LinkParamEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getOutgoingTypeModelFacetLinks_LinkData_4003(
			PortData source) {
		RobotTask container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof RobotTask) {
				container = (RobotTask) element;
			}
		}
		if (container == null) {
			return Collections.EMPTY_LIST;
		}
		Collection result = new LinkedList();
		for (Iterator links = container.getLinkDatas().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof LinkData) {
				continue;
			}
			LinkData link = (LinkData) linkObject;
			if (LinkDataEditPart.VISUAL_ID != TaskVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			PortData dst = link.getTarget();
			PortData src = link.getSource();
			if (src != source) {
				continue;
			}
			result
					.add(new TaskLinkDescriptor(src, dst, link,
							TaskElementTypes.LinkData_4003,
							LinkDataEditPart.VISUAL_ID));
		}
		return result;
	}

}
