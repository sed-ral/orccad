package task.diagram.providers;

import module.ModuleAlgorithmic;
import module.ModulePackage;
import org.eclipse.emf.ecore.EcorePackage;
import port.ExceptionT1;
import port.PortData;
import port.PortFactory;
import port.PortIOType;
import port.PortPackage;
import task.LinkData;
import task.LinkDataType;
import task.TaskPackage;
import task.diagram.expressions.TaskAbstractExpression;
import task.diagram.expressions.TaskOCLFactory;
import task.diagram.part.TaskDiagramEditorPlugin;
import constraint.ConstraintPackage;
import constraint.TemporalConstraint;

/**
 * @generated
 */
public class ElementInitializers {

	/**
	 * @generated
	 */
	public static void init_TemporalConstraint_2004(TemporalConstraint instance) {
		try {
			Object value_0 = TaskOCLFactory.getExpression("1.0",
					ConstraintPackage.eINSTANCE.getTemporalConstraint())
					.evaluate(instance);

			value_0 = TaskAbstractExpression.performCast(value_0,
					EcorePackage.eINSTANCE.getEDouble());
			instance.setPeriodTime(((Double) value_0).doubleValue());
		} catch (RuntimeException e) {
			TaskDiagramEditorPlugin.getInstance().logError(
					"Element initialization failed", e); //$NON-NLS-1$						
		}
	}

}
