package task.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import module.ModulePackage;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import port.PortPackage;
import task.TaskPackage;
import task.diagram.edit.parts.EventCondition2EditPart;
import task.diagram.edit.parts.EventCondition3EditPart;

import task.diagram.edit.parts.EventConditionEditPart;
import task.diagram.edit.parts.ExceptionT12EditPart;

import task.diagram.edit.parts.ExceptionT1EditPart;
import task.diagram.edit.parts.ExceptionT22EditPart;

import task.diagram.edit.parts.ExceptionT2EditPart;
import task.diagram.edit.parts.ExceptionT32EditPart;

import task.diagram.edit.parts.ExceptionT3EditPart;

import task.diagram.edit.parts.LinkDataEditPart;
import task.diagram.edit.parts.LinkDriverEditPart;
import task.diagram.edit.parts.LinkParamEditPart;

import task.diagram.edit.parts.ModuleAlgorithmicEditPart;
import task.diagram.edit.parts.ModulePhysicalRobotEditPart;
import task.diagram.edit.parts.ModulePhysicalSensorEditPart;

import task.diagram.edit.parts.PortDataEditPart;
import task.diagram.edit.parts.PortDriver2EditPart;
import task.diagram.edit.parts.PortDriverEditPart;
import task.diagram.edit.parts.PortParam2EditPart;
import task.diagram.edit.parts.PortParam3EditPart;

import task.diagram.edit.parts.PortParamEditPart;
import task.diagram.edit.parts.RobotTaskEditPart;
import task.diagram.edit.parts.TemporalConstraintEditPart;
import task.diagram.part.TaskDiagramEditorPlugin;
import constraint.ConstraintPackage;

/**
 * @generated
 */
public class TaskElementTypes extends ElementInitializers {

	/**
	 * @generated
	 */
	private TaskElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map elements;

	/**
	 * @generated
	 */
	private static ImageRegistry imageRegistry;

	/**
	 * @generated
	 */
	private static Set KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType RobotTask_1000 = getElementType("fr.inrialpes.necs.orccad.task.diagram.RobotTask_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ModuleAlgorithmic_2001 = getElementType("fr.inrialpes.necs.orccad.task.diagram.ModuleAlgorithmic_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ModulePhysicalRobot_2002 = getElementType("fr.inrialpes.necs.orccad.task.diagram.ModulePhysicalRobot_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ModulePhysicalSensor_2003 = getElementType("fr.inrialpes.necs.orccad.task.diagram.ModulePhysicalSensor_2003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType TemporalConstraint_2004 = getElementType("fr.inrialpes.necs.orccad.task.diagram.TemporalConstraint_2004"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType PortData_3001 = getElementType("fr.inrialpes.necs.orccad.task.diagram.PortData_3001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType PortParam_3002 = getElementType("fr.inrialpes.necs.orccad.task.diagram.PortParam_3002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType ExceptionT3_3007 = getElementType("fr.inrialpes.necs.orccad.task.diagram.ExceptionT3_3007"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType ExceptionT1_3008 = getElementType("fr.inrialpes.necs.orccad.task.diagram.ExceptionT1_3008"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType ExceptionT2_3009 = getElementType("fr.inrialpes.necs.orccad.task.diagram.ExceptionT2_3009"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType EventCondition_3018 = getElementType("fr.inrialpes.necs.orccad.task.diagram.EventCondition_3018"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType PortParam_3003 = getElementType("fr.inrialpes.necs.orccad.task.diagram.PortParam_3003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType PortDriver_3004 = getElementType("fr.inrialpes.necs.orccad.task.diagram.PortDriver_3004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType EventCondition_3020 = getElementType("fr.inrialpes.necs.orccad.task.diagram.EventCondition_3020"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType ExceptionT3_3010 = getElementType("fr.inrialpes.necs.orccad.task.diagram.ExceptionT3_3010"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType ExceptionT1_3011 = getElementType("fr.inrialpes.necs.orccad.task.diagram.ExceptionT1_3011"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType ExceptionT2_3012 = getElementType("fr.inrialpes.necs.orccad.task.diagram.ExceptionT2_3012"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType EventCondition_3019 = getElementType("fr.inrialpes.necs.orccad.task.diagram.EventCondition_3019"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType PortParam_3005 = getElementType("fr.inrialpes.necs.orccad.task.diagram.PortParam_3005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType PortDriver_3006 = getElementType("fr.inrialpes.necs.orccad.task.diagram.PortDriver_3006"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType LinkDriver_4001 = getElementType("fr.inrialpes.necs.orccad.task.diagram.LinkDriver_4001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType LinkParam_4002 = getElementType("fr.inrialpes.necs.orccad.task.diagram.LinkParam_4002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType LinkData_4003 = getElementType("fr.inrialpes.necs.orccad.task.diagram.LinkData_4003"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	private static ImageRegistry getImageRegistry() {
		if (imageRegistry == null) {
			imageRegistry = new ImageRegistry();
		}
		return imageRegistry;
	}

	/**
	 * @generated
	 */
	private static String getImageRegistryKey(ENamedElement element) {
		return element.getName();
	}

	/**
	 * @generated
	 */
	private static ImageDescriptor getProvidedImageDescriptor(
			ENamedElement element) {
		if (element instanceof EStructuralFeature) {
			EStructuralFeature feature = ((EStructuralFeature) element);
			EClass eContainingClass = feature.getEContainingClass();
			EClassifier eType = feature.getEType();
			if (eContainingClass != null && !eContainingClass.isAbstract()) {
				element = eContainingClass;
			} else if (eType instanceof EClass
					&& !((EClass) eType).isAbstract()) {
				element = eType;
			}
		}
		if (element instanceof EClass) {
			EClass eClass = (EClass) element;
			if (!eClass.isAbstract()) {
				return TaskDiagramEditorPlugin.getInstance()
						.getItemImageDescriptor(
								eClass.getEPackage().getEFactoryInstance()
										.create(eClass));
			}
		}
		// TODO : support structural features
		return null;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		String key = getImageRegistryKey(element);
		ImageDescriptor imageDescriptor = getImageRegistry().getDescriptor(key);
		if (imageDescriptor == null) {
			imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
		}
		return imageDescriptor;
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		String key = getImageRegistryKey(element);
		Image image = getImageRegistry().get(key);
		if (image == null) {
			ImageDescriptor imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
			image = getImageRegistry().get(key);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImage(element);
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap();

			elements.put(RobotTask_1000, TaskPackage.eINSTANCE.getRobotTask());

			elements.put(ModuleAlgorithmic_2001, ModulePackage.eINSTANCE
					.getModuleAlgorithmic());

			elements.put(ModulePhysicalRobot_2002, ModulePackage.eINSTANCE
					.getModulePhysicalRobot());

			elements.put(ModulePhysicalSensor_2003, ModulePackage.eINSTANCE
					.getModulePhysicalSensor());

			elements.put(TemporalConstraint_2004, ConstraintPackage.eINSTANCE
					.getTemporalConstraint());

			elements.put(PortData_3001, PortPackage.eINSTANCE.getPortData());

			elements.put(PortParam_3002, PortPackage.eINSTANCE.getPortParam());

			elements.put(ExceptionT3_3007, PortPackage.eINSTANCE
					.getExceptionT3());

			elements.put(ExceptionT1_3008, PortPackage.eINSTANCE
					.getExceptionT1());

			elements.put(ExceptionT2_3009, PortPackage.eINSTANCE
					.getExceptionT2());

			elements.put(EventCondition_3018, PortPackage.eINSTANCE
					.getEventCondition());

			elements.put(PortParam_3003, PortPackage.eINSTANCE.getPortParam());

			elements
					.put(PortDriver_3004, PortPackage.eINSTANCE.getPortDriver());

			elements.put(ExceptionT3_3010, PortPackage.eINSTANCE
					.getExceptionT3());

			elements.put(ExceptionT1_3011, PortPackage.eINSTANCE
					.getExceptionT1());

			elements.put(ExceptionT2_3012, PortPackage.eINSTANCE
					.getExceptionT2());

			elements.put(EventCondition_3019, PortPackage.eINSTANCE
					.getEventCondition());

			elements.put(PortParam_3005, PortPackage.eINSTANCE.getPortParam());

			elements
					.put(PortDriver_3006, PortPackage.eINSTANCE.getPortDriver());

			elements.put(EventCondition_3020, PortPackage.eINSTANCE
					.getEventCondition());

			elements
					.put(LinkDriver_4001, TaskPackage.eINSTANCE.getLinkDriver());

			elements.put(LinkParam_4002, TaskPackage.eINSTANCE.getLinkParam());

			elements.put(LinkData_4003, TaskPackage.eINSTANCE.getLinkData());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet();
			KNOWN_ELEMENT_TYPES.add(RobotTask_1000);
			KNOWN_ELEMENT_TYPES.add(ModuleAlgorithmic_2001);
			KNOWN_ELEMENT_TYPES.add(ModulePhysicalRobot_2002);
			KNOWN_ELEMENT_TYPES.add(ModulePhysicalSensor_2003);
			KNOWN_ELEMENT_TYPES.add(TemporalConstraint_2004);
			KNOWN_ELEMENT_TYPES.add(PortData_3001);
			KNOWN_ELEMENT_TYPES.add(PortParam_3002);
			KNOWN_ELEMENT_TYPES.add(ExceptionT3_3007);
			KNOWN_ELEMENT_TYPES.add(ExceptionT1_3008);
			KNOWN_ELEMENT_TYPES.add(ExceptionT2_3009);
			KNOWN_ELEMENT_TYPES.add(EventCondition_3018);
			KNOWN_ELEMENT_TYPES.add(PortParam_3003);
			KNOWN_ELEMENT_TYPES.add(PortDriver_3004);
			KNOWN_ELEMENT_TYPES.add(ExceptionT3_3010);
			KNOWN_ELEMENT_TYPES.add(ExceptionT1_3011);
			KNOWN_ELEMENT_TYPES.add(ExceptionT2_3012);
			KNOWN_ELEMENT_TYPES.add(EventCondition_3019);
			KNOWN_ELEMENT_TYPES.add(PortParam_3005);
			KNOWN_ELEMENT_TYPES.add(PortDriver_3006);
			KNOWN_ELEMENT_TYPES.add(EventCondition_3020);
			KNOWN_ELEMENT_TYPES.add(LinkDriver_4001);
			KNOWN_ELEMENT_TYPES.add(LinkParam_4002);
			KNOWN_ELEMENT_TYPES.add(LinkData_4003);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case RobotTaskEditPart.VISUAL_ID:
			return RobotTask_1000;
		case ModuleAlgorithmicEditPart.VISUAL_ID:
			return ModuleAlgorithmic_2001;
		case ModulePhysicalRobotEditPart.VISUAL_ID:
			return ModulePhysicalRobot_2002;
		case ModulePhysicalSensorEditPart.VISUAL_ID:
			return ModulePhysicalSensor_2003;
		case TemporalConstraintEditPart.VISUAL_ID:
			return TemporalConstraint_2004;
		case PortDataEditPart.VISUAL_ID:
			return PortData_3001;
		case PortParamEditPart.VISUAL_ID:
			return PortParam_3002;
		case ExceptionT3EditPart.VISUAL_ID:
			return ExceptionT3_3007;
		case ExceptionT1EditPart.VISUAL_ID:
			return ExceptionT1_3008;
		case ExceptionT2EditPart.VISUAL_ID:
			return ExceptionT2_3009;
		case EventConditionEditPart.VISUAL_ID:
			return EventCondition_3018;
		case PortParam2EditPart.VISUAL_ID:
			return PortParam_3003;
		case PortDriverEditPart.VISUAL_ID:
			return PortDriver_3004;
		case ExceptionT32EditPart.VISUAL_ID:
			return ExceptionT3_3010;
		case ExceptionT12EditPart.VISUAL_ID:
			return ExceptionT1_3011;
		case ExceptionT22EditPart.VISUAL_ID:
			return ExceptionT2_3012;
		case EventCondition2EditPart.VISUAL_ID:
			return EventCondition_3019;
		case PortParam3EditPart.VISUAL_ID:
			return PortParam_3005;
		case PortDriver2EditPart.VISUAL_ID:
			return PortDriver_3006;
		case EventCondition3EditPart.VISUAL_ID:
			return EventCondition_3020;
		case LinkDriverEditPart.VISUAL_ID:
			return LinkDriver_4001;
		case LinkParamEditPart.VISUAL_ID:
			return LinkParam_4002;
		case LinkDataEditPart.VISUAL_ID:
			return LinkData_4003;
		}
		return null;
	}

}
