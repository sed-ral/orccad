package task.diagram.providers;

import module.ModulePackage;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;

import port.PortPackage;
import task.diagram.edit.parts.EventConditionEventName2EditPart;
import task.diagram.edit.parts.EventConditionEventName3EditPart;

import task.diagram.edit.parts.EventConditionEventNameEditPart;

import task.diagram.edit.parts.ModuleAlgorithmicNameEditPart;
import task.diagram.edit.parts.ModulePhysicalRobotNameEditPart;
import task.diagram.edit.parts.ModulePhysicalSensorNameEditPart;
import task.diagram.edit.parts.TemporalConstraintNameEditPart;
import task.diagram.parsers.MessageFormatParser;
import task.diagram.part.TaskVisualIDRegistry;
import constraint.ConstraintPackage;

/**
 * @generated
 */
public class TaskParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser moduleAlgorithmicName_5001Parser;

	/**
	 * @generated
	 */
	private IParser getModuleAlgorithmicName_5001Parser() {
		if (moduleAlgorithmicName_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { ModulePackage.eINSTANCE
					.getModuleGeneric_Name() };
			EAttribute[] editableFeatures = new EAttribute[] { ModulePackage.eINSTANCE
					.getModuleGeneric_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			moduleAlgorithmicName_5001Parser = parser;
		}
		return moduleAlgorithmicName_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser modulePhysicalRobotName_5002Parser;

	/**
	 * @generated
	 */
	private IParser getModulePhysicalRobotName_5002Parser() {
		if (modulePhysicalRobotName_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { ModulePackage.eINSTANCE
					.getModuleGeneric_Name() };
			EAttribute[] editableFeatures = new EAttribute[] { ModulePackage.eINSTANCE
					.getModuleGeneric_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			modulePhysicalRobotName_5002Parser = parser;
		}
		return modulePhysicalRobotName_5002Parser;
	}

	/**
	 * @generated
	 */
	private IParser modulePhysicalSensorName_5003Parser;

	/**
	 * @generated
	 */
	private IParser getModulePhysicalSensorName_5003Parser() {
		if (modulePhysicalSensorName_5003Parser == null) {
			EAttribute[] features = new EAttribute[] { ModulePackage.eINSTANCE
					.getModuleGeneric_Name() };
			EAttribute[] editableFeatures = new EAttribute[] { ModulePackage.eINSTANCE
					.getModuleGeneric_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			modulePhysicalSensorName_5003Parser = parser;
		}
		return modulePhysicalSensorName_5003Parser;
	}

	/**
	 * @generated
	 */
	private IParser temporalConstraintNameSynchronisationTypePeriodTimePeriodTimeUnits_5004Parser;

	/**
	 * @generated
	 */
	private IParser getTemporalConstraintNameSynchronisationTypePeriodTimePeriodTimeUnits_5004Parser() {
		if (temporalConstraintNameSynchronisationTypePeriodTimePeriodTimeUnits_5004Parser == null) {
			EAttribute[] features = new EAttribute[] {
					ConstraintPackage.eINSTANCE.getTemporalConstraint_Name(),
					ConstraintPackage.eINSTANCE
							.getTemporalConstraint_SynchronisationType(),
					ConstraintPackage.eINSTANCE
							.getTemporalConstraint_PeriodTime(),
					ConstraintPackage.eINSTANCE
							.getTemporalConstraint_PeriodTimeUnits() };
			EAttribute[] editableFeatures = new EAttribute[] { ConstraintPackage.eINSTANCE
					.getTemporalConstraint_Name() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			parser.setViewPattern("{0} ({1} , T = {2} {3})"); //$NON-NLS-1$
			parser.setEditorPattern("{0} ({1} , T = {2} {3})"); //$NON-NLS-1$
			parser.setEditPattern("{0} ({1} , T = {2} {3})"); //$NON-NLS-1$
			temporalConstraintNameSynchronisationTypePeriodTimePeriodTimeUnits_5004Parser = parser;
		}
		return temporalConstraintNameSynchronisationTypePeriodTimePeriodTimeUnits_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser eventConditionEventName_5006Parser;

	/**
	 * @generated
	 */
	private IParser getEventConditionEventName_5006Parser() {
		if (eventConditionEventName_5006Parser == null) {
			EAttribute[] features = new EAttribute[] { PortPackage.eINSTANCE
					.getPortEvent_EventName() };
			EAttribute[] editableFeatures = new EAttribute[] { PortPackage.eINSTANCE
					.getPortEvent_EventName() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			eventConditionEventName_5006Parser = parser;
		}
		return eventConditionEventName_5006Parser;
	}

	/**
	 * @generated
	 */
	private IParser eventConditionEventName_5007Parser;

	/**
	 * @generated
	 */
	private IParser getEventConditionEventName_5007Parser() {
		if (eventConditionEventName_5007Parser == null) {
			EAttribute[] features = new EAttribute[] { PortPackage.eINSTANCE
					.getPortEvent_EventName() };
			EAttribute[] editableFeatures = new EAttribute[] { PortPackage.eINSTANCE
					.getPortEvent_EventName() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			eventConditionEventName_5007Parser = parser;
		}
		return eventConditionEventName_5007Parser;
	}

	/**
	 * @generated
	 */
	private IParser eventConditionEventName_5008Parser;

	/**
	 * @generated
	 */
	private IParser getEventConditionEventName_5008Parser() {
		if (eventConditionEventName_5008Parser == null) {
			EAttribute[] features = new EAttribute[] { PortPackage.eINSTANCE
					.getPortEvent_EventName() };
			EAttribute[] editableFeatures = new EAttribute[] { PortPackage.eINSTANCE
					.getPortEvent_EventName() };
			MessageFormatParser parser = new MessageFormatParser(features,
					editableFeatures);
			eventConditionEventName_5008Parser = parser;
		}
		return eventConditionEventName_5008Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case ModuleAlgorithmicNameEditPart.VISUAL_ID:
			return getModuleAlgorithmicName_5001Parser();
		case ModulePhysicalRobotNameEditPart.VISUAL_ID:
			return getModulePhysicalRobotName_5002Parser();
		case ModulePhysicalSensorNameEditPart.VISUAL_ID:
			return getModulePhysicalSensorName_5003Parser();
		case TemporalConstraintNameEditPart.VISUAL_ID:
			return getTemporalConstraintNameSynchronisationTypePeriodTimePeriodTimeUnits_5004Parser();
		case EventConditionEventNameEditPart.VISUAL_ID:
			return getEventConditionEventName_5006Parser();
		case EventConditionEventName2EditPart.VISUAL_ID:
			return getEventConditionEventName_5007Parser();
		case EventConditionEventName3EditPart.VISUAL_ID:
			return getEventConditionEventName_5008Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(TaskVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(TaskVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (TaskElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
