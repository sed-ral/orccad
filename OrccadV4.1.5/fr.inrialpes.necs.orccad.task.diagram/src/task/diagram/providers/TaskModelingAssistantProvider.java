package task.diagram.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.modelingassistant.ModelingAssistantProvider;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

import task.diagram.edit.parts.ExceptionT12EditPart;

import task.diagram.edit.parts.ExceptionT1EditPart;

import task.diagram.edit.parts.ModuleAlgorithmicEditPart;
import task.diagram.edit.parts.ModulePhysicalRobotEditPart;
import task.diagram.edit.parts.ModulePhysicalSensorEditPart;

import task.diagram.edit.parts.PortDataEditPart;
import task.diagram.edit.parts.PortDriver2EditPart;
import task.diagram.edit.parts.PortDriverEditPart;
import task.diagram.edit.parts.PortParam2EditPart;
import task.diagram.edit.parts.PortParam3EditPart;

import task.diagram.edit.parts.PortParamEditPart;
import task.diagram.edit.parts.RobotTaskEditPart;
import task.diagram.part.Messages;
import task.diagram.part.TaskDiagramEditorPlugin;

/**
 * @generated
 */
public class TaskModelingAssistantProvider extends ModelingAssistantProvider {

	/**
	 * @generated
	 */
	public List getTypesForPopupBar(IAdaptable host) {
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart instanceof ModuleAlgorithmicEditPart) {
			ArrayList types = new ArrayList(6);
			types.add(TaskElementTypes.PortData_3001);
			types.add(TaskElementTypes.PortParam_3002);
			types.add(TaskElementTypes.ExceptionT3_3007);
			types.add(TaskElementTypes.ExceptionT1_3008);
			types.add(TaskElementTypes.ExceptionT2_3009);
			types.add(TaskElementTypes.EventCondition_3018);
			return types;
		}
		if (editPart instanceof ModulePhysicalRobotEditPart) {
			ArrayList types = new ArrayList(6);
			types.add(TaskElementTypes.PortParam_3003);
			types.add(TaskElementTypes.PortDriver_3004);
			types.add(TaskElementTypes.ExceptionT3_3010);
			types.add(TaskElementTypes.ExceptionT1_3011);
			types.add(TaskElementTypes.ExceptionT2_3012);
			types.add(TaskElementTypes.EventCondition_3019);
			return types;
		}
		if (editPart instanceof ModulePhysicalSensorEditPart) {
			ArrayList types = new ArrayList(3);
			types.add(TaskElementTypes.PortParam_3005);
			types.add(TaskElementTypes.PortDriver_3006);
			types.add(TaskElementTypes.EventCondition_3020);
			return types;
		}
		if (editPart instanceof RobotTaskEditPart) {
			ArrayList types = new ArrayList(4);
			types.add(TaskElementTypes.ModuleAlgorithmic_2001);
			types.add(TaskElementTypes.ModulePhysicalRobot_2002);
			types.add(TaskElementTypes.ModulePhysicalSensor_2003);
			types.add(TaskElementTypes.TemporalConstraint_2004);
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof PortDataEditPart) {
			return ((PortDataEditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof ExceptionT1EditPart) {
			return ((ExceptionT1EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof PortDriverEditPart) {
			return ((PortDriverEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof ExceptionT12EditPart) {
			return ((ExceptionT12EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof PortDriver2EditPart) {
			return ((PortDriver2EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof PortDataEditPart) {
			return ((PortDataEditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof PortParamEditPart) {
			return ((PortParamEditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof PortParam2EditPart) {
			return ((PortParam2EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof PortDriverEditPart) {
			return ((PortDriverEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof PortParam3EditPart) {
			return ((PortParam3EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof PortDriver2EditPart) {
			return ((PortDriver2EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnSourceAndTarget(IAdaptable source,
			IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof PortDataEditPart) {
			return ((PortDataEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof ExceptionT1EditPart) {
			return ((ExceptionT1EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof PortDriverEditPart) {
			return ((PortDriverEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof ExceptionT12EditPart) {
			return ((ExceptionT12EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof PortDriver2EditPart) {
			return ((PortDriver2EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof PortDataEditPart) {
			return ((PortDataEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof PortParamEditPart) {
			return ((PortParamEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof PortParam2EditPart) {
			return ((PortParam2EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof PortDriverEditPart) {
			return ((PortDriverEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof PortParam3EditPart) {
			return ((PortParam3EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof PortDriver2EditPart) {
			return ((PortDriver2EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getTypesForTarget(IAdaptable source,
			IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof PortDataEditPart) {
			return ((PortDataEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof ExceptionT1EditPart) {
			return ((ExceptionT1EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof PortDriverEditPart) {
			return ((PortDriverEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof ExceptionT12EditPart) {
			return ((ExceptionT12EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof PortDriver2EditPart) {
			return ((PortDriver2EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForSource(IAdaptable target,
			IElementType relationshipType) {
		return selectExistingElement(target, getTypesForSource(target,
				relationshipType));
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForTarget(IAdaptable source,
			IElementType relationshipType) {
		return selectExistingElement(source, getTypesForTarget(source,
				relationshipType));
	}

	/**
	 * @generated
	 */
	protected EObject selectExistingElement(IAdaptable host, Collection types) {
		if (types.isEmpty()) {
			return null;
		}
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart == null) {
			return null;
		}
		Diagram diagram = (Diagram) editPart.getRoot().getContents().getModel();
		Collection elements = new HashSet();
		for (Iterator it = diagram.getElement().eAllContents(); it.hasNext();) {
			EObject element = (EObject) it.next();
			if (isApplicableElement(element, types)) {
				elements.add(element);
			}
		}
		if (elements.isEmpty()) {
			return null;
		}
		return selectElement((EObject[]) elements.toArray(new EObject[elements
				.size()]));
	}

	/**
	 * @generated
	 */
	protected boolean isApplicableElement(EObject element, Collection types) {
		IElementType type = ElementTypeRegistry.getInstance().getElementType(
				element);
		return types.contains(type);
	}

	/**
	 * @generated
	 */
	protected EObject selectElement(EObject[] elements) {
		Shell shell = Display.getCurrent().getActiveShell();
		ILabelProvider labelProvider = new AdapterFactoryLabelProvider(
				TaskDiagramEditorPlugin.getInstance()
						.getItemProvidersAdapterFactory());
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(
				shell, labelProvider);
		dialog.setMessage(Messages.TaskModelingAssistantProviderMessage);
		dialog.setTitle(Messages.TaskModelingAssistantProviderTitle);
		dialog.setMultipleSelection(false);
		dialog.setElements(elements);
		EObject selected = null;
		if (dialog.open() == Window.OK) {
			selected = (EObject) dialog.getFirstResult();
		}
		return selected;
	}
}
