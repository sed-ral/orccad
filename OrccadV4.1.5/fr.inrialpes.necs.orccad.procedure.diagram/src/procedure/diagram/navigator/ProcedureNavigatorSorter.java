package procedure.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

import procedure.diagram.part.ProcedureVisualIDRegistry;

/**
 * @generated
 */
public class ProcedureNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 7032;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof ProcedureNavigatorItem) {
			ProcedureNavigatorItem item = (ProcedureNavigatorItem) element;
			return ProcedureVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
