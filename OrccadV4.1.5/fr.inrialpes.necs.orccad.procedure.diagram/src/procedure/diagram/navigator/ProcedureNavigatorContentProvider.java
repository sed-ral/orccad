package procedure.diagram.navigator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gmf.runtime.emf.core.GMFEditingDomainFactory;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonContentProvider;

import procedure.diagram.edit.parts.Condition2EditPart;
import procedure.diagram.edit.parts.ConditionEditPart;
import procedure.diagram.edit.parts.LinkEntitiesEditPart;
import procedure.diagram.edit.parts.Loop2EditPart;
import procedure.diagram.edit.parts.Loop3EditPart;
import procedure.diagram.edit.parts.Loop4EditPart;
import procedure.diagram.edit.parts.Loop5EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen2EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen3EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen4EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen5EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGenEditPart;
import procedure.diagram.edit.parts.LoopEditPart;
import procedure.diagram.edit.parts.Parallel2EditPart;
import procedure.diagram.edit.parts.Parallel3EditPart;
import procedure.diagram.edit.parts.Parallel4EditPart;
import procedure.diagram.edit.parts.Parallel5EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen2EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen3EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen4EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen5EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGenEditPart;
import procedure.diagram.edit.parts.ParallelEditPart;
import procedure.diagram.edit.parts.RobotProcedure2EditPart;
import procedure.diagram.edit.parts.RobotProcedure3EditPart;
import procedure.diagram.edit.parts.RobotProcedure4EditPart;
import procedure.diagram.edit.parts.RobotProcedure5EditPart;
import procedure.diagram.edit.parts.RobotProcedure6EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen2EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen3EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen4EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen5EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGenEditPart;
import procedure.diagram.edit.parts.RobotProcedureEditPart;
import procedure.diagram.edit.parts.RobotTaskEntity2EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity3EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity4EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity5EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityEditPart;
import procedure.diagram.edit.parts.Sequence2EditPart;
import procedure.diagram.edit.parts.Sequence3EditPart;
import procedure.diagram.edit.parts.Sequence4EditPart;
import procedure.diagram.edit.parts.Sequence5EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen2EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen3EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen4EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen5EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGenEditPart;
import procedure.diagram.edit.parts.SequenceEditPart;
import procedure.diagram.part.Messages;
import procedure.diagram.part.ProcedureVisualIDRegistry;

/**
 * @generated
 */
public class ProcedureNavigatorContentProvider implements
		ICommonContentProvider {

	/**
	 * @generated
	 */
	private static final Object[] EMPTY_ARRAY = new Object[0];

	/**
	 * @generated
	 */
	private Viewer myViewer;

	/**
	 * @generated
	 */
	private AdapterFactoryEditingDomain myEditingDomain;

	/**
	 * @generated
	 */
	private WorkspaceSynchronizer myWorkspaceSynchronizer;

	/**
	 * @generated
	 */
	private Runnable myViewerRefreshRunnable;

	/**
	 * @generated
	 */
	public ProcedureNavigatorContentProvider() {
		TransactionalEditingDomain editingDomain = GMFEditingDomainFactory.INSTANCE
				.createEditingDomain();
		myEditingDomain = (AdapterFactoryEditingDomain) editingDomain;
		myEditingDomain.setResourceToReadOnlyMap(new HashMap() {
			public Object get(Object key) {
				if (!containsKey(key)) {
					put(key, Boolean.TRUE);
				}
				return super.get(key);
			}
		});
		myViewerRefreshRunnable = new Runnable() {
			public void run() {
				if (myViewer != null) {
					myViewer.refresh();
				}
			}
		};
		myWorkspaceSynchronizer = new WorkspaceSynchronizer(editingDomain,
				new WorkspaceSynchronizer.Delegate() {
					public void dispose() {
					}

					public boolean handleResourceChanged(final Resource resource) {
						for (Iterator it = myEditingDomain.getResourceSet()
								.getResources().iterator(); it.hasNext();) {
							Resource nextResource = (Resource) it.next();
							nextResource.unload();
						}
						if (myViewer != null) {
							myViewer.getControl().getDisplay().asyncExec(
									myViewerRefreshRunnable);
						}
						return true;
					}

					public boolean handleResourceDeleted(Resource resource) {
						for (Iterator it = myEditingDomain.getResourceSet()
								.getResources().iterator(); it.hasNext();) {
							Resource nextResource = (Resource) it.next();
							nextResource.unload();
						}
						if (myViewer != null) {
							myViewer.getControl().getDisplay().asyncExec(
									myViewerRefreshRunnable);
						}
						return true;
					}

					public boolean handleResourceMoved(Resource resource,
							final URI newURI) {
						for (Iterator it = myEditingDomain.getResourceSet()
								.getResources().iterator(); it.hasNext();) {
							Resource nextResource = (Resource) it.next();
							nextResource.unload();
						}
						if (myViewer != null) {
							myViewer.getControl().getDisplay().asyncExec(
									myViewerRefreshRunnable);
						}
						return true;
					}
				});
	}

	/**
	 * @generated
	 */
	public void dispose() {
		myWorkspaceSynchronizer.dispose();
		myWorkspaceSynchronizer = null;
		myViewerRefreshRunnable = null;
		for (Iterator it = myEditingDomain.getResourceSet().getResources()
				.iterator(); it.hasNext();) {
			Resource resource = (Resource) it.next();
			resource.unload();
		}
		((TransactionalEditingDomain) myEditingDomain).dispose();
		myEditingDomain = null;
	}

	/**
	 * @generated
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		myViewer = viewer;
	}

	/**
	 * @generated
	 */
	public Object[] getElements(Object inputElement) {
		return getChildren(inputElement);
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof IFile) {
			IFile file = (IFile) parentElement;
			URI fileURI = URI.createPlatformResourceURI(file.getFullPath()
					.toString(), true);
			Resource resource = myEditingDomain.getResourceSet().getResource(
					fileURI, true);
			Collection result = new ArrayList();
			result.addAll(createNavigatorItems(selectViewsByType(resource
					.getContents(), RobotProcedureEditPart.MODEL_ID), file,
					false));
			return result.toArray();
		}

		if (parentElement instanceof ProcedureNavigatorGroup) {
			ProcedureNavigatorGroup group = (ProcedureNavigatorGroup) parentElement;
			return group.getChildren();
		}

		if (parentElement instanceof ProcedureNavigatorItem) {
			ProcedureNavigatorItem navigatorItem = (ProcedureNavigatorItem) parentElement;
			if (navigatorItem.isLeaf() || !isOwnView(navigatorItem.getView())) {
				return EMPTY_ARRAY;
			}
			return getViewChildren(navigatorItem.getView(), parentElement);
		}

		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Object[] getViewChildren(View view, Object parentElement) {
		switch (ProcedureVisualIDRegistry.getVisualID(view)) {

		case RobotProcedureEditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup links = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotProcedure_1000_links,
					"icons/linksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(RobotTaskEntityEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedure2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry.getType(LoopEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ConditionEditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getDiagramLinksByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			links
					.addChildren(createNavigatorItems(connectedViews, links,
							false));
			if (!links.isEmpty()) {
				result.add(links);
			}
			return result.toArray();
		}

		case RobotTaskEntityEditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotTaskEntity_2001_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotTaskEntity_2001_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getIncomingLinksByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case ParallelEditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Parallel_2002_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Parallel_2002_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(ParallelCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case SequenceEditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Sequence_2003_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Sequence_2003_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(SequenceCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case RobotProcedure2EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotProcedure_2004_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotProcedure_2004_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(RobotProcedureCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Condition2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case LoopEditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Loop_2005_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Loop_2005_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(LoopCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure6EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case RobotTaskEntity2EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotTaskEntity_3001_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotTaskEntity_3001_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getIncomingLinksByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case RobotProcedure3EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotProcedure_3002_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotProcedure_3002_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(RobotProcedureCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Condition2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case RobotTaskEntity3EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotTaskEntity_3003_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotTaskEntity_3003_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getIncomingLinksByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case RobotProcedure4EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotProcedure_3004_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotProcedure_3004_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(RobotProcedureCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Condition2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Sequence2EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Sequence_3005_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Sequence_3005_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(SequenceCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case RobotTaskEntity4EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotTaskEntity_3021_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotTaskEntity_3021_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getIncomingLinksByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case RobotProcedure5EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotProcedure_3022_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotProcedure_3022_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(RobotProcedureCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Condition2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Parallel2EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Parallel_3008_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Parallel_3008_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(ParallelCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Sequence3EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Sequence_3009_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Sequence_3009_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(SequenceCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Sequence4EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Sequence_3023_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Sequence_3023_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(SequenceCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Parallel3EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Parallel_3024_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Parallel_3024_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(ParallelCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Parallel4EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Parallel_3012_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Parallel_3012_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(ParallelCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Loop2EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Loop_3013_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Loop_3013_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(LoopCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure6EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGenEditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case RobotTaskEntity5EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotTaskEntity_3014_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotTaskEntity_3014_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getIncomingLinksByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case RobotProcedure6EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotProcedure_3015_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_RobotProcedure_3015_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(RobotProcedureCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(
					Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedureCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Condition2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Loop3EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Loop_3016_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Loop_3016_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(LoopCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure6EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen2EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Sequence5EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Sequence_3017_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Sequence_3017_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(SequenceCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Loop4EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Loop_3025_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Loop_3025_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(LoopCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure6EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen3EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Parallel5EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Parallel_3019_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Parallel_3019_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(ParallelCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence3EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel4EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelCompartmentGen5EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop2EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case Loop5EditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup incominglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Loop_3020_incominglinks,
					"icons/incomingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup outgoinglinks = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_Loop_3020_outgoinglinks,
					"icons/outgoingLinksNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getChildrenByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(LoopCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(RobotProcedure6EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Sequence5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry
							.getType(Parallel5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getChildrenByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(LoopCompartmentGen4EditPart.VISUAL_ID));
			connectedViews = getChildrenByType(connectedViews,
					ProcedureVisualIDRegistry.getType(Loop5EditPart.VISUAL_ID));
			result.addAll(createNavigatorItems(connectedViews, parentElement,
					false));
			connectedViews = getIncomingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			incominglinks.addChildren(createNavigatorItems(connectedViews,
					incominglinks, true));
			connectedViews = getOutgoingLinksByType(
					Collections.singleton(view), ProcedureVisualIDRegistry
							.getType(LinkEntitiesEditPart.VISUAL_ID));
			outgoinglinks.addChildren(createNavigatorItems(connectedViews,
					outgoinglinks, true));
			if (!incominglinks.isEmpty()) {
				result.add(incominglinks);
			}
			if (!outgoinglinks.isEmpty()) {
				result.add(outgoinglinks);
			}
			return result.toArray();
		}

		case LinkEntitiesEditPart.VISUAL_ID: {
			Collection result = new ArrayList();
			ProcedureNavigatorGroup target = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_LinkEntities_4001_target,
					"icons/linkTargetNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			ProcedureNavigatorGroup source = new ProcedureNavigatorGroup(
					Messages.NavigatorGroupName_LinkEntities_4001_source,
					"icons/linkSourceNavigatorGroup.gif", parentElement); //$NON-NLS-1$
			Collection connectedViews = getLinksTargetByType(Collections
					.singleton(view), ProcedureVisualIDRegistry
					.getType(RobotTaskEntityEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedure2EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry.getType(LoopEditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity2EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedure3EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity3EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedure4EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Sequence2EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity4EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedure5EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Parallel2EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Sequence3EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Sequence4EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Parallel3EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Parallel4EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry.getType(Loop2EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity5EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedure6EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry.getType(Loop3EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Sequence5EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry.getType(Loop4EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Parallel5EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksTargetByType(Collections.singleton(view),
					ProcedureVisualIDRegistry.getType(Loop5EditPart.VISUAL_ID));
			target.addChildren(createNavigatorItems(connectedViews, target,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntityEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(ParallelEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(SequenceEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedure2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry.getType(LoopEditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedure3EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity3EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedure4EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Sequence2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity4EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedure5EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Parallel2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Sequence3EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Sequence4EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Parallel3EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Parallel4EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry.getType(Loop2EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotTaskEntity5EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(RobotProcedure6EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry.getType(Loop3EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Sequence5EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry.getType(Loop4EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry
							.getType(Parallel5EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			connectedViews = getLinksSourceByType(Collections.singleton(view),
					ProcedureVisualIDRegistry.getType(Loop5EditPart.VISUAL_ID));
			source.addChildren(createNavigatorItems(connectedViews, source,
					true));
			if (!target.isEmpty()) {
				result.add(target);
			}
			if (!source.isEmpty()) {
				result.add(source);
			}
			return result.toArray();
		}
		}
		return EMPTY_ARRAY;
	}

	/**
	 * @generated
	 */
	private Collection getLinksSourceByType(Collection edges, String type) {
		Collection result = new ArrayList();
		for (Iterator it = edges.iterator(); it.hasNext();) {
			Edge nextEdge = (Edge) it.next();
			View nextEdgeSource = nextEdge.getSource();
			if (type.equals(nextEdgeSource.getType())
					&& isOwnView(nextEdgeSource)) {
				result.add(nextEdgeSource);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection getLinksTargetByType(Collection edges, String type) {
		Collection result = new ArrayList();
		for (Iterator it = edges.iterator(); it.hasNext();) {
			Edge nextEdge = (Edge) it.next();
			View nextEdgeTarget = nextEdge.getTarget();
			if (type.equals(nextEdgeTarget.getType())
					&& isOwnView(nextEdgeTarget)) {
				result.add(nextEdgeTarget);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection getOutgoingLinksByType(Collection nodes, String type) {
		Collection result = new ArrayList();
		for (Iterator it = nodes.iterator(); it.hasNext();) {
			View nextNode = (View) it.next();
			result.addAll(selectViewsByType(nextNode.getSourceEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection getIncomingLinksByType(Collection nodes, String type) {
		Collection result = new ArrayList();
		for (Iterator it = nodes.iterator(); it.hasNext();) {
			View nextNode = (View) it.next();
			result.addAll(selectViewsByType(nextNode.getTargetEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection getChildrenByType(Collection nodes, String type) {
		Collection result = new ArrayList();
		for (Iterator it = nodes.iterator(); it.hasNext();) {
			View nextNode = (View) it.next();
			result.addAll(selectViewsByType(nextNode.getChildren(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection getDiagramLinksByType(Collection diagrams, String type) {
		Collection result = new ArrayList();
		for (Iterator it = diagrams.iterator(); it.hasNext();) {
			Diagram nextDiagram = (Diagram) it.next();
			result.addAll(selectViewsByType(nextDiagram.getEdges(), type));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private Collection selectViewsByType(Collection views, String type) {
		Collection result = new ArrayList();
		for (Iterator it = views.iterator(); it.hasNext();) {
			View nextView = (View) it.next();
			if (type.equals(nextView.getType()) && isOwnView(nextView)) {
				result.add(nextView);
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return RobotProcedureEditPart.MODEL_ID.equals(ProcedureVisualIDRegistry
				.getModelID(view));
	}

	/**
	 * @generated
	 */
	private Collection createNavigatorItems(Collection views, Object parent,
			boolean isLeafs) {
		Collection result = new ArrayList();
		for (Iterator it = views.iterator(); it.hasNext();) {
			result.add(new ProcedureNavigatorItem((View) it.next(), parent,
					isLeafs));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public Object getParent(Object element) {
		if (element instanceof ProcedureAbstractNavigatorItem) {
			ProcedureAbstractNavigatorItem abstractNavigatorItem = (ProcedureAbstractNavigatorItem) element;
			return abstractNavigatorItem.getParent();
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean hasChildren(Object element) {
		return element instanceof IFile || getChildren(element).length > 0;
	}

}
