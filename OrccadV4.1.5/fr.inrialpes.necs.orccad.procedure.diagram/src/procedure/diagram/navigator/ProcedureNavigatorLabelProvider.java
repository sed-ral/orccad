package procedure.diagram.navigator;

import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

import procedure.RobotProcedure;
import procedure.diagram.edit.parts.Condition2EditPart;
import procedure.diagram.edit.parts.ConditionEditPart;
import procedure.diagram.edit.parts.ConditionName2EditPart;
import procedure.diagram.edit.parts.ConditionNameEditPart;
import procedure.diagram.edit.parts.LinkEntitiesEditPart;
import procedure.diagram.edit.parts.Loop2EditPart;
import procedure.diagram.edit.parts.Loop3EditPart;
import procedure.diagram.edit.parts.Loop4EditPart;
import procedure.diagram.edit.parts.Loop5EditPart;
import procedure.diagram.edit.parts.LoopCondition2EditPart;
import procedure.diagram.edit.parts.LoopCondition3EditPart;
import procedure.diagram.edit.parts.LoopCondition4EditPart;
import procedure.diagram.edit.parts.LoopCondition5EditPart;
import procedure.diagram.edit.parts.LoopConditionEditPart;
import procedure.diagram.edit.parts.LoopEditPart;
import procedure.diagram.edit.parts.Parallel2EditPart;
import procedure.diagram.edit.parts.Parallel3EditPart;
import procedure.diagram.edit.parts.Parallel4EditPart;
import procedure.diagram.edit.parts.Parallel5EditPart;
import procedure.diagram.edit.parts.ParallelEditPart;
import procedure.diagram.edit.parts.RobotProcedure2EditPart;
import procedure.diagram.edit.parts.RobotProcedure3EditPart;
import procedure.diagram.edit.parts.RobotProcedure4EditPart;
import procedure.diagram.edit.parts.RobotProcedure5EditPart;
import procedure.diagram.edit.parts.RobotProcedure6EditPart;
import procedure.diagram.edit.parts.RobotProcedureEditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditions2EditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditions3EditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditions5EditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditionsEditPart;
import procedure.diagram.edit.parts.RobotProcedureNameEditPart;
import procedure.diagram.edit.parts.RobotTaskEntity2EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity3EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity4EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity5EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityEditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName2EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName3EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName4EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName5EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityNameEditPart;
import procedure.diagram.edit.parts.Sequence2EditPart;
import procedure.diagram.edit.parts.Sequence3EditPart;
import procedure.diagram.edit.parts.Sequence4EditPart;
import procedure.diagram.edit.parts.Sequence5EditPart;
import procedure.diagram.edit.parts.SequenceEditPart;
import procedure.diagram.part.ProcedureDiagramEditorPlugin;
import procedure.diagram.part.ProcedureVisualIDRegistry;
import procedure.diagram.providers.ProcedureElementTypes;
import procedure.diagram.providers.ProcedureParserProvider;

/**
 * @generated
 */
public class ProcedureNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		ProcedureDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put(
						"Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		ProcedureDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put(
						"Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof ProcedureNavigatorItem
				&& !isOwnView(((ProcedureNavigatorItem) element).getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof ProcedureNavigatorGroup) {
			ProcedureNavigatorGroup group = (ProcedureNavigatorGroup) element;
			return ProcedureDiagramEditorPlugin.getInstance().getBundledImage(
					group.getIcon());
		}

		if (element instanceof ProcedureNavigatorItem) {
			ProcedureNavigatorItem navigatorItem = (ProcedureNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (ProcedureVisualIDRegistry.getVisualID(view)) {
		case RobotProcedureEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://procedure?RobotProcedure", ProcedureElementTypes.RobotProcedure_1000); //$NON-NLS-1$
		case RobotTaskEntityEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://procedure?RobotTaskEntity", ProcedureElementTypes.RobotTaskEntity_2001); //$NON-NLS-1$
		case ParallelEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://procedure?Parallel", ProcedureElementTypes.Parallel_2002); //$NON-NLS-1$
		case SequenceEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://procedure?Sequence", ProcedureElementTypes.Sequence_2003); //$NON-NLS-1$
		case RobotProcedure2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://procedure?RobotProcedure", ProcedureElementTypes.RobotProcedure_2004); //$NON-NLS-1$
		case LoopEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://procedure?Loop", ProcedureElementTypes.Loop_2005); //$NON-NLS-1$
		case ConditionEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://procedure?Condition", ProcedureElementTypes.Condition_2006); //$NON-NLS-1$
		case RobotTaskEntity2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?RobotTaskEntity", ProcedureElementTypes.RobotTaskEntity_3001); //$NON-NLS-1$
		case RobotProcedure3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?RobotProcedure", ProcedureElementTypes.RobotProcedure_3002); //$NON-NLS-1$
		case RobotTaskEntity3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?RobotTaskEntity", ProcedureElementTypes.RobotTaskEntity_3003); //$NON-NLS-1$
		case RobotProcedure4EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?RobotProcedure", ProcedureElementTypes.RobotProcedure_3004); //$NON-NLS-1$
		case Sequence2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Sequence", ProcedureElementTypes.Sequence_3005); //$NON-NLS-1$
		case RobotTaskEntity4EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?RobotTaskEntity", ProcedureElementTypes.RobotTaskEntity_3021); //$NON-NLS-1$
		case RobotProcedure5EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?RobotProcedure", ProcedureElementTypes.RobotProcedure_3022); //$NON-NLS-1$
		case Parallel2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Parallel", ProcedureElementTypes.Parallel_3008); //$NON-NLS-1$
		case Sequence3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Sequence", ProcedureElementTypes.Sequence_3009); //$NON-NLS-1$
		case Sequence4EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Sequence", ProcedureElementTypes.Sequence_3023); //$NON-NLS-1$
		case Parallel3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Parallel", ProcedureElementTypes.Parallel_3024); //$NON-NLS-1$
		case Parallel4EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Parallel", ProcedureElementTypes.Parallel_3012); //$NON-NLS-1$
		case Loop2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Loop", ProcedureElementTypes.Loop_3013); //$NON-NLS-1$
		case RobotTaskEntity5EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?RobotTaskEntity", ProcedureElementTypes.RobotTaskEntity_3014); //$NON-NLS-1$
		case RobotProcedure6EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?RobotProcedure", ProcedureElementTypes.RobotProcedure_3015); //$NON-NLS-1$
		case Loop3EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Loop", ProcedureElementTypes.Loop_3016); //$NON-NLS-1$
		case Sequence5EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Sequence", ProcedureElementTypes.Sequence_3017); //$NON-NLS-1$
		case Loop4EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Loop", ProcedureElementTypes.Loop_3025); //$NON-NLS-1$
		case Parallel5EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Parallel", ProcedureElementTypes.Parallel_3019); //$NON-NLS-1$
		case Loop5EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Loop", ProcedureElementTypes.Loop_3020); //$NON-NLS-1$
		case Condition2EditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://procedure?Condition", ProcedureElementTypes.Condition_3026); //$NON-NLS-1$
		case LinkEntitiesEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://procedure?LinkEntities", ProcedureElementTypes.LinkEntities_4001); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = ProcedureDiagramEditorPlugin
				.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null && elementType != null
				&& ProcedureElementTypes.isKnownElementType(elementType)) {
			image = ProcedureElementTypes.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof ProcedureNavigatorGroup) {
			ProcedureNavigatorGroup group = (ProcedureNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof ProcedureNavigatorItem) {
			ProcedureNavigatorItem navigatorItem = (ProcedureNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (ProcedureVisualIDRegistry.getVisualID(view)) {
		case RobotProcedureEditPart.VISUAL_ID:
			return getRobotProcedure_1000Text(view);
		case RobotTaskEntityEditPart.VISUAL_ID:
			return getRobotTaskEntity_2001Text(view);
		case ParallelEditPart.VISUAL_ID:
			return getParallel_2002Text(view);
		case SequenceEditPart.VISUAL_ID:
			return getSequence_2003Text(view);
		case RobotProcedure2EditPart.VISUAL_ID:
			return getRobotProcedure_2004Text(view);
		case LoopEditPart.VISUAL_ID:
			return getLoop_2005Text(view);
		case ConditionEditPart.VISUAL_ID:
			return getCondition_2006Text(view);
		case RobotTaskEntity2EditPart.VISUAL_ID:
			return getRobotTaskEntity_3001Text(view);
		case RobotProcedure3EditPart.VISUAL_ID:
			return getRobotProcedure_3002Text(view);
		case RobotTaskEntity3EditPart.VISUAL_ID:
			return getRobotTaskEntity_3003Text(view);
		case RobotProcedure4EditPart.VISUAL_ID:
			return getRobotProcedure_3004Text(view);
		case Sequence2EditPart.VISUAL_ID:
			return getSequence_3005Text(view);
		case RobotTaskEntity4EditPart.VISUAL_ID:
			return getRobotTaskEntity_3021Text(view);
		case RobotProcedure5EditPart.VISUAL_ID:
			return getRobotProcedure_3022Text(view);
		case Parallel2EditPart.VISUAL_ID:
			return getParallel_3008Text(view);
		case Sequence3EditPart.VISUAL_ID:
			return getSequence_3009Text(view);
		case Sequence4EditPart.VISUAL_ID:
			return getSequence_3023Text(view);
		case Parallel3EditPart.VISUAL_ID:
			return getParallel_3024Text(view);
		case Parallel4EditPart.VISUAL_ID:
			return getParallel_3012Text(view);
		case Loop2EditPart.VISUAL_ID:
			return getLoop_3013Text(view);
		case RobotTaskEntity5EditPart.VISUAL_ID:
			return getRobotTaskEntity_3014Text(view);
		case RobotProcedure6EditPart.VISUAL_ID:
			return getRobotProcedure_3015Text(view);
		case Loop3EditPart.VISUAL_ID:
			return getLoop_3016Text(view);
		case Sequence5EditPart.VISUAL_ID:
			return getSequence_3017Text(view);
		case Loop4EditPart.VISUAL_ID:
			return getLoop_3025Text(view);
		case Parallel5EditPart.VISUAL_ID:
			return getParallel_3019Text(view);
		case Loop5EditPart.VISUAL_ID:
			return getLoop_3020Text(view);
		case Condition2EditPart.VISUAL_ID:
			return getCondition_3026Text(view);
		case LinkEntitiesEditPart.VISUAL_ID:
			return getLinkEntities_4001Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getRobotProcedure_1000Text(View view) {
		RobotProcedure domainModelElement = (RobotProcedure) view.getElement();
		if (domainModelElement != null) {
			return domainModelElement.getName();
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"No domain element for view with visualID = " + 1000); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRobotTaskEntity_2001Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.RobotTaskEntity_2001,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(RobotTaskEntityNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getParallel_2002Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getSequence_2003Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getRobotProcedure_2004Text(View view) {
		IParser parser = ProcedureParserProvider
				.getParser(
						ProcedureElementTypes.RobotProcedure_2004,
						view.getElement() != null ? view.getElement() : view,
						ProcedureVisualIDRegistry
								.getType(RobotProcedureNameConditionsEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5014); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getLoop_2005Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.Loop_2005,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(LoopConditionEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5015); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCondition_2006Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.Condition_2006,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(ConditionNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5020); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRobotTaskEntity_3001Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.RobotTaskEntity_3001,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(RobotTaskEntityName2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5002); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRobotProcedure_3002Text(View view) {
		IParser parser = ProcedureParserProvider
				.getParser(
						ProcedureElementTypes.RobotProcedure_3002,
						view.getElement() != null ? view.getElement() : view,
						ProcedureVisualIDRegistry
								.getType(RobotProcedureNameConditions2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5013); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRobotTaskEntity_3003Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.RobotTaskEntity_3003,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(RobotTaskEntityName3EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5003); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRobotProcedure_3004Text(View view) {
		IParser parser = ProcedureParserProvider
				.getParser(
						ProcedureElementTypes.RobotProcedure_3004,
						view.getElement() != null ? view.getElement() : view,
						ProcedureVisualIDRegistry
								.getType(RobotProcedureNameConditions3EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5012); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSequence_3005Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getRobotTaskEntity_3021Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.RobotTaskEntity_3021,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(RobotTaskEntityName4EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5016); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRobotProcedure_3022Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.RobotProcedure_3022,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(RobotProcedureNameEditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5018); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getParallel_3008Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getSequence_3009Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getSequence_3023Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getParallel_3024Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getParallel_3012Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getLoop_3013Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.Loop_3013,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(LoopCondition2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5010); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRobotTaskEntity_3014Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.RobotTaskEntity_3014,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(RobotTaskEntityName5EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5005); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getRobotProcedure_3015Text(View view) {
		IParser parser = ProcedureParserProvider
				.getParser(
						ProcedureElementTypes.RobotProcedure_3015,
						view.getElement() != null ? view.getElement() : view,
						ProcedureVisualIDRegistry
								.getType(RobotProcedureNameConditions5EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5009); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getLoop_3016Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.Loop_3016,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(LoopCondition3EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5008); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getSequence_3017Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getLoop_3025Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.Loop_3025,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(LoopCondition4EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5017); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getParallel_3019Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getLoop_3020Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.Loop_3020,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(LoopCondition5EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5006); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getCondition_3026Text(View view) {
		IParser parser = ProcedureParserProvider.getParser(
				ProcedureElementTypes.Condition_3026,
				view.getElement() != null ? view.getElement() : view,
				ProcedureVisualIDRegistry
						.getType(ConditionName2EditPart.VISUAL_ID));
		if (parser != null) {
			return parser.getPrintString(new EObjectAdapter(
					view.getElement() != null ? view.getElement() : view),
					ParserOptions.NONE.intValue());
		} else {
			ProcedureDiagramEditorPlugin.getInstance().logError(
					"Parser was not found for label " + 5019); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}
	}

	/**
	 * @generated
	 */
	private String getLinkEntities_4001Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$  //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return RobotProcedureEditPart.MODEL_ID.equals(ProcedureVisualIDRegistry
				.getModelID(view));
	}

}
