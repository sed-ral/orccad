package procedure.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;

import procedure.Condition;
import procedure.Entity;
import procedure.LinkEntities;
import procedure.Loop;
import procedure.Parallel;
import procedure.ProcedurePackage;
import procedure.RobotProcedure;
import procedure.RobotTaskEntity;
import procedure.Sequence;
import procedure.diagram.edit.parts.Condition2EditPart;
import procedure.diagram.edit.parts.ConditionEditPart;
import procedure.diagram.edit.parts.LinkEntitiesEditPart;
import procedure.diagram.edit.parts.Loop2EditPart;
import procedure.diagram.edit.parts.Loop3EditPart;
import procedure.diagram.edit.parts.Loop4EditPart;
import procedure.diagram.edit.parts.Loop5EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen2EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen3EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen4EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen5EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGenEditPart;
import procedure.diagram.edit.parts.LoopEditPart;
import procedure.diagram.edit.parts.Parallel2EditPart;
import procedure.diagram.edit.parts.Parallel3EditPart;
import procedure.diagram.edit.parts.Parallel4EditPart;
import procedure.diagram.edit.parts.Parallel5EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen2EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen3EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen4EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen5EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGenEditPart;
import procedure.diagram.edit.parts.ParallelEditPart;
import procedure.diagram.edit.parts.RobotProcedure2EditPart;
import procedure.diagram.edit.parts.RobotProcedure3EditPart;
import procedure.diagram.edit.parts.RobotProcedure4EditPart;
import procedure.diagram.edit.parts.RobotProcedure5EditPart;
import procedure.diagram.edit.parts.RobotProcedure6EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen2EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen3EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen4EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen5EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGenEditPart;
import procedure.diagram.edit.parts.RobotProcedureEditPart;
import procedure.diagram.edit.parts.RobotTaskEntity2EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity3EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity4EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity5EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityEditPart;
import procedure.diagram.edit.parts.Sequence2EditPart;
import procedure.diagram.edit.parts.Sequence3EditPart;
import procedure.diagram.edit.parts.Sequence4EditPart;
import procedure.diagram.edit.parts.Sequence5EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen2EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen3EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen4EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen5EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGenEditPart;
import procedure.diagram.edit.parts.SequenceEditPart;
import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class ProcedureDiagramUpdater {

	/**
	 * @generated
	 */
	public static List getSemanticChildren(View view) {
		switch (ProcedureVisualIDRegistry.getVisualID(view)) {
		case ParallelCompartmentGenEditPart.VISUAL_ID:
			return getParallelCompartmentGen_7001SemanticChildren(view);
		case RobotProcedureCompartmentGenEditPart.VISUAL_ID:
			return getRobotProcedureCompartmentGen_7002SemanticChildren(view);
		case RobotProcedureCompartmentGen2EditPart.VISUAL_ID:
			return getRobotProcedureCompartmentGen_7003SemanticChildren(view);
		case SequenceCompartmentGenEditPart.VISUAL_ID:
			return getSequenceCompartmentGen_7004SemanticChildren(view);
		case RobotProcedureCompartmentGen3EditPart.VISUAL_ID:
			return getRobotProcedureCompartmentGen_7027SemanticChildren(view);
		case ParallelCompartmentGen2EditPart.VISUAL_ID:
			return getParallelCompartmentGen_7006SemanticChildren(view);
		case SequenceCompartmentGen2EditPart.VISUAL_ID:
			return getSequenceCompartmentGen_7007SemanticChildren(view);
		case SequenceCompartmentGen3EditPart.VISUAL_ID:
			return getSequenceCompartmentGen_7028SemanticChildren(view);
		case ParallelCompartmentGen3EditPart.VISUAL_ID:
			return getParallelCompartmentGen_7029SemanticChildren(view);
		case ParallelCompartmentGen4EditPart.VISUAL_ID:
			return getParallelCompartmentGen_7010SemanticChildren(view);
		case LoopCompartmentGenEditPart.VISUAL_ID:
			return getLoopCompartmentGen_7011SemanticChildren(view);
		case RobotProcedureCompartmentGen4EditPart.VISUAL_ID:
			return getRobotProcedureCompartmentGen_7012SemanticChildren(view);
		case LoopCompartmentGen2EditPart.VISUAL_ID:
			return getLoopCompartmentGen_7013SemanticChildren(view);
		case SequenceCompartmentGen4EditPart.VISUAL_ID:
			return getSequenceCompartmentGen_7014SemanticChildren(view);
		case LoopCompartmentGen3EditPart.VISUAL_ID:
			return getLoopCompartmentGen_7030SemanticChildren(view);
		case ParallelCompartmentGen5EditPart.VISUAL_ID:
			return getParallelCompartmentGen_7016SemanticChildren(view);
		case LoopCompartmentGen4EditPart.VISUAL_ID:
			return getLoopCompartmentGen_7017SemanticChildren(view);
		case SequenceCompartmentGen5EditPart.VISUAL_ID:
			return getSequenceCompartmentGen_7018SemanticChildren(view);
		case RobotProcedureCompartmentGen5EditPart.VISUAL_ID:
			return getRobotProcedureCompartmentGen_7019SemanticChildren(view);
		case LoopCompartmentGen5EditPart.VISUAL_ID:
			return getLoopCompartmentGen_7020SemanticChildren(view);
		case RobotProcedureEditPart.VISUAL_ID:
			return getRobotProcedure_1000SemanticChildren(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getParallelCompartmentGen_7001SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Parallel modelElement = (Parallel) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getParaEntities().iterator(); it
				.hasNext();) {
			Entity childElement = (Entity) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RobotProcedure3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Sequence3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Parallel4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Loop2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedureCompartmentGen_7002SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		RobotProcedure modelElement = (RobotProcedure) containerView
				.getElement();
		List result = new LinkedList();
		{
			Entity childElement = modelElement.getEntity();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == RobotProcedure4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Sequence2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Parallel2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Loop3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
		}
		for (Iterator it = modelElement.getConditions().iterator(); it
				.hasNext();) {
			Condition childElement = (Condition) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Condition2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedureCompartmentGen_7003SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		RobotProcedure modelElement = (RobotProcedure) containerView
				.getElement();
		List result = new LinkedList();
		{
			Entity childElement = modelElement.getEntity();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == RobotProcedure4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Sequence2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Parallel2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Loop3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
		}
		for (Iterator it = modelElement.getConditions().iterator(); it
				.hasNext();) {
			Condition childElement = (Condition) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Condition2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequenceCompartmentGen_7004SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Sequence modelElement = (Sequence) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getSeqEntities().iterator(); it
				.hasNext();) {
			Entity childElement = (Entity) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RobotProcedure5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Sequence4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Parallel3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Loop4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedureCompartmentGen_7027SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		RobotProcedure modelElement = (RobotProcedure) containerView
				.getElement();
		List result = new LinkedList();
		{
			Entity childElement = modelElement.getEntity();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == RobotProcedure4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Sequence2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Parallel2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Loop3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
		}
		for (Iterator it = modelElement.getConditions().iterator(); it
				.hasNext();) {
			Condition childElement = (Condition) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Condition2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallelCompartmentGen_7006SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Parallel modelElement = (Parallel) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getParaEntities().iterator(); it
				.hasNext();) {
			Entity childElement = (Entity) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RobotProcedure3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Sequence3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Parallel4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Loop2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequenceCompartmentGen_7007SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Sequence modelElement = (Sequence) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getSeqEntities().iterator(); it
				.hasNext();) {
			Entity childElement = (Entity) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RobotProcedure5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Sequence4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Parallel3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Loop4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequenceCompartmentGen_7028SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Sequence modelElement = (Sequence) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getSeqEntities().iterator(); it
				.hasNext();) {
			Entity childElement = (Entity) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RobotProcedure5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Sequence4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Parallel3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Loop4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallelCompartmentGen_7029SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Parallel modelElement = (Parallel) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getParaEntities().iterator(); it
				.hasNext();) {
			Entity childElement = (Entity) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RobotProcedure3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Sequence3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Parallel4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Loop2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallelCompartmentGen_7010SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Parallel modelElement = (Parallel) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getParaEntities().iterator(); it
				.hasNext();) {
			Entity childElement = (Entity) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RobotProcedure3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Sequence3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Parallel4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Loop2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoopCompartmentGen_7011SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Loop modelElement = (Loop) containerView.getElement();
		List result = new LinkedList();
		{
			Entity childElement = modelElement.getLoopEntity();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == RobotProcedure6EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Sequence5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Parallel5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Loop5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedureCompartmentGen_7012SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		RobotProcedure modelElement = (RobotProcedure) containerView
				.getElement();
		List result = new LinkedList();
		{
			Entity childElement = modelElement.getEntity();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == RobotProcedure4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Sequence2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Parallel2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Loop3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
		}
		for (Iterator it = modelElement.getConditions().iterator(); it
				.hasNext();) {
			Condition childElement = (Condition) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Condition2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoopCompartmentGen_7013SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Loop modelElement = (Loop) containerView.getElement();
		List result = new LinkedList();
		{
			Entity childElement = modelElement.getLoopEntity();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == RobotProcedure6EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Sequence5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Parallel5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Loop5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequenceCompartmentGen_7014SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Sequence modelElement = (Sequence) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getSeqEntities().iterator(); it
				.hasNext();) {
			Entity childElement = (Entity) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RobotProcedure5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Sequence4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Parallel3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Loop4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoopCompartmentGen_7030SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Loop modelElement = (Loop) containerView.getElement();
		List result = new LinkedList();
		{
			Entity childElement = modelElement.getLoopEntity();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == RobotProcedure6EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Sequence5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Parallel5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Loop5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallelCompartmentGen_7016SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Parallel modelElement = (Parallel) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getParaEntities().iterator(); it
				.hasNext();) {
			Entity childElement = (Entity) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RobotProcedure3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Sequence3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Parallel4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Loop2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoopCompartmentGen_7017SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Loop modelElement = (Loop) containerView.getElement();
		List result = new LinkedList();
		{
			Entity childElement = modelElement.getLoopEntity();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == RobotProcedure6EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Sequence5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Parallel5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Loop5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequenceCompartmentGen_7018SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Sequence modelElement = (Sequence) containerView.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getSeqEntities().iterator(); it
				.hasNext();) {
			Entity childElement = (Entity) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == RobotProcedure5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Sequence4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Parallel3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
			if (visualID == Loop4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedureCompartmentGen_7019SemanticChildren(
			View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		RobotProcedure modelElement = (RobotProcedure) containerView
				.getElement();
		List result = new LinkedList();
		{
			Entity childElement = modelElement.getEntity();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == RobotProcedure4EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Sequence2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Parallel2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Loop3EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
		}
		for (Iterator it = modelElement.getConditions().iterator(); it
				.hasNext();) {
			Condition childElement = (Condition) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == Condition2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoopCompartmentGen_7020SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Loop modelElement = (Loop) containerView.getElement();
		List result = new LinkedList();
		{
			Entity childElement = modelElement.getLoopEntity();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntity5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == RobotProcedure6EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Sequence5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Parallel5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == Loop5EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_1000SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		RobotProcedure modelElement = (RobotProcedure) view.getElement();
		List result = new LinkedList();
		{
			Entity childElement = modelElement.getEntity();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == RobotTaskEntityEditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == ParallelEditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == SequenceEditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == RobotProcedure2EditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
			if (visualID == LoopEditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
			}
		}
		for (Iterator it = modelElement.getConditions().iterator(); it
				.hasNext();) {
			Condition childElement = (Condition) it.next();
			int visualID = ProcedureVisualIDRegistry.getNodeVisualID(view,
					childElement);
			if (visualID == ConditionEditPart.VISUAL_ID) {
				result.add(new ProcedureNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getContainedLinks(View view) {
		switch (ProcedureVisualIDRegistry.getVisualID(view)) {
		case RobotProcedureEditPart.VISUAL_ID:
			return getRobotProcedure_1000ContainedLinks(view);
		case RobotTaskEntityEditPart.VISUAL_ID:
			return getRobotTaskEntity_2001ContainedLinks(view);
		case ParallelEditPart.VISUAL_ID:
			return getParallel_2002ContainedLinks(view);
		case SequenceEditPart.VISUAL_ID:
			return getSequence_2003ContainedLinks(view);
		case RobotProcedure2EditPart.VISUAL_ID:
			return getRobotProcedure_2004ContainedLinks(view);
		case LoopEditPart.VISUAL_ID:
			return getLoop_2005ContainedLinks(view);
		case ConditionEditPart.VISUAL_ID:
			return getCondition_2006ContainedLinks(view);
		case RobotTaskEntity2EditPart.VISUAL_ID:
			return getRobotTaskEntity_3001ContainedLinks(view);
		case RobotProcedure3EditPart.VISUAL_ID:
			return getRobotProcedure_3002ContainedLinks(view);
		case RobotTaskEntity3EditPart.VISUAL_ID:
			return getRobotTaskEntity_3003ContainedLinks(view);
		case RobotProcedure4EditPart.VISUAL_ID:
			return getRobotProcedure_3004ContainedLinks(view);
		case Sequence2EditPart.VISUAL_ID:
			return getSequence_3005ContainedLinks(view);
		case RobotTaskEntity4EditPart.VISUAL_ID:
			return getRobotTaskEntity_3021ContainedLinks(view);
		case RobotProcedure5EditPart.VISUAL_ID:
			return getRobotProcedure_3022ContainedLinks(view);
		case Parallel2EditPart.VISUAL_ID:
			return getParallel_3008ContainedLinks(view);
		case Sequence3EditPart.VISUAL_ID:
			return getSequence_3009ContainedLinks(view);
		case Sequence4EditPart.VISUAL_ID:
			return getSequence_3023ContainedLinks(view);
		case Parallel3EditPart.VISUAL_ID:
			return getParallel_3024ContainedLinks(view);
		case Parallel4EditPart.VISUAL_ID:
			return getParallel_3012ContainedLinks(view);
		case Loop2EditPart.VISUAL_ID:
			return getLoop_3013ContainedLinks(view);
		case RobotTaskEntity5EditPart.VISUAL_ID:
			return getRobotTaskEntity_3014ContainedLinks(view);
		case RobotProcedure6EditPart.VISUAL_ID:
			return getRobotProcedure_3015ContainedLinks(view);
		case Loop3EditPart.VISUAL_ID:
			return getLoop_3016ContainedLinks(view);
		case Sequence5EditPart.VISUAL_ID:
			return getSequence_3017ContainedLinks(view);
		case Loop4EditPart.VISUAL_ID:
			return getLoop_3025ContainedLinks(view);
		case Parallel5EditPart.VISUAL_ID:
			return getParallel_3019ContainedLinks(view);
		case Loop5EditPart.VISUAL_ID:
			return getLoop_3020ContainedLinks(view);
		case Condition2EditPart.VISUAL_ID:
			return getCondition_3026ContainedLinks(view);
		case LinkEntitiesEditPart.VISUAL_ID:
			return getLinkEntities_4001ContainedLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getIncomingLinks(View view) {
		switch (ProcedureVisualIDRegistry.getVisualID(view)) {
		case RobotTaskEntityEditPart.VISUAL_ID:
			return getRobotTaskEntity_2001IncomingLinks(view);
		case ParallelEditPart.VISUAL_ID:
			return getParallel_2002IncomingLinks(view);
		case SequenceEditPart.VISUAL_ID:
			return getSequence_2003IncomingLinks(view);
		case RobotProcedure2EditPart.VISUAL_ID:
			return getRobotProcedure_2004IncomingLinks(view);
		case LoopEditPart.VISUAL_ID:
			return getLoop_2005IncomingLinks(view);
		case ConditionEditPart.VISUAL_ID:
			return getCondition_2006IncomingLinks(view);
		case RobotTaskEntity2EditPart.VISUAL_ID:
			return getRobotTaskEntity_3001IncomingLinks(view);
		case RobotProcedure3EditPart.VISUAL_ID:
			return getRobotProcedure_3002IncomingLinks(view);
		case RobotTaskEntity3EditPart.VISUAL_ID:
			return getRobotTaskEntity_3003IncomingLinks(view);
		case RobotProcedure4EditPart.VISUAL_ID:
			return getRobotProcedure_3004IncomingLinks(view);
		case Sequence2EditPart.VISUAL_ID:
			return getSequence_3005IncomingLinks(view);
		case RobotTaskEntity4EditPart.VISUAL_ID:
			return getRobotTaskEntity_3021IncomingLinks(view);
		case RobotProcedure5EditPart.VISUAL_ID:
			return getRobotProcedure_3022IncomingLinks(view);
		case Parallel2EditPart.VISUAL_ID:
			return getParallel_3008IncomingLinks(view);
		case Sequence3EditPart.VISUAL_ID:
			return getSequence_3009IncomingLinks(view);
		case Sequence4EditPart.VISUAL_ID:
			return getSequence_3023IncomingLinks(view);
		case Parallel3EditPart.VISUAL_ID:
			return getParallel_3024IncomingLinks(view);
		case Parallel4EditPart.VISUAL_ID:
			return getParallel_3012IncomingLinks(view);
		case Loop2EditPart.VISUAL_ID:
			return getLoop_3013IncomingLinks(view);
		case RobotTaskEntity5EditPart.VISUAL_ID:
			return getRobotTaskEntity_3014IncomingLinks(view);
		case RobotProcedure6EditPart.VISUAL_ID:
			return getRobotProcedure_3015IncomingLinks(view);
		case Loop3EditPart.VISUAL_ID:
			return getLoop_3016IncomingLinks(view);
		case Sequence5EditPart.VISUAL_ID:
			return getSequence_3017IncomingLinks(view);
		case Loop4EditPart.VISUAL_ID:
			return getLoop_3025IncomingLinks(view);
		case Parallel5EditPart.VISUAL_ID:
			return getParallel_3019IncomingLinks(view);
		case Loop5EditPart.VISUAL_ID:
			return getLoop_3020IncomingLinks(view);
		case Condition2EditPart.VISUAL_ID:
			return getCondition_3026IncomingLinks(view);
		case LinkEntitiesEditPart.VISUAL_ID:
			return getLinkEntities_4001IncomingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getOutgoingLinks(View view) {
		switch (ProcedureVisualIDRegistry.getVisualID(view)) {
		case RobotTaskEntityEditPart.VISUAL_ID:
			return getRobotTaskEntity_2001OutgoingLinks(view);
		case ParallelEditPart.VISUAL_ID:
			return getParallel_2002OutgoingLinks(view);
		case SequenceEditPart.VISUAL_ID:
			return getSequence_2003OutgoingLinks(view);
		case RobotProcedure2EditPart.VISUAL_ID:
			return getRobotProcedure_2004OutgoingLinks(view);
		case LoopEditPart.VISUAL_ID:
			return getLoop_2005OutgoingLinks(view);
		case ConditionEditPart.VISUAL_ID:
			return getCondition_2006OutgoingLinks(view);
		case RobotTaskEntity2EditPart.VISUAL_ID:
			return getRobotTaskEntity_3001OutgoingLinks(view);
		case RobotProcedure3EditPart.VISUAL_ID:
			return getRobotProcedure_3002OutgoingLinks(view);
		case RobotTaskEntity3EditPart.VISUAL_ID:
			return getRobotTaskEntity_3003OutgoingLinks(view);
		case RobotProcedure4EditPart.VISUAL_ID:
			return getRobotProcedure_3004OutgoingLinks(view);
		case Sequence2EditPart.VISUAL_ID:
			return getSequence_3005OutgoingLinks(view);
		case RobotTaskEntity4EditPart.VISUAL_ID:
			return getRobotTaskEntity_3021OutgoingLinks(view);
		case RobotProcedure5EditPart.VISUAL_ID:
			return getRobotProcedure_3022OutgoingLinks(view);
		case Parallel2EditPart.VISUAL_ID:
			return getParallel_3008OutgoingLinks(view);
		case Sequence3EditPart.VISUAL_ID:
			return getSequence_3009OutgoingLinks(view);
		case Sequence4EditPart.VISUAL_ID:
			return getSequence_3023OutgoingLinks(view);
		case Parallel3EditPart.VISUAL_ID:
			return getParallel_3024OutgoingLinks(view);
		case Parallel4EditPart.VISUAL_ID:
			return getParallel_3012OutgoingLinks(view);
		case Loop2EditPart.VISUAL_ID:
			return getLoop_3013OutgoingLinks(view);
		case RobotTaskEntity5EditPart.VISUAL_ID:
			return getRobotTaskEntity_3014OutgoingLinks(view);
		case RobotProcedure6EditPart.VISUAL_ID:
			return getRobotProcedure_3015OutgoingLinks(view);
		case Loop3EditPart.VISUAL_ID:
			return getLoop_3016OutgoingLinks(view);
		case Sequence5EditPart.VISUAL_ID:
			return getSequence_3017OutgoingLinks(view);
		case Loop4EditPart.VISUAL_ID:
			return getLoop_3025OutgoingLinks(view);
		case Parallel5EditPart.VISUAL_ID:
			return getParallel_3019OutgoingLinks(view);
		case Loop5EditPart.VISUAL_ID:
			return getLoop_3020OutgoingLinks(view);
		case Condition2EditPart.VISUAL_ID:
			return getCondition_3026OutgoingLinks(view);
		case LinkEntitiesEditPart.VISUAL_ID:
			return getLinkEntities_4001OutgoingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_1000ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_2001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getParallel_2002ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getSequence_2003ContainedLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getContainedTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_2004ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLoop_2005ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCondition_2006ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3002ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3003ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3004ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3005ContainedLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getContainedTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3021ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3022ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3008ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3009ContainedLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getContainedTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3023ContainedLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getContainedTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3024ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3012ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3013ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3014ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3015ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3016ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3017ContainedLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getContainedTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3025ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3019ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3020ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCondition_3026ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLinkEntities_4001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_2001IncomingLinks(View view) {
		RobotTaskEntity modelElement = (RobotTaskEntity) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallel_2002IncomingLinks(View view) {
		Parallel modelElement = (Parallel) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequence_2003IncomingLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_2004IncomingLinks(View view) {
		RobotProcedure modelElement = (RobotProcedure) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoop_2005IncomingLinks(View view) {
		Loop modelElement = (Loop) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getCondition_2006IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3001IncomingLinks(View view) {
		RobotTaskEntity modelElement = (RobotTaskEntity) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3002IncomingLinks(View view) {
		RobotProcedure modelElement = (RobotProcedure) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3003IncomingLinks(View view) {
		RobotTaskEntity modelElement = (RobotTaskEntity) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3004IncomingLinks(View view) {
		RobotProcedure modelElement = (RobotProcedure) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3005IncomingLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3021IncomingLinks(View view) {
		RobotTaskEntity modelElement = (RobotTaskEntity) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3022IncomingLinks(View view) {
		RobotProcedure modelElement = (RobotProcedure) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3008IncomingLinks(View view) {
		Parallel modelElement = (Parallel) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3009IncomingLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3023IncomingLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3024IncomingLinks(View view) {
		Parallel modelElement = (Parallel) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3012IncomingLinks(View view) {
		Parallel modelElement = (Parallel) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3013IncomingLinks(View view) {
		Loop modelElement = (Loop) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3014IncomingLinks(View view) {
		RobotTaskEntity modelElement = (RobotTaskEntity) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3015IncomingLinks(View view) {
		RobotProcedure modelElement = (RobotProcedure) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3016IncomingLinks(View view) {
		Loop modelElement = (Loop) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3017IncomingLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3025IncomingLinks(View view) {
		Loop modelElement = (Loop) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3019IncomingLinks(View view) {
		Parallel modelElement = (Parallel) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3020IncomingLinks(View view) {
		Loop modelElement = (Loop) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_LinkEntities_4001(
				modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getCondition_3026IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLinkEntities_4001IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_2001OutgoingLinks(View view) {
		RobotTaskEntity modelElement = (RobotTaskEntity) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallel_2002OutgoingLinks(View view) {
		Parallel modelElement = (Parallel) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequence_2003OutgoingLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_2004OutgoingLinks(View view) {
		RobotProcedure modelElement = (RobotProcedure) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoop_2005OutgoingLinks(View view) {
		Loop modelElement = (Loop) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getCondition_2006OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3001OutgoingLinks(View view) {
		RobotTaskEntity modelElement = (RobotTaskEntity) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3002OutgoingLinks(View view) {
		RobotProcedure modelElement = (RobotProcedure) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3003OutgoingLinks(View view) {
		RobotTaskEntity modelElement = (RobotTaskEntity) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3004OutgoingLinks(View view) {
		RobotProcedure modelElement = (RobotProcedure) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3005OutgoingLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3021OutgoingLinks(View view) {
		RobotTaskEntity modelElement = (RobotTaskEntity) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3022OutgoingLinks(View view) {
		RobotProcedure modelElement = (RobotProcedure) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3008OutgoingLinks(View view) {
		Parallel modelElement = (Parallel) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3009OutgoingLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3023OutgoingLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3024OutgoingLinks(View view) {
		Parallel modelElement = (Parallel) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3012OutgoingLinks(View view) {
		Parallel modelElement = (Parallel) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3013OutgoingLinks(View view) {
		Loop modelElement = (Loop) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotTaskEntity_3014OutgoingLinks(View view) {
		RobotTaskEntity modelElement = (RobotTaskEntity) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getRobotProcedure_3015OutgoingLinks(View view) {
		RobotProcedure modelElement = (RobotProcedure) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3016OutgoingLinks(View view) {
		Loop modelElement = (Loop) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSequence_3017OutgoingLinks(View view) {
		Sequence modelElement = (Sequence) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3025OutgoingLinks(View view) {
		Loop modelElement = (Loop) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getParallel_3019OutgoingLinks(View view) {
		Parallel modelElement = (Parallel) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getLoop_3020OutgoingLinks(View view) {
		Loop modelElement = (Loop) view.getElement();
		List result = new LinkedList();
		result
				.addAll(getOutgoingTypeModelFacetLinks_LinkEntities_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getCondition_3026OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getLinkEntities_4001OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	private static Collection getContainedTypeModelFacetLinks_LinkEntities_4001(
			Sequence container) {
		Collection result = new LinkedList();
		for (Iterator links = container.getLinkEntities().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof LinkEntities) {
				continue;
			}
			LinkEntities link = (LinkEntities) linkObject;
			if (LinkEntitiesEditPart.VISUAL_ID != ProcedureVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Entity dst = link.getTarget();
			Entity src = link.getSource();
			result.add(new ProcedureLinkDescriptor(src, dst, link,
					ProcedureElementTypes.LinkEntities_4001,
					LinkEntitiesEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getIncomingTypeModelFacetLinks_LinkEntities_4001(
			Entity target, Map crossReferences) {
		Collection result = new LinkedList();
		Collection settings = (Collection) crossReferences.get(target);
		for (Iterator it = settings.iterator(); it.hasNext();) {
			EStructuralFeature.Setting setting = (EStructuralFeature.Setting) it
					.next();
			if (setting.getEStructuralFeature() != ProcedurePackage.eINSTANCE
					.getLinkEntities_Target()
					|| false == setting.getEObject() instanceof LinkEntities) {
				continue;
			}
			LinkEntities link = (LinkEntities) setting.getEObject();
			if (LinkEntitiesEditPart.VISUAL_ID != ProcedureVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Entity src = link.getSource();
			result.add(new ProcedureLinkDescriptor(src, target, link,
					ProcedureElementTypes.LinkEntities_4001,
					LinkEntitiesEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getOutgoingTypeModelFacetLinks_LinkEntities_4001(
			Entity source) {
		Sequence container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Sequence) {
				container = (Sequence) element;
			}
		}
		if (container == null) {
			return Collections.EMPTY_LIST;
		}
		Collection result = new LinkedList();
		for (Iterator links = container.getLinkEntities().iterator(); links
				.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof LinkEntities) {
				continue;
			}
			LinkEntities link = (LinkEntities) linkObject;
			if (LinkEntitiesEditPart.VISUAL_ID != ProcedureVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Entity dst = link.getTarget();
			Entity src = link.getSource();
			if (src != source) {
				continue;
			}
			result.add(new ProcedureLinkDescriptor(src, dst, link,
					ProcedureElementTypes.LinkEntities_4001,
					LinkEntitiesEditPart.VISUAL_ID));
		}
		return result;
	}

}
