package procedure.diagram.part;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;

import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class ProcedurePaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createProcedure1Group());
	}

	/**
	 * Creates "procedure" palette tool group
	 * @generated
	 */
	private PaletteContainer createProcedure1Group() {
		PaletteGroup paletteContainer = new PaletteGroup(
				Messages.Procedure1Group_title);
		paletteContainer.setId("createProcedure1Group"); //$NON-NLS-1$
		paletteContainer.add(createSequence1CreationTool());
		paletteContainer.add(createParallel2CreationTool());
		paletteContainer.add(createRobotProcedure3CreationTool());
		paletteContainer.add(createRobotTask4CreationTool());
		paletteContainer.add(createLink5CreationTool());
		paletteContainer.add(createLoop6CreationTool());
		paletteContainer.add(createRobotProcedureCondition7CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createSequence1CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(5);
		types.add(ProcedureElementTypes.Sequence_3005);
		types.add(ProcedureElementTypes.Sequence_3009);
		types.add(ProcedureElementTypes.Sequence_3023);
		types.add(ProcedureElementTypes.Sequence_3017);
		types.add(ProcedureElementTypes.Sequence_2003);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Sequence1CreationTool_title,
				Messages.Sequence1CreationTool_desc, types);
		entry.setId("createSequence1CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ProcedureElementTypes
				.getImageDescriptor(ProcedureElementTypes.Sequence_3005));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createParallel2CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(5);
		types.add(ProcedureElementTypes.Parallel_2002);
		types.add(ProcedureElementTypes.Parallel_3008);
		types.add(ProcedureElementTypes.Parallel_3024);
		types.add(ProcedureElementTypes.Parallel_3012);
		types.add(ProcedureElementTypes.Parallel_3019);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Parallel2CreationTool_title,
				Messages.Parallel2CreationTool_desc, types);
		entry.setId("createParallel2CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ProcedureElementTypes
				.getImageDescriptor(ProcedureElementTypes.Parallel_2002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRobotProcedure3CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(5);
		types.add(ProcedureElementTypes.RobotProcedure_3002);
		types.add(ProcedureElementTypes.RobotProcedure_3004);
		types.add(ProcedureElementTypes.RobotProcedure_3022);
		types.add(ProcedureElementTypes.RobotProcedure_3015);
		types.add(ProcedureElementTypes.RobotProcedure_2004);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.RobotProcedure3CreationTool_title,
				Messages.RobotProcedure3CreationTool_desc, types);
		entry.setId("createRobotProcedure3CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ProcedureElementTypes
				.getImageDescriptor(ProcedureElementTypes.RobotProcedure_3002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRobotTask4CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(5);
		types.add(ProcedureElementTypes.RobotTaskEntity_2001);
		types.add(ProcedureElementTypes.RobotTaskEntity_3001);
		types.add(ProcedureElementTypes.RobotTaskEntity_3003);
		types.add(ProcedureElementTypes.RobotTaskEntity_3021);
		types.add(ProcedureElementTypes.RobotTaskEntity_3014);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.RobotTask4CreationTool_title,
				Messages.RobotTask4CreationTool_desc, types);
		entry.setId("createRobotTask4CreationTool"); //$NON-NLS-1$
		entry
				.setSmallIcon(ProcedureElementTypes
						.getImageDescriptor(ProcedureElementTypes.RobotTaskEntity_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createLink5CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(ProcedureElementTypes.LinkEntities_4001);
		LinkToolEntry entry = new LinkToolEntry(
				Messages.Link5CreationTool_title,
				Messages.Link5CreationTool_desc, types);
		entry.setId("createLink5CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ProcedureElementTypes
				.getImageDescriptor(ProcedureElementTypes.LinkEntities_4001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createLoop6CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(5);
		types.add(ProcedureElementTypes.Loop_3013);
		types.add(ProcedureElementTypes.Loop_3016);
		types.add(ProcedureElementTypes.Loop_3025);
		types.add(ProcedureElementTypes.Loop_3020);
		types.add(ProcedureElementTypes.Loop_2005);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.Loop6CreationTool_title,
				Messages.Loop6CreationTool_desc, types);
		entry.setId("createLoop6CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ProcedureElementTypes
				.getImageDescriptor(ProcedureElementTypes.Loop_3013));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createRobotProcedureCondition7CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(2);
		types.add(ProcedureElementTypes.Condition_3026);
		types.add(ProcedureElementTypes.Condition_2006);
		NodeToolEntry entry = new NodeToolEntry(
				Messages.RobotProcedureCondition7CreationTool_title,
				Messages.RobotProcedureCondition7CreationTool_desc, types);
		entry.setId("createRobotProcedureCondition7CreationTool"); //$NON-NLS-1$
		entry.setSmallIcon(ProcedureElementTypes
				.getImageDescriptor(ProcedureElementTypes.Condition_3026));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
