package procedure.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;

import procedure.ProcedurePackage;
import procedure.RobotProcedure;
import procedure.diagram.edit.parts.Condition2EditPart;
import procedure.diagram.edit.parts.ConditionEditPart;
import procedure.diagram.edit.parts.ConditionName2EditPart;
import procedure.diagram.edit.parts.ConditionNameEditPart;
import procedure.diagram.edit.parts.LinkEntitiesEditPart;
import procedure.diagram.edit.parts.Loop2EditPart;
import procedure.diagram.edit.parts.Loop3EditPart;
import procedure.diagram.edit.parts.Loop4EditPart;
import procedure.diagram.edit.parts.Loop5EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen2EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen3EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen4EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen5EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGenEditPart;
import procedure.diagram.edit.parts.LoopCondition2EditPart;
import procedure.diagram.edit.parts.LoopCondition3EditPart;
import procedure.diagram.edit.parts.LoopCondition4EditPart;
import procedure.diagram.edit.parts.LoopCondition5EditPart;
import procedure.diagram.edit.parts.LoopConditionEditPart;
import procedure.diagram.edit.parts.LoopEditPart;
import procedure.diagram.edit.parts.Parallel2EditPart;
import procedure.diagram.edit.parts.Parallel3EditPart;
import procedure.diagram.edit.parts.Parallel4EditPart;
import procedure.diagram.edit.parts.Parallel5EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen2EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen3EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen4EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen5EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGenEditPart;
import procedure.diagram.edit.parts.ParallelEditPart;
import procedure.diagram.edit.parts.RobotProcedure2EditPart;
import procedure.diagram.edit.parts.RobotProcedure3EditPart;
import procedure.diagram.edit.parts.RobotProcedure4EditPart;
import procedure.diagram.edit.parts.RobotProcedure5EditPart;
import procedure.diagram.edit.parts.RobotProcedure6EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen2EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen3EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen4EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen5EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGenEditPart;
import procedure.diagram.edit.parts.RobotProcedureEditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditions2EditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditions3EditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditions5EditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditionsEditPart;
import procedure.diagram.edit.parts.RobotProcedureNameEditPart;
import procedure.diagram.edit.parts.RobotTaskEntity2EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity3EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity4EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity5EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityCompartmentGen2EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityCompartmentGen3EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityCompartmentGen4EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityCompartmentGen5EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityCompartmentGenEditPart;
import procedure.diagram.edit.parts.RobotTaskEntityEditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName2EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName3EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName4EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName5EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityNameEditPart;
import procedure.diagram.edit.parts.Sequence2EditPart;
import procedure.diagram.edit.parts.Sequence3EditPart;
import procedure.diagram.edit.parts.Sequence4EditPart;
import procedure.diagram.edit.parts.Sequence5EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen2EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen3EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen4EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen5EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGenEditPart;
import procedure.diagram.edit.parts.SequenceEditPart;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class ProcedureVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "fr.inrialpes.necs.orccad.procedure.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (RobotProcedureEditPart.MODEL_ID.equals(view.getType())) {
				return RobotProcedureEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return procedure.diagram.part.ProcedureVisualIDRegistry
				.getVisualID(view.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				ProcedureDiagramEditorPlugin.getInstance().logError(
						"Unable to parse view type as a visualID number: "
								+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return String.valueOf(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((RobotProcedure) domainElement)) {
			return RobotProcedureEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = procedure.diagram.part.ProcedureVisualIDRegistry
				.getModelID(containerView);
		if (!RobotProcedureEditPart.MODEL_ID.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (RobotProcedureEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = procedure.diagram.part.ProcedureVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = RobotProcedureEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case ParallelCompartmentGenEditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop2EditPart.VISUAL_ID;
			}
			break;
		case RobotProcedureCompartmentGenEditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getCondition().isSuperTypeOf(
					domainElement.eClass())) {
				return Condition2EditPart.VISUAL_ID;
			}
			break;
		case RobotProcedureCompartmentGen2EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getCondition().isSuperTypeOf(
					domainElement.eClass())) {
				return Condition2EditPart.VISUAL_ID;
			}
			break;
		case SequenceCompartmentGenEditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop4EditPart.VISUAL_ID;
			}
			break;
		case RobotProcedureCompartmentGen3EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getCondition().isSuperTypeOf(
					domainElement.eClass())) {
				return Condition2EditPart.VISUAL_ID;
			}
			break;
		case ParallelCompartmentGen2EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop2EditPart.VISUAL_ID;
			}
			break;
		case SequenceCompartmentGen2EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop4EditPart.VISUAL_ID;
			}
			break;
		case SequenceCompartmentGen3EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop4EditPart.VISUAL_ID;
			}
			break;
		case ParallelCompartmentGen3EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop2EditPart.VISUAL_ID;
			}
			break;
		case ParallelCompartmentGen4EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop2EditPart.VISUAL_ID;
			}
			break;
		case LoopCompartmentGenEditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure6EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop5EditPart.VISUAL_ID;
			}
			break;
		case RobotProcedureCompartmentGen4EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getCondition().isSuperTypeOf(
					domainElement.eClass())) {
				return Condition2EditPart.VISUAL_ID;
			}
			break;
		case LoopCompartmentGen2EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure6EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop5EditPart.VISUAL_ID;
			}
			break;
		case SequenceCompartmentGen4EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop4EditPart.VISUAL_ID;
			}
			break;
		case LoopCompartmentGen3EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure6EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop5EditPart.VISUAL_ID;
			}
			break;
		case ParallelCompartmentGen5EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop2EditPart.VISUAL_ID;
			}
			break;
		case LoopCompartmentGen4EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure6EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop5EditPart.VISUAL_ID;
			}
			break;
		case SequenceCompartmentGen5EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop4EditPart.VISUAL_ID;
			}
			break;
		case RobotProcedureCompartmentGen5EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure4EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop3EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getCondition().isSuperTypeOf(
					domainElement.eClass())) {
				return Condition2EditPart.VISUAL_ID;
			}
			break;
		case LoopCompartmentGen5EditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntity5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure6EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return Sequence5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return Parallel5EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return Loop5EditPart.VISUAL_ID;
			}
			break;
		case RobotProcedureEditPart.VISUAL_ID:
			if (ProcedurePackage.eINSTANCE.getRobotTaskEntity().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotTaskEntityEditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getParallel().isSuperTypeOf(
					domainElement.eClass())) {
				return ParallelEditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getSequence().isSuperTypeOf(
					domainElement.eClass())) {
				return SequenceEditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getRobotProcedure().isSuperTypeOf(
					domainElement.eClass())) {
				return RobotProcedure2EditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getLoop().isSuperTypeOf(
					domainElement.eClass())) {
				return LoopEditPart.VISUAL_ID;
			}
			if (ProcedurePackage.eINSTANCE.getCondition().isSuperTypeOf(
					domainElement.eClass())) {
				return ConditionEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = procedure.diagram.part.ProcedureVisualIDRegistry
				.getModelID(containerView);
		if (!RobotProcedureEditPart.MODEL_ID.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (RobotProcedureEditPart.MODEL_ID.equals(containerModelID)) {
			containerVisualID = procedure.diagram.part.ProcedureVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = RobotProcedureEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case RobotTaskEntityEditPart.VISUAL_ID:
			if (RobotTaskEntityNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotTaskEntityCompartmentGenEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ParallelEditPart.VISUAL_ID:
			if (ParallelCompartmentGenEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SequenceEditPart.VISUAL_ID:
			if (SequenceCompartmentGen5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotProcedure2EditPart.VISUAL_ID:
			if (RobotProcedureNameConditionsEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedureCompartmentGen5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LoopEditPart.VISUAL_ID:
			if (LoopConditionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (LoopCompartmentGen5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ConditionEditPart.VISUAL_ID:
			if (ConditionNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotTaskEntity2EditPart.VISUAL_ID:
			if (RobotTaskEntityName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotTaskEntityCompartmentGen2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotProcedure3EditPart.VISUAL_ID:
			if (RobotProcedureNameConditions2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedureCompartmentGenEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotTaskEntity3EditPart.VISUAL_ID:
			if (RobotTaskEntityName3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotTaskEntityCompartmentGen3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotProcedure4EditPart.VISUAL_ID:
			if (RobotProcedureNameConditions3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedureCompartmentGen2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Sequence2EditPart.VISUAL_ID:
			if (SequenceCompartmentGenEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotTaskEntity4EditPart.VISUAL_ID:
			if (RobotTaskEntityName4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotTaskEntityCompartmentGen4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotProcedure5EditPart.VISUAL_ID:
			if (RobotProcedureNameEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedureCompartmentGen3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Parallel2EditPart.VISUAL_ID:
			if (ParallelCompartmentGen2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Sequence3EditPart.VISUAL_ID:
			if (SequenceCompartmentGen2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Sequence4EditPart.VISUAL_ID:
			if (SequenceCompartmentGen3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Parallel3EditPart.VISUAL_ID:
			if (ParallelCompartmentGen3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Parallel4EditPart.VISUAL_ID:
			if (ParallelCompartmentGen4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Loop2EditPart.VISUAL_ID:
			if (LoopCondition2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (LoopCompartmentGenEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotTaskEntity5EditPart.VISUAL_ID:
			if (RobotTaskEntityName5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotTaskEntityCompartmentGen5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotProcedure6EditPart.VISUAL_ID:
			if (RobotProcedureNameConditions5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedureCompartmentGen4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Loop3EditPart.VISUAL_ID:
			if (LoopCondition3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (LoopCompartmentGen2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Sequence5EditPart.VISUAL_ID:
			if (SequenceCompartmentGen4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Loop4EditPart.VISUAL_ID:
			if (LoopCondition4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (LoopCompartmentGen3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Parallel5EditPart.VISUAL_ID:
			if (ParallelCompartmentGen5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Loop5EditPart.VISUAL_ID:
			if (LoopCondition5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (LoopCompartmentGen4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Condition2EditPart.VISUAL_ID:
			if (ConditionName2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ParallelCompartmentGenEditPart.VISUAL_ID:
			if (RobotTaskEntity2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotProcedureCompartmentGenEditPart.VISUAL_ID:
			if (RobotTaskEntity3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Condition2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotProcedureCompartmentGen2EditPart.VISUAL_ID:
			if (RobotTaskEntity3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Condition2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SequenceCompartmentGenEditPart.VISUAL_ID:
			if (RobotTaskEntity4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotProcedureCompartmentGen3EditPart.VISUAL_ID:
			if (RobotTaskEntity3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Condition2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ParallelCompartmentGen2EditPart.VISUAL_ID:
			if (RobotTaskEntity2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SequenceCompartmentGen2EditPart.VISUAL_ID:
			if (RobotTaskEntity4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SequenceCompartmentGen3EditPart.VISUAL_ID:
			if (RobotTaskEntity4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ParallelCompartmentGen3EditPart.VISUAL_ID:
			if (RobotTaskEntity2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ParallelCompartmentGen4EditPart.VISUAL_ID:
			if (RobotTaskEntity2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LoopCompartmentGenEditPart.VISUAL_ID:
			if (RobotTaskEntity5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure6EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotProcedureCompartmentGen4EditPart.VISUAL_ID:
			if (RobotTaskEntity3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Condition2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LoopCompartmentGen2EditPart.VISUAL_ID:
			if (RobotTaskEntity5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure6EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SequenceCompartmentGen4EditPart.VISUAL_ID:
			if (RobotTaskEntity4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LoopCompartmentGen3EditPart.VISUAL_ID:
			if (RobotTaskEntity5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure6EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case ParallelCompartmentGen5EditPart.VISUAL_ID:
			if (RobotTaskEntity2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LoopCompartmentGen4EditPart.VISUAL_ID:
			if (RobotTaskEntity5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure6EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case SequenceCompartmentGen5EditPart.VISUAL_ID:
			if (RobotTaskEntity4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotProcedureCompartmentGen5EditPart.VISUAL_ID:
			if (RobotTaskEntity3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure4EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop3EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Condition2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case LoopCompartmentGen5EditPart.VISUAL_ID:
			if (RobotTaskEntity5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure6EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Sequence5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Parallel5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Loop5EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case RobotProcedureEditPart.VISUAL_ID:
			if (RobotTaskEntityEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ParallelEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (SequenceEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (RobotProcedure2EditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (LoopEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (ConditionEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (ProcedurePackage.eINSTANCE.getLinkEntities().isSuperTypeOf(
				domainElement.eClass())) {
			return LinkEntitiesEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(RobotProcedure element) {
		return true;
	}

}
