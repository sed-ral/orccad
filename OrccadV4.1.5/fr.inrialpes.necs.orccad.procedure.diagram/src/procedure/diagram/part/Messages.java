package procedure.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String ProcedureCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String ProcedureCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String ProcedureCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String ProcedureCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String ProcedureCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String ProcedureCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String ProcedureCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String ProcedureCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String ProcedureDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String ProcedureDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String ProcedureDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String ProcedureDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String ProcedureDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String ProcedureDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String ProcedureDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String ProcedureDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String ProcedureDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String ProcedureDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String ProcedureDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String ProcedureDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String ProcedureDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_ResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_WizardTitle;

	/**
	 * @generated
	 */
	public static String InitDiagramFile_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String ProcedureNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String ProcedureNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String ProcedureNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String ProcedureNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String ProcedureNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String ProcedureNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String ProcedureNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String ProcedureNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String ProcedureNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String ProcedureNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String ProcedureNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String ProcedureDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String ProcedureDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String ProcedureDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String ProcedureDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String ProcedureDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String ProcedureElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Procedure1Group_title;

	/**
	 * @generated
	 */
	public static String Sequence1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Sequence1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Parallel2CreationTool_title;

	/**
	 * @generated
	 */
	public static String Parallel2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RobotProcedure3CreationTool_title;

	/**
	 * @generated
	 */
	public static String RobotProcedure3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RobotTask4CreationTool_title;

	/**
	 * @generated
	 */
	public static String RobotTask4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Link5CreationTool_title;

	/**
	 * @generated
	 */
	public static String Link5CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Loop6CreationTool_title;

	/**
	 * @generated
	 */
	public static String Loop6CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RobotProcedureCondition7CreationTool_title;

	/**
	 * @generated
	 */
	public static String RobotProcedureCondition7CreationTool_desc;

	/**
	 * @generated
	 */
	public static String RobotTaskEntityCompartmentGenEditPart_title;

	/**
	 * @generated
	 */
	public static String ParallelCompartmentGenEditPart_title;

	/**
	 * @generated
	 */
	public static String RobotTaskEntityCompartmentGen2EditPart_title;

	/**
	 * @generated
	 */
	public static String RobotProcedureCompartmentGenEditPart_title;

	/**
	 * @generated
	 */
	public static String RobotTaskEntityCompartmentGen3EditPart_title;

	/**
	 * @generated
	 */
	public static String RobotProcedureCompartmentGen2EditPart_title;

	/**
	 * @generated
	 */
	public static String SequenceCompartmentGenEditPart_title;

	/**
	 * @generated
	 */
	public static String RobotTaskEntityCompartmentGen4EditPart_title;

	/**
	 * @generated
	 */
	public static String RobotProcedureCompartmentGen3EditPart_title;

	/**
	 * @generated
	 */
	public static String ParallelCompartmentGen2EditPart_title;

	/**
	 * @generated
	 */
	public static String SequenceCompartmentGen2EditPart_title;

	/**
	 * @generated
	 */
	public static String SequenceCompartmentGen3EditPart_title;

	/**
	 * @generated
	 */
	public static String ParallelCompartmentGen3EditPart_title;

	/**
	 * @generated
	 */
	public static String ParallelCompartmentGen4EditPart_title;

	/**
	 * @generated
	 */
	public static String LoopCompartmentGenEditPart_title;

	/**
	 * @generated
	 */
	public static String RobotTaskEntityCompartmentGen5EditPart_title;

	/**
	 * @generated
	 */
	public static String RobotProcedureCompartmentGen4EditPart_title;

	/**
	 * @generated
	 */
	public static String LoopCompartmentGen2EditPart_title;

	/**
	 * @generated
	 */
	public static String SequenceCompartmentGen4EditPart_title;

	/**
	 * @generated
	 */
	public static String LoopCompartmentGen3EditPart_title;

	/**
	 * @generated
	 */
	public static String ParallelCompartmentGen5EditPart_title;

	/**
	 * @generated
	 */
	public static String LoopCompartmentGen4EditPart_title;

	/**
	 * @generated
	 */
	public static String SequenceCompartmentGen5EditPart_title;

	/**
	 * @generated
	 */
	public static String RobotProcedureCompartmentGen5EditPart_title;

	/**
	 * @generated
	 */
	public static String LoopCompartmentGen5EditPart_title;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotProcedure_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotTaskEntity_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotTaskEntity_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Parallel_2002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Parallel_2002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Sequence_2003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Sequence_2003_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotProcedure_2004_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotProcedure_2004_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Loop_2005_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Loop_2005_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotTaskEntity_3001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotTaskEntity_3001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotProcedure_3002_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotProcedure_3002_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotTaskEntity_3003_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotTaskEntity_3003_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotProcedure_3004_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotProcedure_3004_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Sequence_3005_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Sequence_3005_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotTaskEntity_3021_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotTaskEntity_3021_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotProcedure_3022_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotProcedure_3022_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Parallel_3008_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Parallel_3008_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Sequence_3009_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Sequence_3009_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Sequence_3023_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Sequence_3023_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Parallel_3024_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Parallel_3024_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Parallel_3012_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Parallel_3012_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Loop_3013_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Loop_3013_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotTaskEntity_3014_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotTaskEntity_3014_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotProcedure_3015_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_RobotProcedure_3015_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Loop_3016_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Loop_3016_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Sequence_3017_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Sequence_3017_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Loop_3025_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Loop_3025_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Parallel_3019_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Parallel_3019_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Loop_3020_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Loop_3020_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LinkEntities_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_LinkEntities_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnexpectedValueType;

	/**
	 * @generated
	 */
	public static String AbstractParser_WrongStringConversion;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnknownLiteral;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String ProcedureModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String ProcedureModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
