package procedure.diagram.providers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.modelingassistant.ModelingAssistantProvider;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.dialogs.ElementListSelectionDialog;

import procedure.diagram.edit.parts.Loop2EditPart;
import procedure.diagram.edit.parts.Loop3EditPart;
import procedure.diagram.edit.parts.Loop4EditPart;
import procedure.diagram.edit.parts.Loop5EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen2EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen3EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen4EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGen5EditPart;
import procedure.diagram.edit.parts.LoopCompartmentGenEditPart;
import procedure.diagram.edit.parts.LoopEditPart;
import procedure.diagram.edit.parts.Parallel2EditPart;
import procedure.diagram.edit.parts.Parallel3EditPart;
import procedure.diagram.edit.parts.Parallel4EditPart;
import procedure.diagram.edit.parts.Parallel5EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen2EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen3EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen4EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGen5EditPart;
import procedure.diagram.edit.parts.ParallelCompartmentGenEditPart;
import procedure.diagram.edit.parts.ParallelEditPart;
import procedure.diagram.edit.parts.RobotProcedure2EditPart;
import procedure.diagram.edit.parts.RobotProcedure3EditPart;
import procedure.diagram.edit.parts.RobotProcedure4EditPart;
import procedure.diagram.edit.parts.RobotProcedure5EditPart;
import procedure.diagram.edit.parts.RobotProcedure6EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen2EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen3EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen4EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGen5EditPart;
import procedure.diagram.edit.parts.RobotProcedureCompartmentGenEditPart;
import procedure.diagram.edit.parts.RobotProcedureEditPart;
import procedure.diagram.edit.parts.RobotTaskEntity2EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity3EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity4EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity5EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityEditPart;
import procedure.diagram.edit.parts.Sequence2EditPart;
import procedure.diagram.edit.parts.Sequence3EditPart;
import procedure.diagram.edit.parts.Sequence4EditPart;
import procedure.diagram.edit.parts.Sequence5EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen2EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen3EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen4EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGen5EditPart;
import procedure.diagram.edit.parts.SequenceCompartmentGenEditPart;
import procedure.diagram.edit.parts.SequenceEditPart;
import procedure.diagram.part.Messages;
import procedure.diagram.part.ProcedureDiagramEditorPlugin;

/**
 * @generated
 */
public class ProcedureModelingAssistantProvider extends
		ModelingAssistantProvider {

	/**
	 * @generated
	 */
	public List getTypesForPopupBar(IAdaptable host) {
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart instanceof ParallelCompartmentGenEditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3001);
			types.add(ProcedureElementTypes.RobotProcedure_3002);
			types.add(ProcedureElementTypes.Sequence_3009);
			types.add(ProcedureElementTypes.Parallel_3012);
			types.add(ProcedureElementTypes.Loop_3013);
			return types;
		}
		if (editPart instanceof RobotProcedureCompartmentGenEditPart) {
			ArrayList types = new ArrayList(6);
			types.add(ProcedureElementTypes.RobotTaskEntity_3003);
			types.add(ProcedureElementTypes.RobotProcedure_3004);
			types.add(ProcedureElementTypes.Sequence_3005);
			types.add(ProcedureElementTypes.Parallel_3008);
			types.add(ProcedureElementTypes.Loop_3016);
			types.add(ProcedureElementTypes.Condition_3026);
			return types;
		}
		if (editPart instanceof RobotProcedureCompartmentGen2EditPart) {
			ArrayList types = new ArrayList(6);
			types.add(ProcedureElementTypes.RobotTaskEntity_3003);
			types.add(ProcedureElementTypes.RobotProcedure_3004);
			types.add(ProcedureElementTypes.Sequence_3005);
			types.add(ProcedureElementTypes.Parallel_3008);
			types.add(ProcedureElementTypes.Loop_3016);
			types.add(ProcedureElementTypes.Condition_3026);
			return types;
		}
		if (editPart instanceof SequenceCompartmentGenEditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3021);
			types.add(ProcedureElementTypes.RobotProcedure_3022);
			types.add(ProcedureElementTypes.Sequence_3023);
			types.add(ProcedureElementTypes.Parallel_3024);
			types.add(ProcedureElementTypes.Loop_3025);
			return types;
		}
		if (editPart instanceof RobotProcedureCompartmentGen3EditPart) {
			ArrayList types = new ArrayList(6);
			types.add(ProcedureElementTypes.RobotTaskEntity_3003);
			types.add(ProcedureElementTypes.RobotProcedure_3004);
			types.add(ProcedureElementTypes.Sequence_3005);
			types.add(ProcedureElementTypes.Parallel_3008);
			types.add(ProcedureElementTypes.Loop_3016);
			types.add(ProcedureElementTypes.Condition_3026);
			return types;
		}
		if (editPart instanceof ParallelCompartmentGen2EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3001);
			types.add(ProcedureElementTypes.RobotProcedure_3002);
			types.add(ProcedureElementTypes.Sequence_3009);
			types.add(ProcedureElementTypes.Parallel_3012);
			types.add(ProcedureElementTypes.Loop_3013);
			return types;
		}
		if (editPart instanceof SequenceCompartmentGen2EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3021);
			types.add(ProcedureElementTypes.RobotProcedure_3022);
			types.add(ProcedureElementTypes.Sequence_3023);
			types.add(ProcedureElementTypes.Parallel_3024);
			types.add(ProcedureElementTypes.Loop_3025);
			return types;
		}
		if (editPart instanceof SequenceCompartmentGen3EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3021);
			types.add(ProcedureElementTypes.RobotProcedure_3022);
			types.add(ProcedureElementTypes.Sequence_3023);
			types.add(ProcedureElementTypes.Parallel_3024);
			types.add(ProcedureElementTypes.Loop_3025);
			return types;
		}
		if (editPart instanceof ParallelCompartmentGen3EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3001);
			types.add(ProcedureElementTypes.RobotProcedure_3002);
			types.add(ProcedureElementTypes.Sequence_3009);
			types.add(ProcedureElementTypes.Parallel_3012);
			types.add(ProcedureElementTypes.Loop_3013);
			return types;
		}
		if (editPart instanceof ParallelCompartmentGen4EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3001);
			types.add(ProcedureElementTypes.RobotProcedure_3002);
			types.add(ProcedureElementTypes.Sequence_3009);
			types.add(ProcedureElementTypes.Parallel_3012);
			types.add(ProcedureElementTypes.Loop_3013);
			return types;
		}
		if (editPart instanceof LoopCompartmentGenEditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3014);
			types.add(ProcedureElementTypes.RobotProcedure_3015);
			types.add(ProcedureElementTypes.Sequence_3017);
			types.add(ProcedureElementTypes.Parallel_3019);
			types.add(ProcedureElementTypes.Loop_3020);
			return types;
		}
		if (editPart instanceof RobotProcedureCompartmentGen4EditPart) {
			ArrayList types = new ArrayList(6);
			types.add(ProcedureElementTypes.RobotTaskEntity_3003);
			types.add(ProcedureElementTypes.RobotProcedure_3004);
			types.add(ProcedureElementTypes.Sequence_3005);
			types.add(ProcedureElementTypes.Parallel_3008);
			types.add(ProcedureElementTypes.Loop_3016);
			types.add(ProcedureElementTypes.Condition_3026);
			return types;
		}
		if (editPart instanceof LoopCompartmentGen2EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3014);
			types.add(ProcedureElementTypes.RobotProcedure_3015);
			types.add(ProcedureElementTypes.Sequence_3017);
			types.add(ProcedureElementTypes.Parallel_3019);
			types.add(ProcedureElementTypes.Loop_3020);
			return types;
		}
		if (editPart instanceof SequenceCompartmentGen4EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3021);
			types.add(ProcedureElementTypes.RobotProcedure_3022);
			types.add(ProcedureElementTypes.Sequence_3023);
			types.add(ProcedureElementTypes.Parallel_3024);
			types.add(ProcedureElementTypes.Loop_3025);
			return types;
		}
		if (editPart instanceof LoopCompartmentGen3EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3014);
			types.add(ProcedureElementTypes.RobotProcedure_3015);
			types.add(ProcedureElementTypes.Sequence_3017);
			types.add(ProcedureElementTypes.Parallel_3019);
			types.add(ProcedureElementTypes.Loop_3020);
			return types;
		}
		if (editPart instanceof ParallelCompartmentGen5EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3001);
			types.add(ProcedureElementTypes.RobotProcedure_3002);
			types.add(ProcedureElementTypes.Sequence_3009);
			types.add(ProcedureElementTypes.Parallel_3012);
			types.add(ProcedureElementTypes.Loop_3013);
			return types;
		}
		if (editPart instanceof LoopCompartmentGen4EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3014);
			types.add(ProcedureElementTypes.RobotProcedure_3015);
			types.add(ProcedureElementTypes.Sequence_3017);
			types.add(ProcedureElementTypes.Parallel_3019);
			types.add(ProcedureElementTypes.Loop_3020);
			return types;
		}
		if (editPart instanceof SequenceCompartmentGen5EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3021);
			types.add(ProcedureElementTypes.RobotProcedure_3022);
			types.add(ProcedureElementTypes.Sequence_3023);
			types.add(ProcedureElementTypes.Parallel_3024);
			types.add(ProcedureElementTypes.Loop_3025);
			return types;
		}
		if (editPart instanceof RobotProcedureCompartmentGen5EditPart) {
			ArrayList types = new ArrayList(6);
			types.add(ProcedureElementTypes.RobotTaskEntity_3003);
			types.add(ProcedureElementTypes.RobotProcedure_3004);
			types.add(ProcedureElementTypes.Sequence_3005);
			types.add(ProcedureElementTypes.Parallel_3008);
			types.add(ProcedureElementTypes.Loop_3016);
			types.add(ProcedureElementTypes.Condition_3026);
			return types;
		}
		if (editPart instanceof LoopCompartmentGen5EditPart) {
			ArrayList types = new ArrayList(5);
			types.add(ProcedureElementTypes.RobotTaskEntity_3014);
			types.add(ProcedureElementTypes.RobotProcedure_3015);
			types.add(ProcedureElementTypes.Sequence_3017);
			types.add(ProcedureElementTypes.Parallel_3019);
			types.add(ProcedureElementTypes.Loop_3020);
			return types;
		}
		if (editPart instanceof RobotProcedureEditPart) {
			ArrayList types = new ArrayList(6);
			types.add(ProcedureElementTypes.RobotTaskEntity_2001);
			types.add(ProcedureElementTypes.Parallel_2002);
			types.add(ProcedureElementTypes.Sequence_2003);
			types.add(ProcedureElementTypes.RobotProcedure_2004);
			types.add(ProcedureElementTypes.Loop_2005);
			types.add(ProcedureElementTypes.Condition_2006);
			return types;
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof RobotTaskEntityEditPart) {
			return ((RobotTaskEntityEditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof ParallelEditPart) {
			return ((ParallelEditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof SequenceEditPart) {
			return ((SequenceEditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof RobotProcedure2EditPart) {
			return ((RobotProcedure2EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof LoopEditPart) {
			return ((LoopEditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof RobotTaskEntity2EditPart) {
			return ((RobotTaskEntity2EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof RobotProcedure3EditPart) {
			return ((RobotProcedure3EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof RobotTaskEntity3EditPart) {
			return ((RobotTaskEntity3EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof RobotProcedure4EditPart) {
			return ((RobotProcedure4EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Sequence2EditPart) {
			return ((Sequence2EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof RobotTaskEntity4EditPart) {
			return ((RobotTaskEntity4EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof RobotProcedure5EditPart) {
			return ((RobotProcedure5EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Parallel2EditPart) {
			return ((Parallel2EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Sequence3EditPart) {
			return ((Sequence3EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Sequence4EditPart) {
			return ((Sequence4EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Parallel3EditPart) {
			return ((Parallel3EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Parallel4EditPart) {
			return ((Parallel4EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Loop2EditPart) {
			return ((Loop2EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof RobotTaskEntity5EditPart) {
			return ((RobotTaskEntity5EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof RobotProcedure6EditPart) {
			return ((RobotProcedure6EditPart) sourceEditPart)
					.getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Loop3EditPart) {
			return ((Loop3EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Sequence5EditPart) {
			return ((Sequence5EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Loop4EditPart) {
			return ((Loop4EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Parallel5EditPart) {
			return ((Parallel5EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		if (sourceEditPart instanceof Loop5EditPart) {
			return ((Loop5EditPart) sourceEditPart).getMARelTypesOnSource();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof RobotTaskEntityEditPart) {
			return ((RobotTaskEntityEditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof ParallelEditPart) {
			return ((ParallelEditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof SequenceEditPart) {
			return ((SequenceEditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof RobotProcedure2EditPart) {
			return ((RobotProcedure2EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof LoopEditPart) {
			return ((LoopEditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof RobotTaskEntity2EditPart) {
			return ((RobotTaskEntity2EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof RobotProcedure3EditPart) {
			return ((RobotProcedure3EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof RobotTaskEntity3EditPart) {
			return ((RobotTaskEntity3EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof RobotProcedure4EditPart) {
			return ((RobotProcedure4EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Sequence2EditPart) {
			return ((Sequence2EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof RobotTaskEntity4EditPart) {
			return ((RobotTaskEntity4EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof RobotProcedure5EditPart) {
			return ((RobotProcedure5EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Parallel2EditPart) {
			return ((Parallel2EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Sequence3EditPart) {
			return ((Sequence3EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Sequence4EditPart) {
			return ((Sequence4EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Parallel3EditPart) {
			return ((Parallel3EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Parallel4EditPart) {
			return ((Parallel4EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Loop2EditPart) {
			return ((Loop2EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof RobotTaskEntity5EditPart) {
			return ((RobotTaskEntity5EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof RobotProcedure6EditPart) {
			return ((RobotProcedure6EditPart) targetEditPart)
					.getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Loop3EditPart) {
			return ((Loop3EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Sequence5EditPart) {
			return ((Sequence5EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Loop4EditPart) {
			return ((Loop4EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Parallel5EditPart) {
			return ((Parallel5EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		if (targetEditPart instanceof Loop5EditPart) {
			return ((Loop5EditPart) targetEditPart).getMARelTypesOnTarget();
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getRelTypesOnSourceAndTarget(IAdaptable source,
			IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof RobotTaskEntityEditPart) {
			return ((RobotTaskEntityEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof ParallelEditPart) {
			return ((ParallelEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof SequenceEditPart) {
			return ((SequenceEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof RobotProcedure2EditPart) {
			return ((RobotProcedure2EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof LoopEditPart) {
			return ((LoopEditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof RobotTaskEntity2EditPart) {
			return ((RobotTaskEntity2EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof RobotProcedure3EditPart) {
			return ((RobotProcedure3EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof RobotTaskEntity3EditPart) {
			return ((RobotTaskEntity3EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof RobotProcedure4EditPart) {
			return ((RobotProcedure4EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Sequence2EditPart) {
			return ((Sequence2EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof RobotTaskEntity4EditPart) {
			return ((RobotTaskEntity4EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof RobotProcedure5EditPart) {
			return ((RobotProcedure5EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Parallel2EditPart) {
			return ((Parallel2EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Sequence3EditPart) {
			return ((Sequence3EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Sequence4EditPart) {
			return ((Sequence4EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Parallel3EditPart) {
			return ((Parallel3EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Parallel4EditPart) {
			return ((Parallel4EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Loop2EditPart) {
			return ((Loop2EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof RobotTaskEntity5EditPart) {
			return ((RobotTaskEntity5EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof RobotProcedure6EditPart) {
			return ((RobotProcedure6EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Loop3EditPart) {
			return ((Loop3EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Sequence5EditPart) {
			return ((Sequence5EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Loop4EditPart) {
			return ((Loop4EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Parallel5EditPart) {
			return ((Parallel5EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		if (sourceEditPart instanceof Loop5EditPart) {
			return ((Loop5EditPart) sourceEditPart)
					.getMARelTypesOnSourceAndTarget(targetEditPart);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getTypesForSource(IAdaptable target,
			IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target
				.getAdapter(IGraphicalEditPart.class);
		if (targetEditPart instanceof RobotTaskEntityEditPart) {
			return ((RobotTaskEntityEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof ParallelEditPart) {
			return ((ParallelEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof SequenceEditPart) {
			return ((SequenceEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof RobotProcedure2EditPart) {
			return ((RobotProcedure2EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof LoopEditPart) {
			return ((LoopEditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof RobotTaskEntity2EditPart) {
			return ((RobotTaskEntity2EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof RobotProcedure3EditPart) {
			return ((RobotProcedure3EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof RobotTaskEntity3EditPart) {
			return ((RobotTaskEntity3EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof RobotProcedure4EditPart) {
			return ((RobotProcedure4EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Sequence2EditPart) {
			return ((Sequence2EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof RobotTaskEntity4EditPart) {
			return ((RobotTaskEntity4EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof RobotProcedure5EditPart) {
			return ((RobotProcedure5EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Parallel2EditPart) {
			return ((Parallel2EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Sequence3EditPart) {
			return ((Sequence3EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Sequence4EditPart) {
			return ((Sequence4EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Parallel3EditPart) {
			return ((Parallel3EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Parallel4EditPart) {
			return ((Parallel4EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Loop2EditPart) {
			return ((Loop2EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof RobotTaskEntity5EditPart) {
			return ((RobotTaskEntity5EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof RobotProcedure6EditPart) {
			return ((RobotProcedure6EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Loop3EditPart) {
			return ((Loop3EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Sequence5EditPart) {
			return ((Sequence5EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Loop4EditPart) {
			return ((Loop4EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Parallel5EditPart) {
			return ((Parallel5EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		if (targetEditPart instanceof Loop5EditPart) {
			return ((Loop5EditPart) targetEditPart)
					.getMATypesForSource(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public List getTypesForTarget(IAdaptable source,
			IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source
				.getAdapter(IGraphicalEditPart.class);
		if (sourceEditPart instanceof RobotTaskEntityEditPart) {
			return ((RobotTaskEntityEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof ParallelEditPart) {
			return ((ParallelEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof SequenceEditPart) {
			return ((SequenceEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof RobotProcedure2EditPart) {
			return ((RobotProcedure2EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof LoopEditPart) {
			return ((LoopEditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof RobotTaskEntity2EditPart) {
			return ((RobotTaskEntity2EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof RobotProcedure3EditPart) {
			return ((RobotProcedure3EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof RobotTaskEntity3EditPart) {
			return ((RobotTaskEntity3EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof RobotProcedure4EditPart) {
			return ((RobotProcedure4EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Sequence2EditPart) {
			return ((Sequence2EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof RobotTaskEntity4EditPart) {
			return ((RobotTaskEntity4EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof RobotProcedure5EditPart) {
			return ((RobotProcedure5EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Parallel2EditPart) {
			return ((Parallel2EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Sequence3EditPart) {
			return ((Sequence3EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Sequence4EditPart) {
			return ((Sequence4EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Parallel3EditPart) {
			return ((Parallel3EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Parallel4EditPart) {
			return ((Parallel4EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Loop2EditPart) {
			return ((Loop2EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof RobotTaskEntity5EditPart) {
			return ((RobotTaskEntity5EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof RobotProcedure6EditPart) {
			return ((RobotProcedure6EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Loop3EditPart) {
			return ((Loop3EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Sequence5EditPart) {
			return ((Sequence5EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Loop4EditPart) {
			return ((Loop4EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Parallel5EditPart) {
			return ((Parallel5EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		if (sourceEditPart instanceof Loop5EditPart) {
			return ((Loop5EditPart) sourceEditPart)
					.getMATypesForTarget(relationshipType);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForSource(IAdaptable target,
			IElementType relationshipType) {
		return selectExistingElement(target, getTypesForSource(target,
				relationshipType));
	}

	/**
	 * @generated
	 */
	public EObject selectExistingElementForTarget(IAdaptable source,
			IElementType relationshipType) {
		return selectExistingElement(source, getTypesForTarget(source,
				relationshipType));
	}

	/**
	 * @generated
	 */
	protected EObject selectExistingElement(IAdaptable host, Collection types) {
		if (types.isEmpty()) {
			return null;
		}
		IGraphicalEditPart editPart = (IGraphicalEditPart) host
				.getAdapter(IGraphicalEditPart.class);
		if (editPart == null) {
			return null;
		}
		Diagram diagram = (Diagram) editPart.getRoot().getContents().getModel();
		Collection elements = new HashSet();
		for (Iterator it = diagram.getElement().eAllContents(); it.hasNext();) {
			EObject element = (EObject) it.next();
			if (isApplicableElement(element, types)) {
				elements.add(element);
			}
		}
		if (elements.isEmpty()) {
			return null;
		}
		return selectElement((EObject[]) elements.toArray(new EObject[elements
				.size()]));
	}

	/**
	 * @generated
	 */
	protected boolean isApplicableElement(EObject element, Collection types) {
		IElementType type = ElementTypeRegistry.getInstance().getElementType(
				element);
		return types.contains(type);
	}

	/**
	 * @generated
	 */
	protected EObject selectElement(EObject[] elements) {
		Shell shell = Display.getCurrent().getActiveShell();
		ILabelProvider labelProvider = new AdapterFactoryLabelProvider(
				ProcedureDiagramEditorPlugin.getInstance()
						.getItemProvidersAdapterFactory());
		ElementListSelectionDialog dialog = new ElementListSelectionDialog(
				shell, labelProvider);
		dialog.setMessage(Messages.ProcedureModelingAssistantProviderMessage);
		dialog.setTitle(Messages.ProcedureModelingAssistantProviderTitle);
		dialog.setMultipleSelection(false);
		dialog.setElements(elements);
		EObject selected = null;
		if (dialog.open() == Window.OK) {
			selected = (EObject) dialog.getFirstResult();
		}
		return selected;
	}
}
