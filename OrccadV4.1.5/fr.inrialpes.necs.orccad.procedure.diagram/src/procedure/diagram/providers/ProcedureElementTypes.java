package procedure.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.swt.graphics.Image;

import procedure.ProcedurePackage;
import procedure.diagram.edit.parts.Condition2EditPart;
import procedure.diagram.edit.parts.ConditionEditPart;
import procedure.diagram.edit.parts.LinkEntitiesEditPart;
import procedure.diagram.edit.parts.Loop2EditPart;
import procedure.diagram.edit.parts.Loop3EditPart;
import procedure.diagram.edit.parts.Loop4EditPart;
import procedure.diagram.edit.parts.Loop5EditPart;
import procedure.diagram.edit.parts.LoopEditPart;
import procedure.diagram.edit.parts.Parallel2EditPart;
import procedure.diagram.edit.parts.Parallel3EditPart;
import procedure.diagram.edit.parts.Parallel4EditPart;
import procedure.diagram.edit.parts.Parallel5EditPart;
import procedure.diagram.edit.parts.ParallelEditPart;
import procedure.diagram.edit.parts.RobotProcedure2EditPart;
import procedure.diagram.edit.parts.RobotProcedure3EditPart;
import procedure.diagram.edit.parts.RobotProcedure4EditPart;
import procedure.diagram.edit.parts.RobotProcedure5EditPart;
import procedure.diagram.edit.parts.RobotProcedure6EditPart;
import procedure.diagram.edit.parts.RobotProcedureEditPart;
import procedure.diagram.edit.parts.RobotTaskEntity2EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity3EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity4EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity5EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityEditPart;
import procedure.diagram.edit.parts.Sequence2EditPart;
import procedure.diagram.edit.parts.Sequence3EditPart;
import procedure.diagram.edit.parts.Sequence4EditPart;
import procedure.diagram.edit.parts.Sequence5EditPart;
import procedure.diagram.edit.parts.SequenceEditPart;
import procedure.diagram.part.ProcedureDiagramEditorPlugin;

/**
 * @generated
 */
public class ProcedureElementTypes extends ElementInitializers {

	/**
	 * @generated
	 */
	private ProcedureElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map elements;

	/**
	 * @generated
	 */
	private static ImageRegistry imageRegistry;

	/**
	 * @generated
	 */
	private static Set KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType RobotProcedure_1000 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.RobotProcedure_1000"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RobotTaskEntity_2001 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.RobotTaskEntity_2001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Parallel_2002 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Parallel_2002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Sequence_2003 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Sequence_2003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RobotProcedure_2004 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.RobotProcedure_2004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Loop_2005 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Loop_2005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Condition_2006 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Condition_2006"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RobotTaskEntity_3001 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.RobotTaskEntity_3001"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RobotProcedure_3002 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.RobotProcedure_3002"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RobotTaskEntity_3003 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.RobotTaskEntity_3003"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RobotProcedure_3004 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.RobotProcedure_3004"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Sequence_3005 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Sequence_3005"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RobotTaskEntity_3021 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.RobotTaskEntity_3021"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RobotProcedure_3022 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.RobotProcedure_3022"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Parallel_3008 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Parallel_3008"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Sequence_3009 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Sequence_3009"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Sequence_3023 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Sequence_3023"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Parallel_3024 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Parallel_3024"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Parallel_3012 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Parallel_3012"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Loop_3013 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Loop_3013"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RobotTaskEntity_3014 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.RobotTaskEntity_3014"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType RobotProcedure_3015 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.RobotProcedure_3015"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Loop_3016 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Loop_3016"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Sequence_3017 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Sequence_3017"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Loop_3025 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Loop_3025"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Parallel_3019 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Parallel_3019"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Loop_3020 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Loop_3020"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType Condition_3026 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.Condition_3026"); //$NON-NLS-1$
	/**
	 * @generated
	 */
	public static final IElementType LinkEntities_4001 = getElementType("fr.inrialpes.necs.orccad.procedure.diagram.LinkEntities_4001"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	private static ImageRegistry getImageRegistry() {
		if (imageRegistry == null) {
			imageRegistry = new ImageRegistry();
		}
		return imageRegistry;
	}

	/**
	 * @generated
	 */
	private static String getImageRegistryKey(ENamedElement element) {
		return element.getName();
	}

	/**
	 * @generated
	 */
	private static ImageDescriptor getProvidedImageDescriptor(
			ENamedElement element) {
		if (element instanceof EStructuralFeature) {
			EStructuralFeature feature = ((EStructuralFeature) element);
			EClass eContainingClass = feature.getEContainingClass();
			EClassifier eType = feature.getEType();
			if (eContainingClass != null && !eContainingClass.isAbstract()) {
				element = eContainingClass;
			} else if (eType instanceof EClass
					&& !((EClass) eType).isAbstract()) {
				element = eType;
			}
		}
		if (element instanceof EClass) {
			EClass eClass = (EClass) element;
			if (!eClass.isAbstract()) {
				return ProcedureDiagramEditorPlugin.getInstance()
						.getItemImageDescriptor(
								eClass.getEPackage().getEFactoryInstance()
										.create(eClass));
			}
		}
		// TODO : support structural features
		return null;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		String key = getImageRegistryKey(element);
		ImageDescriptor imageDescriptor = getImageRegistry().getDescriptor(key);
		if (imageDescriptor == null) {
			imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
		}
		return imageDescriptor;
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		String key = getImageRegistryKey(element);
		Image image = getImageRegistry().get(key);
		if (image == null) {
			ImageDescriptor imageDescriptor = getProvidedImageDescriptor(element);
			if (imageDescriptor == null) {
				imageDescriptor = ImageDescriptor.getMissingImageDescriptor();
			}
			getImageRegistry().put(key, imageDescriptor);
			image = getImageRegistry().get(key);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		ENamedElement element = getElement(hint);
		if (element == null) {
			return null;
		}
		return getImage(element);
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap();

			elements.put(RobotProcedure_1000, ProcedurePackage.eINSTANCE
					.getRobotProcedure());

			elements.put(RobotTaskEntity_2001, ProcedurePackage.eINSTANCE
					.getRobotTaskEntity());

			elements.put(Parallel_2002, ProcedurePackage.eINSTANCE
					.getParallel());

			elements.put(Sequence_2003, ProcedurePackage.eINSTANCE
					.getSequence());

			elements.put(RobotProcedure_2004, ProcedurePackage.eINSTANCE
					.getRobotProcedure());

			elements.put(Loop_2005, ProcedurePackage.eINSTANCE.getLoop());

			elements.put(Condition_2006, ProcedurePackage.eINSTANCE
					.getCondition());

			elements.put(RobotTaskEntity_3001, ProcedurePackage.eINSTANCE
					.getRobotTaskEntity());

			elements.put(RobotProcedure_3002, ProcedurePackage.eINSTANCE
					.getRobotProcedure());

			elements.put(RobotTaskEntity_3003, ProcedurePackage.eINSTANCE
					.getRobotTaskEntity());

			elements.put(RobotProcedure_3004, ProcedurePackage.eINSTANCE
					.getRobotProcedure());

			elements.put(Sequence_3005, ProcedurePackage.eINSTANCE
					.getSequence());

			elements.put(RobotTaskEntity_3021, ProcedurePackage.eINSTANCE
					.getRobotTaskEntity());

			elements.put(RobotProcedure_3022, ProcedurePackage.eINSTANCE
					.getRobotProcedure());

			elements.put(Parallel_3008, ProcedurePackage.eINSTANCE
					.getParallel());

			elements.put(Sequence_3009, ProcedurePackage.eINSTANCE
					.getSequence());

			elements.put(Sequence_3023, ProcedurePackage.eINSTANCE
					.getSequence());

			elements.put(Parallel_3024, ProcedurePackage.eINSTANCE
					.getParallel());

			elements.put(Parallel_3012, ProcedurePackage.eINSTANCE
					.getParallel());

			elements.put(Loop_3013, ProcedurePackage.eINSTANCE.getLoop());

			elements.put(RobotTaskEntity_3014, ProcedurePackage.eINSTANCE
					.getRobotTaskEntity());

			elements.put(RobotProcedure_3015, ProcedurePackage.eINSTANCE
					.getRobotProcedure());

			elements.put(Loop_3016, ProcedurePackage.eINSTANCE.getLoop());

			elements.put(Sequence_3017, ProcedurePackage.eINSTANCE
					.getSequence());

			elements.put(Loop_3025, ProcedurePackage.eINSTANCE.getLoop());

			elements.put(Parallel_3019, ProcedurePackage.eINSTANCE
					.getParallel());

			elements.put(Loop_3020, ProcedurePackage.eINSTANCE.getLoop());

			elements.put(Condition_3026, ProcedurePackage.eINSTANCE
					.getCondition());

			elements.put(LinkEntities_4001, ProcedurePackage.eINSTANCE
					.getLinkEntities());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet();
			KNOWN_ELEMENT_TYPES.add(RobotProcedure_1000);
			KNOWN_ELEMENT_TYPES.add(RobotTaskEntity_2001);
			KNOWN_ELEMENT_TYPES.add(Parallel_2002);
			KNOWN_ELEMENT_TYPES.add(Sequence_2003);
			KNOWN_ELEMENT_TYPES.add(RobotProcedure_2004);
			KNOWN_ELEMENT_TYPES.add(Loop_2005);
			KNOWN_ELEMENT_TYPES.add(Condition_2006);
			KNOWN_ELEMENT_TYPES.add(RobotTaskEntity_3001);
			KNOWN_ELEMENT_TYPES.add(RobotProcedure_3002);
			KNOWN_ELEMENT_TYPES.add(RobotTaskEntity_3003);
			KNOWN_ELEMENT_TYPES.add(RobotProcedure_3004);
			KNOWN_ELEMENT_TYPES.add(Sequence_3005);
			KNOWN_ELEMENT_TYPES.add(RobotTaskEntity_3021);
			KNOWN_ELEMENT_TYPES.add(RobotProcedure_3022);
			KNOWN_ELEMENT_TYPES.add(Parallel_3008);
			KNOWN_ELEMENT_TYPES.add(Sequence_3009);
			KNOWN_ELEMENT_TYPES.add(Sequence_3023);
			KNOWN_ELEMENT_TYPES.add(Parallel_3024);
			KNOWN_ELEMENT_TYPES.add(Parallel_3012);
			KNOWN_ELEMENT_TYPES.add(Loop_3013);
			KNOWN_ELEMENT_TYPES.add(RobotTaskEntity_3014);
			KNOWN_ELEMENT_TYPES.add(RobotProcedure_3015);
			KNOWN_ELEMENT_TYPES.add(Loop_3016);
			KNOWN_ELEMENT_TYPES.add(Sequence_3017);
			KNOWN_ELEMENT_TYPES.add(Loop_3025);
			KNOWN_ELEMENT_TYPES.add(Parallel_3019);
			KNOWN_ELEMENT_TYPES.add(Loop_3020);
			KNOWN_ELEMENT_TYPES.add(Condition_3026);
			KNOWN_ELEMENT_TYPES.add(LinkEntities_4001);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case RobotProcedureEditPart.VISUAL_ID:
			return RobotProcedure_1000;
		case RobotTaskEntityEditPart.VISUAL_ID:
			return RobotTaskEntity_2001;
		case ParallelEditPart.VISUAL_ID:
			return Parallel_2002;
		case SequenceEditPart.VISUAL_ID:
			return Sequence_2003;
		case RobotProcedure2EditPart.VISUAL_ID:
			return RobotProcedure_2004;
		case LoopEditPart.VISUAL_ID:
			return Loop_2005;
		case ConditionEditPart.VISUAL_ID:
			return Condition_2006;
		case RobotTaskEntity2EditPart.VISUAL_ID:
			return RobotTaskEntity_3001;
		case RobotProcedure3EditPart.VISUAL_ID:
			return RobotProcedure_3002;
		case RobotTaskEntity3EditPart.VISUAL_ID:
			return RobotTaskEntity_3003;
		case RobotProcedure4EditPart.VISUAL_ID:
			return RobotProcedure_3004;
		case Sequence2EditPart.VISUAL_ID:
			return Sequence_3005;
		case RobotTaskEntity4EditPart.VISUAL_ID:
			return RobotTaskEntity_3021;
		case RobotProcedure5EditPart.VISUAL_ID:
			return RobotProcedure_3022;
		case Parallel2EditPart.VISUAL_ID:
			return Parallel_3008;
		case Sequence3EditPart.VISUAL_ID:
			return Sequence_3009;
		case Sequence4EditPart.VISUAL_ID:
			return Sequence_3023;
		case Parallel3EditPart.VISUAL_ID:
			return Parallel_3024;
		case Parallel4EditPart.VISUAL_ID:
			return Parallel_3012;
		case Loop2EditPart.VISUAL_ID:
			return Loop_3013;
		case RobotTaskEntity5EditPart.VISUAL_ID:
			return RobotTaskEntity_3014;
		case RobotProcedure6EditPart.VISUAL_ID:
			return RobotProcedure_3015;
		case Loop3EditPart.VISUAL_ID:
			return Loop_3016;
		case Sequence5EditPart.VISUAL_ID:
			return Sequence_3017;
		case Loop4EditPart.VISUAL_ID:
			return Loop_3025;
		case Parallel5EditPart.VISUAL_ID:
			return Parallel_3019;
		case Loop5EditPart.VISUAL_ID:
			return Loop_3020;
		case Condition2EditPart.VISUAL_ID:
			return Condition_3026;
		case LinkEntitiesEditPart.VISUAL_ID:
			return LinkEntities_4001;
		}
		return null;
	}

}
