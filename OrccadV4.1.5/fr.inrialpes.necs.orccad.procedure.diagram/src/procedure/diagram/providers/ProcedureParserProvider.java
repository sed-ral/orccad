package procedure.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;

import procedure.ProcedurePackage;
import procedure.diagram.edit.parts.ConditionName2EditPart;
import procedure.diagram.edit.parts.ConditionNameEditPart;
import procedure.diagram.edit.parts.LoopCondition2EditPart;
import procedure.diagram.edit.parts.LoopCondition3EditPart;
import procedure.diagram.edit.parts.LoopCondition4EditPart;
import procedure.diagram.edit.parts.LoopCondition5EditPart;
import procedure.diagram.edit.parts.LoopConditionEditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditions2EditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditions3EditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditions5EditPart;
import procedure.diagram.edit.parts.RobotProcedureNameConditionsEditPart;
import procedure.diagram.edit.parts.RobotProcedureNameEditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName2EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName3EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName4EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityName5EditPart;
import procedure.diagram.edit.parts.RobotTaskEntityNameEditPart;
import procedure.diagram.parsers.MessageFormatParser;
import procedure.diagram.part.ProcedureVisualIDRegistry;
import task.TaskPackage;

/**
 * @generated
 */
public class ProcedureParserProvider extends AbstractProvider implements
		IParserProvider {

	/**
	 * @generated
	 */
	private IParser robotTaskEntityName_5001Parser;

	/**
	 * @generated
	 */
	private IParser getRobotTaskEntityName_5001Parser() {
		if (robotTaskEntityName_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { TaskPackage.eINSTANCE
					.getRobotTask_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			robotTaskEntityName_5001Parser = parser;
		}
		return robotTaskEntityName_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser robotProcedureName_5014Parser;

	/**
	 * @generated
	 */
	private IParser getRobotProcedureName_5014Parser() {
		if (robotProcedureName_5014Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getRobotProcedure_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			robotProcedureName_5014Parser = parser;
		}
		return robotProcedureName_5014Parser;
	}

	/**
	 * @generated
	 */
	private IParser loopCondition_5015Parser;

	/**
	 * @generated
	 */
	private IParser getLoopCondition_5015Parser() {
		if (loopCondition_5015Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getLoop_Condition() };
			MessageFormatParser parser = new MessageFormatParser(features);
			loopCondition_5015Parser = parser;
		}
		return loopCondition_5015Parser;
	}

	/**
	 * @generated
	 */
	private IParser conditionName_5020Parser;

	/**
	 * @generated
	 */
	private IParser getConditionName_5020Parser() {
		if (conditionName_5020Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getCondition_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			conditionName_5020Parser = parser;
		}
		return conditionName_5020Parser;
	}

	/**
	 * @generated
	 */
	private IParser robotTaskEntityName_5002Parser;

	/**
	 * @generated
	 */
	private IParser getRobotTaskEntityName_5002Parser() {
		if (robotTaskEntityName_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { TaskPackage.eINSTANCE
					.getRobotTask_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			robotTaskEntityName_5002Parser = parser;
		}
		return robotTaskEntityName_5002Parser;
	}

	/**
	 * @generated
	 */
	private IParser robotProcedureName_5013Parser;

	/**
	 * @generated
	 */
	private IParser getRobotProcedureName_5013Parser() {
		if (robotProcedureName_5013Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getRobotProcedure_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			robotProcedureName_5013Parser = parser;
		}
		return robotProcedureName_5013Parser;
	}

	/**
	 * @generated
	 */
	private IParser robotTaskEntityName_5003Parser;

	/**
	 * @generated
	 */
	private IParser getRobotTaskEntityName_5003Parser() {
		if (robotTaskEntityName_5003Parser == null) {
			EAttribute[] features = new EAttribute[] { TaskPackage.eINSTANCE
					.getRobotTask_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			robotTaskEntityName_5003Parser = parser;
		}
		return robotTaskEntityName_5003Parser;
	}

	/**
	 * @generated
	 */
	private IParser robotProcedureName_5012Parser;

	/**
	 * @generated
	 */
	private IParser getRobotProcedureName_5012Parser() {
		if (robotProcedureName_5012Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getRobotProcedure_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			robotProcedureName_5012Parser = parser;
		}
		return robotProcedureName_5012Parser;
	}

	/**
	 * @generated
	 */
	private IParser robotTaskEntityName_5016Parser;

	/**
	 * @generated
	 */
	private IParser getRobotTaskEntityName_5016Parser() {
		if (robotTaskEntityName_5016Parser == null) {
			EAttribute[] features = new EAttribute[] { TaskPackage.eINSTANCE
					.getRobotTask_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			robotTaskEntityName_5016Parser = parser;
		}
		return robotTaskEntityName_5016Parser;
	}

	/**
	 * @generated
	 */
	private IParser robotProcedureName_5018Parser;

	/**
	 * @generated
	 */
	private IParser getRobotProcedureName_5018Parser() {
		if (robotProcedureName_5018Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getRobotProcedure_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			robotProcedureName_5018Parser = parser;
		}
		return robotProcedureName_5018Parser;
	}

	/**
	 * @generated
	 */
	private IParser loopCondition_5010Parser;

	/**
	 * @generated
	 */
	private IParser getLoopCondition_5010Parser() {
		if (loopCondition_5010Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getLoop_Condition() };
			MessageFormatParser parser = new MessageFormatParser(features);
			loopCondition_5010Parser = parser;
		}
		return loopCondition_5010Parser;
	}

	/**
	 * @generated
	 */
	private IParser robotTaskEntityName_5005Parser;

	/**
	 * @generated
	 */
	private IParser getRobotTaskEntityName_5005Parser() {
		if (robotTaskEntityName_5005Parser == null) {
			EAttribute[] features = new EAttribute[] { TaskPackage.eINSTANCE
					.getRobotTask_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			robotTaskEntityName_5005Parser = parser;
		}
		return robotTaskEntityName_5005Parser;
	}

	/**
	 * @generated
	 */
	private IParser robotProcedureName_5009Parser;

	/**
	 * @generated
	 */
	private IParser getRobotProcedureName_5009Parser() {
		if (robotProcedureName_5009Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getRobotProcedure_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			robotProcedureName_5009Parser = parser;
		}
		return robotProcedureName_5009Parser;
	}

	/**
	 * @generated
	 */
	private IParser loopCondition_5008Parser;

	/**
	 * @generated
	 */
	private IParser getLoopCondition_5008Parser() {
		if (loopCondition_5008Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getLoop_Condition() };
			MessageFormatParser parser = new MessageFormatParser(features);
			loopCondition_5008Parser = parser;
		}
		return loopCondition_5008Parser;
	}

	/**
	 * @generated
	 */
	private IParser loopCondition_5017Parser;

	/**
	 * @generated
	 */
	private IParser getLoopCondition_5017Parser() {
		if (loopCondition_5017Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getLoop_Condition() };
			MessageFormatParser parser = new MessageFormatParser(features);
			loopCondition_5017Parser = parser;
		}
		return loopCondition_5017Parser;
	}

	/**
	 * @generated
	 */
	private IParser loopCondition_5006Parser;

	/**
	 * @generated
	 */
	private IParser getLoopCondition_5006Parser() {
		if (loopCondition_5006Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getLoop_Condition() };
			MessageFormatParser parser = new MessageFormatParser(features);
			loopCondition_5006Parser = parser;
		}
		return loopCondition_5006Parser;
	}

	/**
	 * @generated
	 */
	private IParser conditionName_5019Parser;

	/**
	 * @generated
	 */
	private IParser getConditionName_5019Parser() {
		if (conditionName_5019Parser == null) {
			EAttribute[] features = new EAttribute[] { ProcedurePackage.eINSTANCE
					.getCondition_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			conditionName_5019Parser = parser;
		}
		return conditionName_5019Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case RobotTaskEntityNameEditPart.VISUAL_ID:
			return getRobotTaskEntityName_5001Parser();
		case RobotProcedureNameConditionsEditPart.VISUAL_ID:
			return getRobotProcedureName_5014Parser();
		case LoopConditionEditPart.VISUAL_ID:
			return getLoopCondition_5015Parser();
		case ConditionNameEditPart.VISUAL_ID:
			return getConditionName_5020Parser();
		case RobotTaskEntityName2EditPart.VISUAL_ID:
			return getRobotTaskEntityName_5002Parser();
		case RobotProcedureNameConditions2EditPart.VISUAL_ID:
			return getRobotProcedureName_5013Parser();
		case RobotTaskEntityName3EditPart.VISUAL_ID:
			return getRobotTaskEntityName_5003Parser();
		case RobotProcedureNameConditions3EditPart.VISUAL_ID:
			return getRobotProcedureName_5012Parser();
		case RobotTaskEntityName4EditPart.VISUAL_ID:
			return getRobotTaskEntityName_5016Parser();
		case RobotProcedureNameEditPart.VISUAL_ID:
			return getRobotProcedureName_5018Parser();
		case LoopCondition2EditPart.VISUAL_ID:
			return getLoopCondition_5010Parser();
		case RobotTaskEntityName5EditPart.VISUAL_ID:
			return getRobotTaskEntityName_5005Parser();
		case RobotProcedureNameConditions5EditPart.VISUAL_ID:
			return getRobotProcedureName_5009Parser();
		case LoopCondition3EditPart.VISUAL_ID:
			return getLoopCondition_5008Parser();
		case LoopCondition4EditPart.VISUAL_ID:
			return getLoopCondition_5017Parser();
		case LoopCondition5EditPart.VISUAL_ID:
			return getLoopCondition_5006Parser();
		case ConditionName2EditPart.VISUAL_ID:
			return getConditionName_5019Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object,
			String parserHint) {
		return ParserService.getInstance().getParser(
				new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(ProcedureVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(ProcedureVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (ProcedureElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
