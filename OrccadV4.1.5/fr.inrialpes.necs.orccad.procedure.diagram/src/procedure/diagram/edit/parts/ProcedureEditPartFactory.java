package procedure.diagram.edit.parts;

import org.eclipse.draw2d.FigureUtilities;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPartFactory;
import org.eclipse.gef.tools.CellEditorLocator;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;

import procedure.diagram.part.ProcedureVisualIDRegistry;

/**
 * @generated
 */
public class ProcedureEditPartFactory implements EditPartFactory {

	/**
	 * @generated
	 */
	public EditPart createEditPart(EditPart context, Object model) {
		if (model instanceof View) {
			View view = (View) model;
			switch (ProcedureVisualIDRegistry.getVisualID(view)) {

			case RobotProcedureEditPart.VISUAL_ID:
				return new RobotProcedureEditPart(view);

			case RobotTaskEntityEditPart.VISUAL_ID:
				return new RobotTaskEntityEditPart(view);

			case RobotTaskEntityNameEditPart.VISUAL_ID:
				return new RobotTaskEntityNameEditPart(view);

			case ParallelEditPart.VISUAL_ID:
				return new ParallelEditPart(view);

			case SequenceEditPart.VISUAL_ID:
				return new SequenceEditPart(view);

			case RobotProcedure2EditPart.VISUAL_ID:
				return new RobotProcedure2EditPart(view);

			case RobotProcedureNameConditionsEditPart.VISUAL_ID:
				return new RobotProcedureNameConditionsEditPart(view);

			case LoopEditPart.VISUAL_ID:
				return new LoopEditPart(view);

			case LoopConditionEditPart.VISUAL_ID:
				return new LoopConditionEditPart(view);

			case ConditionEditPart.VISUAL_ID:
				return new ConditionEditPart(view);

			case ConditionNameEditPart.VISUAL_ID:
				return new ConditionNameEditPart(view);

			case RobotTaskEntity2EditPart.VISUAL_ID:
				return new RobotTaskEntity2EditPart(view);

			case RobotTaskEntityName2EditPart.VISUAL_ID:
				return new RobotTaskEntityName2EditPart(view);

			case RobotProcedure3EditPart.VISUAL_ID:
				return new RobotProcedure3EditPart(view);

			case RobotProcedureNameConditions2EditPart.VISUAL_ID:
				return new RobotProcedureNameConditions2EditPart(view);

			case RobotTaskEntity3EditPart.VISUAL_ID:
				return new RobotTaskEntity3EditPart(view);

			case RobotTaskEntityName3EditPart.VISUAL_ID:
				return new RobotTaskEntityName3EditPart(view);

			case RobotProcedure4EditPart.VISUAL_ID:
				return new RobotProcedure4EditPart(view);

			case RobotProcedureNameConditions3EditPart.VISUAL_ID:
				return new RobotProcedureNameConditions3EditPart(view);

			case Sequence2EditPart.VISUAL_ID:
				return new Sequence2EditPart(view);

			case RobotTaskEntity4EditPart.VISUAL_ID:
				return new RobotTaskEntity4EditPart(view);

			case RobotTaskEntityName4EditPart.VISUAL_ID:
				return new RobotTaskEntityName4EditPart(view);

			case RobotProcedure5EditPart.VISUAL_ID:
				return new RobotProcedure5EditPart(view);

			case RobotProcedureNameEditPart.VISUAL_ID:
				return new RobotProcedureNameEditPart(view);

			case Parallel2EditPart.VISUAL_ID:
				return new Parallel2EditPart(view);

			case Sequence3EditPart.VISUAL_ID:
				return new Sequence3EditPart(view);

			case Sequence4EditPart.VISUAL_ID:
				return new Sequence4EditPart(view);

			case Parallel3EditPart.VISUAL_ID:
				return new Parallel3EditPart(view);

			case Parallel4EditPart.VISUAL_ID:
				return new Parallel4EditPart(view);

			case Loop2EditPart.VISUAL_ID:
				return new Loop2EditPart(view);

			case LoopCondition2EditPart.VISUAL_ID:
				return new LoopCondition2EditPart(view);

			case RobotTaskEntity5EditPart.VISUAL_ID:
				return new RobotTaskEntity5EditPart(view);

			case RobotTaskEntityName5EditPart.VISUAL_ID:
				return new RobotTaskEntityName5EditPart(view);

			case RobotProcedure6EditPart.VISUAL_ID:
				return new RobotProcedure6EditPart(view);

			case RobotProcedureNameConditions5EditPart.VISUAL_ID:
				return new RobotProcedureNameConditions5EditPart(view);

			case Loop3EditPart.VISUAL_ID:
				return new Loop3EditPart(view);

			case LoopCondition3EditPart.VISUAL_ID:
				return new LoopCondition3EditPart(view);

			case Sequence5EditPart.VISUAL_ID:
				return new Sequence5EditPart(view);

			case Loop4EditPart.VISUAL_ID:
				return new Loop4EditPart(view);

			case LoopCondition4EditPart.VISUAL_ID:
				return new LoopCondition4EditPart(view);

			case Parallel5EditPart.VISUAL_ID:
				return new Parallel5EditPart(view);

			case Loop5EditPart.VISUAL_ID:
				return new Loop5EditPart(view);

			case LoopCondition5EditPart.VISUAL_ID:
				return new LoopCondition5EditPart(view);

			case Condition2EditPart.VISUAL_ID:
				return new Condition2EditPart(view);

			case ConditionName2EditPart.VISUAL_ID:
				return new ConditionName2EditPart(view);

			case RobotTaskEntityCompartmentGenEditPart.VISUAL_ID:
				return new RobotTaskEntityCompartmentGenEditPart(view);

			case ParallelCompartmentGenEditPart.VISUAL_ID:
				return new ParallelCompartmentGenEditPart(view);

			case RobotTaskEntityCompartmentGen2EditPart.VISUAL_ID:
				return new RobotTaskEntityCompartmentGen2EditPart(view);

			case RobotProcedureCompartmentGenEditPart.VISUAL_ID:
				return new RobotProcedureCompartmentGenEditPart(view);

			case RobotTaskEntityCompartmentGen3EditPart.VISUAL_ID:
				return new RobotTaskEntityCompartmentGen3EditPart(view);

			case RobotProcedureCompartmentGen2EditPart.VISUAL_ID:
				return new RobotProcedureCompartmentGen2EditPart(view);

			case SequenceCompartmentGenEditPart.VISUAL_ID:
				return new SequenceCompartmentGenEditPart(view);

			case RobotTaskEntityCompartmentGen4EditPart.VISUAL_ID:
				return new RobotTaskEntityCompartmentGen4EditPart(view);

			case RobotProcedureCompartmentGen3EditPart.VISUAL_ID:
				return new RobotProcedureCompartmentGen3EditPart(view);

			case ParallelCompartmentGen2EditPart.VISUAL_ID:
				return new ParallelCompartmentGen2EditPart(view);

			case SequenceCompartmentGen2EditPart.VISUAL_ID:
				return new SequenceCompartmentGen2EditPart(view);

			case SequenceCompartmentGen3EditPart.VISUAL_ID:
				return new SequenceCompartmentGen3EditPart(view);

			case ParallelCompartmentGen3EditPart.VISUAL_ID:
				return new ParallelCompartmentGen3EditPart(view);

			case ParallelCompartmentGen4EditPart.VISUAL_ID:
				return new ParallelCompartmentGen4EditPart(view);

			case LoopCompartmentGenEditPart.VISUAL_ID:
				return new LoopCompartmentGenEditPart(view);

			case RobotTaskEntityCompartmentGen5EditPart.VISUAL_ID:
				return new RobotTaskEntityCompartmentGen5EditPart(view);

			case RobotProcedureCompartmentGen4EditPart.VISUAL_ID:
				return new RobotProcedureCompartmentGen4EditPart(view);

			case LoopCompartmentGen2EditPart.VISUAL_ID:
				return new LoopCompartmentGen2EditPart(view);

			case SequenceCompartmentGen4EditPart.VISUAL_ID:
				return new SequenceCompartmentGen4EditPart(view);

			case LoopCompartmentGen3EditPart.VISUAL_ID:
				return new LoopCompartmentGen3EditPart(view);

			case ParallelCompartmentGen5EditPart.VISUAL_ID:
				return new ParallelCompartmentGen5EditPart(view);

			case LoopCompartmentGen4EditPart.VISUAL_ID:
				return new LoopCompartmentGen4EditPart(view);

			case SequenceCompartmentGen5EditPart.VISUAL_ID:
				return new SequenceCompartmentGen5EditPart(view);

			case RobotProcedureCompartmentGen5EditPart.VISUAL_ID:
				return new RobotProcedureCompartmentGen5EditPart(view);

			case LoopCompartmentGen5EditPart.VISUAL_ID:
				return new LoopCompartmentGen5EditPart(view);

			case LinkEntitiesEditPart.VISUAL_ID:
				return new LinkEntitiesEditPart(view);

			}
		}
		return createUnrecognizedEditPart(context, model);
	}

	/**
	 * @generated
	 */
	private EditPart createUnrecognizedEditPart(EditPart context, Object model) {
		// Handle creation of unrecognized child node EditParts here
		return null;
	}

	/**
	 * @generated
	 */
	public static CellEditorLocator getTextCellEditorLocator(
			ITextAwareEditPart source) {
		if (source.getFigure() instanceof WrappingLabel)
			return new TextCellEditorLocator((WrappingLabel) source.getFigure());
		else {
			return new LabelCellEditorLocator((Label) source.getFigure());
		}
	}

	/**
	 * @generated
	 */
	static private class TextCellEditorLocator implements CellEditorLocator {

		/**
		 * @generated
		 */
		private WrappingLabel wrapLabel;

		/**
		 * @generated
		 */
		public TextCellEditorLocator(WrappingLabel wrapLabel) {
			this.wrapLabel = wrapLabel;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getWrapLabel() {
			return wrapLabel;
		}

		/**
		 * @generated
		 */
		public void relocate(CellEditor celleditor) {
			Text text = (Text) celleditor.getControl();
			Rectangle rect = getWrapLabel().getTextBounds().getCopy();
			getWrapLabel().translateToAbsolute(rect);
			if (getWrapLabel().isTextWrapOn()
					&& getWrapLabel().getText().length() > 0) {
				rect.setSize(new Dimension(text.computeSize(rect.width,
						SWT.DEFAULT)));
			} else {
				int avr = FigureUtilities.getFontMetrics(text.getFont())
						.getAverageCharWidth();
				rect.setSize(new Dimension(text.computeSize(SWT.DEFAULT,
						SWT.DEFAULT)).expand(avr * 2, 0));
			}
			if (!rect.equals(new Rectangle(text.getBounds()))) {
				text.setBounds(rect.x, rect.y, rect.width, rect.height);
			}
		}
	}

	/**
	 * @generated
	 */
	private static class LabelCellEditorLocator implements CellEditorLocator {

		/**
		 * @generated
		 */
		private Label label;

		/**
		 * @generated
		 */
		public LabelCellEditorLocator(Label label) {
			this.label = label;
		}

		/**
		 * @generated
		 */
		public Label getLabel() {
			return label;
		}

		/**
		 * @generated
		 */
		public void relocate(CellEditor celleditor) {
			Text text = (Text) celleditor.getControl();
			Rectangle rect = getLabel().getTextBounds().getCopy();
			getLabel().translateToAbsolute(rect);
			int avr = FigureUtilities.getFontMetrics(text.getFont())
					.getAverageCharWidth();
			rect.setSize(new Dimension(text.computeSize(SWT.DEFAULT,
					SWT.DEFAULT)).expand(avr * 2, 0));
			if (!rect.equals(new Rectangle(text.getBounds()))) {
				text.setBounds(rect.x, rect.y, rect.width, rect.height);
			}
		}
	}
}
