package procedure.diagram.edit.parts;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.Graphics;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import procedure.diagram.edit.policies.Loop4ItemSemanticEditPolicy;
import procedure.diagram.part.ProcedureVisualIDRegistry;
import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class Loop4EditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 3025;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public Loop4EditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE,
				new Loop4ItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		LayoutEditPolicy lep = new LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child
						.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		LoopFigure figure = new LoopFigure();
		return primaryShape = figure;
	}

	/**
	 * @generated
	 */
	public LoopFigure getPrimaryShape() {
		return (LoopFigure) primaryShape;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof LoopCondition4EditPart) {
			((LoopCondition4EditPart) childEditPart).setLabel(getPrimaryShape()
					.getFigureLoopConditionFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof LoopCondition4EditPart) {
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(40, 40);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * @param nodeShape instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(ProcedureVisualIDRegistry
				.getType(LoopCondition4EditPart.VISUAL_ID));
	}

	/**
	 * @generated
	 */
	public List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/getMARelTypesOnSource() {
		List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/types = new ArrayList/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/();
		types.add(ProcedureElementTypes.LinkEntities_4001);
		return types;
	}

	/**
	 * @generated
	 */
	public List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/getMARelTypesOnSourceAndTarget(
			IGraphicalEditPart targetEditPart) {
		List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/types = new ArrayList/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/();
		if (targetEditPart instanceof RobotTaskEntityEditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof ParallelEditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof SequenceEditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof RobotProcedure2EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof LoopEditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof RobotTaskEntity2EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof RobotProcedure3EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof RobotTaskEntity3EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof RobotProcedure4EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof Sequence2EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof RobotTaskEntity4EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof RobotProcedure5EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof Parallel2EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof Sequence3EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof Sequence4EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof Parallel3EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof Parallel4EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof Loop2EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof RobotTaskEntity5EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof RobotProcedure6EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof Loop3EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof Sequence5EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof procedure.diagram.edit.parts.Loop4EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof Parallel5EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		if (targetEditPart instanceof Loop5EditPart) {
			types.add(ProcedureElementTypes.LinkEntities_4001);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/getMATypesForTarget(
			IElementType relationshipType) {
		List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/types = new ArrayList/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/();
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotTaskEntity_2001);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Parallel_2002);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Sequence_2003);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotProcedure_2004);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Loop_2005);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotTaskEntity_3001);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotProcedure_3002);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotTaskEntity_3003);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotProcedure_3004);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Sequence_3005);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotTaskEntity_3021);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotProcedure_3022);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Parallel_3008);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Sequence_3009);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Sequence_3023);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Parallel_3024);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Parallel_3012);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Loop_3013);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotTaskEntity_3014);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotProcedure_3015);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Loop_3016);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Sequence_3017);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Loop_3025);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Parallel_3019);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Loop_3020);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/getMARelTypesOnTarget() {
		List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/types = new ArrayList/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/();
		types.add(ProcedureElementTypes.LinkEntities_4001);
		return types;
	}

	/**
	 * @generated
	 */
	public List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/getMATypesForSource(
			IElementType relationshipType) {
		List/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/types = new ArrayList/*<org.eclipse.gmf.runtime.emf.type.core.IElementType>*/();
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotTaskEntity_2001);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Parallel_2002);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Sequence_2003);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotProcedure_2004);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Loop_2005);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotTaskEntity_3001);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotProcedure_3002);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotTaskEntity_3003);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotProcedure_3004);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Sequence_3005);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotTaskEntity_3021);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotProcedure_3022);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Parallel_3008);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Sequence_3009);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Sequence_3023);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Parallel_3024);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Parallel_3012);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Loop_3013);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotTaskEntity_3014);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.RobotProcedure_3015);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Loop_3016);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Sequence_3017);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Loop_3025);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Parallel_3019);
		}
		if (relationshipType == ProcedureElementTypes.LinkEntities_4001) {
			types.add(ProcedureElementTypes.Loop_3020);
		}
		return types;
	}

	/**
	 * @generated
	 */
	public class LoopFigure extends RectangleFigure {

		/**
		 * @generated
		 */
		private WrappingLabel fFigureLoopNameFigure;
		/**
		 * @generated
		 */
		private WrappingLabel fFigureLoopConditionFigure;
		/**
		 * @generated
		 */
		private RectangleFigure fFigureLoopFigure;

		/**
		 * @generated
		 */
		public LoopFigure() {
			this.setLineWidth(2);
			this.setLineStyle(Graphics.LINE_DASH);
			this.setForegroundColor(ColorConstants.cyan);
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContents() {

			fFigureLoopNameFigure = new WrappingLabel();
			fFigureLoopNameFigure.setText("");

			this.add(fFigureLoopNameFigure);

			fFigureLoopConditionFigure = new WrappingLabel();
			fFigureLoopConditionFigure.setText("");

			this.add(fFigureLoopConditionFigure);

		}

		/**
		 * @generated
		 */
		private boolean myUseLocalCoordinates = false;

		/**
		 * @generated
		 */
		protected boolean useLocalCoordinates() {
			return myUseLocalCoordinates;
		}

		/**
		 * @generated
		 */
		protected void setUseLocalCoordinates(boolean useLocalCoordinates) {
			myUseLocalCoordinates = useLocalCoordinates;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureLoopNameFigure() {
			return fFigureLoopNameFigure;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureLoopConditionFigure() {
			return fFigureLoopConditionFigure;
		}

		/**
		 * @generated
		 */
		public RectangleFigure getFigureLoopFigure() {
			return fFigureLoopFigure;
		}

	}

}
