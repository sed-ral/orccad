package procedure.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import procedure.diagram.edit.commands.Loop5CreateCommand;
import procedure.diagram.edit.commands.Parallel5CreateCommand;
import procedure.diagram.edit.commands.RobotProcedure5CreateCommand;
import procedure.diagram.edit.commands.RobotTaskEntity5CreateCommand;
import procedure.diagram.edit.commands.Sequence5CreateCommand;
import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class LoopCompartmentGen3ItemSemanticEditPolicy extends
		ProcedureBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public LoopCompartmentGen3ItemSemanticEditPolicy() {
		super(ProcedureElementTypes.Loop_3025);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (ProcedureElementTypes.RobotTaskEntity_3014 == req.getElementType()) {
			return getGEFWrapper(new RobotTaskEntity5CreateCommand(req));
		}
		if (ProcedureElementTypes.RobotProcedure_3015 == req.getElementType()) {
			return getGEFWrapper(new RobotProcedure5CreateCommand(req));
		}
		if (ProcedureElementTypes.Sequence_3017 == req.getElementType()) {
			return getGEFWrapper(new Sequence5CreateCommand(req));
		}
		if (ProcedureElementTypes.Parallel_3019 == req.getElementType()) {
			return getGEFWrapper(new Parallel5CreateCommand(req));
		}
		if (ProcedureElementTypes.Loop_3020 == req.getElementType()) {
			return getGEFWrapper(new Loop5CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
