package procedure.diagram.edit.policies;

import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.commands.core.commands.DuplicateEObjectsCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DuplicateElementsRequest;

import procedure.diagram.edit.commands.ConditionCreateCommand;
import procedure.diagram.edit.commands.LoopCreateCommand;
import procedure.diagram.edit.commands.ParallelCreateCommand;
import procedure.diagram.edit.commands.RobotProcedureCreateCommand;
import procedure.diagram.edit.commands.RobotTaskEntityCreateCommand;
import procedure.diagram.edit.commands.SequenceCreateCommand;
import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class RobotProcedureItemSemanticEditPolicy extends
		ProcedureBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RobotProcedureItemSemanticEditPolicy() {
		super(ProcedureElementTypes.RobotProcedure_1000);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (ProcedureElementTypes.RobotTaskEntity_2001 == req.getElementType()) {
			return getGEFWrapper(new RobotTaskEntityCreateCommand(req));
		}
		if (ProcedureElementTypes.Parallel_2002 == req.getElementType()) {
			return getGEFWrapper(new ParallelCreateCommand(req));
		}
		if (ProcedureElementTypes.Sequence_2003 == req.getElementType()) {
			return getGEFWrapper(new SequenceCreateCommand(req));
		}
		if (ProcedureElementTypes.RobotProcedure_2004 == req.getElementType()) {
			return getGEFWrapper(new RobotProcedureCreateCommand(req));
		}
		if (ProcedureElementTypes.Loop_2005 == req.getElementType()) {
			return getGEFWrapper(new LoopCreateCommand(req));
		}
		if (ProcedureElementTypes.Condition_2006 == req.getElementType()) {
			return getGEFWrapper(new ConditionCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getDuplicateCommand(DuplicateElementsRequest req) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost())
				.getEditingDomain();
		return getGEFWrapper(new DuplicateAnythingCommand(editingDomain, req));
	}

	/**
	 * @generated
	 */
	private static class DuplicateAnythingCommand extends
			DuplicateEObjectsCommand {

		/**
		 * @generated
		 */
		public DuplicateAnythingCommand(
				TransactionalEditingDomain editingDomain,
				DuplicateElementsRequest req) {
			super(editingDomain, req.getLabel(), req
					.getElementsToBeDuplicated(), req
					.getAllDuplicatedElementsMap());
		}

	}

}
