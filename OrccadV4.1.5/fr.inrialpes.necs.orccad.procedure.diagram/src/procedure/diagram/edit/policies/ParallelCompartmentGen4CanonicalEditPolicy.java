package procedure.diagram.edit.policies;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.notation.View;

import procedure.ProcedurePackage;
import procedure.diagram.edit.parts.Loop2EditPart;
import procedure.diagram.edit.parts.Parallel4EditPart;
import procedure.diagram.edit.parts.RobotProcedure3EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity2EditPart;
import procedure.diagram.edit.parts.Sequence3EditPart;
import procedure.diagram.part.ProcedureDiagramUpdater;
import procedure.diagram.part.ProcedureNodeDescriptor;
import procedure.diagram.part.ProcedureVisualIDRegistry;

/**
 * @generated
 */
public class ParallelCompartmentGen4CanonicalEditPolicy extends
		CanonicalEditPolicy {

	/**
	 * @generated
	 */
	Set myFeaturesToSynchronize;

	/**
	 * @generated
	 */
	protected List getSemanticChildrenList() {
		View viewObject = (View) getHost().getModel();
		List result = new LinkedList();
		for (Iterator it = ProcedureDiagramUpdater
				.getParallelCompartmentGen_7010SemanticChildren(viewObject)
				.iterator(); it.hasNext();) {
			result.add(((ProcedureNodeDescriptor) it.next()).getModelElement());
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected boolean isOrphaned(Collection semanticChildren, final View view) {
		int visualID = ProcedureVisualIDRegistry.getVisualID(view);
		switch (visualID) {
		case RobotTaskEntity2EditPart.VISUAL_ID:
		case RobotProcedure3EditPart.VISUAL_ID:
		case Sequence3EditPart.VISUAL_ID:
		case Parallel4EditPart.VISUAL_ID:
		case Loop2EditPart.VISUAL_ID:
			if (!semanticChildren.contains(view.getElement())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected String getDefaultFactoryHint() {
		return null;
	}

	/**
	 * @generated
	 */
	protected Set getFeaturesToSynchronize() {
		if (myFeaturesToSynchronize == null) {
			myFeaturesToSynchronize = new HashSet();
			myFeaturesToSynchronize.add(ProcedurePackage.eINSTANCE
					.getParallel_ParaEntities());
		}
		return myFeaturesToSynchronize;
	}

}
