package procedure.diagram.edit.policies;

import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class RobotTaskEntityCompartmentGen3ItemSemanticEditPolicy extends
		ProcedureBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RobotTaskEntityCompartmentGen3ItemSemanticEditPolicy() {
		super(ProcedureElementTypes.RobotTaskEntity_3003);
	}

}
