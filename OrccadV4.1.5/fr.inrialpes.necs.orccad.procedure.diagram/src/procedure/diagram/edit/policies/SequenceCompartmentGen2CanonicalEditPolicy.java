package procedure.diagram.edit.policies;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.notation.View;

import procedure.ProcedurePackage;
import procedure.diagram.edit.parts.Loop4EditPart;
import procedure.diagram.edit.parts.Parallel3EditPart;
import procedure.diagram.edit.parts.RobotProcedure5EditPart;
import procedure.diagram.edit.parts.RobotTaskEntity4EditPart;
import procedure.diagram.edit.parts.Sequence4EditPart;
import procedure.diagram.part.ProcedureDiagramUpdater;
import procedure.diagram.part.ProcedureNodeDescriptor;
import procedure.diagram.part.ProcedureVisualIDRegistry;

/**
 * @generated
 */
public class SequenceCompartmentGen2CanonicalEditPolicy extends
		CanonicalEditPolicy {

	/**
	 * @generated
	 */
	Set myFeaturesToSynchronize;

	/**
	 * @generated
	 */
	protected List getSemanticChildrenList() {
		View viewObject = (View) getHost().getModel();
		List result = new LinkedList();
		for (Iterator it = ProcedureDiagramUpdater
				.getSequenceCompartmentGen_7007SemanticChildren(viewObject)
				.iterator(); it.hasNext();) {
			result.add(((ProcedureNodeDescriptor) it.next()).getModelElement());
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected boolean isOrphaned(Collection semanticChildren, final View view) {
		int visualID = ProcedureVisualIDRegistry.getVisualID(view);
		switch (visualID) {
		case RobotTaskEntity4EditPart.VISUAL_ID:
		case RobotProcedure5EditPart.VISUAL_ID:
		case Sequence4EditPart.VISUAL_ID:
		case Parallel3EditPart.VISUAL_ID:
		case Loop4EditPart.VISUAL_ID:
			if (!semanticChildren.contains(view.getElement())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected String getDefaultFactoryHint() {
		return null;
	}

	/**
	 * @generated
	 */
	protected Set getFeaturesToSynchronize() {
		if (myFeaturesToSynchronize == null) {
			myFeaturesToSynchronize = new HashSet();
			myFeaturesToSynchronize.add(ProcedurePackage.eINSTANCE
					.getSequence_SeqEntities());
		}
		return myFeaturesToSynchronize;
	}

}
