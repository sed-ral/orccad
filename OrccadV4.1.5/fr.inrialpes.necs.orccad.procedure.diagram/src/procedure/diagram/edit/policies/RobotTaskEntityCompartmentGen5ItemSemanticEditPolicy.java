package procedure.diagram.edit.policies;

import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class RobotTaskEntityCompartmentGen5ItemSemanticEditPolicy extends
		ProcedureBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RobotTaskEntityCompartmentGen5ItemSemanticEditPolicy() {
		super(ProcedureElementTypes.RobotTaskEntity_3014);
	}

}
