package procedure.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import procedure.diagram.edit.commands.Loop4CreateCommand;
import procedure.diagram.edit.commands.Parallel3CreateCommand;
import procedure.diagram.edit.commands.RobotProcedure4CreateCommand;
import procedure.diagram.edit.commands.RobotTaskEntity4CreateCommand;
import procedure.diagram.edit.commands.Sequence4CreateCommand;
import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class SequenceCompartmentGen2ItemSemanticEditPolicy extends
		ProcedureBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public SequenceCompartmentGen2ItemSemanticEditPolicy() {
		super(ProcedureElementTypes.Sequence_3009);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (ProcedureElementTypes.RobotTaskEntity_3021 == req.getElementType()) {
			return getGEFWrapper(new RobotTaskEntity4CreateCommand(req));
		}
		if (ProcedureElementTypes.RobotProcedure_3022 == req.getElementType()) {
			return getGEFWrapper(new RobotProcedure4CreateCommand(req));
		}
		if (ProcedureElementTypes.Sequence_3023 == req.getElementType()) {
			return getGEFWrapper(new Sequence4CreateCommand(req));
		}
		if (ProcedureElementTypes.Parallel_3024 == req.getElementType()) {
			return getGEFWrapper(new Parallel3CreateCommand(req));
		}
		if (ProcedureElementTypes.Loop_3025 == req.getElementType()) {
			return getGEFWrapper(new Loop4CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
