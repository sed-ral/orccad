package procedure.diagram.edit.policies;

import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class RobotTaskEntityCompartmentGenItemSemanticEditPolicy extends
		ProcedureBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RobotTaskEntityCompartmentGenItemSemanticEditPolicy() {
		super(ProcedureElementTypes.RobotTaskEntity_2001);
	}

}
