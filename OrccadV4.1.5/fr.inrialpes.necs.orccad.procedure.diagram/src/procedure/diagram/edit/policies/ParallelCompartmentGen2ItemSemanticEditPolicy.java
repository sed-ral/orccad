package procedure.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import procedure.diagram.edit.commands.Loop2CreateCommand;
import procedure.diagram.edit.commands.Parallel4CreateCommand;
import procedure.diagram.edit.commands.RobotProcedure2CreateCommand;
import procedure.diagram.edit.commands.RobotTaskEntity2CreateCommand;
import procedure.diagram.edit.commands.Sequence3CreateCommand;
import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class ParallelCompartmentGen2ItemSemanticEditPolicy extends
		ProcedureBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public ParallelCompartmentGen2ItemSemanticEditPolicy() {
		super(ProcedureElementTypes.Parallel_3008);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (ProcedureElementTypes.RobotTaskEntity_3001 == req.getElementType()) {
			return getGEFWrapper(new RobotTaskEntity2CreateCommand(req));
		}
		if (ProcedureElementTypes.RobotProcedure_3002 == req.getElementType()) {
			return getGEFWrapper(new RobotProcedure2CreateCommand(req));
		}
		if (ProcedureElementTypes.Sequence_3009 == req.getElementType()) {
			return getGEFWrapper(new Sequence3CreateCommand(req));
		}
		if (ProcedureElementTypes.Parallel_3012 == req.getElementType()) {
			return getGEFWrapper(new Parallel4CreateCommand(req));
		}
		if (ProcedureElementTypes.Loop_3013 == req.getElementType()) {
			return getGEFWrapper(new Loop2CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
