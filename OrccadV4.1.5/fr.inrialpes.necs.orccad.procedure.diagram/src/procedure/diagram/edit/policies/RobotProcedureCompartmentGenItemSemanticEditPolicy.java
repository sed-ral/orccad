package procedure.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import procedure.diagram.edit.commands.Condition2CreateCommand;
import procedure.diagram.edit.commands.Loop3CreateCommand;
import procedure.diagram.edit.commands.Parallel2CreateCommand;
import procedure.diagram.edit.commands.RobotProcedure3CreateCommand;
import procedure.diagram.edit.commands.RobotTaskEntity3CreateCommand;
import procedure.diagram.edit.commands.Sequence2CreateCommand;
import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class RobotProcedureCompartmentGenItemSemanticEditPolicy extends
		ProcedureBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RobotProcedureCompartmentGenItemSemanticEditPolicy() {
		super(ProcedureElementTypes.RobotProcedure_3002);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (ProcedureElementTypes.RobotTaskEntity_3003 == req.getElementType()) {
			return getGEFWrapper(new RobotTaskEntity3CreateCommand(req));
		}
		if (ProcedureElementTypes.RobotProcedure_3004 == req.getElementType()) {
			return getGEFWrapper(new RobotProcedure3CreateCommand(req));
		}
		if (ProcedureElementTypes.Sequence_3005 == req.getElementType()) {
			return getGEFWrapper(new Sequence2CreateCommand(req));
		}
		if (ProcedureElementTypes.Parallel_3008 == req.getElementType()) {
			return getGEFWrapper(new Parallel2CreateCommand(req));
		}
		if (ProcedureElementTypes.Loop_3016 == req.getElementType()) {
			return getGEFWrapper(new Loop3CreateCommand(req));
		}
		if (ProcedureElementTypes.Condition_3026 == req.getElementType()) {
			return getGEFWrapper(new Condition2CreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
