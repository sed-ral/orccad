package procedure.diagram.edit.policies;

import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class RobotTaskEntityCompartmentGen4ItemSemanticEditPolicy extends
		ProcedureBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RobotTaskEntityCompartmentGen4ItemSemanticEditPolicy() {
		super(ProcedureElementTypes.RobotTaskEntity_3021);
	}

}
