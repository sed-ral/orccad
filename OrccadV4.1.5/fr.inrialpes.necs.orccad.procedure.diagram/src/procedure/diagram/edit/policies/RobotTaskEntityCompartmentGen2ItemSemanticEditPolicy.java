package procedure.diagram.edit.policies;

import procedure.diagram.providers.ProcedureElementTypes;

/**
 * @generated
 */
public class RobotTaskEntityCompartmentGen2ItemSemanticEditPolicy extends
		ProcedureBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public RobotTaskEntityCompartmentGen2ItemSemanticEditPolicy() {
		super(ProcedureElementTypes.RobotTaskEntity_3001);
	}

}
