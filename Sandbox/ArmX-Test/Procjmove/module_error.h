//----------------------------------------------------------
// Module error
//----------------------------------------------------------
#ifndef __orc_Mod_error_h
#define __orc_Mod_error_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"

#include "module_error_Inc.h"

class orc_Mod_error: public ModuleAlgo { 
protected: 
	// Internal Variables 
// Orccad Version: 3.0 alpha
// Module : error
// Variables declaration File
// Date of creation : Mon Nov  3 13:07:23 1997

int index;
int flagtrack;

public: 
	orc_Mod_error(ModuleTask *mt,double period):
	  ModuleAlgo("orc_Mod_error",mt,period) {};
	~orc_Mod_error() {};
	 // Output Ports declaration
	 double errpos[2];
	 // Output Event Ports declaration
	 int errtrack;
	 // Output param Ports declaration

	 // Methods of computation 
	 void init(const double posd[2], const double posc[2]); 
	 void param(); 
	 void reparam(); 
	 void compute(const double posd[2], const double posc[2] ); 
	 void end(); 
 }; 
#endif 
// End class  orc_Mod_error

