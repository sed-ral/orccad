//----------------------------------------------------------
// Module jtraj
//----------------------------------------------------------
#ifndef __orc_Mod_jtraj_h
#define __orc_Mod_jtraj_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"

#include "module_jtraj_Inc.h"

class orc_Mod_jtraj: public ModuleAlgo { 
protected: 
	// Internal Variables 
// Orccad Version: 3.0 alpha
// Module : trajectory
// Variables declaration File
// Date of creation : Mon Nov  3 13:05:11 1997

int index;
int mode;
int stop;

public: 
	orc_Mod_jtraj(ModuleTask *mt,double period):
	  ModuleAlgo("orc_Mod_jtraj",mt,period) {};
	~orc_Mod_jtraj() {};
	 // Output Ports declaration
	 double pos[2];
	 // Output Event Ports declaration
	 int endtraj;
	 int badtraj;
	 // Output param Ports declaration
	 double posd[2];

	 // Methods of computation 
	 void init(const int &typtraj, const double posI[2]); 
	 void param(); 
	 void reparam(); 
	 void compute(const int &typtraj, const double posI[2] ); 
	 void end(); 
 }; 
#endif 
// End class  orc_Mod_jtraj

