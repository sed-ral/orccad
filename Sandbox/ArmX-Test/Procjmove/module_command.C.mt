//----------------------------------------------------------
// Module module_command
//----------------------------------------------------------
//
// Orccad Version: ORCCADVERSION
// Module : module_command.C
//
// Ports :
//   input data  : double pose[2], double pos[2]
//   output data : double torque[2]
//   PARAM  : 
//   PRECONDITION EVENT : 
//   POSTCONDTION EVENT : 
//   T1 EXCEPTION EVENT : 
//   T2 EXCEPTION EVENT : 
//   T3 EXCEPTION EVENT : 
//
// Time Methods usable in module files:
//     GetLocalTime(): return relative period time as double value
//     GetCurrentTime(): return global period time as double value 
// 
// Date of creation : 2012/05/22 16:27:07
//----------------------------------------------------------

#include "module_command.h"
#include "Exec/prr.h"
#include "rts_ArmXjMove.h"

/*PROTECTED REGION ID(C_methods_ArmXjMove_orc_Mod_command) ENABLED START*/

  //__Add your code and delete ENABLED to regenerate it

/*PROTECTED REGION END*/


void orc_Mod_command::init(const double pose[2], const double pos[2])
{
/*PROTECTED REGION ID(init_ArmXjMove_orc_Mod_command) ENABLED START*/
for(index=0;index<2;index++)
  torque[index] = 0.0;
  //__Add your code and delete ENABLED to regenerate it

/*PROTECTED REGION END*/
}

void orc_Mod_command::param()
{
  plugParam();
} 

void orc_Mod_command::reparam()
{
} 

void orc_Mod_command::compute(const double pose[2], const double pos[2])
{
/*PROTECTED REGION ID(compute_ArmXjMove_orc_Mod_command) ENABLED START*/
// Gravity compensation
torque[0] = ARM_M2*ARM_L2*ARM_GRAV*cos(pos[0]+pos[1]) 
            + (ARM_M1+ARM_M2)*ARM_L1*ARM_GRAV*cos(pos[0]);
torque[1] = ARM_M2*ARM_L2*ARM_GRAV*cos(pos[0]+pos[1]);

// Proportionnal correction
torque[0] = KX*pose[0] + torque[0];
torque[1] = KY*pose[1] + torque[1];
  //__Add your code and delete ENABLED to regenerate it

/*PROTECTED REGION END*/
//T1 events associated parameters processing

//check T1 raised parameters
}
void orc_Mod_command::end()
{
/*PROTECTED REGION ID(end_ArmXjMove_orc_Mod_command) ENABLED START*/

  //__Add your code and delete ENABLED to regenerate it

/*PROTECTED REGION END*/
} 
// End of class orc_Mod_command
