/**
 * This file defines the API functions for Robot Procedure AppliArmX
 * to connect with the generated Orccad program
 * File automatically generated
 *
 * File : AppliArmXAPI.h
 * Date of creation : 2012/08/22 18:03:29
 */
#ifndef __orc_AppliArmXApi_h
#define __orc_AppliArmXApi_h

// C function Interfaces
extern "C" {
  int orcAppliArmXInit();
  int orcAppliArmXLaunch();
  int orcAppliArmXTaskLaunch();
  int orcAppliArmXGo();
  int orcAppliArmXStop();
  int orcArmXjMove_orcc0Prm(double posd[2]);
  int orcArmXjMove_orcc1Prm(double posd[2]);
}// extern "C"

#endif
// End file AppliArmXAPI.h
