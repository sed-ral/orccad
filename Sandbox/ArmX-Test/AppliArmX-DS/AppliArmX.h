/**
 * Execution code for Robot Procedure AppliArmX 
 * File automatically generated
 *
 * File : AppliArmX.h
 * Date of creation : 2012/08/22 18:03:29
 */
#ifndef __orc_AppliArmX_h
#define __orc_AppliArmX_h

#include "Exec/utils.h"
#include "Exec/prr.h"

#include "interface_AppliArmX.h"
#include "rts_ArmXjMove_orcc0.h"
#include "rts_ArmXcMove.h"
#include "rts_ArmXfMove.h"
#include "rts_ArmXjMove_orcc1.h"
#include "PhR_WinX.h"

#define NBCLKS 1 // to be improved !
#define BASECLK  1.0 // to be improved !

class orc_AppliArmX : public ProcedureRobot
{
protected :
  // Index Tab for clocks
  double *TabClks;
  // Physical Resources
  orc_PhR_WinX *orcWinX;

public :
    // Robot Tasks
    orc_RT_ArmXjMove_orcc0 *ArmXjMove_orcc0;
    orc_RT_ArmXjMove_orcc0 *GetRTArmXjMove_orcc0Ptr() { return ArmXjMove_orcc0; };
    orc_RT_ArmXcMove *ArmXcMove;
    orc_RT_ArmXcMove *GetRTArmXcMovePtr() { return ArmXcMove; };
    orc_RT_ArmXfMove *ArmXfMove;
    orc_RT_ArmXfMove *GetRTArmXfMovePtr() { return ArmXfMove; };
    orc_RT_ArmXjMove_orcc1 *ArmXjMove_orcc1;
    orc_RT_ArmXjMove_orcc1 *GetRTArmXjMove_orcc1Ptr() { return ArmXjMove_orcc1; };

    orc_AppliArmX();
    ~orc_AppliArmX();
    void Launch();
};
#endif
// End class orc_AppliArmX
