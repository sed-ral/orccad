/**
 * Robot Task Execution model
 * File automatically generated
 * 
 * File : rts_ArmXcMove.h
 * Date of creation :2012/08/22 18:03:29
 */
#ifndef __rts_ArmXcMove_h
#define __rts_ArmXcMove_h

#include "Exec/utils.h"
#include "Exec/rt.h"
#include "Exec/param.h"
#include "Exec/paramType.h"

#include "Exec/singleReaderACM.h"

#include "mt_CTArmXcMove.h"

class orc_RT_ArmXcMove: public RobotTask
{
protected:
  char cfgFile[MAXFILENAME];
  //Robot task Output parameters
  //Robot task Input parameters
  // T1 parameters
 // Module Tasks
  orc_Mt_CTArmXcMove mt_CTArmXcMove;
  // Links between Mt

public:
  //T1 Exceptions
    
  orc_RT_ArmXcMove(char* , ProcedureRobot *, PhR *, char *);
  ~orc_RT_ArmXcMove();
  
  int SetParam();
  int TestTimeout();
  int SetT1Exceptions();
  ModuleTask* get_mt_CTArmXcMove(void);
};
#endif
// End class orc_RT_ArmXcMove
