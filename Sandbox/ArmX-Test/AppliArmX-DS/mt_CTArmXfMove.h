/**
 * Module execution handling according to the 
 * specified temporal constraints : CTArmXfMove
 *
 * Orccad Version: ORCCADVERSION
 * File : mt_CTArmXfMove.h
 * Date of creation : 2012/08/22 18:03:29
 */
#ifndef __mt_CTArmXfMove_h
#define __mt_CTArmXfMove_h



//ModAlgo Level = 0 depList size = 0

//ModAlgo Level = 0 depList size = 0

//ModAlgo Level = 1 depList size = 1

//ModAlgo Level = 2 depList size = 1

//ModAlgo Level = 3 depList size = 1



#include "Exec/mt.h"
#include "module_sens2th.h"
#include "module_obs.h"
#include "module_traj.h"
#include "module_error.h"
#include "module_command.h"
#include "PhR_WinX.h"

class RobotTask;
class orc_RT_ArmXfMove;
class orc_Mt_CTArmXfMove;
extern void fct_CTArmXfMove(orc_Mt_CTArmXfMove *);

class orc_Mt_CTArmXfMove: public ModuleTask
{
  
  friend void fct_CTArmXfMove(orc_Mt_CTArmXfMove *);
    
protected:
  orc_Mod_sens2th sens2th_b0;
  orc_Mod_obs obs_90;
  orc_Mod_traj traj_7;
  orc_Mod_error error_a0;
  orc_Mod_command command_80;
  orc_PhR_WinX* WinX;
  orc_RT_ArmXfMove *pRT;
    
public:
  void InitTrace();

  orc_Mt_CTArmXfMove(RobotTask *rt, char *name, int itFilter, double period, SYNC_TYPE S_Type, int priority, int nb_sem_link, CONSTRAINT_TYPE C_Type)
    : ModuleTask((FUNCPTR) fct_CTArmXfMove, rt, name, itFilter, period, S_Type, priority, nb_sem_link, C_Type),
sens2th_b0(this, period), obs_90(this, period), traj_7(this, period), error_a0(this, period), command_80(this, period)  {
    InitTrace();
    WinX = (orc_PhR_WinX *) RT->GetPhR();
    pRT = (orc_RT_ArmXfMove *)RT;
  }
  ~orc_Mt_CTArmXfMove() {};
  
  void Param();
  void ReParam();
  int SetT1Exceptions();
  orc_Mod_sens2th &getsens2th_b0() {return sens2th_b0;};
  orc_Mod_obs &getobs_90() {return obs_90;};
  orc_Mod_traj &gettraj_7() {return traj_7;};
  orc_Mod_error &geterror_a0() {return error_a0;};
  orc_Mod_command &getcommand_80() {return command_80;};
};
#endif
// End of class orc_Mt_CTArmXfMove
