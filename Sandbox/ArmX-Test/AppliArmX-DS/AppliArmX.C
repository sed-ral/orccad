/**
 * Execution code for Robot Procedure AppliArmX
 * File automatically generated
 *
 * File : AppliArmX.C
 * Date of creation : 2012/08/22 18:03:29
 */
#include "AppliArmX.h"
#include "AppliArmXAPI.h"

orc_AppliArmX::orc_AppliArmX() : ProcedureRobot()
{
  TabClks = new double[NBCLKS];
  TabClks[0] = 1;
  
  SetUp((char *)"orcPrRAppliArmX", 0,
        (FUNCPTR) aa_AppliArmX,
        (FUNCPTR) prr_reset,
        TABSIG,
        TABEVENT, NB_SIGNALS,
        
        TabClks, NBCLKS,
        
        DBG_NONE); //Beware to be improved
  if (!GetStatus()) {
    delete this;
    return;
  }
  
  SetPath((char *)"$(ORCCAD_USER)/Bin/$(ARCH)/"); // Beware to be improved
    
  //Physical Resource Creation
  orcWinX = new orc_PhR_WinX(this, (char *)"WinX", 
                               InitOK_WinX, ActivateOK_WinX, CmdStopOK_WinX, KillOK_WinX);
  if (!orcWinX->GetStatus()) {
    delete orcWinX;
    return;
  }
  // Physical Resource Sensor Creation
  // Robot Task Creation
  ArmXjMove_orcc0 = new orc_RT_ArmXjMove_orcc0((char *)"ArmXjMove_orcc0", this, orcWinX, (char *)"WinX");
  if (!ArmXjMove_orcc0->GetStatus()) {
    EventStorage->displayError(PRR_ERROR3, 0.0);
    delete ArmXjMove_orcc0;
    delete this;
    return;
  }
  // Robot Task Signal Connection
  if (!insertRT(ArmXjMove_orcc0, (FUNCPTR) aa_AppliArmX_I_ArmXjMove_orcc0_Start)) {
  	EventStorage->displayError(PRR_ERROR3, 0.0);
    delete this;
    return;
  }
  ArmXcMove = new orc_RT_ArmXcMove((char *)"ArmXcMove", this, orcWinX, (char *)"WinX");
  if (!ArmXcMove->GetStatus()) {
    EventStorage->displayError(PRR_ERROR3, 0.0);
    delete ArmXcMove;
    delete this;
    return;
  }
  // Robot Task Signal Connection
  if (!insertRT(ArmXcMove, (FUNCPTR) aa_AppliArmX_I_ArmXcMove_Start)) {
  	EventStorage->displayError(PRR_ERROR3, 0.0);
    delete this;
    return;
  }
  ArmXfMove = new orc_RT_ArmXfMove((char *)"ArmXfMove", this, orcWinX, (char *)"WinX");
  if (!ArmXfMove->GetStatus()) {
    EventStorage->displayError(PRR_ERROR3, 0.0);
    delete ArmXfMove;
    delete this;
    return;
  }
  // Robot Task Signal Connection
  if (!insertRT(ArmXfMove, (FUNCPTR) aa_AppliArmX_I_ArmXfMove_Start)) {
  	EventStorage->displayError(PRR_ERROR3, 0.0);
    delete this;
    return;
  }
  ArmXjMove_orcc1 = new orc_RT_ArmXjMove_orcc1((char *)"ArmXjMove_orcc1", this, orcWinX, (char *)"WinX");
  if (!ArmXjMove_orcc1->GetStatus()) {
    EventStorage->displayError(PRR_ERROR3, 0.0);
    delete ArmXjMove_orcc1;
    delete this;
    return;
  }
  // Robot Task Signal Connection
  if (!insertRT(ArmXjMove_orcc1, (FUNCPTR) aa_AppliArmX_I_ArmXjMove_orcc1_Start)) {
  	EventStorage->displayError(PRR_ERROR3, 0.0);
    delete this;
    return;
  }
  if (!GetStatus()) {
    EventStorage->displayError(PRR_ERROR3,0.0);
    delete this;
    return;
  }
  // Init of Horloge
  initHl();
}

orc_AppliArmX::~orc_AppliArmX()
{
  delete orcWinX;
  
}

void orc_AppliArmX::Launch()
{
  // Start execution
  StartAutomaton();
  // Wait End
  WaitEnd();
  // Close Physical Resource
  if (orcWinX->GetState() == PHR_ACCESS)
    orcWinX->Close();
  fprintf(stdout, (char *)"END\n");
  // Save Session and Kill
  delete this;
}

// C function Interfaces
orc_AppliArmX *_Orc_AppliArmX;

orc_AppliArmX *GetRPrPtr()
{
  return _Orc_AppliArmX;
}

int orcAppliArmXInit()
{
  int cr = OK;
  _Orc_AppliArmX = new orc_AppliArmX();
  if (_Orc_AppliArmX == NULL)
    cr = ERROR;
  else if (!_Orc_AppliArmX->GetStatus())
    cr = ERROR;

  return cr;
}

int orcAppliArmXTaskLaunch()
{
  int cr = OK;
  if (!_Orc_AppliArmX->GetStatus())
    cr = ERROR;
  else 
    _Orc_AppliArmX->Launch();

  return cr;
}

int orcAppliArmXLaunch()
{
  int prrid, cr = OK;
  return cr;
}

//Trap Ctrl C - Segmentation Fault - Bus Error Interruption
extern "C" void SIG_Handler(int nSig)
{
  switch (nSig) {
  // Trap Ctrl C Interruption
  case SIGINT:
    fprintf(stderr, "Ctrl C Interrupt\n");
    break;
  // Trap Segmentation Fault Interruption
  case SIGSEGV:
    fprintf(stderr, "Segmentation Fault Interrupt\n");
    break;
  // Trap Bus Error Interruption
  case SIGBUS:
    fprintf(stderr, "Bus Error Interrupt\n");
    break;
  }
  delete _Orc_AppliArmX;
}

int main()
{
  int cr = OK;
  //Trap Ctrl C, SEGV or Bus Error signals
  struct sigaction action;
  action.sa_handler = SIG_Handler;
  sigemptyset (&action.sa_mask);
  action.sa_flags = 0;
  sigaction(SIGINT, &action, NULL);
  sigaction(SIGSEGV, &action, NULL);
  sigaction(SIGBUS, &action, NULL);
  // orcMakeRealTime();
  cr = orcAppliArmXInit();
  if (cr == OK) 
    cr = orcAppliArmXTaskLaunch();

  return cr;
}

int orcAppliArmXGo()
{
  int cr = OK;
  cr = orcAppliArmXInit();
  if (cr == OK) 
    cr = orcAppliArmXLaunch();

  return cr;
}

int orcAppliArmXStop()
{
  int cr = OK;
  if (!_Orc_AppliArmX->GetStatus())
    cr = ERROR;
  else 
    _Orc_AppliArmX->Reset();

  return cr;
}

int orcArmXjMove_orcc0Prm(double posd[2])
{
  int cr = OK;
  if (!_Orc_AppliArmX->GetStatus())
    cr = ERROR;
  else
    _Orc_AppliArmX->ArmXjMove_orcc0->ChangeParam(posd);

  return cr;
}
int orcArmXjMove_orcc1Prm(double posd[2])
{
  int cr = OK;
  if (!_Orc_AppliArmX->GetStatus())
    cr = ERROR;
  else
    _Orc_AppliArmX->ArmXjMove_orcc1->ChangeParam(posd);

  return cr;
}
