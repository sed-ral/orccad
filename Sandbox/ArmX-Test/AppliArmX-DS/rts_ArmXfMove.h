/**
 * Robot Task Execution model
 * File automatically generated
 * 
 * File : rts_ArmXfMove.h
 * Date of creation :2012/08/22 18:03:29
 */
#ifndef __rts_ArmXfMove_h
#define __rts_ArmXfMove_h

#include "Exec/utils.h"
#include "Exec/rt.h"
#include "Exec/param.h"
#include "Exec/paramType.h"

#include "Exec/singleReaderACM.h"

#include "mt_CTArmXfMove.h"

class orc_RT_ArmXfMove: public RobotTask
{
protected:
  char cfgFile[MAXFILENAME];
  //Robot task Output parameters
  //Robot task Input parameters
  // T1 parameters
 // Module Tasks
  orc_Mt_CTArmXfMove mt_CTArmXfMove;
  // Links between Mt

public:
  //T1 Exceptions
    
  orc_RT_ArmXfMove(char* , ProcedureRobot *, PhR *, char *);
  ~orc_RT_ArmXfMove();
  
  int SetParam();
  int TestTimeout();
  int SetT1Exceptions();
  ModuleTask* get_mt_CTArmXfMove(void);
};
#endif
// End class orc_RT_ArmXfMove
