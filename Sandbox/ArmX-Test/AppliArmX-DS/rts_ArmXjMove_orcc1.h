/**
 * Robot Task Execution model
 * File automatically generated
 * 
 * File : rts_ArmXjMove_orcc1.h
 * Date of creation :2012/08/22 18:03:29
 */
#ifndef __rts_ArmXjMove_orcc1_h
#define __rts_ArmXjMove_orcc1_h

#include "Exec/utils.h"
#include "Exec/rt.h"
#include "Exec/param.h"
#include "Exec/paramType.h"

#include "Exec/singleReaderACM.h"

#include "mt_CTArmjMove_orcc1.h"

class orc_RT_ArmXjMove_orcc1: public RobotTask
{
protected:
  char cfgFile[MAXFILENAME];
  //Robot task Output parameters
  //Robot task Input parameters
  double posd[2];
  // T1 parameters
  paramType swtraj;
 // Module Tasks
  orc_Mt_CTArmjMove_orcc1 mt_CTArmjMove_orcc1;
  // Links between Mt

public:
  //T1 Exceptions
  int aut_output_swtraj;
  paramType Getswtraj() { return swtraj;};
  void Putswtraj(int _T1) { 
    //printf("RT::Putswtraj() _T1 = %d\n", _T1);
  swtraj.isT1 = _T1;
  swtraj.varT1 = mt_CTArmjMove_orcc1.getjobs_110().getT1Var();
  };
    
  orc_RT_ArmXjMove_orcc1(char* , ProcedureRobot *, PhR *, char *);
  ~orc_RT_ArmXjMove_orcc1();
  
  int SetParam();
  int TestTimeout();
  int SetT1Exceptions();
  int SetParam(double _posd[2]);
  int ChangeParam(double _posd[2]);
  ModuleTask* get_mt_CTArmjMove_orcc1(void);
};
#endif
// End class orc_RT_ArmXjMove_orcc1
