/**
 * Module execution handling according to the 
 * specified temporal constraints : CTArmjMove_orcc1
 *
 * Orccad Version: ORCCADVERSION
 * File : mt_CTArmjMove_orcc1.h
 * Date of creation : 2012/08/22 18:03:29
 */
#ifndef __mt_CTArmjMove_orcc1_h
#define __mt_CTArmjMove_orcc1_h



//ModAlgo Level = 0 depList size = 0

//ModAlgo Level = 0 depList size = 0

//ModAlgo Level = 1 depList size = 1

//ModAlgo Level = 2 depList size = 1



#include "Exec/mt.h"
#include "module_jtraj.h"
#include "module_jobs.h"
#include "module_error.h"
#include "module_command.h"
#include "PhR_WinX.h"

class RobotTask;
class orc_RT_ArmXjMove_orcc1;
class orc_Mt_CTArmjMove_orcc1;
extern void fct_CTArmjMove_orcc1(orc_Mt_CTArmjMove_orcc1 *);

class orc_Mt_CTArmjMove_orcc1: public ModuleTask
{
  
  friend void fct_CTArmjMove_orcc1(orc_Mt_CTArmjMove_orcc1 *);
    
protected:
  orc_Mod_jtraj jtraj_111;
  orc_Mod_jobs jobs_110;
  orc_Mod_error error_112;
  orc_Mod_command command_19;
  orc_PhR_WinX* WinX;
  orc_RT_ArmXjMove_orcc1 *pRT;
    
public:
  void InitTrace();

  orc_Mt_CTArmjMove_orcc1(RobotTask *rt, char *name, int itFilter, double period, SYNC_TYPE S_Type, int priority, int nb_sem_link, CONSTRAINT_TYPE C_Type)
    : ModuleTask((FUNCPTR) fct_CTArmjMove_orcc1, rt, name, itFilter, period, S_Type, priority, nb_sem_link, C_Type),
jtraj_111(this, period), jobs_110(this, period), error_112(this, period), command_19(this, period)  {
    InitTrace();
    WinX = (orc_PhR_WinX *) RT->GetPhR();
    pRT = (orc_RT_ArmXjMove_orcc1 *)RT;
  }
  ~orc_Mt_CTArmjMove_orcc1() {};
  
  void Param();
  void ReParam();
  int SetT1Exceptions();
  orc_Mod_jtraj &getjtraj_111() {return jtraj_111;};
  orc_Mod_jobs &getjobs_110() {return jobs_110;};
  orc_Mod_error &geterror_112() {return error_112;};
  orc_Mod_command &getcommand_19() {return command_19;};
};
#endif
// End of class orc_Mt_CTArmjMove_orcc1
