/**
 * Module execution handling according to the 
 * specified temporal constraints : CTArmXcMove
 *
 * Orccad Version: ORCCADVERSION
 * File : mt_CTArmXcMove.h
 * Date of creation : 2012/08/22 18:03:29
 */
#ifndef __mt_CTArmXcMove_h
#define __mt_CTArmXcMove_h



//ModAlgo Level = 0 depList size = 0

//ModAlgo Level = 0 depList size = 0

//ModAlgo Level = 1 depList size = 1

//ModAlgo Level = 2 depList size = 1

//ModAlgo Level = 3 depList size = 1

//ModAlgo Level = 4 depList size = 1



#include "Exec/mt.h"
#include "module_obs.h"
#include "module_sens2x.h"
#include "module_invcart.h"
#include "module_traj.h"
#include "module_error.h"
#include "module_command.h"
#include "PhR_WinX.h"

class RobotTask;
class orc_RT_ArmXcMove;
class orc_Mt_CTArmXcMove;
extern void fct_CTArmXcMove(orc_Mt_CTArmXcMove *);

class orc_Mt_CTArmXcMove: public ModuleTask
{
  
  friend void fct_CTArmXcMove(orc_Mt_CTArmXcMove *);
    
protected:
  orc_Mod_obs obs_2;
  orc_Mod_sens2x sens2x_5;
  orc_Mod_invcart invcart_4;
  orc_Mod_traj traj_0;
  orc_Mod_error error_3;
  orc_Mod_command command_1;
  orc_PhR_WinX* WinX;
  orc_RT_ArmXcMove *pRT;
    
public:
  void InitTrace();

  orc_Mt_CTArmXcMove(RobotTask *rt, char *name, int itFilter, double period, SYNC_TYPE S_Type, int priority, int nb_sem_link, CONSTRAINT_TYPE C_Type)
    : ModuleTask((FUNCPTR) fct_CTArmXcMove, rt, name, itFilter, period, S_Type, priority, nb_sem_link, C_Type),
obs_2(this, period), sens2x_5(this, period), invcart_4(this, period), traj_0(this, period), error_3(this, period), command_1(this, period)  {
    InitTrace();
    WinX = (orc_PhR_WinX *) RT->GetPhR();
    pRT = (orc_RT_ArmXcMove *)RT;
  }
  ~orc_Mt_CTArmXcMove() {};
  
  void Param();
  void ReParam();
  int SetT1Exceptions();
  orc_Mod_obs &getobs_2() {return obs_2;};
  orc_Mod_sens2x &getsens2x_5() {return sens2x_5;};
  orc_Mod_invcart &getinvcart_4() {return invcart_4;};
  orc_Mod_traj &gettraj_0() {return traj_0;};
  orc_Mod_error &geterror_3() {return error_3;};
  orc_Mod_command &getcommand_1() {return command_1;};
};
#endif
// End of class orc_Mt_CTArmXcMove
