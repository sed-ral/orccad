/**
 * Interface component to cope with events from or to STRL program
 *
 * File generated automatically
 *
 * File : interface_AppliArmX.h
 * Date of creation : 2012/08/22 18:03:29
 */
#ifndef __interface_h
#define __interface_h

#include "Exec/canal.h"

enum INPUT_SIGNALS_USER {
  ArmXjMove_orcc0_Start = COMPUTETIMEOUT + 1,
  ArmXcMove_Start,
  ArmXfMove_Start,
  ArmXjMove_orcc1_Start,
  USERSTART_ArmXjMove_orcc0,
  USERSTART_ArmXcMove,
  USERSTART_ArmXfMove,
  USERSTART_ArmXjMove_orcc1,
  swtraj,
  reconf,
  badtraj,
  redbut,
  outbound,
  errtrack,
  endtraj,
  outwork,
  inwork,
  InitOK_WinX,
  ActivateOK_WinX,
  KillOK_WinX,
  CmdStopOK_WinX,
  NB_SIGNALS
};

extern "C" { int aa_AppliArmX(); }
extern "C" { int aa_AppliArmX_reset (); }
extern "C" { int prr_reset (ProcedureRobot *); }
extern "C" {
  extern FUNCPTR TABSIG[NB_SIGNALS];
  FUNCPTR aa_AppliArmX_I_ArmXjMove_orcc0_Start();
  FUNCPTR aa_AppliArmX_I_ArmXcMove_Start();
  FUNCPTR aa_AppliArmX_I_ArmXfMove_Start();
  FUNCPTR aa_AppliArmX_I_ArmXjMove_orcc1_Start();
  FUNCPTR aa_AppliArmX_I_USERSTART_ArmXjMove_orcc0();
  FUNCPTR aa_AppliArmX_I_USERSTART_ArmXcMove();
  FUNCPTR aa_AppliArmX_I_USERSTART_ArmXfMove();
  FUNCPTR aa_AppliArmX_I_USERSTART_ArmXjMove_orcc1();
  FUNCPTR aa_AppliArmX_I_swtraj();
  FUNCPTR aa_AppliArmX_I_reconf();
  FUNCPTR aa_AppliArmX_I_badtraj();
  FUNCPTR aa_AppliArmX_I_redbut();
  FUNCPTR aa_AppliArmX_I_outbound();
  FUNCPTR aa_AppliArmX_I_errtrack();
  FUNCPTR aa_AppliArmX_I_endtraj();
  FUNCPTR aa_AppliArmX_I_outwork();
  FUNCPTR aa_AppliArmX_I_inwork();
  FUNCPTR aa_AppliArmX_I_InitOK_WinX();
  FUNCPTR aa_AppliArmX_I_ActivateOK_WinX();
  FUNCPTR aa_AppliArmX_I_KillOK_WinX();
  FUNCPTR aa_AppliArmX_I_CmdStopOK_WinX();
  FUNCPTR aa_AppliArmX_I_Prr_Start(void*);
  FUNCPTR aa_AppliArmX_I_ROBOT_FAIL();
  FUNCPTR aa_AppliArmX_I_SOFT_FAIL();
  FUNCPTR aa_AppliArmX_I_SENSOR_FAIL();
  FUNCPTR aa_AppliArmX_I_CPU_OVERLOAD();
  int aa_AppliArmX_O_Abort_ArmXjMove_orcc0(void *);
  int aa_AppliArmX_O_Abort_ArmXcMove(void *);
  int aa_AppliArmX_O_Abort_ArmXfMove(void *);
  int aa_AppliArmX_O_Abort_ArmXjMove_orcc1(void *);
  int aa_AppliArmX_O_ActivateArmXjMove_orcc0_WinX(void *);
  int aa_AppliArmX_O_ActivateArmXcMove_WinX(void *);
  int aa_AppliArmX_O_ActivateArmXfMove_WinX(void *);
  int aa_AppliArmX_O_ActivateArmXjMove_orcc1_WinX(void *);
  int aa_AppliArmX_O_ArmXjMove_orcc0Transite(void *);
  int aa_AppliArmX_O_ArmXcMoveTransite(void *);
  int aa_AppliArmX_O_ArmXfMoveTransite(void *);
  int aa_AppliArmX_O_ArmXjMove_orcc1Transite(void *);
  int aa_AppliArmX_O_Treatswtraj_ArmXjMove_orcc0(void*);	
  int aa_AppliArmX_O_Treatswtraj_ArmXjMove_orcc1(void*);	
  int aa_AppliArmX_O_Activate(void*);
  int aa_AppliArmX_O_Transite(void*);
  int aa_AppliArmX_O_EndParam(void*);
  int aa_AppliArmX_O_EndKill(void*);
  int aa_AppliArmX_O_EndObs(void*);
  int aa_AppliArmX_O_FinBF(void*);
  int aa_AppliArmX_O_FinT3(void*);
  int aa_AppliArmX_O_GoodEndRPr();
  int aa_AppliArmX_O_T3RPr();
  int f_default();
} // end extern

extern char TABEVENT[NB_SIGNALS][MAXCHARNAME];
#endif
