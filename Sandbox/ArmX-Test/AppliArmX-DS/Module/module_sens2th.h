//----------------------------------------------------------
// Module sens2th
//
// File : module_sens2th.h
// Date of creation : 2012/08/22 18:03:29
//----------------------------------------------------------
#ifndef __orc_Mod_sens2th_h
#define __orc_Mod_sens2th_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"
#include "Exec/paramType.h"
/*PROTECTED REGION ID(h_include_ArmXfMove_orc_Mod_sens2th) ENABLED START*/
#include <math.h>

// Robot parameters
#define ARM_L1   1.0
#define ARM_L2   1.0

//__Add include file directive here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/

class orc_Mod_sens2th: public ModuleAlgo { 
protected: 
  // Internal Variables 
  char rt_pname[MAXCHARNAME];
/*PROTECTED REGION ID(h_intern_ArmXfMove_orc_Mod_sens2th) ENABLED START*/
double k1,k2;
double x[2];
double s12,c12;
int    top;
  //__Add local variable declaration here and delete ENABLED to regenerate it
/*PROTECTED REGION END*/

public:
  orc_Mod_sens2th(ModuleTask *mt, double period) :
    ModuleAlgo((char *)"orc_Mod_sens2th", mt, period){};
  ~orc_Mod_sens2th(){};

  // Output Data Ports declaration
  double posd[2];

  // Output Event Ports
  int inwork;
  
  //T1 Parameters declaration
  // Parameter Ports declaration			
  // Value sent by T1
  // Methods of computation
  
 /*PROTECTED REGION ID(h_methods_ArmXfMove_orc_Mod_sens2th) ENABLED START*/

  //__Add custom methods here and delete ENABLED to regenerate it
 /*PROTECTED REGION END*/

  void init(const double pos[2], const double sensor[2]); 
  void param(); 
  void reparam(); 
  void compute(const double pos[2], const double sensor[2]); 
  void end();
};
#endif
// End of class orc_Mod_sens2th
