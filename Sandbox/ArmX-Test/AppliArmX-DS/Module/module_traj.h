//----------------------------------------------------------
// Module traj
//
// File : module_traj.h
// Date of creation : 2012/08/22 18:03:29
//----------------------------------------------------------
#ifndef __orc_Mod_traj_h
#define __orc_Mod_traj_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"
#include "Exec/paramType.h"
/*PROTECTED REGION ID(h_include_ArmXfMove_orc_Mod_traj) ENABLED START*/
#define STEP_POS 0.05
//__Add include file directive here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/

class orc_Mod_traj: public ModuleAlgo { 
protected: 
  // Internal Variables 
  char rt_pname[MAXCHARNAME];
/*PROTECTED REGION ID(h_intern_ArmXfMove_orc_Mod_traj) ENABLED START*/
int index;
  //__Add local variable declaration here and delete ENABLED to regenerate it
/*PROTECTED REGION END*/

public:
  orc_Mod_traj(ModuleTask *mt, double period) :
    ModuleAlgo((char *)"orc_Mod_traj", mt, period){};
  ~orc_Mod_traj(){};

  // Output Data Ports declaration
  double posi[2];

  // Output Event Ports
  
  //T1 Parameters declaration
  // Parameter Ports declaration			
  // Value sent by T1
  // Methods of computation
  
 /*PROTECTED REGION ID(h_methods_ArmXfMove_orc_Mod_traj) ENABLED START*/

  //__Add custom methods here and delete ENABLED to regenerate it
 /*PROTECTED REGION END*/

  void init(const double pos[2], const double posd[2]); 
  void param(); 
  void reparam(); 
  void compute(const double pos[2], const double posd[2]); 
  void end();
};
#endif
// End of class orc_Mod_traj
