//----------------------------------------------------------
// Module obs
//
// File : module_obs.h
// Date of creation : 2012/08/22 18:03:29
//----------------------------------------------------------
#ifndef __orc_Mod_obs_h
#define __orc_Mod_obs_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"
#include "Exec/paramType.h"
/*PROTECTED REGION ID(h_include_ArmXfMove_orc_Mod_obs) ENABLED START*/

//__Add include file directive here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/

class orc_Mod_obs: public ModuleAlgo { 
protected: 
  // Internal Variables 
  char rt_pname[MAXCHARNAME];
/*PROTECTED REGION ID(h_intern_ArmXfMove_orc_Mod_obs) ENABLED START*/

  //__Add local variable declaration here and delete ENABLED to regenerate it
/*PROTECTED REGION END*/

public:
  orc_Mod_obs(ModuleTask *mt, double period) :
    ModuleAlgo((char *)"orc_Mod_obs", mt, period){};
  ~orc_Mod_obs(){};

  // Output Data Ports declaration
  
  // Output Event Ports
  int redbut;
  int outbound;
  
  //T1 Parameters declaration
  // Parameter Ports declaration			
  // Value sent by T1
  // Methods of computation
  
 /*PROTECTED REGION ID(h_methods_ArmXfMove_orc_Mod_obs) ENABLED START*/

  //__Add custom methods here and delete ENABLED to regenerate it
 /*PROTECTED REGION END*/

  void init(const int  &key, const int limit[2]); 
  void param(); 
  void reparam(); 
  void compute(const int  &key, const int limit[2]); 
  void end();
};
#endif
// End of class orc_Mod_obs
