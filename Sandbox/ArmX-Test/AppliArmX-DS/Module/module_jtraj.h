//----------------------------------------------------------
// Module jtraj
//
// File : module_jtraj.h
// Date of creation : 2012/08/22 18:03:29
//----------------------------------------------------------
#ifndef __orc_Mod_jtraj_h
#define __orc_Mod_jtraj_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"
#include "Exec/paramType.h"
/*PROTECTED REGION ID(h_include_ArmXjMove_orcc1_orc_Mod_jtraj) ENABLED START*/
#include <math.h>

#define LIMITMAX   M_PI
#define LIMITMIN  -M_PI
#define STEP_JPOS   0.01
#define EPSILON    0.01

#define MODE_SUSPEND 0
#define MODE_ACTIVE  1
//__Add include file directive here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/

class orc_Mod_jtraj: public ModuleAlgo { 
protected: 
  // Internal Variables 
  char rt_pname[MAXCHARNAME];
/*PROTECTED REGION ID(h_intern_ArmXjMove_orcc1_orc_Mod_jtraj) ENABLED START*/
int index;
int mode;
int stop;
  //__Add local variable declaration here and delete ENABLED to regenerate it
/*PROTECTED REGION END*/

public:
  orc_Mod_jtraj(ModuleTask *mt, double period) :
    ModuleAlgo((char *)"orc_Mod_jtraj", mt, period){};
  ~orc_Mod_jtraj(){};

  // Output Data Ports declaration
  double pos[2];

  // Output Event Ports
  int badtraj;
  int endtraj;
  
  //T1 Parameters declaration
  paramType typtrajT1Param;
  
  // Parameter Ports declaration			
  double posd[2];
  int typtraj;
  // Value sent by T1
  // Methods of computation
  
 /*PROTECTED REGION ID(h_methods_ArmXjMove_orcc1_orc_Mod_jtraj) ENABLED START*/

  //__Add custom methods here and delete ENABLED to regenerate it
 /*PROTECTED REGION END*/

  void init(const double posI[2]); 
  void param(); 
  void reparam(); 
  void compute(const double posI[2]); 
  void end();
};
#endif
// End of class orc_Mod_jtraj
