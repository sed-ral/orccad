//----------------------------------------------------------
// Module sens2x
//
// File : module_sens2x.h
// Date of creation : 2012/08/22 18:03:28
//----------------------------------------------------------
#ifndef __orc_Mod_sens2x_h
#define __orc_Mod_sens2x_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"
#include "Exec/paramType.h"
/*PROTECTED REGION ID(h_include_ArmXcMove_orc_Mod_sens2x) ENABLED START*/
#include <math.h>

// Robot parameters
#define ARM_L1   1.0
#define ARM_L2   1.0
//__Add include file directive here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/

class orc_Mod_sens2x: public ModuleAlgo { 
protected: 
  // Internal Variables 
  char rt_pname[MAXCHARNAME];
/*PROTECTED REGION ID(h_intern_ArmXcMove_orc_Mod_sens2x) ENABLED START*/
double k1,k2;

double s12,c12;

int top;
  //__Add local variable declaration here and delete ENABLED to regenerate it
/*PROTECTED REGION END*/

public:
  orc_Mod_sens2x(ModuleTask *mt, double period) :
    ModuleAlgo((char *)"orc_Mod_sens2x", mt, period){};
  ~orc_Mod_sens2x(){};

  // Output Data Ports declaration
  double x[2];

  // Output Event Ports
  int outwork;
  
  //T1 Parameters declaration
  // Parameter Ports declaration			
  // Value sent by T1
  // Methods of computation
  
 /*PROTECTED REGION ID(h_methods_ArmXcMove_orc_Mod_sens2x) ENABLED START*/

  //__Add custom methods here and delete ENABLED to regenerate it
 /*PROTECTED REGION END*/

  void init(const double pos[2], const double sensor[2]); 
  void param(); 
  void reparam(); 
  void compute(const double pos[2], const double sensor[2]); 
  void end();
};
#endif
// End of class orc_Mod_sens2x
