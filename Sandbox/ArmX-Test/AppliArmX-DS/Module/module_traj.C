//----------------------------------------------------------
// Module module_traj
//----------------------------------------------------------
//
// Orccad Version: ORCCADVERSION
// Module : module_traj.C
//
// Ports :
//   input data  : double pos[2], double posd[2]
//   output data : double posi[2]
//   PARAM  : 
//   PRECONDITION EVENT : 
//   POSTCONDTION EVENT : 
//   T1 EXCEPTION EVENT : 
//   T2 EXCEPTION EVENT : 
//   T3 EXCEPTION EVENT : 
//
// Time Methods usable in module files:
//     GetLocalTime(): return relative period time as double value
//     GetCurrentTime(): return global period time as double value 
// 
// Date of creation : 2012/08/22 18:03:29
//----------------------------------------------------------

#include "module_traj.h"
#include "Exec/prr.h"

/*PROTECTED REGION ID(C_methods_ArmXfMove_orc_Mod_traj) ENABLED START*/

  //__Add your code and delete ENABLED to regenerate it

/*PROTECTED REGION END*/


void orc_Mod_traj::init(const double pos[2], const double posd[2])
{
/*PROTECTED REGION ID(init_ArmXfMove_orc_Mod_traj) ENABLED START*/
for(index=0;index <2;index++)
    posi[index] = pos[index];
  //__Add your code and delete ENABLED to regenerate it

/*PROTECTED REGION END*/
}

void orc_Mod_traj::param()
{
   plugParam();
  
} 

void orc_Mod_traj::reparam()
{
} 

void orc_Mod_traj::compute(const double pos[2], const double posd[2])
{
/*PROTECTED REGION ID(compute_ArmXfMove_orc_Mod_traj) ENABLED START*/
for(index=0;index <2;index++){
  if (posi[index] < posd[index]){
    posi[index] += STEP_POS;
    if (posi[index] > posd[index])
      posi[index] = posd[index] ;
  }
  else{
    if (posi[index] > posd[index]){
      posi[index] -= STEP_POS; 
      if (posi[index] < posd[index])
	posi[index] = posd[index];
    }
  }
}
  //__Add your code and delete ENABLED to regenerate it

/*PROTECTED REGION END*/
//T1 events associated parameters processing

//check T1 raised parameters
}
void orc_Mod_traj::end()
{
/*PROTECTED REGION ID(end_ArmXfMove_orc_Mod_traj) ENABLED START*/

  //__Add your code and delete ENABLED to regenerate it

/*PROTECTED REGION END*/
} 
// End of class orc_Mod_traj
