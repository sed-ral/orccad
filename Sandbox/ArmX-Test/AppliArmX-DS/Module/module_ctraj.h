//----------------------------------------------------------
// Module ctraj
//
// File : module_ctraj.h
// Date of creation : 2012/08/08 10:59:37
//----------------------------------------------------------
#ifndef __orc_Mod_ctraj_h
#define __orc_Mod_ctraj_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"
#include "Exec/paramType.h"
/*PROTECTED REGION ID(h_include_ArmXfMove_orc_Mod_ctraj) ENABLED START*/
#define STEP_CPOS 0.05
//__Add include file directive here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/

//class orc_RT_ArmXfMove;
class orc_Mod_ctraj: public ModuleAlgo { 
protected: 
  // Internal Variables 
/*PROTECTED REGION ID(h_intern_ArmXfMove_orc_Mod_ctraj) ENABLED START*/
int index;
  //__Add local variable declaration here and delete ENABLED to regenerate it
/*PROTECTED REGION END*/
//	orc_RT_ArmXfMove *pRT;
public:
  orc_Mod_ctraj(ModuleTask *mt, double period) :
    ModuleAlgo((char *)"orc_Mod_ctraj", mt, period){};
  ~orc_Mod_ctraj(){};

  // Output Data Ports declaration
  double posi[2];

  // Output Event Ports
  
  //T1 Parameters declaration
  // Parameter Ports declaration			
  // Value sent by T1
  // Methods of computation
  
 /*PROTECTED REGION ID(h_methods_ArmXfMove_orc_Mod_ctraj) ENABLED START*/

  //__Add custom methods here and delete ENABLED to regenerate it
 /*PROTECTED REGION END*/

  void init(const double pos[2], const double posd[2]); 
  void param(); 
  void reparam(); 
  void compute(const double pos[2], const double posd[2]); 
  void end();
};
#endif
// End of class orc_Mod_ctraj
