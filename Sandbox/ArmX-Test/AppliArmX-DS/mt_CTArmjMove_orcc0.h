/**
 * Module execution handling according to the 
 * specified temporal constraints : CTArmjMove_orcc0
 *
 * Orccad Version: ORCCADVERSION
 * File : mt_CTArmjMove_orcc0.h
 * Date of creation : 2012/08/22 18:03:28
 */
#ifndef __mt_CTArmjMove_orcc0_h
#define __mt_CTArmjMove_orcc0_h



//ModAlgo Level = 0 depList size = 0

//ModAlgo Level = 0 depList size = 0

//ModAlgo Level = 1 depList size = 1

//ModAlgo Level = 2 depList size = 1



#include "Exec/mt.h"
#include "module_jtraj.h"
#include "module_jobs.h"
#include "module_error.h"
#include "module_command.h"
#include "PhR_WinX.h"

class RobotTask;
class orc_RT_ArmXjMove_orcc0;
class orc_Mt_CTArmjMove_orcc0;
extern void fct_CTArmjMove_orcc0(orc_Mt_CTArmjMove_orcc0 *);

class orc_Mt_CTArmjMove_orcc0: public ModuleTask
{
  
  friend void fct_CTArmjMove_orcc0(orc_Mt_CTArmjMove_orcc0 *);
    
protected:
  orc_Mod_jtraj jtraj_11;
  orc_Mod_jobs jobs_10;
  orc_Mod_error error_12;
  orc_Mod_command command_9;
  orc_PhR_WinX* WinX;
  orc_RT_ArmXjMove_orcc0 *pRT;
    
public:
  void InitTrace();

  orc_Mt_CTArmjMove_orcc0(RobotTask *rt, char *name, int itFilter, double period, SYNC_TYPE S_Type, int priority, int nb_sem_link, CONSTRAINT_TYPE C_Type)
    : ModuleTask((FUNCPTR) fct_CTArmjMove_orcc0, rt, name, itFilter, period, S_Type, priority, nb_sem_link, C_Type),
jtraj_11(this, period), jobs_10(this, period), error_12(this, period), command_9(this, period)  {
    InitTrace();
    WinX = (orc_PhR_WinX *) RT->GetPhR();
    pRT = (orc_RT_ArmXjMove_orcc0 *)RT;
  }
  ~orc_Mt_CTArmjMove_orcc0() {};
  
  void Param();
  void ReParam();
  int SetT1Exceptions();
  orc_Mod_jtraj &getjtraj_11() {return jtraj_11;};
  orc_Mod_jobs &getjobs_10() {return jobs_10;};
  orc_Mod_error &geterror_12() {return error_12;};
  orc_Mod_command &getcommand_9() {return command_9;};
};
#endif
// End of class orc_Mt_CTArmjMove_orcc0
