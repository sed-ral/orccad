/**
 * Module execution handling according to the 
 * specified temporal constraints : TC2
 *
 * Orccad Version: ORCCADVERSION
 * File : mt_TC2.h
 * Date of creation : 2012/08/22 17:03:45
 */
#ifndef __mt_TC2_h
#define __mt_TC2_h



//ModAlgo Level = 2 depList size = 1



#include "Exec/mt.h"
#include "module_command.h"
#include "PhR_WinX.h"

class RobotTask;
class orc_RT_ArmXjMove;
class orc_Mt_TC2;
extern void fct_TC2(orc_Mt_TC2 *);

class orc_Mt_TC2: public ModuleTask
{
  
  friend void fct_TC2(orc_Mt_TC2 *);
    
protected:
  orc_Mod_command command_9;
  orc_PhR_WinX* WinX;
  orc_RT_ArmXjMove *pRT;
    
public:
  void InitTrace();

  orc_Mt_TC2(RobotTask *rt, char *name, int itFilter, double period, SYNC_TYPE S_Type, int priority, int nb_sem_link, CONSTRAINT_TYPE C_Type)
    : ModuleTask((FUNCPTR) fct_TC2, rt, name, itFilter, period, S_Type, priority, nb_sem_link, C_Type),
command_9(this, period)  {
    InitTrace();
    WinX = (orc_PhR_WinX *) RT->GetPhR();
    pRT = (orc_RT_ArmXjMove *)RT;
  }
  ~orc_Mt_TC2() {};
  
  void Param();
  void ReParam();
  int SetT1Exceptions();
  orc_Mod_command &getcommand_9() {return command_9;};
};
#endif
// End of class orc_Mt_TC2
