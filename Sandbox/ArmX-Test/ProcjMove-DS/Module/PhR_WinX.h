//----------------------------------------------------------
// Physical Resource WinX
//
// File : PhR_WinX.h
// Date of creation : 2012/08/22 17:03:45
//----------------------------------------------------------

#ifndef __phr_WinX_h
#define __phr_WinX_h

#include "Exec/phr.h"
/*PROTECTED REGION ID(h_include_ArmXjMove_orc_PhR_WinX) ENABLED START*/
#include "win.h"
//__Add include file directive here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/
class orc_PhR_WinX : public PhR {
private:
  // Internal Variables 
/*PROTECTED REGION ID(h_intern_ArmXjMove_orc_PhR_WinX) ENABLED START*/

  //__Add local variable declaration here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/

public: 
	//constructor and destructor
  orc_PhR_WinX(ProcedureRobot *, char *,int , int ,int ,int );
  ~orc_PhR_WinX();
  // Output Ports declaration
  int key;
  double pos[2];
  double sensor[2];
  int limit[2];
  
  // Event Ports declaration
  
  // Methods to call functions driver
  void PUT_torque(double torque[2]);
  void GET_key();
  void GET_pos();
  void GET_sensor();
  void GET_limit();

  // Methods to control the driver mode
  void reinit(); 
  void param(); 
  int Init(); 
  int Close();
   /*PROTECTED REGION ID(h_methods_ArmXjMove_orc_PhR_WinX) ENABLED START*/

//__Add include file directive here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/
};
#endif 
// End of Physical Resource WinX
