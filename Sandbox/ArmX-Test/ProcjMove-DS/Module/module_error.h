//----------------------------------------------------------
// Module error
//
// File : module_error.h
// Date of creation : 2012/08/22 17:03:44
//----------------------------------------------------------
#ifndef __orc_Mod_error_h
#define __orc_Mod_error_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"
#include "Exec/paramType.h"
/*PROTECTED REGION ID(h_include_ArmXjMove_orc_Mod_error) ENABLED START*/
#define TRACKERROR 2.0
//__Add include file directive here and delete ENABLED to regenerate it

/*PROTECTED REGION END*/

class orc_Mod_error: public ModuleAlgo { 
protected: 
  // Internal Variables 
/*PROTECTED REGION ID(h_intern_ArmXjMove_orc_Mod_error) ENABLED START*/
int index;
int flagtrack;
  //__Add local variable declaration here and delete ENABLED to regenerate it
/*PROTECTED REGION END*/

public:
  orc_Mod_error(ModuleTask *mt, double period) :
    ModuleAlgo((char *)"orc_Mod_error", mt, period){};
  ~orc_Mod_error(){};

  // Output Data Ports declaration
  double errpos[2];

  // Output Event Ports
  int errtrack;
  
  //T1 Parameters declaration
  // Parameter Ports declaration			
  // Value sent by T1
  // Methods of computation
  
 /*PROTECTED REGION ID(h_methods_ArmXjMove_orc_Mod_error) ENABLED START*/

  //__Add custom methods here and delete ENABLED to regenerate it
 /*PROTECTED REGION END*/

  void init(const double posc[2], const double posd[2]); 
  void param(); 
  void reparam(); 
  void compute(const double posc[2], const double posd[2]); 
  void end();
};
#endif
// End of class orc_Mod_error
