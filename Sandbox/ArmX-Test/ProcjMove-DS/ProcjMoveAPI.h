/**
 * This file defines the API functions for Robot Procedure ProcjMove
 * to connect with the generated Orccad program
 * File automatically generated
 *
 * File : ProcjMoveAPI.h
 * Date of creation : 2012/08/22 17:03:45
 */
#ifndef __orc_ProcjMoveApi_h
#define __orc_ProcjMoveApi_h

// C function Interfaces
extern "C" {
  int orcProcjMoveInit();
  int orcProcjMoveLaunch();
  int orcProcjMoveTaskLaunch();
  int orcProcjMoveGo();
  int orcProcjMoveStop();
  int orcArmXjMovePrm(double posd[2]);
}// extern "C"

#endif
// End file ProcjMoveAPI.h
