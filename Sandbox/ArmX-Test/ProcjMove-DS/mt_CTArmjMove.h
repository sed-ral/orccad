/**
 * Module execution handling according to the 
 * specified temporal constraints : CTArmjMove
 *
 * Orccad Version: ORCCADVERSION
 * File : mt_CTArmjMove.h
 * Date of creation : 2012/08/22 17:03:45
 */
#ifndef __mt_CTArmjMove_h
#define __mt_CTArmjMove_h



//ModAlgo Level = 0 depList size = 0

//ModAlgo Level = 0 depList size = 0

//ModAlgo Level = 1 depList size = 1



#include "Exec/mt.h"
#include "module_jtraj.h"
#include "module_jobs.h"
#include "module_error.h"
#include "PhR_WinX.h"

class RobotTask;
class orc_RT_ArmXjMove;
class orc_Mt_CTArmjMove;
extern void fct_CTArmjMove(orc_Mt_CTArmjMove *);

class orc_Mt_CTArmjMove: public ModuleTask
{
  
  friend void fct_CTArmjMove(orc_Mt_CTArmjMove *);
    
protected:
  orc_Mod_jtraj jtraj_11;
  orc_Mod_jobs jobs_10;
  orc_Mod_error error_12;
  orc_PhR_WinX* WinX;
  orc_RT_ArmXjMove *pRT;
    
public:
  void InitTrace();

  orc_Mt_CTArmjMove(RobotTask *rt, char *name, int itFilter, double period, SYNC_TYPE S_Type, int priority, int nb_sem_link, CONSTRAINT_TYPE C_Type)
    : ModuleTask((FUNCPTR) fct_CTArmjMove, rt, name, itFilter, period, S_Type, priority, nb_sem_link, C_Type),
jtraj_11(this, period), jobs_10(this, period), error_12(this, period)  {
    InitTrace();
    WinX = (orc_PhR_WinX *) RT->GetPhR();
    pRT = (orc_RT_ArmXjMove *)RT;
  }
  ~orc_Mt_CTArmjMove() {};
  
  void Param();
  void ReParam();
  int SetT1Exceptions();
  orc_Mod_jtraj &getjtraj_11() {return jtraj_11;};
  orc_Mod_jobs &getjobs_10() {return jobs_10;};
  orc_Mod_error &geterror_12() {return error_12;};
};
#endif
// End of class orc_Mt_CTArmjMove
