/**
 * Required by C code generator constructed by Esterel 
 * 
 * File : aa_ProcjMove.h
 * Date of creation : 2012/08/22 17:03:44
 */
/* Hold ORCCAD user type functions */
typedef void* orcStrlPtr;
#define _orcStrlPtr(x,y) (*(x) = y)
int _eq_orcStrlPtr(orcStrlPtr x, orcStrlPtr y)
{ return (x == y); }
int _ne_orcStrlPtr(orcStrlPtr x, orcStrlPtr y)
{ return (x != y); }
