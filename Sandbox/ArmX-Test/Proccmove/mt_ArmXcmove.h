#ifndef __mt_ArmXcmove_h
#define __mt_ArmXcmove_h

#include "Exec/mt.h"

#include "module_command.h"
#include "module_error.h"
#include "module_sens2x.h"
#include "module_invcart.h"
#include "module_ctraj.h"
#include "module_cobs.h"
#include "PhR_WinX.h"
#include "module_csecure.h"

class RobotTask; 
class orc_Mt_ArmXcmove;
extern void fct_ArmXcmove(orc_Mt_ArmXcmove*);

class orc_Mt_ArmXcmove: public ModuleTask
{
	 friend void fct_ArmXcmove(orc_Mt_ArmXcmove*);
protected:
	 orc_Mod_command command_3;
	 orc_Mod_error error_4;
	 orc_Mod_sens2x sens2x_6;
	 orc_Mod_invcart invcart_10;
	 orc_Mod_ctraj ctraj_11;
	 orc_Mod_cobs cobs_13;
	 orc_PhR_WinX* WinX;
public: 
	 orc_Mod_csecure csecure_12;
	 void InitTrace();
public:

 orc_Mt_ArmXcmove(RobotTask *rt, char *name, int itFilter, double period
, SYNC_TYPE S_Type, int priority, int nb_sem_link, CONSTRAINT_TYPE C_Type)
    : ModuleTask((FUNCPTR) fct_ArmXcmove, rt, name, itFilter, period, S_Type, priority, nb_sem_link, C_Type),
	 command_3(this,0)
	 ,error_4(this,0)
	 ,sens2x_6(this,0)
	 ,invcart_10(this,0)
	 ,ctraj_11(this,0)
	 ,cobs_13(this,0)
	 ,csecure_12()
	 {
	    InitTrace();

	    WinX = (orc_PhR_WinX*) RT->GetPhR();
	 }
	 ~orc_Mt_ArmXcmove(){};
};
#endif
