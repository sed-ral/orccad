//----------------------------------------------------------
// Module invcart
//----------------------------------------------------------
#ifndef __orc_Mod_invcart_h
#define __orc_Mod_invcart_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"

#include "module_invcart_Inc.h"

class orc_Mod_invcart: public ModuleAlgo { 
protected: 
	// Internal Variables 
// Orccad Version: 3.0 alpha
// Module : invcart
// Variables declaration File
// Date of creation : Wed Mar 17 14:19:49 1999

double k1,k2;

public: 
	orc_Mod_invcart(ModuleTask *mt,double period):
	  ModuleAlgo("orc_Mod_invcart",mt,period) {};
	~orc_Mod_invcart() {};
	 // Output Ports declaration
	 double jointd[2];
	 // Output Event Ports declaration
	 int reconf;
	 // Output param Ports declaration

	 // Methods of computation 
	 void init(const double xd[2]); 
	 void param(); 
	 void reparam(); 
	 void compute(const double xd[2] ); 
	 void end(); 
 }; 
#endif 
// End class  orc_Mod_invcart

