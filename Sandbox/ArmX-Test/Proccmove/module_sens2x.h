//----------------------------------------------------------
// Module sens2x
//----------------------------------------------------------
#ifndef __orc_Mod_sens2x_h
#define __orc_Mod_sens2x_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"

#include "module_sens2x_Inc.h"

class orc_Mod_sens2x: public ModuleAlgo { 
protected: 
	// Internal Variables 
// Orccad Version: 3.0 alpha
// Module : sens2x
// Variables declaration File
// Date of creation : Wed Mar 17 14:44:30 1999

double k1,k2;

double s12,c12;

int top;

public: 
	orc_Mod_sens2x(ModuleTask *mt,double period):
	  ModuleAlgo("orc_Mod_sens2x",mt,period) {};
	~orc_Mod_sens2x() {};
	 // Output Ports declaration
	 double x[2];
	 // Output Event Ports declaration
	 int outwork;
	 // Output param Ports declaration

	 // Methods of computation 
	 void init(const double sensor[2], const double pos[2]); 
	 void param(); 
	 void reparam(); 
	 void compute(const double sensor[2], const double pos[2] ); 
	 void end(); 
 }; 
#endif 
// End class  orc_Mod_sens2x

