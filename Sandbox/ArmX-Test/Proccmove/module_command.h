//----------------------------------------------------------
// Module command
//----------------------------------------------------------
#ifndef __orc_Mod_command_h
#define __orc_Mod_command_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"

#include "module_command_Inc.h"

class orc_Mod_command: public ModuleAlgo { 
protected: 
	// Internal Variables 
// Orccad Version: 3.0 alpha
// Module : command
// Variables declaration File
// Date of creation : Tue Mar 16 19:12:49 1999

int index;



public: 
	orc_Mod_command(ModuleTask *mt,double period):
	  ModuleAlgo("orc_Mod_command",mt,period) {};
	~orc_Mod_command() {};
	 // Output Ports declaration
	 double torque[2];
	 // Output Event Ports declaration
	 // Output param Ports declaration

	 // Methods of computation 
	 void init(const double pose[2], const double pos[2]); 
	 void param(); 
	 void reparam(); 
	 void compute(const double pose[2], const double pos[2] ); 
	 void end(); 
 }; 
#endif 
// End class  orc_Mod_command

