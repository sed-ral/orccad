#ifndef __mt_ArmXjmove_h
#define __mt_ArmXjmove_h

#include "Exec/mt.h"

#include "module_error.h"
#include "module_jtraj.h"
#include "module_command.h"
#include "module_jobs.h"
#include "PhR_WinX.h"
#include "module_jsecure.h"

class RobotTask; 
class orc_Mt_ArmXjmove;
extern void fct_ArmXjmove(orc_Mt_ArmXjmove*);

class orc_Mt_ArmXjmove: public ModuleTask
{
	 friend void fct_ArmXjmove(orc_Mt_ArmXjmove*);
protected:
	 orc_Mod_error error_5;
	 orc_Mod_jtraj jtraj_8;
	 orc_Mod_command command_9;
	 orc_Mod_jobs jobs_11;
	 orc_PhR_WinX* WinX;
public: 
	 orc_Mod_jsecure jsecure_10;
	 void InitTrace();
public:


orc_Mt_ArmXjmove(RobotTask *rt, char *name, int itFilter, double period
, SYNC_TYPE S_Type, int priority, int nb_sem_link, CONSTRAINT_TYPE C_Type)
    : ModuleTask((FUNCPTR) fct_ArmXjmove, rt, name, itFilter, period, S_Type, priority, nb_sem_link, C_Type),

	 error_5(this,0)
	 ,jtraj_8(this,0)
	 ,command_9(this,0)
	 ,jobs_11(this,0)
	 ,jsecure_10()
	 {
	    InitTrace();

	    WinX = (orc_PhR_WinX*) RT->GetPhR();
	 }
	 ~orc_Mt_ArmXjmove(){};
};
#endif
