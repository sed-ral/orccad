//----------------------------------------------------------
// Module cobs
//----------------------------------------------------------

#include "module_cobs.h"
#include "Exec/prr.h"


void orc_Mod_cobs::init(const int limit[2], const int &key)
{
// Orccad Version: 3.0 alpha
// Module : jobs
// Initialisation File
// Date of creation : Mon Nov  3 13:23:55 1997

outbound   = NO_EVENT;
redbut     = NO_EVENT;

} 
void orc_Mod_cobs::param()
{
	 plugParam();



} 
void orc_Mod_cobs::reparam()
{

} 
void orc_Mod_cobs::compute(const int limit[2], const int &key )
{
// Orccad Version: 3.0 alpha   
// Module : jobs
// Computation File 
// Date of creation : Mon Nov  3 13:23:55 1997

if ( ((key == 'q')||(key == 'Q'))&& redbut==NO_EVENT){
    redbut = SET_EVENT;
    fprintf(stdout, "WinXmove::Emergency T3 Event\n");
}

if ( ((limit[0]==TRUE)||(limit[1]==TRUE)) && outbound == NO_EVENT){
    outbound = SET_EVENT;
    fprintf(stdout, "WinXmove::Joint Limit T3 Event\n");
}


} 
void orc_Mod_cobs::end()
{
// Orccad Version: 3.0 alpha
// Module :  jobs
// End File 
// Date of creation : Mon Nov  3 13:23:55 1997


} 
// End class orc_Mod_cobs

