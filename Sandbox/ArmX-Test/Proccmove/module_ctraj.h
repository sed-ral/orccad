//----------------------------------------------------------
// Module ctraj
//----------------------------------------------------------
#ifndef __orc_Mod_ctraj_h
#define __orc_Mod_ctraj_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"

#include "module_ctraj_Inc.h"

class orc_Mod_ctraj: public ModuleAlgo { 
protected: 
	// Internal Variables 
// Orccad Version: 3.0 alpha
// Module : ctraj
// Variables declaration File
// Date of creation : Wed Mar 17 14:52:13 1999

int index;


public: 
	orc_Mod_ctraj(ModuleTask *mt,double period):
	  ModuleAlgo("orc_Mod_ctraj",mt,period) {};
	~orc_Mod_ctraj() {};
	 // Output Ports declaration
	 double posi[2];
	 // Output Event Ports declaration
	 // Output param Ports declaration

	 // Methods of computation 
	 void init(const double posd[2], const double pos[2]); 
	 void param(); 
	 void reparam(); 
	 void compute(const double posd[2], const double pos[2] ); 
	 void end(); 
 }; 
#endif 
// End class  orc_Mod_ctraj

