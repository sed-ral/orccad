/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see Famille.FamilleFactory
 * @model kind="package"
 * @generated
 */
public interface FamillePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Famille";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://famille";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "Famille";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FamillePackage eINSTANCE = Famille.impl.FamillePackageImpl.init();

	/**
	 * The meta object id for the '{@link Famille.impl.MaisonImpl <em>Maison</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Famille.impl.MaisonImpl
	 * @see Famille.impl.FamillePackageImpl#getMaison()
	 * @generated
	 */
	int MAISON = 0;

	/**
	 * The feature id for the '<em><b>Chambres</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAISON__CHAMBRES = 0;

	/**
	 * The feature id for the '<em><b>Cuisine</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAISON__CUISINE = 1;

	/**
	 * The feature id for the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAISON__NOM = 2;

	/**
	 * The number of structural features of the '<em>Maison</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAISON_FEATURE_COUNT = 3;

	/**
	 * The meta object id for the '{@link Famille.impl.PieceImpl <em>Piece</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Famille.impl.PieceImpl
	 * @see Famille.impl.FamillePackageImpl#getPiece()
	 * @generated
	 */
	int PIECE = 3;

	/**
	 * The number of structural features of the '<em>Piece</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PIECE_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link Famille.impl.CuisineImpl <em>Cuisine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Famille.impl.CuisineImpl
	 * @see Famille.impl.FamillePackageImpl#getCuisine()
	 * @generated
	 */
	int CUISINE = 1;

	/**
	 * The number of structural features of the '<em>Cuisine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CUISINE_FEATURE_COUNT = PIECE_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link Famille.impl.ChambreImpl <em>Chambre</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Famille.impl.ChambreImpl
	 * @see Famille.impl.FamillePackageImpl#getChambre()
	 * @generated
	 */
	int CHAMBRE = 2;

	/**
	 * The number of structural features of the '<em>Chambre</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CHAMBRE_FEATURE_COUNT = PIECE_FEATURE_COUNT + 0;


	/**
	 * The meta object id for the '{@link Famille.impl.SousFamilleImpl <em>Sous Famille</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Famille.impl.SousFamilleImpl
	 * @see Famille.impl.FamillePackageImpl#getSousFamille()
	 * @generated
	 */
	int SOUS_FAMILLE = 4;

	/**
	 * The feature id for the '<em><b>Maisons</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOUS_FAMILLE__MAISONS = 0;

	/**
	 * The feature id for the '<em><b>Totos</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOUS_FAMILLE__TOTOS = 1;

	/**
	 * The number of structural features of the '<em>Sous Famille</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOUS_FAMILLE_FEATURE_COUNT = 2;

	/**
	 * The meta object id for the '{@link Famille.impl.TotoImpl <em>Toto</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Famille.impl.TotoImpl
	 * @see Famille.impl.FamillePackageImpl#getToto()
	 * @generated
	 */
	int TOTO = 5;

	/**
	 * The feature id for the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOTO__FROM = 0;

	/**
	 * The feature id for the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOTO__TO = 1;

	/**
	 * The number of structural features of the '<em>Toto</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TOTO_FEATURE_COUNT = 2;


	/**
	 * Returns the meta object for class '{@link Famille.Maison <em>Maison</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Maison</em>'.
	 * @see Famille.Maison
	 * @generated
	 */
	EClass getMaison();

	/**
	 * Returns the meta object for the containment reference list '{@link Famille.Maison#getChambres <em>Chambres</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Chambres</em>'.
	 * @see Famille.Maison#getChambres()
	 * @see #getMaison()
	 * @generated
	 */
	EReference getMaison_Chambres();

	/**
	 * Returns the meta object for the containment reference '{@link Famille.Maison#getCuisine <em>Cuisine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cuisine</em>'.
	 * @see Famille.Maison#getCuisine()
	 * @see #getMaison()
	 * @generated
	 */
	EReference getMaison_Cuisine();

	/**
	 * Returns the meta object for the attribute '{@link Famille.Maison#getNom <em>Nom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nom</em>'.
	 * @see Famille.Maison#getNom()
	 * @see #getMaison()
	 * @generated
	 */
	EAttribute getMaison_Nom();

	/**
	 * Returns the meta object for class '{@link Famille.Cuisine <em>Cuisine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cuisine</em>'.
	 * @see Famille.Cuisine
	 * @generated
	 */
	EClass getCuisine();

	/**
	 * Returns the meta object for class '{@link Famille.Chambre <em>Chambre</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Chambre</em>'.
	 * @see Famille.Chambre
	 * @generated
	 */
	EClass getChambre();

	/**
	 * Returns the meta object for class '{@link Famille.Piece <em>Piece</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Piece</em>'.
	 * @see Famille.Piece
	 * @generated
	 */
	EClass getPiece();

	/**
	 * Returns the meta object for class '{@link Famille.SousFamille <em>Sous Famille</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sous Famille</em>'.
	 * @see Famille.SousFamille
	 * @generated
	 */
	EClass getSousFamille();

	/**
	 * Returns the meta object for the containment reference list '{@link Famille.SousFamille#getMaisons <em>Maisons</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Maisons</em>'.
	 * @see Famille.SousFamille#getMaisons()
	 * @see #getSousFamille()
	 * @generated
	 */
	EReference getSousFamille_Maisons();

	/**
	 * Returns the meta object for the containment reference list '{@link Famille.SousFamille#getTotos <em>Totos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Totos</em>'.
	 * @see Famille.SousFamille#getTotos()
	 * @see #getSousFamille()
	 * @generated
	 */
	EReference getSousFamille_Totos();

	/**
	 * Returns the meta object for class '{@link Famille.Toto <em>Toto</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Toto</em>'.
	 * @see Famille.Toto
	 * @generated
	 */
	EClass getToto();

	/**
	 * Returns the meta object for the reference '{@link Famille.Toto#getFrom <em>From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From</em>'.
	 * @see Famille.Toto#getFrom()
	 * @see #getToto()
	 * @generated
	 */
	EReference getToto_From();

	/**
	 * Returns the meta object for the reference '{@link Famille.Toto#getTo <em>To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To</em>'.
	 * @see Famille.Toto#getTo()
	 * @see #getToto()
	 * @generated
	 */
	EReference getToto_To();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	FamilleFactory getFamilleFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link Famille.impl.MaisonImpl <em>Maison</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Famille.impl.MaisonImpl
		 * @see Famille.impl.FamillePackageImpl#getMaison()
		 * @generated
		 */
		EClass MAISON = eINSTANCE.getMaison();

		/**
		 * The meta object literal for the '<em><b>Chambres</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAISON__CHAMBRES = eINSTANCE.getMaison_Chambres();

		/**
		 * The meta object literal for the '<em><b>Cuisine</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MAISON__CUISINE = eINSTANCE.getMaison_Cuisine();

		/**
		 * The meta object literal for the '<em><b>Nom</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MAISON__NOM = eINSTANCE.getMaison_Nom();

		/**
		 * The meta object literal for the '{@link Famille.impl.CuisineImpl <em>Cuisine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Famille.impl.CuisineImpl
		 * @see Famille.impl.FamillePackageImpl#getCuisine()
		 * @generated
		 */
		EClass CUISINE = eINSTANCE.getCuisine();

		/**
		 * The meta object literal for the '{@link Famille.impl.ChambreImpl <em>Chambre</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Famille.impl.ChambreImpl
		 * @see Famille.impl.FamillePackageImpl#getChambre()
		 * @generated
		 */
		EClass CHAMBRE = eINSTANCE.getChambre();

		/**
		 * The meta object literal for the '{@link Famille.impl.PieceImpl <em>Piece</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Famille.impl.PieceImpl
		 * @see Famille.impl.FamillePackageImpl#getPiece()
		 * @generated
		 */
		EClass PIECE = eINSTANCE.getPiece();

		/**
		 * The meta object literal for the '{@link Famille.impl.SousFamilleImpl <em>Sous Famille</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Famille.impl.SousFamilleImpl
		 * @see Famille.impl.FamillePackageImpl#getSousFamille()
		 * @generated
		 */
		EClass SOUS_FAMILLE = eINSTANCE.getSousFamille();

		/**
		 * The meta object literal for the '<em><b>Maisons</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOUS_FAMILLE__MAISONS = eINSTANCE.getSousFamille_Maisons();

		/**
		 * The meta object literal for the '<em><b>Totos</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOUS_FAMILLE__TOTOS = eINSTANCE.getSousFamille_Totos();

		/**
		 * The meta object literal for the '{@link Famille.impl.TotoImpl <em>Toto</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see Famille.impl.TotoImpl
		 * @see Famille.impl.FamillePackageImpl#getToto()
		 * @generated
		 */
		EClass TOTO = eINSTANCE.getToto();

		/**
		 * The meta object literal for the '<em><b>From</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOTO__FROM = eINSTANCE.getToto_From();

		/**
		 * The meta object literal for the '<em><b>To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TOTO__TO = eINSTANCE.getToto_To();

	}

} //FamillePackage
