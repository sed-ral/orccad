/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Piece</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Famille.FamillePackage#getPiece()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='Toto2009'"
 * @generated
 */
public interface Piece extends EObject {
} // Piece
