/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sous Famille</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Famille.SousFamille#getMaisons <em>Maisons</em>}</li>
 *   <li>{@link Famille.SousFamille#getTotos <em>Totos</em>}</li>
 * </ul>
 * </p>
 *
 * @see Famille.FamillePackage#getSousFamille()
 * @model
 * @generated
 */
public interface SousFamille extends EObject {
	/**
	 * Returns the value of the '<em><b>Maisons</b></em>' containment reference list.
	 * The list contents are of type {@link Famille.Maison}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Maisons</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Maisons</em>' containment reference list.
	 * @see Famille.FamillePackage#getSousFamille_Maisons()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Maison> getMaisons();

	/**
	 * Returns the value of the '<em><b>Totos</b></em>' containment reference list.
	 * The list contents are of type {@link Famille.Toto}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Totos</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Totos</em>' containment reference list.
	 * @see Famille.FamillePackage#getSousFamille_Totos()
	 * @model containment="true"
	 * @generated
	 */
	EList<Toto> getTotos();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean isThereALoop(DiagnosticChain loop1, Map<?, ?> loop2);

} // SousFamille
