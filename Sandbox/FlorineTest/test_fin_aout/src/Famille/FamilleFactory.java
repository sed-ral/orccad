/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille;

import org.eclipse.emf.ecore.EFactory;
import Famille.Toto;
/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see Famille.FamillePackage
 * @generated
 */
public interface FamilleFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	FamilleFactory eINSTANCE = Famille.impl.FamilleFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Maison</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Maison</em>'.
	 * @generated
	 */
	Maison createMaison();

	/**
	 * Returns a new object of class '<em>Cuisine</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cuisine</em>'.
	 * @generated
	 */
	Cuisine createCuisine();

	/**
	 * Returns a new object of class '<em>Chambre</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Chambre</em>'.
	 * @generated
	 */
	Chambre createChambre();

	/**
	 * Returns a new object of class '<em>Piece</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Piece</em>'.
	 * @generated
	 */
	Piece createPiece();

	/**
	 * Returns a new object of class '<em>Sous Famille</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Sous Famille</em>'.
	 * @generated
	 */
	SousFamille createSousFamille();

	/**
	 * Returns a new object of class '<em>Toto</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Toto</em>'.
	 * @generated
	 */
	Toto createToto();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	FamillePackage getFamillePackage();

} //FamilleFactory
