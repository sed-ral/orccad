/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille;

import java.util.Map;

import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Maison</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Famille.Maison#getChambres <em>Chambres</em>}</li>
 *   <li>{@link Famille.Maison#getCuisine <em>Cuisine</em>}</li>
 *   <li>{@link Famille.Maison#getNom <em>Nom</em>}</li>
 * </ul>
 * </p>
 *
 * @see Famille.FamillePackage#getMaison()
 * @model
 * @generated
 */
public interface Maison extends EObject {
	/**
	 * Returns the value of the '<em><b>Chambres</b></em>' containment reference list.
	 * The list contents are of type {@link Famille.Chambre}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chambres</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chambres</em>' containment reference list.
	 * @see Famille.FamillePackage#getMaison_Chambres()
	 * @model containment="true"
	 * @generated
	 */
	EList<Chambre> getChambres();

	/**
	 * Returns the value of the '<em><b>Cuisine</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cuisine</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cuisine</em>' containment reference.
	 * @see #setCuisine(Cuisine)
	 * @see Famille.FamillePackage#getMaison_Cuisine()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Cuisine getCuisine();

	/**
	 * Sets the value of the '{@link Famille.Maison#getCuisine <em>Cuisine</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cuisine</em>' containment reference.
	 * @see #getCuisine()
	 * @generated
	 */
	void setCuisine(Cuisine value);

	/**
	 * Returns the value of the '<em><b>Nom</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nom</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nom</em>' attribute.
	 * @see #setNom(String)
	 * @see Famille.FamillePackage#getMaison_Nom()
	 * @model
	 * @generated
	 */
	String getNom();

	/**
	 * Sets the value of the '{@link Famille.Maison#getNom <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nom</em>' attribute.
	 * @see #getNom()
	 * @generated
	 */
	void setNom(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean MaisonHasPiece(DiagnosticChain param1, Map<?, ?> param2);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean MaisonHasName(DiagnosticChain param3, Map<?, ?> param4);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean MaisonHasNameUnique(DiagnosticChain param5, Map<?, ?> param6);

} // Maison
