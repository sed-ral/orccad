/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Chambre</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Famille.FamillePackage#getChambre()
 * @model
 * @generated
 */
public interface Chambre extends Piece {
} // Chambre
