/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille;

import java.util.Map;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Toto</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link Famille.Toto#getFrom <em>From</em>}</li>
 *   <li>{@link Famille.Toto#getTo <em>To</em>}</li>
 * </ul>
 * </p>
 *
 * @see Famille.FamillePackage#getToto()
 * @model
 * @generated
 */
public interface Toto extends EObject {

	/**
	 * Returns the value of the '<em><b>From</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From</em>' reference.
	 * @see #setFrom(Maison)
	 * @see Famille.FamillePackage#getToto_From()
	 * @model
	 * @generated
	 */
	Maison getFrom();

	/**
	 * Sets the value of the '{@link Famille.Toto#getFrom <em>From</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From</em>' reference.
	 * @see #getFrom()
	 * @generated
	 */
	void setFrom(Maison value);

	/**
	 * Returns the value of the '<em><b>To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To</em>' reference.
	 * @see #setTo(Maison)
	 * @see Famille.FamillePackage#getToto_To()
	 * @model
	 * @generated
	 */
	Maison getTo();

	/**
	 * Sets the value of the '{@link Famille.Toto#getTo <em>To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To</em>' reference.
	 * @see #getTo()
	 * @generated
	 */
	void setTo(Maison value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	boolean NoLoop(DiagnosticChain param5, Map<?, ?> param6);
} // Toto
