/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cuisine</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see Famille.FamillePackage#getCuisine()
 * @model
 * @generated
 */
public interface Cuisine extends Piece {
} // Cuisine
