/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille.util;

import Famille.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see Famille.FamillePackage
 * @generated
 */
public class FamilleValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final FamilleValidator INSTANCE = new FamilleValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "Famille";

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Maison Has Piece' of 'Maison'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int MAISON__MAISON_HAS_PIECE = 1;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Maison Has Name' of 'Maison'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int MAISON__MAISON_HAS_NAME = 2;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Maison Has Name Unique' of 'Maison'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int MAISON__MAISON_HAS_NAME_UNIQUE = 3;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'Is There ALoop' of 'Sous Famille'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int SOUS_FAMILLE__IS_THERE_ALOOP = 4;

	/**
	 * The {@link org.eclipse.emf.common.util.Diagnostic#getCode() code} for constraint 'No Loop' of 'Toto'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final int TOTO__NO_LOOP = 5;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 5;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FamilleValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return FamillePackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case FamillePackage.MAISON:
				return validateMaison((Maison)value, diagnostics, context);
			case FamillePackage.CUISINE:
				return validateCuisine((Cuisine)value, diagnostics, context);
			case FamillePackage.CHAMBRE:
				return validateChambre((Chambre)value, diagnostics, context);
			case FamillePackage.PIECE:
				return validatePiece((Piece)value, diagnostics, context);
			case FamillePackage.SOUS_FAMILLE:
				return validateSousFamille((SousFamille)value, diagnostics, context);
			case FamillePackage.TOTO:
				return validateToto((Toto)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaison(Maison maison, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(maison, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(maison, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(maison, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(maison, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(maison, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(maison, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(maison, diagnostics, context);
		if (result || diagnostics != null) result &= validateMaison_MaisonHasPiece(maison, diagnostics, context);
		if (result || diagnostics != null) result &= validateMaison_MaisonHasName(maison, diagnostics, context);
		if (result || diagnostics != null) result &= validateMaison_MaisonHasNameUnique(maison, diagnostics, context);
		return result;
	}

	/**
	 * Validates the MaisonHasPiece constraint of '<em>Maison</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaison_MaisonHasPiece(Maison maison, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return maison.MaisonHasPiece(diagnostics, context);
	}

	/**
	 * Validates the MaisonHasName constraint of '<em>Maison</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaison_MaisonHasName(Maison maison, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return maison.MaisonHasName(diagnostics, context);
	}

	/**
	 * Validates the MaisonHasNameUnique constraint of '<em>Maison</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMaison_MaisonHasNameUnique(Maison maison, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return maison.MaisonHasNameUnique(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCuisine(Cuisine cuisine, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(cuisine, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(cuisine, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(cuisine, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(cuisine, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(cuisine, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(cuisine, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(cuisine, diagnostics, context);
		if (result || diagnostics != null) result &= validatePiece_Toto2009(cuisine, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateChambre(Chambre chambre, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(chambre, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(chambre, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(chambre, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(chambre, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(chambre, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(chambre, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(chambre, diagnostics, context);
		if (result || diagnostics != null) result &= validatePiece_Toto2009(chambre, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePiece(Piece piece, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(piece, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(piece, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(piece, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(piece, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(piece, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(piece, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(piece, diagnostics, context);
		if (result || diagnostics != null) result &= validatePiece_Toto2009(piece, diagnostics, context);
		return result;
	}

	/**
	 * Validates the Toto2009 constraint of '<em>Piece</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public boolean validatePiece_Toto2009(Piece piece, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		

		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "Toto2009", getObjectLabel(piece, context) },
						 new Object[] { piece },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSousFamille(SousFamille sousFamille, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(sousFamille, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(sousFamille, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(sousFamille, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(sousFamille, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(sousFamille, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(sousFamille, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(sousFamille, diagnostics, context);
		if (result || diagnostics != null) result &= validateSousFamille_isThereALoop(sousFamille, diagnostics, context);
		return result;
	}

	/**
	 * Validates the isThereALoop constraint of '<em>Sous Famille</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSousFamille_isThereALoop(SousFamille sousFamille, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return sousFamille.isThereALoop(diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateToto(Toto toto, DiagnosticChain diagnostics, Map<Object, Object> context) {
		boolean result = validate_EveryMultiplicityConforms(toto, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms(toto, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained(toto, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves(toto, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID(toto, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique(toto, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique(toto, diagnostics, context);
		if (result || diagnostics != null) result &= validateToto_NoLoop(toto, diagnostics, context);
		return result;
	}

	/**
	 * Validates the NoLoop constraint of '<em>Toto</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateToto_NoLoop(Toto toto, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return toto.NoLoop(diagnostics, context);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //FamilleValidator
