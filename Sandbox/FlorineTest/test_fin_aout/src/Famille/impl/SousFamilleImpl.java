/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille.impl;

import Famille.FamillePackage;
import Famille.Lien;
import Famille.Maison;
import Famille.SousFamille;

import Famille.Toto;
import Famille.util.FamilleValidator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import java.util.Map;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sous Famille</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link Famille.impl.SousFamilleImpl#getMaisons <em>Maisons</em>}</li>
 *   <li>{@link Famille.impl.SousFamilleImpl#getTotos <em>Totos</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class SousFamilleImpl extends EObjectImpl implements SousFamille {
	/**
	 * The cached value of the '{@link #getMaisons() <em>Maisons</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaisons()
	 * @generated
	 * @ordered
	 */
	protected EList<Maison> maisons;

	/**
	 * The cached value of the '{@link #getTotos() <em>Totos</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotos()
	 * @generated
	 * @ordered
	 */
	protected EList<Toto> totos;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SousFamilleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FamillePackage.Literals.SOUS_FAMILLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Maison> getMaisons() {
		if (maisons == null) {
			maisons = new EObjectContainmentEList<Maison>(Maison.class, this, FamillePackage.SOUS_FAMILLE__MAISONS);
		}
		return maisons;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Toto> getTotos() {
		if (totos == null) {
			totos = new EObjectContainmentEList<Toto>(Toto.class, this, FamillePackage.SOUS_FAMILLE__TOTOS);
		}
		return totos;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isThereALoop(DiagnosticChain loop1, Map<?, ?> loop2) {
		
		System.out.println("Validation - IsThereALoop");
		
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		
		// copie de la la liste de liens
		EList<Toto> ltCopie = this.getTotos();
		System.out.println("ltCopieSize = " + ltCopie.size());
		boolean isLoop = false;
		// il faut que la liste soit vide
				if(ltCopie.size()>1){
		
		// le booleen de la condition finale
			
	
		// cration d'une liste vide initialisee a la premiere valeur 
			ArrayList<Toto> ltTmp = new ArrayList();
			Toto tmp = ltCopie.get(0);
			System.out.println(tmp.getFrom().getNom());
			
			ltTmp.add(tmp); // -> probleme
			
			System.out.println("Nb ltTmp = " + ltTmp.size());
		// on supprime cette valeur de la copie
			ltCopie.remove(0);
			int currentIndexToto = 0;
			System.out.println("Nb Toto = " + ltCopie.size());
			
			
		// on cherche les liens
			for(Iterator<Toto> it = ltCopie.iterator(); it.hasNext(); ){
			
				Toto totoCurrent = it.next();
				if(totoCurrent.getFrom().equals(ltCopie.get(currentIndexToto).getTo())){
				
				// on ajoute le voisin dans la liste et on le supprime de la copie
					ltTmp.add(ltCopie.get(currentIndexToto));
					if (ltTmp.get(0).getFrom().equals(ltCopie.get(currentIndexToto))){
						isLoop = true;
						break;
					}	
					ltCopie.remove(currentIndexToto);
		
				}else {
					currentIndexToto ++;
				}
			}	
		}

		if (isLoop) {
			if (loop1 != null) {
				loop1.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 FamilleValidator.DIAGNOSTIC_SOURCE,
						 FamilleValidator.SOUS_FAMILLE__IS_THERE_ALOOP,
						 //EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "isThereALoop", EObjectValidator.getObjectLabel(this, loop2) }),
						 "ERREUR - Le circuit ne doit pas etre ferme",
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FamillePackage.SOUS_FAMILLE__MAISONS:
				return ((InternalEList<?>)getMaisons()).basicRemove(otherEnd, msgs);
			case FamillePackage.SOUS_FAMILLE__TOTOS:
				return ((InternalEList<?>)getTotos()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FamillePackage.SOUS_FAMILLE__MAISONS:
				return getMaisons();
			case FamillePackage.SOUS_FAMILLE__TOTOS:
				return getTotos();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FamillePackage.SOUS_FAMILLE__MAISONS:
				getMaisons().clear();
				getMaisons().addAll((Collection<? extends Maison>)newValue);
				return;
			case FamillePackage.SOUS_FAMILLE__TOTOS:
				getTotos().clear();
				getTotos().addAll((Collection<? extends Toto>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FamillePackage.SOUS_FAMILLE__MAISONS:
				getMaisons().clear();
				return;
			case FamillePackage.SOUS_FAMILLE__TOTOS:
				getTotos().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FamillePackage.SOUS_FAMILLE__MAISONS:
				return maisons != null && !maisons.isEmpty();
			case FamillePackage.SOUS_FAMILLE__TOTOS:
				return totos != null && !totos.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //SousFamilleImpl
