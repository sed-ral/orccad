/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille.impl;

import Famille.Chambre;
import Famille.Cuisine;
import Famille.FamilleFactory;
import Famille.FamillePackage;
import Famille.Maison;
import Famille.SousFamille;

import Famille.util.FamilleValidator;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.TreeIterator;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Maison</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link Famille.impl.MaisonImpl#getChambres <em>Chambres</em>}</li>
 *   <li>{@link Famille.impl.MaisonImpl#getCuisine <em>Cuisine</em>}</li>
 *   <li>{@link Famille.impl.MaisonImpl#getNom <em>Nom</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class MaisonImpl extends EObjectImpl implements Maison {
	/**
	 * The cached value of the '{@link #getChambres() <em>Chambres</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChambres()
	 * @generated
	 * @ordered
	 */
	protected EList<Chambre> chambres;

	/**
	 * The cached value of the '{@link #getCuisine() <em>Cuisine</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCuisine()
	 * @generated
	 * @ordered
	 */
	protected Cuisine cuisine;

	/**
	 * The default value of the '{@link #getNom() <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNom()
	 * @generated
	 * @ordered
	 */
	protected static final String NOM_EDEFAULT = null;

	private static final EClass SousFamilleImpl = null;

	/**
	 * The cached value of the '{@link #getNom() <em>Nom</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNom()
	 * @generated
	 * @ordered
	 */
	protected String nom = NOM_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MaisonImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return FamillePackage.Literals.MAISON;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Chambre> getChambres() {
		if (chambres == null) {
			chambres = new EObjectContainmentEList<Chambre>(Chambre.class, this, FamillePackage.MAISON__CHAMBRES);
		}
		return chambres;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cuisine getCuisine() {
		return cuisine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCuisine(Cuisine newCuisine, NotificationChain msgs) {
		Cuisine oldCuisine = cuisine;
		cuisine = newCuisine;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, FamillePackage.MAISON__CUISINE, oldCuisine, newCuisine);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCuisine(Cuisine newCuisine) {
		if (newCuisine != cuisine) {
			NotificationChain msgs = null;
			if (cuisine != null)
				msgs = ((InternalEObject)cuisine).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - FamillePackage.MAISON__CUISINE, null, msgs);
			if (newCuisine != null)
				msgs = ((InternalEObject)newCuisine).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - FamillePackage.MAISON__CUISINE, null, msgs);
			msgs = basicSetCuisine(newCuisine, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FamillePackage.MAISON__CUISINE, newCuisine, newCuisine));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNom(String newNom) {
		String oldNom = nom;
		nom = newNom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, FamillePackage.MAISON__NOM, oldNom, nom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean MaisonHasPiece(DiagnosticChain param1, Map<?, ?> param2) {
	
		System.out.println("Validation - MaisonHasPiece");
		
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		
		try{
			
			//System.out.println(FamilleFactoryImpl.getPackage().getFamilleFactory().toString());	
			
			//SousFamilleImpl titi = (SousFamilleImpl)FamilleFactoryImpl.eContainmentFeature(SousFamilleImpl, titi, eContainerFeatureID);
			//titi.maisons.get(0);
			//System.out.println(titi.maisons.get(0).toString());
		
			//System.out.println(FamillePackage.eINSTANCE.getSousFamille_Maisons().eAdapters().toString());
			
			//FamilleFactory.eINSTANCE.createMaison().setNom("Test1");
			//FamilleFactory.eINSTANCE.create(SousFamilleImpl);
			//FamilleFactory.eINSTANCE.eGet(feature)
			
			//System.out.println("----------------------\nnombre de chambres = " + chambres.size());
			//this.nom = new String("Maison [" + chambres.size() + "]");
			
			
			
			//System.out.println("Nb Maisons = " + Lmaisons.size());
		
			
			//if(SousFamilleImpl != null)
			//System.out.println(SousFamilleImpl.getClass().toString()); else System.out.println("mouarf");
			
			
			//Famille.SousFamille sf = new SousFamilleImpl();
			//TreeIterator Ti = FamilleFactory.eINSTANCE.eAllContents();
			
			//if (Ti == null) System.out.println( "arbre vide"); else  System.out.println( Ti.getClass().toString());
			
			
			
			
			//System.out.println(this.eContainerFeatureID);

			//SousFamilleImpl toto = (SousFamilleImpl ) FamillePackage.eINSTANCE.getSousFamille();
			//System.out.println(toto.getMaisons().get(0).toString());
			
		//	System.out.println(
			//Famille.SousFamille m1 = (Famille.SousFamille)FamillePackage.eINSTANCE.getSousFamille().eContainer();
			
			//System.out.println(m1.getChambres().get(0).toString());
			//);
			//System.out.println(ope.toString());
		}catch( Exception e){
			System.out.println(e.toString());
		}
		
		
		if (chambres.size()<1 || cuisine == null ) {
			if (param1 != null) {
				param1.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 FamilleValidator.DIAGNOSTIC_SOURCE,
						 FamilleValidator.MAISON__MAISON_HAS_PIECE,
						// EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "MaisonHasPiece", EObjectValidator.getObjectLabel(this, (Map<Object, Object>) param2) }),
						 "ERREUR - La Maison doit avoir au moins une chambre.",
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean MaisonHasName(DiagnosticChain param3, Map<?, ?> param4) {
		
		System.out.println("Validation - MaisonHasName");
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
	
//		List<Maison> MaisonListe = (List<Maison>) FamillePackage.eINSTANCE.getMaison() ;		
		
//		System.out.println("MaisonListe = " + MaisonListe.size());
		
/*		System.out.println("--------------TestChambre--------------");
		Chambre ch = (Chambre)FamillePackage.eINSTANCE.getChambre();
		System.out.println(ch.toString());
		System.out.println("-------------- fin TestChambre--------------");
*/		
		
		if (this.getNom() != null && this.getNom().length() <1 ) {
			if (param3 != null) {
				

				param3.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 FamilleValidator.DIAGNOSTIC_SOURCE,
						 FamilleValidator.MAISON__MAISON_HAS_NAME,
						 "ERREUR - La Maison doit porter un nom",
						 //EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "MaisonHasName", EObjectValidator.getObjectLabel(this, (Map<Object, Object>) param4) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean MaisonHasNameUnique(DiagnosticChain param5, Map<?, ?> param6) {
		// TODO: implement this method
		// -> specify the condition that violates the invariant
		// -> verify the details of the diagnostic, including severity and message
		// Ensure that you remove @generated or mark it @generated NOT
		System.out.println("Validation - MaisonHasUniqueName");
		
		SousFamille sf = (SousFamille)this.eContainer();
		EList<Maison> ListMaisons = sf.getMaisons();
		System.out.println("Nb Maisons = " + ListMaisons.size());
		int memeNom = 0;
		for(Iterator<Maison> im = ListMaisons.iterator(); im.hasNext();){
			
			String MaisonNameTmp = im.next().getNom();
			boolean b = (this.getNom()!= null) && (MaisonNameTmp!= null) && (this.getNom().equals(MaisonNameTmp));
			System.out.println("MaisonNameTmp = " + MaisonNameTmp + " this.getNom() = " + this.getNom() +" -> " + b);
			if(this.getNom()!= null && MaisonNameTmp!= null && this.getNom().equals(MaisonNameTmp)) memeNom++;
		}
		
		System.out.println("memeNom = "+ memeNom);
		
		if (memeNom > 1) {
			if (param5 != null) {
				param5.add
					(new BasicDiagnostic
						(Diagnostic.ERROR,
						 FamilleValidator.DIAGNOSTIC_SOURCE,
						 FamilleValidator.MAISON__MAISON_HAS_NAME_UNIQUE,
						 "ERREUR - Le nom des maisons doit etre unique",
						 //EcorePlugin.INSTANCE.getString("_UI_GenericInvariant_diagnostic", new Object[] { "MaisonHasNameUnique", EObjectValidator.getObjectLabel(this, param6) }),
						 new Object [] { this }));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case FamillePackage.MAISON__CHAMBRES:
				return ((InternalEList<?>)getChambres()).basicRemove(otherEnd, msgs);
			case FamillePackage.MAISON__CUISINE:
				return basicSetCuisine(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case FamillePackage.MAISON__CHAMBRES:
				return getChambres();
			case FamillePackage.MAISON__CUISINE:
				return getCuisine();
			case FamillePackage.MAISON__NOM:
				return getNom();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case FamillePackage.MAISON__CHAMBRES:
				getChambres().clear();
				getChambres().addAll((Collection<? extends Chambre>)newValue);
				return;
			case FamillePackage.MAISON__CUISINE:
				setCuisine((Cuisine)newValue);
				return;
			case FamillePackage.MAISON__NOM:
				setNom((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case FamillePackage.MAISON__CHAMBRES:
				getChambres().clear();
				return;
			case FamillePackage.MAISON__CUISINE:
				setCuisine((Cuisine)null);
				return;
			case FamillePackage.MAISON__NOM:
				setNom(NOM_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case FamillePackage.MAISON__CHAMBRES:
				return chambres != null && !chambres.isEmpty();
			case FamillePackage.MAISON__CUISINE:
				return cuisine != null;
			case FamillePackage.MAISON__NOM:
				return NOM_EDEFAULT == null ? nom != null : !NOM_EDEFAULT.equals(nom);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nom: ");
		result.append(nom);
		result.append(')');
		return result.toString();
	}

} //MaisonImpl
