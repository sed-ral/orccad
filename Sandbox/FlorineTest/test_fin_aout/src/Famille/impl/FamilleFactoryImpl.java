/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille.impl;

import Famille.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class FamilleFactoryImpl extends EFactoryImpl implements FamilleFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static FamilleFactory init() {
		try {
			FamilleFactory theFamilleFactory = (FamilleFactory)EPackage.Registry.INSTANCE.getEFactory("http://famille"); 
			if (theFamilleFactory != null) {
				return theFamilleFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new FamilleFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FamilleFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case FamillePackage.MAISON: return createMaison();
			case FamillePackage.CUISINE: return createCuisine();
			case FamillePackage.CHAMBRE: return createChambre();
			case FamillePackage.PIECE: return createPiece();
			case FamillePackage.SOUS_FAMILLE: return createSousFamille();
			case FamillePackage.TOTO: return createToto();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Maison createMaison() {
		MaisonImpl maison = new MaisonImpl();
		return maison;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cuisine createCuisine() {
		CuisineImpl cuisine = new CuisineImpl();
		return cuisine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Chambre createChambre() {
		ChambreImpl chambre = new ChambreImpl();
		return chambre;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Piece createPiece() {
		PieceImpl piece = new PieceImpl();
		return piece;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SousFamille createSousFamille() {
		SousFamilleImpl sousFamille = new SousFamilleImpl();
		return sousFamille;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Toto createToto() {
		TotoImpl toto = new TotoImpl();
		return toto;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FamillePackage getFamillePackage() {
		return (FamillePackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static FamillePackage getPackage() {
		return FamillePackage.eINSTANCE;
	}

} //FamilleFactoryImpl
