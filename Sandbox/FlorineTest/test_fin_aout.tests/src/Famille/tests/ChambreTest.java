/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille.tests;

import Famille.Chambre;
import Famille.FamilleFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Chambre</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class ChambreTest extends PieceTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(ChambreTest.class);
	}

	/**
	 * Constructs a new Chambre test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ChambreTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Chambre test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Chambre getFixture() {
		return (Chambre)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FamilleFactory.eINSTANCE.createChambre());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //ChambreTest
