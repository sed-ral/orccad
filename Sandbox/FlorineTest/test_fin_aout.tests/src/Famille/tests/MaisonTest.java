/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille.tests;

import Famille.FamilleFactory;
import Famille.Maison;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Maison</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link Famille.Maison#MaisonHasPiece(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Maison Has Piece</em>}</li>
 *   <li>{@link Famille.Maison#MaisonHasName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Maison Has Name</em>}</li>
 *   <li>{@link Famille.Maison#MaisonHasNameUnique(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Maison Has Name Unique</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class MaisonTest extends TestCase {

	/**
	 * The fixture for this Maison test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Maison fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(MaisonTest.class);
	}

	/**
	 * Constructs a new Maison test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MaisonTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Maison test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Maison fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Maison test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Maison getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FamilleFactory.eINSTANCE.createMaison());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link Famille.Maison#MaisonHasPiece(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Maison Has Piece</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Famille.Maison#MaisonHasPiece(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testMaisonHasPiece__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link Famille.Maison#MaisonHasName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Maison Has Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Famille.Maison#MaisonHasName(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testMaisonHasName__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

	/**
	 * Tests the '{@link Famille.Maison#MaisonHasNameUnique(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>Maison Has Name Unique</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Famille.Maison#MaisonHasNameUnique(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testMaisonHasNameUnique__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //MaisonTest
