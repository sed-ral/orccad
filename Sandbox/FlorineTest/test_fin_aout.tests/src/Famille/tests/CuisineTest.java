/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille.tests;

import Famille.Cuisine;
import Famille.FamilleFactory;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Cuisine</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class CuisineTest extends PieceTest {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(CuisineTest.class);
	}

	/**
	 * Constructs a new Cuisine test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CuisineTest(String name) {
		super(name);
	}

	/**
	 * Returns the fixture for this Cuisine test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Cuisine getFixture() {
		return (Cuisine)fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FamilleFactory.eINSTANCE.createCuisine());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //CuisineTest
