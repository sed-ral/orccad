/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille.tests;

import Famille.FamilleFactory;
import Famille.Toto;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Toto</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link Famille.Toto#NoLoop(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>No Loop</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class TotoTest extends TestCase {

	/**
	 * The fixture for this Toto test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Toto fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(TotoTest.class);
	}

	/**
	 * Constructs a new Toto test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TotoTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Toto test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Toto fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Toto test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Toto getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(FamilleFactory.eINSTANCE.createToto());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link Famille.Toto#NoLoop(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map) <em>No Loop</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see Famille.Toto#NoLoop(org.eclipse.emf.common.util.DiagnosticChain, java.util.Map)
	 * @generated
	 */
	public void testNoLoop__DiagnosticChain_Map() {
		// TODO: implement this operation test method
		// Ensure that you remove @generated or mark it @generated NOT
		fail();
	}

} //TotoTest
