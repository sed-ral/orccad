/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package Famille.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Famille</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class FamilleTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new FamilleTests("Famille Tests");
		suite.addTestSuite(MaisonTest.class);
		suite.addTestSuite(SousFamilleTest.class);
		suite.addTestSuite(TotoTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FamilleTests(String name) {
		super(name);
	}

} //FamilleTests
