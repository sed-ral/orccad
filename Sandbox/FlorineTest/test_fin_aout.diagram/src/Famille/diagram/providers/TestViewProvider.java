package Famille.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.core.providers.AbstractViewProvider;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @generated
 */
public class TestViewProvider extends AbstractViewProvider {

	/**
	 * @generated
	 */
	protected Class getDiagramViewClass(IAdaptable semanticAdapter,
			String diagramKind) {
		EObject semanticElement = getSemanticElement(semanticAdapter);
		if (Famille.diagram.edit.parts.SousFamilleEditPart.MODEL_ID
				.equals(diagramKind)
				&& Famille.diagram.part.TestVisualIDRegistry
						.getDiagramVisualID(semanticElement) != -1) {
			return Famille.diagram.view.factories.SousFamilleViewFactory.class;
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Class getNodeViewClass(IAdaptable semanticAdapter,
			View containerView, String semanticHint) {
		if (containerView == null) {
			return null;
		}
		IElementType elementType = getSemanticElementType(semanticAdapter);
		EObject domainElement = getSemanticElement(semanticAdapter);
		int visualID;
		if (semanticHint == null) {
			// Semantic hint is not specified. Can be a result of call from CanonicalEditPolicy.
			// In this situation there should be NO elementType, visualID will be determined
			// by VisualIDRegistry.getNodeVisualID() for domainElement.
			if (elementType != null || domainElement == null) {
				return null;
			}
			visualID = Famille.diagram.part.TestVisualIDRegistry
					.getNodeVisualID(containerView, domainElement);
		} else {
			visualID = Famille.diagram.part.TestVisualIDRegistry
					.getVisualID(semanticHint);
			if (elementType != null) {
				// Semantic hint is specified together with element type.
				// Both parameters should describe exactly the same diagram element.
				// In addition we check that visualID returned by VisualIDRegistry.getNodeVisualID() for
				// domainElement (if specified) is the same as in element type.
				if (!Famille.diagram.providers.TestElementTypes
						.isKnownElementType(elementType)
						|| (!(elementType instanceof IHintedType))) {
					return null; // foreign element type
				}
				String elementTypeHint = ((IHintedType) elementType)
						.getSemanticHint();
				if (!semanticHint.equals(elementTypeHint)) {
					return null; // if semantic hint is specified it should be the same as in element type
				}
				if (domainElement != null
						&& visualID != Famille.diagram.part.TestVisualIDRegistry
								.getNodeVisualID(containerView, domainElement)) {
					return null; // visual id for node EClass should match visual id from element type
				}
			} else {
				// Element type is not specified. Domain element should be present (except pure design elements).
				// This method is called with EObjectAdapter as parameter from:
				//   - ViewService.createNode(View container, EObject eObject, String type, PreferencesHint preferencesHint) 
				//   - generated ViewFactory.decorateView() for parent element
				if (!Famille.diagram.edit.parts.SousFamilleEditPart.MODEL_ID
						.equals(Famille.diagram.part.TestVisualIDRegistry
								.getModelID(containerView))) {
					return null; // foreign diagram
				}
				switch (visualID) {
				case Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID:
				case Famille.diagram.edit.parts.CuisineEditPart.VISUAL_ID:
				case Famille.diagram.edit.parts.ChambreEditPart.VISUAL_ID:
					if (domainElement == null
							|| visualID != Famille.diagram.part.TestVisualIDRegistry
									.getNodeVisualID(containerView,
											domainElement)) {
						return null; // visual id in semantic hint should match visual id for domain element
					}
					break;
				case Famille.diagram.edit.parts.MaisonNomEditPart.VISUAL_ID:
				case Famille.diagram.edit.parts.MaisonMaisonCompartmentEditPart.VISUAL_ID:
					if (Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID != Famille.diagram.part.TestVisualIDRegistry
							.getVisualID(containerView)
							|| containerView.getElement() != domainElement) {
						return null; // wrong container
					}
					break;
				default:
					return null;
				}
			}
		}
		return getNodeViewClass(containerView, visualID);
	}

	/**
	 * @generated
	 */
	protected Class getNodeViewClass(View containerView, int visualID) {
		if (containerView == null
				|| !Famille.diagram.part.TestVisualIDRegistry.canCreateNode(
						containerView, visualID)) {
			return null;
		}
		switch (visualID) {
		case Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID:
			return Famille.diagram.view.factories.MaisonViewFactory.class;
		case Famille.diagram.edit.parts.MaisonNomEditPart.VISUAL_ID:
			return Famille.diagram.view.factories.MaisonNomViewFactory.class;
		case Famille.diagram.edit.parts.CuisineEditPart.VISUAL_ID:
			return Famille.diagram.view.factories.CuisineViewFactory.class;
		case Famille.diagram.edit.parts.ChambreEditPart.VISUAL_ID:
			return Famille.diagram.view.factories.ChambreViewFactory.class;
		case Famille.diagram.edit.parts.MaisonMaisonCompartmentEditPart.VISUAL_ID:
			return Famille.diagram.view.factories.MaisonMaisonCompartmentViewFactory.class;
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Class getEdgeViewClass(IAdaptable semanticAdapter,
			View containerView, String semanticHint) {
		IElementType elementType = getSemanticElementType(semanticAdapter);
		if (!Famille.diagram.providers.TestElementTypes
				.isKnownElementType(elementType)
				|| (!(elementType instanceof IHintedType))) {
			return null; // foreign element type
		}
		String elementTypeHint = ((IHintedType) elementType).getSemanticHint();
		if (elementTypeHint == null) {
			return null; // our hint is visual id and must be specified
		}
		if (semanticHint != null && !semanticHint.equals(elementTypeHint)) {
			return null; // if semantic hint is specified it should be the same as in element type
		}
		int visualID = Famille.diagram.part.TestVisualIDRegistry
				.getVisualID(elementTypeHint);
		EObject domainElement = getSemanticElement(semanticAdapter);
		if (domainElement != null
				&& visualID != Famille.diagram.part.TestVisualIDRegistry
						.getLinkWithClassVisualID(domainElement)) {
			return null; // visual id for link EClass should match visual id from element type
		}
		return getEdgeViewClass(visualID);
	}

	/**
	 * @generated
	 */
	protected Class getEdgeViewClass(int visualID) {
		switch (visualID) {
		case Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID:
			return Famille.diagram.view.factories.TotoViewFactory.class;
		}
		return null;
	}

	/**
	 * @generated
	 */
	private IElementType getSemanticElementType(IAdaptable semanticAdapter) {
		if (semanticAdapter == null) {
			return null;
		}
		return (IElementType) semanticAdapter.getAdapter(IElementType.class);
	}
}
