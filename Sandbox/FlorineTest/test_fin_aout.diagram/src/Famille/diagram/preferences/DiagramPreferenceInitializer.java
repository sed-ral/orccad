package Famille.diagram.preferences;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

/**
 * @generated
 */
public class DiagramPreferenceInitializer extends AbstractPreferenceInitializer {

	/**
	 * @generated
	 */
	public void initializeDefaultPreferences() {
		IPreferenceStore store = getPreferenceStore();
		Famille.diagram.preferences.DiagramPrintingPreferencePage
				.initDefaults(store);
		Famille.diagram.preferences.DiagramGeneralPreferencePage
				.initDefaults(store);
		Famille.diagram.preferences.DiagramAppearancePreferencePage
				.initDefaults(store);
		Famille.diagram.preferences.DiagramConnectionsPreferencePage
				.initDefaults(store);
		Famille.diagram.preferences.DiagramRulersAndGridPreferencePage
				.initDefaults(store);
	}

	/**
	 * @generated
	 */
	protected IPreferenceStore getPreferenceStore() {
		return Famille.diagram.part.TestDiagramEditorPlugin.getInstance()
				.getPreferenceStore();
	}
}
