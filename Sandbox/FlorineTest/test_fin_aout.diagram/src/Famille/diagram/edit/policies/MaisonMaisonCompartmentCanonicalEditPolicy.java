package Famille.diagram.edit.policies;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @generated
 */
public class MaisonMaisonCompartmentCanonicalEditPolicy extends
		CanonicalEditPolicy {

	/**
	 * @generated
	 */
	Set myFeaturesToSynchronize;

	/**
	 * @generated
	 */
	protected List getSemanticChildrenList() {
		View viewObject = (View) getHost().getModel();
		List result = new LinkedList();
		for (Iterator it = Famille.diagram.part.TestDiagramUpdater
				.getMaisonMaisonCompartment_7001SemanticChildren(viewObject)
				.iterator(); it.hasNext();) {
			result.add(((Famille.diagram.part.TestNodeDescriptor) it.next())
					.getModelElement());
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected boolean isOrphaned(Collection semanticChildren, final View view) {
		int visualID = Famille.diagram.part.TestVisualIDRegistry
				.getVisualID(view);
		switch (visualID) {
		case Famille.diagram.edit.parts.CuisineEditPart.VISUAL_ID:
		case Famille.diagram.edit.parts.ChambreEditPart.VISUAL_ID:
			if (!semanticChildren.contains(view.getElement())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected String getDefaultFactoryHint() {
		return null;
	}

	/**
	 * @generated
	 */
	protected Set getFeaturesToSynchronize() {
		if (myFeaturesToSynchronize == null) {
			myFeaturesToSynchronize = new HashSet();
			myFeaturesToSynchronize.add(Famille.FamillePackage.eINSTANCE
					.getMaison_Cuisine());
			myFeaturesToSynchronize.add(Famille.FamillePackage.eINSTANCE
					.getMaison_Chambres());
		}
		return myFeaturesToSynchronize;
	}

}
