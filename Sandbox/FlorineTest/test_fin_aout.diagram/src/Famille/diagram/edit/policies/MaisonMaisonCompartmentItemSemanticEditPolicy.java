package Famille.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

/**
 * @generated
 */
public class MaisonMaisonCompartmentItemSemanticEditPolicy extends
		Famille.diagram.edit.policies.TestBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (Famille.diagram.providers.TestElementTypes.Cuisine_3002 == req
				.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(Famille.FamillePackage.eINSTANCE
						.getMaison_Cuisine());
			}
			return getGEFWrapper(new Famille.diagram.edit.commands.CuisineCreateCommand(
					req));
		}
		if (Famille.diagram.providers.TestElementTypes.Chambre_3001 == req
				.getElementType()) {
			if (req.getContainmentFeature() == null) {
				req.setContainmentFeature(Famille.FamillePackage.eINSTANCE
						.getMaison_Chambres());
			}
			return getGEFWrapper(new Famille.diagram.edit.commands.ChambreCreateCommand(
					req));
		}
		return super.getCreateCommand(req);
	}

}
