package Famille.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

/**
 * @generated
 */
public class TotoReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public TotoReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof Famille.Toto) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof Famille.Maison && newEnd instanceof Famille.Maison)) {
			return false;
		}
		Famille.Maison target = getLink().getTo();
		if (!(getLink().eContainer() instanceof Famille.SousFamille)) {
			return false;
		}
		Famille.SousFamille container = (Famille.SousFamille) getLink()
				.eContainer();
		return Famille.diagram.edit.policies.TestBaseItemSemanticEditPolicy.LinkConstraints
				.canExistToto_4001(container, getNewSource(), target);
	}

	/**
	 * @generated
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof Famille.Maison && newEnd instanceof Famille.Maison)) {
			return false;
		}
		Famille.Maison source = getLink().getFrom();
		if (!(getLink().eContainer() instanceof Famille.SousFamille)) {
			return false;
		}
		Famille.SousFamille container = (Famille.SousFamille) getLink()
				.eContainer();
		return Famille.diagram.edit.policies.TestBaseItemSemanticEditPolicy.LinkConstraints
				.canExistToto_4001(container, source, getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor,
			IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException(
					"Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getLink().setFrom(getNewSource());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTo(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated
	 */
	protected Famille.Toto getLink() {
		return (Famille.Toto) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected Famille.Maison getOldSource() {
		return (Famille.Maison) oldEnd;
	}

	/**
	 * @generated
	 */
	protected Famille.Maison getNewSource() {
		return (Famille.Maison) newEnd;
	}

	/**
	 * @generated
	 */
	protected Famille.Maison getOldTarget() {
		return (Famille.Maison) oldEnd;
	}

	/**
	 * @generated
	 */
	protected Famille.Maison getNewTarget() {
		return (Famille.Maison) newEnd;
	}
}
