package Famille.diagram.navigator;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserOptions;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.viewers.ITreePathLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.ViewerLabel;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.navigator.ICommonContentExtensionSite;
import org.eclipse.ui.navigator.ICommonLabelProvider;

/**
 * @generated
 */
public class TestNavigatorLabelProvider extends LabelProvider implements
		ICommonLabelProvider, ITreePathLabelProvider {

	/**
	 * @generated
	 */
	static {
		Famille.diagram.part.TestDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put(
						"Navigator?UnknownElement", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
		Famille.diagram.part.TestDiagramEditorPlugin
				.getInstance()
				.getImageRegistry()
				.put(
						"Navigator?ImageNotFound", ImageDescriptor.getMissingImageDescriptor()); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	public void updateLabel(ViewerLabel label, TreePath elementPath) {
		Object element = elementPath.getLastSegment();
		if (element instanceof Famille.diagram.navigator.TestNavigatorItem
				&& !isOwnView(((Famille.diagram.navigator.TestNavigatorItem) element)
						.getView())) {
			return;
		}
		label.setText(getText(element));
		label.setImage(getImage(element));
	}

	/**
	 * @generated
	 */
	public Image getImage(Object element) {
		if (element instanceof Famille.diagram.navigator.TestNavigatorGroup) {
			Famille.diagram.navigator.TestNavigatorGroup group = (Famille.diagram.navigator.TestNavigatorGroup) element;
			return Famille.diagram.part.TestDiagramEditorPlugin.getInstance()
					.getBundledImage(group.getIcon());
		}

		if (element instanceof Famille.diagram.navigator.TestNavigatorItem) {
			Famille.diagram.navigator.TestNavigatorItem navigatorItem = (Famille.diagram.navigator.TestNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return super.getImage(element);
			}
			return getImage(navigatorItem.getView());
		}

		return super.getImage(element);
	}

	/**
	 * @generated
	 */
	public Image getImage(View view) {
		switch (Famille.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Famille.diagram.edit.parts.SousFamilleEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Diagram?http://famille?SousFamille", Famille.diagram.providers.TestElementTypes.SousFamille_1000); //$NON-NLS-1$
		case Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID:
			return getImage(
					"Navigator?TopLevelNode?http://famille?Maison", Famille.diagram.providers.TestElementTypes.Maison_2001); //$NON-NLS-1$
		case Famille.diagram.edit.parts.CuisineEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://famille?Cuisine", Famille.diagram.providers.TestElementTypes.Cuisine_3002); //$NON-NLS-1$
		case Famille.diagram.edit.parts.ChambreEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Node?http://famille?Chambre", Famille.diagram.providers.TestElementTypes.Chambre_3001); //$NON-NLS-1$
		case Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID:
			return getImage(
					"Navigator?Link?http://famille?Toto", Famille.diagram.providers.TestElementTypes.Toto_4001); //$NON-NLS-1$
		}
		return getImage("Navigator?UnknownElement", null); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Image getImage(String key, IElementType elementType) {
		ImageRegistry imageRegistry = Famille.diagram.part.TestDiagramEditorPlugin
				.getInstance().getImageRegistry();
		Image image = imageRegistry.get(key);
		if (image == null
				&& elementType != null
				&& Famille.diagram.providers.TestElementTypes
						.isKnownElementType(elementType)) {
			image = Famille.diagram.providers.TestElementTypes
					.getImage(elementType);
			imageRegistry.put(key, image);
		}

		if (image == null) {
			image = imageRegistry.get("Navigator?ImageNotFound"); //$NON-NLS-1$
			imageRegistry.put(key, image);
		}
		return image;
	}

	/**
	 * @generated
	 */
	public String getText(Object element) {
		if (element instanceof Famille.diagram.navigator.TestNavigatorGroup) {
			Famille.diagram.navigator.TestNavigatorGroup group = (Famille.diagram.navigator.TestNavigatorGroup) element;
			return group.getGroupName();
		}

		if (element instanceof Famille.diagram.navigator.TestNavigatorItem) {
			Famille.diagram.navigator.TestNavigatorItem navigatorItem = (Famille.diagram.navigator.TestNavigatorItem) element;
			if (!isOwnView(navigatorItem.getView())) {
				return null;
			}
			return getText(navigatorItem.getView());
		}

		return super.getText(element);
	}

	/**
	 * @generated
	 */
	public String getText(View view) {
		if (view.getElement() != null && view.getElement().eIsProxy()) {
			return getUnresolvedDomainElementProxyText(view);
		}
		switch (Famille.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Famille.diagram.edit.parts.SousFamilleEditPart.VISUAL_ID:
			return getSousFamille_1000Text(view);
		case Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID:
			return getMaison_2001Text(view);
		case Famille.diagram.edit.parts.CuisineEditPart.VISUAL_ID:
			return getCuisine_3002Text(view);
		case Famille.diagram.edit.parts.ChambreEditPart.VISUAL_ID:
			return getChambre_3001Text(view);
		case Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID:
			return getToto_4001Text(view);
		}
		return getUnknownElementText(view);
	}

	/**
	 * @generated
	 */
	private String getSousFamille_1000Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getMaison_2001Text(View view) {
		IAdaptable hintAdapter = new Famille.diagram.providers.TestParserProvider.HintAdapter(
				Famille.diagram.providers.TestElementTypes.Maison_2001,
				(view.getElement() != null ? view.getElement() : view),
				Famille.diagram.part.TestVisualIDRegistry
						.getType(Famille.diagram.edit.parts.MaisonNomEditPart.VISUAL_ID));
		IParser parser = ParserService.getInstance().getParser(hintAdapter);

		if (parser != null) {
			return parser.getPrintString(hintAdapter, ParserOptions.NONE
					.intValue());
		} else {
			Famille.diagram.part.TestDiagramEditorPlugin.getInstance()
					.logError("Parser was not found for label " + 5001); //$NON-NLS-1$
			return ""; //$NON-NLS-1$
		}

	}

	/**
	 * @generated
	 */
	private String getCuisine_3002Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getChambre_3001Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getToto_4001Text(View view) {
		return ""; //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private String getUnknownElementText(View view) {
		return "<UnknownElement Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	private String getUnresolvedDomainElementProxyText(View view) {
		return "<Unresolved domain element Visual_ID = " + view.getType() + ">"; //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * @generated
	 */
	public void init(ICommonContentExtensionSite aConfig) {
	}

	/**
	 * @generated
	 */
	public void restoreState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public void saveState(IMemento aMemento) {
	}

	/**
	 * @generated
	 */
	public String getDescription(Object anElement) {
		return null;
	}

	/**
	 * @generated
	 */
	private boolean isOwnView(View view) {
		return Famille.diagram.edit.parts.SousFamilleEditPart.MODEL_ID
				.equals(Famille.diagram.part.TestVisualIDRegistry
						.getModelID(view));
	}

}
