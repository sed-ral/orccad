package Famille.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

/**
 * @generated
 */
public class TestNavigatorSorter extends ViewerSorter {

	/**
	 * @generated
	 */
	private static final int GROUP_CATEGORY = 7003;

	/**
	 * @generated
	 */
	public int category(Object element) {
		if (element instanceof Famille.diagram.navigator.TestNavigatorItem) {
			Famille.diagram.navigator.TestNavigatorItem item = (Famille.diagram.navigator.TestNavigatorItem) element;
			return Famille.diagram.part.TestVisualIDRegistry.getVisualID(item
					.getView());
		}
		return GROUP_CATEGORY;
	}

}
