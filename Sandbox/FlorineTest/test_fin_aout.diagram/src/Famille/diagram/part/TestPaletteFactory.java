package Famille.diagram.part;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.gef.Tool;
import org.eclipse.gef.palette.PaletteContainer;
import org.eclipse.gef.palette.PaletteGroup;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gef.palette.ToolEntry;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeConnectionTool;
import org.eclipse.gmf.runtime.diagram.ui.tools.UnspecifiedTypeCreationTool;

/**
 * @generated
 */
public class TestPaletteFactory {

	/**
	 * @generated
	 */
	public void fillPalette(PaletteRoot paletteRoot) {
		paletteRoot.add(createFamille1Group());
	}

	/**
	 * Creates "Famille" palette tool group
	 * @generated
	 */
	private PaletteContainer createFamille1Group() {
		PaletteGroup paletteContainer = new PaletteGroup(
				Famille.diagram.part.Messages.Famille1Group_title);
		paletteContainer.add(createMaison1CreationTool());
		paletteContainer.add(createCuisine2CreationTool());
		paletteContainer.add(createChambre3CreationTool());
		paletteContainer.add(createToto4CreationTool());
		return paletteContainer;
	}

	/**
	 * @generated
	 */
	private ToolEntry createMaison1CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(Famille.diagram.providers.TestElementTypes.Maison_2001);
		NodeToolEntry entry = new NodeToolEntry(
				Famille.diagram.part.Messages.Maison1CreationTool_title,
				Famille.diagram.part.Messages.Maison1CreationTool_desc, types);
		entry
				.setSmallIcon(Famille.diagram.providers.TestElementTypes
						.getImageDescriptor(Famille.diagram.providers.TestElementTypes.Maison_2001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createCuisine2CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(Famille.diagram.providers.TestElementTypes.Cuisine_3002);
		NodeToolEntry entry = new NodeToolEntry(
				Famille.diagram.part.Messages.Cuisine2CreationTool_title,
				Famille.diagram.part.Messages.Cuisine2CreationTool_desc, types);
		entry
				.setSmallIcon(Famille.diagram.providers.TestElementTypes
						.getImageDescriptor(Famille.diagram.providers.TestElementTypes.Cuisine_3002));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createChambre3CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(Famille.diagram.providers.TestElementTypes.Chambre_3001);
		NodeToolEntry entry = new NodeToolEntry(
				Famille.diagram.part.Messages.Chambre3CreationTool_title,
				Famille.diagram.part.Messages.Chambre3CreationTool_desc, types);
		entry
				.setSmallIcon(Famille.diagram.providers.TestElementTypes
						.getImageDescriptor(Famille.diagram.providers.TestElementTypes.Chambre_3001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private ToolEntry createToto4CreationTool() {
		List/*<IElementType>*/types = new ArrayList/*<IElementType>*/(1);
		types.add(Famille.diagram.providers.TestElementTypes.Toto_4001);
		LinkToolEntry entry = new LinkToolEntry(
				Famille.diagram.part.Messages.Toto4CreationTool_title,
				Famille.diagram.part.Messages.Toto4CreationTool_desc, types);
		entry
				.setSmallIcon(Famille.diagram.providers.TestElementTypes
						.getImageDescriptor(Famille.diagram.providers.TestElementTypes.Toto_4001));
		entry.setLargeIcon(entry.getSmallIcon());
		return entry;
	}

	/**
	 * @generated
	 */
	private static class NodeToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List elementTypes;

		/**
		 * @generated
		 */
		private NodeToolEntry(String title, String description,
				List elementTypes) {
			super(title, description, null, null);
			this.elementTypes = elementTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeCreationTool(elementTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}

	/**
	 * @generated
	 */
	private static class LinkToolEntry extends ToolEntry {

		/**
		 * @generated
		 */
		private final List relationshipTypes;

		/**
		 * @generated
		 */
		private LinkToolEntry(String title, String description,
				List relationshipTypes) {
			super(title, description, null, null);
			this.relationshipTypes = relationshipTypes;
		}

		/**
		 * @generated
		 */
		public Tool createTool() {
			Tool tool = new UnspecifiedTypeConnectionTool(relationshipTypes);
			tool.setProperties(getToolProperties());
			return tool;
		}
	}
}
