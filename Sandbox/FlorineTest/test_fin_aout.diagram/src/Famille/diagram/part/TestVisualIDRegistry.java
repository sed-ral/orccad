package Famille.diagram.part;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;

/**
 * This registry is used to determine which type of visual object should be
 * created for the corresponding Diagram, Node, ChildNode or Link represented
 * by a domain model object.
 * 
 * @generated
 */
public class TestVisualIDRegistry {

	/**
	 * @generated
	 */
	private static final String DEBUG_KEY = "test_fin_aout.diagram/debug/visualID"; //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static int getVisualID(View view) {
		if (view instanceof Diagram) {
			if (Famille.diagram.edit.parts.SousFamilleEditPart.MODEL_ID
					.equals(view.getType())) {
				return Famille.diagram.edit.parts.SousFamilleEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		return Famille.diagram.part.TestVisualIDRegistry.getVisualID(view
				.getType());
	}

	/**
	 * @generated
	 */
	public static String getModelID(View view) {
		View diagram = view.getDiagram();
		while (view != diagram) {
			EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
			if (annotation != null) {
				return (String) annotation.getDetails().get("modelID"); //$NON-NLS-1$
			}
			view = (View) view.eContainer();
		}
		return diagram != null ? diagram.getType() : null;
	}

	/**
	 * @generated
	 */
	public static int getVisualID(String type) {
		try {
			return Integer.parseInt(type);
		} catch (NumberFormatException e) {
			if (Boolean.TRUE.toString().equalsIgnoreCase(
					Platform.getDebugOption(DEBUG_KEY))) {
				Famille.diagram.part.TestDiagramEditorPlugin.getInstance()
						.logError(
								"Unable to parse view type as a visualID number: "
										+ type);
			}
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static String getType(int visualID) {
		return String.valueOf(visualID);
	}

	/**
	 * @generated
	 */
	public static int getDiagramVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (Famille.FamillePackage.eINSTANCE.getSousFamille().isSuperTypeOf(
				domainElement.eClass())
				&& isDiagram((Famille.SousFamille) domainElement)) {
			return Famille.diagram.edit.parts.SousFamilleEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static int getNodeVisualID(View containerView, EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		String containerModelID = Famille.diagram.part.TestVisualIDRegistry
				.getModelID(containerView);
		if (!Famille.diagram.edit.parts.SousFamilleEditPart.MODEL_ID
				.equals(containerModelID)) {
			return -1;
		}
		int containerVisualID;
		if (Famille.diagram.edit.parts.SousFamilleEditPart.MODEL_ID
				.equals(containerModelID)) {
			containerVisualID = Famille.diagram.part.TestVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = Famille.diagram.edit.parts.SousFamilleEditPart.VISUAL_ID;
			} else {
				return -1;
			}
		}
		switch (containerVisualID) {
		case Famille.diagram.edit.parts.MaisonMaisonCompartmentEditPart.VISUAL_ID:
			if (Famille.FamillePackage.eINSTANCE.getCuisine().isSuperTypeOf(
					domainElement.eClass())) {
				return Famille.diagram.edit.parts.CuisineEditPart.VISUAL_ID;
			}
			if (Famille.FamillePackage.eINSTANCE.getChambre().isSuperTypeOf(
					domainElement.eClass())) {
				return Famille.diagram.edit.parts.ChambreEditPart.VISUAL_ID;
			}
			break;
		case Famille.diagram.edit.parts.SousFamilleEditPart.VISUAL_ID:
			if (Famille.FamillePackage.eINSTANCE.getMaison().isSuperTypeOf(
					domainElement.eClass())) {
				return Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID;
			}
			break;
		}
		return -1;
	}

	/**
	 * @generated
	 */
	public static boolean canCreateNode(View containerView, int nodeVisualID) {
		String containerModelID = Famille.diagram.part.TestVisualIDRegistry
				.getModelID(containerView);
		if (!Famille.diagram.edit.parts.SousFamilleEditPart.MODEL_ID
				.equals(containerModelID)) {
			return false;
		}
		int containerVisualID;
		if (Famille.diagram.edit.parts.SousFamilleEditPart.MODEL_ID
				.equals(containerModelID)) {
			containerVisualID = Famille.diagram.part.TestVisualIDRegistry
					.getVisualID(containerView);
		} else {
			if (containerView instanceof Diagram) {
				containerVisualID = Famille.diagram.edit.parts.SousFamilleEditPart.VISUAL_ID;
			} else {
				return false;
			}
		}
		switch (containerVisualID) {
		case Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID:
			if (Famille.diagram.edit.parts.MaisonNomEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Famille.diagram.edit.parts.MaisonMaisonCompartmentEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Famille.diagram.edit.parts.MaisonMaisonCompartmentEditPart.VISUAL_ID:
			if (Famille.diagram.edit.parts.CuisineEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			if (Famille.diagram.edit.parts.ChambreEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		case Famille.diagram.edit.parts.SousFamilleEditPart.VISUAL_ID:
			if (Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID == nodeVisualID) {
				return true;
			}
			break;
		}
		return false;
	}

	/**
	 * @generated
	 */
	public static int getLinkWithClassVisualID(EObject domainElement) {
		if (domainElement == null) {
			return -1;
		}
		if (Famille.FamillePackage.eINSTANCE.getToto().isSuperTypeOf(
				domainElement.eClass())) {
			return Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID;
		}
		return -1;
	}

	/**
	 * User can change implementation of this method to handle some specific
	 * situations not covered by default logic.
	 * 
	 * @generated
	 */
	private static boolean isDiagram(Famille.SousFamille element) {
		return true;
	}

}
