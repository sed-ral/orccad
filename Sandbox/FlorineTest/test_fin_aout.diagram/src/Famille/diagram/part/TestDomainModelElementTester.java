package Famille.diagram.part;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * @generated
 */
public class TestDomainModelElementTester extends PropertyTester {

	/**
	 * @generated
	 */
	public boolean test(Object receiver, String method, Object[] args,
			Object expectedValue) {
		if (false == receiver instanceof EObject) {
			return false;
		}
		EObject eObject = (EObject) receiver;
		EClass eClass = eObject.eClass();
		if (eClass == Famille.FamillePackage.eINSTANCE.getMaison()) {
			return true;
		}
		if (eClass == Famille.FamillePackage.eINSTANCE.getCuisine()) {
			return true;
		}
		if (eClass == Famille.FamillePackage.eINSTANCE.getChambre()) {
			return true;
		}
		if (eClass == Famille.FamillePackage.eINSTANCE.getPiece()) {
			return true;
		}
		if (eClass == Famille.FamillePackage.eINSTANCE.getSousFamille()) {
			return true;
		}
		if (eClass == Famille.FamillePackage.eINSTANCE.getToto()) {
			return true;
		}
		return false;
	}

}
