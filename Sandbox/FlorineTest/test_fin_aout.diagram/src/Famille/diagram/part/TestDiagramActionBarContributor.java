package Famille.diagram.part;

import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramActionBarContributor;

/**
 * @generated
 */
public class TestDiagramActionBarContributor extends
		DiagramActionBarContributor {

	/**
	 * @generated
	 */
	protected Class getEditorClass() {
		return Famille.diagram.part.TestDiagramEditor.class;
	}

	/**
	 * @generated
	 */
	protected String getEditorId() {
		return Famille.diagram.part.TestDiagramEditor.ID;
	}
}
