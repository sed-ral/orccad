package Famille.diagram.part;

import org.eclipse.osgi.util.NLS;

/**
 * @generated
 */
public class Messages extends NLS {

	/**
	 * @generated
	 */
	static {
		NLS.initializeMessages("messages", Messages.class); //$NON-NLS-1$
	}

	/**
	 * @generated
	 */
	private Messages() {
	}

	/**
	 * @generated
	 */
	public static String TestCreationWizardTitle;

	/**
	 * @generated
	 */
	public static String TestCreationWizard_DiagramModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TestCreationWizard_DiagramModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TestCreationWizard_DomainModelFilePageTitle;

	/**
	 * @generated
	 */
	public static String TestCreationWizard_DomainModelFilePageDescription;

	/**
	 * @generated
	 */
	public static String TestCreationWizardOpenEditorError;

	/**
	 * @generated
	 */
	public static String TestCreationWizardCreationError;

	/**
	 * @generated
	 */
	public static String TestCreationWizardPageExtensionError;

	/**
	 * @generated
	 */
	public static String TestDiagramEditorUtil_OpenModelResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String TestDiagramEditorUtil_OpenModelResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String TestDiagramEditorUtil_CreateDiagramProgressTask;

	/**
	 * @generated
	 */
	public static String TestDiagramEditorUtil_CreateDiagramCommandLabel;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_isModifiable;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_handleElementContentChanged;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_IncorrectInputError;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_NoDiagramInResourceError;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_DiagramLoadingError;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_UnsynchronizedFileSaveError;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_SaveDiagramTask;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_SaveNextResourceTask;

	/**
	 * @generated
	 */
	public static String TestDocumentProvider_SaveAsOperation;

	/**
	 * @generated
	 */
	public static String TestInitDiagramFileAction_InitDiagramFileResourceErrorDialogTitle;

	/**
	 * @generated
	 */
	public static String TestInitDiagramFileAction_InitDiagramFileResourceErrorDialogMessage;

	/**
	 * @generated
	 */
	public static String TestInitDiagramFileAction_InitDiagramFileWizardTitle;

	/**
	 * @generated
	 */
	public static String TestInitDiagramFileAction_OpenModelFileDialogTitle;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_CreationPageName;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_CreationPageTitle;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_CreationPageDescription;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageName;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageTitle;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageDescription;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageSelectionTitle;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageNoSelectionMessage;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_RootSelectionPageInvalidSelectionMessage;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_InitDiagramCommand;

	/**
	 * @generated
	 */
	public static String TestNewDiagramFileWizard_IncorrectRootError;

	/**
	 * @generated
	 */
	public static String TestDiagramEditor_SavingDeletedFile;

	/**
	 * @generated
	 */
	public static String TestDiagramEditor_SaveAsErrorTitle;

	/**
	 * @generated
	 */
	public static String TestDiagramEditor_SaveAsErrorMessage;

	/**
	 * @generated
	 */
	public static String TestDiagramEditor_SaveErrorTitle;

	/**
	 * @generated
	 */
	public static String TestDiagramEditor_SaveErrorMessage;

	/**
	 * @generated
	 */
	public static String TestElementChooserDialog_SelectModelElementTitle;

	/**
	 * @generated
	 */
	public static String ModelElementSelectionPageMessage;

	/**
	 * @generated
	 */
	public static String ValidateActionMessage;

	/**
	 * @generated
	 */
	public static String Famille1Group_title;

	/**
	 * @generated
	 */
	public static String Maison1CreationTool_title;

	/**
	 * @generated
	 */
	public static String Maison1CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Cuisine2CreationTool_title;

	/**
	 * @generated
	 */
	public static String Cuisine2CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Chambre3CreationTool_title;

	/**
	 * @generated
	 */
	public static String Chambre3CreationTool_desc;

	/**
	 * @generated
	 */
	public static String Toto4CreationTool_title;

	/**
	 * @generated
	 */
	public static String Toto4CreationTool_desc;

	/**
	 * @generated
	 */
	public static String MaisonMaisonCompartmentEditPart_title;

	/**
	 * @generated
	 */
	public static String CommandName_OpenDiagram;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_SousFamille_1000_links;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Maison_2001_incominglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Maison_2001_outgoinglinks;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Toto_4001_target;

	/**
	 * @generated
	 */
	public static String NavigatorGroupName_Toto_4001_source;

	/**
	 * @generated
	 */
	public static String NavigatorActionProvider_OpenDiagramActionName;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnexpectedValueTypeMessage;

	/**
	 * @generated
	 */
	public static String AbstractParser_WrongStringConversionMessage;

	/**
	 * @generated
	 */
	public static String AbstractParser_UnknownLiteralMessage;

	/**
	 * @generated
	 */
	public static String MessageFormatParser_InvalidInputError;

	/**
	 * @generated
	 */
	public static String TestModelingAssistantProviderTitle;

	/**
	 * @generated
	 */
	public static String TestModelingAssistantProviderMessage;

	//TODO: put accessor fields manually	
}
