package Famille.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;

/**
 * @generated
 */
public class TestDiagramUpdater {

	/**
	 * @generated
	 */
	public static List getSemanticChildren(View view) {
		switch (Famille.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Famille.diagram.edit.parts.MaisonMaisonCompartmentEditPart.VISUAL_ID:
			return getMaisonMaisonCompartment_7001SemanticChildren(view);
		case Famille.diagram.edit.parts.SousFamilleEditPart.VISUAL_ID:
			return getSousFamille_1000SemanticChildren(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getMaisonMaisonCompartment_7001SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.EMPTY_LIST;
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Famille.Maison modelElement = (Famille.Maison) containerView
				.getElement();
		List result = new LinkedList();
		{
			Famille.Cuisine childElement = modelElement.getCuisine();
			int visualID = Famille.diagram.part.TestVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == Famille.diagram.edit.parts.CuisineEditPart.VISUAL_ID) {
				result.add(new Famille.diagram.part.TestNodeDescriptor(
						childElement, visualID));
			}
		}
		for (Iterator it = modelElement.getChambres().iterator(); it.hasNext();) {
			Famille.Chambre childElement = (Famille.Chambre) it.next();
			int visualID = Famille.diagram.part.TestVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == Famille.diagram.edit.parts.ChambreEditPart.VISUAL_ID) {
				result.add(new Famille.diagram.part.TestNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getSousFamille_1000SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.EMPTY_LIST;
		}
		Famille.SousFamille modelElement = (Famille.SousFamille) view
				.getElement();
		List result = new LinkedList();
		for (Iterator it = modelElement.getMaisons().iterator(); it.hasNext();) {
			Famille.Maison childElement = (Famille.Maison) it.next();
			int visualID = Famille.diagram.part.TestVisualIDRegistry
					.getNodeVisualID(view, childElement);
			if (visualID == Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID) {
				result.add(new Famille.diagram.part.TestNodeDescriptor(
						childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static List getContainedLinks(View view) {
		switch (Famille.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Famille.diagram.edit.parts.SousFamilleEditPart.VISUAL_ID:
			return getSousFamille_1000ContainedLinks(view);
		case Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID:
			return getMaison_2001ContainedLinks(view);
		case Famille.diagram.edit.parts.CuisineEditPart.VISUAL_ID:
			return getCuisine_3002ContainedLinks(view);
		case Famille.diagram.edit.parts.ChambreEditPart.VISUAL_ID:
			return getChambre_3001ContainedLinks(view);
		case Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID:
			return getToto_4001ContainedLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getIncomingLinks(View view) {
		switch (Famille.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID:
			return getMaison_2001IncomingLinks(view);
		case Famille.diagram.edit.parts.CuisineEditPart.VISUAL_ID:
			return getCuisine_3002IncomingLinks(view);
		case Famille.diagram.edit.parts.ChambreEditPart.VISUAL_ID:
			return getChambre_3001IncomingLinks(view);
		case Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID:
			return getToto_4001IncomingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getOutgoingLinks(View view) {
		switch (Famille.diagram.part.TestVisualIDRegistry.getVisualID(view)) {
		case Famille.diagram.edit.parts.MaisonEditPart.VISUAL_ID:
			return getMaison_2001OutgoingLinks(view);
		case Famille.diagram.edit.parts.CuisineEditPart.VISUAL_ID:
			return getCuisine_3002OutgoingLinks(view);
		case Famille.diagram.edit.parts.ChambreEditPart.VISUAL_ID:
			return getChambre_3001OutgoingLinks(view);
		case Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID:
			return getToto_4001OutgoingLinks(view);
		}
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getSousFamille_1000ContainedLinks(View view) {
		Famille.SousFamille modelElement = (Famille.SousFamille) view
				.getElement();
		List result = new LinkedList();
		result.addAll(getContainedTypeModelFacetLinks_Toto_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getMaison_2001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getCuisine_3002ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getChambre_3001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getToto_4001ContainedLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getMaison_2001IncomingLinks(View view) {
		Famille.Maison modelElement = (Famille.Maison) view.getElement();
		Map crossReferences = EcoreUtil.CrossReferencer.find(view.eResource()
				.getResourceSet().getResources());
		List result = new LinkedList();
		result.addAll(getIncomingTypeModelFacetLinks_Toto_4001(modelElement,
				crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getCuisine_3002IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getChambre_3001IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getToto_4001IncomingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getMaison_2001OutgoingLinks(View view) {
		Famille.Maison modelElement = (Famille.Maison) view.getElement();
		List result = new LinkedList();
		result.addAll(getOutgoingTypeModelFacetLinks_Toto_4001(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List getCuisine_3002OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getChambre_3001OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	public static List getToto_4001OutgoingLinks(View view) {
		return Collections.EMPTY_LIST;
	}

	/**
	 * @generated
	 */
	private static Collection getContainedTypeModelFacetLinks_Toto_4001(
			Famille.SousFamille container) {
		Collection result = new LinkedList();
		for (Iterator links = container.getTotos().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Famille.Toto) {
				continue;
			}
			Famille.Toto link = (Famille.Toto) linkObject;
			if (Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID != Famille.diagram.part.TestVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Famille.Maison dst = link.getTo();
			Famille.Maison src = link.getFrom();
			result.add(new Famille.diagram.part.TestLinkDescriptor(src, dst,
					link, Famille.diagram.providers.TestElementTypes.Toto_4001,
					Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getIncomingTypeModelFacetLinks_Toto_4001(
			Famille.Maison target, Map crossReferences) {
		Collection result = new LinkedList();
		Collection settings = (Collection) crossReferences.get(target);
		for (Iterator it = settings.iterator(); it.hasNext();) {
			EStructuralFeature.Setting setting = (EStructuralFeature.Setting) it
					.next();
			if (setting.getEStructuralFeature() != Famille.FamillePackage.eINSTANCE
					.getToto_To()
					|| false == setting.getEObject() instanceof Famille.Toto) {
				continue;
			}
			Famille.Toto link = (Famille.Toto) setting.getEObject();
			if (Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID != Famille.diagram.part.TestVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Famille.Maison src = link.getFrom();
			result.add(new Famille.diagram.part.TestLinkDescriptor(src, target,
					link, Famille.diagram.providers.TestElementTypes.Toto_4001,
					Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection getOutgoingTypeModelFacetLinks_Toto_4001(
			Famille.Maison source) {
		Famille.SousFamille container = null;
		// Find container element for the link.
		// Climb up by containment hierarchy starting from the source
		// and return the first element that is instance of the container class.
		for (EObject element = source; element != null && container == null; element = element
				.eContainer()) {
			if (element instanceof Famille.SousFamille) {
				container = (Famille.SousFamille) element;
			}
		}
		if (container == null) {
			return Collections.EMPTY_LIST;
		}
		Collection result = new LinkedList();
		for (Iterator links = container.getTotos().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof Famille.Toto) {
				continue;
			}
			Famille.Toto link = (Famille.Toto) linkObject;
			if (Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID != Famille.diagram.part.TestVisualIDRegistry
					.getLinkWithClassVisualID(link)) {
				continue;
			}
			Famille.Maison dst = link.getTo();
			Famille.Maison src = link.getFrom();
			if (src != source) {
				continue;
			}
			result.add(new Famille.diagram.part.TestLinkDescriptor(src, dst,
					link, Famille.diagram.providers.TestElementTypes.Toto_4001,
					Famille.diagram.edit.parts.TotoEditPart.VISUAL_ID));
		}
		return result;
	}

}
