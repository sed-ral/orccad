#include "interface.h"
#include "Exec/rt.h"
#include "Exec/prr.h"

#include "rts.h"

int prr_reset (ProcedureRobot* prr)
{
  aa_reset();
  aa();
  aa_I_Prr_Start((void*)prr);
  aa();
  return OK; 
}

FUNCPTR TABSIG[NB_SIGNALS] =
{
  (FUNCPTR) aa_I_ROBOT_FAIL,
  (FUNCPTR) aa_I_SOFT_FAIL,
  (FUNCPTR) aa_I_SENSOR_FAIL,
  (FUNCPTR) aa_I_CPU_OVERLOAD,
  (FUNCPTR) f_default,
  (FUNCPTR) f_default,
  (FUNCPTR) aa_I_outbound, 
  (FUNCPTR) aa_I_KillOK_WinX, 
  (FUNCPTR) aa_I_endtraj, 
  (FUNCPTR) aa_I_outwork, 
  (FUNCPTR) aa_I_errtrack, 
  (FUNCPTR) aa_I_ActivateOK_WinX, 
  (FUNCPTR) aa_I_InitOK_WinX, 
  (FUNCPTR) aa_I_badtraj, 
  (FUNCPTR) aa_I_reconf, 
  (FUNCPTR) aa_I_USERSTART_ArmXjmove, 
  (FUNCPTR) aa_I_USERSTART_ArmXcmove, 
  (FUNCPTR) aa_I_ArmXjmove_Start, 
  (FUNCPTR) aa_I_ArmXcmove_Start, 
  (FUNCPTR) aa_I_redbut, 
  (FUNCPTR) aa_I_typetraj, 
  (FUNCPTR) aa_I_CmdStopOK_WinX};

char TABEVENT[NB_SIGNALS][MAXCHARNAME]=
{
 "ROBOT_FAIL",
 "SOFT_FAIL",
 "SENSOR_FAIL",
 "CPU_OVERLOAD",
 "PRECTIMEOUT",
 "COMPUTETIMEOUT", 
 "outbound", 
 "KillOK_WinX", 
 "endtraj", 
 "outwork", 
 "errtrack", 
 "ActivateOK_WinX", 
 "InitOK_WinX", 
 "badtraj", 
 "reconf", 
 "USERSTART_ArmXjmove", 
 "USERSTART_ArmXcmove", 
 "ArmXjmove_Start", 
 "ArmXcmove_Start", 
 "redbut", 
 "typetraj", 
 "CmdStopOK_WinX"};

int aa_O_Activate(void* pt_rt) {
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->OutAuto(O_ACTIVATE);
    return OK;
}
int aa_O_Normal(void* pt_rt) {
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->OutAuto(O_NORMAL);
    return OK;
}
int aa_O_Transite(void* pt_rt) {
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->PutState(TRANSITE);
    RT->OutAuto(O_TRANSITE);
    return OK;
}
int aa_O_EndParam(void* pt_rt) {
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->PutState(REPARAM);
    RT->OutAuto(O_END);
    return OK;
}
int aa_O_EndKill(void* pt_rt) {
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->OutAuto(O_END);
    return OK;
}
int aa_O_FinBF(void* pt_prr) {
    ProcedureRobot *prr = (ProcedureRobot *) pt_prr;
    prr->EmitEnd();
    return OK;
}
int aa_O_FinT3(void* pt_prr) {
    ProcedureRobot *prr = (ProcedureRobot *) pt_prr;
    prr->EmitUrgentEnd();
    return OK;
}
int f_default() {return OK;}
int aa_O_GoodEndRPr() {return OK;}
int aa_O_T3RPr() {return OK;}


extern "C" { 
int aa_O_Abort_ArmXjmove(void*) {return 1;}
int aa_O_ActivateArmXjmove_WinX(void* pt_rt) {
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->OutAuto(O_ACTIVATE);
    return OK;
}
int aa_O_ArmXjmoveTransite(void* pt_rt) {
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->PutState(TRANSITE);
    RT->OutAuto(O_TRANSITE);
    return OK;
}
int aa_O_STARTED_ArmXjmove() {return OK;} 
int aa_O_GoodEnd_ArmXjmove() {return OK;} 
int aa_O_Treattypetraj(void* pt_rt) {
    orc_RT_ArmXjmove *RT = (orc_RT_ArmXjmove *) pt_rt;
    RT->aut_output_typetraj = 1; 
     return OK;
}
void ArmXjmove_parameter(void* pt_rt, char * char_joint)
{
   orc_RT_ArmXjmove *RT = (orc_RT_ArmXjmove *)  pt_rt; 
   double joint[2];
   sscanf(char_joint,"%lf %lf ",&joint[0],&joint[1]);
   RT->SetParam(joint);
}

void ArmXjmove_controler(void* pt_rt)
{
   orc_RT_ArmXjmove *RT = (orc_RT_ArmXjmove *)  pt_rt; 
   RT->controler(); 
}

int aa_O_Abort_ArmXcmove(void*) {return 1;}
int aa_O_ActivateArmXcmove_WinX(void* pt_rt) {
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->OutAuto(O_ACTIVATE);
    return OK;
}
int aa_O_ArmXcmoveTransite(void* pt_rt) {
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->PutState(TRANSITE);
    RT->OutAuto(O_TRANSITE);
    return OK;
}
int aa_O_STARTED_ArmXcmove() {return OK;} 
int aa_O_GoodEnd_ArmXcmove() {return OK;} 
void ArmXcmove_parameter(void* pt_rt)
{
   orc_RT_ArmXcmove *RT = (orc_RT_ArmXcmove *)  pt_rt; 
   RT->SetParam();
}

void ArmXcmove_controler(void* pt_rt)
{
   orc_RT_ArmXcmove *RT = (orc_RT_ArmXcmove *)  pt_rt; 
   RT->controler(); 
}

int aa_O_STARTED_Proccmove() {return OK;}
int aa_O_GoodEnd_Proccmove() {return OK;}
int aa_O_Abort_Proccmove() {return OK;}
} // extern "C" 
