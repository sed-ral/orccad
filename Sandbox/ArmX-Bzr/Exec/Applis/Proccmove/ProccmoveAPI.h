//
// This file defines the API functions 
// to interfere with the generated Orccad program 
//
// FOR SOLARIS USERS:
// You can use it including it in the main program
// launching the Orccad run time tasks:
// e.g: 
// 	 #include "ProccmoveAPI.h"
// 	 main() { 
// 	 orcProccmoveInit();
// 	 orcProccmoveLaunch();
// 	 // Launch task calling the others API Functions 
// 	 // ...add code here...
// 	 }
//
#ifndef __orc_ProccmoveApi_h
#define __orc_ProccmoveApi_h

// C function Interfaces
extern "C" { 
extern int orcProccmoveInit();
extern int orcProccmoveLaunch();
extern int orcProccmoveTaskLaunch();
extern int orcProccmoveGo();
extern int orcProccmoveStop();
extern int orcArmXjmovePrm(double joint[2]);
} // extern "C"

#endif 
