//----------------------------------------------------------
// Module cobs
//----------------------------------------------------------
#ifndef __orc_Mod_cobs_h
#define __orc_Mod_cobs_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"

#include "module_cobs_Inc.h"

class orc_Mod_cobs: public ModuleAlgo { 
protected: 
	// Internal Variables 
// Orccad Version: 3.0 alpha
// Module : jobs
// Variables declaration File
// Date of creation : Mon Nov  3 13:23:55 1997



public: 
	orc_Mod_cobs(ModuleTask *mt,int indexclk):
	  ModuleAlgo("orc_Mod_cobs",mt,indexclk) {};
	~orc_Mod_cobs() {};
	 // Output Ports declaration
	 // Output Event Ports declaration
	 int outbound;
	 int redbut;
	 // Output param Ports declaration

	 // Methods of computation 
	 void init(const int limit[2], const int &key); 
	 void param(); 
	 void reparam(); 
	 void compute(const int limit[2], const int &key ); 
	 void end(); 
 }; 
#endif 
// End class  orc_Mod_cobs

