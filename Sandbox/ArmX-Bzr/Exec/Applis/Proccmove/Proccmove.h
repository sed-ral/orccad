#ifndef __orc_Proccmove_h
#define __orc_Proccmove_h

#include "Exec/utils.h"
#include "Exec/prr.h"

#include "interface.h"
#include "rts.h"

#include "PhR_WinX.h"
#define NBCLKS 1
#define BASECLK 0.1

class orc_Proccmove:public ProcedureRobot {
protected: 
   // Index Tab for clocks
   int *TabClks;
   // Physical Ressources
   orc_PhR_WinX *orcWinX;
public:
   // Robot-Tasks
   orc_RT_ArmXjmove *ArmXjmove;
   orc_RT_ArmXjmove *GetRTArmXjmovePtr() { return ArmXjmove;}
   orc_RT_ArmXcmove *ArmXcmove;
   orc_RT_ArmXcmove *GetRTArmXcmovePtr() { return ArmXcmove;}
   orc_Proccmove();
   ~orc_Proccmove();
   void Launch();
}; 

#endif 
// End class orc_Proccmove
