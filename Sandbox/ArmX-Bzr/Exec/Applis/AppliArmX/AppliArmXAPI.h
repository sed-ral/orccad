//
// This file defines the API functions 
// to interfere with the generated Orccad program 
//
// FOR SOLARIS USERS:
// You can use it including it in the main program
// launching the Orccad run time tasks:
// e.g: 
// 	 #include "AppliArmXAPI.h"
// 	 main() { 
// 	 orcAppliArmXInit();
// 	 orcAppliArmXLaunch();
// 	 // Launch task calling the others API Functions 
// 	 // ...add code here...
// 	 }
//
#ifndef __orc_AppliArmXApi_h
#define __orc_AppliArmXApi_h

// C function Interfaces
extern "C" { 
extern int orcAppliArmXInit();
extern int orcAppliArmXLaunch();
extern int orcAppliArmXTaskLaunch();
extern int orcAppliArmXGo();
extern int orcAppliArmXStop();
extern int orcAppliArmXSendUserStopEvt();
extern int orcArmXjmovePrm(double joint[2]);
} // extern "C"

#endif 
