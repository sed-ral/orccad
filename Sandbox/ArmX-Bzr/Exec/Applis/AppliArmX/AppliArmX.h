#ifndef __orc_AppliArmX_h
#define __orc_AppliArmX_h

#include "Exec/utils.h"
#include "Exec/prr.h"

#include "interface.h"
#include "rts.h"

#include "PhR_WinX.h"
#define NBCLKS 1
#define BASECLK 0.1

class orc_AppliArmX:public ProcedureRobot {
protected: 
   // Index Tab for clocks
   int *TabClks;
   // Physical Ressources
   orc_PhR_WinX *orcWinX;
public:
   // Robot-Tasks
   orc_RT_ArmXjmove *ArmXjmove;
   orc_RT_ArmXjmove *GetRTArmXjmovePtr() { return ArmXjmove;}
   orc_RT_ArmXfmove *ArmXfmove;
   orc_RT_ArmXfmove *GetRTArmXfmovePtr() { return ArmXfmove;}
   orc_RT_ArmXcmove *ArmXcmove;
   orc_RT_ArmXcmove *GetRTArmXcmovePtr() { return ArmXcmove;}
   orc_AppliArmX();
   ~orc_AppliArmX();
   void Launch();
}; 

#endif 
// End class orc_AppliArmX
