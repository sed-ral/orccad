#ifndef __interface_h
#define __interface_h

#include "Exec/canal.h"

enum INPUT_SIGNALS_USER {
  outbound = COMPUTETIMEOUT + 1,  
  KillOK_WinX,  
  endtraj,  
  errtrack,  
  ActivateOK_WinX,  
  InitOK_WinX,  
  badtraj,  
  USERSTART_ArmXjmove,  
  ArmXjmove_Start,  
  redbut,  
  typetraj,  
  CmdStopOK_WinX, 
  NB_SIGNALS
};

extern "C" { int aa();}
extern "C" { int aa_reset ();}
extern "C" { int prr_reset (ProcedureRobot*);}
extern "C" { 
  extern FUNCPTR TABSIG[NB_SIGNALS];

  FUNCPTR aa_I_outbound();
  FUNCPTR aa_I_KillOK_WinX();
  FUNCPTR aa_I_endtraj();
  FUNCPTR aa_I_errtrack();
  FUNCPTR aa_I_ActivateOK_WinX();
  FUNCPTR aa_I_InitOK_WinX();
  FUNCPTR aa_I_badtraj();
  FUNCPTR aa_I_USERSTART_ArmXjmove();
  FUNCPTR aa_I_ArmXjmove_Start();
  FUNCPTR aa_I_redbut();
  FUNCPTR aa_I_typetraj();
  FUNCPTR aa_I_CmdStopOK_WinX();
  FUNCPTR aa_I_Prr_Start(void*);
  FUNCPTR aa_I_ROBOT_FAIL();
  FUNCPTR aa_I_SOFT_FAIL();
  FUNCPTR aa_I_SENSOR_FAIL();
  FUNCPTR aa_I_CPU_OVERLOAD();
  int aa_O_ArmXjmoveTransite(void*);
  int aa_O_ActivateArmXjmove_WinX(void*);
  int aa_O_Treattypetraj(void*);
  int aa_O_Abort_ArmXjmove(void*);
  int aa_O_Activate(void*);
  int aa_O_Transite(void*);
  int aa_O_EndParam(void*);
  int aa_O_EndKill(void*);
  int aa_O_EndObs(void*);
  int aa_O_FinBF(void*);
  int aa_O_FinT3(void*);
  int aa_O_GoodEndRPr();
  int aa_O_T3RPr();
  int f_default();
} // end extern 

extern char TABEVENT[NB_SIGNALS][MAXCHARNAME];

#endif
