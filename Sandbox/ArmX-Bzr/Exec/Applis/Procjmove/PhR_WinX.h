//----------------------------------------------------------
// Physical Resource WinX
//----------------------------------------------------------

#ifndef __phr_WinX_h
#define __phr_WinX_h

#include "Exec/phr.h"

#include "module_WinX_Inc.h"

class orc_PhR_WinX : public PhR {
private:
// Orccad Version: 3.0 alpha
// Module : WinX
// Variables declaration File
// Date of creation : Mon Nov  3 13:16:03 1997



public: 
   orc_PhR_WinX(ProcedureRobot *prr, char *ch ,int a, int b,int c ,int d) : PhR (prr,ch,a,b,c,d) {}; 
	 // Output Ports declaration
	 double pos[2];
	 int key;
	 int limit[2];
	 double sensor[2];
	 // Event Ports declaration
	 // Methods to call functions driver
	 void PUT_torque(const double torque[2]);
	 void GET_pos();
	 void GET_key();
	 void GET_limit();
	 void GET_sensor();
	 // Methods to control the driver mode
	 void reinit(); 
	 void param(); 
	 int Init(); 
	 int Close(); 
};

#endif 
// End of Physical Resource WinX

