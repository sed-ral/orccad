//----------------------------------------------------------
// Module command
//----------------------------------------------------------

#include "module_command.h"
#include "Exec/prr.h"


void orc_Mod_command::init(const double pose[2], const double pos[2])
{
// Orccad Version: 3.0 alpha
// Module : command
// Initialisation File
//
// Module Ports :
// 	input DOUBLE posi[2]
// 	input DOUBLE pos[2]
// 	output DOUBLE torque[2]
//
// Date of creation : Tue Mar 16 19:12:49 1999

for(index=0;index<2;index++)
  torque[index] = 0.0;

} 
void orc_Mod_command::param()
{
	 plugParam();



} 
void orc_Mod_command::reparam()
{

} 
void orc_Mod_command::compute(const double pose[2], const double pos[2] )
{
// Orccad Version: 3.0 alpha   
// Module : command
// Computation File 
//
// Module Ports :
// 	input DOUBLE posi[2]
// 	input DOUBLE pos[2]
// 	output DOUBLE torque[2]
//
// Date of creation : Tue Mar 16 19:12:49 1999

// Gravity compensation
torque[0] = ARM_M2*ARM_L2*ARM_GRAV*cos(pos[0]+pos[1]) 
            + (ARM_M1+ARM_M2)*ARM_L1*ARM_GRAV*cos(pos[0]);
torque[1] = ARM_M2*ARM_L2*ARM_GRAV*cos(pos[0]+pos[1]);

// Proportionnal correction
torque[0] = KX*pose[0] + torque[0];
torque[1] = KY*pose[1] + torque[1];

} 
void orc_Mod_command::end()
{
// Orccad Version: 3.0 alpha
// Module :  command
// End File 
//
// Module Ports :
// 	input DOUBLE posi[2]
// 	input DOUBLE pos[2]
// 	output DOUBLE torque[2]
//
// Date of creation : Tue Mar 16 19:12:49 1999


} 
// End class orc_Mod_command

