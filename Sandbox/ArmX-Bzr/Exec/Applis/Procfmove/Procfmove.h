#ifndef __orc_Procfmove_h
#define __orc_Procfmove_h

#include "Exec/utils.h"
#include "Exec/prr.h"

#include "interface.h"
#include "rts.h"

#include "PhR_WinX.h"
#define NBCLKS 1
#define BASECLK 0.1

class orc_Procfmove:public ProcedureRobot {
protected: 
   // Index Tab for clocks
   int *TabClks;
   // Physical Ressources
   orc_PhR_WinX *orcWinX;
public:
   // Robot-Tasks
   orc_RT_ArmXjmove *ArmXjmove;
   orc_RT_ArmXjmove *GetRTArmXjmovePtr() { return ArmXjmove;}
   orc_RT_ArmXfmove *ArmXfmove;
   orc_RT_ArmXfmove *GetRTArmXfmovePtr() { return ArmXfmove;}
   orc_Procfmove();
   ~orc_Procfmove();
   void Launch();
}; 

#endif 
// End class orc_Procfmove
