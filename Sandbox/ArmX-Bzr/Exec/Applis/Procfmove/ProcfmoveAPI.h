//
// This file defines the API functions 
// to interfere with the generated Orccad program 
//
// FOR SOLARIS USERS:
// You can use it including it in the main program
// launching the Orccad run time tasks:
// e.g: 
// 	 #include "ProcfmoveAPI.h"
// 	 main() { 
// 	 orcProcfmoveInit();
// 	 orcProcfmoveLaunch();
// 	 // Launch task calling the others API Functions 
// 	 // ...add code here...
// 	 }
//
#ifndef __orc_ProcfmoveApi_h
#define __orc_ProcfmoveApi_h

// C function Interfaces
extern "C" { 
extern int orcProcfmoveInit();
extern int orcProcfmoveLaunch();
extern int orcProcfmoveTaskLaunch();
extern int orcProcfmoveGo();
extern int orcProcfmoveStop();
extern int orcArmXjmovePrm(double joint[2]);
} // extern "C"

#endif 
