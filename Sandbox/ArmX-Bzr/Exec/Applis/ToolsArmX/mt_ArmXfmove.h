#ifndef __mt_ArmXfmove_h
#define __mt_ArmXfmove_h

#include "Exec/mt.h"

#include "module_command.h"
#include "module_error.h"
#include "module_ctraj.h"
#include "module_sens2th.h"
#include "module_cobs.h"
#include "PhR_WinX.h"
#include "module_fsecure.h"

class RobotTask; 
class orc_Mt_ArmXfmove;
extern void fct_ArmXfmove(orc_Mt_ArmXfmove*);

class orc_Mt_ArmXfmove: public ModuleTask
{
	 friend void fct_ArmXfmove(orc_Mt_ArmXfmove*);
protected:
	 orc_Mod_command command_3;
	 orc_Mod_error error_4;
	 orc_Mod_ctraj ctraj_5;
	 orc_Mod_sens2th sens2th_6;
	 orc_Mod_cobs cobs_8;
	 orc_PhR_WinX* WinX;
public: 
	 orc_Mod_fsecure fsecure_7;
	 void InitTrace();
public:
#ifndef SIMPARC_RUN
	 orc_Mt_ArmXfmove(RobotTask *rt, char *name, int itFilter)
	 :ModuleTask((FUNCPTR) fct_ArmXfmove,rt,name,itFilter,0), 
#else
	 orc_Mt_ArmXfmove(RobotTask *rt, char *name, int itFilter)
	 :ModuleTask((VOIDFUNCPTRVOID) fct_ArmXfmove,rt,name,itFilter,0), 
#endif /* SIMPARC_RUN */
	 command_3(this,0)
	 ,error_4(this,0)
	 ,ctraj_5(this,0)
	 ,sens2th_6(this,0)
	 ,cobs_8(this,0)
	 ,fsecure_7()
	 {
	    InitTrace();
#ifdef SIMPARC_RUN
	command_3.vxwSet(vxwGet());
	error_4.vxwSet(vxwGet());
	ctraj_5.vxwSet(vxwGet());
	sens2th_6.vxwSet(vxwGet());
	cobs_8.vxwSet(vxwGet());
	fsecure_7.vxwSet(vxwGet());
#endif /* SIMPARC_RUN */
	    WinX = (orc_PhR_WinX*) RT->GetPhR();
	 }
	 ~orc_Mt_ArmXfmove(){};
};
#endif
