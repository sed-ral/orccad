//
// Module changetool
//
#ifndef __module_changetool_Inc_h
#define __module_changetool_Inc_h

// Orccad Version: 3.0 alpha   
// Module : trajectory
// Include and Definition File
// Date of creation : Mon Nov  3 13:05:11 1997

#include <math.h>

#define LIMITMAX   M_PI
#define LIMITMIN  -M_PI
#define STEP_POS   0.1
#define EPSILON    0.01
 // arm position for changing tool
#define JOINT1_TOOL 0.0
#define JOINT2_TOOL 0.0

#define MODE_SUSPEND 0
#define MODE_ACTIVE  1


#endif
