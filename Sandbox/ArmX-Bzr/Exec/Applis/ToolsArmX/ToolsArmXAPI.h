//
// This file defines the API functions 
// to interfere with the generated Orccad program 
//
// FOR SOLARIS USERS:
// You can use it including it in the main program
// launching the Orccad run time tasks:
// e.g: 
// 	 #include "ToolsArmXAPI.h"
// 	 main() { 
// 	 orcToolsArmXInit();
// 	 orcToolsArmXLaunch();
// 	 // Launch task calling the others API Functions 
// 	 // ...add code here...
// 	 }
//
#ifndef __orc_ToolsArmXApi_h
#define __orc_ToolsArmXApi_h

// C function Interfaces
extern "C" { 
extern int orcToolsArmXInit();
extern int orcToolsArmXLaunch();
extern int orcToolsArmXTaskLaunch();
extern int orcToolsArmXGo();
extern int orcToolsArmXStop();
extern int orcToolsArmXSendUserStopEvt();
extern int orcArmXjmovePrm(double joint[2]);
} // extern "C"

#endif 
