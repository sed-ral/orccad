#ifndef __mt_ArmXchangetool_h
#define __mt_ArmXchangetool_h

#include "Exec/mt.h"

#include "module_error.h"
#include "module_changetool.h"
#include "module_command.h"
#include "module_jobs.h"
#include "PhR_WinX.h"
#include "module_jsecure.h"

class RobotTask; 
class orc_Mt_ArmXchangetool;
extern void fct_ArmXchangetool(orc_Mt_ArmXchangetool*);

class orc_Mt_ArmXchangetool: public ModuleTask
{
	 friend void fct_ArmXchangetool(orc_Mt_ArmXchangetool*);
protected:
	 orc_Mod_error error_5;
	 orc_Mod_changetool changetool_8;
	 orc_Mod_command command_9;
	 orc_Mod_jobs jobs_11;
	 orc_PhR_WinX* WinX;
public: 
	 orc_Mod_jsecure jsecure_10;
	 void InitTrace();
public:
#ifndef SIMPARC_RUN
	 orc_Mt_ArmXchangetool(RobotTask *rt, char *name, int itFilter)
	 :ModuleTask((FUNCPTR) fct_ArmXchangetool,rt,name,itFilter,0), 
#else
	 orc_Mt_ArmXchangetool(RobotTask *rt, char *name, int itFilter)
	 :ModuleTask((VOIDFUNCPTRVOID) fct_ArmXchangetool,rt,name,itFilter,0), 
#endif /* SIMPARC_RUN */
	 error_5(this,0)
	 ,changetool_8(this,0)
	 ,command_9(this,0)
	 ,jobs_11(this,0)
	 ,jsecure_10()
	 {
	   InitTrace();
#ifdef SIMPARC_RUN
	error_5.vxwSet(vxwGet());
	changetool_8.vxwSet(vxwGet());
	command_9.vxwSet(vxwGet());
	jobs_11.vxwSet(vxwGet());
	jsecure_10.vxwSet(vxwGet());
#endif /* SIMPARC_RUN */
	    WinX = (orc_PhR_WinX*) RT->GetPhR();
	 }
	 ~orc_Mt_ArmXchangetool(){};
};
#endif
