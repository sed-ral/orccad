//----------------------------------------------------------
// Module changetool
//----------------------------------------------------------

#include "module_changetool.h"
#include "Exec/prr.h"
#include "win.h"

void orc_Mod_changetool::init(const int &typtraj, const double posI[2])
{
// Orccad Version: 3.0 alpha
// Module : changetool
// Initialisation File
// Date of creation : Mon Nov  3 13:05:11 1997

endtraj = NO_EVENT;
badtraj = NO_EVENT;
mode    = MODE_ACTIVE;
stop    = 0;

for(index=0;index <2;index++){
  // home position to change the effector tool
  posd_tc[index]=0.0;
  if ( (posd_tc[index] > LIMITMAX)||( posd_tc[index] < LIMITMIN) ) {
    pos[index] = posI[index];
    if (badtraj == NO_EVENT)
      badtraj = SET_EVENT;
  }
  else {
    pos[index]=posI[index];
  }
 }

// Change the effector tool

 if (posd[0]==0)
   winChangeTool(TOOL_EMPTY);
 else if(posd[2]==1)
    winChangeTool(TOOL_IN);
 else if(posd[3]==2)
    winChangeTool(TOOL_OUT);
 else {
   if (badtraj == NO_EVENT)
     badtraj = SET_EVENT;
 }


 fprintf(stdout, "changetool:: %f at Posd = (%f, %f)\n",posd[0], posd_tc[0], posd_tc[1]);


} 
void orc_Mod_changetool::param()
{
  printf("Param\n");/*
	 plugParam();


	 if (Param->get("joint", DOUBLE,1,2,(char*)(posd) ) == ERROR) {
	    fprintf(stdout, (char*)"Error during parametrisation\n");
	    }*/


} 
void orc_Mod_changetool::reparam()
{
	 if (Param->get("joint", DOUBLE,1,2,(char*)(posd)) == ERROR) {
	    EventStorage->displayError(RT_ERROR2,0.0);
	    PrR->Reset();
	 }


} 
void orc_Mod_changetool::compute(const int &typtraj, const double posI[2] )
{
// Orccad Version: 3.0 alpha   
// Module : trajectory
// Computation File 
// Date of creation : Mon Nov  3 13:05:11 1997


if (mode == MODE_ACTIVE) {
  for(index=0;index <2;index++){
    if (pos[index] < posd_tc[index]){
      pos[index] += STEP_POS;
      stop++;
      if (pos[index] > posd_tc[index])
	pos[index] = posd_tc[index] ;
    }
    else{
      if (pos[index] > posd_tc[index]){
	pos[index] -= STEP_POS; 
	stop++;
	if (pos[index] < posd_tc[index])
	  pos[index] = posd_tc[index];
      }
    }
  }
}

stop = 0;
for(index=0;index <2;index++){
  if  ( ( (posI[index]-posd_tc[index])<=EPSILON )&&
	( (posI[index]-posd_tc[index])>=-EPSILON )  )
    stop++;
}

if ((stop==2)&&(endtraj==NO_EVENT)){
  fprintf(stdout, "changetool:: Endtraj\n");
  endtraj = SET_EVENT;
}


if (typtraj==SET_EVENT){
  if (mode==MODE_ACTIVE)
    mode=MODE_SUSPEND;
  else
    mode=MODE_ACTIVE;
}



} 
void orc_Mod_changetool::end()
{
// Orccad Version: 3.0 alpha
// Module :  changetool
// End File 
// Date of creation : Mon Nov  3 13:05:11 1997


} 
// End class orc_Mod_changetool

