#ifndef __orc_ToolsArmX_h
#define __orc_ToolsArmX_h

#include "Exec/utils.h"
#include "Exec/prr.h"

#include "interface.h"
#include "rts.h"

#include "PhR_WinX.h"
#define NBCLKS 1
#define BASECLK 0.1

class orc_ToolsArmX:public ProcedureRobot {
protected: 
   // Index Tab for clocks
   int *TabClks;
   // Physical Ressources
   orc_PhR_WinX *orcWinX;
public:
   // Robot-Tasks
   orc_RT_ArmXjmove *ArmXjmove;
   orc_RT_ArmXjmove *GetRTArmXjmovePtr() { return ArmXjmove;}
   orc_RT_ArmXchangetool *ArmXchangetool;
   orc_RT_ArmXchangetool *GetRTArmXchangetooltr() { return ArmXchangetool;}
   orc_RT_ArmXfmove *ArmXfmove;
   orc_RT_ArmXfmove *GetRTArmXfmovePtr() { return ArmXfmove;}
   orc_RT_ArmXcmove *ArmXcmove;
   orc_RT_ArmXcmove *GetRTArmXcmovePtr() { return ArmXcmove;}
   orc_ToolsArmX();
   ~orc_ToolsArmX();
   void Launch();
}; 

#endif 
// End class orc_ToolsArmX
