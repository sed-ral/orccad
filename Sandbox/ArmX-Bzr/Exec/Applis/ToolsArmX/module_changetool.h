//----------------------------------------------------------
// Module changetool
//----------------------------------------------------------
#ifndef __orc_Mod_changetool_h
#define __orc_Mod_changetool_h

#include "Exec/mt.h"
#include "Exec/rt.h"
#include "Exec/module.h"

#include "module_changetool_Inc.h"

class orc_Mod_changetool: public ModuleAlgo { 
protected: 
	// Internal Variables 
// Orccad Version: 3.0 alpha
// Module : trajectory
// Variables declaration File
// Date of creation : Mon Nov  3 13:05:11 1997

int index;
int mode;
int stop;
 double posd_ct[2];

public: 
	orc_Mod_changetool(ModuleTask *mt,int indexclk):
 ModuleAlgo("orc_Mod_changetool",mt,indexclk) {
   // arm position for changing tool
   posd_ct[0]=JOINT1_TOOL; 
   posd_ct[1]=JOINT2_TOOL;
   };
	~orc_Mod_changetool() {};
	 // Output Ports declaration
	 double pos[2];
	 // Output Event Ports declaration
	 int endtraj;
	 int badtraj;
	 // Output param Ports declaration
	 double posd[2];

	 // Methods of computation 
	 void init(const int &typtraj, const double posI[2]); 
	 void param(); 
	 void reparam(); 
	 void compute(const int &typtraj, const double posI[2] ); 
	 void end(); 
 }; 
#endif 
// End class  orc_Mod_changetool

