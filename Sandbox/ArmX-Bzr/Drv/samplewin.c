/********************************  Orccad ************************************
*
* File           : Samples/Misc/Drv/samplewin.c
* Author         : Roger Pissard-Gibollet and Nicolas Turro
* Version        : 3.1
* Creation       : 12 March  1998
*
****************************** Description **********************************
*
*   Example of use of the API to simulate a robot arm with 2 d.o.f
*   without using Orccad.
*
***************************** Modifications *********************************
*
*
*****************************************************************************
* (c) Copyright 1998, INRIA, all rights reserved
*****************************************************************************/
#include "win.h"
#include <stdlib.h>

void * sampleControl(void *arg)
{
  double couple[2],pos[2],posd[2],posi[2],spos[2],xd[2];
  double k1,k2,c12,s12;
  int end = FALSE, free=FALSE;
  enum  ToolType tool;
  char key,oldkey=0;
    
  posd[0] = 0.0; posd[1] = 0.0;
  posi[0] = 0.0; posi[1] = 0.0;
  xd[0] =  0.0 ; xd[1] =  0.0 ;

  while (end == FALSE){
    usleep(100 MS);

    winGetPos(pos);
    winGetSensor(spos);
    key = winKey();
    /* Choose the mode */
    switch(key)
      {
      case 'q':
	end = TRUE;
	break; 
  
      case '0':
	winChangeTool(TOOL_EMPTY);
	printf("Changement d'outils: vide\n");
	break;
      case '1':
	winChangeTool(TOOL_IN);
	printf("Changement d'outils: TOOL_IN\n");
	break;
      case '2':
	winChangeTool(TOOL_OUT);
	printf("Changement d'outils: TOOL_OUT\n");
	break;
	
      case 'S':
	/* Free mode */
	posd[0] = pos[0];
	posd[1] = pos[1];
	couple[0] = 0.0;
	couple[1] = 0.0;
	free=TRUE;
	printf("Mode Robot Libre\n");
	break;
      case 'c':

	/* Check if the order is out of range */
	/* Compute xd with the mouse position */
	k1    = spos[0] + L1*cos(pos[1]) + L2;
	k2    = spos[1] - L1*sin(pos[1]);;
	s12   = sin(pos[0]+pos[1]);
	c12   = cos(pos[0]+pos[1]);
	xd[0] = c12*k1 - s12*k2;
	xd[1] = s12*k1 + c12*k2;

	k1 = sqrt(xd[0]*xd[0] + xd[1]*xd[1]);
	if ( (xd[0] == 0.0)&&(xd[1] == 0.0))
	  printf("xd must not be null\n");
	else
	  if  (k1 > (L1+L2) ){
	    /* Pointage */
	    posd[1] = 0.0;
	    posd[0] = atan2(spos[1],L1+L2+spos[0]) + pos[0];
	    printf("Mode Pointage sur %g\n",spos[0]);
	  }
	  else{	
	    /* Cartesian Position p 127 */
	    k1 = (xd[0]*xd[0] + xd[1]*xd[1] - L1*L1 - L2*L2)/(2*L1*L2);
	    posd[1] = acos(k1);
	    if (posd[1] > 0)
	      posd[0] = - posd[0];
	    k1 = atan2(xd[1],xd[0]);
	    k2 = (xd[0]*xd[0] + xd[1]*xd[1] + L1*L1 - L2*L2);
	    k2 = k2/(2*L1*L1*sqrt(xd[0]*xd[0] + xd[1]*xd[1]));
	    k2 = acos(k2);
	  
	    if (k2 < 0)
	      posd[0] = k1 + k2;
	    else
	      posd[0] = k1 - k2;
	    printf("Mode Cartesien sur xd = %g yd= %g\n",xd[0],xd[1]);
	  }
	
	free=FALSE;
	break;
      case 'j': 
	posd[0] = M_PI/4;
	posd[1] = M_PI/4;
	printf("Mode Articulaire q1 = %g q2 = %g\n",posd[0],posd[1]);
	free=FALSE;
	break;
      }
    /* trajectory generation*/
    if (posi[0] < posd[0]) 
      posi[0] += 0.05;
    else
      if (posi[0] > posd[0]) 
	posi[0] -=0.05;

    if (posi[1] < posd[1]) 
      posi[1] += 0.05;
    else
      if (posi[1] > posd[1]) 
	posi[1] -=0.05;
    /* PD gravity compensation */
    if (free==FALSE){
      couple[0] = M2*L2*GRAV*cos(pos[0]+pos[1]) 
	+ (M1+M2)*L1*GRAV*cos(pos[0]);
      couple[1] = M2*L2*GRAV*cos(pos[0]+pos[1]);
      couple[0] = 20*(posi[0]-pos[0]) + couple[0];
      couple[1] = 20*(posi[1]-pos[1]) + couple[1];
    }
    else{
      couple[0] = 0.0;
      couple[1] = 0.0;
    }

    winPutTau(couple);
    printf("Robot joint= %g %g, torque= %g %g \n",
	   posi[0]-pos[0],posi[1]-pos[1],couple[0],couple[1]);
  }

  winKill();

  return (0);
}

int main()
{
  int n;
  /* thread_t tid; */
  pthread_t tid;

  if (winInit(450,450)== ERROR){
    fprintf(stderr, "winInit::Cannot window initialize\n");
    exit(1);
  }

  /* Launch the control     */
  if (n = pthread_create(&tid, NULL, sampleControl,NULL)) {
    fprintf( stderr,"win::Cannot create thread: %s\n",strerror(n));
    exit(1);
  }  
  /* Wait end of the control */
  pthread_join(tid, NULL);
  return(OK);
}
