/********************************  Orccad ************************************
*
* File           : Samples/Misc/Drv/win.h
* Author         : Roger Pissard-Gibollet and Nicolas Turro
* Version        : 3.0 alpha
* Creation       : 12 March  1998
*
****************************** Description **********************************
*
*  Header of an API to simulate a robot arm with 2 d.o.f
*
***************************** Modifications *********************************
*
*
*****************************************************************************
* (c) Copyright 1998, INRIA, all rights reserved
*****************************************************************************/

#ifndef __win_h
#define __win_h
#include <stdio.h>
#include <math.h>
#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>
#include <X11/X.h>
#include <unistd.h>
#include <string.h>
#include <malloc.h>
#include <pthread.h>
#include <assert.h>

/*
Configuration of the application 
*/
/* size of the windows              */
#define XMAX      450
#define YMAX      450
/* Graphic elements                 */
/* ssize of the target */
#define DIMTARGET 10
/* joint circle radius */
#define RAXIS     8    
/* grip lenght         */
#define LGRIP     10   
/* 100 pixels = 1 meter             */
#define WPIXMET   100.0    
/* robot description                */
#define NBAXIS    2
#define L1        1.0
#define L2        1.0
#define M1        10.0
#define M2        5.0
/* friction coefficients            */
#define FRIC1     50.0
#define FRIC2     40.0
#define GRAV      9.81
/* Limits                           */
#define LIMITMAX  M_PI
#define LIMITMIN  -M_PI
#define TORQUEMAX 1000.0
/* Graphic Robot Frame Definition   */
#define RBASEX     g.winX/2
#define RBASEY     g.winY/2
/* simulator rate (unit:second) */
/* any DT smaller than 20 ms will not be taken into account by linux */
#define DT        0.01
#define MS        *1000
#define MICROS    *1000000
/* this gives 25 fps */
#define TIMETODISPLAY 2 
/*
Standard definition
*/
#define TRUE      1
#define FALSE     0
#define OK        0
#define ERROR     -1
#define TMPSTRLEN 10
#define STRLEN    80

enum ToolType {TOOL_EMPTY,TOOL_IN,TOOL_OUT}; 


/*
Definition of the structure which maintains
the state of the application
*/
typedef struct GINFO{
  /* Display         */
  Display  *disp;
  char     *dispname;
  /* Graphics        */
  GC       gc;
  Pixmap db;
  long black, white;
  /* Window          */
  Window   win;
  int screen;
  /* Window Geometry */
  int dispX, dispY, winX, winY;
  /* Window Event    */
  XEvent event;
  int    end;
  int    refresh;
  char   key;
  int    mousex;
  int    mousey;
  /* Axis Position   */
  double pos[NBAXIS];
  int    limit[NBAXIS];
  /* input control   */
  double tau[NBAXIS];
  /* Sensor values   */
  double sensorx;
  double sensory;  
  /* Type of the arm tool */
  enum ToolType armTool;
  /* internal velocity and acceleration */
  double t_d[NBAXIS];
  double t_dd[NBAXIS];
  /* Thread Info     */ 
  pthread_t       tid;
} Ginfo;

/*
User functions exportation
*/
#ifdef __cplusplus
extern "C" {
#endif

/* Function Name : winInit = initialize window
   parameters    : int x   = x size of the window
                   int y   = y size of the window
   return        : OK/ERROR 
 */
extern int  winInit(int x, int y);

/* Function Name : winkill = destroy window
   return        : OK
 */ 
extern int  winKill();

/* Function Name : winKey = initialize window
   return        : the key write on keyboard (the mouse must be on window)
 */
extern char winKey();

/* Function Name : winPutTau = inputs the torque on robot
   parameters    : const double tau[NBAXIS]  =  torque input (Nm)
   return        : OK
 */
extern int  winPutTau(const double tau[NBAXIS]);

/* Function Name : winGetPos = gives the values of robot articulation
   parameters    : double pos[NBAXIS]  = robot articulation (radians)
   return        : OK
 */
extern int  winGetPos(double pos[NBAXIS]);

/* Function Name : winGetLimit = gives if limit articulation are reached
   parameters    : double limit[NBAXIS]  = limits (TRUE/FALSE)
   return        : OK
 */
extern int  winGetLimit(int limit[NBAXIS]);

/* Function Name : winGetSensor = gives the position of target in effector frame
   parameters    : double spos[2] = x, y
   return        : OK
 */
extern int  winGetSensor(double spos[2]);

/* Function Name : winChangeTool = change the tool in effector frame
   parameters    : enum  ToolType = {TOOL_EMPTY,TOOL_IN,TOOL_OUT}
   return        : OK
 */
extern  int winChangeTool(enum  ToolType tool);


/* Function Name : winGetTool = give the tool in effector frame
   parameters    : 
   return        : enum  ToolType = {TOOL_EMPTY,TOOL_IN,TOOL_OUT}
   return        : OK
 */
extern  int winGetTool(enum  ToolType *tool);


#ifdef __cplusplus
}
#endif

#endif
