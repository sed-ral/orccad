/* sscc : C CODE OF SORTED EQUATIONS Proccmove - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __Proccmove_GENERIC_TEST(TEST) return TEST;
typedef void (*__Proccmove_APF)();
static __Proccmove_APF *__Proccmove_PActionArray;

#include "Proccmove.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef __orcStrlPtr_DEFINED
#ifndef _orcStrlPtr
extern void _orcStrlPtr(orcStrlPtr* ,orcStrlPtr);
#endif
#endif
#ifndef _ArmXjmove_controler_DEFINED
#ifndef ArmXjmove_controler
extern void ArmXjmove_controler(orcStrlPtr);
#endif
#endif
#ifndef _ArmXjmove_fileparameter_DEFINED
#ifndef ArmXjmove_fileparameter
extern void ArmXjmove_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXjmove_parameter_DEFINED
#ifndef ArmXjmove_parameter
extern void ArmXjmove_parameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXcmove_controler_DEFINED
#ifndef ArmXcmove_controler
extern void ArmXcmove_controler(orcStrlPtr);
#endif
#endif
#ifndef _ArmXcmove_fileparameter_DEFINED
#ifndef ArmXcmove_fileparameter
extern void ArmXcmove_fileparameter(orcStrlPtr ,string);
#endif
#endif
#ifndef _ArmXcmove_parameter_DEFINED
#ifndef ArmXcmove_parameter
extern void ArmXcmove_parameter(orcStrlPtr);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __Proccmove_V0;
static boolean __Proccmove_V1;
static orcStrlPtr __Proccmove_V2;
static boolean __Proccmove_V3;
static boolean __Proccmove_V4;
static boolean __Proccmove_V5;
static boolean __Proccmove_V6;
static orcStrlPtr __Proccmove_V7;
static boolean __Proccmove_V8;
static boolean __Proccmove_V9;
static boolean __Proccmove_V10;
static boolean __Proccmove_V11;
static boolean __Proccmove_V12;


/* INPUT FUNCTIONS */

void Proccmove_I_START_Proccmove () {
__Proccmove_V0 = _true;
}
void Proccmove_I_Abort_Local_Proccmove () {
__Proccmove_V1 = _true;
}
void Proccmove_I_ArmXjmove_Start (orcStrlPtr __V) {
_orcStrlPtr(&__Proccmove_V2,__V);
__Proccmove_V3 = _true;
}
void Proccmove_I_USERSTART_ArmXjmove () {
__Proccmove_V4 = _true;
}
void Proccmove_I_BF_ArmXjmove () {
__Proccmove_V5 = _true;
}
void Proccmove_I_T3_ArmXjmove () {
__Proccmove_V6 = _true;
}
void Proccmove_I_ArmXcmove_Start (orcStrlPtr __V) {
_orcStrlPtr(&__Proccmove_V7,__V);
__Proccmove_V8 = _true;
}
void Proccmove_I_USERSTART_ArmXcmove () {
__Proccmove_V9 = _true;
}
void Proccmove_I_BF_ArmXcmove () {
__Proccmove_V10 = _true;
}
void Proccmove_I_T3_ArmXcmove () {
__Proccmove_V11 = _true;
}
void Proccmove_I_T2_reconf () {
__Proccmove_V12 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __Proccmove_A1 \
__Proccmove_V0
#define __Proccmove_A2 \
__Proccmove_V1
#define __Proccmove_A3 \
__Proccmove_V3
#define __Proccmove_A4 \
__Proccmove_V4
#define __Proccmove_A5 \
__Proccmove_V5
#define __Proccmove_A6 \
__Proccmove_V6
#define __Proccmove_A7 \
__Proccmove_V8
#define __Proccmove_A8 \
__Proccmove_V9
#define __Proccmove_A9 \
__Proccmove_V10
#define __Proccmove_A10 \
__Proccmove_V11
#define __Proccmove_A11 \
__Proccmove_V12

/* OUTPUT ACTIONS */

#define __Proccmove_A12 \
Proccmove_O_BF_Proccmove()
#define __Proccmove_A13 \
Proccmove_O_STARTED_Proccmove()
#define __Proccmove_A14 \
Proccmove_O_GoodEnd_Proccmove()
#define __Proccmove_A15 \
Proccmove_O_Abort_Proccmove()
#define __Proccmove_A16 \
Proccmove_O_T3_Proccmove()
#define __Proccmove_A17 \
Proccmove_O_START_ArmXjmove()
#define __Proccmove_A18 \
Proccmove_O_Abort_Local_ArmXjmove()
#define __Proccmove_A19 \
Proccmove_O_START_ArmXcmove()
#define __Proccmove_A20 \
Proccmove_O_Abort_Local_ArmXcmove()

/* ASSIGNMENTS */

#define __Proccmove_A21 \
__Proccmove_V0 = _false
#define __Proccmove_A22 \
__Proccmove_V1 = _false
#define __Proccmove_A23 \
__Proccmove_V3 = _false
#define __Proccmove_A24 \
__Proccmove_V4 = _false
#define __Proccmove_A25 \
__Proccmove_V5 = _false
#define __Proccmove_A26 \
__Proccmove_V6 = _false
#define __Proccmove_A27 \
__Proccmove_V8 = _false
#define __Proccmove_A28 \
__Proccmove_V9 = _false
#define __Proccmove_A29 \
__Proccmove_V10 = _false
#define __Proccmove_A30 \
__Proccmove_V11 = _false
#define __Proccmove_A31 \
__Proccmove_V12 = _false

/* PROCEDURE CALLS */

#define __Proccmove_A32 \
ArmXjmove_parameter(__Proccmove_V2,"0.5 0.5")
#define __Proccmove_A33 \
ArmXjmove_controler(__Proccmove_V2)
#define __Proccmove_A34 \
ArmXcmove_parameter(__Proccmove_V7)
#define __Proccmove_A35 \
ArmXcmove_controler(__Proccmove_V7)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

#define __Proccmove_A36 \

#define __Proccmove_A37 \


/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int Proccmove_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __Proccmove__reset_input () {
__Proccmove_V0 = _false;
__Proccmove_V1 = _false;
__Proccmove_V3 = _false;
__Proccmove_V4 = _false;
__Proccmove_V5 = _false;
__Proccmove_V6 = _false;
__Proccmove_V8 = _false;
__Proccmove_V9 = _false;
__Proccmove_V10 = _false;
__Proccmove_V11 = _false;
__Proccmove_V12 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __Proccmove_R[5] = {_true,_false,_false,_false,_false};

/* AUTOMATON ENGINE */

int Proccmove () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[18];
E[0] = (__Proccmove_R[3]&&!(__Proccmove_R[0]));
E[1] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__Proccmove_A9));
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__Proccmove_A9)));
E[2] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__Proccmove_A11));
E[2] = (E[1]||E[2]);
E[1] = (__Proccmove_R[2]||__Proccmove_R[3]);
E[3] = (E[1]||__Proccmove_R[4]);
E[4] = (__Proccmove_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__Proccmove_A3)));
if (E[4]) {
__Proccmove_A36;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A36\n");
#endif
}
E[5] = (__Proccmove_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__Proccmove_A7)));
if (E[5]) {
__Proccmove_A37;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A37\n");
#endif
}
E[6] = (__Proccmove_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Proccmove_A1));
E[7] = (__Proccmove_R[1]&&!(__Proccmove_R[0]));
E[8] = (E[7]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Proccmove_A1));
E[8] = (E[6]||E[8]);
if (E[8]) {
__Proccmove_A32;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A32\n");
#endif
}
if (E[8]) {
__Proccmove_A33;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A33\n");
#endif
}
E[6] = (__Proccmove_R[2]&&!(__Proccmove_R[0]));
E[9] = (E[6]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Proccmove_A5)));
E[10] = (E[9]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Proccmove_A6)));
E[10] = (E[8]||((__Proccmove_R[2]&&E[10])));
E[6] = (E[6]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__Proccmove_A5));
if (E[6]) {
__Proccmove_A34;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A34\n");
#endif
}
if (E[6]) {
__Proccmove_A35;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A35\n");
#endif
}
E[0] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__Proccmove_A11)));
E[11] = (E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__Proccmove_A10)));
E[11] = (E[6]||((__Proccmove_R[3]&&E[11])));
E[1] = ((((E[3]&&!(E[1])))||E[10])||E[11]);
E[12] = (E[1]||E[2]);
E[13] = (__Proccmove_R[4]&&!(__Proccmove_R[0]));
E[14] = (E[13]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Proccmove_A2)));
E[14] = (E[8]||((__Proccmove_R[4]&&E[14])));
E[15] = (((E[3]&&!(__Proccmove_R[4])))||E[14]);
E[2] = ((E[2]&&E[12])&&E[15]);
if (E[2]) {
__Proccmove_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A12\n");
#endif
}
if (E[8]) {
__Proccmove_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A13\n");
#endif
}
if (E[2]) {
__Proccmove_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A14\n");
#endif
}
E[13] = (E[13]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__Proccmove_A2));
if (E[13]) {
__Proccmove_A15;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A15\n");
#endif
}
E[0] = (E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__Proccmove_A10));
E[9] = (E[9]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__Proccmove_A6));
E[12] = ((E[12]||E[0])||E[9]);
E[16] = ((((E[0]||E[9]))&&E[12])&&E[15]);
if (E[16]) {
__Proccmove_A16;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A16\n");
#endif
}
if (E[8]) {
__Proccmove_A17;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A17\n");
#endif
}
if (E[13]) {
__Proccmove_A18;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A18\n");
#endif
}
if (E[6]) {
__Proccmove_A19;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A19\n");
#endif
}
if (E[13]) {
__Proccmove_A20;
#ifdef TRACE_ACTION
fprintf(stderr, "__Proccmove_A20\n");
#endif
}
E[9] = (E[0]||E[9]);
E[12] = ((E[13]&&E[12])&&((E[15]||E[13])));
E[0] = ((((E[2]||E[16]))||E[12]));
E[17] = (__Proccmove_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Proccmove_A1)));
E[7] = (E[7]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__Proccmove_A1)));
E[7] = (E[17]||((__Proccmove_R[1]&&E[7])));
E[15] = ((((((((E[10]||E[11])||E[14]))&&E[1])&&E[15]))||E[7]));
E[3] = (E[3]||__Proccmove_R[1]);
E[12] = (((((E[12]||E[16]))||E[2])||E[16])||E[12]);
__Proccmove_R[2] = (E[10]&&!(E[12]));
__Proccmove_R[3] = (E[11]&&!(E[12]));
__Proccmove_R[4] = (E[14]&&!(E[12]));
__Proccmove_R[0] = !(_true);
__Proccmove_R[1] = E[7];
__Proccmove__reset_input();
return E[15];
}

/* AUTOMATON RESET */

int Proccmove_reset () {
__Proccmove_R[0] = _true;
__Proccmove_R[1] = _false;
__Proccmove_R[2] = _false;
__Proccmove_R[3] = _false;
__Proccmove_R[4] = _false;
__Proccmove__reset_input();
return 0;
}
