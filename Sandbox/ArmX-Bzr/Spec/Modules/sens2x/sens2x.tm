// Ilv Version: 2.4
// File generated: Wed Mar 17 15:01:11 1999
// Creator class: IlvGraphOutput
Palettes 8
6 0 49087 65535 45232 50372 57054 "default" "9x15" 0 solid solid 0 0 0
3 0 49087 65535 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
5 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 0 49087 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 7 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "red" "red" "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
6 IlvFilledRectangle 90 90 170 180 
7 IlvMessageLabel 90 90 171 181 0 "sens2x" 16 [Box
0 1 1 sens2x 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 80 130 20 20 
2 IlvMessageLabel 102 126 74 28 0 "sensor" 1 0
[Port
0 1 1 sensor 90 140 102 126 74 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 250 130 20 20 
2 IlvMessageLabel 242 126 12 28 0 "x" 1 0
[Port
0 1 2 x 260 140 242 126 12 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
1 IlvFilledEllipse 80 220 20 20 
2 IlvMessageLabel 102 216 40 28 0 "pos" 1 0
[Port
0 1 3 pos 90 230 102 216 40 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 160 260 20 20 2 
5 IlvMessageLabel 148 241 88 28 0 "outwork" 1 0
[Port
0 1 4 outwork 170 270 148 241 88 28 8  8 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 26
