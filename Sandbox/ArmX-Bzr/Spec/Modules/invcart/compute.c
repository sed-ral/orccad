// Orccad Version: 3.0 alpha   
// Module : invcart
// Computation File 
//
// Module Ports :
// 	input DOUBLE xd[2]
// 	output DOUBLE jointd[2]
//
// Date of creation : Wed Mar 17 14:19:49 1999

k1 = (xd[0]*xd[0] + xd[1]*xd[1] - ARM_L1*ARM_L1 
      - ARM_L2*ARM_L2)/(2*ARM_L1*ARM_L2);
jointd[1] = acos(k1);
if (jointd[1] > 0)
  jointd[0] = - jointd[0];
k1 = atan2(xd[1],xd[0]);
k2 = (xd[0]*xd[0] + xd[1]*xd[1] + ARM_L1*ARM_L1 - ARM_L2*ARM_L2);
k2 = k2/(2*ARM_L1*ARM_L1*sqrt(xd[0]*xd[0] + xd[1]*xd[1]));
k2 = acos(k2);
	  
if (k2 < 0)
  jointd[0] = k1 + k2;
else
  jointd[0] = k1 - k2;

