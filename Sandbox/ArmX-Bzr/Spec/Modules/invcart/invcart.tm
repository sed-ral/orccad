// Ilv Version: 2.4
// File generated: Wed Mar 17 15:08:47 1999
// Creator class: IlvGraphOutput
Palettes 8
6 0 49087 65535 45232 50372 57054 "default" "9x15" 0 solid solid 0 0 0
4 "red" "red" "default" "9x15" 0 solid solid 0 0 0
3 0 49087 65535 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 0 49087 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 7 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
5 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 7
3 0 { 0 0 OrcIlvModAlgo 2
6 IlvFilledRectangle 100 80 170 190 
7 IlvMessageLabel 100 80 171 191 0 "invcart" 16 [Box
0 1 1 invcart 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 90 120 20 20 
2 IlvMessageLabel 112 116 26 28 0 "xd" 1 0
[Port
0 1 1 xd 100 130 112 116 26 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 260 120 20 20 
2 IlvMessageLabel 227 116 62 28 0 "jointd" 1 0
[Port
0 1 2 jointd 270 130 227 116 62 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 170 260 20 20 2 
5 IlvMessageLabel 163 241 70 28 0 "reconf" 1 0
[Port
0 1 3 reconf 180 270 163 241 70 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 26
