// Ilv Version: 2.4
// File generated: Tue Mar 16 18:58:11 1999
// Creator class: IlvGraphOutput
Palettes 9
1 0 49087 65535 "blue" "default" "9x15" 0 solid solid 0 0 0
3 0 49087 65535 "red" "default" "9x15" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"IlvStText" 8 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
7 0 49087 65535 62965 57054 46003 "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
5 "deepskyblue" "red" "default" "9x15" 0 solid solid 0 0 0
4 0 49087 65535 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
6 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 11
3 0 { 0 0 OrcIlvModPr 2
7 IlvFilledRectangle 80 90 150 160 
8 IlvMessageLabel 80 90 151 161 0 "WinX" 16 [Box
0 1 1 WinX 
Box]

 } 16
2 0 { 1 0 OrcIlvPortDrv 2
1 IlvFilledRoundRectangle 70 120 20 20 2 
2 IlvMessageLabel 92 116 72 28 0 "torque" 1 0
[Port
0 1 1 torque 80 130 92 116 72 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 220 120 20 20 2 
2 IlvMessageLabel 198 116 40 28 0 "pos" 1 0
[Port
0 1 2 pos 230 130 198 116 40 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortDrv 2
3 IlvFilledRoundRectangle 190 240 20 20 2 
2 IlvMessageLabel 190 221 40 28 0 "key" 1 0
[Port
0 1 3 key 200 250 190 221 40 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortDrv 2
4 IlvFilledRoundRectangle 100 240 20 20 2 
2 IlvMessageLabel 100 221 42 28 0 "limit" 1 0
[Port
0 1 4 limit 110 250 100 221 42 28 8  8 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortDrv 2
5 IlvFilledRoundRectangle 220 190 20 20 2 
6 IlvMessageLabel 181 186 74 28 0 "sensor" 1 0
[Port
0 1 5 sensor 230 200 181 186 74 28 4  4 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 26
