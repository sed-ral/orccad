// Ilv Version: 2.4
// File generated: Wed Mar 17 16:40:50 1999
// Creator class: IlvGraphOutput
Palettes 7
5 0 49087 65535 45232 50372 57054 "default" "9x15" 0 solid solid 0 0 0
3 0 49087 65535 "red" "default" "9x15" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 "red" "red" "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 6 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
1 0 49087 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
IlvObjects 9
3 0 { 0 0 OrcIlvModAlgo 2
5 IlvFilledRectangle 90 90 170 180 
6 IlvMessageLabel 90 90 171 181 0 "sens2th" 16 [Box
0 1 1 sens2th 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 80 130 20 20 
2 IlvMessageLabel 102 126 74 28 0 "sensor" 1 0
[Port
0 1 1 sensor 90 140 102 126 74 28 1  1 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortVar 2
3 IlvFilledEllipse 250 130 20 20 
2 IlvMessageLabel 221 126 54 28 0 "posd" 1 0
[Port
0 1 2 posd 260 140 221 126 54 28 4  4 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortVar 2
1 IlvFilledEllipse 80 220 20 20 
2 IlvMessageLabel 102 216 40 28 0 "pos" 1 0
[Port
0 1 3 pos 90 230 102 216 40 28 1  1 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortEvt 2
4 IlvReliefDiamond 160 260 20 20 2 
2 IlvMessageLabel 152 241 72 28 0 "inwork" 1 0
[Port
0 1 4 inwork 170 270 152 241 72 28 8  8 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
