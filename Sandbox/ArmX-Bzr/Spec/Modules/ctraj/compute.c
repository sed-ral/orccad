// Orccad Version: 3.0 alpha   
// Module : ctraj
// Computation File 
//
// Module Ports :
// 	input  DOUBLE posd[2]
// 	input  DOUBLE posi[2]
// 	output DOUBLE pos[2]
//
// Date of creation : Wed Mar 17 14:52:13 1999

for(index=0;index <2;index++){
  if (posi[index] < posd[index]){
    posi[index] += STEP_POS;
    if (posi[index] > posd[index])
      posi[index] = posd[index] ;
  }
  else{
    if (posi[index] > posd[index]){
      posi[index] -= STEP_POS; 
      if (posi[index] < posd[index])
	posi[index] = posd[index];
    }
  }
}

