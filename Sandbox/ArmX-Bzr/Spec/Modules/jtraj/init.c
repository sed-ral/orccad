// Orccad Version: 3.0 alpha
// Module : jtraj
// Initialisation File
// Date of creation : Mon Nov  3 13:05:11 1997

endtraj = NO_EVENT;
badtraj = NO_EVENT;
mode    = MODE_ACTIVE;
stop    = 0;

for(index=0;index <2;index++){
  if ( (posd[index] > LIMITMAX)||( posd[index] < LIMITMIN) ) {
    pos[index] = posI[index];
    if (badtraj == NO_EVENT)
      badtraj = SET_EVENT;
  }
  else {
    pos[index]=posI[index];
  }
}

fprintf(stdout, "jtraj:: Posd = (%f, %f)\n", posd[0], posd[1]);
