// Ilv Version: 2.4
// File generated: Wed Mar 17 10:24:20 1999
// Creator class: IlvGraphOutput
Palettes 9
3 65535 0 0 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
6 "white" "black" "normal" "%helvetica-12-" 0 solid solid 0 0 0
4 0 49087 65535 0 0 65535 "default" "9x15" 0 solid solid 0 0 0
5 "deepskyblue" "blue" "default" "9x15" 0 solid solid 0 0 0
1 0 49087 65535 65535 0 0 "default" "9x15" 0 solid solid 0 0 0
7 0 49087 65535 45232 50372 57054 "default" "9x15" 0 solid solid 0 0 0
"IlvStText" 8 0 49087 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
"default" 0 "deepskyblue" "black" "default" "9x15" 0 solid solid 0 0 0
2 65535 65535 65535 0 0 0 "normal" "%helvetica-12-" 0 solid solid 0 0 0
IlvObjects 13
3 0 { 0 0 OrcIlvModAlgo 2
7 IlvFilledRectangle 110 80 150 170 
8 IlvMessageLabel 110 80 151 171 0 "jtraj" 16 [Box
0 1 1 jtraj 
Box]

 } 16
2 0 { 1 0 OrcIlvPortVar 2
1 IlvFilledEllipse 250 110 20 20 
2 IlvMessageLabel 228 106 40 28 0 "pos" 1 0
[Port
0 1 1 pos 260 120 228 106 40 28 4  4 
Port]

 } 20
1 1 { 2 0 OrcIlvVoidLink 1 0 1 } 30
2 0 { 3 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 210 240 20 20 2 
2 IlvMessageLabel 201 221 76 28 0 "endtraj" 1 0
[Port
0 1 2 endtraj 220 250 201 221 76 28 8  8 
Port]

 } 20
1 1 { 4 0 OrcIlvVoidLink 1 0 3 } 30
2 0 { 5 0 OrcIlvPortEvt 2
3 IlvReliefDiamond 140 240 20 20 2 
2 IlvMessageLabel 131 221 76 28 0 "badtraj" 1 0
[Port
0 1 3 badtraj 150 250 131 221 76 28 8  8 
Port]

 } 20
1 1 { 6 0 OrcIlvVoidLink 1 0 5 } 30
2 0 { 7 0 OrcIlvPortVar 2
4 IlvFilledEllipse 100 110 20 20 
2 IlvMessageLabel 122 106 68 28 0 "typtraj" 1 0
[Port
0 1 4 typtraj 110 120 122 106 68 28 1  1 
Port]

 } 20
1 1 { 8 0 OrcIlvVoidLink 1 0 7 } 30
2 0 { 9 0 OrcIlvPortPrm 2
4 IlvPolyline 11
172 72 188 88 180 80 188 72 172 88 180 80 188 80 172 80 180 80 180 72
180 88 
2 IlvMessageLabel 167 85 54 28 0 "posd" 1 0
[Port
0 1 5 posd 180 80 167 85 54 28 2  2 
Port]

 } 20
1 1 { 10 0 OrcIlvVoidLink 1 0 9 } 30
2 0 { 11 0 OrcIlvPortVar 2
5 IlvFilledEllipse 100 200 20 20 
6 IlvMessageLabel 122 196 46 28 0 "posI" 1 0
[Port
0 1 6 posI 110 210 122 196 46 28 1  1 
Port]

 } 20
1 1 { 12 0 OrcIlvVoidLink 1 0 11 } 26
