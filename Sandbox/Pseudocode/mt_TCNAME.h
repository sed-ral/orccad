#ifndef __mt_TCNAME_h
#define __mt_TCNAME_h

#include "Exec/mt.h"
foreach(module algo in TC){
#include "module_NOMMODULEALGO_INDEX.h"
}
#include "PhR_NOMMODULEPHYS.h"

class RobotTask;
class orc_RT_RTNAME;
class orc_Mt_TCNAME;
extern void fct_TCNAME(orc_Mt_TCNAME*);

class orc_Mt_TCNAME: public ModuleTask
{
    friend void fct_TCNAME(orc_Mt_TCNAME*);
protected:
foreach(module algo in TC){
    orc_Mod_NOMMODULEALGO NOMMODULEALGO_INDEX;
 }
    orc_PhR_NOMMODULEPHYS* NOMMODULEPHYS;
    orc_RT_RTNAME *pRT;
public:
    void InitTrace();
public:

    orc_Mt_TCNAME(RobotTask *rt, char *name, int itFilter,double period, SYNC_TYPE S_Type, int priority, int nb_sem_link, CONSTRAINT_TYPE C_Type)
            :ModuleTask((FUNCPTR) fct_TCNAME,rt,name,itFilter,period,S_Type,priority,nb_sem_link, C_Type),

            NOMMODULEALGO_INDEX(this,period)
    {
        InitTrace();
        NOMMODULEPHYS = (orc_PhR_NOMMODULEPHYS*) RT->GetPhR();
	//cas des PhR_SENSOR????
        pRT=(orc_RT_RTNAME *) (RT);
    }
    ~orc_Mt_TCNAME()
    {};
    void Param();
    void ReParam();
    int SetT1Exceptions();
};
#endif
