#ifndef __interface_h
#define __interface_h

#include "Exec/canal.h"

enum INPUT_SIGNALS_USER {
  foreach(RT in PROC){
    USERSTART_RTNAME = COMPUTETIMEOUT + 1,
    foreach(T1 in RT)    T1NAME,
    InitOK_NOMMODULEPHYS,
    foreach(T2 in RT)    T2NAME,
    ActivateOK_NOMMODULEPHYS,
    KillOK_NOMMODULEPHYS,
    CmdStopOK_NOMMODULEPHYS,
    RTNAME_Start,
    foreach(Precond in RT){
    RTNAMETimeoutPRECONDNAME,
    PRECONDNAME,
    // sans doute pareil pour les PostConds?
    }
  }
    NB_SIGNALS
};

extern "C"
{
    int aa();
}
extern "C"
{
    int aa_reset ();
}
extern "C"
{
    int prr_reset (ProcedureRobot*);
}
extern "C"
{
    extern FUNCPTR TABSIG[NB_SIGNALS];
  foreach(RT in PROC) {
    FUNCPTR aa_I_USERSTART_RTNAME();
  foreach(T1 in RT)    FUNCPTR aa_I_T1NAME();
    FUNCPTR aa_I_InitOK_NOMMODULEPHYS();
  foreach(T2 in RT)   FUNCPTR aa_I_T2NAME();
    FUNCPTR aa_I_ActivateOK_NOMMODULEPHYS();
    FUNCPTR aa_I_KillOK_NOMMODULEPHYS();
    FUNCPTR aa_I_CmdStopOK_NOMMODULEPHYS();
    FUNCPTR aa_I_RTNAME_Start();
    FUNCPTR aa_I_RTNAMETimeoutPRENAME();
    FUNCPTR aa_I_PRENAME();
    FUNCPTR aa_I_Prr_Start(void*);
    FUNCPTR aa_I_ROBOT_FAIL();
    FUNCPTR aa_I_SOFT_FAIL();
    FUNCPTR aa_I_SENSOR_FAIL();
    FUNCPTR aa_I_CPU_OVERLOAD();
    int aa_O_Abort_RTNAME(void*);
    foreach(T1 in RT)    int aa_O_TreatT1NAME(void*);
    int aa_O_ActivateRTNAME_NOMMODULEPHYS(void*);
    int aa_O_RTNAMETransite(void*);
  }
    int aa_O_Activate(void*);
    int aa_O_Transite(void*);
    int aa_O_EndParam(void*);
    int aa_O_EndKill(void*);
    int aa_O_EndObs(void*);
    int aa_O_FinBF(void*);
    int aa_O_FinT3(void*);
    int aa_O_GoodEndRPr();
    int aa_O_T3RPr();
    int f_default();
} // end extern

extern char TABEVENT[NB_SIGNALS][MAXCHARNAME];

#endif
