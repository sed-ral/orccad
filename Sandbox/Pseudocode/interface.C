#include "interface.h"
#include "Exec/rt.h"
#include "Exec/prr.h"

#include "rts.h"

int prr_reset (ProcedureRobot* prr)
{
    aa_reset();
    aa();
    aa_I_Prr_Start((void*)prr);
    aa();
    return OK;
}

FUNCPTR TABSIG[NB_SIGNALS] =
    {
        (FUNCPTR) aa_I_ROBOT_FAIL,
        (FUNCPTR) aa_I_SOFT_FAIL,
        (FUNCPTR) aa_I_SENSOR_FAIL,
        (FUNCPTR) aa_I_CPU_OVERLOAD,
        (FUNCPTR) f_default,
foreach(RT in PROC){
        (FUNCPTR) aa_I_USERSTART_RTNAME,
foreach(T1 in RT)  (FUNCPTR) aa_I_T1NAME,
        (FUNCPTR) aa_I_InitOK_NOMMODULEPHYS,
foreach(T2 in RT)       (FUNCPTR) aa_I_T2NAME,
        (FUNCPTR) aa_I_ActivateOK_NOMMODULEPHYS,
        (FUNCPTR) aa_I_KillOK_NOMMODULEPHYS,
        (FUNCPTR) aa_I_CmdStopOK_NOMMODULEPHYS,
        (FUNCPTR) aa_I_RTNAME_Start,
foreach(Precond in RT){
        (FUNCPTR) aa_I_RTNAMETimeoutPrecond,
        (FUNCPTR) aa_I_Precond,
	}
	}
        ;

char TABEVENT[NB_SIGNALS][MAXCHARNAME]=
    {
        "ROBOT_FAIL",
        "SOFT_FAIL",
        "SENSOR_FAIL",
        "CPU_OVERLOAD",
        "PRECTIMEOUT",
        "COMPUTETIMEOUT",
	foreach(RT in PROC) {
        "USERSTART_RTNAME",
  foreach(T1 in RT)      "T1NAME",
        "InitOK_NOMMODULEPHYS",
  foreach(T2 in RT)        "T2NAME",
        "ActivateOK_NOMMODULEPHYS",
        "KillOK_NOMMODULEPHYS",
        "CmdStopOK_NOMMODULEPHYS",
        "RTNAME_Start",
foreach(Precond in RT){
        "RTNAMETimeoutPrecond",
        "Precond",
	}
	}
        };

int aa_O_Activate(void* pt_rt)
{
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->OutAuto(O_ACTIVATE);
    return OK;
}
int aa_O_Normal(void* pt_rt)
{
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->OutAuto(O_NORMAL);
    return OK;
}
int aa_O_Transite(void* pt_rt)
{
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->PutState(TRANSITE);
    RT->OutAuto(O_TRANSITE);
    return OK;
}
int aa_O_EndParam(void* pt_rt)
{
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->PutState(REPARAM);
    RT->OutAuto(O_END);
    return OK;
}
int aa_O_EndKill(void* pt_rt)
{
    RobotTask *RT = (RobotTask *) pt_rt;
    RT->OutAuto(O_END);
    return OK;
}
int aa_O_FinBF(void* pt_prr)
{
    ProcedureRobot *prr = (ProcedureRobot *) pt_prr;
    prr->EmitEnd();
    return OK;
}
int aa_O_FinT3(void* pt_prr)
{
    ProcedureRobot *prr = (ProcedureRobot *) pt_prr;
    prr->EmitUrgentEnd();
    return OK;
}
int f_default()
{
    return OK;
}
int aa_O_GoodEndRPr()
{
    return OK;
}
int aa_O_T3RPr()
{
    return OK;
}


extern "C"
{
  foreach(RT in PROC){
    int aa_O_Abort_RTNAME(void*)
    {
        return 1;
    }
    int aa_O_ActivateRTNAME_NOMMODULEPHYS(void* pt_rt)
    {
        RobotTask *RT = (RobotTask *) pt_rt;
        RT->OutAuto(O_ACTIVATE);
        return OK;
    }
    int aa_O_RTNAMETransite(void* pt_rt)
    {
        RobotTask *RT = (RobotTask *) pt_rt;
        RT->PutState(TRANSITE);
        RT->OutAuto(O_TRANSITE);
        return OK;
    }
    int aa_O_STARTED_RTNAME()
    {
        return OK;
    }
    int aa_O_GoodEnd_RTNAME()
    {
        return OK;
    }
  foreach(T1 in RT){
    int aa_O_TreatT1NAME(void* pt_rt)
    {
        orc_RT_RTNAME *RT = (orc_RT_RTNAME *) pt_rt;
        RT->aut_output_T1NAME = 1;
        return OK;
    }
  }
    void RTNAME_parameter(void* pt_rt)
    {
        orc_RT_RTNAME *RT = (orc_RT_RTNAME *)  pt_rt;
        RT->SetParam();
    }

    void RTNAME_controler(void* pt_rt)
    {
        orc_RT_RTNAME *RT = (orc_RT_RTNAME *)  pt_rt;
        RT->controler();
    }
  }
    int aa_O_STARTED_PROCNAME()
    {
        return OK;
    }
    int aa_O_GoodEnd_PROCNAME()
    {
        return OK;
    }
    int aa_O_Abort_PROCNAME()
    {
        return OK;
    }
} // extern "C"
