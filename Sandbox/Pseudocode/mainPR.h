#ifndef __orc_PROCNAME_h
#define __orc_PROCNAME_h

#include "Exec/utils.h"
#include "Exec/prr.h"

#include "interface.h"
#include "rts.h"

foreach (PhR in PROC) #include "PhR_NOMMODULEPHYS.h"
#define NBCLKS 1
//#define BASECLK 0.005

class orc_PROCNAME:public ProcedureRobot
{
protected:
    // Index Tab for clocks
    double *TabClks;
    // Physical Ressources
    foreach(PhR in PROC){
    orc_PhR_NOMMODULEPHYS *orcNOMMODULEPHYS;
    }
public:
    // Robot-Tasks
    foreach(RT in PROC){
    orc_RT_RTNAME *RTNAME;
    orc_RT_RTNAME *GetRTRTNAMEPtr()
    {
        return RTNAME;
    }
    }
    orc_PROCNAME();
    ~orc_PROCNAME();
    void Launch();
};

#endif
// End class orc_PROCNAME
