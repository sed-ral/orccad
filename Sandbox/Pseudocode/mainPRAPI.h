//
// This file defines the API functions 
// to interfere with the generated Orccad program 
//
// FOR SOLARIS USERS:
// You can use it including it in the main program
// launching the Orccad run time tasks:
// e.g: 
// 	 #include "SafenecsT1API.h"
// 	 main() { 
// 	 orcSafenecsT1Init();
// 	 orcSafenecsT1Launch();
// 	 // Launch task calling the others API Functions 
// 	 // ...add code here...
// 	 }
//
#ifndef __orc_PROCNAMEApi_h
#define __orc_PROCNAMEApi_h

// C function Interfaces
extern "C" { 
extern int orcPROCNAMEInit();
extern int orcPROCNAMELaunch();
extern int orcPROCNAMETaskLaunch();
extern int orcPROCNAMEGo();
extern int orcPROCNAMEStop();
} // extern "C"

#endif 
