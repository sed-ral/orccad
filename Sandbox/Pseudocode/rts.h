#ifndef __rts_h
#define __rts_h

#include "Exec/utils.h"
#include "Exec/rt.h"
#include "Exec/param.h"
#include "Exec/singleReaderACM.h"
foreach(RT in PROC){
foreach(TC in RT){
#include "mt_TCNAME.h"
}

class orc_RT_RTNAME: public RobotTask
{
protected:
    char cfgFile[MAXFILENAME];
    //Robot task Output parameters
    //Robot task Input parameters
    foreach(T1 in RT)    int T1NameState;
    // Module Tasks
    foreach(TC in RT){
    orc_Mt_TCNAME mt_TCNAME;
    }
    // Links between Mt
    foreach synchronized link{
    SingleReaderACM<VARTYPE> * sharedMemoryOnLink_LINKINDEX;
    }

public:
    //Type 1 Exceptions
    foreach(T1 in RT){
    int aut_output_T1NAME;
    int T1NAME;
    }
    orc_RT_RTNAME(char* ,ProcedureRobot *, PhR *,char *);
    ~orc_RT_RTNAME();
    int SetParam();
    int TestTimeout();
    foreach synchronized link{
    int ReadLink_LINKINDEX(vartype *port);
    void WriteLink_LINKINDEX(vartype *port);
    }
    foreach(T1 in RT) {
    void PutT1NAME(int);
    int Get_T1NAMEState();
    void Put_T1NAMEState(int);
    }
};
}
#endif

