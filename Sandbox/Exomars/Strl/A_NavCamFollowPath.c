/* sscc : C CODE OF SORTED EQUATIONS A_NavCamFollowPath - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __A_NavCamFollowPath_GENERIC_TEST(TEST) return TEST;
typedef void (*__A_NavCamFollowPath_APF)();
static __A_NavCamFollowPath_APF *__A_NavCamFollowPath_PActionArray;

#include "A_NavCamFollowPath.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef _ActivateCtrl_DEFINED
#ifndef ActivateCtrl
extern void ActivateCtrl(string);
#endif
#endif
#ifndef _Deactivate_DEFINED
#ifndef Deactivate
extern void Deactivate(string);
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __A_NavCamFollowPath_V0;
static boolean __A_NavCamFollowPath_V1;
static boolean __A_NavCamFollowPath_V2;
static boolean __A_NavCamFollowPath_V3;
static boolean __A_NavCamFollowPath_V4;
static boolean __A_NavCamFollowPath_V5;


/* INPUT FUNCTIONS */

void A_NavCamFollowPath_I_prec_navcamon () {
__A_NavCamFollowPath_V0 = _true;
}
void A_NavCamFollowPath_I_TO_prec_navcamon () {
__A_NavCamFollowPath_V1 = _true;
}
void A_NavCamFollowPath_I_post_navcamposeok () {
__A_NavCamFollowPath_V2 = _true;
}
void A_NavCamFollowPath_I_TO_post_navcamposeok () {
__A_NavCamFollowPath_V3 = _true;
}
void A_NavCamFollowPath_I_START () {
__A_NavCamFollowPath_V4 = _true;
}
void A_NavCamFollowPath_I_ABORT () {
__A_NavCamFollowPath_V5 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __A_NavCamFollowPath_A1 \
__A_NavCamFollowPath_V0
#define __A_NavCamFollowPath_A2 \
__A_NavCamFollowPath_V1
#define __A_NavCamFollowPath_A3 \
__A_NavCamFollowPath_V2
#define __A_NavCamFollowPath_A4 \
__A_NavCamFollowPath_V3
#define __A_NavCamFollowPath_A5 \
__A_NavCamFollowPath_V4
#define __A_NavCamFollowPath_A6 \
__A_NavCamFollowPath_V5

/* OUTPUT ACTIONS */

#define __A_NavCamFollowPath_A7 \
A_NavCamFollowPath_O_handle_T3_event()
#define __A_NavCamFollowPath_A8 \
A_NavCamFollowPath_O_GoodEnd()
#define __A_NavCamFollowPath_A9 \
A_NavCamFollowPath_O_ExecAborted()
#define __A_NavCamFollowPath_A10 \
A_NavCamFollowPath_O_VERIF_START()
#define __A_NavCamFollowPath_A11 \
A_NavCamFollowPath_O_VERIF_ABORT()
#define __A_NavCamFollowPath_A12 \
A_NavCamFollowPath_O_VERIF_GoodEnd()
#define __A_NavCamFollowPath_A13 \
A_NavCamFollowPath_O_VERIF_ExecAborted()
#define __A_NavCamFollowPath_A14 \
A_NavCamFollowPath_O_VERIF_SUBSYSTEM_ACQUIRED()
#define __A_NavCamFollowPath_A15 \
A_NavCamFollowPath_O_VERIF_SUBSYSTEM_RELEASED()

/* ASSIGNMENTS */

#define __A_NavCamFollowPath_A16 \
__A_NavCamFollowPath_V0 = _false
#define __A_NavCamFollowPath_A17 \
__A_NavCamFollowPath_V1 = _false
#define __A_NavCamFollowPath_A18 \
__A_NavCamFollowPath_V2 = _false
#define __A_NavCamFollowPath_A19 \
__A_NavCamFollowPath_V3 = _false
#define __A_NavCamFollowPath_A20 \
__A_NavCamFollowPath_V4 = _false
#define __A_NavCamFollowPath_A21 \
__A_NavCamFollowPath_V5 = _false

/* PROCEDURE CALLS */

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int A_NavCamFollowPath_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __A_NavCamFollowPath__reset_input () {
__A_NavCamFollowPath_V0 = _false;
__A_NavCamFollowPath_V1 = _false;
__A_NavCamFollowPath_V2 = _false;
__A_NavCamFollowPath_V3 = _false;
__A_NavCamFollowPath_V4 = _false;
__A_NavCamFollowPath_V5 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __A_NavCamFollowPath_R[5] = {_true,
 _false,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int A_NavCamFollowPath () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[18];
E[0] = __A_NavCamFollowPath_R[3]&&!(__A_NavCamFollowPath_R[0]);
E[1] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__A_NavCamFollowPath_A2);
E[2] = __A_NavCamFollowPath_R[4]&&!(__A_NavCamFollowPath_R[0]);
E[3] = E[2]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__A_NavCamFollowPath_A4);
if (E[1]||E[3]) {
__A_NavCamFollowPath_A7;
#ifdef TRACE_ACTION
fprintf(stderr, "__A_NavCamFollowPath_A7\n");
#endif
}
E[2] = E[2]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__A_NavCamFollowPath_A4));
E[2] = __A_NavCamFollowPath_R[4]&&E[2];
E[4] = E[2]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__A_NavCamFollowPath_A3);
if (E[4]) {
__A_NavCamFollowPath_A8;
#ifdef TRACE_ACTION
fprintf(stderr, "__A_NavCamFollowPath_A8\n");
#endif
}
E[5] = __A_NavCamFollowPath_R[2]&&!(__A_NavCamFollowPath_R[0]);
E[6] = E[5]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__A_NavCamFollowPath_A6);
if (E[6]) {
__A_NavCamFollowPath_A9;
#ifdef TRACE_ACTION
fprintf(stderr, "__A_NavCamFollowPath_A9\n");
#endif
}
E[7] = __A_NavCamFollowPath_R[3]||__A_NavCamFollowPath_R[4];
E[8] = __A_NavCamFollowPath_R[2]||E[7];
E[5] = E[5]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__A_NavCamFollowPath_A6));
E[5] = __A_NavCamFollowPath_R[2]&&E[5];
E[9] = (E[8]&&!(__A_NavCamFollowPath_R[2]))||E[5];
E[10] = E[9]||E[6];
E[0] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__A_NavCamFollowPath_A2));
E[0] = __A_NavCamFollowPath_R[3]&&E[0];
E[11] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__A_NavCamFollowPath_A1));
E[11] = __A_NavCamFollowPath_R[3]&&E[11];
E[0] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__A_NavCamFollowPath_A1);
E[2] = E[2]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__A_NavCamFollowPath_A3));
E[2] = E[0]||(__A_NavCamFollowPath_R[4]&&E[2]);
E[7] = (E[8]&&!(E[7]))||E[11]||E[2];
E[0] = E[7]||E[4];
E[12] = (E[6]||E[4])&&E[10]&&E[0];
E[13] = E[12]||__A_NavCamFollowPath_R[0];
E[14] = E[13]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__A_NavCamFollowPath_A5);
E[15] = __A_NavCamFollowPath_R[1]&&!(__A_NavCamFollowPath_R[0]);
E[16] = E[15]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__A_NavCamFollowPath_A5);
E[16] = E[14]||E[16];
if (E[16]) {
__A_NavCamFollowPath_A10;
#ifdef TRACE_ACTION
fprintf(stderr, "__A_NavCamFollowPath_A10\n");
#endif
}
if (E[6]) {
__A_NavCamFollowPath_A11;
#ifdef TRACE_ACTION
fprintf(stderr, "__A_NavCamFollowPath_A11\n");
#endif
}
if (E[4]) {
__A_NavCamFollowPath_A12;
#ifdef TRACE_ACTION
fprintf(stderr, "__A_NavCamFollowPath_A12\n");
#endif
}
if (E[6]) {
__A_NavCamFollowPath_A13;
#ifdef TRACE_ACTION
fprintf(stderr, "__A_NavCamFollowPath_A13\n");
#endif
}
if (E[16]) {
__A_NavCamFollowPath_A14;
#ifdef TRACE_ACTION
fprintf(stderr, "__A_NavCamFollowPath_A14\n");
#endif
}
E[0] = (E[1]||E[3])&&E[10]&&(E[0]||E[1]||E[3]);
if (E[12]||E[0]) {
__A_NavCamFollowPath_A15;
#ifdef TRACE_ACTION
fprintf(stderr, "__A_NavCamFollowPath_A15\n");
#endif
}
E[10] = E[1]||E[3];
E[14] = E[6]||E[4];
E[17] = E[0];
E[13] = E[13]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__A_NavCamFollowPath_A5));
E[15] = E[15]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__A_NavCamFollowPath_A5));
E[15] = E[13]||(__A_NavCamFollowPath_R[1]&&E[15]);
E[7] = E[16]||((E[5]||E[11]||E[2])&&E[9]&&E[7])||E[15];
E[8] = E[8]||__A_NavCamFollowPath_R[1];
E[9] = E[12]||E[0];
__A_NavCamFollowPath_R[2] = E[16]||(E[5]&&!(E[9]));
__A_NavCamFollowPath_R[3] = E[16]||(E[11]&&!(E[9]));
__A_NavCamFollowPath_R[4] = E[2]&&!(E[9]);
__A_NavCamFollowPath_R[0] = !(_true);
__A_NavCamFollowPath_R[1] = E[15];
__A_NavCamFollowPath__reset_input();
return E[7];
}

/* AUTOMATON RESET */

int A_NavCamFollowPath_reset () {
__A_NavCamFollowPath_R[0] = _true;
__A_NavCamFollowPath_R[1] = _false;
__A_NavCamFollowPath_R[2] = _false;
__A_NavCamFollowPath_R[3] = _false;
__A_NavCamFollowPath_R[4] = _false;
__A_NavCamFollowPath__reset_input();
return 0;
}
