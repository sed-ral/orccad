module T_coord_CriticalDeployment:

% Component handle requests and synchronisation signals :
  input handle_A_SelfCheck_T3_event;
  input A_SelfCheck_GoodEnd;
  input A_SelfCheck_ExecAborted;
  output A_SelfCheck_START;
  output A_SelfCheck_ABORT;

  input handle_A_DeploySolarArray_T3_event;
  input A_DeploySolarArray_GoodEnd;
  input A_DeploySolarArray_ExecAborted;
  output A_DeploySolarArray_START;
  output A_DeploySolarArray_ABORT;

  input handle_A_DeployAntenna_T3_event;
  input A_DeployAntenna_GoodEnd;
  input A_DeployAntenna_ExecAborted;
  output A_DeployAntenna_START;
  output A_DeployAntenna_ABORT;

  input handle_A_DhuSendTm_exc_comm_los;
  input handle_A_DhuSendTm_T3_event;
  input A_DhuSendTm_GoodEnd;
  input A_DhuSendTm_ExecAborted;
  output A_DhuSendTm_START;
  output A_DhuSendTm_ABORT;

  input handle_A_DhuSendImages_exc_comm_los;
  input handle_A_DhuSendImages_T3_event;
  input A_DhuSendImages_GoodEnd;
  input A_DhuSendImages_ExecAborted;
  output A_DhuSendImages_START;
  output A_DhuSendImages_ABORT;

% Task-level events and synchronisation signals :
  input sol_day_ended;
  input sol_day_started;
  input comm_window_started;
  input comm_window_ended;

% Task-level verification signals
  output VERIF_sol_day_ended;
  output VERIF_sol_day_started;
  output VERIF_comm_window_started;
  output VERIF_comm_window_ended;
  
  input START;
  input ABORT;
  output GoodEnd;
  output ExecAborted;
  
  output ABORT_COMPONENTS;
  output REQ_HANDLE_T3_EVENT;
  
  % verification signals
  output VERIF_START;
  output VERIF_ABORT;
  output VERIF_GoodEnd;
  output VERIF_ExecAborted;
  
  % Procedure Signature declarations
  procedure Activate_SelfCheck()();
  procedure Activate_DeploySolarArray()();
  procedure Activate_DeployAntenna()();
  procedure Activate_DhuSendTm()();
  procedure Activate_DhuSendImages()();

  trap Fatality_Trap in
    loop
      await immediate START
      ;
      emit VERIF_START
      ;
      trap Reset_Trap in
        await ABORT
        ;
        emit VERIF_ABORT
        ;
        emit ExecAborted
        ;
        emit VERIF_ExecAborted
        ;
        emit ABORT_COMPONENTS
        ;
        exit Reset_Trap
        ||
        % waiting pre-conditions, if any;
        % we require all conditions to be met => put in [] !
        
        trap ObjectiveReached_Trap in
        [
          [% ------------- operator-defined coordinator body -------------
                 
                    [ % self check of the rover 
                      %call Activate_SelfCheck()();
                      emit A_SelfCheck_START;
                      abort
                        await
                          case A_SelfCheck_GoodEnd
                          case A_SelfCheck_ExecAborted do [ halt ]
                        end await
                      when handle_A_SelfCheck_T3_event do
                      [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                      end abort
                    ]
                    ;
                    % deployment of critical items - solar array and antenna 
                    [
                      %call Activate_DeploySolarArray()();
                      emit A_DeploySolarArray_START;
                      abort
                        await
                          case A_DeploySolarArray_GoodEnd
                          case A_DeploySolarArray_ExecAborted do [ halt ]
                        end await
                      when handle_A_DeploySolarArray_T3_event do
                      [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                      end abort
                    ]
                    ;
                    [
                      %call Activate_DeployAntenna()();
                      emit A_DeployAntenna_START;
                      abort
                        await
                          case A_DeployAntenna_GoodEnd
                          case A_DeployAntenna_ExecAborted do [ halt ]
                        end await
                      when handle_A_DeployAntenna_T3_event do
                      [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                      end abort
                    ]
                    ;
                    % wait for communication window and send the TM and the first images
                    trap Tm_sended in
                    [
                     loop
                     [
                      abort % comm_window_ended
                      [
                       await comm_window_started; emit VERIF_comm_window_started;
                       [
                        %call Activate_DhuSendTm()();
                        emit A_DhuSendTm_START ;
                        abort
                         await
                          case A_DhuSendTm_GoodEnd
                          case A_DhuSendTm_ExecAborted do  [ halt ]
                          end await
                        when
                         case handle_A_DhuSendTm_exc_comm_los do
                           [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                         case handle_A_DhuSendTm_T3_event do
                           [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                        end abort
                        ]
                        ;
                        [
                          %call Activate_DhuSendImages()();
                          emit A_DhuSendImages_START;
                          abort
                           await
                            case A_DhuSendImages_GoodEnd
                            case A_DhuSendImages_ExecAborted do [ halt ]
                           end await
                          when
                           case handle_A_DhuSendImages_exc_comm_los do
                             [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                           case handle_A_DhuSendImages_T3_event do
                             [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                          end abort
                        ]
                        ; 
                        exit Tm_sended
                       ]
                       when comm_window_ended do emit VERIF_comm_window_ended;
                       emit A_DhuSendTm_ABORT; emit A_DhuSendImages_ABORT
                    end abort
                 ]
                 end loop
                ] 
                end trap 
          ]% ---------- end of operator-defined coordinator body ---------
          ;
          exit ObjectiveReached_Trap
          ||
          [ % waiting postconditions; we require all conditions to be met => put in [] !
              halt
          ]
          ;
          emit ABORT_COMPONENTS
          ;
          exit ObjectiveReached_Trap
        ]
        handle ObjectiveReached_Trap do
          emit GoodEnd
          ;
          emit VERIF_GoodEnd
        end trap % ObjectiveReached_Trap
        ;
        exit Reset_Trap
      end trap % Reset_Trap
    end loop % main loop
  end trap % Fatality_Trap
end module
