/* sscc : C CODE OF SORTED EQUATIONS T_SurfaceAcquireMeas - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __T_SurfaceAcquireMeas_GENERIC_TEST(TEST) return TEST;
typedef void (*__T_SurfaceAcquireMeas_APF)();
static __T_SurfaceAcquireMeas_APF *__T_SurfaceAcquireMeas_PActionArray;

#include "esterel.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef _Activate_PanCamMonitor_DEFINED
#ifndef Activate_PanCamMonitor
extern void Activate_PanCamMonitor(integer);
#endif
#endif
#ifndef _Activate_DrillMoveTo_DEFINED
#ifndef Activate_DrillMoveTo
extern void Activate_DrillMoveTo(string);
#endif
#endif
#ifndef _Activate_DrillMoveInContact_DEFINED
#ifndef Activate_DrillMoveInContact
extern void Activate_DrillMoveInContact(double);
#endif
#endif
#ifndef _Activate_DrillInsert_DEFINED
#ifndef Activate_DrillInsert
extern void Activate_DrillInsert(double);
#endif
#endif
#ifndef _Activate_DrillAcquireSample_DEFINED
#ifndef Activate_DrillAcquireSample
extern void Activate_DrillAcquireSample();
#endif
#endif
#ifndef _Activate_DrillRemove_DEFINED
#ifndef Activate_DrillRemove
extern void Activate_DrillRemove(double);
#endif
#endif
#ifndef _Activate_CloseImagerMonitor_DEFINED
#ifndef Activate_CloseImagerMonitor
extern void Activate_CloseImagerMonitor(integer);
#endif
#endif
#ifndef _Activate_CloseImagerMoveHome_DEFINED
#ifndef Activate_CloseImagerMoveHome
extern void Activate_CloseImagerMoveHome();
#endif
#endif
#ifndef _Activate_CloseImagerMoveTo_DEFINED
#ifndef Activate_CloseImagerMoveTo
extern void Activate_CloseImagerMoveTo(string);
#endif
#endif
#ifndef _Activate_ApxsGetMeas_DEFINED
#ifndef Activate_ApxsGetMeas
extern void Activate_ApxsGetMeas();
#endif
#endif
#ifndef _Activate_DrillBrush_DEFINED
#ifndef Activate_DrillBrush
extern void Activate_DrillBrush();
#endif
#endif
#ifndef _Activate_DrillMoveHome_DEFINED
#ifndef Activate_DrillMoveHome
extern void Activate_DrillMoveHome();
#endif
#endif
#ifndef _Activate_GoToSleep_DEFINED
#ifndef Activate_GoToSleep
extern void Activate_GoToSleep();
#endif
#endif
#ifndef _Activate_IddMoveTo_DEFINED
#ifndef Activate_IddMoveTo
extern void Activate_IddMoveTo(string);
#endif
#endif
#ifndef _Activate_MoSGetMeas_DEFINED
#ifndef Activate_MoSGetMeas
extern void Activate_MoSGetMeas();
#endif
#endif
#ifndef _Activate_PasteurAnalyseSample_DEFINED
#ifndef Activate_PasteurAnalyseSample
extern void Activate_PasteurAnalyseSample();
#endif
#endif
#ifndef _Activate_SelfCheck_DEFINED
#ifndef Activate_SelfCheck
extern void Activate_SelfCheck();
#endif
#endif
#ifndef _Activate_PasteurAnalyseData_DEFINED
#ifndef Activate_PasteurAnalyseData
extern void Activate_PasteurAnalyseData();
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __T_SurfaceAcquireMeas_V0;
static boolean __T_SurfaceAcquireMeas_V1;
static boolean __T_SurfaceAcquireMeas_V2;
static boolean __T_SurfaceAcquireMeas_V3;
static boolean __T_SurfaceAcquireMeas_V4;
static boolean __T_SurfaceAcquireMeas_V5;
static boolean __T_SurfaceAcquireMeas_V6;
static boolean __T_SurfaceAcquireMeas_V7;
static boolean __T_SurfaceAcquireMeas_V8;
static boolean __T_SurfaceAcquireMeas_V9;
static boolean __T_SurfaceAcquireMeas_V10;
static boolean __T_SurfaceAcquireMeas_V11;
static boolean __T_SurfaceAcquireMeas_V12;
static boolean __T_SurfaceAcquireMeas_V13;
static boolean __T_SurfaceAcquireMeas_V14;
static boolean __T_SurfaceAcquireMeas_V15;
static boolean __T_SurfaceAcquireMeas_V16;
static boolean __T_SurfaceAcquireMeas_V17;
static boolean __T_SurfaceAcquireMeas_V18;
static boolean __T_SurfaceAcquireMeas_V19;
static boolean __T_SurfaceAcquireMeas_V20;
static boolean __T_SurfaceAcquireMeas_V21;
static boolean __T_SurfaceAcquireMeas_V22;
static boolean __T_SurfaceAcquireMeas_V23;
static boolean __T_SurfaceAcquireMeas_V24;
static boolean __T_SurfaceAcquireMeas_V25;
static boolean __T_SurfaceAcquireMeas_V26;
static boolean __T_SurfaceAcquireMeas_V27;
static boolean __T_SurfaceAcquireMeas_V28;
static boolean __T_SurfaceAcquireMeas_V29;
static boolean __T_SurfaceAcquireMeas_V30;
static boolean __T_SurfaceAcquireMeas_V31;
static boolean __T_SurfaceAcquireMeas_V32;
static boolean __T_SurfaceAcquireMeas_V33;
static boolean __T_SurfaceAcquireMeas_V34;
static boolean __T_SurfaceAcquireMeas_V35;
static boolean __T_SurfaceAcquireMeas_V36;
static boolean __T_SurfaceAcquireMeas_V37;
static boolean __T_SurfaceAcquireMeas_V38;
static boolean __T_SurfaceAcquireMeas_V39;
static boolean __T_SurfaceAcquireMeas_V40;
static boolean __T_SurfaceAcquireMeas_V41;
static boolean __T_SurfaceAcquireMeas_V42;
static boolean __T_SurfaceAcquireMeas_V43;
static boolean __T_SurfaceAcquireMeas_V44;
static boolean __T_SurfaceAcquireMeas_V45;
static boolean __T_SurfaceAcquireMeas_V46;
static boolean __T_SurfaceAcquireMeas_V47;
static boolean __T_SurfaceAcquireMeas_V48;
static boolean __T_SurfaceAcquireMeas_V49;
static boolean __T_SurfaceAcquireMeas_V50;
static boolean __T_SurfaceAcquireMeas_V51;
static boolean __T_SurfaceAcquireMeas_V52;
static boolean __T_SurfaceAcquireMeas_V53;
static boolean __T_SurfaceAcquireMeas_V54;
static boolean __T_SurfaceAcquireMeas_V55;
static boolean __T_SurfaceAcquireMeas_V56;
static boolean __T_SurfaceAcquireMeas_V57;
static boolean __T_SurfaceAcquireMeas_V58;
static boolean __T_SurfaceAcquireMeas_V59;
static boolean __T_SurfaceAcquireMeas_V60;
static boolean __T_SurfaceAcquireMeas_V61;
static boolean __T_SurfaceAcquireMeas_V62;
static boolean __T_SurfaceAcquireMeas_V63;
static boolean __T_SurfaceAcquireMeas_V64;
static boolean __T_SurfaceAcquireMeas_V65;
static boolean __T_SurfaceAcquireMeas_V66;
static boolean __T_SurfaceAcquireMeas_V67;
static boolean __T_SurfaceAcquireMeas_V68;
static boolean __T_SurfaceAcquireMeas_V69;
static boolean __T_SurfaceAcquireMeas_V70;
static boolean __T_SurfaceAcquireMeas_V71;


/* INPUT FUNCTIONS */

void T_SurfaceAcquireMeas_I_START () {
__T_SurfaceAcquireMeas_V0 = _true;
}
void T_SurfaceAcquireMeas_I_ABORT () {
__T_SurfaceAcquireMeas_V1 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_ApxsGetMeas_post_apxs_measok () {
__T_SurfaceAcquireMeas_V2 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_ApxsGetMeas_prec_apxs_on () {
__T_SurfaceAcquireMeas_V3 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_CloseImagerMonitor_prec_closeimager_ok () {
__T_SurfaceAcquireMeas_V4 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_CloseImagerMoveHome_post_closeimagerposeok () {
__T_SurfaceAcquireMeas_V5 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_CloseImagerMoveHome_prec_closeimager_ok () {
__T_SurfaceAcquireMeas_V6 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_CloseImagerMoveTo_post_closeimagerposeok () {
__T_SurfaceAcquireMeas_V7 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_CloseImagerMoveTo_prec_closeimager_ok () {
__T_SurfaceAcquireMeas_V8 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillAcquireSample_post_drillsampleok () {
__T_SurfaceAcquireMeas_V9 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillAcquireSample_prec_drill_ok () {
__T_SurfaceAcquireMeas_V10 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillBrush_post_drilldepthok () {
__T_SurfaceAcquireMeas_V11 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillBrush_prec_drill_ok () {
__T_SurfaceAcquireMeas_V12 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillInsert_post_drilldepthok () {
__T_SurfaceAcquireMeas_V13 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillInsert_prec_drill_ok () {
__T_SurfaceAcquireMeas_V14 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillMoveHome_post_drillposeok () {
__T_SurfaceAcquireMeas_V15 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillMoveHome_prec_drill_ok () {
__T_SurfaceAcquireMeas_V16 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillMoveInContact_post_drillincontact () {
__T_SurfaceAcquireMeas_V17 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillMoveInContact_prec_drill_ok () {
__T_SurfaceAcquireMeas_V18 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillMoveTo_post_drillposeok () {
__T_SurfaceAcquireMeas_V19 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillMoveTo_prec_drill_ok () {
__T_SurfaceAcquireMeas_V20 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillRemove_post_drilldepthok () {
__T_SurfaceAcquireMeas_V21 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_DrillRemove_prec_drill_ok () {
__T_SurfaceAcquireMeas_V22 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_GoToSleep_post_sleep_ok () {
__T_SurfaceAcquireMeas_V23 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_IddMoveTo_post_idd_poseok () {
__T_SurfaceAcquireMeas_V24 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_IddMoveTo_prec_idd_ok () {
__T_SurfaceAcquireMeas_V25 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_MoSGetMeas_post_mos_measok () {
__T_SurfaceAcquireMeas_V26 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_MoSGetMeas_prec_mos_on () {
__T_SurfaceAcquireMeas_V27 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_PanCamMonitor_prec_pancamok () {
__T_SurfaceAcquireMeas_V28 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_PasteurAnalyseData_post_pasteur_data_anal () {
__T_SurfaceAcquireMeas_V29 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_PasteurAnalyseData_prec_pasteur_data_ok () {
__T_SurfaceAcquireMeas_V30 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_PasteurAnalyseSample_post_pasteur_sample_anal () {
__T_SurfaceAcquireMeas_V31 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_PasteurAnalyseSample_prec_pasteur_sample_ok () {
__T_SurfaceAcquireMeas_V32 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_SelfCheck_post_comm_ok () {
__T_SurfaceAcquireMeas_V33 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_SelfCheck_post_dhu_ok () {
__T_SurfaceAcquireMeas_V34 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_SelfCheck_post_loc_ok () {
__T_SurfaceAcquireMeas_V35 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_SelfCheck_post_power_ok () {
__T_SurfaceAcquireMeas_V36 = _true;
}
void T_SurfaceAcquireMeas_I_TO_A_SelfCheck_post_thermal_ok () {
__T_SurfaceAcquireMeas_V37 = _true;
}
void T_SurfaceAcquireMeas_I_exc_drillblocked () {
__T_SurfaceAcquireMeas_V38 = _true;
}
void T_SurfaceAcquireMeas_I_exc_drillmaxthrust () {
__T_SurfaceAcquireMeas_V39 = _true;
}
void T_SurfaceAcquireMeas_I_exc_drillmovefail () {
__T_SurfaceAcquireMeas_V40 = _true;
}
void T_SurfaceAcquireMeas_I_exc_drillsamplemaxtemp () {
__T_SurfaceAcquireMeas_V41 = _true;
}
void T_SurfaceAcquireMeas_I_exc_evening () {
__T_SurfaceAcquireMeas_V42 = _true;
}
void T_SurfaceAcquireMeas_I_exc_pancamfail () {
__T_SurfaceAcquireMeas_V43 = _true;
}
void T_SurfaceAcquireMeas_I_exc_power_lim () {
__T_SurfaceAcquireMeas_V44 = _true;
}
void T_SurfaceAcquireMeas_I_post_apxs_measok () {
__T_SurfaceAcquireMeas_V45 = _true;
}
void T_SurfaceAcquireMeas_I_post_closeimagerposeok () {
__T_SurfaceAcquireMeas_V46 = _true;
}
void T_SurfaceAcquireMeas_I_post_comm_ok () {
__T_SurfaceAcquireMeas_V47 = _true;
}
void T_SurfaceAcquireMeas_I_post_dhu_ok () {
__T_SurfaceAcquireMeas_V48 = _true;
}
void T_SurfaceAcquireMeas_I_post_drilldepthok () {
__T_SurfaceAcquireMeas_V49 = _true;
}
void T_SurfaceAcquireMeas_I_post_drillincontact () {
__T_SurfaceAcquireMeas_V50 = _true;
}
void T_SurfaceAcquireMeas_I_post_drillposeok () {
__T_SurfaceAcquireMeas_V51 = _true;
}
void T_SurfaceAcquireMeas_I_post_drillsampleok () {
__T_SurfaceAcquireMeas_V52 = _true;
}
void T_SurfaceAcquireMeas_I_post_idd_poseok () {
__T_SurfaceAcquireMeas_V53 = _true;
}
void T_SurfaceAcquireMeas_I_post_loc_ok () {
__T_SurfaceAcquireMeas_V54 = _true;
}
void T_SurfaceAcquireMeas_I_post_mos_measok () {
__T_SurfaceAcquireMeas_V55 = _true;
}
void T_SurfaceAcquireMeas_I_post_pasteur_data_anal () {
__T_SurfaceAcquireMeas_V56 = _true;
}
void T_SurfaceAcquireMeas_I_post_pasteur_sample_anal () {
__T_SurfaceAcquireMeas_V57 = _true;
}
void T_SurfaceAcquireMeas_I_post_power_ok () {
__T_SurfaceAcquireMeas_V58 = _true;
}
void T_SurfaceAcquireMeas_I_post_sleep_ok () {
__T_SurfaceAcquireMeas_V59 = _true;
}
void T_SurfaceAcquireMeas_I_post_thermal_ok () {
__T_SurfaceAcquireMeas_V60 = _true;
}
void T_SurfaceAcquireMeas_I_prec_apxs_on () {
__T_SurfaceAcquireMeas_V61 = _true;
}
void T_SurfaceAcquireMeas_I_prec_closeimager_ok () {
__T_SurfaceAcquireMeas_V62 = _true;
}
void T_SurfaceAcquireMeas_I_prec_drill_ok () {
__T_SurfaceAcquireMeas_V63 = _true;
}
void T_SurfaceAcquireMeas_I_prec_idd_ok () {
__T_SurfaceAcquireMeas_V64 = _true;
}
void T_SurfaceAcquireMeas_I_prec_mos_on () {
__T_SurfaceAcquireMeas_V65 = _true;
}
void T_SurfaceAcquireMeas_I_prec_pancamok () {
__T_SurfaceAcquireMeas_V66 = _true;
}
void T_SurfaceAcquireMeas_I_prec_pasteur_data_ok () {
__T_SurfaceAcquireMeas_V67 = _true;
}
void T_SurfaceAcquireMeas_I_prec_pasteur_sample_ok () {
__T_SurfaceAcquireMeas_V68 = _true;
}
void T_SurfaceAcquireMeas_I_analysis_positive () {
__T_SurfaceAcquireMeas_V69 = _true;
}
void T_SurfaceAcquireMeas_I_analysis_negative () {
__T_SurfaceAcquireMeas_V70 = _true;
}
void T_SurfaceAcquireMeas_I_sol_day_started () {
__T_SurfaceAcquireMeas_V71 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __T_SurfaceAcquireMeas_A1 \
__T_SurfaceAcquireMeas_V0
#define __T_SurfaceAcquireMeas_A2 \
__T_SurfaceAcquireMeas_V1
#define __T_SurfaceAcquireMeas_A3 \
__T_SurfaceAcquireMeas_V2
#define __T_SurfaceAcquireMeas_A4 \
__T_SurfaceAcquireMeas_V3
#define __T_SurfaceAcquireMeas_A5 \
__T_SurfaceAcquireMeas_V4
#define __T_SurfaceAcquireMeas_A6 \
__T_SurfaceAcquireMeas_V5
#define __T_SurfaceAcquireMeas_A7 \
__T_SurfaceAcquireMeas_V6
#define __T_SurfaceAcquireMeas_A8 \
__T_SurfaceAcquireMeas_V7
#define __T_SurfaceAcquireMeas_A9 \
__T_SurfaceAcquireMeas_V8
#define __T_SurfaceAcquireMeas_A10 \
__T_SurfaceAcquireMeas_V9
#define __T_SurfaceAcquireMeas_A11 \
__T_SurfaceAcquireMeas_V10
#define __T_SurfaceAcquireMeas_A12 \
__T_SurfaceAcquireMeas_V11
#define __T_SurfaceAcquireMeas_A13 \
__T_SurfaceAcquireMeas_V12
#define __T_SurfaceAcquireMeas_A14 \
__T_SurfaceAcquireMeas_V13
#define __T_SurfaceAcquireMeas_A15 \
__T_SurfaceAcquireMeas_V14
#define __T_SurfaceAcquireMeas_A16 \
__T_SurfaceAcquireMeas_V15
#define __T_SurfaceAcquireMeas_A17 \
__T_SurfaceAcquireMeas_V16
#define __T_SurfaceAcquireMeas_A18 \
__T_SurfaceAcquireMeas_V17
#define __T_SurfaceAcquireMeas_A19 \
__T_SurfaceAcquireMeas_V18
#define __T_SurfaceAcquireMeas_A20 \
__T_SurfaceAcquireMeas_V19
#define __T_SurfaceAcquireMeas_A21 \
__T_SurfaceAcquireMeas_V20
#define __T_SurfaceAcquireMeas_A22 \
__T_SurfaceAcquireMeas_V21
#define __T_SurfaceAcquireMeas_A23 \
__T_SurfaceAcquireMeas_V22
#define __T_SurfaceAcquireMeas_A24 \
__T_SurfaceAcquireMeas_V23
#define __T_SurfaceAcquireMeas_A25 \
__T_SurfaceAcquireMeas_V24
#define __T_SurfaceAcquireMeas_A26 \
__T_SurfaceAcquireMeas_V25
#define __T_SurfaceAcquireMeas_A27 \
__T_SurfaceAcquireMeas_V26
#define __T_SurfaceAcquireMeas_A28 \
__T_SurfaceAcquireMeas_V27
#define __T_SurfaceAcquireMeas_A29 \
__T_SurfaceAcquireMeas_V28
#define __T_SurfaceAcquireMeas_A30 \
__T_SurfaceAcquireMeas_V29
#define __T_SurfaceAcquireMeas_A31 \
__T_SurfaceAcquireMeas_V30
#define __T_SurfaceAcquireMeas_A32 \
__T_SurfaceAcquireMeas_V31
#define __T_SurfaceAcquireMeas_A33 \
__T_SurfaceAcquireMeas_V32
#define __T_SurfaceAcquireMeas_A34 \
__T_SurfaceAcquireMeas_V33
#define __T_SurfaceAcquireMeas_A35 \
__T_SurfaceAcquireMeas_V34
#define __T_SurfaceAcquireMeas_A36 \
__T_SurfaceAcquireMeas_V35
#define __T_SurfaceAcquireMeas_A37 \
__T_SurfaceAcquireMeas_V36
#define __T_SurfaceAcquireMeas_A38 \
__T_SurfaceAcquireMeas_V37
#define __T_SurfaceAcquireMeas_A39 \
__T_SurfaceAcquireMeas_V38
#define __T_SurfaceAcquireMeas_A40 \
__T_SurfaceAcquireMeas_V39
#define __T_SurfaceAcquireMeas_A41 \
__T_SurfaceAcquireMeas_V40
#define __T_SurfaceAcquireMeas_A42 \
__T_SurfaceAcquireMeas_V41
#define __T_SurfaceAcquireMeas_A43 \
__T_SurfaceAcquireMeas_V42
#define __T_SurfaceAcquireMeas_A44 \
__T_SurfaceAcquireMeas_V43
#define __T_SurfaceAcquireMeas_A45 \
__T_SurfaceAcquireMeas_V44
#define __T_SurfaceAcquireMeas_A46 \
__T_SurfaceAcquireMeas_V45
#define __T_SurfaceAcquireMeas_A47 \
__T_SurfaceAcquireMeas_V46
#define __T_SurfaceAcquireMeas_A48 \
__T_SurfaceAcquireMeas_V47
#define __T_SurfaceAcquireMeas_A49 \
__T_SurfaceAcquireMeas_V48
#define __T_SurfaceAcquireMeas_A50 \
__T_SurfaceAcquireMeas_V49
#define __T_SurfaceAcquireMeas_A51 \
__T_SurfaceAcquireMeas_V50
#define __T_SurfaceAcquireMeas_A52 \
__T_SurfaceAcquireMeas_V51
#define __T_SurfaceAcquireMeas_A53 \
__T_SurfaceAcquireMeas_V52
#define __T_SurfaceAcquireMeas_A54 \
__T_SurfaceAcquireMeas_V53
#define __T_SurfaceAcquireMeas_A55 \
__T_SurfaceAcquireMeas_V54
#define __T_SurfaceAcquireMeas_A56 \
__T_SurfaceAcquireMeas_V55
#define __T_SurfaceAcquireMeas_A57 \
__T_SurfaceAcquireMeas_V56
#define __T_SurfaceAcquireMeas_A58 \
__T_SurfaceAcquireMeas_V57
#define __T_SurfaceAcquireMeas_A59 \
__T_SurfaceAcquireMeas_V58
#define __T_SurfaceAcquireMeas_A60 \
__T_SurfaceAcquireMeas_V59
#define __T_SurfaceAcquireMeas_A61 \
__T_SurfaceAcquireMeas_V60
#define __T_SurfaceAcquireMeas_A62 \
__T_SurfaceAcquireMeas_V61
#define __T_SurfaceAcquireMeas_A63 \
__T_SurfaceAcquireMeas_V62
#define __T_SurfaceAcquireMeas_A64 \
__T_SurfaceAcquireMeas_V63
#define __T_SurfaceAcquireMeas_A65 \
__T_SurfaceAcquireMeas_V64
#define __T_SurfaceAcquireMeas_A66 \
__T_SurfaceAcquireMeas_V65
#define __T_SurfaceAcquireMeas_A67 \
__T_SurfaceAcquireMeas_V66
#define __T_SurfaceAcquireMeas_A68 \
__T_SurfaceAcquireMeas_V67
#define __T_SurfaceAcquireMeas_A69 \
__T_SurfaceAcquireMeas_V68
#define __T_SurfaceAcquireMeas_A70 \
__T_SurfaceAcquireMeas_V69
#define __T_SurfaceAcquireMeas_A71 \
__T_SurfaceAcquireMeas_V70
#define __T_SurfaceAcquireMeas_A72 \
__T_SurfaceAcquireMeas_V71

/* OUTPUT ACTIONS */

#define __T_SurfaceAcquireMeas_A73 \
T_SurfaceAcquireMeas_O_GoodEnd()
#define __T_SurfaceAcquireMeas_A74 \
T_SurfaceAcquireMeas_O_ExecAborted()
#define __T_SurfaceAcquireMeas_A75 \
T_SurfaceAcquireMeas_O_VERIF_A_ApxsGetMeas_ABORT()
#define __T_SurfaceAcquireMeas_A76 \
T_SurfaceAcquireMeas_O_VERIF_A_ApxsGetMeas_ExecAborted()
#define __T_SurfaceAcquireMeas_A77 \
T_SurfaceAcquireMeas_O_VERIF_A_ApxsGetMeas_GoodEnd()
#define __T_SurfaceAcquireMeas_A78 \
T_SurfaceAcquireMeas_O_VERIF_A_ApxsGetMeas_START()
#define __T_SurfaceAcquireMeas_A79 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMonitor_ABORT()
#define __T_SurfaceAcquireMeas_A80 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMonitor_ExecAborted()
#define __T_SurfaceAcquireMeas_A81 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMonitor_GoodEnd()
#define __T_SurfaceAcquireMeas_A82 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMonitor_START()
#define __T_SurfaceAcquireMeas_A83 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMoveHome_ABORT()
#define __T_SurfaceAcquireMeas_A84 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMoveHome_ExecAborted()
#define __T_SurfaceAcquireMeas_A85 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMoveHome_GoodEnd()
#define __T_SurfaceAcquireMeas_A86 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMoveHome_START()
#define __T_SurfaceAcquireMeas_A87 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMoveTo_ABORT()
#define __T_SurfaceAcquireMeas_A88 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMoveTo_ExecAborted()
#define __T_SurfaceAcquireMeas_A89 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMoveTo_GoodEnd()
#define __T_SurfaceAcquireMeas_A90 \
T_SurfaceAcquireMeas_O_VERIF_A_CloseImagerMoveTo_START()
#define __T_SurfaceAcquireMeas_A91 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillAcquireSample_ABORT()
#define __T_SurfaceAcquireMeas_A92 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillAcquireSample_ExecAborted()
#define __T_SurfaceAcquireMeas_A93 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillAcquireSample_GoodEnd()
#define __T_SurfaceAcquireMeas_A94 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillAcquireSample_START()
#define __T_SurfaceAcquireMeas_A95 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillBrush_ABORT()
#define __T_SurfaceAcquireMeas_A96 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillBrush_ExecAborted()
#define __T_SurfaceAcquireMeas_A97 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillBrush_GoodEnd()
#define __T_SurfaceAcquireMeas_A98 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillBrush_START()
#define __T_SurfaceAcquireMeas_A99 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillInsert_ABORT()
#define __T_SurfaceAcquireMeas_A100 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillInsert_ExecAborted()
#define __T_SurfaceAcquireMeas_A101 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillInsert_GoodEnd()
#define __T_SurfaceAcquireMeas_A102 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillInsert_START()
#define __T_SurfaceAcquireMeas_A103 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveHome_ABORT()
#define __T_SurfaceAcquireMeas_A104 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveHome_ExecAborted()
#define __T_SurfaceAcquireMeas_A105 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveHome_GoodEnd()
#define __T_SurfaceAcquireMeas_A106 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveHome_START()
#define __T_SurfaceAcquireMeas_A107 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveInContact_ABORT()
#define __T_SurfaceAcquireMeas_A108 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveInContact_ExecAborted()
#define __T_SurfaceAcquireMeas_A109 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveInContact_GoodEnd()
#define __T_SurfaceAcquireMeas_A110 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveInContact_START()
#define __T_SurfaceAcquireMeas_A111 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveTo_ABORT()
#define __T_SurfaceAcquireMeas_A112 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveTo_ExecAborted()
#define __T_SurfaceAcquireMeas_A113 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveTo_GoodEnd()
#define __T_SurfaceAcquireMeas_A114 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillMoveTo_START()
#define __T_SurfaceAcquireMeas_A115 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillRemove_ABORT()
#define __T_SurfaceAcquireMeas_A116 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillRemove_ExecAborted()
#define __T_SurfaceAcquireMeas_A117 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillRemove_GoodEnd()
#define __T_SurfaceAcquireMeas_A118 \
T_SurfaceAcquireMeas_O_VERIF_A_DrillRemove_START()
#define __T_SurfaceAcquireMeas_A119 \
T_SurfaceAcquireMeas_O_VERIF_A_GoToSleep_ABORT()
#define __T_SurfaceAcquireMeas_A120 \
T_SurfaceAcquireMeas_O_VERIF_A_GoToSleep_ExecAborted()
#define __T_SurfaceAcquireMeas_A121 \
T_SurfaceAcquireMeas_O_VERIF_A_GoToSleep_GoodEnd()
#define __T_SurfaceAcquireMeas_A122 \
T_SurfaceAcquireMeas_O_VERIF_A_GoToSleep_START()
#define __T_SurfaceAcquireMeas_A123 \
T_SurfaceAcquireMeas_O_VERIF_A_IddMoveTo_ABORT()
#define __T_SurfaceAcquireMeas_A124 \
T_SurfaceAcquireMeas_O_VERIF_A_IddMoveTo_ExecAborted()
#define __T_SurfaceAcquireMeas_A125 \
T_SurfaceAcquireMeas_O_VERIF_A_IddMoveTo_GoodEnd()
#define __T_SurfaceAcquireMeas_A126 \
T_SurfaceAcquireMeas_O_VERIF_A_IddMoveTo_START()
#define __T_SurfaceAcquireMeas_A127 \
T_SurfaceAcquireMeas_O_VERIF_A_MoSGetMeas_ABORT()
#define __T_SurfaceAcquireMeas_A128 \
T_SurfaceAcquireMeas_O_VERIF_A_MoSGetMeas_ExecAborted()
#define __T_SurfaceAcquireMeas_A129 \
T_SurfaceAcquireMeas_O_VERIF_A_MoSGetMeas_GoodEnd()
#define __T_SurfaceAcquireMeas_A130 \
T_SurfaceAcquireMeas_O_VERIF_A_MoSGetMeas_START()
#define __T_SurfaceAcquireMeas_A131 \
T_SurfaceAcquireMeas_O_VERIF_A_PanCamMonitor_ABORT()
#define __T_SurfaceAcquireMeas_A132 \
T_SurfaceAcquireMeas_O_VERIF_A_PanCamMonitor_ExecAborted()
#define __T_SurfaceAcquireMeas_A133 \
T_SurfaceAcquireMeas_O_VERIF_A_PanCamMonitor_GoodEnd()
#define __T_SurfaceAcquireMeas_A134 \
T_SurfaceAcquireMeas_O_VERIF_A_PanCamMonitor_START()
#define __T_SurfaceAcquireMeas_A135 \
T_SurfaceAcquireMeas_O_VERIF_A_PasteurAnalyseData_ABORT()
#define __T_SurfaceAcquireMeas_A136 \
T_SurfaceAcquireMeas_O_VERIF_A_PasteurAnalyseData_ExecAborted()
#define __T_SurfaceAcquireMeas_A137 \
T_SurfaceAcquireMeas_O_VERIF_A_PasteurAnalyseData_GoodEnd()
#define __T_SurfaceAcquireMeas_A138 \
T_SurfaceAcquireMeas_O_VERIF_A_PasteurAnalyseData_START()
#define __T_SurfaceAcquireMeas_A139 \
T_SurfaceAcquireMeas_O_VERIF_A_PasteurAnalyseSample_ABORT()
#define __T_SurfaceAcquireMeas_A140 \
T_SurfaceAcquireMeas_O_VERIF_A_PasteurAnalyseSample_ExecAborted()
#define __T_SurfaceAcquireMeas_A141 \
T_SurfaceAcquireMeas_O_VERIF_A_PasteurAnalyseSample_GoodEnd()
#define __T_SurfaceAcquireMeas_A142 \
T_SurfaceAcquireMeas_O_VERIF_A_PasteurAnalyseSample_START()
#define __T_SurfaceAcquireMeas_A143 \
T_SurfaceAcquireMeas_O_VERIF_A_SelfCheck_ABORT()
#define __T_SurfaceAcquireMeas_A144 \
T_SurfaceAcquireMeas_O_VERIF_A_SelfCheck_ExecAborted()
#define __T_SurfaceAcquireMeas_A145 \
T_SurfaceAcquireMeas_O_VERIF_A_SelfCheck_GoodEnd()
#define __T_SurfaceAcquireMeas_A146 \
T_SurfaceAcquireMeas_O_VERIF_A_SelfCheck_START()
#define __T_SurfaceAcquireMeas_A147 \
T_SurfaceAcquireMeas_O_VERIF_CloseImager_ACQUIRED()
#define __T_SurfaceAcquireMeas_A148 \
T_SurfaceAcquireMeas_O_VERIF_CloseImager_RELEASED()
#define __T_SurfaceAcquireMeas_A149 \
T_SurfaceAcquireMeas_O_VERIF_Drill_ACQUIRED()
#define __T_SurfaceAcquireMeas_A150 \
T_SurfaceAcquireMeas_O_VERIF_Drill_RELEASED()
#define __T_SurfaceAcquireMeas_A151 \
T_SurfaceAcquireMeas_O_VERIF_InstrDeplDev_ACQUIRED()
#define __T_SurfaceAcquireMeas_A152 \
T_SurfaceAcquireMeas_O_VERIF_InstrDeplDev_RELEASED()
#define __T_SurfaceAcquireMeas_A153 \
T_SurfaceAcquireMeas_O_VERIF_PanCam_ACQUIRED()
#define __T_SurfaceAcquireMeas_A154 \
T_SurfaceAcquireMeas_O_VERIF_PanCam_RELEASED()
#define __T_SurfaceAcquireMeas_A155 \
T_SurfaceAcquireMeas_O_VERIF_PasteurPL_ACQUIRED()
#define __T_SurfaceAcquireMeas_A156 \
T_SurfaceAcquireMeas_O_VERIF_PasteurPL_RELEASED()
#define __T_SurfaceAcquireMeas_A157 \
T_SurfaceAcquireMeas_O_VERIF_Rover_ACQUIRED()
#define __T_SurfaceAcquireMeas_A158 \
T_SurfaceAcquireMeas_O_VERIF_Rover_RELEASED()
#define __T_SurfaceAcquireMeas_A159 \
T_SurfaceAcquireMeas_O_VERIF_T_SurfaceAcquireSample_ABORT()
#define __T_SurfaceAcquireMeas_A160 \
T_SurfaceAcquireMeas_O_VERIF_T_SurfaceAcquireSample_ExecAborted()
#define __T_SurfaceAcquireMeas_A161 \
T_SurfaceAcquireMeas_O_VERIF_T_SurfaceAcquireSample_GoodEnd()
#define __T_SurfaceAcquireMeas_A162 \
T_SurfaceAcquireMeas_O_VERIF_T_SurfaceAcquireSample_START()
#define __T_SurfaceAcquireMeas_A163 \
T_SurfaceAcquireMeas_O_VERIF_exc_drillblocked()
#define __T_SurfaceAcquireMeas_A164 \
T_SurfaceAcquireMeas_O_VERIF_exc_drillmaxthrust()
#define __T_SurfaceAcquireMeas_A165 \
T_SurfaceAcquireMeas_O_VERIF_exc_drillmovefail()
#define __T_SurfaceAcquireMeas_A166 \
T_SurfaceAcquireMeas_O_VERIF_exc_drillsamplemaxtemp()
#define __T_SurfaceAcquireMeas_A167 \
T_SurfaceAcquireMeas_O_VERIF_exc_evening()
#define __T_SurfaceAcquireMeas_A168 \
T_SurfaceAcquireMeas_O_VERIF_exc_pancamfail()
#define __T_SurfaceAcquireMeas_A169 \
T_SurfaceAcquireMeas_O_VERIF_exc_power_lim()
#define __T_SurfaceAcquireMeas_A170 \
T_SurfaceAcquireMeas_O_REQ_HANDLE_T3_EVENT()
#define __T_SurfaceAcquireMeas_A171 \
T_SurfaceAcquireMeas_O_VERIF_START()
#define __T_SurfaceAcquireMeas_A172 \
T_SurfaceAcquireMeas_O_VERIF_ABORT()
#define __T_SurfaceAcquireMeas_A173 \
T_SurfaceAcquireMeas_O_VERIF_GoodEnd()
#define __T_SurfaceAcquireMeas_A174 \
T_SurfaceAcquireMeas_O_VERIF_ExecAborted()
#define __T_SurfaceAcquireMeas_A175 \
T_SurfaceAcquireMeas_O_VERIF_analysis_positive()
#define __T_SurfaceAcquireMeas_A176 \
T_SurfaceAcquireMeas_O_VERIF_analysis_negative()
#define __T_SurfaceAcquireMeas_A177 \
T_SurfaceAcquireMeas_O_VERIF_sol_day_started()
#define __T_SurfaceAcquireMeas_A178 \
T_SurfaceAcquireMeas_O_VERIF_OK()
#define __T_SurfaceAcquireMeas_A179 \
T_SurfaceAcquireMeas_O_VERIF_ERROR()

/* ASSIGNMENTS */

#define __T_SurfaceAcquireMeas_A180 \
__T_SurfaceAcquireMeas_V0 = _false
#define __T_SurfaceAcquireMeas_A181 \
__T_SurfaceAcquireMeas_V1 = _false
#define __T_SurfaceAcquireMeas_A182 \
__T_SurfaceAcquireMeas_V2 = _false
#define __T_SurfaceAcquireMeas_A183 \
__T_SurfaceAcquireMeas_V3 = _false
#define __T_SurfaceAcquireMeas_A184 \
__T_SurfaceAcquireMeas_V4 = _false
#define __T_SurfaceAcquireMeas_A185 \
__T_SurfaceAcquireMeas_V5 = _false
#define __T_SurfaceAcquireMeas_A186 \
__T_SurfaceAcquireMeas_V6 = _false
#define __T_SurfaceAcquireMeas_A187 \
__T_SurfaceAcquireMeas_V7 = _false
#define __T_SurfaceAcquireMeas_A188 \
__T_SurfaceAcquireMeas_V8 = _false
#define __T_SurfaceAcquireMeas_A189 \
__T_SurfaceAcquireMeas_V9 = _false
#define __T_SurfaceAcquireMeas_A190 \
__T_SurfaceAcquireMeas_V10 = _false
#define __T_SurfaceAcquireMeas_A191 \
__T_SurfaceAcquireMeas_V11 = _false
#define __T_SurfaceAcquireMeas_A192 \
__T_SurfaceAcquireMeas_V12 = _false
#define __T_SurfaceAcquireMeas_A193 \
__T_SurfaceAcquireMeas_V13 = _false
#define __T_SurfaceAcquireMeas_A194 \
__T_SurfaceAcquireMeas_V14 = _false
#define __T_SurfaceAcquireMeas_A195 \
__T_SurfaceAcquireMeas_V15 = _false
#define __T_SurfaceAcquireMeas_A196 \
__T_SurfaceAcquireMeas_V16 = _false
#define __T_SurfaceAcquireMeas_A197 \
__T_SurfaceAcquireMeas_V17 = _false
#define __T_SurfaceAcquireMeas_A198 \
__T_SurfaceAcquireMeas_V18 = _false
#define __T_SurfaceAcquireMeas_A199 \
__T_SurfaceAcquireMeas_V19 = _false
#define __T_SurfaceAcquireMeas_A200 \
__T_SurfaceAcquireMeas_V20 = _false
#define __T_SurfaceAcquireMeas_A201 \
__T_SurfaceAcquireMeas_V21 = _false
#define __T_SurfaceAcquireMeas_A202 \
__T_SurfaceAcquireMeas_V22 = _false
#define __T_SurfaceAcquireMeas_A203 \
__T_SurfaceAcquireMeas_V23 = _false
#define __T_SurfaceAcquireMeas_A204 \
__T_SurfaceAcquireMeas_V24 = _false
#define __T_SurfaceAcquireMeas_A205 \
__T_SurfaceAcquireMeas_V25 = _false
#define __T_SurfaceAcquireMeas_A206 \
__T_SurfaceAcquireMeas_V26 = _false
#define __T_SurfaceAcquireMeas_A207 \
__T_SurfaceAcquireMeas_V27 = _false
#define __T_SurfaceAcquireMeas_A208 \
__T_SurfaceAcquireMeas_V28 = _false
#define __T_SurfaceAcquireMeas_A209 \
__T_SurfaceAcquireMeas_V29 = _false
#define __T_SurfaceAcquireMeas_A210 \
__T_SurfaceAcquireMeas_V30 = _false
#define __T_SurfaceAcquireMeas_A211 \
__T_SurfaceAcquireMeas_V31 = _false
#define __T_SurfaceAcquireMeas_A212 \
__T_SurfaceAcquireMeas_V32 = _false
#define __T_SurfaceAcquireMeas_A213 \
__T_SurfaceAcquireMeas_V33 = _false
#define __T_SurfaceAcquireMeas_A214 \
__T_SurfaceAcquireMeas_V34 = _false
#define __T_SurfaceAcquireMeas_A215 \
__T_SurfaceAcquireMeas_V35 = _false
#define __T_SurfaceAcquireMeas_A216 \
__T_SurfaceAcquireMeas_V36 = _false
#define __T_SurfaceAcquireMeas_A217 \
__T_SurfaceAcquireMeas_V37 = _false
#define __T_SurfaceAcquireMeas_A218 \
__T_SurfaceAcquireMeas_V38 = _false
#define __T_SurfaceAcquireMeas_A219 \
__T_SurfaceAcquireMeas_V39 = _false
#define __T_SurfaceAcquireMeas_A220 \
__T_SurfaceAcquireMeas_V40 = _false
#define __T_SurfaceAcquireMeas_A221 \
__T_SurfaceAcquireMeas_V41 = _false
#define __T_SurfaceAcquireMeas_A222 \
__T_SurfaceAcquireMeas_V42 = _false
#define __T_SurfaceAcquireMeas_A223 \
__T_SurfaceAcquireMeas_V43 = _false
#define __T_SurfaceAcquireMeas_A224 \
__T_SurfaceAcquireMeas_V44 = _false
#define __T_SurfaceAcquireMeas_A225 \
__T_SurfaceAcquireMeas_V45 = _false
#define __T_SurfaceAcquireMeas_A226 \
__T_SurfaceAcquireMeas_V46 = _false
#define __T_SurfaceAcquireMeas_A227 \
__T_SurfaceAcquireMeas_V47 = _false
#define __T_SurfaceAcquireMeas_A228 \
__T_SurfaceAcquireMeas_V48 = _false
#define __T_SurfaceAcquireMeas_A229 \
__T_SurfaceAcquireMeas_V49 = _false
#define __T_SurfaceAcquireMeas_A230 \
__T_SurfaceAcquireMeas_V50 = _false
#define __T_SurfaceAcquireMeas_A231 \
__T_SurfaceAcquireMeas_V51 = _false
#define __T_SurfaceAcquireMeas_A232 \
__T_SurfaceAcquireMeas_V52 = _false
#define __T_SurfaceAcquireMeas_A233 \
__T_SurfaceAcquireMeas_V53 = _false
#define __T_SurfaceAcquireMeas_A234 \
__T_SurfaceAcquireMeas_V54 = _false
#define __T_SurfaceAcquireMeas_A235 \
__T_SurfaceAcquireMeas_V55 = _false
#define __T_SurfaceAcquireMeas_A236 \
__T_SurfaceAcquireMeas_V56 = _false
#define __T_SurfaceAcquireMeas_A237 \
__T_SurfaceAcquireMeas_V57 = _false
#define __T_SurfaceAcquireMeas_A238 \
__T_SurfaceAcquireMeas_V58 = _false
#define __T_SurfaceAcquireMeas_A239 \
__T_SurfaceAcquireMeas_V59 = _false
#define __T_SurfaceAcquireMeas_A240 \
__T_SurfaceAcquireMeas_V60 = _false
#define __T_SurfaceAcquireMeas_A241 \
__T_SurfaceAcquireMeas_V61 = _false
#define __T_SurfaceAcquireMeas_A242 \
__T_SurfaceAcquireMeas_V62 = _false
#define __T_SurfaceAcquireMeas_A243 \
__T_SurfaceAcquireMeas_V63 = _false
#define __T_SurfaceAcquireMeas_A244 \
__T_SurfaceAcquireMeas_V64 = _false
#define __T_SurfaceAcquireMeas_A245 \
__T_SurfaceAcquireMeas_V65 = _false
#define __T_SurfaceAcquireMeas_A246 \
__T_SurfaceAcquireMeas_V66 = _false
#define __T_SurfaceAcquireMeas_A247 \
__T_SurfaceAcquireMeas_V67 = _false
#define __T_SurfaceAcquireMeas_A248 \
__T_SurfaceAcquireMeas_V68 = _false
#define __T_SurfaceAcquireMeas_A249 \
__T_SurfaceAcquireMeas_V69 = _false
#define __T_SurfaceAcquireMeas_A250 \
__T_SurfaceAcquireMeas_V70 = _false
#define __T_SurfaceAcquireMeas_A251 \
__T_SurfaceAcquireMeas_V71 = _false

/* PROCEDURE CALLS */

#define __T_SurfaceAcquireMeas_A252 \
Activate_PanCamMonitor(5)
#define __T_SurfaceAcquireMeas_A253 \
Activate_DrillMoveTo("surfacetarget")
#define __T_SurfaceAcquireMeas_A254 \
Activate_DrillMoveInContact(10.0)
#define __T_SurfaceAcquireMeas_A255 \
Activate_DrillInsert(10.0)
#define __T_SurfaceAcquireMeas_A256 \
Activate_DrillAcquireSample()
#define __T_SurfaceAcquireMeas_A257 \
Activate_DrillRemove(10.0)

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int T_SurfaceAcquireMeas_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __T_SurfaceAcquireMeas__reset_input () {
__T_SurfaceAcquireMeas_V0 = _false;
__T_SurfaceAcquireMeas_V1 = _false;
__T_SurfaceAcquireMeas_V2 = _false;
__T_SurfaceAcquireMeas_V3 = _false;
__T_SurfaceAcquireMeas_V4 = _false;
__T_SurfaceAcquireMeas_V5 = _false;
__T_SurfaceAcquireMeas_V6 = _false;
__T_SurfaceAcquireMeas_V7 = _false;
__T_SurfaceAcquireMeas_V8 = _false;
__T_SurfaceAcquireMeas_V9 = _false;
__T_SurfaceAcquireMeas_V10 = _false;
__T_SurfaceAcquireMeas_V11 = _false;
__T_SurfaceAcquireMeas_V12 = _false;
__T_SurfaceAcquireMeas_V13 = _false;
__T_SurfaceAcquireMeas_V14 = _false;
__T_SurfaceAcquireMeas_V15 = _false;
__T_SurfaceAcquireMeas_V16 = _false;
__T_SurfaceAcquireMeas_V17 = _false;
__T_SurfaceAcquireMeas_V18 = _false;
__T_SurfaceAcquireMeas_V19 = _false;
__T_SurfaceAcquireMeas_V20 = _false;
__T_SurfaceAcquireMeas_V21 = _false;
__T_SurfaceAcquireMeas_V22 = _false;
__T_SurfaceAcquireMeas_V23 = _false;
__T_SurfaceAcquireMeas_V24 = _false;
__T_SurfaceAcquireMeas_V25 = _false;
__T_SurfaceAcquireMeas_V26 = _false;
__T_SurfaceAcquireMeas_V27 = _false;
__T_SurfaceAcquireMeas_V28 = _false;
__T_SurfaceAcquireMeas_V29 = _false;
__T_SurfaceAcquireMeas_V30 = _false;
__T_SurfaceAcquireMeas_V31 = _false;
__T_SurfaceAcquireMeas_V32 = _false;
__T_SurfaceAcquireMeas_V33 = _false;
__T_SurfaceAcquireMeas_V34 = _false;
__T_SurfaceAcquireMeas_V35 = _false;
__T_SurfaceAcquireMeas_V36 = _false;
__T_SurfaceAcquireMeas_V37 = _false;
__T_SurfaceAcquireMeas_V38 = _false;
__T_SurfaceAcquireMeas_V39 = _false;
__T_SurfaceAcquireMeas_V40 = _false;
__T_SurfaceAcquireMeas_V41 = _false;
__T_SurfaceAcquireMeas_V42 = _false;
__T_SurfaceAcquireMeas_V43 = _false;
__T_SurfaceAcquireMeas_V44 = _false;
__T_SurfaceAcquireMeas_V45 = _false;
__T_SurfaceAcquireMeas_V46 = _false;
__T_SurfaceAcquireMeas_V47 = _false;
__T_SurfaceAcquireMeas_V48 = _false;
__T_SurfaceAcquireMeas_V49 = _false;
__T_SurfaceAcquireMeas_V50 = _false;
__T_SurfaceAcquireMeas_V51 = _false;
__T_SurfaceAcquireMeas_V52 = _false;
__T_SurfaceAcquireMeas_V53 = _false;
__T_SurfaceAcquireMeas_V54 = _false;
__T_SurfaceAcquireMeas_V55 = _false;
__T_SurfaceAcquireMeas_V56 = _false;
__T_SurfaceAcquireMeas_V57 = _false;
__T_SurfaceAcquireMeas_V58 = _false;
__T_SurfaceAcquireMeas_V59 = _false;
__T_SurfaceAcquireMeas_V60 = _false;
__T_SurfaceAcquireMeas_V61 = _false;
__T_SurfaceAcquireMeas_V62 = _false;
__T_SurfaceAcquireMeas_V63 = _false;
__T_SurfaceAcquireMeas_V64 = _false;
__T_SurfaceAcquireMeas_V65 = _false;
__T_SurfaceAcquireMeas_V66 = _false;
__T_SurfaceAcquireMeas_V67 = _false;
__T_SurfaceAcquireMeas_V68 = _false;
__T_SurfaceAcquireMeas_V69 = _false;
__T_SurfaceAcquireMeas_V70 = _false;
__T_SurfaceAcquireMeas_V71 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __T_SurfaceAcquireMeas_R[157] = {_true,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int T_SurfaceAcquireMeas () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[532];
E[0] = __T_SurfaceAcquireMeas_R[144]||__T_SurfaceAcquireMeas_R[145];
E[1] = E[0]&&!(__T_SurfaceAcquireMeas_R[0]);
E[2] = __T_SurfaceAcquireMeas_R[102]||__T_SurfaceAcquireMeas_R[103];
E[3] = E[2]&&!(__T_SurfaceAcquireMeas_R[0]);
E[4] = __T_SurfaceAcquireMeas_R[72]&&!(__T_SurfaceAcquireMeas_R[0]);
E[5] = E[4]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 44\n"),
#endif
__T_SurfaceAcquireMeas_A44);
E[6] = E[3]&&E[5];
E[3] = E[3]&&!(E[5]);
E[7] = __T_SurfaceAcquireMeas_R[71]&&!(__T_SurfaceAcquireMeas_R[0]);
E[8] = E[7]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 29\n"),
#endif
__T_SurfaceAcquireMeas_A29);
E[9] = E[3]&&E[8];
E[10] = __T_SurfaceAcquireMeas_R[104]||__T_SurfaceAcquireMeas_R[105];
E[11] = E[10]&&!(__T_SurfaceAcquireMeas_R[0]);
E[12] = __T_SurfaceAcquireMeas_R[77]&&!(__T_SurfaceAcquireMeas_R[0]);
E[13] = E[12]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 41\n"),
#endif
__T_SurfaceAcquireMeas_A41);
E[14] = E[11]&&E[13];
E[11] = E[11]&&!(E[13]);
E[15] = __T_SurfaceAcquireMeas_R[76]&&!(__T_SurfaceAcquireMeas_R[0]);
E[16] = E[15]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__T_SurfaceAcquireMeas_A21);
E[17] = __T_SurfaceAcquireMeas_R[78]&&!(__T_SurfaceAcquireMeas_R[0]);
E[18] = E[17]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 20\n"),
#endif
__T_SurfaceAcquireMeas_A20);
E[19] = E[16]||E[18];
E[20] = E[11]&&E[19];
E[21] = __T_SurfaceAcquireMeas_R[106]||__T_SurfaceAcquireMeas_R[107];
E[22] = E[21]&&!(__T_SurfaceAcquireMeas_R[0]);
E[23] = __T_SurfaceAcquireMeas_R[81]&&!(__T_SurfaceAcquireMeas_R[0]);
E[24] = E[23]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__T_SurfaceAcquireMeas_A19);
E[25] = __T_SurfaceAcquireMeas_R[84]&&!(__T_SurfaceAcquireMeas_R[0]);
E[26] = E[25]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__T_SurfaceAcquireMeas_A18);
E[27] = E[24]||E[26];
E[28] = E[22]&&E[27];
E[29] = __T_SurfaceAcquireMeas_R[108]||__T_SurfaceAcquireMeas_R[109];
E[30] = E[29]&&!(__T_SurfaceAcquireMeas_R[0]);
E[31] = __T_SurfaceAcquireMeas_R[88]&&!(__T_SurfaceAcquireMeas_R[0]);
E[32] = E[31]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 40\n"),
#endif
__T_SurfaceAcquireMeas_A40);
E[33] = E[30]&&E[32];
E[30] = E[30]&&!(E[32]);
E[34] = __T_SurfaceAcquireMeas_R[87]&&!(__T_SurfaceAcquireMeas_R[0]);
E[35] = E[34]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__T_SurfaceAcquireMeas_A15);
E[36] = __T_SurfaceAcquireMeas_R[89]&&!(__T_SurfaceAcquireMeas_R[0]);
E[37] = E[36]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__T_SurfaceAcquireMeas_A14);
E[38] = E[35]||E[37];
E[39] = E[30]&&E[38];
E[40] = __T_SurfaceAcquireMeas_R[110]||__T_SurfaceAcquireMeas_R[111];
E[41] = E[40]&&!(__T_SurfaceAcquireMeas_R[0]);
E[42] = __T_SurfaceAcquireMeas_R[93]&&!(__T_SurfaceAcquireMeas_R[0]);
E[43] = E[42]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 42\n"),
#endif
__T_SurfaceAcquireMeas_A42);
E[44] = E[41]&&E[43];
E[41] = E[41]&&!(E[43]);
E[45] = __T_SurfaceAcquireMeas_R[92]&&!(__T_SurfaceAcquireMeas_R[0]);
E[46] = E[45]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__T_SurfaceAcquireMeas_A11);
E[47] = __T_SurfaceAcquireMeas_R[94]&&!(__T_SurfaceAcquireMeas_R[0]);
E[48] = E[47]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__T_SurfaceAcquireMeas_A10);
E[49] = E[46]||E[48];
E[50] = E[41]&&E[49];
E[51] = __T_SurfaceAcquireMeas_R[112]||__T_SurfaceAcquireMeas_R[113];
E[52] = E[51]&&!(__T_SurfaceAcquireMeas_R[0]);
E[53] = __T_SurfaceAcquireMeas_R[98]&&!(__T_SurfaceAcquireMeas_R[0]);
E[54] = E[53]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 39\n"),
#endif
__T_SurfaceAcquireMeas_A39);
E[55] = E[52]&&E[54];
E[52] = E[52]&&!(E[54]);
E[56] = __T_SurfaceAcquireMeas_R[97]&&!(__T_SurfaceAcquireMeas_R[0]);
E[57] = E[56]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 23\n"),
#endif
__T_SurfaceAcquireMeas_A23);
E[58] = __T_SurfaceAcquireMeas_R[99]&&!(__T_SurfaceAcquireMeas_R[0]);
E[59] = E[58]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__T_SurfaceAcquireMeas_A22);
E[60] = E[57]||E[59];
E[61] = E[52]&&E[60];
E[62] = E[6]||E[9]||E[14]||E[20]||E[28]||E[33]||E[39]||E[44]||E[50]||E[55]||E[61];
E[63] = E[1]&&!(E[62]);
E[64] = __T_SurfaceAcquireMeas_R[144]&&E[63];
E[52] = E[52]&&!(E[60]);
E[65] = __T_SurfaceAcquireMeas_R[112]&&E[52];
E[58] = E[58]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 22\n"),
#endif
__T_SurfaceAcquireMeas_A22));
E[58] = __T_SurfaceAcquireMeas_R[99]&&E[58];
E[66] = E[58]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 50\n"),
#endif
__T_SurfaceAcquireMeas_A50);
E[67] = E[65]&&E[66];
E[68] = E[2]||E[10];
E[51] = E[21]||E[29]||E[40]||E[51]||E[68];
E[40] = E[51]||__T_SurfaceAcquireMeas_R[114];
E[11] = E[11]&&!(E[19]);
E[29] = __T_SurfaceAcquireMeas_R[104]&&E[11];
E[17] = E[17]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 20\n"),
#endif
__T_SurfaceAcquireMeas_A20));
E[17] = __T_SurfaceAcquireMeas_R[78]&&E[17];
E[21] = E[17]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 52\n"),
#endif
__T_SurfaceAcquireMeas_A52);
E[69] = E[29]&&E[21];
E[3] = E[3]&&!(E[8]);
E[70] = __T_SurfaceAcquireMeas_R[102]&&E[3];
E[71] = __T_SurfaceAcquireMeas_R[70]&&!(__T_SurfaceAcquireMeas_R[0]);
E[72] = __T_SurfaceAcquireMeas_R[116]&&!(__T_SurfaceAcquireMeas_R[0]);
E[73] = __T_SurfaceAcquireMeas_R[101]&&!(__T_SurfaceAcquireMeas_R[0]);
E[74] = __T_SurfaceAcquireMeas_R[154]&&!(__T_SurfaceAcquireMeas_R[0]);
E[75] = __T_SurfaceAcquireMeas_R[124]&&!(__T_SurfaceAcquireMeas_R[0]);
E[76] = E[75]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__T_SurfaceAcquireMeas_A2);
E[77] = __T_SurfaceAcquireMeas_R[125]||__T_SurfaceAcquireMeas_R[126];
E[78] = E[77]&&!(__T_SurfaceAcquireMeas_R[0]);
E[79] = __T_SurfaceAcquireMeas_R[36]&&!(__T_SurfaceAcquireMeas_R[0]);
E[80] = E[79]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__T_SurfaceAcquireMeas_A19);
E[81] = __T_SurfaceAcquireMeas_R[39]&&!(__T_SurfaceAcquireMeas_R[0]);
E[82] = E[81]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__T_SurfaceAcquireMeas_A18);
E[83] = E[80]||E[82];
E[84] = E[78]&&E[83];
E[85] = __T_SurfaceAcquireMeas_R[127]||__T_SurfaceAcquireMeas_R[128];
E[86] = E[85]&&!(__T_SurfaceAcquireMeas_R[0]);
E[87] = __T_SurfaceAcquireMeas_R[50]&&!(__T_SurfaceAcquireMeas_R[0]);
E[88] = E[87]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 26\n"),
#endif
__T_SurfaceAcquireMeas_A26);
E[89] = __T_SurfaceAcquireMeas_R[53]&&!(__T_SurfaceAcquireMeas_R[0]);
E[90] = E[89]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 25\n"),
#endif
__T_SurfaceAcquireMeas_A25);
E[91] = E[88]||E[90];
E[92] = E[86]&&E[91];
E[93] = __T_SurfaceAcquireMeas_R[129]||__T_SurfaceAcquireMeas_R[130];
E[94] = E[93]&&!(__T_SurfaceAcquireMeas_R[0]);
E[95] = __T_SurfaceAcquireMeas_R[20]&&!(__T_SurfaceAcquireMeas_R[0]);
E[96] = E[95]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 40\n"),
#endif
__T_SurfaceAcquireMeas_A40);
E[97] = E[94]&&E[96];
E[94] = E[94]&&!(E[96]);
E[98] = __T_SurfaceAcquireMeas_R[19]&&!(__T_SurfaceAcquireMeas_R[0]);
E[99] = E[98]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__T_SurfaceAcquireMeas_A13);
E[100] = __T_SurfaceAcquireMeas_R[23]&&!(__T_SurfaceAcquireMeas_R[0]);
E[101] = E[100]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__T_SurfaceAcquireMeas_A12);
E[102] = E[99]||E[101];
E[103] = E[94]&&E[102];
E[104] = __T_SurfaceAcquireMeas_R[131]||__T_SurfaceAcquireMeas_R[132];
E[105] = E[104]&&!(__T_SurfaceAcquireMeas_R[0]);
E[106] = __T_SurfaceAcquireMeas_R[3]&&!(__T_SurfaceAcquireMeas_R[0]);
E[107] = E[106]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__T_SurfaceAcquireMeas_A5);
E[108] = E[105]&&E[107];
E[109] = __T_SurfaceAcquireMeas_R[133]||__T_SurfaceAcquireMeas_R[134];
E[110] = E[109]&&!(__T_SurfaceAcquireMeas_R[0]);
E[111] = __T_SurfaceAcquireMeas_R[32]&&!(__T_SurfaceAcquireMeas_R[0]);
E[112] = E[111]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 41\n"),
#endif
__T_SurfaceAcquireMeas_A41);
E[113] = E[110]&&E[112];
E[110] = E[110]&&!(E[112]);
E[114] = __T_SurfaceAcquireMeas_R[31]&&!(__T_SurfaceAcquireMeas_R[0]);
E[115] = E[114]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__T_SurfaceAcquireMeas_A17);
E[116] = __T_SurfaceAcquireMeas_R[33]&&!(__T_SurfaceAcquireMeas_R[0]);
E[117] = E[116]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 16\n"),
#endif
__T_SurfaceAcquireMeas_A16);
E[118] = E[115]||E[117];
E[119] = E[110]&&E[118];
E[120] = __T_SurfaceAcquireMeas_R[135]||__T_SurfaceAcquireMeas_R[136];
E[121] = E[120]&&!(__T_SurfaceAcquireMeas_R[0]);
E[122] = E[121]&&E[91];
E[123] = __T_SurfaceAcquireMeas_R[137]||__T_SurfaceAcquireMeas_R[138];
E[124] = E[123]&&!(__T_SurfaceAcquireMeas_R[0]);
E[125] = __T_SurfaceAcquireMeas_R[56]&&!(__T_SurfaceAcquireMeas_R[0]);
E[126] = E[125]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 28\n"),
#endif
__T_SurfaceAcquireMeas_A28);
E[127] = __T_SurfaceAcquireMeas_R[57]&&!(__T_SurfaceAcquireMeas_R[0]);
E[128] = E[127]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 27\n"),
#endif
__T_SurfaceAcquireMeas_A27);
E[129] = E[126]||E[128];
E[130] = E[124]&&E[129];
E[131] = __T_SurfaceAcquireMeas_R[139]||__T_SurfaceAcquireMeas_R[140];
E[132] = E[131]&&!(__T_SurfaceAcquireMeas_R[0]);
E[133] = __T_SurfaceAcquireMeas_R[15]&&!(__T_SurfaceAcquireMeas_R[0]);
E[134] = E[133]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__T_SurfaceAcquireMeas_A4);
E[135] = __T_SurfaceAcquireMeas_R[16]&&!(__T_SurfaceAcquireMeas_R[0]);
E[136] = E[135]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__T_SurfaceAcquireMeas_A3);
E[137] = E[134]||E[136];
E[138] = E[132]&&E[137];
E[139] = __T_SurfaceAcquireMeas_R[141]||__T_SurfaceAcquireMeas_R[142];
E[140] = E[139]&&!(__T_SurfaceAcquireMeas_R[0]);
E[141] = __T_SurfaceAcquireMeas_R[121]&&!(__T_SurfaceAcquireMeas_R[0]);
E[142] = E[141]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 31\n"),
#endif
__T_SurfaceAcquireMeas_A31);
E[143] = __T_SurfaceAcquireMeas_R[122]&&!(__T_SurfaceAcquireMeas_R[0]);
E[144] = E[143]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 30\n"),
#endif
__T_SurfaceAcquireMeas_A30);
E[145] = E[142]||E[144];
E[146] = E[140]&&E[145];
E[1] = E[1]&&E[62];
E[147] = __T_SurfaceAcquireMeas_R[147]||__T_SurfaceAcquireMeas_R[148];
E[148] = E[147]&&!(__T_SurfaceAcquireMeas_R[0]);
E[149] = __T_SurfaceAcquireMeas_R[47]&&!(__T_SurfaceAcquireMeas_R[0]);
E[150] = E[149]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 24\n"),
#endif
__T_SurfaceAcquireMeas_A24);
E[151] = E[148]&&E[150];
E[152] = __T_SurfaceAcquireMeas_R[150]||__T_SurfaceAcquireMeas_R[151];
E[153] = E[152]&&!(__T_SurfaceAcquireMeas_R[0]);
E[154] = __T_SurfaceAcquireMeas_R[64]&&!(__T_SurfaceAcquireMeas_R[0]);
E[155] = E[154]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 38\n"),
#endif
__T_SurfaceAcquireMeas_A38);
E[156] = __T_SurfaceAcquireMeas_R[65]&&!(__T_SurfaceAcquireMeas_R[0]);
E[157] = E[156]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 37\n"),
#endif
__T_SurfaceAcquireMeas_A37);
E[158] = __T_SurfaceAcquireMeas_R[66]&&!(__T_SurfaceAcquireMeas_R[0]);
E[159] = E[158]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 36\n"),
#endif
__T_SurfaceAcquireMeas_A36);
E[160] = __T_SurfaceAcquireMeas_R[67]&&!(__T_SurfaceAcquireMeas_R[0]);
E[161] = E[160]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 34\n"),
#endif
__T_SurfaceAcquireMeas_A34);
E[162] = __T_SurfaceAcquireMeas_R[68]&&!(__T_SurfaceAcquireMeas_R[0]);
E[163] = E[162]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 35\n"),
#endif
__T_SurfaceAcquireMeas_A35);
E[164] = E[155]||E[157]||E[159]||E[161]||E[163];
E[165] = E[153]&&E[164];
E[166] = E[76]||E[84]||E[92]||E[97]||E[103]||E[108]||E[113]||E[119]||E[122]||E[130]||E[138]||E[146]||E[1]||E[151]||E[165];
E[167] = E[74]&&E[166];
E[168] = E[167]&&!(E[107]);
E[167] = (E[167]&&E[107])||E[168];
E[169] = __T_SurfaceAcquireMeas_R[7]&&!(__T_SurfaceAcquireMeas_R[0]);
E[170] = E[169]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__T_SurfaceAcquireMeas_A7);
E[171] = __T_SurfaceAcquireMeas_R[8]&&!(__T_SurfaceAcquireMeas_R[0]);
E[172] = E[171]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__T_SurfaceAcquireMeas_A6);
E[173] = E[170]||E[172];
E[174] = E[167]&&!(E[173]);
E[173] = (E[167]&&E[173])||E[174];
E[167] = __T_SurfaceAcquireMeas_R[11]&&!(__T_SurfaceAcquireMeas_R[0]);
E[175] = E[167]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__T_SurfaceAcquireMeas_A9);
E[176] = __T_SurfaceAcquireMeas_R[12]&&!(__T_SurfaceAcquireMeas_R[0]);
E[177] = E[176]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__T_SurfaceAcquireMeas_A8);
E[178] = E[175]||E[177];
E[179] = E[173]&&!(E[178]);
E[178] = (E[173]&&E[178])||E[179];
E[173] = E[178]&&!(E[137]);
E[178] = (E[178]&&E[137])||E[173];
E[180] = E[178]&&!(E[102]);
E[178] = (E[178]&&E[102])||E[180];
E[181] = __T_SurfaceAcquireMeas_R[26]&&!(__T_SurfaceAcquireMeas_R[0]);
E[182] = E[181]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__T_SurfaceAcquireMeas_A15);
E[183] = __T_SurfaceAcquireMeas_R[28]&&!(__T_SurfaceAcquireMeas_R[0]);
E[184] = E[183]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__T_SurfaceAcquireMeas_A14);
E[185] = E[182]||E[184];
E[186] = E[178]&&!(E[185]);
E[185] = (E[178]&&E[185])||E[186];
E[178] = E[185]&&!(E[118]);
E[185] = (E[185]&&E[118])||E[178];
E[187] = E[185]&&!(E[83]);
E[185] = (E[185]&&E[83])||E[187];
E[188] = __T_SurfaceAcquireMeas_R[42]&&!(__T_SurfaceAcquireMeas_R[0]);
E[189] = E[188]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__T_SurfaceAcquireMeas_A21);
E[190] = __T_SurfaceAcquireMeas_R[44]&&!(__T_SurfaceAcquireMeas_R[0]);
E[191] = E[190]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 20\n"),
#endif
__T_SurfaceAcquireMeas_A20);
E[192] = E[189]||E[191];
E[193] = E[185]&&!(E[192]);
E[192] = (E[185]&&E[192])||E[193];
E[185] = E[192]&&!(E[150]);
E[192] = (E[192]&&E[150])||E[185];
E[194] = E[192]&&!(E[91]);
E[192] = (E[192]&&E[91])||E[194];
E[195] = E[192]&&!(E[129]);
E[192] = (E[192]&&E[129])||E[195];
E[196] = __T_SurfaceAcquireMeas_R[60]&&!(__T_SurfaceAcquireMeas_R[0]);
E[197] = E[196]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 33\n"),
#endif
__T_SurfaceAcquireMeas_A33);
E[198] = __T_SurfaceAcquireMeas_R[61]&&!(__T_SurfaceAcquireMeas_R[0]);
E[199] = E[198]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 32\n"),
#endif
__T_SurfaceAcquireMeas_A32);
E[200] = E[197]||E[199];
E[201] = E[192]&&!(E[200]);
E[200] = (E[192]&&E[200])||E[201];
E[192] = E[200]&&!(E[164]);
E[200] = (E[200]&&E[164])||E[192];
E[202] = E[200]&&!(E[62]);
E[203] = E[73]&&E[202];
E[204] = E[203]||E[6]||E[9]||E[14]||E[20]||E[28]||E[33]||E[39]||E[44]||E[50]||E[55]||E[61];
E[205] = E[72]&&E[204];
E[206] = E[205]&&!(E[8]);
E[207] = E[69]||E[206];
E[208] = E[71]&&E[207];
E[209] = __T_SurfaceAcquireMeas_R[102]&&E[70]&&!(E[208]);
E[3] = (E[70]&&E[208])||(__T_SurfaceAcquireMeas_R[103]&&E[3]);
E[2] = (E[68]&&!(E[2]))||E[209]||E[3];
E[29] = E[29]&&!(E[21]);
E[70] = __T_SurfaceAcquireMeas_R[75]&&!(__T_SurfaceAcquireMeas_R[0]);
E[206] = (E[205]&&E[8])||E[206];
E[205] = E[206]&&!(E[19]);
E[210] = E[70]&&E[205];
E[211] = __T_SurfaceAcquireMeas_R[104]&&E[29]&&!(E[210]);
E[11] = (E[29]&&E[210])||(__T_SurfaceAcquireMeas_R[105]&&E[11]);
E[10] = (E[68]&&!(E[10]))||E[211]||E[11];
E[68] = E[10]||E[69];
E[69] = E[69]&&E[2]&&E[68];
if (E[69]) {
__T_SurfaceAcquireMeas_A254;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A254\n");
#endif
}
E[22] = E[22]&&!(E[27]);
E[29] = __T_SurfaceAcquireMeas_R[106]&&E[22];
E[25] = E[25]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__T_SurfaceAcquireMeas_A18));
E[25] = __T_SurfaceAcquireMeas_R[84]&&E[25];
E[212] = E[25]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 51\n"),
#endif
__T_SurfaceAcquireMeas_A51);
E[213] = E[29]&&!(E[212]);
E[214] = __T_SurfaceAcquireMeas_R[80]&&!(__T_SurfaceAcquireMeas_R[0]);
E[19] = (E[206]&&E[19])||E[205];
E[206] = E[19]&&!(E[27]);
E[215] = E[214]&&E[206];
E[216] = E[69]||(__T_SurfaceAcquireMeas_R[106]&&E[213]&&!(E[215]));
E[29] = E[29]&&E[212];
if (E[29]) {
__T_SurfaceAcquireMeas_A255;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A255\n");
#endif
}
E[30] = E[30]&&!(E[38]);
E[217] = __T_SurfaceAcquireMeas_R[108]&&E[30];
E[36] = E[36]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__T_SurfaceAcquireMeas_A14));
E[36] = __T_SurfaceAcquireMeas_R[89]&&E[36];
E[218] = E[36]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 50\n"),
#endif
__T_SurfaceAcquireMeas_A50);
E[219] = E[217]&&!(E[218]);
E[220] = __T_SurfaceAcquireMeas_R[86]&&!(__T_SurfaceAcquireMeas_R[0]);
E[27] = (E[19]&&E[27])||E[206];
E[19] = E[27]&&!(E[38]);
E[221] = E[220]&&E[19];
E[222] = E[29]||(__T_SurfaceAcquireMeas_R[108]&&E[219]&&!(E[221]));
E[217] = E[217]&&E[218];
if (E[217]) {
__T_SurfaceAcquireMeas_A256;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A256\n");
#endif
}
E[41] = E[41]&&!(E[49]);
E[223] = __T_SurfaceAcquireMeas_R[110]&&E[41];
E[47] = E[47]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 10\n"),
#endif
__T_SurfaceAcquireMeas_A10));
E[47] = __T_SurfaceAcquireMeas_R[94]&&E[47];
E[224] = E[47]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 53\n"),
#endif
__T_SurfaceAcquireMeas_A53);
E[225] = E[223]&&!(E[224]);
E[226] = __T_SurfaceAcquireMeas_R[91]&&!(__T_SurfaceAcquireMeas_R[0]);
E[38] = (E[27]&&E[38])||E[19];
E[27] = E[38]&&!(E[49]);
E[227] = E[226]&&E[27];
E[228] = E[217]||(__T_SurfaceAcquireMeas_R[110]&&E[225]&&!(E[227]));
E[223] = E[223]&&E[224];
if (E[223]) {
__T_SurfaceAcquireMeas_A257;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A257\n");
#endif
}
E[65] = E[65]&&!(E[66]);
E[229] = __T_SurfaceAcquireMeas_R[96]&&!(__T_SurfaceAcquireMeas_R[0]);
E[49] = (E[38]&&E[49])||E[27];
E[38] = E[49]&&!(E[60]);
E[230] = E[229]&&E[38];
E[231] = E[223]||(__T_SurfaceAcquireMeas_R[112]&&E[65]&&!(E[230]));
E[52] = (E[65]&&E[230])||(__T_SurfaceAcquireMeas_R[113]&&E[52]);
E[41] = (E[225]&&E[227])||(__T_SurfaceAcquireMeas_R[111]&&E[41]);
E[30] = (E[219]&&E[221])||(__T_SurfaceAcquireMeas_R[109]&&E[30]);
E[22] = (E[213]&&E[215])||(__T_SurfaceAcquireMeas_R[107]&&E[22]);
E[10] = (E[209]||E[3]||E[211]||E[11])&&E[2]&&E[10];
E[51] = (E[40]&&!(E[51]))||E[216]||E[222]||E[228]||E[231]||E[52]||E[41]||E[30]||E[22]||E[10];
E[213] = E[51]||E[67];
E[219] = __T_SurfaceAcquireMeas_R[114]&&!(__T_SurfaceAcquireMeas_R[0]);
E[225] = (E[40]&&!(__T_SurfaceAcquireMeas_R[114]))||E[219];
E[67] = E[67]&&E[213]&&E[225];
E[65] = __T_SurfaceAcquireMeas_R[143]&&!(__T_SurfaceAcquireMeas_R[0]);
E[232] = E[65]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 70\n"),
#endif
__T_SurfaceAcquireMeas_A70));
E[233] = E[232]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 71\n"),
#endif
__T_SurfaceAcquireMeas_A71);
E[233] = (E[64]&&E[67])||E[233];
E[234] = E[93]||E[104];
E[77] = E[85]||E[109]||E[120]||E[123]||E[131]||E[139]||__T_SurfaceAcquireMeas_R[143]||E[0]||E[234]||E[77];
E[152] = __T_SurfaceAcquireMeas_R[146]||E[147]||__T_SurfaceAcquireMeas_R[149]||E[152];
E[147] = E[77]||E[152];
E[83] = E[78]&&!(E[83]);
E[78] = __T_SurfaceAcquireMeas_R[125]&&E[83];
E[81] = E[81]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__T_SurfaceAcquireMeas_A18));
E[81] = __T_SurfaceAcquireMeas_R[39]&&E[81];
E[0] = E[81]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 51\n"),
#endif
__T_SurfaceAcquireMeas_A51);
E[139] = E[78]&&E[0];
E[86] = E[86]&&!(E[91]);
E[131] = __T_SurfaceAcquireMeas_R[127]&&E[86];
E[89] = E[89]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 25\n"),
#endif
__T_SurfaceAcquireMeas_A25));
E[89] = __T_SurfaceAcquireMeas_R[53]&&E[89];
E[123] = E[89]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 54\n"),
#endif
__T_SurfaceAcquireMeas_A54);
E[120] = E[131]&&!(E[123]);
E[109] = __T_SurfaceAcquireMeas_R[49]&&!(__T_SurfaceAcquireMeas_R[0]);
E[85] = E[109]&&E[194];
E[235] = E[139]||(__T_SurfaceAcquireMeas_R[127]&&E[120]&&!(E[85]));
E[131] = E[131]&&E[123];
E[102] = E[94]&&!(E[102]);
E[94] = __T_SurfaceAcquireMeas_R[129]&&E[102];
E[100] = E[100]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__T_SurfaceAcquireMeas_A12));
E[100] = __T_SurfaceAcquireMeas_R[23]&&E[100];
E[236] = E[100]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 50\n"),
#endif
__T_SurfaceAcquireMeas_A50);
E[237] = E[94]&&!(E[236]);
E[238] = __T_SurfaceAcquireMeas_R[18]&&!(__T_SurfaceAcquireMeas_R[0]);
E[239] = E[238]&&E[180];
E[240] = E[131]||(__T_SurfaceAcquireMeas_R[129]&&E[237]&&!(E[239]));
E[102] = (E[237]&&E[239])||(__T_SurfaceAcquireMeas_R[130]&&E[102]);
E[105] = E[105]&&!(E[107]);
E[237] = __T_SurfaceAcquireMeas_R[131]&&E[105];
E[241] = __T_SurfaceAcquireMeas_R[2]&&!(__T_SurfaceAcquireMeas_R[0]);
E[94] = E[94]&&E[236];
E[168] = E[94]||E[168];
E[242] = E[241]&&E[168];
E[243] = E[131]||(__T_SurfaceAcquireMeas_R[131]&&E[237]&&!(E[242]));
E[105] = (E[237]&&E[242])||(__T_SurfaceAcquireMeas_R[132]&&E[105]);
E[93] = (E[234]&&!(E[93]))||E[240]||E[102];
E[104] = (E[234]&&!(E[104]))||E[243]||E[105];
E[234] = (E[240]||E[102]||E[243]||E[105])&&E[93]&&E[104];
E[93] = E[93]||E[94];
E[94] = E[94]&&E[93]&&E[104];
E[118] = E[110]&&!(E[118]);
E[110] = __T_SurfaceAcquireMeas_R[133]&&E[118];
E[116] = E[116]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 16\n"),
#endif
__T_SurfaceAcquireMeas_A16));
E[116] = __T_SurfaceAcquireMeas_R[33]&&E[116];
E[237] = E[116]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 52\n"),
#endif
__T_SurfaceAcquireMeas_A52);
E[244] = E[110]&&!(E[237]);
E[245] = __T_SurfaceAcquireMeas_R[30]&&!(__T_SurfaceAcquireMeas_R[0]);
E[246] = E[245]&&E[178];
E[247] = E[94]||(__T_SurfaceAcquireMeas_R[133]&&E[244]&&!(E[246]));
E[110] = E[110]&&E[237];
E[91] = E[121]&&!(E[91]);
E[121] = __T_SurfaceAcquireMeas_R[135]&&E[91];
E[248] = E[121]&&!(E[123]);
E[249] = E[110]||(__T_SurfaceAcquireMeas_R[135]&&E[248]&&!(E[85]));
E[121] = E[121]&&E[123];
E[129] = E[124]&&!(E[129]);
E[124] = __T_SurfaceAcquireMeas_R[137]&&E[129];
E[127] = E[127]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 27\n"),
#endif
__T_SurfaceAcquireMeas_A27));
E[127] = __T_SurfaceAcquireMeas_R[57]&&E[127];
E[250] = E[127]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 56\n"),
#endif
__T_SurfaceAcquireMeas_A56);
E[251] = E[124]&&!(E[250]);
E[252] = __T_SurfaceAcquireMeas_R[55]&&!(__T_SurfaceAcquireMeas_R[0]);
E[253] = E[252]&&E[195];
E[254] = E[121]||(__T_SurfaceAcquireMeas_R[137]&&E[251]&&!(E[253]));
E[124] = E[124]&&E[250];
E[137] = E[132]&&!(E[137]);
E[132] = __T_SurfaceAcquireMeas_R[139]&&E[137];
E[135] = E[135]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 3\n"),
#endif
__T_SurfaceAcquireMeas_A3));
E[135] = __T_SurfaceAcquireMeas_R[16]&&E[135];
E[255] = E[135]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 46\n"),
#endif
__T_SurfaceAcquireMeas_A46);
E[256] = E[132]&&!(E[255]);
E[257] = __T_SurfaceAcquireMeas_R[14]&&!(__T_SurfaceAcquireMeas_R[0]);
E[258] = E[257]&&E[173];
E[259] = E[124]||(__T_SurfaceAcquireMeas_R[139]&&E[256]&&!(E[258]));
E[132] = E[132]&&E[255];
E[140] = E[140]&&!(E[145]);
E[260] = __T_SurfaceAcquireMeas_R[141]&&E[140];
E[143] = E[143]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 30\n"),
#endif
__T_SurfaceAcquireMeas_A30));
E[143] = __T_SurfaceAcquireMeas_R[122]&&E[143];
E[261] = E[143]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 57\n"),
#endif
__T_SurfaceAcquireMeas_A57);
E[262] = E[260]&&!(E[261]);
E[263] = __T_SurfaceAcquireMeas_R[120]&&!(__T_SurfaceAcquireMeas_R[0]);
E[62] = (E[200]&&E[62])||E[202];
E[200] = E[62]&&!(E[145]);
E[264] = E[263]&&E[200];
E[265] = E[132]||(__T_SurfaceAcquireMeas_R[141]&&E[262]&&!(E[264]));
E[232] = E[232]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 71\n"),
#endif
__T_SurfaceAcquireMeas_A71));
E[232] = (E[260]&&E[261])||(__T_SurfaceAcquireMeas_R[143]&&E[232]);
E[65] = E[65]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 70\n"),
#endif
__T_SurfaceAcquireMeas_A70);
E[64] = E[64]&&!(E[67]);
E[260] = E[65]||(__T_SurfaceAcquireMeas_R[144]&&E[64]&&!(E[203]));
E[63] = (E[64]&&E[203])||(__T_SurfaceAcquireMeas_R[145]&&E[63]);
E[140] = (E[262]&&E[264])||(__T_SurfaceAcquireMeas_R[142]&&E[140]);
E[137] = (E[256]&&E[258])||(__T_SurfaceAcquireMeas_R[140]&&E[137]);
E[129] = (E[251]&&E[253])||(__T_SurfaceAcquireMeas_R[138]&&E[129]);
E[91] = (E[248]&&E[85])||(__T_SurfaceAcquireMeas_R[136]&&E[91]);
E[118] = (E[244]&&E[246])||(__T_SurfaceAcquireMeas_R[134]&&E[118]);
E[86] = (E[120]&&E[85])||(__T_SurfaceAcquireMeas_R[128]&&E[86]);
E[78] = E[78]&&!(E[0]);
E[120] = __T_SurfaceAcquireMeas_R[35]&&!(__T_SurfaceAcquireMeas_R[0]);
E[244] = E[120]&&E[187];
E[248] = __T_SurfaceAcquireMeas_R[125]&&E[78]&&!(E[244]);
E[83] = (E[78]&&E[244])||(__T_SurfaceAcquireMeas_R[126]&&E[83]);
E[78] = E[248]||E[83];
E[77] = (E[147]&&!(E[77]))||E[235]||E[234]||E[247]||E[249]||E[254]||E[259]||E[265]||E[232]||E[260]||E[63]||E[140]||E[137]||E[129]||E[91]||E[118]||E[86]||E[78];
E[251] = E[77]||E[233];
E[164] = E[153]&&!(E[164]);
E[153] = __T_SurfaceAcquireMeas_R[150]&&E[164];
E[154] = E[154]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 38\n"),
#endif
__T_SurfaceAcquireMeas_A38));
E[154] = __T_SurfaceAcquireMeas_R[64]&&E[154];
E[256] = E[154]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 61\n"),
#endif
__T_SurfaceAcquireMeas_A61);
E[156] = E[156]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 37\n"),
#endif
__T_SurfaceAcquireMeas_A37));
E[156] = __T_SurfaceAcquireMeas_R[65]&&E[156];
E[262] = E[156]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 59\n"),
#endif
__T_SurfaceAcquireMeas_A59);
E[158] = E[158]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 36\n"),
#endif
__T_SurfaceAcquireMeas_A36));
E[158] = __T_SurfaceAcquireMeas_R[66]&&E[158];
E[64] = E[158]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 55\n"),
#endif
__T_SurfaceAcquireMeas_A55);
E[160] = E[160]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 34\n"),
#endif
__T_SurfaceAcquireMeas_A34));
E[160] = __T_SurfaceAcquireMeas_R[67]&&E[160];
E[266] = E[160]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 48\n"),
#endif
__T_SurfaceAcquireMeas_A48);
E[162] = E[162]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 35\n"),
#endif
__T_SurfaceAcquireMeas_A35));
E[162] = __T_SurfaceAcquireMeas_R[68]&&E[162];
E[267] = E[162]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 49\n"),
#endif
__T_SurfaceAcquireMeas_A49);
E[268] = __T_SurfaceAcquireMeas_R[64]||__T_SurfaceAcquireMeas_R[65]||__T_SurfaceAcquireMeas_R[66]||__T_SurfaceAcquireMeas_R[67]||__T_SurfaceAcquireMeas_R[68];
E[269] = (E[268]&&!(__T_SurfaceAcquireMeas_R[64]))||E[256];
E[270] = (E[268]&&!(__T_SurfaceAcquireMeas_R[65]))||E[262];
E[271] = (E[268]&&!(__T_SurfaceAcquireMeas_R[66]))||E[64];
E[272] = (E[268]&&!(__T_SurfaceAcquireMeas_R[67]))||E[266];
E[273] = (E[268]&&!(__T_SurfaceAcquireMeas_R[68]))||E[267];
E[267] = (E[256]||E[262]||E[64]||E[266]||E[267])&&E[269]&&E[270]&&E[271]&&E[272]&&E[273];
E[266] = (E[153]&&E[267])||(__T_SurfaceAcquireMeas_R[146]&&__T_SurfaceAcquireMeas_R[146]&&!(__T_SurfaceAcquireMeas_R[0]));
E[148] = E[148]&&!(E[150]);
E[64] = __T_SurfaceAcquireMeas_R[147]&&E[148];
E[149] = E[149]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 24\n"),
#endif
__T_SurfaceAcquireMeas_A24));
E[149] = __T_SurfaceAcquireMeas_R[47]&&E[149];
E[262] = E[149]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 60\n"),
#endif
__T_SurfaceAcquireMeas_A60);
E[256] = E[64]&&!(E[262]);
E[274] = __T_SurfaceAcquireMeas_R[46]&&!(__T_SurfaceAcquireMeas_R[0]);
E[275] = E[274]&&E[185];
E[276] = __T_SurfaceAcquireMeas_R[147]&&E[256]&&!(E[275]);
E[277] = __T_SurfaceAcquireMeas_R[149]&&!(__T_SurfaceAcquireMeas_R[0]);
E[278] = E[277]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 72\n"),
#endif
__T_SurfaceAcquireMeas_A72));
E[278] = (E[64]&&E[262])||(__T_SurfaceAcquireMeas_R[149]&&E[278]);
E[277] = E[277]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 72\n"),
#endif
__T_SurfaceAcquireMeas_A72);
E[153] = E[153]&&!(E[267]);
E[64] = __T_SurfaceAcquireMeas_R[63]&&!(__T_SurfaceAcquireMeas_R[0]);
E[279] = E[64]&&E[192];
E[280] = E[277]||(__T_SurfaceAcquireMeas_R[150]&&E[153]&&!(E[279]));
E[164] = (E[153]&&E[279])||(__T_SurfaceAcquireMeas_R[151]&&E[164]);
E[148] = (E[256]&&E[275])||(__T_SurfaceAcquireMeas_R[148]&&E[148]);
E[152] = (E[147]&&!(E[152]))||E[266]||E[276]||E[278]||E[280]||E[164]||E[148];
E[233] = E[233]&&E[251]&&E[152];
E[256] = E[147]||__T_SurfaceAcquireMeas_R[152];
E[77] = (E[235]||E[234]||E[247]||E[249]||E[254]||E[259]||E[265]||E[232]||E[260]||E[63]||E[140]||E[137]||E[129]||E[91]||E[118]||E[86]||E[78]||E[266]||E[276]||E[278]||E[280]||E[164]||E[148])&&E[77]&&E[152];
E[147] = (E[256]&&!(E[147]))||E[77];
E[78] = E[147]||E[233];
E[234] = __T_SurfaceAcquireMeas_R[152]&&!(__T_SurfaceAcquireMeas_R[0]);
E[153] = (E[256]&&!(__T_SurfaceAcquireMeas_R[152]))||E[234];
E[281] = E[233]&&E[78]&&E[153];
if (E[281]) {
__T_SurfaceAcquireMeas_A73;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A73\n");
#endif
}
if (E[76]) {
__T_SurfaceAcquireMeas_A74;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A74\n");
#endif
}
if (E[258]) {
__T_SurfaceAcquireMeas_A75;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A75\n");
#endif
}
if (E[258]) {
__T_SurfaceAcquireMeas_A76;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A76\n");
#endif
}
if (E[255]) {
__T_SurfaceAcquireMeas_A77;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A77\n");
#endif
}
E[282] = __T_SurfaceAcquireMeas_R[15]||__T_SurfaceAcquireMeas_R[16];
E[283] = __T_SurfaceAcquireMeas_R[14]||E[282];
E[173] = __T_SurfaceAcquireMeas_R[14]&&E[257]&&!(E[173]);
E[257] = (E[283]&&!(__T_SurfaceAcquireMeas_R[14]))||E[173];
E[284] = E[257]||E[258];
E[133] = E[133]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 4\n"),
#endif
__T_SurfaceAcquireMeas_A4));
E[133] = __T_SurfaceAcquireMeas_R[15]&&E[133];
E[285] = E[133]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 62\n"),
#endif
__T_SurfaceAcquireMeas_A62));
E[285] = __T_SurfaceAcquireMeas_R[15]&&E[285];
E[133] = E[133]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 62\n"),
#endif
__T_SurfaceAcquireMeas_A62);
E[135] = E[135]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 46\n"),
#endif
__T_SurfaceAcquireMeas_A46));
E[135] = E[133]||(__T_SurfaceAcquireMeas_R[16]&&E[135]);
E[282] = (E[283]&&!(E[282]))||E[285]||E[135];
E[133] = E[282]||E[255];
E[286] = (E[258]||E[255])&&E[284]&&E[133];
E[287] = E[286]||__T_SurfaceAcquireMeas_R[0];
E[288] = __T_SurfaceAcquireMeas_R[13]&&!(__T_SurfaceAcquireMeas_R[0]);
E[289] = (E[287]&&E[124])||(E[288]&&E[124]);
if (E[289]) {
__T_SurfaceAcquireMeas_A78;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A78\n");
#endif
}
if (E[242]) {
__T_SurfaceAcquireMeas_A79;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A79\n");
#endif
}
if (E[242]) {
__T_SurfaceAcquireMeas_A80;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A80\n");
#endif
}
if (!(_true)) {
__T_SurfaceAcquireMeas_A81;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A81\n");
#endif
}
E[290] = __T_SurfaceAcquireMeas_R[3]||__T_SurfaceAcquireMeas_R[4];
E[291] = __T_SurfaceAcquireMeas_R[2]||E[290];
E[168] = __T_SurfaceAcquireMeas_R[2]&&E[241]&&!(E[168]);
E[241] = (E[291]&&!(__T_SurfaceAcquireMeas_R[2]))||E[168];
E[292] = E[241]||E[242];
E[106] = E[106]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 5\n"),
#endif
__T_SurfaceAcquireMeas_A5));
E[106] = __T_SurfaceAcquireMeas_R[3]&&E[106];
E[293] = E[106]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 63\n"),
#endif
__T_SurfaceAcquireMeas_A63));
E[293] = __T_SurfaceAcquireMeas_R[3]&&E[293];
E[106] = E[106]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 63\n"),
#endif
__T_SurfaceAcquireMeas_A63);
E[106] = E[106]||(__T_SurfaceAcquireMeas_R[4]&&!(__T_SurfaceAcquireMeas_R[0]));
E[290] = (E[291]&&!(E[290]))||E[293]||E[106];
E[294] = E[242]&&E[292]&&E[290];
E[295] = E[294]||__T_SurfaceAcquireMeas_R[0];
E[296] = __T_SurfaceAcquireMeas_R[1]&&!(__T_SurfaceAcquireMeas_R[0]);
E[297] = (E[295]&&E[131])||(E[296]&&E[131]);
if (E[297]) {
__T_SurfaceAcquireMeas_A82;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A82\n");
#endif
}
E[298] = __T_SurfaceAcquireMeas_R[6]&&!(__T_SurfaceAcquireMeas_R[0]);
E[299] = E[298]&&E[174];
if (E[299]) {
__T_SurfaceAcquireMeas_A83;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A83\n");
#endif
}
if (E[299]) {
__T_SurfaceAcquireMeas_A84;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A84\n");
#endif
}
E[171] = E[171]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 6\n"),
#endif
__T_SurfaceAcquireMeas_A6));
E[171] = __T_SurfaceAcquireMeas_R[8]&&E[171];
E[300] = E[171]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 47\n"),
#endif
__T_SurfaceAcquireMeas_A47);
if (E[300]) {
__T_SurfaceAcquireMeas_A85;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A85\n");
#endif
}
E[301] = __T_SurfaceAcquireMeas_R[7]||__T_SurfaceAcquireMeas_R[8];
E[302] = __T_SurfaceAcquireMeas_R[6]||E[301];
E[174] = __T_SurfaceAcquireMeas_R[6]&&E[298]&&!(E[174]);
E[298] = (E[302]&&!(__T_SurfaceAcquireMeas_R[6]))||E[174];
E[303] = E[298]||E[299];
E[169] = E[169]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 7\n"),
#endif
__T_SurfaceAcquireMeas_A7));
E[169] = __T_SurfaceAcquireMeas_R[7]&&E[169];
E[304] = E[169]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 63\n"),
#endif
__T_SurfaceAcquireMeas_A63));
E[304] = __T_SurfaceAcquireMeas_R[7]&&E[304];
E[169] = E[169]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 63\n"),
#endif
__T_SurfaceAcquireMeas_A63);
E[171] = E[171]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 47\n"),
#endif
__T_SurfaceAcquireMeas_A47));
E[171] = E[169]||(__T_SurfaceAcquireMeas_R[8]&&E[171]);
E[301] = (E[302]&&!(E[301]))||E[304]||E[171];
E[169] = E[301]||E[300];
E[305] = (E[299]||E[300])&&E[303]&&E[169];
if (!(_true)) {
__T_SurfaceAcquireMeas_A86;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A86\n");
#endif
}
E[306] = __T_SurfaceAcquireMeas_R[10]&&!(__T_SurfaceAcquireMeas_R[0]);
E[307] = E[306]&&E[179];
if (E[307]) {
__T_SurfaceAcquireMeas_A87;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A87\n");
#endif
}
if (E[307]) {
__T_SurfaceAcquireMeas_A88;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A88\n");
#endif
}
E[176] = E[176]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 8\n"),
#endif
__T_SurfaceAcquireMeas_A8));
E[176] = __T_SurfaceAcquireMeas_R[12]&&E[176];
E[308] = E[176]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 47\n"),
#endif
__T_SurfaceAcquireMeas_A47);
if (E[308]) {
__T_SurfaceAcquireMeas_A89;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A89\n");
#endif
}
E[309] = __T_SurfaceAcquireMeas_R[11]||__T_SurfaceAcquireMeas_R[12];
E[310] = __T_SurfaceAcquireMeas_R[10]||E[309];
E[179] = __T_SurfaceAcquireMeas_R[10]&&E[306]&&!(E[179]);
E[306] = (E[310]&&!(__T_SurfaceAcquireMeas_R[10]))||E[179];
E[311] = E[306]||E[307];
E[167] = E[167]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 9\n"),
#endif
__T_SurfaceAcquireMeas_A9));
E[167] = __T_SurfaceAcquireMeas_R[11]&&E[167];
E[312] = E[167]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 63\n"),
#endif
__T_SurfaceAcquireMeas_A63));
E[312] = __T_SurfaceAcquireMeas_R[11]&&E[312];
E[167] = E[167]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 63\n"),
#endif
__T_SurfaceAcquireMeas_A63);
E[176] = E[176]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 47\n"),
#endif
__T_SurfaceAcquireMeas_A47));
E[176] = E[167]||(__T_SurfaceAcquireMeas_R[12]&&E[176]);
E[309] = (E[310]&&!(E[309]))||E[312]||E[176];
E[167] = E[309]||E[308];
E[313] = (E[307]||E[308])&&E[311]&&E[167];
if (!(_true)) {
__T_SurfaceAcquireMeas_A90;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A90\n");
#endif
}
if (E[227]) {
__T_SurfaceAcquireMeas_A91;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A91\n");
#endif
}
if (E[227]) {
__T_SurfaceAcquireMeas_A92;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A92\n");
#endif
}
if (E[224]) {
__T_SurfaceAcquireMeas_A93;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A93\n");
#endif
}
E[314] = __T_SurfaceAcquireMeas_R[93]||__T_SurfaceAcquireMeas_R[94];
E[45] = E[45]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__T_SurfaceAcquireMeas_A11));
E[45] = __T_SurfaceAcquireMeas_R[92]&&E[45];
E[315] = E[45]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64);
E[42] = E[42]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 42\n"),
#endif
__T_SurfaceAcquireMeas_A42));
E[42] = E[315]||(__T_SurfaceAcquireMeas_R[93]&&E[42]);
E[316] = (E[314]&&!(__T_SurfaceAcquireMeas_R[93]))||E[42];
E[317] = E[316]||E[43];
E[47] = E[47]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 53\n"),
#endif
__T_SurfaceAcquireMeas_A53));
E[47] = E[315]||(__T_SurfaceAcquireMeas_R[94]&&E[47]);
E[315] = (E[314]&&!(__T_SurfaceAcquireMeas_R[94]))||E[47];
E[318] = E[315]||E[224];
E[319] = (E[43]||E[224])&&E[317]&&E[318];
E[314] = __T_SurfaceAcquireMeas_R[92]||E[314];
E[320] = __T_SurfaceAcquireMeas_R[91]||E[314];
E[27] = __T_SurfaceAcquireMeas_R[91]&&E[226]&&!(E[27]);
E[226] = (E[320]&&!(__T_SurfaceAcquireMeas_R[91]))||E[27];
E[321] = E[226]||E[227];
E[45] = E[45]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64));
E[45] = __T_SurfaceAcquireMeas_R[92]&&E[45];
E[315] = (E[42]||E[47])&&E[316]&&E[315];
E[314] = (E[320]&&!(E[314]))||E[45]||E[315];
E[316] = E[314]||E[319];
E[322] = (E[227]||E[319])&&E[321]&&E[316];
E[323] = E[322]||__T_SurfaceAcquireMeas_R[0];
E[324] = __T_SurfaceAcquireMeas_R[90]&&!(__T_SurfaceAcquireMeas_R[0]);
E[325] = (E[323]&&E[217])||(E[324]&&E[217]);
if (E[325]) {
__T_SurfaceAcquireMeas_A94;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A94\n");
#endif
}
if (E[239]) {
__T_SurfaceAcquireMeas_A95;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A95\n");
#endif
}
if (E[239]) {
__T_SurfaceAcquireMeas_A96;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A96\n");
#endif
}
if (E[236]) {
__T_SurfaceAcquireMeas_A97;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A97\n");
#endif
}
E[326] = __T_SurfaceAcquireMeas_R[21]&&!(__T_SurfaceAcquireMeas_R[0]);
E[327] = E[326]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 45\n"),
#endif
__T_SurfaceAcquireMeas_A45);
E[328] = __T_SurfaceAcquireMeas_R[22]&&!(__T_SurfaceAcquireMeas_R[0]);
E[329] = E[328]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 43\n"),
#endif
__T_SurfaceAcquireMeas_A43);
E[330] = __T_SurfaceAcquireMeas_R[20]||__T_SurfaceAcquireMeas_R[21]||__T_SurfaceAcquireMeas_R[22];
E[98] = E[98]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__T_SurfaceAcquireMeas_A13));
E[98] = __T_SurfaceAcquireMeas_R[19]&&E[98];
E[331] = E[98]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64);
E[95] = E[95]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 40\n"),
#endif
__T_SurfaceAcquireMeas_A40));
E[95] = E[331]||(__T_SurfaceAcquireMeas_R[20]&&E[95]);
E[332] = (E[330]&&!(__T_SurfaceAcquireMeas_R[20]))||E[95];
E[326] = E[326]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 45\n"),
#endif
__T_SurfaceAcquireMeas_A45));
E[326] = E[331]||(__T_SurfaceAcquireMeas_R[21]&&E[326]);
E[333] = (E[330]&&!(__T_SurfaceAcquireMeas_R[21]))||E[326];
E[328] = E[328]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 43\n"),
#endif
__T_SurfaceAcquireMeas_A43));
E[328] = E[331]||(__T_SurfaceAcquireMeas_R[22]&&E[328]);
E[334] = (E[330]&&!(__T_SurfaceAcquireMeas_R[22]))||E[328];
E[335] = (E[96]||E[327]||E[329])&&(E[332]||E[96])&&(E[333]||E[327])&&(E[334]||E[329]);
E[336] = E[330]||__T_SurfaceAcquireMeas_R[23];
E[334] = (E[95]||E[326]||E[328])&&E[332]&&E[333]&&E[334];
E[330] = (E[336]&&!(E[330]))||E[334];
E[333] = E[330]||E[335];
E[100] = E[100]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 50\n"),
#endif
__T_SurfaceAcquireMeas_A50));
E[100] = E[331]||(__T_SurfaceAcquireMeas_R[23]&&E[100]);
E[331] = (E[336]&&!(__T_SurfaceAcquireMeas_R[23]))||E[100];
E[332] = E[331]||E[236];
E[337] = (E[335]||E[236])&&E[333]&&E[332];
E[336] = __T_SurfaceAcquireMeas_R[19]||E[336];
E[338] = __T_SurfaceAcquireMeas_R[18]||E[336];
E[180] = __T_SurfaceAcquireMeas_R[18]&&E[238]&&!(E[180]);
E[238] = (E[338]&&!(__T_SurfaceAcquireMeas_R[18]))||E[180];
E[339] = E[238]||E[239];
E[98] = E[98]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64));
E[98] = __T_SurfaceAcquireMeas_R[19]&&E[98];
E[331] = (E[334]||E[100])&&E[330]&&E[331];
E[336] = (E[338]&&!(E[336]))||E[98]||E[331];
E[330] = E[336]||E[337];
E[334] = (E[239]||E[337])&&E[339]&&E[330];
E[340] = E[334]||__T_SurfaceAcquireMeas_R[0];
E[341] = __T_SurfaceAcquireMeas_R[17]&&!(__T_SurfaceAcquireMeas_R[0]);
E[342] = (E[340]&&E[131])||(E[341]&&E[131]);
if (E[342]) {
__T_SurfaceAcquireMeas_A98;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A98\n");
#endif
}
E[343] = __T_SurfaceAcquireMeas_R[25]&&!(__T_SurfaceAcquireMeas_R[0]);
E[344] = E[343]&&E[186];
if (E[344]||E[221]) {
__T_SurfaceAcquireMeas_A99;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A99\n");
#endif
}
if (E[344]||E[221]) {
__T_SurfaceAcquireMeas_A100;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A100\n");
#endif
}
E[183] = E[183]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 14\n"),
#endif
__T_SurfaceAcquireMeas_A14));
E[183] = __T_SurfaceAcquireMeas_R[28]&&E[183];
E[345] = E[183]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 50\n"),
#endif
__T_SurfaceAcquireMeas_A50);
if (E[345]||E[218]) {
__T_SurfaceAcquireMeas_A101;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A101\n");
#endif
}
E[346] = __T_SurfaceAcquireMeas_R[27]&&!(__T_SurfaceAcquireMeas_R[0]);
E[347] = E[346]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 40\n"),
#endif
__T_SurfaceAcquireMeas_A40);
E[348] = __T_SurfaceAcquireMeas_R[27]||__T_SurfaceAcquireMeas_R[28];
E[181] = E[181]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__T_SurfaceAcquireMeas_A15));
E[181] = __T_SurfaceAcquireMeas_R[26]&&E[181];
E[349] = E[181]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64);
E[346] = E[346]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 40\n"),
#endif
__T_SurfaceAcquireMeas_A40));
E[346] = E[349]||(__T_SurfaceAcquireMeas_R[27]&&E[346]);
E[350] = (E[348]&&!(__T_SurfaceAcquireMeas_R[27]))||E[346];
E[351] = E[350]||E[347];
E[183] = E[183]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 50\n"),
#endif
__T_SurfaceAcquireMeas_A50));
E[183] = E[349]||(__T_SurfaceAcquireMeas_R[28]&&E[183]);
E[349] = (E[348]&&!(__T_SurfaceAcquireMeas_R[28]))||E[183];
E[352] = E[349]||E[345];
E[353] = (E[347]||E[345])&&E[351]&&E[352];
E[348] = __T_SurfaceAcquireMeas_R[26]||E[348];
E[354] = __T_SurfaceAcquireMeas_R[25]||E[348];
E[186] = __T_SurfaceAcquireMeas_R[25]&&E[343]&&!(E[186]);
E[343] = (E[354]&&!(__T_SurfaceAcquireMeas_R[25]))||E[186];
E[355] = E[343]||E[344];
E[181] = E[181]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64));
E[181] = __T_SurfaceAcquireMeas_R[26]&&E[181];
E[349] = (E[346]||E[183])&&E[350]&&E[349];
E[348] = (E[354]&&!(E[348]))||E[181]||E[349];
E[350] = E[348]||E[353];
E[356] = (E[344]||E[353])&&E[355]&&E[350];
E[357] = __T_SurfaceAcquireMeas_R[88]||__T_SurfaceAcquireMeas_R[89];
E[34] = E[34]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 15\n"),
#endif
__T_SurfaceAcquireMeas_A15));
E[34] = __T_SurfaceAcquireMeas_R[87]&&E[34];
E[358] = E[34]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64);
E[31] = E[31]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 40\n"),
#endif
__T_SurfaceAcquireMeas_A40));
E[31] = E[358]||(__T_SurfaceAcquireMeas_R[88]&&E[31]);
E[359] = (E[357]&&!(__T_SurfaceAcquireMeas_R[88]))||E[31];
E[360] = E[359]||E[32];
E[36] = E[36]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 50\n"),
#endif
__T_SurfaceAcquireMeas_A50));
E[36] = E[358]||(__T_SurfaceAcquireMeas_R[89]&&E[36]);
E[358] = (E[357]&&!(__T_SurfaceAcquireMeas_R[89]))||E[36];
E[361] = E[358]||E[218];
E[362] = (E[32]||E[218])&&E[360]&&E[361];
E[357] = __T_SurfaceAcquireMeas_R[87]||E[357];
E[363] = __T_SurfaceAcquireMeas_R[86]||E[357];
E[19] = __T_SurfaceAcquireMeas_R[86]&&E[220]&&!(E[19]);
E[220] = (E[363]&&!(__T_SurfaceAcquireMeas_R[86]))||E[19];
E[364] = E[220]||E[221];
E[34] = E[34]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64));
E[34] = __T_SurfaceAcquireMeas_R[87]&&E[34];
E[358] = (E[31]||E[36])&&E[359]&&E[358];
E[357] = (E[363]&&!(E[357]))||E[34]||E[358];
E[359] = E[357]||E[362];
E[365] = (E[221]||E[362])&&E[364]&&E[359];
E[366] = E[365]||__T_SurfaceAcquireMeas_R[0];
E[367] = __T_SurfaceAcquireMeas_R[85]&&!(__T_SurfaceAcquireMeas_R[0]);
E[368] = (E[366]&&E[29])||(E[367]&&E[29]);
if (E[368]) {
__T_SurfaceAcquireMeas_A102;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A102\n");
#endif
}
if (E[246]) {
__T_SurfaceAcquireMeas_A103;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A103\n");
#endif
}
if (E[246]) {
__T_SurfaceAcquireMeas_A104;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A104\n");
#endif
}
if (E[237]) {
__T_SurfaceAcquireMeas_A105;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A105\n");
#endif
}
E[369] = __T_SurfaceAcquireMeas_R[32]||__T_SurfaceAcquireMeas_R[33];
E[114] = E[114]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__T_SurfaceAcquireMeas_A17));
E[114] = __T_SurfaceAcquireMeas_R[31]&&E[114];
E[370] = E[114]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64);
E[111] = E[111]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 41\n"),
#endif
__T_SurfaceAcquireMeas_A41));
E[111] = E[370]||(__T_SurfaceAcquireMeas_R[32]&&E[111]);
E[371] = (E[369]&&!(__T_SurfaceAcquireMeas_R[32]))||E[111];
E[372] = E[371]||E[112];
E[116] = E[116]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 52\n"),
#endif
__T_SurfaceAcquireMeas_A52));
E[116] = E[370]||(__T_SurfaceAcquireMeas_R[33]&&E[116]);
E[370] = (E[369]&&!(__T_SurfaceAcquireMeas_R[33]))||E[116];
E[373] = E[370]||E[237];
E[374] = (E[112]||E[237])&&E[372]&&E[373];
E[369] = __T_SurfaceAcquireMeas_R[31]||E[369];
E[375] = __T_SurfaceAcquireMeas_R[30]||E[369];
E[178] = __T_SurfaceAcquireMeas_R[30]&&E[245]&&!(E[178]);
E[245] = (E[375]&&!(__T_SurfaceAcquireMeas_R[30]))||E[178];
E[376] = E[245]||E[246];
E[114] = E[114]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64));
E[114] = __T_SurfaceAcquireMeas_R[31]&&E[114];
E[370] = (E[111]||E[116])&&E[371]&&E[370];
E[369] = (E[375]&&!(E[369]))||E[114]||E[370];
E[371] = E[369]||E[374];
E[377] = (E[246]||E[374])&&E[376]&&E[371];
E[378] = E[377]||__T_SurfaceAcquireMeas_R[0];
E[379] = __T_SurfaceAcquireMeas_R[29]&&!(__T_SurfaceAcquireMeas_R[0]);
E[380] = (E[378]&&E[94])||(E[379]&&E[94]);
if (E[380]) {
__T_SurfaceAcquireMeas_A106;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A106\n");
#endif
}
if (E[244]||E[215]) {
__T_SurfaceAcquireMeas_A107;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A107\n");
#endif
}
if (E[244]||E[215]) {
__T_SurfaceAcquireMeas_A108;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A108\n");
#endif
}
if (E[0]||E[212]) {
__T_SurfaceAcquireMeas_A109;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A109\n");
#endif
}
E[381] = __T_SurfaceAcquireMeas_R[37]&&!(__T_SurfaceAcquireMeas_R[0]);
E[382] = E[381]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 45\n"),
#endif
__T_SurfaceAcquireMeas_A45);
E[383] = __T_SurfaceAcquireMeas_R[38]&&!(__T_SurfaceAcquireMeas_R[0]);
E[384] = E[383]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 43\n"),
#endif
__T_SurfaceAcquireMeas_A43);
E[385] = __T_SurfaceAcquireMeas_R[37]||__T_SurfaceAcquireMeas_R[38];
E[79] = E[79]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__T_SurfaceAcquireMeas_A19));
E[79] = __T_SurfaceAcquireMeas_R[36]&&E[79];
E[386] = E[79]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64);
E[381] = E[381]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 45\n"),
#endif
__T_SurfaceAcquireMeas_A45));
E[381] = E[386]||(__T_SurfaceAcquireMeas_R[37]&&E[381]);
E[387] = (E[385]&&!(__T_SurfaceAcquireMeas_R[37]))||E[381];
E[383] = E[383]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 43\n"),
#endif
__T_SurfaceAcquireMeas_A43));
E[383] = E[386]||(__T_SurfaceAcquireMeas_R[38]&&E[383]);
E[388] = (E[385]&&!(__T_SurfaceAcquireMeas_R[38]))||E[383];
E[389] = (E[382]||E[384])&&(E[387]||E[382])&&(E[388]||E[384]);
E[390] = E[385]||__T_SurfaceAcquireMeas_R[39];
E[388] = (E[381]||E[383])&&E[387]&&E[388];
E[385] = (E[390]&&!(E[385]))||E[388];
E[387] = E[385]||E[389];
E[81] = E[81]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 51\n"),
#endif
__T_SurfaceAcquireMeas_A51));
E[81] = E[386]||(__T_SurfaceAcquireMeas_R[39]&&E[81]);
E[386] = (E[390]&&!(__T_SurfaceAcquireMeas_R[39]))||E[81];
E[391] = E[386]||E[0];
E[392] = (E[389]||E[0])&&E[387]&&E[391];
E[390] = __T_SurfaceAcquireMeas_R[36]||E[390];
E[393] = __T_SurfaceAcquireMeas_R[35]||E[390];
E[187] = __T_SurfaceAcquireMeas_R[35]&&E[120]&&!(E[187]);
E[120] = (E[393]&&!(__T_SurfaceAcquireMeas_R[35]))||E[187];
E[394] = E[120]||E[244];
E[79] = E[79]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64));
E[79] = __T_SurfaceAcquireMeas_R[36]&&E[79];
E[386] = (E[388]||E[81])&&E[385]&&E[386];
E[390] = (E[393]&&!(E[390]))||E[79]||E[386];
E[385] = E[390]||E[392];
E[388] = (E[244]||E[392])&&E[394]&&E[385];
E[395] = E[388]||__T_SurfaceAcquireMeas_R[0];
E[396] = __T_SurfaceAcquireMeas_R[124]||E[256];
E[75] = E[75]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 2\n"),
#endif
__T_SurfaceAcquireMeas_A2));
E[75] = __T_SurfaceAcquireMeas_R[124]&&E[75];
E[397] = (E[396]&&!(__T_SurfaceAcquireMeas_R[124]))||E[75];
E[398] = E[397]||E[76];
E[147] = (E[77]||E[234])&&E[147]&&E[153];
E[256] = (E[396]&&!(E[256]))||E[147];
E[77] = E[256]||E[281];
E[399] = (E[76]||E[281])&&E[398]&&E[77];
E[400] = E[399]||__T_SurfaceAcquireMeas_R[0];
E[401] = E[400]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1);
E[402] = __T_SurfaceAcquireMeas_R[123]&&!(__T_SurfaceAcquireMeas_R[0]);
E[403] = E[402]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1);
E[403] = E[401]||E[403];
E[401] = __T_SurfaceAcquireMeas_R[34]&&!(__T_SurfaceAcquireMeas_R[0]);
E[404] = (E[395]&&E[403])||(E[401]&&E[403]);
E[405] = __T_SurfaceAcquireMeas_R[82]&&!(__T_SurfaceAcquireMeas_R[0]);
E[406] = E[405]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 45\n"),
#endif
__T_SurfaceAcquireMeas_A45);
E[407] = __T_SurfaceAcquireMeas_R[83]&&!(__T_SurfaceAcquireMeas_R[0]);
E[408] = E[407]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 43\n"),
#endif
__T_SurfaceAcquireMeas_A43);
E[409] = __T_SurfaceAcquireMeas_R[82]||__T_SurfaceAcquireMeas_R[83];
E[23] = E[23]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__T_SurfaceAcquireMeas_A19));
E[23] = __T_SurfaceAcquireMeas_R[81]&&E[23];
E[410] = E[23]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64);
E[405] = E[405]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 45\n"),
#endif
__T_SurfaceAcquireMeas_A45));
E[405] = E[410]||(__T_SurfaceAcquireMeas_R[82]&&E[405]);
E[411] = (E[409]&&!(__T_SurfaceAcquireMeas_R[82]))||E[405];
E[407] = E[407]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 43\n"),
#endif
__T_SurfaceAcquireMeas_A43));
E[407] = E[410]||(__T_SurfaceAcquireMeas_R[83]&&E[407]);
E[412] = (E[409]&&!(__T_SurfaceAcquireMeas_R[83]))||E[407];
E[413] = (E[406]||E[408])&&(E[411]||E[406])&&(E[412]||E[408]);
E[414] = E[409]||__T_SurfaceAcquireMeas_R[84];
E[412] = (E[405]||E[407])&&E[411]&&E[412];
E[409] = (E[414]&&!(E[409]))||E[412];
E[411] = E[409]||E[413];
E[25] = E[25]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 51\n"),
#endif
__T_SurfaceAcquireMeas_A51));
E[25] = E[410]||(__T_SurfaceAcquireMeas_R[84]&&E[25]);
E[410] = (E[414]&&!(__T_SurfaceAcquireMeas_R[84]))||E[25];
E[415] = E[410]||E[212];
E[416] = (E[413]||E[212])&&E[411]&&E[415];
E[414] = __T_SurfaceAcquireMeas_R[81]||E[414];
E[417] = __T_SurfaceAcquireMeas_R[80]||E[414];
E[206] = __T_SurfaceAcquireMeas_R[80]&&E[214]&&!(E[206]);
E[214] = (E[417]&&!(__T_SurfaceAcquireMeas_R[80]))||E[206];
E[418] = E[214]||E[215];
E[23] = E[23]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64));
E[23] = __T_SurfaceAcquireMeas_R[81]&&E[23];
E[410] = (E[412]||E[25])&&E[409]&&E[410];
E[414] = (E[417]&&!(E[414]))||E[23]||E[410];
E[409] = E[414]||E[416];
E[412] = (E[215]||E[416])&&E[418]&&E[409];
E[419] = E[412]||__T_SurfaceAcquireMeas_R[0];
E[420] = __T_SurfaceAcquireMeas_R[79]&&!(__T_SurfaceAcquireMeas_R[0]);
E[421] = (E[419]&&E[69])||(E[420]&&E[69]);
if (E[404]||E[421]) {
__T_SurfaceAcquireMeas_A110;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A110\n");
#endif
}
E[422] = __T_SurfaceAcquireMeas_R[41]&&!(__T_SurfaceAcquireMeas_R[0]);
E[423] = E[422]&&E[193];
if (E[423]||E[210]) {
__T_SurfaceAcquireMeas_A111;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A111\n");
#endif
}
if (E[423]||E[210]) {
__T_SurfaceAcquireMeas_A112;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A112\n");
#endif
}
E[190] = E[190]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 20\n"),
#endif
__T_SurfaceAcquireMeas_A20));
E[190] = __T_SurfaceAcquireMeas_R[44]&&E[190];
E[424] = E[190]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 52\n"),
#endif
__T_SurfaceAcquireMeas_A52);
if (E[424]||E[21]) {
__T_SurfaceAcquireMeas_A113;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A113\n");
#endif
}
E[425] = __T_SurfaceAcquireMeas_R[43]&&!(__T_SurfaceAcquireMeas_R[0]);
E[426] = E[425]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 41\n"),
#endif
__T_SurfaceAcquireMeas_A41);
E[427] = __T_SurfaceAcquireMeas_R[43]||__T_SurfaceAcquireMeas_R[44];
E[188] = E[188]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__T_SurfaceAcquireMeas_A21));
E[188] = __T_SurfaceAcquireMeas_R[42]&&E[188];
E[428] = E[188]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64);
E[425] = E[425]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 41\n"),
#endif
__T_SurfaceAcquireMeas_A41));
E[425] = E[428]||(__T_SurfaceAcquireMeas_R[43]&&E[425]);
E[429] = (E[427]&&!(__T_SurfaceAcquireMeas_R[43]))||E[425];
E[430] = E[429]||E[426];
E[190] = E[190]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 52\n"),
#endif
__T_SurfaceAcquireMeas_A52));
E[190] = E[428]||(__T_SurfaceAcquireMeas_R[44]&&E[190]);
E[428] = (E[427]&&!(__T_SurfaceAcquireMeas_R[44]))||E[190];
E[431] = E[428]||E[424];
E[432] = (E[426]||E[424])&&E[430]&&E[431];
E[427] = __T_SurfaceAcquireMeas_R[42]||E[427];
E[433] = __T_SurfaceAcquireMeas_R[41]||E[427];
E[193] = __T_SurfaceAcquireMeas_R[41]&&E[422]&&!(E[193]);
E[422] = (E[433]&&!(__T_SurfaceAcquireMeas_R[41]))||E[193];
E[434] = E[422]||E[423];
E[188] = E[188]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64));
E[188] = __T_SurfaceAcquireMeas_R[42]&&E[188];
E[428] = (E[425]||E[190])&&E[429]&&E[428];
E[427] = (E[433]&&!(E[427]))||E[188]||E[428];
E[429] = E[427]||E[432];
E[435] = (E[423]||E[432])&&E[434]&&E[429];
E[436] = __T_SurfaceAcquireMeas_R[77]||__T_SurfaceAcquireMeas_R[78];
E[15] = E[15]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 21\n"),
#endif
__T_SurfaceAcquireMeas_A21));
E[15] = __T_SurfaceAcquireMeas_R[76]&&E[15];
E[437] = E[15]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64);
E[12] = E[12]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 41\n"),
#endif
__T_SurfaceAcquireMeas_A41));
E[12] = E[437]||(__T_SurfaceAcquireMeas_R[77]&&E[12]);
E[438] = (E[436]&&!(__T_SurfaceAcquireMeas_R[77]))||E[12];
E[439] = E[438]||E[13];
E[17] = E[17]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 52\n"),
#endif
__T_SurfaceAcquireMeas_A52));
E[17] = E[437]||(__T_SurfaceAcquireMeas_R[78]&&E[17]);
E[437] = (E[436]&&!(__T_SurfaceAcquireMeas_R[78]))||E[17];
E[440] = E[437]||E[21];
E[441] = (E[13]||E[21])&&E[439]&&E[440];
E[436] = __T_SurfaceAcquireMeas_R[76]||E[436];
E[442] = __T_SurfaceAcquireMeas_R[75]||E[436];
E[205] = __T_SurfaceAcquireMeas_R[75]&&E[70]&&!(E[205]);
E[70] = (E[442]&&!(__T_SurfaceAcquireMeas_R[75]))||E[205];
E[443] = E[70]||E[210];
E[15] = E[15]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64));
E[15] = __T_SurfaceAcquireMeas_R[76]&&E[15];
E[437] = (E[12]||E[17])&&E[438]&&E[437];
E[436] = (E[442]&&!(E[436]))||E[15]||E[437];
E[438] = E[436]||E[441];
E[444] = (E[210]||E[441])&&E[443]&&E[438];
E[445] = E[444]||__T_SurfaceAcquireMeas_R[0];
E[446] = __T_SurfaceAcquireMeas_R[101]||E[40];
E[202] = __T_SurfaceAcquireMeas_R[101]&&E[73]&&!(E[202]);
E[73] = (E[446]&&!(__T_SurfaceAcquireMeas_R[101]))||E[202];
E[447] = E[73]||E[203];
E[51] = (E[216]||E[222]||E[228]||E[231]||E[52]||E[41]||E[30]||E[22]||E[10]||E[219])&&E[51]&&E[225];
E[40] = (E[446]&&!(E[40]))||E[51];
E[10] = E[40]||E[67];
E[448] = (E[203]||E[67])&&E[447]&&E[10];
E[449] = E[448]||__T_SurfaceAcquireMeas_R[0];
E[450] = __T_SurfaceAcquireMeas_R[100]&&!(__T_SurfaceAcquireMeas_R[0]);
E[451] = (E[449]&&E[65])||(E[450]&&E[65]);
if (E[451]) {
__T_SurfaceAcquireMeas_A253;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A253\n");
#endif
}
E[452] = __T_SurfaceAcquireMeas_R[74]&&!(__T_SurfaceAcquireMeas_R[0]);
E[453] = (E[445]&&E[451])||(E[452]&&E[451]);
if (E[453]) {
__T_SurfaceAcquireMeas_A114;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A114\n");
#endif
}
if (E[230]) {
__T_SurfaceAcquireMeas_A115;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A115\n");
#endif
}
if (E[230]) {
__T_SurfaceAcquireMeas_A116;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A116\n");
#endif
}
if (E[66]) {
__T_SurfaceAcquireMeas_A117;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A117\n");
#endif
}
E[454] = __T_SurfaceAcquireMeas_R[98]||__T_SurfaceAcquireMeas_R[99];
E[56] = E[56]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 23\n"),
#endif
__T_SurfaceAcquireMeas_A23));
E[56] = __T_SurfaceAcquireMeas_R[97]&&E[56];
E[455] = E[56]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64);
E[53] = E[53]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 39\n"),
#endif
__T_SurfaceAcquireMeas_A39));
E[53] = E[455]||(__T_SurfaceAcquireMeas_R[98]&&E[53]);
E[456] = (E[454]&&!(__T_SurfaceAcquireMeas_R[98]))||E[53];
E[457] = E[456]||E[54];
E[58] = E[58]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 50\n"),
#endif
__T_SurfaceAcquireMeas_A50));
E[58] = E[455]||(__T_SurfaceAcquireMeas_R[99]&&E[58]);
E[455] = (E[454]&&!(__T_SurfaceAcquireMeas_R[99]))||E[58];
E[458] = E[455]||E[66];
E[459] = (E[54]||E[66])&&E[457]&&E[458];
E[454] = __T_SurfaceAcquireMeas_R[97]||E[454];
E[460] = __T_SurfaceAcquireMeas_R[96]||E[454];
E[229] = __T_SurfaceAcquireMeas_R[96]&&E[229]&&!(E[38]);
E[461] = (E[460]&&!(__T_SurfaceAcquireMeas_R[96]))||E[229];
E[462] = E[461]||E[230];
E[56] = E[56]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 64\n"),
#endif
__T_SurfaceAcquireMeas_A64));
E[56] = __T_SurfaceAcquireMeas_R[97]&&E[56];
E[455] = (E[53]||E[58])&&E[456]&&E[455];
E[454] = (E[460]&&!(E[454]))||E[56]||E[455];
E[456] = E[454]||E[459];
E[463] = (E[230]||E[459])&&E[462]&&E[456];
E[464] = E[463]||__T_SurfaceAcquireMeas_R[0];
E[465] = __T_SurfaceAcquireMeas_R[95]&&!(__T_SurfaceAcquireMeas_R[0]);
E[466] = (E[464]&&E[223])||(E[465]&&E[223]);
if (E[466]) {
__T_SurfaceAcquireMeas_A118;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A118\n");
#endif
}
if (E[275]) {
__T_SurfaceAcquireMeas_A119;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A119\n");
#endif
}
if (E[275]) {
__T_SurfaceAcquireMeas_A120;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A120\n");
#endif
}
if (E[262]) {
__T_SurfaceAcquireMeas_A121;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A121\n");
#endif
}
E[467] = __T_SurfaceAcquireMeas_R[46]||__T_SurfaceAcquireMeas_R[47];
E[185] = __T_SurfaceAcquireMeas_R[46]&&E[274]&&!(E[185]);
E[274] = (E[467]&&!(__T_SurfaceAcquireMeas_R[46]))||E[185];
E[468] = E[274]||E[275];
E[149] = E[149]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 60\n"),
#endif
__T_SurfaceAcquireMeas_A60));
E[149] = __T_SurfaceAcquireMeas_R[47]&&E[149];
E[469] = (E[467]&&!(__T_SurfaceAcquireMeas_R[47]))||E[149];
E[470] = E[469]||E[262];
E[471] = (E[275]||E[262])&&E[468]&&E[470];
if (!(_true)) {
__T_SurfaceAcquireMeas_A122;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A122\n");
#endif
}
if (E[85]) {
__T_SurfaceAcquireMeas_A123;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A123\n");
#endif
}
if (E[85]) {
__T_SurfaceAcquireMeas_A124;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A124\n");
#endif
}
if (E[123]) {
__T_SurfaceAcquireMeas_A125;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A125\n");
#endif
}
E[472] = __T_SurfaceAcquireMeas_R[51]&&!(__T_SurfaceAcquireMeas_R[0]);
E[473] = E[472]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 43\n"),
#endif
__T_SurfaceAcquireMeas_A43);
E[474] = __T_SurfaceAcquireMeas_R[52]&&!(__T_SurfaceAcquireMeas_R[0]);
E[475] = E[474]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 45\n"),
#endif
__T_SurfaceAcquireMeas_A45);
E[476] = __T_SurfaceAcquireMeas_R[51]||__T_SurfaceAcquireMeas_R[52];
E[87] = E[87]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 26\n"),
#endif
__T_SurfaceAcquireMeas_A26));
E[87] = __T_SurfaceAcquireMeas_R[50]&&E[87];
E[477] = E[87]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 65\n"),
#endif
__T_SurfaceAcquireMeas_A65);
E[472] = E[472]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 43\n"),
#endif
__T_SurfaceAcquireMeas_A43));
E[472] = E[477]||(__T_SurfaceAcquireMeas_R[51]&&E[472]);
E[478] = (E[476]&&!(__T_SurfaceAcquireMeas_R[51]))||E[472];
E[474] = E[474]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 45\n"),
#endif
__T_SurfaceAcquireMeas_A45));
E[474] = E[477]||(__T_SurfaceAcquireMeas_R[52]&&E[474]);
E[479] = (E[476]&&!(__T_SurfaceAcquireMeas_R[52]))||E[474];
E[480] = (E[473]||E[475])&&(E[478]||E[473])&&(E[479]||E[475]);
E[481] = E[476]||__T_SurfaceAcquireMeas_R[53];
E[479] = (E[472]||E[474])&&E[478]&&E[479];
E[476] = (E[481]&&!(E[476]))||E[479];
E[478] = E[476]||E[480];
E[89] = E[89]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 54\n"),
#endif
__T_SurfaceAcquireMeas_A54));
E[89] = E[477]||(__T_SurfaceAcquireMeas_R[53]&&E[89]);
E[477] = (E[481]&&!(__T_SurfaceAcquireMeas_R[53]))||E[89];
E[482] = E[477]||E[123];
E[483] = (E[480]||E[123])&&E[478]&&E[482];
E[481] = __T_SurfaceAcquireMeas_R[50]||E[481];
E[484] = __T_SurfaceAcquireMeas_R[49]||E[481];
E[194] = __T_SurfaceAcquireMeas_R[49]&&E[109]&&!(E[194]);
E[109] = (E[484]&&!(__T_SurfaceAcquireMeas_R[49]))||E[194];
E[485] = E[109]||E[85];
E[87] = E[87]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 65\n"),
#endif
__T_SurfaceAcquireMeas_A65));
E[87] = __T_SurfaceAcquireMeas_R[50]&&E[87];
E[477] = (E[479]||E[89])&&E[476]&&E[477];
E[481] = (E[484]&&!(E[481]))||E[87]||E[477];
E[476] = E[481]||E[483];
E[479] = (E[85]||E[483])&&E[485]&&E[476];
E[486] = E[479]||__T_SurfaceAcquireMeas_R[0];
E[110] = E[139]||E[110];
E[139] = __T_SurfaceAcquireMeas_R[48]&&!(__T_SurfaceAcquireMeas_R[0]);
E[487] = (E[486]&&E[110])||(E[139]&&E[110]);
if (E[487]) {
__T_SurfaceAcquireMeas_A126;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A126\n");
#endif
}
if (E[253]) {
__T_SurfaceAcquireMeas_A127;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A127\n");
#endif
}
if (E[253]) {
__T_SurfaceAcquireMeas_A128;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A128\n");
#endif
}
if (E[250]) {
__T_SurfaceAcquireMeas_A129;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A129\n");
#endif
}
E[488] = __T_SurfaceAcquireMeas_R[56]||__T_SurfaceAcquireMeas_R[57];
E[489] = __T_SurfaceAcquireMeas_R[55]||E[488];
E[195] = __T_SurfaceAcquireMeas_R[55]&&E[252]&&!(E[195]);
E[252] = (E[489]&&!(__T_SurfaceAcquireMeas_R[55]))||E[195];
E[490] = E[252]||E[253];
E[125] = E[125]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 28\n"),
#endif
__T_SurfaceAcquireMeas_A28));
E[125] = __T_SurfaceAcquireMeas_R[56]&&E[125];
E[491] = E[125]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 66\n"),
#endif
__T_SurfaceAcquireMeas_A66));
E[491] = __T_SurfaceAcquireMeas_R[56]&&E[491];
E[125] = E[125]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 66\n"),
#endif
__T_SurfaceAcquireMeas_A66);
E[127] = E[127]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 56\n"),
#endif
__T_SurfaceAcquireMeas_A56));
E[127] = E[125]||(__T_SurfaceAcquireMeas_R[57]&&E[127]);
E[488] = (E[489]&&!(E[488]))||E[491]||E[127];
E[125] = E[488]||E[250];
E[492] = (E[253]||E[250])&&E[490]&&E[125];
E[493] = E[492]||__T_SurfaceAcquireMeas_R[0];
E[494] = __T_SurfaceAcquireMeas_R[54]&&!(__T_SurfaceAcquireMeas_R[0]);
E[495] = (E[493]&&E[121])||(E[494]&&E[121]);
if (E[495]) {
__T_SurfaceAcquireMeas_A130;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A130\n");
#endif
}
if (E[208]) {
__T_SurfaceAcquireMeas_A131;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A131\n");
#endif
}
if (E[208]) {
__T_SurfaceAcquireMeas_A132;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A132\n");
#endif
}
if (!(_true)) {
__T_SurfaceAcquireMeas_A133;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A133\n");
#endif
}
E[496] = __T_SurfaceAcquireMeas_R[72]||__T_SurfaceAcquireMeas_R[73];
E[7] = E[7]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 29\n"),
#endif
__T_SurfaceAcquireMeas_A29));
E[7] = __T_SurfaceAcquireMeas_R[71]&&E[7];
E[497] = E[7]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 67\n"),
#endif
__T_SurfaceAcquireMeas_A67);
E[4] = E[4]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 44\n"),
#endif
__T_SurfaceAcquireMeas_A44));
E[4] = E[497]||(__T_SurfaceAcquireMeas_R[72]&&E[4]);
E[498] = (E[496]&&!(__T_SurfaceAcquireMeas_R[72]))||E[4];
E[497] = E[497]||(__T_SurfaceAcquireMeas_R[73]&&!(__T_SurfaceAcquireMeas_R[0]));
E[499] = (E[496]&&!(__T_SurfaceAcquireMeas_R[73]))||E[497];
E[500] = E[5]&&(E[498]||E[5])&&E[499];
E[496] = __T_SurfaceAcquireMeas_R[71]||E[496];
E[501] = __T_SurfaceAcquireMeas_R[70]||E[496];
E[207] = __T_SurfaceAcquireMeas_R[70]&&E[71]&&!(E[207]);
E[71] = (E[501]&&!(__T_SurfaceAcquireMeas_R[70]))||E[207];
E[502] = E[71]||E[208];
E[7] = E[7]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 67\n"),
#endif
__T_SurfaceAcquireMeas_A67));
E[7] = __T_SurfaceAcquireMeas_R[71]&&E[7];
E[499] = (E[4]||E[497])&&E[498]&&E[499];
E[496] = (E[501]&&!(E[496]))||E[7]||E[499];
E[498] = E[496]||E[500];
E[503] = (E[208]||E[500])&&E[502]&&E[498];
E[504] = E[503]||__T_SurfaceAcquireMeas_R[0];
if (E[451]) {
__T_SurfaceAcquireMeas_A252;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A252\n");
#endif
}
E[505] = __T_SurfaceAcquireMeas_R[69]&&!(__T_SurfaceAcquireMeas_R[0]);
E[506] = (E[504]&&E[451])||(E[505]&&E[451]);
if (E[506]) {
__T_SurfaceAcquireMeas_A134;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A134\n");
#endif
}
if (E[264]) {
__T_SurfaceAcquireMeas_A135;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A135\n");
#endif
}
if (E[264]) {
__T_SurfaceAcquireMeas_A136;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A136\n");
#endif
}
if (E[261]) {
__T_SurfaceAcquireMeas_A137;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A137\n");
#endif
}
E[507] = __T_SurfaceAcquireMeas_R[121]||__T_SurfaceAcquireMeas_R[122];
E[508] = __T_SurfaceAcquireMeas_R[120]||E[507];
E[263] = __T_SurfaceAcquireMeas_R[120]&&E[263]&&!(E[200]);
E[509] = (E[508]&&!(__T_SurfaceAcquireMeas_R[120]))||E[263];
E[510] = E[509]||E[264];
E[141] = E[141]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 31\n"),
#endif
__T_SurfaceAcquireMeas_A31));
E[141] = __T_SurfaceAcquireMeas_R[121]&&E[141];
E[511] = E[141]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 68\n"),
#endif
__T_SurfaceAcquireMeas_A68));
E[511] = __T_SurfaceAcquireMeas_R[121]&&E[511];
E[141] = E[141]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 68\n"),
#endif
__T_SurfaceAcquireMeas_A68);
E[143] = E[143]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 57\n"),
#endif
__T_SurfaceAcquireMeas_A57));
E[143] = E[141]||(__T_SurfaceAcquireMeas_R[122]&&E[143]);
E[507] = (E[508]&&!(E[507]))||E[511]||E[143];
E[141] = E[507]||E[261];
E[512] = (E[264]||E[261])&&E[510]&&E[141];
E[513] = E[512]||__T_SurfaceAcquireMeas_R[0];
E[514] = __T_SurfaceAcquireMeas_R[119]&&!(__T_SurfaceAcquireMeas_R[0]);
E[515] = (E[513]&&E[132])||(E[514]&&E[132]);
if (E[515]) {
__T_SurfaceAcquireMeas_A138;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A138\n");
#endif
}
E[516] = __T_SurfaceAcquireMeas_R[59]&&!(__T_SurfaceAcquireMeas_R[0]);
E[517] = E[516]&&E[201];
if (E[517]) {
__T_SurfaceAcquireMeas_A139;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A139\n");
#endif
}
if (E[517]) {
__T_SurfaceAcquireMeas_A140;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A140\n");
#endif
}
E[198] = E[198]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 32\n"),
#endif
__T_SurfaceAcquireMeas_A32));
E[198] = __T_SurfaceAcquireMeas_R[61]&&E[198];
E[518] = E[198]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 58\n"),
#endif
__T_SurfaceAcquireMeas_A58);
if (E[518]) {
__T_SurfaceAcquireMeas_A141;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A141\n");
#endif
}
E[519] = __T_SurfaceAcquireMeas_R[60]||__T_SurfaceAcquireMeas_R[61];
E[520] = __T_SurfaceAcquireMeas_R[59]||E[519];
E[201] = __T_SurfaceAcquireMeas_R[59]&&E[516]&&!(E[201]);
E[516] = (E[520]&&!(__T_SurfaceAcquireMeas_R[59]))||E[201];
E[521] = E[516]||E[517];
E[196] = E[196]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 33\n"),
#endif
__T_SurfaceAcquireMeas_A33));
E[196] = __T_SurfaceAcquireMeas_R[60]&&E[196];
E[522] = E[196]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 69\n"),
#endif
__T_SurfaceAcquireMeas_A69));
E[522] = __T_SurfaceAcquireMeas_R[60]&&E[522];
E[196] = E[196]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 69\n"),
#endif
__T_SurfaceAcquireMeas_A69);
E[198] = E[198]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 58\n"),
#endif
__T_SurfaceAcquireMeas_A58));
E[198] = E[196]||(__T_SurfaceAcquireMeas_R[61]&&E[198]);
E[519] = (E[520]&&!(E[519]))||E[522]||E[198];
E[196] = E[519]||E[518];
E[523] = (E[517]||E[518])&&E[521]&&E[196];
if (!(_true)) {
__T_SurfaceAcquireMeas_A142;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A142\n");
#endif
}
if (E[279]) {
__T_SurfaceAcquireMeas_A143;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A143\n");
#endif
}
if (E[279]) {
__T_SurfaceAcquireMeas_A144;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A144\n");
#endif
}
if (E[267]) {
__T_SurfaceAcquireMeas_A145;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A145\n");
#endif
}
E[524] = __T_SurfaceAcquireMeas_R[63]||E[268];
E[192] = __T_SurfaceAcquireMeas_R[63]&&E[64]&&!(E[192]);
E[64] = (E[524]&&!(__T_SurfaceAcquireMeas_R[63]))||E[192];
E[525] = E[64]||E[279];
E[154] = E[154]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 61\n"),
#endif
__T_SurfaceAcquireMeas_A61));
E[154] = __T_SurfaceAcquireMeas_R[64]&&E[154];
E[156] = E[156]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 59\n"),
#endif
__T_SurfaceAcquireMeas_A59));
E[156] = __T_SurfaceAcquireMeas_R[65]&&E[156];
E[158] = E[158]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 55\n"),
#endif
__T_SurfaceAcquireMeas_A55));
E[158] = __T_SurfaceAcquireMeas_R[66]&&E[158];
E[160] = E[160]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 48\n"),
#endif
__T_SurfaceAcquireMeas_A48));
E[160] = __T_SurfaceAcquireMeas_R[67]&&E[160];
E[162] = E[162]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 49\n"),
#endif
__T_SurfaceAcquireMeas_A49));
E[162] = __T_SurfaceAcquireMeas_R[68]&&E[162];
E[269] = E[269]||E[154];
E[270] = E[270]||E[156];
E[271] = E[271]||E[158];
E[272] = E[272]||E[160];
E[273] = E[273]||E[162];
E[526] = (E[154]||E[156]||E[158]||E[160]||E[162])&&E[269]&&E[270]&&E[271]&&E[272]&&E[273];
E[268] = (E[524]&&!(E[268]))||E[526];
E[527] = E[268]||E[267];
E[528] = (E[279]||E[267])&&E[525]&&E[527];
E[529] = E[528]||__T_SurfaceAcquireMeas_R[0];
E[530] = __T_SurfaceAcquireMeas_R[62]&&!(__T_SurfaceAcquireMeas_R[0]);
E[531] = (E[529]&&E[277])||(E[530]&&E[277]);
if (E[531]) {
__T_SurfaceAcquireMeas_A146;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A146\n");
#endif
}
if (E[297]) {
__T_SurfaceAcquireMeas_A147;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A147\n");
#endif
}
E[107] = E[107]&&E[292]&&(E[290]||E[107]);
E[169] = (E[170]||E[172])&&E[303]&&(E[169]||E[170]||E[172]);
E[167] = (E[175]||E[177])&&E[311]&&(E[167]||E[175]||E[177]);
if (E[294]||E[107]||E[305]||E[169]||E[313]||E[167]) {
__T_SurfaceAcquireMeas_A148;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A148\n");
#endif
}
if (E[342]||E[380]||E[404]||E[453]||E[421]||E[368]||E[325]||E[466]) {
__T_SurfaceAcquireMeas_A149;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A149\n");
#endif
}
E[332] = E[101]&&E[333]&&(E[332]||E[101]);
E[330] = (E[99]||E[332])&&E[339]&&(E[330]||E[99]||E[332]);
E[352] = E[184]&&E[351]&&(E[352]||E[184]);
E[350] = (E[182]||E[352])&&E[355]&&(E[350]||E[182]||E[352]);
E[373] = E[117]&&E[372]&&(E[373]||E[117]);
E[371] = (E[115]||E[373])&&E[376]&&(E[371]||E[115]||E[373]);
E[391] = E[82]&&E[387]&&(E[391]||E[82]);
E[385] = (E[80]||E[391])&&E[394]&&(E[385]||E[80]||E[391]);
E[431] = E[191]&&E[430]&&(E[431]||E[191]);
E[429] = (E[189]||E[431])&&E[434]&&(E[429]||E[189]||E[431]);
E[440] = E[18]&&E[439]&&(E[440]||E[18]);
E[438] = (E[16]||E[440])&&E[443]&&(E[438]||E[16]||E[440]);
E[415] = E[26]&&E[411]&&(E[415]||E[26]);
E[409] = (E[24]||E[415])&&E[418]&&(E[409]||E[24]||E[415]);
E[361] = E[37]&&E[360]&&(E[361]||E[37]);
E[359] = (E[35]||E[361])&&E[364]&&(E[359]||E[35]||E[361]);
E[318] = E[48]&&E[317]&&(E[318]||E[48]);
E[316] = (E[46]||E[318])&&E[321]&&(E[316]||E[46]||E[318]);
E[458] = E[59]&&E[457]&&(E[458]||E[59]);
E[456] = (E[57]||E[458])&&E[462]&&(E[456]||E[57]||E[458]);
if (E[334]||E[330]||E[356]||E[350]||E[377]||E[371]||E[388]||E[385]||E[435]||E[429]||E[444]||E[438]||E[412]||E[409]||E[365]||E[359]||E[322]||E[316]||E[463]||E[456]) {
__T_SurfaceAcquireMeas_A150;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A150\n");
#endif
}
if (E[487]) {
__T_SurfaceAcquireMeas_A151;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A151\n");
#endif
}
E[482] = E[90]&&E[478]&&(E[482]||E[90]);
E[476] = (E[88]||E[482])&&E[485]&&(E[476]||E[88]||E[482]);
if (E[479]||E[476]) {
__T_SurfaceAcquireMeas_A152;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A152\n");
#endif
}
if (E[506]) {
__T_SurfaceAcquireMeas_A153;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A153\n");
#endif
}
E[8] = E[8]&&E[502]&&(E[498]||E[8]);
if (E[503]||E[8]) {
__T_SurfaceAcquireMeas_A154;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A154\n");
#endif
}
if (E[289]||E[495]||E[515]) {
__T_SurfaceAcquireMeas_A155;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A155\n");
#endif
}
E[133] = (E[134]||E[136])&&E[284]&&(E[133]||E[134]||E[136]);
E[125] = (E[126]||E[128])&&E[490]&&(E[125]||E[126]||E[128]);
E[196] = (E[197]||E[199])&&E[521]&&(E[196]||E[197]||E[199]);
E[141] = (E[142]||E[144])&&E[510]&&(E[141]||E[142]||E[144]);
if (E[286]||E[133]||E[492]||E[125]||E[523]||E[196]||E[512]||E[141]) {
__T_SurfaceAcquireMeas_A156;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A156\n");
#endif
}
if (E[531]) {
__T_SurfaceAcquireMeas_A157;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A157\n");
#endif
}
E[150] = E[150]&&E[468]&&(E[470]||E[150]);
E[273] = (E[155]||E[157]||E[159]||E[161]||E[163])&&(E[269]||E[155])&&(E[270]||E[157])&&(E[271]||E[159])&&(E[272]||E[161])&&(E[273]||E[163]);
E[527] = E[273]&&E[525]&&(E[527]||E[273]);
if (E[471]||E[150]||E[528]||E[527]) {
__T_SurfaceAcquireMeas_A158;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A158\n");
#endif
}
if (E[203]) {
__T_SurfaceAcquireMeas_A159;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A159\n");
#endif
}
if (E[203]) {
__T_SurfaceAcquireMeas_A160;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A160\n");
#endif
}
if (E[67]) {
__T_SurfaceAcquireMeas_A161;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A161\n");
#endif
}
if (E[451]) {
__T_SurfaceAcquireMeas_A162;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A162\n");
#endif
}
if (E[54]) {
__T_SurfaceAcquireMeas_A163;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A163\n");
#endif
}
if (E[96]||E[347]||E[32]) {
__T_SurfaceAcquireMeas_A164;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A164\n");
#endif
}
if (E[112]||E[426]||E[13]) {
__T_SurfaceAcquireMeas_A165;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A165\n");
#endif
}
if (E[43]) {
__T_SurfaceAcquireMeas_A166;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A166\n");
#endif
}
if (E[329]||E[384]||E[473]||E[408]) {
__T_SurfaceAcquireMeas_A167;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A167\n");
#endif
}
if (E[5]) {
__T_SurfaceAcquireMeas_A168;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A168\n");
#endif
}
if (E[327]||E[382]||E[475]||E[406]) {
__T_SurfaceAcquireMeas_A169;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A169\n");
#endif
}
if (E[84]||E[92]||E[97]||E[103]||E[108]||E[113]||E[119]||E[122]||E[130]||E[138]||E[146]||E[1]||E[151]||E[165]) {
__T_SurfaceAcquireMeas_A170;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A170\n");
#endif
}
if (E[403]) {
__T_SurfaceAcquireMeas_A171;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A171\n");
#endif
}
if (E[76]) {
__T_SurfaceAcquireMeas_A172;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A172\n");
#endif
}
if (E[281]) {
__T_SurfaceAcquireMeas_A173;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A173\n");
#endif
}
if (E[76]) {
__T_SurfaceAcquireMeas_A174;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A174\n");
#endif
}
if (!(_true)) {
__T_SurfaceAcquireMeas_A175;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A175\n");
#endif
}
if (!(_true)) {
__T_SurfaceAcquireMeas_A176;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A176\n");
#endif
}
if (!(_true)) {
__T_SurfaceAcquireMeas_A177;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A177\n");
#endif
}
E[525] = __T_SurfaceAcquireMeas_R[118]&&!(__T_SurfaceAcquireMeas_R[0]);
E[272] = E[525]&&E[67];
E[271] = __T_SurfaceAcquireMeas_R[156]&&!(__T_SurfaceAcquireMeas_R[0]);
E[270] = E[271]&&E[281];
if (E[272]||E[270]) {
__T_SurfaceAcquireMeas_A178;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A178\n");
#endif
}
if (!(_true)) {
__T_SurfaceAcquireMeas_A179;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_SurfaceAcquireMeas_A179\n");
#endif
}
E[104] = (E[97]||E[103]||E[108])&&(E[93]||E[97]||E[103])&&(E[104]||E[108]);
E[152] = (E[92]||E[113]||E[119]||E[122]||E[130]||E[138]||E[146]||E[1]||E[104]||E[84]||E[151]||E[165])&&(E[251]||E[92]||E[113]||E[119]||E[122]||E[130]||E[138]||E[146]||E[1]||E[104]||E[84])&&(E[152]||E[151]||E[165]);
E[153] = E[152]&&(E[78]||E[152])&&E[153];
E[77] = E[153]&&E[398]&&(E[77]||E[153]);
E[172] = E[170]||E[172];
E[170] = E[299]||E[300];
E[177] = E[175]||E[177];
E[175] = E[307]||E[308];
E[136] = E[134]||E[136];
E[134] = E[258]||E[255];
E[101] = E[99]||E[101];
E[99] = E[239]||E[96]||E[327]||E[329]||E[236];
E[184] = E[182]||E[184];
E[182] = E[344]||E[347]||E[345];
E[117] = E[115]||E[117];
E[115] = E[246]||E[112]||E[237];
E[82] = E[80]||E[82];
E[80] = E[244]||E[382]||E[384]||E[0];
E[191] = E[189]||E[191];
E[189] = E[423]||E[426]||E[424];
E[398] = E[275]||E[262];
E[90] = E[88]||E[90];
E[88] = E[85]||E[473]||E[475]||E[123];
E[128] = E[126]||E[128];
E[126] = E[253]||E[250];
E[199] = E[197]||E[199];
E[197] = E[517]||E[518];
E[163] = E[155]||E[157]||E[159]||E[161]||E[163];
E[161] = E[279]||E[267];
E[68] = (E[6]||E[9]||E[14]||E[20])&&(E[2]||E[6]||E[9])&&(E[68]||E[14]||E[20]);
E[225] = (E[28]||E[33]||E[39]||E[44]||E[50]||E[55]||E[61]||E[68])&&(E[213]||E[28]||E[33]||E[39]||E[44]||E[50]||E[55]||E[61]||E[68])&&E[225];
E[10] = E[225]&&E[447]&&(E[10]||E[225]);
E[447] = E[208]||E[5];
E[18] = E[16]||E[18];
E[16] = E[210]||E[13]||E[21];
E[26] = E[24]||E[26];
E[24] = E[215]||E[406]||E[408]||E[212];
E[37] = E[35]||E[37];
E[35] = E[221]||E[32]||E[218];
E[48] = E[46]||E[48];
E[46] = E[227]||E[43]||E[224];
E[59] = E[57]||E[59];
E[57] = E[230]||E[54]||E[66];
E[61] = E[6]||E[9]||E[14]||E[20]||E[28]||E[33]||E[39]||E[44]||E[50]||E[55]||E[61];
E[55] = E[203]||E[67];
E[144] = E[142]||E[144];
E[142] = E[264]||E[261];
E[50] = E[84]||E[92]||E[97]||E[103]||E[108]||E[113]||E[119]||E[122]||E[130]||E[138]||E[146]||E[1]||E[151]||E[165];
E[44] = E[76]||E[281];
E[291] = E[291]||__T_SurfaceAcquireMeas_R[1];
E[302] = E[302]||__T_SurfaceAcquireMeas_R[5];
E[310] = E[310]||__T_SurfaceAcquireMeas_R[9];
E[283] = E[283]||__T_SurfaceAcquireMeas_R[13];
E[338] = E[338]||__T_SurfaceAcquireMeas_R[17];
E[354] = E[354]||__T_SurfaceAcquireMeas_R[24];
E[375] = E[375]||__T_SurfaceAcquireMeas_R[29];
E[393] = E[393]||__T_SurfaceAcquireMeas_R[34];
E[433] = E[433]||__T_SurfaceAcquireMeas_R[40];
E[467] = E[467]||__T_SurfaceAcquireMeas_R[45];
E[484] = E[484]||__T_SurfaceAcquireMeas_R[48];
E[489] = E[489]||__T_SurfaceAcquireMeas_R[54];
E[520] = E[520]||__T_SurfaceAcquireMeas_R[58];
E[524] = E[524]||__T_SurfaceAcquireMeas_R[62];
E[501] = E[501]||__T_SurfaceAcquireMeas_R[69];
E[442] = E[442]||__T_SurfaceAcquireMeas_R[74];
E[417] = E[417]||__T_SurfaceAcquireMeas_R[79];
E[363] = E[363]||__T_SurfaceAcquireMeas_R[85];
E[320] = E[320]||__T_SurfaceAcquireMeas_R[90];
E[460] = E[460]||__T_SurfaceAcquireMeas_R[95];
E[446] = E[446]||__T_SurfaceAcquireMeas_R[100];
E[39] = __T_SurfaceAcquireMeas_R[116]||__T_SurfaceAcquireMeas_R[115];
E[33] = __T_SurfaceAcquireMeas_R[118]||__T_SurfaceAcquireMeas_R[117];
E[28] = E[501]||E[442]||E[417]||E[363]||E[320]||E[460]||E[446]||E[39]||E[33];
E[508] = E[508]||__T_SurfaceAcquireMeas_R[119];
E[396] = E[396]||__T_SurfaceAcquireMeas_R[123];
E[20] = __T_SurfaceAcquireMeas_R[154]||__T_SurfaceAcquireMeas_R[153];
E[14] = __T_SurfaceAcquireMeas_R[156]||__T_SurfaceAcquireMeas_R[155];
E[9] = E[291]||E[302]||E[310]||E[283]||E[338]||E[354]||E[375]||E[393]||E[433]||E[467]||E[484]||E[489]||E[520]||E[524]||E[28]||E[508]||E[396]||E[20]||E[14];
E[296] = (E[295]&&!(E[131]))||(__T_SurfaceAcquireMeas_R[1]&&E[296]&&!(E[131]));
E[290] = E[297]||((E[168]||E[293]||E[106])&&E[241]&&E[290])||E[296];
E[291] = (E[9]&&!(E[291]))||E[107]||E[290];
E[241] = E[305]||__T_SurfaceAcquireMeas_R[0]||(__T_SurfaceAcquireMeas_R[5]&&__T_SurfaceAcquireMeas_R[5]&&!(__T_SurfaceAcquireMeas_R[0]));
E[301] = ((E[174]||E[304]||E[171])&&E[298]&&E[301])||E[241];
E[302] = (E[9]&&!(E[302]))||E[169]||E[301];
E[298] = E[313]||__T_SurfaceAcquireMeas_R[0]||(__T_SurfaceAcquireMeas_R[9]&&__T_SurfaceAcquireMeas_R[9]&&!(__T_SurfaceAcquireMeas_R[0]));
E[309] = ((E[179]||E[312]||E[176])&&E[306]&&E[309])||E[298];
E[310] = (E[9]&&!(E[310]))||E[167]||E[309];
E[124] = (E[287]&&!(E[124]))||(__T_SurfaceAcquireMeas_R[13]&&E[288]&&!(E[124]));
E[282] = E[289]||((E[173]||E[285]||E[135])&&E[257]&&E[282])||E[124];
E[283] = (E[9]&&!(E[283]))||E[133]||E[282];
E[131] = (E[340]&&!(E[131]))||(__T_SurfaceAcquireMeas_R[17]&&E[341]&&!(E[131]));
E[336] = E[342]||((E[180]||E[98]||E[331])&&E[238]&&E[336])||E[131];
E[338] = (E[9]&&!(E[338]))||E[330]||E[336];
E[238] = E[356]||__T_SurfaceAcquireMeas_R[0]||(__T_SurfaceAcquireMeas_R[24]&&__T_SurfaceAcquireMeas_R[24]&&!(__T_SurfaceAcquireMeas_R[0]));
E[348] = ((E[186]||E[181]||E[349])&&E[343]&&E[348])||E[238];
E[354] = (E[9]&&!(E[354]))||E[350]||E[348];
E[379] = (E[378]&&!(E[94]))||(__T_SurfaceAcquireMeas_R[29]&&E[379]&&!(E[94]));
E[369] = E[380]||((E[178]||E[114]||E[370])&&E[245]&&E[369])||E[379];
E[375] = (E[9]&&!(E[375]))||E[371]||E[369];
E[401] = (E[395]&&!(E[403]))||(__T_SurfaceAcquireMeas_R[34]&&E[401]&&!(E[403]));
E[390] = E[404]||((E[187]||E[79]||E[386])&&E[120]&&E[390])||E[401];
E[393] = (E[9]&&!(E[393]))||E[385]||E[390];
E[120] = E[435]||__T_SurfaceAcquireMeas_R[0]||(__T_SurfaceAcquireMeas_R[40]&&__T_SurfaceAcquireMeas_R[40]&&!(__T_SurfaceAcquireMeas_R[0]));
E[427] = ((E[193]||E[188]||E[428])&&E[422]&&E[427])||E[120];
E[433] = (E[9]&&!(E[433]))||E[429]||E[427];
E[422] = E[471]||__T_SurfaceAcquireMeas_R[0]||(__T_SurfaceAcquireMeas_R[45]&&__T_SurfaceAcquireMeas_R[45]&&!(__T_SurfaceAcquireMeas_R[0]));
E[469] = ((E[185]||E[149])&&E[274]&&E[469])||E[422];
E[467] = (E[9]&&!(E[467]))||E[150]||E[469];
E[110] = (E[486]&&!(E[110]))||(__T_SurfaceAcquireMeas_R[48]&&E[139]&&!(E[110]));
E[481] = E[487]||((E[194]||E[87]||E[477])&&E[109]&&E[481])||E[110];
E[484] = (E[9]&&!(E[484]))||E[476]||E[481];
E[121] = (E[493]&&!(E[121]))||(__T_SurfaceAcquireMeas_R[54]&&E[494]&&!(E[121]));
E[488] = E[495]||((E[195]||E[491]||E[127])&&E[252]&&E[488])||E[121];
E[489] = (E[9]&&!(E[489]))||E[125]||E[488];
E[252] = E[523]||__T_SurfaceAcquireMeas_R[0]||(__T_SurfaceAcquireMeas_R[58]&&__T_SurfaceAcquireMeas_R[58]&&!(__T_SurfaceAcquireMeas_R[0]));
E[519] = ((E[201]||E[522]||E[198])&&E[516]&&E[519])||E[252];
E[520] = (E[9]&&!(E[520]))||E[196]||E[519];
E[277] = (E[529]&&!(E[277]))||(__T_SurfaceAcquireMeas_R[62]&&E[530]&&!(E[277]));
E[268] = E[531]||((E[192]||E[526])&&E[64]&&E[268])||E[277];
E[524] = (E[9]&&!(E[524]))||E[527]||E[268];
E[505] = (E[504]&&!(E[451]))||(__T_SurfaceAcquireMeas_R[69]&&E[505]&&!(E[451]));
E[496] = E[506]||((E[207]||E[7]||E[499])&&E[71]&&E[496])||E[505];
E[501] = (E[28]&&!(E[501]))||E[8]||E[496];
E[452] = (E[445]&&!(E[451]))||(__T_SurfaceAcquireMeas_R[74]&&E[452]&&!(E[451]));
E[436] = E[453]||((E[205]||E[15]||E[437])&&E[70]&&E[436])||E[452];
E[442] = (E[28]&&!(E[442]))||E[438]||E[436];
E[420] = (E[419]&&!(E[69]))||(__T_SurfaceAcquireMeas_R[79]&&E[420]&&!(E[69]));
E[414] = E[421]||((E[206]||E[23]||E[410])&&E[214]&&E[414])||E[420];
E[417] = (E[28]&&!(E[417]))||E[409]||E[414];
E[367] = (E[366]&&!(E[29]))||(__T_SurfaceAcquireMeas_R[85]&&E[367]&&!(E[29]));
E[357] = E[368]||((E[19]||E[34]||E[358])&&E[220]&&E[357])||E[367];
E[363] = (E[28]&&!(E[363]))||E[359]||E[357];
E[324] = (E[323]&&!(E[217]))||(__T_SurfaceAcquireMeas_R[90]&&E[324]&&!(E[217]));
E[314] = E[325]||((E[27]||E[45]||E[315])&&E[226]&&E[314])||E[324];
E[320] = (E[28]&&!(E[320]))||E[316]||E[314];
E[465] = (E[464]&&!(E[223]))||(__T_SurfaceAcquireMeas_R[95]&&E[465]&&!(E[223]));
E[454] = E[466]||((E[229]||E[56]||E[455])&&E[461]&&E[454])||E[465];
E[460] = (E[28]&&!(E[460]))||E[456]||E[454];
E[450] = (E[449]&&!(E[65]))||(__T_SurfaceAcquireMeas_R[100]&&E[450]&&!(E[65]));
E[40] = E[451]||((E[202]||E[51])&&E[73]&&E[40])||E[450];
E[446] = (E[28]&&!(E[446]))||E[40];
E[73] = __T_SurfaceAcquireMeas_R[115]&&!(__T_SurfaceAcquireMeas_R[0]);
E[204] = (__T_SurfaceAcquireMeas_R[0]&&E[65])||(E[49]&&E[60])||E[38]||(__T_SurfaceAcquireMeas_R[116]&&E[72]&&!(E[204]))||(E[73]&&E[65]);
E[73] = (__T_SurfaceAcquireMeas_R[0]&&!(E[65]))||(__T_SurfaceAcquireMeas_R[115]&&E[73]&&!(E[65]));
E[39] = (E[28]&&!(E[39]))||E[204]||E[73];
E[72] = __T_SurfaceAcquireMeas_R[117]&&!(__T_SurfaceAcquireMeas_R[0]);
E[525] = (__T_SurfaceAcquireMeas_R[0]&&E[65])||E[272]||(__T_SurfaceAcquireMeas_R[118]&&E[525]&&!(E[67]))||(E[72]&&E[65]);
E[65] = (__T_SurfaceAcquireMeas_R[0]&&!(E[65]))||(__T_SurfaceAcquireMeas_R[117]&&E[72]&&!(E[65]));
E[33] = (E[28]&&!(E[33]))||E[525]||E[65];
E[72] = E[10]&&E[501]&&E[442]&&E[417]&&E[363]&&E[320]&&E[460]&&(E[446]||E[10])&&E[39]&&E[33];
E[33] = (E[496]||E[436]||E[414]||E[357]||E[314]||E[454]||E[40]||E[204]||E[73]||E[525]||E[65])&&E[501]&&E[442]&&E[417]&&E[363]&&E[320]&&E[460]&&E[446]&&E[39]&&E[33];
E[28] = (E[9]&&!(E[28]))||E[72]||E[33];
E[132] = (E[513]&&!(E[132]))||(__T_SurfaceAcquireMeas_R[119]&&E[514]&&!(E[132]));
E[507] = E[515]||((E[263]||E[511]||E[143])&&E[509]&&E[507])||E[132];
E[508] = (E[9]&&!(E[508]))||E[141]||E[507];
E[400] = E[400]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1));
E[402] = E[402]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1));
E[402] = E[400]||(__T_SurfaceAcquireMeas_R[123]&&E[402]);
E[256] = E[403]||((E[75]||E[147])&&E[397]&&E[256])||E[402];
E[396] = (E[9]&&!(E[396]))||E[256];
E[397] = __T_SurfaceAcquireMeas_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1);
E[147] = __T_SurfaceAcquireMeas_R[153]&&!(__T_SurfaceAcquireMeas_R[0]);
E[400] = E[147]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1);
E[400] = E[397]||(E[62]&&E[145])||E[200]||(__T_SurfaceAcquireMeas_R[154]&&E[74]&&!(E[166]))||E[400];
E[166] = __T_SurfaceAcquireMeas_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1));
E[147] = E[147]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1));
E[147] = E[166]||(__T_SurfaceAcquireMeas_R[153]&&E[147]);
E[20] = (E[9]&&!(E[20]))||E[400]||E[147];
E[166] = __T_SurfaceAcquireMeas_R[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1);
E[74] = __T_SurfaceAcquireMeas_R[155]&&!(__T_SurfaceAcquireMeas_R[0]);
E[200] = E[74]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1);
E[200] = E[166]||E[270]||(__T_SurfaceAcquireMeas_R[156]&&E[271]&&!(E[281]))||E[200];
E[271] = __T_SurfaceAcquireMeas_R[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1));
E[74] = E[74]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 1\n"),
#endif
__T_SurfaceAcquireMeas_A1));
E[74] = E[271]||(__T_SurfaceAcquireMeas_R[155]&&E[74]);
E[14] = (E[9]&&!(E[14]))||E[200]||E[74];
E[9] = E[77]&&E[291]&&E[302]&&E[310]&&E[283]&&E[338]&&E[354]&&E[375]&&E[393]&&E[433]&&E[467]&&E[484]&&E[489]&&E[520]&&E[524]&&E[28]&&E[508]&&(E[396]||E[77])&&E[20]&&E[14];
E[271] = E[9];
E[14] = (E[290]||E[301]||E[309]||E[282]||E[336]||E[348]||E[369]||E[390]||E[427]||E[469]||E[481]||E[488]||E[519]||E[268]||E[33]||E[507]||E[256]||E[400]||E[147]||E[200]||E[74])&&E[291]&&E[302]&&E[310]&&E[283]&&E[338]&&E[354]&&E[375]&&E[393]&&E[433]&&E[467]&&E[484]&&E[489]&&E[520]&&E[524]&&E[28]&&E[508]&&E[396]&&E[20]&&E[14];
__T_SurfaceAcquireMeas_R[1] = E[296]&&!(E[9]);
E[296] = E[294]||E[107]||E[107]||E[9];
__T_SurfaceAcquireMeas_R[2] = (E[297]&&!(E[9]))||(E[168]&&!(E[296]));
__T_SurfaceAcquireMeas_R[3] = (E[297]&&!(E[9]))||(E[293]&&!(E[296]));
__T_SurfaceAcquireMeas_R[4] = E[106]&&!(E[296]);
__T_SurfaceAcquireMeas_R[5] = E[241]&&!(E[9]);
E[241] = E[305]||E[169]||E[169]||E[9];
__T_SurfaceAcquireMeas_R[6] = E[174]&&!(E[241]);
__T_SurfaceAcquireMeas_R[7] = E[304]&&!(E[241]);
__T_SurfaceAcquireMeas_R[8] = E[171]&&!(E[241]);
__T_SurfaceAcquireMeas_R[9] = E[298]&&!(E[9]);
E[298] = E[313]||E[167]||E[167]||E[9];
__T_SurfaceAcquireMeas_R[10] = E[179]&&!(E[298]);
__T_SurfaceAcquireMeas_R[11] = E[312]&&!(E[298]);
__T_SurfaceAcquireMeas_R[12] = E[176]&&!(E[298]);
__T_SurfaceAcquireMeas_R[13] = E[124]&&!(E[9]);
E[124] = E[286]||E[133]||E[133]||E[9];
__T_SurfaceAcquireMeas_R[14] = (E[289]&&!(E[9]))||(E[173]&&!(E[124]));
__T_SurfaceAcquireMeas_R[15] = (E[289]&&!(E[9]))||(E[285]&&!(E[124]));
__T_SurfaceAcquireMeas_R[16] = E[135]&&!(E[124]);
__T_SurfaceAcquireMeas_R[17] = E[131]&&!(E[9]);
E[131] = E[334]||E[330]||E[330]||E[9];
__T_SurfaceAcquireMeas_R[18] = (E[342]&&!(E[9]))||(E[180]&&!(E[131]));
__T_SurfaceAcquireMeas_R[19] = (E[342]&&!(E[9]))||(E[98]&&!(E[131]));
E[332] = E[131]||E[337]||E[332];
E[335] = E[332]||E[335];
__T_SurfaceAcquireMeas_R[20] = E[95]&&!(E[335]);
__T_SurfaceAcquireMeas_R[21] = E[326]&&!(E[335]);
__T_SurfaceAcquireMeas_R[22] = E[328]&&!(E[335]);
__T_SurfaceAcquireMeas_R[23] = E[100]&&!(E[332]);
__T_SurfaceAcquireMeas_R[24] = E[238]&&!(E[9]);
E[238] = E[356]||E[350]||E[350]||E[9];
__T_SurfaceAcquireMeas_R[25] = E[186]&&!(E[238]);
__T_SurfaceAcquireMeas_R[26] = E[181]&&!(E[238]);
E[352] = E[238]||E[353]||E[352];
__T_SurfaceAcquireMeas_R[27] = E[346]&&!(E[352]);
__T_SurfaceAcquireMeas_R[28] = E[183]&&!(E[352]);
__T_SurfaceAcquireMeas_R[29] = E[379]&&!(E[9]);
E[379] = E[377]||E[371]||E[371]||E[9];
__T_SurfaceAcquireMeas_R[30] = (E[380]&&!(E[9]))||(E[178]&&!(E[379]));
__T_SurfaceAcquireMeas_R[31] = (E[380]&&!(E[9]))||(E[114]&&!(E[379]));
E[373] = E[379]||E[374]||E[373];
__T_SurfaceAcquireMeas_R[32] = E[111]&&!(E[373]);
__T_SurfaceAcquireMeas_R[33] = E[116]&&!(E[373]);
__T_SurfaceAcquireMeas_R[34] = E[401]&&!(E[9]);
E[401] = E[388]||E[385]||E[385]||E[9];
__T_SurfaceAcquireMeas_R[35] = (E[404]&&!(E[9]))||(E[187]&&!(E[401]));
__T_SurfaceAcquireMeas_R[36] = (E[404]&&!(E[9]))||(E[79]&&!(E[401]));
E[391] = E[401]||E[392]||E[391];
E[389] = E[391]||E[389];
__T_SurfaceAcquireMeas_R[37] = E[381]&&!(E[389]);
__T_SurfaceAcquireMeas_R[38] = E[383]&&!(E[389]);
__T_SurfaceAcquireMeas_R[39] = E[81]&&!(E[391]);
__T_SurfaceAcquireMeas_R[40] = E[120]&&!(E[9]);
E[120] = E[435]||E[429]||E[429]||E[9];
__T_SurfaceAcquireMeas_R[41] = E[193]&&!(E[120]);
__T_SurfaceAcquireMeas_R[42] = E[188]&&!(E[120]);
E[431] = E[120]||E[432]||E[431];
__T_SurfaceAcquireMeas_R[43] = E[425]&&!(E[431]);
__T_SurfaceAcquireMeas_R[44] = E[190]&&!(E[431]);
__T_SurfaceAcquireMeas_R[45] = E[422]&&!(E[9]);
E[422] = E[471]||E[150]||E[150]||E[9];
__T_SurfaceAcquireMeas_R[46] = E[185]&&!(E[422]);
__T_SurfaceAcquireMeas_R[47] = E[149]&&!(E[422]);
__T_SurfaceAcquireMeas_R[48] = E[110]&&!(E[9]);
E[110] = E[479]||E[476]||E[476]||E[9];
__T_SurfaceAcquireMeas_R[49] = (E[487]&&!(E[9]))||(E[194]&&!(E[110]));
__T_SurfaceAcquireMeas_R[50] = (E[487]&&!(E[9]))||(E[87]&&!(E[110]));
E[482] = E[110]||E[483]||E[482];
E[480] = E[482]||E[480];
__T_SurfaceAcquireMeas_R[51] = E[472]&&!(E[480]);
__T_SurfaceAcquireMeas_R[52] = E[474]&&!(E[480]);
__T_SurfaceAcquireMeas_R[53] = E[89]&&!(E[482]);
__T_SurfaceAcquireMeas_R[54] = E[121]&&!(E[9]);
E[121] = E[492]||E[125]||E[125]||E[9];
__T_SurfaceAcquireMeas_R[55] = (E[495]&&!(E[9]))||(E[195]&&!(E[121]));
__T_SurfaceAcquireMeas_R[56] = (E[495]&&!(E[9]))||(E[491]&&!(E[121]));
__T_SurfaceAcquireMeas_R[57] = E[127]&&!(E[121]);
__T_SurfaceAcquireMeas_R[58] = E[252]&&!(E[9]);
E[252] = E[523]||E[196]||E[196]||E[9];
__T_SurfaceAcquireMeas_R[59] = E[201]&&!(E[252]);
__T_SurfaceAcquireMeas_R[60] = E[522]&&!(E[252]);
__T_SurfaceAcquireMeas_R[61] = E[198]&&!(E[252]);
__T_SurfaceAcquireMeas_R[62] = E[277]&&!(E[9]);
E[277] = E[528]||E[527]||E[527]||E[9];
__T_SurfaceAcquireMeas_R[63] = (E[531]&&!(E[9]))||(E[192]&&!(E[277]));
E[277] = E[273]||E[277];
__T_SurfaceAcquireMeas_R[64] = (E[531]&&!(E[9]))||(E[154]&&!(E[277]));
__T_SurfaceAcquireMeas_R[65] = (E[531]&&!(E[9]))||(E[156]&&!(E[277]));
__T_SurfaceAcquireMeas_R[66] = (E[531]&&!(E[9]))||(E[158]&&!(E[277]));
__T_SurfaceAcquireMeas_R[67] = (E[531]&&!(E[9]))||(E[160]&&!(E[277]));
__T_SurfaceAcquireMeas_R[68] = (E[531]&&!(E[9]))||(E[162]&&!(E[277]));
E[72] = E[9]||E[72];
__T_SurfaceAcquireMeas_R[69] = E[505]&&!(E[72]);
E[505] = E[503]||E[8]||E[8]||E[72];
__T_SurfaceAcquireMeas_R[70] = (E[506]&&!(E[72]))||(E[207]&&!(E[505]));
__T_SurfaceAcquireMeas_R[71] = (E[506]&&!(E[72]))||(E[7]&&!(E[505]));
E[500] = E[505]||E[500];
__T_SurfaceAcquireMeas_R[72] = E[4]&&!(E[500]);
__T_SurfaceAcquireMeas_R[73] = E[497]&&!(E[500]);
__T_SurfaceAcquireMeas_R[74] = E[452]&&!(E[72]);
E[452] = E[444]||E[438]||E[438]||E[72];
__T_SurfaceAcquireMeas_R[75] = (E[453]&&!(E[72]))||(E[205]&&!(E[452]));
__T_SurfaceAcquireMeas_R[76] = (E[453]&&!(E[72]))||(E[15]&&!(E[452]));
E[440] = E[452]||E[441]||E[440];
__T_SurfaceAcquireMeas_R[77] = E[12]&&!(E[440]);
__T_SurfaceAcquireMeas_R[78] = E[17]&&!(E[440]);
__T_SurfaceAcquireMeas_R[79] = E[420]&&!(E[72]);
E[420] = E[412]||E[409]||E[409]||E[72];
__T_SurfaceAcquireMeas_R[80] = (E[421]&&!(E[72]))||(E[206]&&!(E[420]));
__T_SurfaceAcquireMeas_R[81] = (E[421]&&!(E[72]))||(E[23]&&!(E[420]));
E[415] = E[420]||E[416]||E[415];
E[413] = E[415]||E[413];
__T_SurfaceAcquireMeas_R[82] = E[405]&&!(E[413]);
__T_SurfaceAcquireMeas_R[83] = E[407]&&!(E[413]);
__T_SurfaceAcquireMeas_R[84] = E[25]&&!(E[415]);
__T_SurfaceAcquireMeas_R[85] = E[367]&&!(E[72]);
E[367] = E[365]||E[359]||E[359]||E[72];
__T_SurfaceAcquireMeas_R[86] = (E[368]&&!(E[72]))||(E[19]&&!(E[367]));
__T_SurfaceAcquireMeas_R[87] = (E[368]&&!(E[72]))||(E[34]&&!(E[367]));
E[361] = E[367]||E[362]||E[361];
__T_SurfaceAcquireMeas_R[88] = E[31]&&!(E[361]);
__T_SurfaceAcquireMeas_R[89] = E[36]&&!(E[361]);
__T_SurfaceAcquireMeas_R[90] = E[324]&&!(E[72]);
E[324] = E[322]||E[316]||E[316]||E[72];
__T_SurfaceAcquireMeas_R[91] = (E[325]&&!(E[72]))||(E[27]&&!(E[324]));
__T_SurfaceAcquireMeas_R[92] = (E[325]&&!(E[72]))||(E[45]&&!(E[324]));
E[318] = E[324]||E[319]||E[318];
__T_SurfaceAcquireMeas_R[93] = E[42]&&!(E[318]);
__T_SurfaceAcquireMeas_R[94] = E[47]&&!(E[318]);
__T_SurfaceAcquireMeas_R[95] = E[465]&&!(E[72]);
E[465] = E[463]||E[456]||E[456]||E[72];
__T_SurfaceAcquireMeas_R[96] = (E[466]&&!(E[72]))||(E[229]&&!(E[465]));
__T_SurfaceAcquireMeas_R[97] = (E[466]&&!(E[72]))||(E[56]&&!(E[465]));
E[458] = E[465]||E[459]||E[458];
__T_SurfaceAcquireMeas_R[98] = E[53]&&!(E[458]);
__T_SurfaceAcquireMeas_R[99] = E[58]&&!(E[458]);
__T_SurfaceAcquireMeas_R[100] = E[450]&&!(E[72]);
E[10] = E[448]||E[10]||E[10]||E[72];
__T_SurfaceAcquireMeas_R[101] = (E[451]&&!(E[72]))||(E[202]&&!(E[10]));
E[10] = E[67]||E[225]||E[225]||E[10];
E[68] = E[69]||E[68]||E[68]||E[10];
__T_SurfaceAcquireMeas_R[102] = (E[451]&&!(E[72]))||(E[209]&&!(E[68]));
__T_SurfaceAcquireMeas_R[103] = E[3]&&!(E[68]);
__T_SurfaceAcquireMeas_R[104] = (E[451]&&!(E[72]))||(E[211]&&!(E[68]));
__T_SurfaceAcquireMeas_R[105] = E[11]&&!(E[68]);
__T_SurfaceAcquireMeas_R[106] = E[216]&&!(E[10]);
__T_SurfaceAcquireMeas_R[107] = E[22]&&!(E[10]);
__T_SurfaceAcquireMeas_R[108] = E[222]&&!(E[10]);
__T_SurfaceAcquireMeas_R[109] = E[30]&&!(E[10]);
__T_SurfaceAcquireMeas_R[110] = E[228]&&!(E[10]);
__T_SurfaceAcquireMeas_R[111] = E[41]&&!(E[10]);
__T_SurfaceAcquireMeas_R[112] = E[231]&&!(E[10]);
__T_SurfaceAcquireMeas_R[113] = E[52]&&!(E[10]);
__T_SurfaceAcquireMeas_R[114] = (E[451]&&!(E[72]))||(E[219]&&!(E[10]));
__T_SurfaceAcquireMeas_R[115] = E[73]&&!(E[72]);
__T_SurfaceAcquireMeas_R[116] = E[204]&&!(E[72]);
__T_SurfaceAcquireMeas_R[117] = E[65]&&!(E[72]);
__T_SurfaceAcquireMeas_R[118] = E[525]&&!(E[72]);
__T_SurfaceAcquireMeas_R[119] = E[132]&&!(E[9]);
E[132] = E[512]||E[141]||E[141]||E[9];
__T_SurfaceAcquireMeas_R[120] = (E[515]&&!(E[9]))||(E[263]&&!(E[132]));
__T_SurfaceAcquireMeas_R[121] = (E[515]&&!(E[9]))||(E[511]&&!(E[132]));
__T_SurfaceAcquireMeas_R[122] = E[143]&&!(E[132]);
__T_SurfaceAcquireMeas_R[123] = E[402]&&!(E[9]);
E[77] = E[399]||E[77]||E[77]||E[9];
__T_SurfaceAcquireMeas_R[124] = (E[403]&&!(E[9]))||(E[75]&&!(E[77]));
E[77] = E[281]||E[153]||E[153]||E[77];
E[152] = E[233]||E[152]||E[152]||E[77];
E[233] = E[84]||E[152];
__T_SurfaceAcquireMeas_R[125] = (E[403]&&!(E[9]))||(E[248]&&!(E[233]));
__T_SurfaceAcquireMeas_R[126] = E[83]&&!(E[233]);
__T_SurfaceAcquireMeas_R[127] = E[235]&&!(E[152]);
__T_SurfaceAcquireMeas_R[128] = E[86]&&!(E[152]);
E[104] = E[152]||E[104]||E[94]||E[104];
__T_SurfaceAcquireMeas_R[129] = E[240]&&!(E[104]);
__T_SurfaceAcquireMeas_R[130] = E[102]&&!(E[104]);
__T_SurfaceAcquireMeas_R[131] = E[243]&&!(E[104]);
__T_SurfaceAcquireMeas_R[132] = E[105]&&!(E[104]);
__T_SurfaceAcquireMeas_R[133] = E[247]&&!(E[152]);
__T_SurfaceAcquireMeas_R[134] = E[118]&&!(E[152]);
__T_SurfaceAcquireMeas_R[135] = E[249]&&!(E[152]);
__T_SurfaceAcquireMeas_R[136] = E[91]&&!(E[152]);
__T_SurfaceAcquireMeas_R[137] = E[254]&&!(E[152]);
__T_SurfaceAcquireMeas_R[138] = E[129]&&!(E[152]);
__T_SurfaceAcquireMeas_R[139] = E[259]&&!(E[152]);
__T_SurfaceAcquireMeas_R[140] = E[137]&&!(E[152]);
__T_SurfaceAcquireMeas_R[141] = E[265]&&!(E[152]);
__T_SurfaceAcquireMeas_R[142] = E[140]&&!(E[152]);
__T_SurfaceAcquireMeas_R[143] = E[232]&&!(E[152]);
__T_SurfaceAcquireMeas_R[144] = E[260]&&!(E[152]);
__T_SurfaceAcquireMeas_R[145] = E[63]&&!(E[152]);
__T_SurfaceAcquireMeas_R[146] = (E[403]&&!(E[9]))||(E[266]&&!(E[152]));
__T_SurfaceAcquireMeas_R[147] = E[276]&&!(E[152]);
__T_SurfaceAcquireMeas_R[148] = E[148]&&!(E[152]);
__T_SurfaceAcquireMeas_R[149] = E[278]&&!(E[152]);
__T_SurfaceAcquireMeas_R[150] = E[280]&&!(E[152]);
__T_SurfaceAcquireMeas_R[151] = E[164]&&!(E[152]);
__T_SurfaceAcquireMeas_R[152] = (E[403]&&!(E[9]))||(E[234]&&!(E[77]));
__T_SurfaceAcquireMeas_R[153] = E[147]&&!(E[9]);
__T_SurfaceAcquireMeas_R[154] = E[400]&&!(E[9]);
__T_SurfaceAcquireMeas_R[155] = E[74]&&!(E[9]);
__T_SurfaceAcquireMeas_R[156] = E[200]&&!(E[9]);
__T_SurfaceAcquireMeas_R[0] = !(_true);
__T_SurfaceAcquireMeas__reset_input();
return E[14];
}

/* AUTOMATON RESET */

int T_SurfaceAcquireMeas_reset () {
__T_SurfaceAcquireMeas_R[0] = _true;
__T_SurfaceAcquireMeas_R[1] = _false;
__T_SurfaceAcquireMeas_R[2] = _false;
__T_SurfaceAcquireMeas_R[3] = _false;
__T_SurfaceAcquireMeas_R[4] = _false;
__T_SurfaceAcquireMeas_R[5] = _false;
__T_SurfaceAcquireMeas_R[6] = _false;
__T_SurfaceAcquireMeas_R[7] = _false;
__T_SurfaceAcquireMeas_R[8] = _false;
__T_SurfaceAcquireMeas_R[9] = _false;
__T_SurfaceAcquireMeas_R[10] = _false;
__T_SurfaceAcquireMeas_R[11] = _false;
__T_SurfaceAcquireMeas_R[12] = _false;
__T_SurfaceAcquireMeas_R[13] = _false;
__T_SurfaceAcquireMeas_R[14] = _false;
__T_SurfaceAcquireMeas_R[15] = _false;
__T_SurfaceAcquireMeas_R[16] = _false;
__T_SurfaceAcquireMeas_R[17] = _false;
__T_SurfaceAcquireMeas_R[18] = _false;
__T_SurfaceAcquireMeas_R[19] = _false;
__T_SurfaceAcquireMeas_R[20] = _false;
__T_SurfaceAcquireMeas_R[21] = _false;
__T_SurfaceAcquireMeas_R[22] = _false;
__T_SurfaceAcquireMeas_R[23] = _false;
__T_SurfaceAcquireMeas_R[24] = _false;
__T_SurfaceAcquireMeas_R[25] = _false;
__T_SurfaceAcquireMeas_R[26] = _false;
__T_SurfaceAcquireMeas_R[27] = _false;
__T_SurfaceAcquireMeas_R[28] = _false;
__T_SurfaceAcquireMeas_R[29] = _false;
__T_SurfaceAcquireMeas_R[30] = _false;
__T_SurfaceAcquireMeas_R[31] = _false;
__T_SurfaceAcquireMeas_R[32] = _false;
__T_SurfaceAcquireMeas_R[33] = _false;
__T_SurfaceAcquireMeas_R[34] = _false;
__T_SurfaceAcquireMeas_R[35] = _false;
__T_SurfaceAcquireMeas_R[36] = _false;
__T_SurfaceAcquireMeas_R[37] = _false;
__T_SurfaceAcquireMeas_R[38] = _false;
__T_SurfaceAcquireMeas_R[39] = _false;
__T_SurfaceAcquireMeas_R[40] = _false;
__T_SurfaceAcquireMeas_R[41] = _false;
__T_SurfaceAcquireMeas_R[42] = _false;
__T_SurfaceAcquireMeas_R[43] = _false;
__T_SurfaceAcquireMeas_R[44] = _false;
__T_SurfaceAcquireMeas_R[45] = _false;
__T_SurfaceAcquireMeas_R[46] = _false;
__T_SurfaceAcquireMeas_R[47] = _false;
__T_SurfaceAcquireMeas_R[48] = _false;
__T_SurfaceAcquireMeas_R[49] = _false;
__T_SurfaceAcquireMeas_R[50] = _false;
__T_SurfaceAcquireMeas_R[51] = _false;
__T_SurfaceAcquireMeas_R[52] = _false;
__T_SurfaceAcquireMeas_R[53] = _false;
__T_SurfaceAcquireMeas_R[54] = _false;
__T_SurfaceAcquireMeas_R[55] = _false;
__T_SurfaceAcquireMeas_R[56] = _false;
__T_SurfaceAcquireMeas_R[57] = _false;
__T_SurfaceAcquireMeas_R[58] = _false;
__T_SurfaceAcquireMeas_R[59] = _false;
__T_SurfaceAcquireMeas_R[60] = _false;
__T_SurfaceAcquireMeas_R[61] = _false;
__T_SurfaceAcquireMeas_R[62] = _false;
__T_SurfaceAcquireMeas_R[63] = _false;
__T_SurfaceAcquireMeas_R[64] = _false;
__T_SurfaceAcquireMeas_R[65] = _false;
__T_SurfaceAcquireMeas_R[66] = _false;
__T_SurfaceAcquireMeas_R[67] = _false;
__T_SurfaceAcquireMeas_R[68] = _false;
__T_SurfaceAcquireMeas_R[69] = _false;
__T_SurfaceAcquireMeas_R[70] = _false;
__T_SurfaceAcquireMeas_R[71] = _false;
__T_SurfaceAcquireMeas_R[72] = _false;
__T_SurfaceAcquireMeas_R[73] = _false;
__T_SurfaceAcquireMeas_R[74] = _false;
__T_SurfaceAcquireMeas_R[75] = _false;
__T_SurfaceAcquireMeas_R[76] = _false;
__T_SurfaceAcquireMeas_R[77] = _false;
__T_SurfaceAcquireMeas_R[78] = _false;
__T_SurfaceAcquireMeas_R[79] = _false;
__T_SurfaceAcquireMeas_R[80] = _false;
__T_SurfaceAcquireMeas_R[81] = _false;
__T_SurfaceAcquireMeas_R[82] = _false;
__T_SurfaceAcquireMeas_R[83] = _false;
__T_SurfaceAcquireMeas_R[84] = _false;
__T_SurfaceAcquireMeas_R[85] = _false;
__T_SurfaceAcquireMeas_R[86] = _false;
__T_SurfaceAcquireMeas_R[87] = _false;
__T_SurfaceAcquireMeas_R[88] = _false;
__T_SurfaceAcquireMeas_R[89] = _false;
__T_SurfaceAcquireMeas_R[90] = _false;
__T_SurfaceAcquireMeas_R[91] = _false;
__T_SurfaceAcquireMeas_R[92] = _false;
__T_SurfaceAcquireMeas_R[93] = _false;
__T_SurfaceAcquireMeas_R[94] = _false;
__T_SurfaceAcquireMeas_R[95] = _false;
__T_SurfaceAcquireMeas_R[96] = _false;
__T_SurfaceAcquireMeas_R[97] = _false;
__T_SurfaceAcquireMeas_R[98] = _false;
__T_SurfaceAcquireMeas_R[99] = _false;
__T_SurfaceAcquireMeas_R[100] = _false;
__T_SurfaceAcquireMeas_R[101] = _false;
__T_SurfaceAcquireMeas_R[102] = _false;
__T_SurfaceAcquireMeas_R[103] = _false;
__T_SurfaceAcquireMeas_R[104] = _false;
__T_SurfaceAcquireMeas_R[105] = _false;
__T_SurfaceAcquireMeas_R[106] = _false;
__T_SurfaceAcquireMeas_R[107] = _false;
__T_SurfaceAcquireMeas_R[108] = _false;
__T_SurfaceAcquireMeas_R[109] = _false;
__T_SurfaceAcquireMeas_R[110] = _false;
__T_SurfaceAcquireMeas_R[111] = _false;
__T_SurfaceAcquireMeas_R[112] = _false;
__T_SurfaceAcquireMeas_R[113] = _false;
__T_SurfaceAcquireMeas_R[114] = _false;
__T_SurfaceAcquireMeas_R[115] = _false;
__T_SurfaceAcquireMeas_R[116] = _false;
__T_SurfaceAcquireMeas_R[117] = _false;
__T_SurfaceAcquireMeas_R[118] = _false;
__T_SurfaceAcquireMeas_R[119] = _false;
__T_SurfaceAcquireMeas_R[120] = _false;
__T_SurfaceAcquireMeas_R[121] = _false;
__T_SurfaceAcquireMeas_R[122] = _false;
__T_SurfaceAcquireMeas_R[123] = _false;
__T_SurfaceAcquireMeas_R[124] = _false;
__T_SurfaceAcquireMeas_R[125] = _false;
__T_SurfaceAcquireMeas_R[126] = _false;
__T_SurfaceAcquireMeas_R[127] = _false;
__T_SurfaceAcquireMeas_R[128] = _false;
__T_SurfaceAcquireMeas_R[129] = _false;
__T_SurfaceAcquireMeas_R[130] = _false;
__T_SurfaceAcquireMeas_R[131] = _false;
__T_SurfaceAcquireMeas_R[132] = _false;
__T_SurfaceAcquireMeas_R[133] = _false;
__T_SurfaceAcquireMeas_R[134] = _false;
__T_SurfaceAcquireMeas_R[135] = _false;
__T_SurfaceAcquireMeas_R[136] = _false;
__T_SurfaceAcquireMeas_R[137] = _false;
__T_SurfaceAcquireMeas_R[138] = _false;
__T_SurfaceAcquireMeas_R[139] = _false;
__T_SurfaceAcquireMeas_R[140] = _false;
__T_SurfaceAcquireMeas_R[141] = _false;
__T_SurfaceAcquireMeas_R[142] = _false;
__T_SurfaceAcquireMeas_R[143] = _false;
__T_SurfaceAcquireMeas_R[144] = _false;
__T_SurfaceAcquireMeas_R[145] = _false;
__T_SurfaceAcquireMeas_R[146] = _false;
__T_SurfaceAcquireMeas_R[147] = _false;
__T_SurfaceAcquireMeas_R[148] = _false;
__T_SurfaceAcquireMeas_R[149] = _false;
__T_SurfaceAcquireMeas_R[150] = _false;
__T_SurfaceAcquireMeas_R[151] = _false;
__T_SurfaceAcquireMeas_R[152] = _false;
__T_SurfaceAcquireMeas_R[153] = _false;
__T_SurfaceAcquireMeas_R[154] = _false;
__T_SurfaceAcquireMeas_R[155] = _false;
__T_SurfaceAcquireMeas_R[156] = _false;
__T_SurfaceAcquireMeas__reset_input();
return 0;
}
