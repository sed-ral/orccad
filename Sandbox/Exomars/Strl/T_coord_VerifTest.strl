module T_coord_VerifTest:

% Component handle requests and synchronisation signals :
  input handle_A_DrillDislodge_exc_dislodgefailed;
  input handle_A_DrillDislodge_T3_event;
  input A_DrillDislodge_GoodEnd;
  input A_DrillDislodge_ExecAborted;
  output A_DrillDislodge_START;
  output A_DrillDislodge_ABORT;

  input handle_A_DrillMoveTo_exc_drillmovefail;
  input handle_A_DrillMoveTo_exc_evening;
  input handle_A_DrillMoveTo_exc_power_lim;
  input handle_A_DrillMoveTo_T3_event;
  input A_DrillMoveTo_GoodEnd;
  input A_DrillMoveTo_ExecAborted;
  output A_DrillMoveTo_START;
  output A_DrillMoveTo_ABORT;

  input handle_A_DrillRemove_exc_drillblocked;
  input handle_A_DrillRemove_T3_event;
  input A_DrillRemove_GoodEnd;
  input A_DrillRemove_ExecAborted;
  output A_DrillRemove_START;
  output A_DrillRemove_ABORT;

% Task-level events and synchronisation signals :
  input comm_window_started;
  input comm_window_ended;

% Task-level verification signals
  output VERIF_comm_window_started;
  output VERIF_comm_window_ended;
  
  input START;
  input ABORT;
  output GoodEnd;
  output ExecAborted;
  
  output ABORT_COMPONENTS;
  output REQ_HANDLE_T3_EVENT;
  
  % verification signals
  output VERIF_START;
  output VERIF_ABORT;
  output VERIF_GoodEnd;
  output VERIF_ExecAborted;
  
  % Procedure Signature declarations
  procedure Activate_DrillDislodge()();
  procedure Activate_DrillMoveTo()(string);
  procedure Activate_DrillRemove()(double);

  trap Fatality_Trap in
    loop
      await immediate START
      ;
      emit VERIF_START
      ;
      trap Reset_Trap in
        await ABORT
        ;
        emit VERIF_ABORT
        ;
        emit ExecAborted
        ;
        emit VERIF_ExecAborted
        ;
        emit ABORT_COMPONENTS
        ;
        exit Reset_Trap
        ||
        % waiting pre-conditions, if any;
        % we require all conditions to be met => put in [] !
        
        trap ObjectiveReached_Trap in
        [
          [% ------------- operator-defined coordinator body -------------
                    [
                      call Activate_DrillMoveTo()("subsurf");
                      emit A_DrillMoveTo_START
                      ;
                      abort
                        await
                          case A_DrillMoveTo_GoodEnd
                          case A_DrillMoveTo_ExecAborted do
                            [ halt ]
                        end await
                      when
                        case handle_A_DrillMoveTo_exc_drillmovefail do
                          [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                     
                        case handle_A_DrillMoveTo_exc_evening do
                        [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                    
                        case handle_A_DrillMoveTo_exc_power_lim do
                        [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                    
                      case handle_A_DrillMoveTo_T3_event do
                        [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                      end abort
                    ]
                    ;
                    [
                      call Activate_DrillRemove()(50.0);
                      emit A_DrillRemove_START
                      ;
                      abort
                        await
                          case A_DrillRemove_GoodEnd
                          case A_DrillRemove_ExecAborted do
                            [ halt ]
                        end await
                      when
                        case handle_A_DrillRemove_exc_drillblocked do
                              [ await tick ]
                         case handle_A_DrillRemove_T3_event do
                        [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                      end abort
                    ]
                    ;
                    [
                      call Activate_DrillDislodge()();
                      emit A_DrillDislodge_START
                      ;
                      abort
                        await
                          case A_DrillDislodge_GoodEnd
                          case A_DrillDislodge_ExecAborted do
                            [ halt ]
                        end await
                      when
                        case handle_A_DrillDislodge_exc_dislodgefailed do
                        [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                       case handle_A_DrillDislodge_T3_event do
                        [ emit REQ_HANDLE_T3_EVENT ; emit ABORT_COMPONENTS ; exit Fatality_Trap ]
                      end abort
                    ]
          ]% ---------- end of operator-defined coordinator body ---------
          ;
          exit ObjectiveReached_Trap
          ||
          [ % waiting postconditions; we require all conditions to be met => put in [] !
              halt
          ]
          ;
          emit ABORT_COMPONENTS
          ;
          exit ObjectiveReached_Trap
        ]
        handle ObjectiveReached_Trap do
          emit GoodEnd
          ;
          emit VERIF_GoodEnd
        end trap % ObjectiveReached_Trap
        ;
        exit Reset_Trap
      end trap % Reset_Trap
    end loop % main loop
  end trap % Fatality_Trap
end module
