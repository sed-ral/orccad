/* sscc : C CODE OF SORTED EQUATIONS T_coord_AutonomousNavigation - INLINE MODE */

/* AUXILIARY DECLARATIONS */

#ifndef STRLEN
#define STRLEN 81
#endif
#define _COND(A,B,C) ((A)?(B):(C))
#ifdef TRACE_ACTION
#include <stdio.h>
#endif
#ifndef NULL
#define NULL ((char*)0)
#endif

#ifndef __EXEC_STATUS_H_LOADED
#define __EXEC_STATUS_H_LOADED

typedef struct {
unsigned int start:1;
unsigned int kill:1;
unsigned int active:1;
unsigned int suspended:1;
unsigned int prev_active:1;
unsigned int prev_suspended:1;
unsigned int exec_index;
unsigned int task_exec_index;
void (*pStart)();
void (*pRet)();
} __ExecStatus;

#endif
#define __ResetExecStatus(status) {\
   status.prev_active = status.active; \
   status.prev_suspended = status.suspended; \
   status.start = status.kill = status.active = status.suspended = 0; }
#define __DSZ(V) (--(V)<=0)
#define BASIC_TYPES_DEFINED
typedef int boolean;
typedef int integer;
typedef char* string;
#define _true 1
#define _false 0
#define __T_coord_AutonomousNavigation_GENERIC_TEST(TEST) return TEST;
typedef void (*__T_coord_AutonomousNavigation_APF)();
static __T_coord_AutonomousNavigation_APF *__T_coord_AutonomousNavigation_PActionArray;

#include "T_coord_AutonomousNavigation.h"

/* EXTERN DECLARATIONS */

#ifndef _NO_EXTERN_DEFINITIONS
#ifndef _NO_PROCEDURE_DEFINITIONS
#ifndef _Activate_NavCamSwitchOn_DEFINED
#ifndef Activate_NavCamSwitchOn
extern void Activate_NavCamSwitchOn();
#endif
#endif
#ifndef _Activate_NavCamTakeImage_DEFINED
#ifndef Activate_NavCamTakeImage
extern void Activate_NavCamTakeImage();
#endif
#endif
#ifndef _Activate_NavCamComputePath_DEFINED
#ifndef Activate_NavCamComputePath
extern void Activate_NavCamComputePath();
#endif
#endif
#ifndef _Activate_NavCamFollowPath_DEFINED
#ifndef Activate_NavCamFollowPath
extern void Activate_NavCamFollowPath();
#endif
#endif
#ifndef _Activate_NavCamSwitchOff_DEFINED
#ifndef Activate_NavCamSwitchOff
extern void Activate_NavCamSwitchOff();
#endif
#endif
#ifndef _Activate_SelfCheck_DEFINED
#ifndef Activate_SelfCheck
extern void Activate_SelfCheck();
#endif
#endif
#ifndef _Activate_GoToSleep_DEFINED
#ifndef Activate_GoToSleep
extern void Activate_GoToSleep();
#endif
#endif
#ifndef _Activate_ApxsGetMeas_DEFINED
#ifndef Activate_ApxsGetMeas
extern void Activate_ApxsGetMeas();
#endif
#endif
#ifndef _Activate_IddMoveTo_DEFINED
#ifndef Activate_IddMoveTo
extern void Activate_IddMoveTo(string);
#endif
#endif
#ifndef _Activate_LocTrench_DEFINED
#ifndef Activate_LocTrench
extern void Activate_LocTrench();
#endif
#endif
#endif
#endif

/* INITIALIZED CONSTANTS */

/* MEMORY ALLOCATION */

static boolean __T_coord_AutonomousNavigation_V0;
static boolean __T_coord_AutonomousNavigation_V1;
static boolean __T_coord_AutonomousNavigation_V2;
static boolean __T_coord_AutonomousNavigation_V3;
static boolean __T_coord_AutonomousNavigation_V4;
static boolean __T_coord_AutonomousNavigation_V5;
static boolean __T_coord_AutonomousNavigation_V6;
static boolean __T_coord_AutonomousNavigation_V7;
static boolean __T_coord_AutonomousNavigation_V8;
static boolean __T_coord_AutonomousNavigation_V9;
static boolean __T_coord_AutonomousNavigation_V10;
static boolean __T_coord_AutonomousNavigation_V11;
static boolean __T_coord_AutonomousNavigation_V12;
static boolean __T_coord_AutonomousNavigation_V13;
static boolean __T_coord_AutonomousNavigation_V14;
static boolean __T_coord_AutonomousNavigation_V15;
static boolean __T_coord_AutonomousNavigation_V16;
static boolean __T_coord_AutonomousNavigation_V17;
static boolean __T_coord_AutonomousNavigation_V18;
static boolean __T_coord_AutonomousNavigation_V19;
static boolean __T_coord_AutonomousNavigation_V20;
static boolean __T_coord_AutonomousNavigation_V21;
static boolean __T_coord_AutonomousNavigation_V22;
static boolean __T_coord_AutonomousNavigation_V23;
static boolean __T_coord_AutonomousNavigation_V24;
static boolean __T_coord_AutonomousNavigation_V25;
static boolean __T_coord_AutonomousNavigation_V26;
static boolean __T_coord_AutonomousNavigation_V27;
static boolean __T_coord_AutonomousNavigation_V28;
static boolean __T_coord_AutonomousNavigation_V29;
static boolean __T_coord_AutonomousNavigation_V30;
static boolean __T_coord_AutonomousNavigation_V31;
static boolean __T_coord_AutonomousNavigation_V32;
static boolean __T_coord_AutonomousNavigation_V33;
static boolean __T_coord_AutonomousNavigation_V34;
static boolean __T_coord_AutonomousNavigation_V35;
static boolean __T_coord_AutonomousNavigation_V36;
static boolean __T_coord_AutonomousNavigation_V37;
static boolean __T_coord_AutonomousNavigation_V38;


/* INPUT FUNCTIONS */

void T_coord_AutonomousNavigation_I_handle_A_NavCamSwitchOn_T3_event () {
__T_coord_AutonomousNavigation_V0 = _true;
}
void T_coord_AutonomousNavigation_I_A_NavCamSwitchOn_GoodEnd () {
__T_coord_AutonomousNavigation_V1 = _true;
}
void T_coord_AutonomousNavigation_I_A_NavCamSwitchOn_ExecAborted () {
__T_coord_AutonomousNavigation_V2 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_NavCamTakeImage_T3_event () {
__T_coord_AutonomousNavigation_V3 = _true;
}
void T_coord_AutonomousNavigation_I_A_NavCamTakeImage_GoodEnd () {
__T_coord_AutonomousNavigation_V4 = _true;
}
void T_coord_AutonomousNavigation_I_A_NavCamTakeImage_ExecAborted () {
__T_coord_AutonomousNavigation_V5 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_NavCamComputePath_exc_traveltargetinview () {
__T_coord_AutonomousNavigation_V6 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_NavCamComputePath_T3_event () {
__T_coord_AutonomousNavigation_V7 = _true;
}
void T_coord_AutonomousNavigation_I_A_NavCamComputePath_GoodEnd () {
__T_coord_AutonomousNavigation_V8 = _true;
}
void T_coord_AutonomousNavigation_I_A_NavCamComputePath_ExecAborted () {
__T_coord_AutonomousNavigation_V9 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_NavCamFollowPath_T3_event () {
__T_coord_AutonomousNavigation_V10 = _true;
}
void T_coord_AutonomousNavigation_I_A_NavCamFollowPath_GoodEnd () {
__T_coord_AutonomousNavigation_V11 = _true;
}
void T_coord_AutonomousNavigation_I_A_NavCamFollowPath_ExecAborted () {
__T_coord_AutonomousNavigation_V12 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_NavCamSwitchOff_T3_event () {
__T_coord_AutonomousNavigation_V13 = _true;
}
void T_coord_AutonomousNavigation_I_A_NavCamSwitchOff_GoodEnd () {
__T_coord_AutonomousNavigation_V14 = _true;
}
void T_coord_AutonomousNavigation_I_A_NavCamSwitchOff_ExecAborted () {
__T_coord_AutonomousNavigation_V15 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_SelfCheck_T3_event () {
__T_coord_AutonomousNavigation_V16 = _true;
}
void T_coord_AutonomousNavigation_I_A_SelfCheck_GoodEnd () {
__T_coord_AutonomousNavigation_V17 = _true;
}
void T_coord_AutonomousNavigation_I_A_SelfCheck_ExecAborted () {
__T_coord_AutonomousNavigation_V18 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_GoToSleep_T3_event () {
__T_coord_AutonomousNavigation_V19 = _true;
}
void T_coord_AutonomousNavigation_I_A_GoToSleep_GoodEnd () {
__T_coord_AutonomousNavigation_V20 = _true;
}
void T_coord_AutonomousNavigation_I_A_GoToSleep_ExecAborted () {
__T_coord_AutonomousNavigation_V21 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_ApxsGetMeas_T3_event () {
__T_coord_AutonomousNavigation_V22 = _true;
}
void T_coord_AutonomousNavigation_I_A_ApxsGetMeas_GoodEnd () {
__T_coord_AutonomousNavigation_V23 = _true;
}
void T_coord_AutonomousNavigation_I_A_ApxsGetMeas_ExecAborted () {
__T_coord_AutonomousNavigation_V24 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_IddMoveTo_exc_evening () {
__T_coord_AutonomousNavigation_V25 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_IddMoveTo_exc_power_lim () {
__T_coord_AutonomousNavigation_V26 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_IddMoveTo_T3_event () {
__T_coord_AutonomousNavigation_V27 = _true;
}
void T_coord_AutonomousNavigation_I_A_IddMoveTo_GoodEnd () {
__T_coord_AutonomousNavigation_V28 = _true;
}
void T_coord_AutonomousNavigation_I_A_IddMoveTo_ExecAborted () {
__T_coord_AutonomousNavigation_V29 = _true;
}
void T_coord_AutonomousNavigation_I_handle_A_LocTrench_T3_event () {
__T_coord_AutonomousNavigation_V30 = _true;
}
void T_coord_AutonomousNavigation_I_A_LocTrench_GoodEnd () {
__T_coord_AutonomousNavigation_V31 = _true;
}
void T_coord_AutonomousNavigation_I_A_LocTrench_ExecAborted () {
__T_coord_AutonomousNavigation_V32 = _true;
}
void T_coord_AutonomousNavigation_I_sol_day_ended () {
__T_coord_AutonomousNavigation_V33 = _true;
}
void T_coord_AutonomousNavigation_I_sol_day_started () {
__T_coord_AutonomousNavigation_V34 = _true;
}
void T_coord_AutonomousNavigation_I_touchandgo_start () {
__T_coord_AutonomousNavigation_V35 = _true;
}
void T_coord_AutonomousNavigation_I_eng_exp_start () {
__T_coord_AutonomousNavigation_V36 = _true;
}
void T_coord_AutonomousNavigation_I_START () {
__T_coord_AutonomousNavigation_V37 = _true;
}
void T_coord_AutonomousNavigation_I_ABORT () {
__T_coord_AutonomousNavigation_V38 = _true;
}

/* ACTIONS */

/* PREDEFINED ACTIONS */

/* PRESENT SIGNAL TESTS */

#define __T_coord_AutonomousNavigation_A1 \
__T_coord_AutonomousNavigation_V0
#define __T_coord_AutonomousNavigation_A2 \
__T_coord_AutonomousNavigation_V1
#define __T_coord_AutonomousNavigation_A3 \
__T_coord_AutonomousNavigation_V2
#define __T_coord_AutonomousNavigation_A4 \
__T_coord_AutonomousNavigation_V3
#define __T_coord_AutonomousNavigation_A5 \
__T_coord_AutonomousNavigation_V4
#define __T_coord_AutonomousNavigation_A6 \
__T_coord_AutonomousNavigation_V5
#define __T_coord_AutonomousNavigation_A7 \
__T_coord_AutonomousNavigation_V6
#define __T_coord_AutonomousNavigation_A8 \
__T_coord_AutonomousNavigation_V7
#define __T_coord_AutonomousNavigation_A9 \
__T_coord_AutonomousNavigation_V8
#define __T_coord_AutonomousNavigation_A10 \
__T_coord_AutonomousNavigation_V9
#define __T_coord_AutonomousNavigation_A11 \
__T_coord_AutonomousNavigation_V10
#define __T_coord_AutonomousNavigation_A12 \
__T_coord_AutonomousNavigation_V11
#define __T_coord_AutonomousNavigation_A13 \
__T_coord_AutonomousNavigation_V12
#define __T_coord_AutonomousNavigation_A14 \
__T_coord_AutonomousNavigation_V13
#define __T_coord_AutonomousNavigation_A15 \
__T_coord_AutonomousNavigation_V14
#define __T_coord_AutonomousNavigation_A16 \
__T_coord_AutonomousNavigation_V15
#define __T_coord_AutonomousNavigation_A17 \
__T_coord_AutonomousNavigation_V16
#define __T_coord_AutonomousNavigation_A18 \
__T_coord_AutonomousNavigation_V17
#define __T_coord_AutonomousNavigation_A19 \
__T_coord_AutonomousNavigation_V18
#define __T_coord_AutonomousNavigation_A20 \
__T_coord_AutonomousNavigation_V19
#define __T_coord_AutonomousNavigation_A21 \
__T_coord_AutonomousNavigation_V20
#define __T_coord_AutonomousNavigation_A22 \
__T_coord_AutonomousNavigation_V21
#define __T_coord_AutonomousNavigation_A23 \
__T_coord_AutonomousNavigation_V22
#define __T_coord_AutonomousNavigation_A24 \
__T_coord_AutonomousNavigation_V23
#define __T_coord_AutonomousNavigation_A25 \
__T_coord_AutonomousNavigation_V24
#define __T_coord_AutonomousNavigation_A26 \
__T_coord_AutonomousNavigation_V25
#define __T_coord_AutonomousNavigation_A27 \
__T_coord_AutonomousNavigation_V26
#define __T_coord_AutonomousNavigation_A28 \
__T_coord_AutonomousNavigation_V27
#define __T_coord_AutonomousNavigation_A29 \
__T_coord_AutonomousNavigation_V28
#define __T_coord_AutonomousNavigation_A30 \
__T_coord_AutonomousNavigation_V29
#define __T_coord_AutonomousNavigation_A31 \
__T_coord_AutonomousNavigation_V30
#define __T_coord_AutonomousNavigation_A32 \
__T_coord_AutonomousNavigation_V31
#define __T_coord_AutonomousNavigation_A33 \
__T_coord_AutonomousNavigation_V32
#define __T_coord_AutonomousNavigation_A34 \
__T_coord_AutonomousNavigation_V33
#define __T_coord_AutonomousNavigation_A35 \
__T_coord_AutonomousNavigation_V34
#define __T_coord_AutonomousNavigation_A36 \
__T_coord_AutonomousNavigation_V35
#define __T_coord_AutonomousNavigation_A37 \
__T_coord_AutonomousNavigation_V36
#define __T_coord_AutonomousNavigation_A38 \
__T_coord_AutonomousNavigation_V37
#define __T_coord_AutonomousNavigation_A39 \
__T_coord_AutonomousNavigation_V38

/* OUTPUT ACTIONS */

#define __T_coord_AutonomousNavigation_A40 \
T_coord_AutonomousNavigation_O_A_NavCamSwitchOn_START()
#define __T_coord_AutonomousNavigation_A41 \
T_coord_AutonomousNavigation_O_A_NavCamSwitchOn_ABORT()
#define __T_coord_AutonomousNavigation_A42 \
T_coord_AutonomousNavigation_O_A_NavCamTakeImage_START()
#define __T_coord_AutonomousNavigation_A43 \
T_coord_AutonomousNavigation_O_A_NavCamTakeImage_ABORT()
#define __T_coord_AutonomousNavigation_A44 \
T_coord_AutonomousNavigation_O_A_NavCamComputePath_START()
#define __T_coord_AutonomousNavigation_A45 \
T_coord_AutonomousNavigation_O_A_NavCamComputePath_ABORT()
#define __T_coord_AutonomousNavigation_A46 \
T_coord_AutonomousNavigation_O_A_NavCamFollowPath_START()
#define __T_coord_AutonomousNavigation_A47 \
T_coord_AutonomousNavigation_O_A_NavCamFollowPath_ABORT()
#define __T_coord_AutonomousNavigation_A48 \
T_coord_AutonomousNavigation_O_A_NavCamSwitchOff_START()
#define __T_coord_AutonomousNavigation_A49 \
T_coord_AutonomousNavigation_O_A_NavCamSwitchOff_ABORT()
#define __T_coord_AutonomousNavigation_A50 \
T_coord_AutonomousNavigation_O_A_SelfCheck_START()
#define __T_coord_AutonomousNavigation_A51 \
T_coord_AutonomousNavigation_O_A_SelfCheck_ABORT()
#define __T_coord_AutonomousNavigation_A52 \
T_coord_AutonomousNavigation_O_A_GoToSleep_START()
#define __T_coord_AutonomousNavigation_A53 \
T_coord_AutonomousNavigation_O_A_GoToSleep_ABORT()
#define __T_coord_AutonomousNavigation_A54 \
T_coord_AutonomousNavigation_O_A_ApxsGetMeas_START()
#define __T_coord_AutonomousNavigation_A55 \
T_coord_AutonomousNavigation_O_A_ApxsGetMeas_ABORT()
#define __T_coord_AutonomousNavigation_A56 \
T_coord_AutonomousNavigation_O_A_IddMoveTo_START()
#define __T_coord_AutonomousNavigation_A57 \
T_coord_AutonomousNavigation_O_A_IddMoveTo_ABORT()
#define __T_coord_AutonomousNavigation_A58 \
T_coord_AutonomousNavigation_O_A_LocTrench_START()
#define __T_coord_AutonomousNavigation_A59 \
T_coord_AutonomousNavigation_O_A_LocTrench_ABORT()
#define __T_coord_AutonomousNavigation_A60 \
T_coord_AutonomousNavigation_O_GoodEnd()
#define __T_coord_AutonomousNavigation_A61 \
T_coord_AutonomousNavigation_O_ExecAborted()
#define __T_coord_AutonomousNavigation_A62 \
T_coord_AutonomousNavigation_O_ABORT_COMPONENTS()
#define __T_coord_AutonomousNavigation_A63 \
T_coord_AutonomousNavigation_O_REQ_HANDLE_T3_EVENT()
#define __T_coord_AutonomousNavigation_A64 \
T_coord_AutonomousNavigation_O_VERIF_START()
#define __T_coord_AutonomousNavigation_A65 \
T_coord_AutonomousNavigation_O_VERIF_ABORT()
#define __T_coord_AutonomousNavigation_A66 \
T_coord_AutonomousNavigation_O_VERIF_GoodEnd()
#define __T_coord_AutonomousNavigation_A67 \
T_coord_AutonomousNavigation_O_VERIF_ExecAborted()

/* ASSIGNMENTS */

#define __T_coord_AutonomousNavigation_A68 \
__T_coord_AutonomousNavigation_V0 = _false
#define __T_coord_AutonomousNavigation_A69 \
__T_coord_AutonomousNavigation_V1 = _false
#define __T_coord_AutonomousNavigation_A70 \
__T_coord_AutonomousNavigation_V2 = _false
#define __T_coord_AutonomousNavigation_A71 \
__T_coord_AutonomousNavigation_V3 = _false
#define __T_coord_AutonomousNavigation_A72 \
__T_coord_AutonomousNavigation_V4 = _false
#define __T_coord_AutonomousNavigation_A73 \
__T_coord_AutonomousNavigation_V5 = _false
#define __T_coord_AutonomousNavigation_A74 \
__T_coord_AutonomousNavigation_V6 = _false
#define __T_coord_AutonomousNavigation_A75 \
__T_coord_AutonomousNavigation_V7 = _false
#define __T_coord_AutonomousNavigation_A76 \
__T_coord_AutonomousNavigation_V8 = _false
#define __T_coord_AutonomousNavigation_A77 \
__T_coord_AutonomousNavigation_V9 = _false
#define __T_coord_AutonomousNavigation_A78 \
__T_coord_AutonomousNavigation_V10 = _false
#define __T_coord_AutonomousNavigation_A79 \
__T_coord_AutonomousNavigation_V11 = _false
#define __T_coord_AutonomousNavigation_A80 \
__T_coord_AutonomousNavigation_V12 = _false
#define __T_coord_AutonomousNavigation_A81 \
__T_coord_AutonomousNavigation_V13 = _false
#define __T_coord_AutonomousNavigation_A82 \
__T_coord_AutonomousNavigation_V14 = _false
#define __T_coord_AutonomousNavigation_A83 \
__T_coord_AutonomousNavigation_V15 = _false
#define __T_coord_AutonomousNavigation_A84 \
__T_coord_AutonomousNavigation_V16 = _false
#define __T_coord_AutonomousNavigation_A85 \
__T_coord_AutonomousNavigation_V17 = _false
#define __T_coord_AutonomousNavigation_A86 \
__T_coord_AutonomousNavigation_V18 = _false
#define __T_coord_AutonomousNavigation_A87 \
__T_coord_AutonomousNavigation_V19 = _false
#define __T_coord_AutonomousNavigation_A88 \
__T_coord_AutonomousNavigation_V20 = _false
#define __T_coord_AutonomousNavigation_A89 \
__T_coord_AutonomousNavigation_V21 = _false
#define __T_coord_AutonomousNavigation_A90 \
__T_coord_AutonomousNavigation_V22 = _false
#define __T_coord_AutonomousNavigation_A91 \
__T_coord_AutonomousNavigation_V23 = _false
#define __T_coord_AutonomousNavigation_A92 \
__T_coord_AutonomousNavigation_V24 = _false
#define __T_coord_AutonomousNavigation_A93 \
__T_coord_AutonomousNavigation_V25 = _false
#define __T_coord_AutonomousNavigation_A94 \
__T_coord_AutonomousNavigation_V26 = _false
#define __T_coord_AutonomousNavigation_A95 \
__T_coord_AutonomousNavigation_V27 = _false
#define __T_coord_AutonomousNavigation_A96 \
__T_coord_AutonomousNavigation_V28 = _false
#define __T_coord_AutonomousNavigation_A97 \
__T_coord_AutonomousNavigation_V29 = _false
#define __T_coord_AutonomousNavigation_A98 \
__T_coord_AutonomousNavigation_V30 = _false
#define __T_coord_AutonomousNavigation_A99 \
__T_coord_AutonomousNavigation_V31 = _false
#define __T_coord_AutonomousNavigation_A100 \
__T_coord_AutonomousNavigation_V32 = _false
#define __T_coord_AutonomousNavigation_A101 \
__T_coord_AutonomousNavigation_V33 = _false
#define __T_coord_AutonomousNavigation_A102 \
__T_coord_AutonomousNavigation_V34 = _false
#define __T_coord_AutonomousNavigation_A103 \
__T_coord_AutonomousNavigation_V35 = _false
#define __T_coord_AutonomousNavigation_A104 \
__T_coord_AutonomousNavigation_V36 = _false
#define __T_coord_AutonomousNavigation_A105 \
__T_coord_AutonomousNavigation_V37 = _false
#define __T_coord_AutonomousNavigation_A106 \
__T_coord_AutonomousNavigation_V38 = _false

/* PROCEDURE CALLS */

#define __T_coord_AutonomousNavigation_A107 \
Activate_NavCamFollowPath()
#define __T_coord_AutonomousNavigation_A108 \
Activate_LocTrench()
#define __T_coord_AutonomousNavigation_A109 \
Activate_SelfCheck()
#define __T_coord_AutonomousNavigation_A110 \
Activate_SelfCheck()

/* CONDITIONS */

/* DECREMENTS */

/* START ACTIONS */

/* KILL ACTIONS */

/* SUSPEND ACTIONS */

/* ACTIVATE ACTIONS */

/* WRITE ARGS ACTIONS */

/* RESET ACTIONS */

/* ACTION SEQUENCES */

/* FUNCTIONS RETURNING NUMBER OF EXEC */

int T_coord_AutonomousNavigation_number_of_execs () {
return (0);
}


/* AUTOMATON (STATE ACTION-TREES) */



static void __T_coord_AutonomousNavigation__reset_input () {
__T_coord_AutonomousNavigation_V0 = _false;
__T_coord_AutonomousNavigation_V1 = _false;
__T_coord_AutonomousNavigation_V2 = _false;
__T_coord_AutonomousNavigation_V3 = _false;
__T_coord_AutonomousNavigation_V4 = _false;
__T_coord_AutonomousNavigation_V5 = _false;
__T_coord_AutonomousNavigation_V6 = _false;
__T_coord_AutonomousNavigation_V7 = _false;
__T_coord_AutonomousNavigation_V8 = _false;
__T_coord_AutonomousNavigation_V9 = _false;
__T_coord_AutonomousNavigation_V10 = _false;
__T_coord_AutonomousNavigation_V11 = _false;
__T_coord_AutonomousNavigation_V12 = _false;
__T_coord_AutonomousNavigation_V13 = _false;
__T_coord_AutonomousNavigation_V14 = _false;
__T_coord_AutonomousNavigation_V15 = _false;
__T_coord_AutonomousNavigation_V16 = _false;
__T_coord_AutonomousNavigation_V17 = _false;
__T_coord_AutonomousNavigation_V18 = _false;
__T_coord_AutonomousNavigation_V19 = _false;
__T_coord_AutonomousNavigation_V20 = _false;
__T_coord_AutonomousNavigation_V21 = _false;
__T_coord_AutonomousNavigation_V22 = _false;
__T_coord_AutonomousNavigation_V23 = _false;
__T_coord_AutonomousNavigation_V24 = _false;
__T_coord_AutonomousNavigation_V25 = _false;
__T_coord_AutonomousNavigation_V26 = _false;
__T_coord_AutonomousNavigation_V27 = _false;
__T_coord_AutonomousNavigation_V28 = _false;
__T_coord_AutonomousNavigation_V29 = _false;
__T_coord_AutonomousNavigation_V30 = _false;
__T_coord_AutonomousNavigation_V31 = _false;
__T_coord_AutonomousNavigation_V32 = _false;
__T_coord_AutonomousNavigation_V33 = _false;
__T_coord_AutonomousNavigation_V34 = _false;
__T_coord_AutonomousNavigation_V35 = _false;
__T_coord_AutonomousNavigation_V36 = _false;
__T_coord_AutonomousNavigation_V37 = _false;
__T_coord_AutonomousNavigation_V38 = _false;
}

/* REDEFINABLE BIT TYPE */

#ifndef __SSC_BIT_TYPE_DEFINED
typedef char __SSC_BIT_TYPE;
#endif

/* REGISTER VARIABLES */

static __SSC_BIT_TYPE __T_coord_AutonomousNavigation_R[19] = {_true,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false,
 _false};

/* AUTOMATON ENGINE */

int T_coord_AutonomousNavigation () {
/* AUXILIARY VARIABLES */

static __SSC_BIT_TYPE E[43];
if (!(_true)) {
__T_coord_AutonomousNavigation_A40;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A40\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A41;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A41\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A42;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A42\n");
#endif
}
E[0] = __T_coord_AutonomousNavigation_R[3]||__T_coord_AutonomousNavigation_R[4];
E[1] = E[0]||__T_coord_AutonomousNavigation_R[5];
E[2] = __T_coord_AutonomousNavigation_R[9]||__T_coord_AutonomousNavigation_R[10];
E[3] = __T_coord_AutonomousNavigation_R[7]||__T_coord_AutonomousNavigation_R[8];
E[4] = __T_coord_AutonomousNavigation_R[6]||E[2]||E[3];
E[5] = E[1]||E[4];
E[6] = E[5]&&!(__T_coord_AutonomousNavigation_R[0]);
E[7] = __T_coord_AutonomousNavigation_R[11]&&!(__T_coord_AutonomousNavigation_R[0]);
E[8] = E[7]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 34\n"),
#endif
__T_coord_AutonomousNavigation_A34);
E[9] = E[6]&&E[8];
if (E[9]) {
__T_coord_AutonomousNavigation_A43;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A43\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A44;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A44\n");
#endif
}
if (E[9]) {
__T_coord_AutonomousNavigation_A45;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A45\n");
#endif
}
E[10] = __T_coord_AutonomousNavigation_R[2]&&!(__T_coord_AutonomousNavigation_R[0]);
E[11] = E[10]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 39\n"),
#endif
__T_coord_AutonomousNavigation_A39);
E[12] = __T_coord_AutonomousNavigation_R[13]||__T_coord_AutonomousNavigation_R[14];
E[13] = __T_coord_AutonomousNavigation_R[16]||__T_coord_AutonomousNavigation_R[17];
E[14] = __T_coord_AutonomousNavigation_R[11]||__T_coord_AutonomousNavigation_R[12]||E[12]||__T_coord_AutonomousNavigation_R[15]||E[13];
E[15] = E[5]||E[14];
E[16] = E[15]||__T_coord_AutonomousNavigation_R[18];
E[17] = __T_coord_AutonomousNavigation_R[2]||E[16];
E[10] = E[10]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 39\n"),
#endif
__T_coord_AutonomousNavigation_A39));
E[10] = __T_coord_AutonomousNavigation_R[2]&&E[10];
E[18] = (E[17]&&!(__T_coord_AutonomousNavigation_R[2]))||E[10];
E[19] = E[18]||E[11];
E[6] = E[6]&&!(E[8]);
E[20] = __T_coord_AutonomousNavigation_R[5]&&E[6];
E[2] = E[2]&&E[6];
E[21] = E[2]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 31\n"),
#endif
__T_coord_AutonomousNavigation_A31));
E[22] = __T_coord_AutonomousNavigation_R[9]&&E[21];
E[23] = E[22]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 32\n"),
#endif
__T_coord_AutonomousNavigation_A32);
E[24] = E[20]&&E[23];
if (E[24]) {
__T_coord_AutonomousNavigation_A107;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A107\n");
#endif
}
E[0] = E[0]&&E[6];
E[25] = E[0]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__T_coord_AutonomousNavigation_A11));
E[26] = __T_coord_AutonomousNavigation_R[3]&&E[25];
E[27] = E[26]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__T_coord_AutonomousNavigation_A12));
E[28] = E[27]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__T_coord_AutonomousNavigation_A13));
E[28] = E[24]||(__T_coord_AutonomousNavigation_R[3]&&E[28]);
E[26] = E[26]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 12\n"),
#endif
__T_coord_AutonomousNavigation_A12);
E[29] = __T_coord_AutonomousNavigation_R[6]&&E[6];
E[30] = E[29]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 37\n"),
#endif
__T_coord_AutonomousNavigation_A37);
E[31] = E[30]||(__T_coord_AutonomousNavigation_R[7]&&E[6]);
E[32] = E[26]&&E[31];
E[26] = E[32]||(__T_coord_AutonomousNavigation_R[5]&&E[20]&&!(E[23]))||(E[26]&&!(E[31]));
E[27] = E[27]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 13\n"),
#endif
__T_coord_AutonomousNavigation_A13);
E[25] = E[27]||(__T_coord_AutonomousNavigation_R[4]&&E[25]);
E[29] = E[29]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 37\n"),
#endif
__T_coord_AutonomousNavigation_A37));
E[29] = __T_coord_AutonomousNavigation_R[6]&&E[29];
E[6] = __T_coord_AutonomousNavigation_R[8]&&E[6];
E[27] = (E[30]&&E[32])||(E[6]&&E[32]);
E[20] = (E[3]&&!(__T_coord_AutonomousNavigation_R[7]))||E[31];
E[32] = (E[30]&&!(E[32]))||(__T_coord_AutonomousNavigation_R[8]&&E[6]&&!(E[32]));
E[3] = (E[3]&&!(__T_coord_AutonomousNavigation_R[8]))||E[32];
E[27] = E[27]&&E[20]&&(E[3]||E[27]);
if (E[27]) {
__T_coord_AutonomousNavigation_A108;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A108\n");
#endif
}
E[22] = E[22]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 32\n"),
#endif
__T_coord_AutonomousNavigation_A32));
E[6] = E[22]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 33\n"),
#endif
__T_coord_AutonomousNavigation_A33));
E[6] = E[27]||(__T_coord_AutonomousNavigation_R[9]&&E[6]);
E[22] = E[22]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 33\n"),
#endif
__T_coord_AutonomousNavigation_A33);
E[21] = E[22]||(__T_coord_AutonomousNavigation_R[10]&&E[21]);
E[3] = (E[31]||E[32])&&E[20]&&E[3];
E[1] = (E[5]&&!(E[1]))||E[28]||E[26]||E[25];
E[23] = (E[5]&&!(E[4]))||E[23]||E[29]||E[6]||E[21]||E[3];
E[3] = (E[28]||E[26]||E[25]||E[29]||E[6]||E[21]||E[3])&&E[1]&&E[23];
E[13] = E[13]&&!(__T_coord_AutonomousNavigation_R[0]);
E[4] = E[13]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__T_coord_AutonomousNavigation_A17));
E[20] = __T_coord_AutonomousNavigation_R[16]&&E[4];
E[22] = E[20]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__T_coord_AutonomousNavigation_A18);
E[7] = E[7]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 34\n"),
#endif
__T_coord_AutonomousNavigation_A34));
E[7] = E[22]||(__T_coord_AutonomousNavigation_R[11]&&E[7]);
E[22] = __T_coord_AutonomousNavigation_R[12]&&!(__T_coord_AutonomousNavigation_R[0]);
E[30] = (E[8]&&!(E[9]))||(__T_coord_AutonomousNavigation_R[12]&&E[22]&&!(E[9]));
E[8] = (E[22]&&E[9])||(E[8]&&E[9]);
if (E[8]) {
__T_coord_AutonomousNavigation_A109;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A109\n");
#endif
}
E[12] = E[12]&&!(__T_coord_AutonomousNavigation_R[0]);
E[22] = E[12]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__T_coord_AutonomousNavigation_A17));
E[33] = __T_coord_AutonomousNavigation_R[13]&&E[22];
E[34] = E[33]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__T_coord_AutonomousNavigation_A18));
E[35] = E[34]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__T_coord_AutonomousNavigation_A19));
E[35] = E[8]||(__T_coord_AutonomousNavigation_R[13]&&E[35]);
E[33] = E[33]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__T_coord_AutonomousNavigation_A18);
E[36] = __T_coord_AutonomousNavigation_R[15]&&!(__T_coord_AutonomousNavigation_R[0]);
E[37] = E[36]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 35\n"),
#endif
__T_coord_AutonomousNavigation_A35));
E[37] = E[33]||(__T_coord_AutonomousNavigation_R[15]&&E[37]);
E[36] = E[36]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 35\n"),
#endif
__T_coord_AutonomousNavigation_A35);
if (E[36]) {
__T_coord_AutonomousNavigation_A110;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A110\n");
#endif
}
E[20] = E[20]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 18\n"),
#endif
__T_coord_AutonomousNavigation_A18));
E[33] = E[20]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__T_coord_AutonomousNavigation_A19));
E[33] = E[36]||(__T_coord_AutonomousNavigation_R[16]&&E[33]);
E[20] = E[20]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__T_coord_AutonomousNavigation_A19);
E[4] = E[20]||(__T_coord_AutonomousNavigation_R[17]&&E[4]);
E[34] = E[34]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 19\n"),
#endif
__T_coord_AutonomousNavigation_A19);
E[22] = E[34]||(__T_coord_AutonomousNavigation_R[14]&&E[22]);
E[5] = (E[15]&&!(E[5]))||E[9]||E[3];
E[14] = (E[15]&&!(E[14]))||E[7]||E[30]||E[35]||E[37]||E[33]||E[4]||E[22];
E[3] = (E[3]||E[7]||E[30]||E[35]||E[37]||E[33]||E[4]||E[22])&&E[5]&&E[14];
E[34] = __T_coord_AutonomousNavigation_R[18]&&!(__T_coord_AutonomousNavigation_R[0]);
E[15] = (E[16]&&!(E[15]))||E[3];
E[20] = (E[16]&&!(__T_coord_AutonomousNavigation_R[18]))||E[34];
E[3] = (E[3]||E[34])&&E[15]&&E[20];
E[16] = (E[17]&&!(E[16]))||E[3];
E[38] = E[11]&&E[19]&&E[16];
E[39] = E[38]||__T_coord_AutonomousNavigation_R[0];
E[40] = E[39]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 38\n"),
#endif
__T_coord_AutonomousNavigation_A38);
E[41] = __T_coord_AutonomousNavigation_R[1]&&!(__T_coord_AutonomousNavigation_R[0]);
E[42] = E[41]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 38\n"),
#endif
__T_coord_AutonomousNavigation_A38);
E[42] = E[40]||E[42];
if (E[42]) {
__T_coord_AutonomousNavigation_A107;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A107\n");
#endif
}
if (E[42]||E[24]) {
__T_coord_AutonomousNavigation_A46;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A46\n");
#endif
}
if (E[9]) {
__T_coord_AutonomousNavigation_A47;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A47\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A48;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A48\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A49;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A49\n");
#endif
}
if (E[8]||E[36]) {
__T_coord_AutonomousNavigation_A50;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A50\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A51;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A51\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A52;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A52\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A53;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A53\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A54;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A54\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A55;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A55\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A56;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A56\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A57;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A57\n");
#endif
}
if (E[27]) {
__T_coord_AutonomousNavigation_A58;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A58\n");
#endif
}
if (E[9]) {
__T_coord_AutonomousNavigation_A59;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A59\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A60;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A60\n");
#endif
}
if (E[11]) {
__T_coord_AutonomousNavigation_A61;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A61\n");
#endif
}
E[0] = E[0]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 11\n"),
#endif
__T_coord_AutonomousNavigation_A11);
E[2] = E[2]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 31\n"),
#endif
__T_coord_AutonomousNavigation_A31);
E[12] = E[12]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__T_coord_AutonomousNavigation_A17);
E[13] = E[13]&&(
#ifdef TRACE_ACTION
fprintf(stderr, "test 17\n"),
#endif
__T_coord_AutonomousNavigation_A17);
if (E[11]||E[0]||E[2]||E[12]||E[13]) {
__T_coord_AutonomousNavigation_A62;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A62\n");
#endif
}
if (E[0]||E[2]||E[12]||E[13]) {
__T_coord_AutonomousNavigation_A63;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A63\n");
#endif
}
if (E[42]) {
__T_coord_AutonomousNavigation_A64;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A64\n");
#endif
}
if (E[11]) {
__T_coord_AutonomousNavigation_A65;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A65\n");
#endif
}
if (!(_true)) {
__T_coord_AutonomousNavigation_A66;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A66\n");
#endif
}
if (E[11]) {
__T_coord_AutonomousNavigation_A67;
#ifdef TRACE_ACTION
fprintf(stderr, "__T_coord_AutonomousNavigation_A67\n");
#endif
}
E[40] = E[0]||E[2]||E[12]||E[13];
E[23] = (E[0]||E[2])&&(E[1]||E[0])&&(E[23]||E[2]);
E[14] = (E[23]||E[12]||E[13])&&(E[5]||E[23])&&(E[14]||E[12]||E[13]);
E[20] = E[14]&&(E[15]||E[14])&&E[20];
E[19] = E[20]&&E[19]&&(E[16]||E[20]);
E[15] = E[19];
E[39] = E[39]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 38\n"),
#endif
__T_coord_AutonomousNavigation_A38));
E[41] = E[41]&&!((
#ifdef TRACE_ACTION
fprintf(stderr, "test 38\n"),
#endif
__T_coord_AutonomousNavigation_A38));
E[41] = E[39]||(__T_coord_AutonomousNavigation_R[1]&&E[41]);
E[16] = E[42]||((E[10]||E[3])&&E[18]&&E[16])||E[41];
E[17] = E[17]||__T_coord_AutonomousNavigation_R[1];
E[19] = E[38]||E[19];
__T_coord_AutonomousNavigation_R[2] = E[42]||(E[10]&&!(E[19]));
E[19] = E[20]||E[20]||E[19];
E[14] = E[14]||E[19];
E[23] = E[23]||E[14];
__T_coord_AutonomousNavigation_R[3] = E[42]||(E[28]&&!(E[23]));
__T_coord_AutonomousNavigation_R[4] = E[25]&&!(E[23]);
__T_coord_AutonomousNavigation_R[5] = E[26]&&!(E[23]);
__T_coord_AutonomousNavigation_R[6] = E[42]||(E[29]&&!(E[23]));
E[29] = E[27]||E[23];
__T_coord_AutonomousNavigation_R[7] = E[31]&&!(E[29]);
__T_coord_AutonomousNavigation_R[8] = E[32]&&!(E[29]);
__T_coord_AutonomousNavigation_R[9] = E[6]&&!(E[23]);
__T_coord_AutonomousNavigation_R[10] = E[21]&&!(E[23]);
__T_coord_AutonomousNavigation_R[11] = E[42]||(E[7]&&!(E[14]));
__T_coord_AutonomousNavigation_R[12] = E[30]&&!(E[14]);
__T_coord_AutonomousNavigation_R[13] = E[35]&&!(E[14]);
__T_coord_AutonomousNavigation_R[14] = E[22]&&!(E[14]);
__T_coord_AutonomousNavigation_R[15] = E[37]&&!(E[14]);
__T_coord_AutonomousNavigation_R[16] = E[33]&&!(E[14]);
__T_coord_AutonomousNavigation_R[17] = E[4]&&!(E[14]);
__T_coord_AutonomousNavigation_R[18] = E[42]||(E[34]&&!(E[19]));
__T_coord_AutonomousNavigation_R[0] = !(_true);
__T_coord_AutonomousNavigation_R[1] = E[41];
__T_coord_AutonomousNavigation__reset_input();
return E[16];
}

/* AUTOMATON RESET */

int T_coord_AutonomousNavigation_reset () {
__T_coord_AutonomousNavigation_R[0] = _true;
__T_coord_AutonomousNavigation_R[1] = _false;
__T_coord_AutonomousNavigation_R[2] = _false;
__T_coord_AutonomousNavigation_R[3] = _false;
__T_coord_AutonomousNavigation_R[4] = _false;
__T_coord_AutonomousNavigation_R[5] = _false;
__T_coord_AutonomousNavigation_R[6] = _false;
__T_coord_AutonomousNavigation_R[7] = _false;
__T_coord_AutonomousNavigation_R[8] = _false;
__T_coord_AutonomousNavigation_R[9] = _false;
__T_coord_AutonomousNavigation_R[10] = _false;
__T_coord_AutonomousNavigation_R[11] = _false;
__T_coord_AutonomousNavigation_R[12] = _false;
__T_coord_AutonomousNavigation_R[13] = _false;
__T_coord_AutonomousNavigation_R[14] = _false;
__T_coord_AutonomousNavigation_R[15] = _false;
__T_coord_AutonomousNavigation_R[16] = _false;
__T_coord_AutonomousNavigation_R[17] = _false;
__T_coord_AutonomousNavigation_R[18] = _false;
__T_coord_AutonomousNavigation__reset_input();
return 0;
}
