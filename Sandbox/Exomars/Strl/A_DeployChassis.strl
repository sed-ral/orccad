module A_DeployChassis:

% Parametric event interface and synchronisation signals :
% synchronisation signals
  input START;
  input ABORT;
  output GoodEnd;
  output ExecAborted;

% verification signals
  output VERIF_START;
  output VERIF_ABORT;
  output VERIF_GoodEnd;
  output VERIF_ExecAborted;
  output VERIF_SUBSYSTEM_ACQUIRED;
  output VERIF_SUBSYSTEM_RELEASED;

% -------------------------------------

% System event interface :
  input prec_chassis_ok,
        TO_prec_chassis_ok;
  input post_chassis_deployed,
        TO_post_chassis_deployed;

  output handle_T3_event;

  relation START#ABORT#prec_chassis_ok#TO_prec_chassis_ok#post_chassis_deployed#TO_post_chassis_deployed;

  
  trap Fatality_Trap in
    loop
      await immediate START
      ;
      emit VERIF_START
      ;
      emit VERIF_SUBSYSTEM_ACQUIRED
      ;
      % This would be the place to call Action Implementation A_move_to_activate() ;
      trap Reset_Trap in
        await ABORT
        ;
        emit VERIF_ABORT
        ;
        emit ExecAborted
        ;
        emit VERIF_ExecAborted
        ;
        exit Reset_Trap
        ||
        % waiting pre-conditions, if any
        % we require all conditions to be met => put in [] !
        [
          abort
            await prec_chassis_ok
          when TO_prec_chassis_ok do
            emit handle_T3_event
            ;
            exit Fatality_Trap
          end abort
        ]
        ;
        [
          % waiting exceptions, if any
          [ % waiting postconditions, or halt if none
            % we require all conditions to be met => put in [] !
            abort
              await post_chassis_deployed
            when TO_post_chassis_deployed do
              emit handle_T3_event
              ;
              exit Fatality_Trap
            end abort
          ]
          ;
          emit GoodEnd
          ;
          emit VERIF_GoodEnd
          ;
          exit Reset_Trap
        ]
      end trap % Reset_Trap
      ;
      emit VERIF_SUBSYSTEM_RELEASED
    end loop
  end trap % Fatality_Trap
  ;
  emit VERIF_SUBSYSTEM_RELEASED
end module
